package uk.co.nickthecoder.tickle.launcher

import org.lwjgl.opengl.GL11
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import java.io.File

/**
 * This is the main entry point for Tickle.
 * It can start games, and the Editor for games which only use scripting languages.
 * It can NOT start games which require additional jar files (yet?)
 *
 * If you want a stand-alone game, or if your game requires additional jar files
 * not contained in Tickle, then you will need to write your own main entry point.
 */
object Tickle {

    @JvmStatic
    fun main(args: Array<String>) {

        var endOfOptions = false
        var showHelp = false
        var showVersion = false
        var verbose = false
        var fullScreen: Boolean? = null
        var sceneName: String? = null
        var resourcesFile: File? = null
        var startEditor = false

        /**
         * @return The number of arguments parsed (usually 1, but 2 for --scene XXX
         */
        fun parseArg(i: Int): Int {

            val arg = args[i]

            if (!endOfOptions) {
                when (arg) {
                    "--" -> {
                        endOfOptions = true
                        return 1
                    }
                    "--help" -> showHelp = true
                    "--version" -> showVersion = true
                    "--verbose" -> verbose = true
                    "--edit" -> startEditor = true
                    "--fullscreen" -> fullScreen = true
                    "--windowed" -> fullScreen = false
                    "--scene" -> {
                        if (i + 1 < args.size) {
                            sceneName = args[i + 1]
                            return 2
                        }
                    }
                    else -> {
                        if (arg.startsWith("-")) {
                            severe("Unexpected argument : $arg\n")
                            System.exit(-1)
                        } else {
                            endOfOptions = true
                        }
                    }
                }
            }
            if (endOfOptions) {
                if (resourcesFile == null) {
                    resourcesFile = File(arg).absoluteFile
                    endOfOptions = true
                    return 1
                } else {
                    println("Too many resource files ('$arg'). Only 1 permitted\n")
                    println(USAGE)
                    System.exit(-1)
                }
            }
            return 1
        }

        var i = 0
        while (i < args.size) {
            i += parseArg(i)
        }

        if (showVersion) {
            TickleErrorHandler.instance = SilentTickleErrorHandler()
            OpenGL.begin()
            onOpenGLThread {
                val window = Window( "Version", 640, 480 )
                println("Tickle version : ${GameInfo.tickleVersion}")
                println("OpenGL Version : ${GL11.glGetString(GL11.GL_VERSION)}")
                window.delete()
                OpenGL.end()
            }
            return
        }

        if (showHelp) {
            println(USAGE)
            return
        }
        if (!verbose) {
            TickleErrorHandler.instance = SilentTickleErrorHandler()
            GameErrorHandler.instance = SimpleGameErrorHandler()
        }

        if (resourcesFile == null) {
            LauncherApp.start()
        } else {

            if (!resourcesFile!!.exists()) {
                println("File $$resourcesFile not found")
                System.exit(1)
            }

            LauncherWindow.addRecent(resourcesFile!!)
            if (startEditor) {
                Editor.startEditorApplication(resourcesFile!!)

            } else {
                Game.play(resourcesFile!!, sceneName, fullScreen)
            }
        }
        if (verbose) {
            Game.printThreads()
        }
    }

    val USAGE = """USAGE
=====

tickle --help                     : Shows this message
tickle                            : Shows the Launcher Window
tickle --edit RESOURCE_FILE       : Starts the editor
tickle [OPTIONS] RESOURCE_FILE    : Runs the game

Game Options
------------
    --fullscreen        : Forces the game to start in fullscreen mode
    --windowed          : Forces the game to start in a window
    --scene SCENE_NAME  : Starts the game at the given scene
    --verbose           : Log messages appear on the console
    
"""

}
