/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.launcher

import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParameterException
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.util.child
import uk.co.nickthecoder.paratask.util.process.Exec
import uk.co.nickthecoder.paratask.util.process.OSCommand
import uk.co.nickthecoder.paratask.util.process.PrintErrSink
import uk.co.nickthecoder.paratask.util.process.PrintOutSink
import uk.co.nickthecoder.tickle.GameInfo
import uk.co.nickthecoder.tickle.info
import java.io.File

/**
 * Creates a new project (for a new game).
 * It creates the directory hierarchy including the files :
 *
 * - build.gradle
 * - README.md
 * - The resources file (file type .tickle)
 * - The Producer class (file type .kt). This also contains the game's 'main' entry point
 * - A scene (file type .scene), with no actors.
 * - .gitignore (if git option is chosen)
 *
 * It then optionally initialises git (including the first commit).
 * Finally the Tickle editor is launched.
 */
class NewGameWizard() : AbstractTask() {

    private val gameName = StringParameter("gameName", hint = "Only letters, numbers, spaces and underscores are allowed. e.g. Space Invaders", required = true)

    private val parentDirectory = FileParameter("parentDirectory", hint = "Do NOT include the name of the game (it will be added automatically)", mustExist = true, expectFile = false)

    private val initialSceneName = StringParameter("initialSceneName", required = true, value = "menu")

    private val width = IntParameter("width", value = 640)
    private val cross = LabelParameter("cross", "x")
    private val height = IntParameter("height", value = 480)

    private val windowSize = SimpleGroupParameter("windowSize")
            .addParameters(width, cross, height)
            .asHorizontal(LabelPosition.NONE)

    private val packageBase = StringParameter("packageBase", hint = "Blank, or your domain name backwards (e.g. com.example)", required = false)

    private val language = ChoiceParameter("language", value = "feather").apply {
        addChoice("feather", "feather")
        addChoice("kotlin", "kotlin")
        addChoice("java", "java")
    }

    private val git = BooleanParameter("initialiseGit", value = true, hint = "If you don't know what git is, google it! It's very useful.")

    override val taskD = TaskDescription("New Game Wizard")
            .addParameters(gameName, parentDirectory, windowSize, initialSceneName, packageBase, language, git)


    override fun check() {
        super.check()
        if (!gameName.value.matches(Regex("[a-zA-Z0-9 _]*"))) {
            throw ParameterException(gameName, "Only letters, numbers, spaces and underscores are allowed")
        }
        if (gameDirectory().exists()) {
            throw ParameterException(gameName, "Directory '${gameDirectory().name}' already exists")
        }
        if (!packageBase.value.matches(Regex("[a-zA-Z0-9.]*"))) {
            throw ParameterException(packageBase, "Only letters, numbers and periods allowed")
        }
    }

    fun identifier() = gameName.value.replace(" ", "")

    fun scripted() = language.value !in listOf("java", "kotlin")

    fun gameDirectory() = File(parentDirectory.value, identifier().lowercase())

    fun packageName(): String {
        return if (packageBase.value.isBlank())
            identifier().lowercase()
        else
            "${packageBase.value}.${identifier().lowercase()}"
    }

    fun mainPackageDir(): File {
        val kotlin = gameDirectory().child("src", "main", "kotlin")
        return File(kotlin, packageName().replace('.', File.separatorChar))
    }

    fun resourcesDir() = if (scripted()) gameDirectory() else gameDirectory().child("src", "dist", "resources")

    fun resourcesFile() = File(resourcesDir(), identifier() + ".tickle")

    override fun run() {
        val directory = gameDirectory()
        val packageDir = mainPackageDir()
        val resourcesDir = resourcesDir()
        val scripted = scripted()

        info("Creating directory structure at : $directory")

        directory.mkdir()
        packageDir.mkdirs()
        resourcesDir.mkdirs()
        resourcesDir.child("scenes").mkdir()
        resourcesDir.child("images").mkdir()
        resourcesDir.child("sounds").mkdir()
        if (scripted) {
            File(resourcesDir, "scripts").mkdir()
        }

        val resourcesFile = resourcesFile()
        info("Creating $resourcesFile")
        resourcesFile.writeText(resourceContents())

        val mainSceneFile = resourcesDir.child("scenes", "menu.scene")
        info("Creating $mainSceneFile")
        mainSceneFile.writeText(mainSceneContents())

        if (language.value == "kotlin") {
            val mainClassFile = File(packageDir, identifier() + ".kt")
            info("Creating $mainClassFile")
            mainClassFile.writeText(kotlinMainClassContents())
        }
        if (language.value == "java") {
            val mainClassFile = File(packageDir, identifier() + ".java")
            info("Creating $mainClassFile")
            mainClassFile.writeText(javaMainClassContents())
        }

        if (!scripted) {
            val gradleFile = File(directory, "build.gradle")
            info("Creating $gradleFile")
            gradleFile.writeText(gradleContents())
        }

        val readmeFile = File(directory, "README.md")
        info("Creating $readmeFile")
        readmeFile.writeText(readMeContents())

        if (git.value == true) {
            val gitIgnoreFile = File(directory, ".gitignore")
            info("Creating $gitIgnoreFile")
            gitIgnoreFile.writeText(gitIgnoreContents())
            info("Initialising git")
            exec(directory, "git", "init")
            info("Performing first git commit")
            exec(directory, "git", "add", ".")
            exec(directory, "git", "commit", "-m", "Project created using NewGameWizard")
        }

        info("Project Created.")

        if (!scripted) {
            if (exec(directory, "gradle", "installDist")) {
                info("Build OK")
            }
            info("")
            info("To build the game : ")
            info("cd '$directory'")
            info("gradle installDist")
            info("")
            info("To run the game : ")
            info("build/install/${identifier().lowercase()}/bin/${identifier().lowercase()}")
            info("")
            info("To launch the editor : ")
            info("build/install/${identifier().lowercase()}/bin/${identifier().lowercase()} --editor")
        }

    }

    fun exec(dir: File, program: String, vararg args: String): Boolean {
        println("Running $program ${args.joinToString(separator = " ")} (dir=$dir)")
        val cmd = OSCommand(program, * args)
        cmd.directory = dir
        val exec = Exec(cmd)
        exec.outSink = PrintOutSink()
        exec.errSink = PrintErrSink()
        val result = exec.start().waitFor(30) // Run, and abort if the command takes more than 30 seconds.
        if (result != 0) {
            info("Exit code : $result")
        }
        return result == 0
    }

    /*
    The following functions are templates for each of the files to be generated.
     */

    fun gradleContents() = """
buildscript {
    ext.kotlin_version = '1.1.3-2'

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:${'$'}kotlin_version"
    }
}

apply plugin: 'kotlin'
apply plugin: 'application'

mainClassName = "${packageName()}.${identifier()}Kt"

defaultTasks 'installDist'

version = '0.1'
group = '${packageBase.value}'

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    compile 'uk.co.nickthecoder:tickle-core:${GameInfo.tickleVersion}'
    compile 'uk.co.nickthecoder:tickle-editor:${GameInfo.tickleVersion}'
}

"""

    fun resourceContents() = """
{
  "info": {
    "title": "${gameName.value}",
    "width": ${width.value},
    "height": ${height.value},
    "initialScene": "${initialSceneName.value}",
    "testScene": "${initialSceneName.value}",
    "enableFeather": ${language.value == "feather"},
    "producer": "uk.co.nickthecoder.tickle.NoProducer"
  },
  "preferences": {
    "outputFormat": "PRETTY",
    "packages": [
      "uk.co.nickthecoder.tickle" ${if (scripted()) "" else """, "${packageName()}" """}
    ]
  },
  "layouts": [
    {
      "name": "default",
      "stages": [
        {
          "name": "main",
          "isDefault": true,
          "stage": "uk.co.nickthecoder.tickle.stage.GameStage",
          "constraint": "uk.co.nickthecoder.tickle.resources.NoStageConstraint"
        }
      ],
      "views": [
        {
          "name": "main",
          "view": "uk.co.nickthecoder.tickle.stage.ZOrderStageView",
          "stage": "main",
          "zOrder": 50,
          "hAlignment": "LEFT",
          "leftRightMargin": 0,
          "vAlignment": "TOP",
          "topBottomMargin": 0
        }
      ]
    }
  ]
}
"""

    fun kotlinMainClassContents() = """
package ${packageName()}

import uk.co.nickthecoder.tickle.AbstractProducer
import uk.co.nickthecoder.tickle.editor.EditorMain

/**
 * The main entry point for the game.
 */
fun main(args: Array<String>) {
    EditorMain("${identifier().lowercase()}", args).start()
}

"""

    fun javaMainClassContents() = """
package ${packageName()};

import uk.co.nickthecoder.tickle.AbstractProducer;
import uk.co.nickthecoder.tickle.editor.EditorMain;

public class ${identifier()} {
    /**
     * The main entry point for the game.
     */
    public static void main(String... args) {
        new EditorMain("${identifier().lowercase()}", args).start();
    }
}

"""

    fun mainSceneContents() = """
{
  "director": "uk.co.nickthecoder.tickle.NoDirector",
  "background": "#000000",
  "showMouse": true,
  "layout": "default",
  "stages": [
    {
      "name": "main",
      "actors": [
      ]
    }
  ]
}
"""

    fun gitIgnoreContents() = """
/.gradle
/.idea
/build
/out
/${identifier().lowercase()}.iml
/${identifier().lowercase()}.ipr
/${identifier().lowercase()}.iws
"""


    fun readMeContents() = "# ${gameName.value}" + (if (scripted()) """

To play the game, you must first install
[Tickle](https://gitlab.com/nickthecoder/tickle).

Start Tickle, then click "Play", and choose the file : "${identifier()}.tickle".
(If you've set up the proper file associations for ".tickle" files, you can double click it instead).

""" else """

To build the game :

    gradle installDist

To run the game :

    build/install/${identifier()}/bin/${identifier()}

To launch the editor :

    build/install/${identifier()}/bin/${identifier()} --editor

To create a zip file, ready for distribution :

    gradle distZip

""") + """

Powered by [Tickle](https://gitlab.com/nickthecoder/tickle) and [LWJGL](https://www.lwjgl.org/).
"""
}
