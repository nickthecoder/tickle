/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.launcher

import javafx.application.Application
import javafx.scene.image.Image
import javafx.stage.Stage
import uk.co.nickthecoder.paratask.util.AutoExit
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.editor.application
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.info
import uk.co.nickthecoder.tickle.severe

class LauncherApp : Application() {

    init {
        application = this
    }

    override fun start(primaryStage: Stage) {
        instance = this
        AutoExit.disable()

        primaryStage.icons.add(Image(Game::class.java.getResource("tickle.png")!!.toExternalForm()))

        info("Creating LauncherWindow")
        LauncherWindow(primaryStage)
        info("Created LauncherWindow")
    }

    override fun stop() {
        // Terminate GLFW and free the error callback
        onOpenGLThread {
            try {
                info("Stopping JavaFX Application, deleting GL context")
                //glWindow.delete()
                OpenGL.end()
            } catch (e: Exception) {
                severe("Failed while cleaning up OpenGL")
                e.printStackTrace()
            }

        }
        super.stop() // Does nothing, but I think it is good practice anyway.
        info("Launched JavaFX stopped")
    }

    companion object {

        lateinit var instance : LauncherApp

        fun start() {
            OpenGL.begin()
            launch(LauncherApp::class.java)
        }
    }
}
