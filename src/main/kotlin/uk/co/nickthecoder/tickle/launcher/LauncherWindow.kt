/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.launcher

import javafx.application.Platform
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.FileChooser
import javafx.stage.Stage
import uk.co.nickthecoder.fxessentials.openFile
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.info
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.misc.ResourcesReader
import uk.co.nickthecoder.tickle.severe
import java.io.File
import java.util.*
import java.util.prefs.Preferences

class LauncherWindow(

    /**
     * The primary Stage of the LauncherApp
     */
    private val stage: Stage,

    ) {

    val recentContent = FlowPane()

    init {
        info("Initialising LauncherWindow")

        val resource = LauncherApp::class.java.getResource("launcher.css")

        with(recentContent) {
            prefWidth(800.0)
        }
        val recentScroll = ScrollPane(recentContent).apply {
            isFitToWidth = true
            isFitToHeight = true
            styleClass.add("scroll")
        }

        val playButton = createBigButton("Play Game", "play") { onPlay() }
        val editButton = createBigButton("Edit Game", "edit") { onEdit() }
        val newButton = createBigButton("New Game Wizard", "new") { onNew() }
        val newPlayEdit = HBox().apply {
            children.addAll(newButton, playButton, editButton)
            styleClass.add("newPlayEdit")
            alignment = Pos.CENTER
        }

        val borderPane = BorderPane().apply {
            bottom = newPlayEdit
            center = recentScroll
            styleClass.add("borderPane")
        }

        val scene = Scene(borderPane).apply {
            stylesheets.add(resource.toExternalForm())
        }

        stage.scene = scene
        stage.title = "Tickle"
        rebuildRecentContent()
        stage.sizeToScene()

        stage.show()
    }

    private fun createBigButton(label: String, style: String, action: () -> Unit): Button {
        val iv = imageView("$style.png")
        val button = Button(label, iv)
        with(button) {
            wrapTextProperty().value = true
            onAction = EventHandler { action() }
        }
        return button
    }

    private fun forget(resourcesFile : File) {
        removeRecent(resourcesFile)
        rebuildRecentContent()
    }

    private fun onPlay(resourcesFile: File) {
        addRecent(resourcesFile)
        rebuildRecentContent()
        stage.hide()

        // runLater allows the stage.hide to actually hide the window. Without this, the window remains visible.
        Platform.runLater {
            // The game must run on the OpenGL Thread, not the JavaFX thread.
            OpenGL.runLater {
                try {
                    val window =
                        with(ResourcesReader.loadGameInfo(resourcesFile)) {
                            Window(title, width, height, resizable, fullScreen = fullScreen)
                        }
                    val resources = Resources()
                    resources.load(resourcesFile)
                    val gameInfo = resources.gameInfo

                    window.show()

                    val game = Game(window, resources)

                    try {
                        game.run(
                            resources.sceneFileToPath(gameInfo.initialScenePath)
                        )
                    } finally {
                        // NOTE, the LauncherApp will delete the window soon
                        // (as part of the Application's stop() method)
                        window.hide()
                        window.delete()
                    }

                    info("Game finished")

                    game.pollEvents = false
                    // Clean up the old Game instance, and create a new one.

                    Platform.runLater {
                        // As this is the only JavaFX window open, this will cause the JavaFX application
                        // to stop.
                        stage.close()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun onPlay() {
        val resourcesFile = chooseResourcesFile("Play Game")
        if (resourcesFile != null) {
            onPlay(resourcesFile)
        }
    }

    private fun onEdit(resourcesFile: File) {
        addRecent(resourcesFile)

        // Main window will handle errors when it is created
        // ErrorHandler.errorHandler = DelayedErrorHandler()

        stage.hide()

        val window = onOpenGLThread {
            Window( "Tickle Editor", 100, 100, true)
        }

        // runLater allows the stage.hide to actually hide the window. Without this, the window remains visible.
        Platform.runLater {
            Editor.startEditor(window, resourcesFile)
        }

    }

    private fun onEdit() {
        val resourcesFile = chooseResourcesFile("Edit Game")
        if (resourcesFile != null) {
            onEdit(resourcesFile)
        }
    }

    private fun onNew() {
        val task = NewGameWizard()
        task.taskRunner.listen { cancelled ->
            if (!cancelled) {
                Platform.runLater {
                    onEdit(task.resourcesFile())
                }
            }
        }
        TaskPrompter(task).placeOnStage(Stage())
    }

    private fun rebuildRecentContent() {

        recentContent.children.clear()

        listRecent().forEach { resourcesFile ->

            val thumbnailFile = File(resourcesFile.parentFile, "thumbnail.png")

            val imageView = if (thumbnailFile.exists()) {
                try {
                    val image = Image(thumbnailFile.toURI().toURL().toString())
                    val iv = ImageView(image)
                    iv.fitWidth = 160.0
                    iv.fitHeight = 120.0
                    iv.isPreserveRatio = true
                    iv
                } catch (e: Exception) {
                    severe("Failed to load $thumbnailFile")
                    e.printStackTrace()
                    null
                }
            } else {
                null
            }
            val playButton = Button("", imageView).apply {
                onAction = EventHandler { onPlay(resourcesFile) }
                tooltip = Tooltip(resourcesFile.path)
                prefWidth = 180.0
                prefHeight = 140.0
                styleClass.add("playButton")
                contextMenu = createGameContextMenu(resourcesFile)
            }

            val label = Label(fromCamelCase(resourcesFile.nameWithoutExtension)).apply {
                styleClass.add("gameName")
            }

            val editButton = Button("", imageView("edit.png")).apply {
                onAction = EventHandler { onEdit(resourcesFile) }
                tooltip = Tooltip("Edit Game")
                styleClass.add("editButton")
            }

            val bottomRow = HBox(label, editButton).apply {
                alignment = Pos.CENTER
                styleClass.add("labelEditRow")
            }

            val gameBox = VBox().apply {
                styleClass.add("gameBox")
                alignment = Pos.CENTER
                children.addAll(playButton, bottomRow)
            }

            recentContent.children.add(gameBox)
        }
    }

    private fun createGameContextMenu(resourcesFile: File): ContextMenu {
        val folder = resourcesFile.parentFile

        val menu = ContextMenu().apply { }

        menu.items.add(MenuItem("Open Folder").apply {
            onAction = EventHandler {
                LauncherApp.instance.hostServices.showDocument(folder.toURI().toString())
            }
        })

        var readme = File(folder, "README.md")
        if (!readme.exists()) readme = File(folder, "README.txt")
        if (!readme.exists()) readme = File(folder, "README")
        if (readme.exists()) {
            menu.items.add(MenuItem("Open ${readme.name}").apply {
                onAction = EventHandler {
                    LauncherApp.instance.openFile(readme)
                }
            })
        }

        var licence = File(folder, "LICENSE")
        if (!licence.exists()) licence = File(folder, "LICENSE.txt")
        if (licence.exists()) {
            menu.items.add(MenuItem("Open ${licence.name}").apply {
                onAction = EventHandler {
                    LauncherApp.instance.openFile(licence)
                }
            })
        }

        menu.items.add( SeparatorMenuItem() )

        menu.items.add( Menu( "Remove from Launcher" ).apply {
            onAction = EventHandler {
                forget(resourcesFile)
            }
        })

        return menu
    }

    /**
     * Converts a camel case string into a nicer looking alternatives, by adding spaces when we go
     * from lower case to upper case. The first character is always made upper case too.
     */
    private fun fromCamelCase(str: String): String {
        val buffer = StringBuilder()
        var prevWasLowerCase = false
        var first = true
        for (c in str) {
            if (c.isUpperCase() && prevWasLowerCase) {
                buffer.append(" ")
            }
            if (first) {
                buffer.append(c.toUpperCase())
                first = false
            } else {
                buffer.append(c)
            }
            prevWasLowerCase = c.isLowerCase()
        }
        return buffer.toString()
    }

    private fun listRecent(): List<File> {
        val files = mutableMapOf<File, Long>()
        val rp = recentPreferences()

        rp.keys().forEach { name ->
            val date = rp.getLong(name, 0)
            val file = File(name)
            if (file.exists()) {
                files[file] = date
            } else {
                rp.remove(name)
            }
        }

        rp.flush()
        return files.toList().sortedBy { -it.second }.map { it.first }
    }

    private fun chooseResourcesFile(title: String): File? {
        val fileChooser = FileChooser()
        fileChooser.title = title
        fileChooser.extensionFilters.add(FileChooser.ExtensionFilter("Tickle Resource File", "*.tickle"))
        return fileChooser.showOpenDialog(stage)
    }

    private fun imageView(name: String): ImageView {
        val imageStream = LauncherApp::class.java.getResourceAsStream(name)
        return ImageView(if (imageStream == null) null else Image(imageStream))
    }

    companion object {

        private fun preferences(): Preferences {
            return Preferences.userRoot().node("uk/co/nickthecoder/tickle/launcher")
        }

        private fun recentPreferences(): Preferences {
            return preferences().node("recent")
        }

        fun addRecent(file: File) {
            val rp = recentPreferences()
            rp.putLong(file.canonicalPath, Date().time)
            rp.flush()
        }

        fun removeRecent(file : File) {
            val rp = recentPreferences()
            rp.remove(file.canonicalPath)
        }
    }

}
