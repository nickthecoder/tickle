#!/bin/bash

#
# Registers the mime type for *.tickle files.
# For Linux only.
# Adds desktop entries for the tickle game engine and the tickle editor.
#
# Requires Debian packages: desktop-file-utils xdg-utils
#
# Script based on :
# https://stackoverflow.com/questions/30931/register-file-extensions-mime-types-in-linux
#
# Desktop Entry spec :
# https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html

set -e # stop on error

APP=tickle
EXT=tickle
EXEC=`pwd`/bin/tickle
ICON_32=`pwd`/tickle32.png
ICON_64=`pwd`/tickle64.png
DATA_DIR=$HOME/.local/share/


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<mime-info xmlns=\"http://www.freedesktop.org/standards/shared-mime-info\">
    <mime-type type=\"application/x-$APP\">
        <comment>Tickle Game Resources</comment>
        <icon name=\"application-x-$APP\"/>
        <glob pattern=\"*.$EXT\"/>
    </mime-type>
</mime-info>
" > "/tmp/$APP-mime.xml"

    echo "[Desktop Entry]
Name=Tickle
Exec=$EXEC %f
MimeType=application/x-$APP
Icon=application-x-$APP
Terminal=false
Type=Application
Comment=Play a Tickle game, or start the Tickle launcher
Categories=Development;Game;ArcadeGame;Java;
"> "/tmp/$APP.desktop"


    echo "[Desktop Entry]
Name=Tickle Editor
Exec=$EXEC --edit %f
MimeType=application/x-$APP
Icon=application-x-$APP
Terminal=false
Type=Application
Comment=Edit a Tickle game, or start the Tickle launcher
"> "/tmp/$APP-edit.desktop"

if [ "--uninstall" = "$1" ]
then
    echo "Unregistering Tickle"

    # Uninstall
    xdg-mime uninstall /tmp/$APP-mime.xml
    xdg-icon-resource uninstall --context mimetypes --size 32 $ICON_32 application-x-$APP
    xdg-icon-resource uninstall --context mimetypes --size 64 $ICON_64 application-x-$APP

    # There doesn't appear to be an "uninstall" option on command desktop-file-install (or an uninstall command).
    # So I just delete the files using rm
    rm -f "$DATA_DIR/applications/$APP.desktop"
    rm -f "$DATA_DIR/applications/$APP-edit.desktop"

else
    echo "Registering Tickle"

    if [ ! -f "$EXEC" ]; then
        echo "Tickle executable : $EXEC not found."
        echo "You must run this from tickle's install directory"
        exit
    fi

    xdg-icon-resource install --context mimetypes --size 32 $ICON_32 application-x-$APP
    xdg-icon-resource install --context mimetypes --size 64 $ICON_64 application-x-$APP
    # Grr, xdg-icon-resource doesn't support svg files. Why?

    desktop-file-install --dir=$DATA_DIR/applications "/tmp/$APP.desktop"
    desktop-file-install --dir=$DATA_DIR/applications "/tmp/$APP-edit.desktop"

    xdg-mime install "/tmp/$APP-mime.xml"

    # The default action is to RUN the game.
    xdg-mime default "$APP.desktop" application/x-$APP
    # If you prefer the default action to EDIT the game, then replace with :
    # xdg-mime default $APP-edit.desktop application/x-$APP

fi

update-mime-database "$DATA_DIR/mime"
update-desktop-database "$DATA_DIR/applications"

# Tidy up temp files. -f to be quiet if the file doesn't exist.
rm -f "/tmp/$APP-mime.xml"
rm -f "/tmp/$APP.desktop"
rm -f "/tmp/$APP-edit.desktop"

echo "Done"
