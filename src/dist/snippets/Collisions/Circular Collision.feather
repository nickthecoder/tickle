/**
    A very simple collision routine. This will look natural if the items colliding are circular (or close the circular).
    It assumes that the "offset" of the poses are in the middle, and that the pose is round(ish).

    For more complex collisions, consider using the Physics Engine instead (which is much harder to use,
    but is much more flexible).

    Note, you can sub-class this in you own game e.g. CircularShip and CircularEnemy
*/
class Circular : AbstractRole {

    // The radius is set in begin() based on the size of the Actor's Pose.
    var radius = 1.0

    // By making this a CostumeAttribute, you can change the mass in the "Costume" tab within the Editor.
    // You can one of more Costumes, all with the same role (MyRole), but each one can have a different mass.
    // e.g. collisions between a football (soccer ball) and a bowling ball.
    @CostumeAttribute
    var mass = 1.0

    var velocity = Vector2()

    override fun begin() {
        super.begin()
        radius = actor.appearance.width() * 0.5 // Consider making this smaller, so that there is never a gap.
    }

    override fun tick() {
        // Move this actor, based on our velocity.
        actor.position += velocity

        // Loop over all Roles, of the same type (i.e. Circular)
        for (other in actor.stage.findRolesByType<Circular>( Circular ) ) {
            // Don't collide with myself!
            if (other != this) {

                // Do our imaginary circles overlap?
                // We could instead use pixel based collision detection. Search for "Pixel Collision"
                if ( CircularCollision.overlapping( radius + other.radius, actor.position, other.actor.position ) ) {

                    // Perform the collision.
                    val hit = CircularCollision.collision(
                        actor.position, velocity, mass,
                        other.actor.position, other.velocity, other.mass
                    )
                    // "hit" gives an indication of the strength of the collision.
                    // If hit < 0, then it means the two actors are heading AWAY from each other!
                    if ( hit > 0 ) {
                        // Assuming there is a "sound" event called "hit" in this actor's costume
                        // then this will make a sound...
                        actor.event( "hit" )
                    }
                }
            }
        }        

    }

}
