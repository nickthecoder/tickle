Tickle
======
A 2D game engine written in Kotlin, using LWJGL for high performance OpenGL graphics for Linux, windows and MacOS

This is my 3rd game engine, and I'm very pleased with the result.

![screenshot](screenshot.jpg)

The Aim
-------

- Fun : The primary aim is to make writing games as enjoyable as possible.
- Quick : No need for boiler plate code, such as writing code to load resources.
  No code to draw anything on the screen either!
  Every time I can remove a line of game script code, that is a win!
  e.g. None of my games have *any* import statements. Ask yourself why all java code needs such "cruft".
  IMHO, the answer is because nobody has tried to get rid of it (but if they did, almost all import statements would be redundant).
- Easy : No knowledge of OpenGL required, and in fact most none of my games have ANY drawing routines.
- Flexible : There should be very few limits on the type of 2D game.
  This goal isn't fully realised. For example, Tickle doesn't have any concept of "fog of war".
  So if you wanted that feature, then it would break items 1 & 2 (Quick and Easy).
- High Performance : Ability to create good games that run well on crusty out-of-date hardware.

Target Audience
---------------

The current target audience for Tickle are people who are already familiar with programming.

It will NEVER be suitable for triple-A games,
not least because triple-A games tend to use 3D models (which Tickle will never support).

Compiling From Source
---------------------

From the tickle directory, run :

    ./gradlew

Fingers crossed, it should download all dependencies, and compile the game engine.

Alternatively, you can download and unzip the pre-compiled application from :

    http://nickthecoder.co.uk/public/software

If there is more than one tickle zip file, pick the latest one!

Running Tickle
--------------

If you've compiled from source, then :

    cd build/install/tickle

If you've downloaded a pre-compiled version, then head to the tickle directory, and run :

    bin/tickle

On Linux, there is a script, which registers the Tickle application, and the mime type for .tickle files :

    ./register

This script requires packages desktop-file-utils and xdg-utils.
These are the Debian package names; they may be named different on non-debian based distros???


If you use a different OS, please send me a similar script for your operating system ;-)

To play a game, double click any .tickle file from your file manager (such as dolphin, nautilus etc).
Right click should also show an option to edit the tickle game.

If you wish to undo the changes made by ./register, then run :

    ./register --uninstall

Example Code
------------

The game: Move the golf ball around with your mouse. Avoid hitting any of the other objects,
which are bouncing around the screen.
The objects get bigger, and faster - the game will end quite quickly!

Here's a link to the source code for the demo game :
[Bounce.feather](https://gitlab.com/nickthecoder/tickle/-/raw/master/HelloWorld/scripts/Bounce.feather)
[Player.feather](https://gitlab.com/nickthecoder/tickle/-/raw/master/HelloWorld/scripts/Player.feather)

BTW, A traditional "Hello World" program wouldn't have any code at all.
You would just place the text where you wanted it from the Scene Editor.
(using whichever font/size/colour of your choosing).

Resources Editor
----------------

Tickle includes a GUI to manage resources, such as graphics and sounds etc.
Loading and saving resources is done automatically.

It also let's you edit custom properties unique to your game.
For example a shoot-em-up game may have an object called "Ship", but this ship can have different "Costumes".
Not only will each costume look different, but it can have different properties,
such as the speed of the ship, its maximum health etc.


Scene Editor
------------

Tickle has a built-in scene editor, allowing game levels to be created quickly.
Saving and loading scenes is done for you.

As with the Resources Editor, objects placed in a scene can have custom properties.
For example, keys and doors can share a property, so that picking up a key
opens a particular door (or set of doors).


Supported Languages
-------------------

Tickle comes with its own scripting language called "Feather".
It is similar to Java, but IMHO it is much nicer than Java.
Many of Feather's features are based on Java's big brother : Kotlin
If you've never heard of Kotlin, check it out, it is excellent.

You can also use Tickle with a compiled JVM language, such as Java or Kotlin,
but I don't recommend it.

Structure
---------

Tickle is split into modules...

**tickle-core** contains everything needed during actual game-play

**tickle-editor** contains additional classes needed for the editor (to edit game resources, and game levels (aka Scenes).

**tickle-feather** Adds support for the feather to be used as a scripting language for game development

**tickle-demo** A simple Hello World program to test that tickle is working correctly.

Why is it called Tickle?
------------------------

I was very impressed by [Scratch](https://en.wikipedia.org/wiki/Scratch_(programming_language)),
the game engine designed by MIT for children to learn how to program.

However, Scratch was very limited, and even worse, it encourages very bad programming practices.
So I created "Itch", written in Python, and I soon realised that I hated Python.

So I started with a blank slate, and created "Itchy", written in Java.
Itchy was okay, but after a few years, I wasn't happy with it.

So, I started with a blank slate again, and created Tickle
Tickle is written in Kotlin. Gosh Kotlin is good, I cannot praise it enough. Java sucks by comparison.


Current Status
--------------

All the key features have been written, but there is still a huge number of features that I'd like to add.

Documentation is sparse / non-existent. Sorry.
I code for my own enjoyment, and never bother to get others to use my projects,
so good documentation isn't high on my priority list.

For the type of games I like to write, I've had no problems with performance, even on my clunky old laptop
(a Lenovo T400, which is well over 10 years old, and has a really bad integrated graphics!)
Though, I may be nearing the limit of this old laptop!
There are fundamental sub-optimal design choices that I've made.
For example, the renderer sorts all Actors within a Stage by z-order every frame.
This becomes a bottle-neck when there are thousands of game objects,
but I've yet to write a real game where this is of actual concern.

My most taxing game is bottle-necked by the drawing of rotated textures.
Note that I'm testing this on an old laptop without a "proper" graphics card, so this is done in software on the CPU!
Most of these rotated images are "static" scenery,
so a "simple" solution would be to perform the rotation ONCE, and cache the results.
(Increase memory usage, and decrease CPU load).
With a GPU, rotations are not slow, and this game isn't anywhere near the limit.

I'm very happy with the Feather language, which I designed specifically for writing games.
However, there are still quirks, and bugs.

The Editor is a janky, but there no show-stoppers.
Having said that, I did run into an issue recently, which lost some data in a .tickle file, which would have been
a nightmare, if I couldn't revert the changes using git!
I want Tickle to be easy and safe for those who don't use, or understand version control software,
so this was a big, unacceptable failure!

It has been developed solely on Linux, and worked first time when I tested it on Windows,
which gives me some confidence that it may also work first time on MacOS.
Feel free to send me an old Mac if you have one gathering dust ;-)
I only own linux boxes, but have friends and family that have Windows, but no MacOS.

I'm still occasionally making backwards *incompatible* API changes to Tickle.
These are rare, and quite trivial, but even the smallest of backwards incompatible API changes means that games
will fail to run without the corresponding (simple) game-code changes.

Planned Additional Features
===========================

Have a look in the todo.txt file for a bucket load of ideas.

Transition from Scratch-like code to fully scripted code
--------------------------------------------------------

Scratch is an interesting language, because from the outside (as a pro programmer), it *looks* like it is highly parallel.
Each game object has one or more blocks of "code" which are independent, apart from "messages" sent from one to another.

There is no "game loop", or drawing routine.
If I were to limit all Tickle Roles to be "Action"s, then it would be possible to transition between Scratch-like structures
to "proper" Kotlin/Java/Feather code. But the code would not contain traditional "while", "if" statements, but instead,
each code block would be an Action, built up of smaller actions, and one of the actions would be something like :

    IfAction( condition, ifAction, elseAction )

In the IfAction's act() method, it would test the condition, and then call act() on either the ifAction or the elseAction.
This is far from ideal, because it doesn't help youngsters move from Scratch-like pseudo parallel code to "proper"
code, with if *statements*.
On the plus side, they could take a game written in Scratch-like code, transform it into Actions, and then gradually replace
the likes of IfAction, with real "if" statements.

Notable Limitations
===================

OS Support
----------
Only supports Linux, Windows and MacOS. No support for mobile devices nor browser based games.

No auto-complete
----------------
No auto-complete for the Feather language. This is easily the most pressing ommision,
but it would take more time than I'm willing to put in.
I'm an old-fogie, and grew up when home computers first arrived on the scene, so lacking
auto-complete isn't a big deal for me.
I do notice the lack of features common to IDEs, but I still like the simplicity of using Feather, which is typed
(and therefore detects most errors before runt-time), and despite it being a compiled language, it *feels* like it is
interpretted, because compilation takes almost no time at all for the types of games that Tickle is designed for.
i.e. thousands of lines of code, rather than tens of thousands of lines of code or more.

Crappy Editor
-------------
The scene editor is buggy, slow and far from perfect.
On the plus side, it *is* usable, and much, much better than trying to create a custom scene editor for every game.

Destructive Changes
-------------------
No support for destructive changes to game sprites
(e.g. for games such as Lemmings, where you blow up parts of the background).

Views
-----

No out-of-the-box support for isometric (or other 2.5D games).

Rendering is simplistic. There's no "fog-of-war", nor other more specialised rendering features
(such as outlining objects hidden behind buildings as seen in Age of Empires II).
While it is *possible* for you to write these features yourself, it will be HARD!
(which goes against the "quick and easy" goals of Tickle).

Path Finding
------------

No path-finding. You could write your own, but this is such a common feature for certain game genres,
that Tickle should do much of the hard work for you.

No Multi-threading
------------------
Tickle is NOT thread safe, and I don't intend to change this!
This may sound crazy in the world of multi-core CPUs which aren't getting faster.
However, debugging a multi-threaded game isn't fun, quick, nor easy, so it goes against three of my goals.

I may add limit support for threading, e.g. perform path finding in a separate thread.
My plan would allow the finding the optimal path, but the "main" thread would still be responsible for updating Actors to follow the path.
The 2nd thread would not update *any* actor/role properties.
When the path finder finishes, the path may be "wrong". e.g. if a building is placed along the calculated route after the route planning started,
then the route *may* assume that there is still a viable path "through" the building.
It would be up to the game writer to decide when to re-evaluate the path.
Maybe when the Actor tries to walk through the building,
or to re-test the path occasionally on-route, before meeting the new obstruction.
It would be trivally easy to test routes every n seconds, and start the path finding again when the "old" path is blocked.
I believe that re-testing, and re-evaluating paths in a separate thread would have near-zero overhead on modern harware.

No networking support
---------------------

So muti-player games are currently far from easy!
I have ideas on how to achieve this, while still making games quick and easy to write,
and have prototyped these ideas in the past (in a different game engine - a precursor to Tickle, called Itchy ;-).

I envision a "server" which runs all of the game code (holding the Actor and Role data etc).
The clients would be very light-weight (with little, or no game-specific code running on the client).
The server sends changes to Actor's positions and appearances.
Keyword and mouse events would be passed as messages from the client to the server.
There would need to be extra data on MouseEvent and KeyEvent (with a client id of some kind), so that the game code on the server knows
which player pressed which button.
Each client would have local copies of all resources (such as Textures, Poses, Sounds etc),
therefore the data sent between the server and client during the game would be quite small.

There are problems related to "fairness". It may be non-trivial to prevent low ping clients having an unfair advantage.
On the flip side, if it was possible to write LAN games quickly and easily (i.e. by noob coders), IMHO, that would be bloody great!
As an example, I have a 1 player game called Doll's House, and it would be great if it could be transformed into a 2 player LAN game with minimal
game-code changes.
The network load would be very minimal as there are not many Actors which change each frame.
I believe this is easily obtainable.
