/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.feather

import uk.co.nickthecoder.feather.*
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.scripts.Language
import uk.co.nickthecoder.tickle.scripts.ScriptException
import uk.co.nickthecoder.tickle.stage.Stage
import java.io.File
import java.nio.charset.Charset
import java.util.*

private fun extraScript(resourceName: String, charset: Charset = Charsets.UTF_8): StringScript {
    return try {
        val name = "$resourceName.feather"
        val stream = FeatherLanguage::class.java.getResourceAsStream(name)
        StringScript(name, stream.reader(charset).readText())
    } catch (e: Exception) {
        severe("Failed to load '$resourceName' : $e")
        StringScript(resourceName, "")
    }
}

class FeatherLanguage : Language() {

    private val compiler = createFeatherCompiler()

    override val fileExtension = "feather"

    override val name = "Feather"

    private lateinit var path: File

    private var classLoader = createClassLoader()

    private val byteCodeByName = mutableMapOf<String, ByteArray>()

    /**
     * A Feather script, which contains helper functions and classes, which give
     */
    private val featherExtras = extraScript("FeatherExtras")

    override fun setClasspath(directory: File) {
        path = directory
        createFeatherCompiler()
    }

    private fun createFeatherCompiler(): FeatherCompiler {
        info("Creating Feather Compiler")
        return FeatherCompiler().apply {

            val includePackages = listOf(
                "uk.co.nickthecoder.tickle",
                "uk.co.nickthecoder.tickle.action",
                "uk.co.nickthecoder.tickle.action.movement",
                "uk.co.nickthecoder.tickle.action.movement.polar",
                "uk.co.nickthecoder.tickle.action.animation",
                "uk.co.nickthecoder.tickle.collision",
                "uk.co.nickthecoder.tickle.control",
                "uk.co.nickthecoder.tickle.events",
                "uk.co.nickthecoder.tickle.graphics",
                "uk.co.nickthecoder.tickle.loop",
                "uk.co.nickthecoder.tickle.neighbourhood",
                "uk.co.nickthecoder.tickle.path",
                "uk.co.nickthecoder.tickle.physics",
                "uk.co.nickthecoder.tickle.resources",
                "uk.co.nickthecoder.tickle.sound",
                "uk.co.nickthecoder.tickle.stage",
                "uk.co.nickthecoder.tickle.util",
                "java.util",
                "org.jbox2d.dynamics.contacts"
            )

            val includeClasses = listOf<String>(
                // I used to have Random and Date in here, but now I'm including the whole of java.util.
            )

            with(configuration) {
                allowIncludes = false
                includeFeatherRuntime()
                (sandBox as AllowList).apply {
                    allowedPackages.addAll(includePackages)
                    allowedClasses.addAll(includeClasses)
                }
                impliedImportPackages.addAll(includePackages)
                impliedImportClasses["Random"] = Random::class.java
                impliedImportClasses["Date"] = Date::class.java

                impliedImportStaticClasses.addAll(
                    listOf(
                        "uk.co.nickthecoder.tickle.feather.FeatherExtras",
                        "uk.co.nickthecoder.tickle.Extras",
                        "uk.co.nickthecoder.tickle.action.Eases",
                        "uk.co.nickthecoder.tickle.action.movement.EllipticalArcTo", // For Circles
                        "uk.co.nickthecoder.tickle.action.movement.EllipticalArcBy",
                        "uk.co.nickthecoder.tickle.action.movement.Elliptical"
                    )
                )
            }
        }
    }

    private fun createClassLoader(): ClassLoader {
        return object : ClassLoader(javaClass.classLoader) {
            override fun findClass(name: String?): Class<*>? {
                val bytes = byteCodeByName[name] ?: return null
                return defineClass(name, bytes, 0, bytes.size)
            }
        }
    }

    override fun reload() {
        val startTime = Date().time
        info("Reloading Feather scripts")
        classLoader = createClassLoader()
        byteCodeByName.clear()

        path.listFiles()?.filter { it.extension == fileExtension }?.let { scriptFiles ->
            val scripts = mutableListOf<Script>()
            scripts.addAll(scriptFiles.map { FileScript(it) })
            // Add extra utilities written in Feather
            scripts.add(0, featherExtras)

            try {
                val map = compiler.compile(scripts)
                byteCodeByName.putAll(map)
                map.keys.forEach { className ->
                    classesByName[className] = classLoader.loadClass(className)
                }
            } catch (e: CompilationFailed) {
                val first = e.errors.first()
                severe(
                    ScriptException(
                        first.message ?: e.message ?: "",
                        e,
                        first.pos.source?.let { File(it) },
                        first.pos.line,
                        first.pos.column
                    )
                )
            } catch (e: Exception) {
                severe(e)
            }
        }

        val duration = Date().time - startTime
        info("Finished compiling Feather scripts in ${duration / 1000}s")
    }

    private fun toScriptException(e: Exception): ScriptException {
        return if (e is ScriptException) {
            e
        } else if (e is CompilationFailed) {
            return toScriptException(e.errors.first())
        } else if (e is FeatherException) {
            ScriptException(e.message ?: "", e, File(e.pos.source), e.pos.line, e.pos.column)
        } else {
            ScriptException(e)
        }
    }

    override fun addScript(file: File) {
        info("FeatherLanguage.addScript $file : Using reload!")
        reload()
        /*
        try {
            val map = compiler.compile(file)
            byteCodeByName.putAll(map)
            map.keys.forEach { className ->
                classesByName[className] = classLoader.loadClass(className)
            }
        } catch (e: Exception) {
            throw toScriptException(e)
        }
        */
    }

    override fun scriptFile(className: String): File {
        return File(path, className.replace('.', File.separatorChar) + "." + fileExtension)
    }


    override fun generateScript(name: String, type: Class<*>?): String {
        return when (type) {
            Role::class.java -> generateRole(name)
            Director::class.java -> generateDirector(name)
            Producer::class.java -> generateProducer(name)
            Stage::class.java -> generateStage(name)
            else -> generatePlainScript(name, type)
        }
    }

    private fun generatePlainScript(name: String, type: Class<*>?) = """
${if (type == null || type.`package`.name == "uk.co.nickthecoder.tickle") "" else "import ${type.name}"}
class $name ${if (type == null) "" else " : ${type.simpleName}"}{

}
"""

    private fun generateRole(name: String) = """
class $name : AbstractRole {

    override fun begin() {
    }

    override fun tick() {
    }

}
"""

    private fun generateDirector(name: String) = """
class $name : AbstractDirector {

}
"""

    private fun generateProducer(name: String) = """
class $name : AbstractProducer {

    override fun layout() {
        // Changes the size/scale of the views based on GameInfo.width, GameInfo.height,
        // and the details of each view in the "Layout"
        Game.instance.scene.arrangeViews()
    }
    
}
"""

    private fun generateStage(name: String) = """
class $name : GameStage {

}
"""

    companion object {

        fun compileMacroScript(script: Script): Pair<ClassLoader, Set<String>> {
            with(FeatherLanguage()) {

                // Turn OFF the sandbox - all classes, include File and other "dangerous" classes are allowed.
                compiler.configuration.sandBox = object : SandBox {
                    override fun isAllowed(klass: Class<*>) = true
                    override fun clone() = this
                }

                val includePackages = listOf(
                    "uk.co.nickthecoder.tickle.editor",
                    "uk.co.nickthecoder.tickle.editor.code",
                    "uk.co.nickthecoder.tickle.editor.dockable",
                    "uk.co.nickthecoder.tickle.editor.parameters",
                    "uk.co.nickthecoder.tickle.editor.resources",
                    "uk.co.nickthecoder.tickle.editor.scene",
                    "uk.co.nickthecoder.tickle.editor.tabs",
                    "uk.co.nickthecoder.tickle.editor.tasks",
                    "uk.co.nickthecoder.tickle.editor.util",
                    "java.util",
                    "java.io"
                )
                compiler.configuration.impliedImportPackages.addAll(includePackages)

                classLoader = createClassLoader()
                byteCodeByName.clear()

                val map = compiler.compile(script)
                byteCodeByName.putAll(map)
                map.keys.forEach { className ->
                    classesByName[className] = classLoader.loadClass(className)
                }

                return Pair(classLoader, byteCodeByName.keys)

            }

        }

    }
}