package uk.co.nickthecoder.tickle.feather

/**
 * Feather doesn't (currently) support lambdas, so it is tricky to mix predefined Actions with ad-hoc code.
 * These extensions allows us to call arbitrary functions as part of a complex Action definition.
 *
 *      override fun createAction() : Action {
 *          return Delay( 1 ) then this:>doSomething thenOnce this:>foo and this:>somethingElse andOnce this:>bar
 *      }
 *      fun doSomething() : bool { ... }
 *      fun somethingElse() : bool { ... }
 *      fun foo() { ... }
 *      fun bar() { ... }
 *
 * Also includes `runLater` (which is not related to Actions).
 */
class FeatherExtras {

    /**
     * Creates an [Action] from a [Function] with no arguments which returns a boolean.
     * The [Action] will end when the return value of the function is `true`.
     */
    static fun action( func : ()->bool ) : Action = FunctionAction( func )

    /**
     * Creates an [Action] from a [Function] with no arguments.
     * The [Action] will end after act() is called once.
     */
    static fun once( func : () ) : Action = OnceFunctionAction( func )

    /**
     * Creates an [Action] from a [Function] with no arguments.
     * The [Action] will end after begin() is called. act() will never be called..
     */
    static fun immediately( func : () ) : Action = ImmediateFunctionAction( func )

    static fun forever( func : () ) : Action = OnceFunctionAction( func ).forever()

    static fun then( action : Action, func : ()->bool ) = action then FunctionAction(func)
    static fun and( action : Action, func : ()->bool ) = action then FunctionAction(func)

    static fun thenOnce( action : Action, func : () ) = action then OnceFunctionAction(func)
    static fun thenImmediately( action : Action, func : () ) = action then ImmediateFunctionAction(func)
    static fun andOnce( action : Action, func : () ) = action and OnceFunctionAction(func)

    // e.g. foo:myFunc onceThen Delay( 1 )
    // myFunc returns void
    static fun onceThen( func : (), action : Action ) = OnceFunctionAction(func) then action

    // e.g. foo:myFunc then Delay( 1 )
    // myFunc return true iff the action has finished.
    static fun then( func : ()->bool, action : Action ) = FunctionAction(func) then action

    // e.g. foo:myFunc and Delay( 1 )
    // myFunc return true iff the action has finished.
    static fun and( func : ()->bool, action : Action ) = FunctionAction(func) and action

    // e.g. foo:myFunc onceAnd( Delay( 1 ) )
    // myFunc returns void
    static fun onceAnd( func : (), action : Action ) = OnceFunctionAction(func) and action

    /**
     * Replaces the action with a [function] call.
     * The new [Action] will end when [function] return `true`
     */
    static fun replaceAction( actionHolder : ActionHolder, function : ()->bool ) {
        actionHolder.replaceAction( FunctionAction(function) )
    }

    // === ActionHolder extension ===

    /**
     * The current [Action] is extended. When the existing [Action] ends
     * [function] will be called repeatedly, until it returns `true`.
     */
    static fun then( actionHolder : ActionHolder, function : ()->bool ) {
        actionHolder.then(FunctionAction(function))
    }

    /**
     * The current [Action] is extended. As well as acting on the existing [Action],
     * [function] will be called repeatedly, until it returns `true`.
     */
    static fun and( actionHolder : ActionHolder, function : ()->bool ) {
        actionHolder.and( FunctionAction(function) )
    }

    static fun andOnce( actionHolder : ActionHolder, function : () ) {
        actionHolder.and( OnceFunctionAction(function) )
    }

    static fun thenOnce( actionHolder : ActionHolder, function : () ) {
        actionHolder.then( OnceFunctionAction(function) )
    }

    static fun thenImmediately( actionHolder : ActionHolder, function : () ) {
        actionHolder.then( ImmediateFunctionAction(function) )
    }

    // === Actions extensions ===

    static fun addOnce( actions : Actions, function : () ) {
        actions.add( OnceFunctionAction( function ) )
    }

    static fun add( actions : Actions, function : ()->bool ) {
        actions.add( FunctionAction( function ) )
    }

    // === FragmentMaker ===
    
    static fun createFragmentRoles( parent : Actor, factory : (Actor, Vector2)->Role ) {
        FragmentMaker.createFragmentRoles( parent, FeatherFragmentRoleFactory( factory ) )
    }

    static fun createFragmentActions( parent : Actor, factory : (Actor, Vector2)->Action ) {
        FragmentMaker.createFragmentRoles( parent, FeatherFragmentActionFactory( factory ) )
    }

    // === Others ===

    static fun runLater( function : () ) {
        Game.instance.runLater( RunnableFunction( function ) )
    }



}

/**
    Allows a feather function, which returns a bool to be used as an Action.
    When the function returns true, the action ends.
*/
class FunctionAction( val function : ()->bool ) : Action {
    override fun act(): bool {
        return function()
    }
}

/**
    Allows a feather function, without a return value to be used as an Action.
    The action lasts for exactly 1 tick.

    Calls [function] from act() and returns true.
    begin() always returns false.
    If you want the [function] to be called from begin(), then use ImmediateFunctionAction instead.
*/
class OnceFunctionAction( val function : () ) : Action {
    override fun act(): bool {
        function()
        return true
    }
}
/**
    Similar to OnceFunctionAction, but calls the function from begin(), rather than act().

    Calls [function[ from begin() and returns true.
    Therefore act() will never be called.
*/
class ImmediateFunctionAction( val function : () ) : Action {
    override fun begin(): bool {
        function()
        return true
    }
    override fun act() = true
}

/**

*/
class UntilAction( val function : ()->bool ) : Action {
    override fun begin() : bool = act()
    override fun act() : bool = function() == true
}

class WhileAction( val function : ()->bool ) : Action {
    override fun begin() : bool  = act()
    override fun act() :bool = function() == false
}

/**
    An Action, which is created, each time begin() is called.
    This is useful when the Action uses random numbers.
    e.g. ( Delay( randomAmount ) then EventAction( "hi" ) ).forever()

    will NOT cause random delays between the "hi" events, because the random number is only generated once.
*/
class FactoryAction( val actionFactory : ()->Action ) : Action {

    var current : Action = null

    override fun begin() : bool {
        current = actionFactory()
        return current.begin()
    }

    override fun act() : bool {
        return current.act()
    }

}

/**
    Allows Feather functions to be used with Game.runLater( Runnable ).
    You don't need to use this class directly, instead just use the static function :

        runLater( myFunc )

    Note, this is largely redundant now, because Game.scene.actions providers a richer
    way to call functions (via Actions). But runLater(func) is easier to use, and
    therefore it will remain.
*/
class RunnableFunction( val function: () ) : Runnable {
    override fun run() {
        function()
    }
}

/**
    Allows feather functions to be used with FragmentMaker
*/
class FeatherFragmentRoleFactory( val factory : (Actor, Vector2)->Role ) : FragmentRoleFactory {
    override fun create( fragment : Actor, offset : Vector2 ) = factory( fragment, offset )
}
class FeatherFragmentActionFactory( val factory : (Actor, Vector2)->Action ) : FragmentRoleFactory {
    override fun create( fragment : Actor, offset : Vector2 ) = ActionRole( factory( fragment, offset ) )
}
