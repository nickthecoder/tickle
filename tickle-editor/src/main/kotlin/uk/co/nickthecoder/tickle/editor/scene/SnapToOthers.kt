/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import javafx.beans.property.SimpleBooleanProperty
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.BooleanParameter
import uk.co.nickthecoder.paratask.parameters.InformationParameter
import uk.co.nickthecoder.paratask.parameters.asHorizontal
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.editor.SNAP_TO_OTHERS_TOGGLE
import uk.co.nickthecoder.tickle.editor.parameters.Vector2Parameter
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * When dragging an [Actor], snap in three different ways :
 *
 *  1. Snap the [Actor]'s position to another Actor's position
 *
 *  2. Snap the [Actor]'s position to another Actor's snap point (in [Pose]).
 *
 *  3. Snap this [Actor]'s snap point to another Actor's position.
 */
class SnapToOthers() : Snap {

    override val enabledProperty = SimpleBooleanProperty(true)

    var closeness = Vector2(10f, 10f)

    fun snapActor(sceneEditor: SceneEditor, actor: Actor, adjustments: MutableList<SnapToAdjustment>) {

        if (!isEnabled) return

        for (es in sceneEditor.editingStages) {
            val stage = es.stage
            for (other in stage.actors) {
                if (other !== actor) {

                    // Snap the two actors positions
                    val dx = actor.x - other.x
                    val dy = actor.y - other.y
                    if (dx > -closeness.x && dx < closeness.x && dy > -closeness.y && dy < closeness.y) {
                        adjustments.add(SnapToAdjustment(-dx, -dy, Math.abs(dx) + Math.abs(dy)))
                    }

                    // Snap this actor position with the other actor's snap points
                    other.poseAppearance?.pose?.snapPoints?.forEach { point ->
                        snapPositionToSnapPoint(actor.position, other, point, other.poseAppearance!!.pose, adjustments)
                    }

                    // Snap this actor's snap points with the other actor's position
                    actor.poseAppearance?.pose?.snapPoints?.forEach { point ->
                        snapSnapPointToOtherPosition(actor, other, point, actor.poseAppearance!!.pose, adjustments)
                    }
                }
            }
        }
    }

    fun snapPointToSnapPoints(sceneEditor: SceneEditor, position: Vector2, exclude : Actor, adjustments: MutableList<SnapToAdjustment>) {

        if (!isEnabled) return

        for (es in sceneEditor.editingStages) {
            val stage = es.stage
            for (other in stage.actors) {
                if (other !== exclude) { // Prevent snapping to oneself!
                    other.poseAppearance?.pose?.snapPoints?.forEach { point ->
                        snapPositionToSnapPoint(position, other, point, other.poseAppearance!!.pose, adjustments)
                    }
                }
            }
        }
    }

    private fun snapPositionToSnapPoint(
        position: Vector2, other: Actor, point: Vector2, pose: Pose,
        adjustments: MutableList<SnapToAdjustment>
    ) {

        val adjustedPoint = Vector2(point)
        adjustedPoint.x -= pose.offsetX
        adjustedPoint.y -= pose.offsetY

        if (other.direction.radians != pose.direction.radians) {
            adjustedPoint.setRotateRadians(other.direction.radians - pose.direction.radians)
        }

        adjustedPoint.x *= other.scale.x
        adjustedPoint.y *= other.scale.y

        val dx = position.x - other.x - adjustedPoint.x
        val dy = position.y - other.y - adjustedPoint.y
        if (dx > -closeness.x && dx < closeness.x && dy > -closeness.y && dy < closeness.y) {
            adjustments.add(SnapToAdjustment(-dx, -dy, Math.abs(dx) + Math.abs(dy)))
        }
    }

    private fun snapSnapPointToOtherPosition(
        actor: Actor,
        other: Actor,
        point: Vector2,
        pose: Pose,
        adjustments: MutableList<SnapToAdjustment>
    ) {

        val adjustedPoint = Vector2(point)
        adjustedPoint.x -= pose.offsetX
        adjustedPoint.y -= pose.offsetY

        if (actor.direction.radians != pose.direction.radians) {
            adjustedPoint.setRotateRadians(actor.direction.radians - pose.direction.radians)
        }

        adjustedPoint.x *= actor.scale.x
        adjustedPoint.y *= actor.scale.y

        val dx = other.x - actor.x - adjustedPoint.x
        val dy = other.y - actor.y - adjustedPoint.y
        if (dx > -closeness.x && dx < closeness.x && dy > -closeness.y && dy < closeness.y) {
            adjustments.add(SnapToAdjustment(dx, dy, Math.abs(dx) + Math.abs(dy)))
        }
    }

    override fun task() = SnapToOthersTask()


    inner class SnapToOthersTask : AbstractTask() {

        val enabledP = BooleanParameter("enabled", value = enabledProperty.value)

        val toggleInfoP = InformationParameter(
            "toggleInfo",
            information = "Note. You can toggle snapping to other actors using the keyboard shortcut : ${SNAP_TO_OTHERS_TOGGLE.shortcutLabel() ?: "<NONE>"}\n${snapInfo()}"
        )

        val closenessP =
            Vector2Parameter("closeness", value = closeness, description = "Snap when this close to another actor")
                .asHorizontal()

        override val taskD = TaskDescription("snapToOthers")
            .addParameters(enabledP, toggleInfoP, closenessP)


        override fun run() {
            enabledProperty.value = enabledP.value!!
            closeness.set(closenessP.value)
        }

    }

}
