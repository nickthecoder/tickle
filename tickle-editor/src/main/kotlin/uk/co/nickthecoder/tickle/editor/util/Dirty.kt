package uk.co.nickthecoder.tickle.editor.util

import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0
import kotlin.reflect.jvm.isAccessible

fun <T> dirty(initializer: () -> T): Dirty<T> = DirtyValue(initializer)

// TODO This look wrong on two counts. Firstly, I shouldn't be using isAccessible = true,
// Secondly, this isn't type checked (it can be called for any KProperty0, not only those with a Dirty delegate.
fun KProperty0<*>.markAsDirty() {
    isAccessible = true
    (getDelegate() as Dirty<*>).reset()
}

interface Dirty<T> {
    operator fun getValue(from: Any, property: KProperty<*>): T
    fun reset()
}

private class DirtyValue<T>(val initializer: () -> T) : Dirty<T> {

    private var _value: T? = null

    override operator fun getValue(from: Any, property: KProperty<*>): T {
        val v1 = _value
        if (v1 !== null) {
            return v1
        }

        val typedValue = initializer()
        _value = typedValue
        return typedValue
    }

    override fun reset() {
        _value = null
    }
}
