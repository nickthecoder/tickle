/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.resources

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.scene.SelectedActor
import uk.co.nickthecoder.tickle.editor.scene.history.AddActor
import uk.co.nickthecoder.tickle.editor.scene.history.Change
import uk.co.nickthecoder.tickle.editor.scene.history.DeleteActors
import uk.co.nickthecoder.tickle.editor.scene.history.MoveActor
import uk.co.nickthecoder.tickle.stage.Stage
import java.util.jar.Attributes

/**
 * Allows your game to impose constraints on where actors can be placed on a stage.
 * For example, if your game is based on a rectangular grid, then you would use a
 * [GridConstraint] to ensure that the x & y values all line up correctly.
 *
 * You can create more complex constraints, for example you could nudge other actors
 * out of the way if they are getting too crowded in one place.
 *
 * Another possibility is to replace neighbouring actors. e.g. If you have a set of tiles
 * for different types of terrain, sea, beach, grassland etc. then when you add a "sea" tile, it
 * could automatically replace surrounding grassland tiles with beach tiles.
 *
 * It is also possible to change the actor being added. i.e. [addActor] can return
 * an [AddActor] [Change] for a different ActorResource, or maybe more than one [AddActor].
 *
 *
 * NOTE. StageConstraint is only used within the Editor, it is never used while the game is running.
 */
interface StageConstraint {

    val attributes: Attributes

    fun forStage(stage: Stage) {}

    /**
     * Called while an actor is being dragged.
     *
     * @param dragX and [dragY] are the world coordinates of where the actor would be moved to
     * without any stage constraint.
     *
     * On exit, [ActorResource.x] and [ActorResource.y] should contain the point where the [Actor]
     * should be moved to.
     *
     * Nothing else should be changed
     * (unlike [addActor] and [moveActor], where other [ActorResource]s can be affected).
     *
     * Return false iff further snapping can be done.
     */
    fun snapActor(sa: SelectedActor, dragX: Float, dragY: Float): Boolean

    /**
     * Called when an actor is first added to the scene.
     * @return A list of Changes. [NoStageConstraint] returns an [AddActor] [Change].
     */
    fun addActor(sa: SelectedActor): List<Change>

    /**
     * * return [NoStageConstraint] returns a [MoveActor] [Change].
     */
    fun moveActor(sa: SelectedActor): List<Change>

    /**
     * @return [NoStageConstraint] returns a [DeleteActors] [Change].
     */
    fun deleteActor(sa: SelectedActor): List<Change>

}

open class NoStageConstraint : StageConstraint {

    override val attributes = Attributes()

    override fun addActor(sa: SelectedActor): List<Change> = listOf(AddActor(sa))

    override fun moveActor(sa: SelectedActor): List<Change> = listOf(MoveActor(sa.actor, sa.oldX, sa.oldY))

    override fun deleteActor(sa: SelectedActor): List<Change> = listOf(DeleteActors(listOf(sa)))

    override fun snapActor(sa: SelectedActor, dragX: Float, dragY: Float): Boolean {
        sa.actor.x = dragX
        sa.actor.y = dragY
        return false
    }
}
