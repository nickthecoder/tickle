/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.parameters

import javafx.util.StringConverter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.util.uncamel
import uk.co.nickthecoder.tickle.misc.vector2FromString
import uk.co.nickthecoder.tickle.misc.vector2ToString
import uk.co.nickthecoder.tickle.util.Vector2

class Vector2Parameter(
    name: String,
    label: String = name.uncamel(),
    value: Vector2 = Vector2(),
    description: String = "",
)

    : CompoundParameter<Vector2>(
    name, label, description
) {

    val xP = FloatParameter("${name}_x", label = "", minValue = -Float.MAX_VALUE)
    var x by xP

    val yP = FloatParameter("${name}_y", label = ",", minValue = -Float.MAX_VALUE)
    var y by yP

    override val converter = object : StringConverter<Vector2>() {
        override fun fromString(string: String): Vector2 {
            return vector2FromString(string)
        }

        override fun toString(obj: Vector2): String {
            return vector2ToString(obj)
        }
    }

    override var value: Vector2
        get() {
            return Vector2(xP.value ?: 0f, yP.value ?: 0f)
        }
        set(value) {
            xP.value = value.x
            yP.value = value.y
        }


    init {
        this.value = value
        addParameters(xP, yP)
        asHorizontal(labelPosition = LabelPosition.LEFT)
    }

    override fun coerce(v: Any?) {
        if (v is Vector2) {
            value = v
        } else {
            super.coerce(v)
        }
    }

    override fun toString(): String {
        return "Vector2Parameter : $value"
    }

    override fun copy(): Vector2Parameter {
        val copy = Vector2Parameter(name, label, value, description)
        return copy
    }
}
