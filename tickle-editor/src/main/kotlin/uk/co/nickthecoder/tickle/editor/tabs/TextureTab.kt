/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.event.EventHandler
import javafx.geometry.Rectangle2D
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.canvas.Canvas
import javafx.scene.control.Button
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.image.ImageView
import javafx.scene.layout.StackPane
import javafx.stage.Stage
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParameterException
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.parameters.fields.ParameterField
import uk.co.nickthecoder.paratask.parameters.fields.TaskForm
import uk.co.nickthecoder.paratask.util.process.Exec
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.parameters.ImageParameter
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.tasks.RenameFileTask
import uk.co.nickthecoder.tickle.editor.tasks.colorTransparentPixels
import uk.co.nickthecoder.tickle.editor.util.ImageCache
import uk.co.nickthecoder.tickle.editor.util.cachedImage
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.util.YDownRect
import java.io.File

class TextureTab(name: String, val texture: Texture)

    : EditTab(name, texture, ResourceType.TEXTURE.createImageView()) {

    val task = TextureTask(name, texture)
    val taskForm = TaskForm(task)

    val gridTask = TextureGridTask()
    val gridForm = TaskForm(gridTask)

    val posesEditor = PosesEditor(texture)

    val minorTabs = TabPane()

    val detailsTab = Tab("Details", taskForm.build())
    val posesTab = Tab("Poses", posesEditor.build())
    val gridTab = Tab("Grid of Poses", gridForm.build())

    init {
        minorTabs.side = Side.BOTTOM
        minorTabs.tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

        minorTabs.tabs.add(detailsTab)
        minorTabs.tabs.add(posesTab)
        minorTabs.tabs.add(gridTab)

        borderPane.center = minorTabs

        val editButton = Button("Edit")
        editButton.setOnAction { edit() }
        leftButtons.children.add(editButton)

        texture.file?.let { file ->
            val fixTransparentPixelsButton = Button("Fix Transparent Pixels")
            fixTransparentPixelsButton.setOnAction { colorTransparentPixels(file) }
            leftButtons.children.add(fixTransparentPixelsButton)
        }

        task.taskD.root.listen { needsSaving = true }

    }

    override fun removed() {
        super.removed()
        posesEditor.closed()
    }

    override fun justSave(): Boolean {
        val result = taskForm.check()
        if (result) {
            task.run()
        }
        return result
    }

    fun edit() {
        texture.file?.absoluteFile?.let { file ->
            val xcf = File(file.parentFile, file.nameWithoutExtension + ".xcf")
            if (xcf.exists()) {
                Exec("gimp", xcf).start()
            } else {
                val svg = File(file.parentFile, file.nameWithoutExtension + ".svg")
                if (svg.exists()) {
                    Exec("inkscape", svg).start()
                } else {
                    Exec("gimp", file).start()
                }
            }
        }
    }


    inner class TextureGridTask() : AbstractTask() {

        private val widthP = IntParameter("poseWidth")

        private val heightP = IntParameter("poseHeight")


        private val marginTopP = IntParameter("marginTop", value = 0)

        private val marginLeftP = IntParameter("marginLeft", value = 0)

        private val xGapP = IntParameter("xGap", value = 0)

        private val yGapP = IntParameter("yGap", value = 0)

        private val imageP = ImageParameter("image", image = texture.cachedImage()) {
            GridImageField(it)
        }

        private val poseNamePrefixP = StringParameter("poseNamePrefix",
                value = Editor.resources.textures.findName(texture) ?: "newGrid")


        private val createPosesP = ButtonParameter("createPoses", label = "", buttonText = "Create Poses") {
            if (gridForm.check()) {
                run()
            }
        }


        override val taskD = TaskDescription("Grid of Poses")
                .addParameters(widthP, heightP, marginTopP, marginLeftP, xGapP, yGapP, poseNamePrefixP, imageP, createPosesP)


        override fun run() {
            val across = (texture.width - marginLeftP.value!!) / (widthP.value!! + xGapP.value!!)
            val down = (texture.height - marginTopP.value!!) / (heightP.value!! + yGapP.value!!)

            for (y in 0 until down) {
                for (x in 0 until across) {
                    createPose(x, y, false)
                }
            }

        }

        private fun createPose(x: Int, y: Int, open: Boolean): Pose {
            val rect = YDownRect(
                    x * (widthP.value!! + xGapP.value!!) + marginLeftP.value!!,
                    y * (heightP.value!! + yGapP.value!!) + marginTopP.value!!,
                    x * (widthP.value!! + xGapP.value!!) + marginLeftP.value!! + widthP.value!!,
                    y * (heightP.value!! + yGapP.value!!) + marginTopP.value!! + heightP.value!!
            )

            val poseName = poseNamePrefixP.value + "${x}x${y}"

            val existingPose = Editor.resources.poses.find(poseName)
            if (existingPose == null) {
                val pose = Pose(texture, rect)
                Editor.resources.poses.add(poseName, pose)
                if (open) {
                    MainWindow.instance.openTab(poseName, pose)
                }
                return pose

            } else {
                if (existingPose.texture === texture) {
                    existingPose.pixelRect = rect
                }
                if (open) {
                    MainWindow.instance.openTab(poseName, existingPose)
                }
                return existingPose
            }
        }

        inner class GridImageField(private val imageParameter: ImageParameter)

            : ParameterField(imageParameter) {

            private val imageView = ImageView(imageParameter.image)

            private val canvas = Canvas()

            override fun createControl(): Node {

                val image = imageParameter.image
                val width = if (image.width > 400) 400.0 else image.width
                val height = if (image.height > 400) 400.0 else image.height
                val viewPort = Rectangle2D(0.0, 0.0, width, height)
                imageView.viewportProperty().set(viewPort)

                canvas.width = width
                canvas.height = height

                updateCanvas()

                val stack = StackPane()
                stack.children.addAll(imageView, canvas)
                stack.onMouseClicked = EventHandler { event ->
                    if (event.clickCount == 2) {
                        createPoseAt(event.x, event.y)
                    }
                }

                gridTask.taskD.root.listen { updateCanvas() }

                return stack
            }

            private fun createPoseAt(x: Double, y: Double) {

                val width = widthP.value ?: return
                val height = heightP.value ?: return
                val marginLeft = marginLeftP.value ?: return
                val marginTop = marginTopP.value ?: return
                val xGap = xGapP.value ?: return
                val yGap = yGapP.value ?: return

                val x1 = (x.toInt() - marginLeft) / (width + xGap)
                val y1 = (y.toInt() - marginTop) / (height + yGap)

                createPose(x1, y1, true)
            }

            private fun updateCanvas() {
                val gc = canvas.graphicsContext2D
                gc.clearRect(0.0, 0.0, canvas.width, canvas.height)

                val width = widthP.value ?: return
                val height = heightP.value ?: return
                val marginLeft = marginLeftP.value ?: return
                val marginTop = marginTopP.value ?: return
                val xGap = xGapP.value ?: return
                val yGap = yGapP.value ?: return

                if (width < 2 || height < 2) return

                val across = Math.min((texture.width - marginLeft) / (width + xGap), 20)
                val down = Math.min((texture.height - marginTop) / (height + yGap), 20)

                if (xGap < 1 || yGap < 1) {
                    // Two "axis" just before marginLeft and marginTop, with divider marks (like a ruler)
                    // acros the axis.

                    gc.beginPath()
                    gc.moveTo(canvas.width, marginTop - 1.0)
                    gc.lineTo(marginLeft - 1.0, marginTop - 1.0)
                    gc.lineTo(marginLeft - 1.0, canvas.height)
                    gc.stroke()

                    for (y in 0..across) {
                        gc.beginPath()
                        gc.moveTo(marginLeft + 0.0, marginTop + y * (height + yGap) + 0.0)
                        gc.lineTo(marginLeft + 4.0, marginTop + y * (height + yGap) + 0.0)
                        gc.stroke()

                    }
                    for (x in 0..down) {
                        gc.beginPath()
                        gc.moveTo(marginLeft + x * (width + xGap) + 0.0, marginTop + 0.0)
                        gc.lineTo(marginLeft + x * (width + xGap) + 0.0, marginTop + 4.0)
                        gc.stroke()
                    }

                } else {
                    // Rectangles around each pose.

                    for (y in 0 until down) {
                        for (x in 0 until across) {
                            gc.beginPath()
                            gc.rect(
                                    marginLeft + x * (width + xGap) - 1.0,
                                    marginTop + y * (height + yGap) - 1.0,
                                    width + 1.0,
                                    height + 1.0
                            )
                            gc.stroke()
                        }
                    }
                }

            }

        }
    }

}

class TextureTask(val name: String, val texture: Texture) : AbstractTask() {

    val nameP = StringParameter("name", value = name)


    val fileP = StringParameter("file", value = texture.file?.path ?: "")
    val renameP = ButtonParameter("rename", buttonText = "Rename") { onRename() }
    val fileAndRenameP = SimpleGroupParameter("fileAndRename", label = "Filename")
            .addParameters(fileP, renameP).asHorizontal(labelPosition = LabelPosition.NONE)

    val commentP = StringParameter("comment", required = false, rows = 5)

    val imageP = ImageParameter("image", image = ImageCache.image(texture.file!!))

    override val taskD = TaskDescription("editTexture")
            .addParameters(nameP, fileAndRenameP, commentP, imageP)

    init {
        fileP.enabled = false
        commentP.value = texture.comment

    }

    override fun customCheck() {
        val t = Editor.resources.textures.find(nameP.value)
        if (t != null && t != texture) {
            throw ParameterException(nameP, "This name is already used.")
        }
    }

    override fun run() {
        if (nameP.value != name) {
            Editor.resources.textures.rename(name, nameP.value)
        }

        texture.comment = commentP.value
    }

    // TODO Renaming the file doesn't change the Texture object

    fun onRename() {
        texture.file?.let { file ->
            val renameTask = RenameFileTask(file)
            try {
                check()
            } catch (e: Exception) {
                return
            }
            renameTask.taskRunner.listen { cancelled ->
                if (!cancelled) {
                    renameTask.newNameP.value?.path?.let { fileP.value = it }
                    run()
                    Editor.resources.save()
                }
            }
            val tp = TaskPrompter(renameTask)
            tp.placeOnStage(Stage())
        }
    }

}
