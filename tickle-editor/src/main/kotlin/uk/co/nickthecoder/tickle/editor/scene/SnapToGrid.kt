/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import javafx.beans.property.SimpleBooleanProperty
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.BooleanParameter
import uk.co.nickthecoder.paratask.parameters.InformationParameter
import uk.co.nickthecoder.paratask.parameters.asHorizontal
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.SNAP_TO_GRID_TOGGLE
import uk.co.nickthecoder.tickle.editor.parameters.Vector2Parameter
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Snaps [Actor]s positions to a grid.
 *
 * Note, this only considers the [Actor.position], not its edges/corners.
 */
class SnapToGrid : Snap {

    override val enabledProperty = SimpleBooleanProperty(false)

    var spacing = Vector2(50f, 50f)

    var offset = Vector2(0f, 0f)

    var closeness = Vector2(10f, 10f)

    val adjustment = SnapToAdjustment()

    fun snapPosition(position: Vector2, adjustments: MutableList<SnapToAdjustment>) {

        if (enabledProperty.value == false) return

        adjustment.reset()

        var dx = (position.x - offset.x) % spacing.x
        var dy = (position.y - offset.y) % spacing.y

        if (dx < 0) dx += spacing.x
        if (dy < 0) dy += spacing.y

        if (dx < closeness.x) {
            adjustment.dx = -dx
            adjustment.score = closeness.y + Math.abs(adjustment.dx)
        } else if (spacing.x - dx < closeness.x) {
            adjustment.dx = spacing.x - dx
            adjustment.score = closeness.y + Math.abs(adjustment.dx)
        }

        if (dy < closeness.y) {
            adjustment.dy = -dy
            adjustment.score =
                if (adjustment.score == Float.MAX_VALUE) Math.abs(adjustment.dy) + closeness.x else adjustment.score + Math.abs(
                    adjustment.dy
                )
        } else if (spacing.y - dy < closeness.y) {
            adjustment.dy = spacing.y - dy
            adjustment.score =
                if (adjustment.score == Float.MAX_VALUE) Math.abs(adjustment.dy) + closeness.x else adjustment.score + Math.abs(
                    adjustment.dy
                )
        }

        if (adjustment.score != Float.MAX_VALUE) {
            adjustments.add(adjustment)
        }
    }

    override fun task() = GridTask()

    override fun toString(): String {
        return "Grid spacing=(${spacing.x},${spacing.y}) offset=(${offset.x}, ${offset.y}) closeness=(${closeness.x},${closeness.y}) enabled=$enabledProperty"
    }

    inner class GridTask : AbstractTask() {

        val enabledP = BooleanParameter("enabled", value = enabledProperty.value)

        val toggleInfoP = InformationParameter(
            "toggleInfo",
            information = "Note. You can toggle grid snapping using the keyboard shortcut : ${SNAP_TO_GRID_TOGGLE.shortcutLabel() ?: "<NONE>"}\n${snapInfo()}"
        )

        val spacingP = Vector2Parameter("spacing", value = spacing)
            .asHorizontal()

        val closenessP =
            Vector2Parameter("closeness", value = closeness, description = "Snap when this close to a grid marker")
                .asHorizontal()

        val offsetP = Vector2Parameter("offset", value = offset)
            .asHorizontal()

        override val taskD = TaskDescription("editGrid")
            .addParameters(enabledP, toggleInfoP, spacingP, closenessP, offsetP)

        override fun run() {
            spacing.set(spacingP.value)
            offset.set(offsetP.value)
            closeness.set(closenessP.value)
            enabledProperty.value = enabledP.value!!

            if (closeness.x > spacing.x / 2) {
                closeness.x = spacing.x / 2
            }
            if (closeness.y > spacing.y / 2) {
                closeness.y = spacing.y / 2
            }
        }
    }

}
