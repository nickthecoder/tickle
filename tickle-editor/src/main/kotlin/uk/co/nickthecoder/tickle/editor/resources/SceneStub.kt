package uk.co.nickthecoder.tickle.editor.resources

import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.Scene
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.resources.Layout
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.util.DeletableResource
import uk.co.nickthecoder.tickle.util.Dependable
import uk.co.nickthecoder.tickle.util.SceneReader
import uk.co.nickthecoder.tickle.warn
import java.io.File


/**
 * Used in place of a SceneResource, without having to load it.
 * Used within the editor, and is only in the core project, because classes such as Costume
 * need this for their implementation of Deletable.
 * The Deletable methods will only be called from the editor though.
 */
class SceneStub(val file: File) : Dependable, DeletableResource {

    val name: String
        get() = Editor.resources.toPath(file).removeSuffix(".scene").removePrefix("scenes/")

    override fun equals(other: Any?): Boolean {
        if (other is SceneStub) {
            return file == other.file
        }
        return false
    }

    override fun hashCode() = file.hashCode() + 1

    fun load(): Scene = SceneReader.load(file, Editor.resources)

    override fun delete(resources: Resources) {
        file.delete()
        resources.fireRemoved(this, name)
    }

    override fun dependables(resources: Resources): List<Dependable> = emptyList()

    fun dependsOn(costume: Costume): Boolean {
        try {
            return load().dependsOn(costume)
        } catch (e: Exception) {
            warn("Couldn't load $file, when checking dependencies of $costume")
            return false
        }
    }

    fun dependsOn(resources: Resources, layout: Layout): Boolean {
        try {
            return load().dependsOn(resources, layout)
        } catch (e: Exception) {
            warn("Couldn't load $file, when checking dependencies of $layout")
            return false
        }
    }

    override fun toString() = "SceneStub for $file"

}
