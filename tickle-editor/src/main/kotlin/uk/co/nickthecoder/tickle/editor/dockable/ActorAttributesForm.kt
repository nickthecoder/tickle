/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.Node
import javafx.scene.control.ScrollPane
import javafx.scene.control.TitledPane
import javafx.scene.layout.VBox
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.parameters.TextStyleParameter
import uk.co.nickthecoder.tickle.editor.parameters.Vector2Parameter
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.resources.SceneListener
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.scene.history.ChangeActorAttribute
import uk.co.nickthecoder.tickle.editor.scene.history.ChangeRoleAttribute
import uk.co.nickthecoder.tickle.editor.scene.history.ChangeTextStyleAttribute
import uk.co.nickthecoder.tickle.editor.scene.history.History
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes
import uk.co.nickthecoder.tickle.editor.util.toJavaFX
import uk.co.nickthecoder.tickle.graphics.TextStyle
import uk.co.nickthecoder.tickle.util.Attribute
import uk.co.nickthecoder.tickle.util.Vector2
import java.lang.reflect.Field

private val roleExpandedProperty = SimpleBooleanProperty(true)
private val actorExpandedProperty = SimpleBooleanProperty(true)
private val typeStyleExpandedProperty = SimpleBooleanProperty(true)
private val costumeExpandedProperty = SimpleBooleanProperty(false)

/**
 * Edit properties of an [Actor] and its [Appearance], as well as the @[Attribute]s of the Actor's [Role]
 * using a form (i.e. by typing in values).
 * Each property/attribute is edited via a [Parameter] (from the paratask library).
 * The parameters for [Actor] and [Appearance] are known in advance, but those for [Role] are based on
 * @[Attribute] tags on fields of the [Role]'s subclass (and are therefore dynamic).
 *
 * Changes to [History] (the undo/redo mechanism) is performed through the [ChangeActorAttribute]
 * and [ChangeRoleAttribute].
 *
 * When a [SceneListener] event occurs, we update the [Parameter]s in this form based on the
 * [Actor]'s, [Appearance]'s and [Role]'s properties.
 */
class ActorAttributesForm(
    private val sceneEditor: SceneEditor,
    val actor: Actor,
    private val scene: Scene,
    showAutoPositionAlignment: Boolean = false
) : SceneListener {

    /**
     * Custom Role attributes
     */
    private val roleAttributesP = SimpleGroupParameter("roleAttributes").asVertical()

    /**
     * Parameters for Actor and Appearance (excluding TextStyle of TextAppearance).
     */
    private val actorAttributesP = SimpleGroupParameter("actorGroup").apply {
        asVertical()
    }

    private val nameP = StringParameter("name", value = actor.name, required = false)

    private val positionP = Vector2Parameter("position", value = actor.position)

    private val zOrderP = FloatParameter("zOrder", value = actor.zOrder)

    private val autoPositionAlignmentP =
        Vector2Parameter("autoPositionAlignment", value = actor.autoPositionAlignment)

    private val directionP = DoubleParameter("direction", value = actor.direction.degrees)

    /**
     * Not used when Actor's appearance is NinePatchAppearance or TiledAppearance.
     */
    private val scaleP = Vector2Parameter("scale", value = actor.scale)

    /**
     * Only used when Actor's appearance is NinePatchAppearance or TiledAppearance.
     */
    private val sizeP = Vector2Parameter("size", value = actor.resizeAppearance?.size ?: Vector2(1f, 1f))

    private val resizeableAlignmentP =
        Vector2Parameter(
            "resizeableAlignment",
            label = "Alignment",
            value = actor.resizeAppearance?.sizeAlignment ?: Vector2()
        )

    /**
     * The Group for [sizeP] and [resizeableAlignmentP] (for NinePatchAppearance and TiledAppearance).
     */
    private val resizableGroupP = SimpleGroupParameter("resizeable")
        .addParameters(sizeP, resizeableAlignmentP)
        .asPlain()

    /**
     * Not used for TextAppearance (when TextStyle's color is used instead).
     */
    private val colorP = AlphaColorParameter("color", value = actor.tint.toJavaFX())

    private val textP = StringParameter("text", value = actor.textAppearance?.text ?: "", rows = 3)

    // Parameters for TextStyle (only used if the Actor's appearance is TextAppearance).

    private val textStyleP = TextStyleParameter("textStyle")
        .asVertical()

    /**
     * When we are notified of a change to the [actor], we update the parameters.
     * If the update to the parameters causes ANOTHER notification to be fired, then we ignore it
     * (because we were the one causing the change).
     */
    private var ignoreChanges: Boolean = false

    private val roleAttributes = createRoleAttributes()

    /**
     *
     */
    private val roleParameterListener = object : ParameterListener {
        override fun parameterChanged(event: ParameterEvent) {
            roleParameterChanged(event)
        }
    }
    private val actorParameterListener = object : ParameterListener {
        override fun parameterChanged(event: ParameterEvent) {
            actorParameterChanged(event)
        }
    }
    private val textStyleParameterListener = object : ParameterListener {
        override fun parameterChanged(event: ParameterEvent) {
            textStyleParameterChanged(event)
        }
    }

    init {
        actor.textAppearance?.let { ta ->
            textStyleP.from(ta.textStyle)
        }

        actorAttributesP.addParameters(nameP, positionP, zOrderP, directionP)

        if (actor.textAppearance != null) {
            actorAttributesP.addParameters(textP)
        }

        if (actor.appearance !is TextAppearance) {
            actorAttributesP.addParameters(colorP)
        }

        if (actor.appearance is ResizeAppearance) {
            actorAttributesP.addParameters(resizableGroupP)
        } else {
            actorAttributesP.addParameters(scaleP)
        }

        if (showAutoPositionAlignment) {
            actorAttributesP.addParameters(autoPositionAlignmentP)
        }

        roleAttributesP.parameterListeners.add(roleParameterListener)
        actorAttributesP.parameterListeners.add(actorParameterListener)
        textStyleP.parameterListeners.add(textStyleParameterListener)
        sceneEditor.sceneListeners.add(this)
    }

    /**
     * ActorAttributeBox must call this when this form is no longer used.
     * (Otherwise we will have memory leaks, as well as a waste of time updating
     * parameters that can never be seen).
     */
    fun unregister() {
        roleAttributesP.parameterListeners.remove(roleParameterListener)
        actorAttributesP.parameterListeners.remove(actorParameterListener)
        textStyleP.parameterListeners.remove(textStyleParameterListener)
        sceneEditor.sceneListeners.remove(this)
    }

    /**
     * [DesignAttributes] does the hard work of scanning the Role subclass for @[Attribute] tags,
     * and creating the appropriate [Parameter]s for each field.
     */
    private fun createRoleAttributes(): DesignAttributes {

        return DesignAttributes().apply {
            actor.role?.let { updateAttributesMetaData(it) }

            map().keys.sorted().map { getOrCreateData(it) }.forEach { data ->
                data.parameter?.let { it ->
                    val parameter = it.copyBounded()
                    if (!data.isAlternate) {
                        roleAttributesP.add(parameter)
                    }
                    try {
                        parameter.stringValue = data.value ?: ""
                    } catch (e: Exception) {
                        // Do nothing
                    }
                }
            }
        }
    }

    /**
     * Creates a [VBox] containing [TitledPane]s which hold the [Parameter]s.
     * The return value is a [ScrollPane].
     */
    fun build(): Node {
        val box = VBox()

        // The form is split into 3 TitledPane.
        // The 1st is for Role, the 2nd for Actor and Appearance, and the 3rd for TextStyle.

        if (roleAttributesP.children.isNotEmpty()) {
            val tp = TitledPane("Role Attributes", roleAttributesP.createField().controlContainer)
            tp.expandedProperty().bindBidirectional(roleExpandedProperty)
            box.children.add(tp)
        }
        val actorTP = TitledPane("Actor Attributes", actorAttributesP.createField().controlContainer)
        actorTP.expandedProperty().bindBidirectional(actorExpandedProperty)
        box.children.add(actorTP)

        val textStyleTP = TitledPane("Text Style", textStyleP.createField().controlContainer)
        textStyleTP.expandedProperty().bindBidirectional(typeStyleExpandedProperty)
        if (actor.textAppearance != null) {
            box.children.add(textStyleTP)
        }

        val scrollPane = ScrollPane(box)
        scrollPane.isFitToWidth = true
        return scrollPane
    }

    /**
     * A [Parameter] for [Role] has been changed. We need to apply this change to [History],
     * which will also cause a [SceneListener] event to occur.
     * The [SceneListener] event may cause other parts of the scene editor to update themselves.
     */
    private fun roleParameterChanged(event: ParameterEvent) {
        if (!ignoreChanges) {

            val parameter = (event.innerParameter as? ValueParameter<*>) ?: return
            val fullName = parameter.name
            if (fullName.startsWith("attribute_")) {
                val name = fullName.substring(10)
                val field = findRoleField(name)!!
                val newValue = parameter.value
                sceneEditor.history.makeChange(
                    ChangeRoleAttribute(actor.role!!, field, event.oldValue, newValue)
                )
            }
        }
    }

    /**
     * A [Parameter] for [Actor] or [Appearance] has been changed. We need to apply this change to [History],
     * which will also cause a [SceneListener] event to occur.
     * The [SceneListener] event will cause other parts of the scene editor to update themselves.
     */
    private fun actorParameterChanged(event: ParameterEvent) {
        if (!ignoreChanges) {
            if (!sceneEditor.history.updating) { // Not needed?
                (event.innerParameter as? ValueParameter<*>)?.let { p ->
                    // Get to the CompoundParameter if there is one.
                    val parameter = if (p.parent is CompoundParameter<*>) {
                        p.parent as CompoundParameter<*>
                    } else {
                        p
                    }
                    val name = parameter.name
                    sceneEditor.history.makeChange(
                        ChangeActorAttribute(actor, name, event.oldValue, parameter.value)
                    )
                }
            }
        }
    }

    /**
     * A [Parameter] for [TextStyle] has been changed. We need to apply this change to [History],
     * which will also cause a [SceneListener] event to occur.
     * The [SceneListener] event will cause other parts of the scene editor to update themselves.
     */
    private fun textStyleParameterChanged(event: ParameterEvent) {
        if (!ignoreChanges) {
            if (!sceneEditor.history.updating) { // Not needed?
                (event.innerParameter as? ValueParameter<*>)?.let { p ->
                    // Get to the CompoundParameter if there is one.
                    val parameter = if (p.parent is CompoundParameter<*>) {
                        p.parent as CompoundParameter<*>
                    } else {
                        p
                    }
                    val name = parameter.name
                    sceneEditor.history.makeChange(
                        ChangeTextStyleAttribute(actor, name, event.oldValue, parameter.value)
                    )
                }
            }
        }
    }

    private fun findRoleField(fieldName: String): Field? {
        val roleClass = actor.role!!.javaClass
        return roleClass.getField(fieldName)
    }

    override fun actorModified(scene: Scene, actor: Actor, type: ModificationType) {
        if (actor === this.actor) {
            ignoreChanges = true
            updateActorParameters()
            updateRoleParameters()
            ignoreChanges = false
        }
    }

    private fun updateActorParameters() {
        with(actor) {
            nameP.value = name
            positionP.value = Vector2(position)
            autoPositionAlignmentP.value.set(autoPositionAlignment)

            directionP.value = direction.degrees
            actor.resizeAppearance?.let {
                sizeP.value = Vector2(it.size)
                resizeableAlignmentP.value = Vector2(it.sizeAlignment)
            }
            scaleP.value = Vector2(scale)

            textP.value = textAppearance?.text ?: ""
            textAppearance?.textStyle?.let { textStyleP.from(it) }

            zOrderP.value = zOrder

            textP.hidden = textAppearance == null
            textStyleP.hidden = textAppearance == null
        }
    }

    private fun updateRoleParameters() {
        actor.role?.let { roleAttributes.updateParameterValues(it) }
    }

}
