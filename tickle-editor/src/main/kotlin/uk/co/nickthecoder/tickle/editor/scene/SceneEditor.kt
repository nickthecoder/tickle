/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.util.StringConverter
import uk.co.nickthecoder.fxessentials.ActionGroup
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.*
import uk.co.nickthecoder.tickle.editor.dockable.ActorsBox
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.resources.SceneListener
import uk.co.nickthecoder.tickle.editor.scene.history.AddActor
import uk.co.nickthecoder.tickle.editor.scene.history.ChangeZOrder
import uk.co.nickthecoder.tickle.editor.scene.history.DeleteActors
import uk.co.nickthecoder.tickle.editor.scene.history.History
import uk.co.nickthecoder.tickle.editor.tabs.SceneTab
import uk.co.nickthecoder.tickle.editor.util.alignMiddle
import uk.co.nickthecoder.tickle.editor.util.expandH
import uk.co.nickthecoder.tickle.editor.util.formattedTextFlow
import java.text.DecimalFormat

/**
 * Edits a Tickle [Scene] (not to be confused with JavaFX's Scene class!).
 *
 * The graphical part of a [SceneTab], where we can move, add, delete Actors etc.
 */
class SceneEditor(val scene: Scene, val snapPreferences: SnapPreferences) {

    /**
     * Stores the changes made to the scene to allow undo/redo.
     */
    val history = History(this)

    val editingStages: List<EditingStage> = scene.stages.entries.mapNotNull {
        val view = it.value.firstView()
        if (view == null) {
            null
        } else {
            EditingStage(this@SceneEditor, it.key, it.value, view)
        }
    }.sortedBy { -it.view.zOrder }

    val sceneListeners = mutableListOf<SceneListener>()

    /**
     * Currently, the [SceneEditor] contains a single view of the [Scene],
     * but at a later date, we may implement a SplitPane, so that we can see two different views of the same Scene.
     */
    val sceneView = SceneView(this)

    val statusBar = ToolBar()

    val borderPane = BorderPane().apply {
        center = sceneView
        bottom = statusBar
    }

    val actorsBox = ActorsBox(this)

    private val costumeHistory = mutableListOf<Costume>()

    val actionGroup = ActionGroup().apply {
        toggle(SNAP_TO_GRID_TOGGLE, snapPreferences.snapToGrid.enabledProperty)
        toggle(SNAP_TO_GUIDES_TOGGLE, snapPreferences.snapToGuides.enabledProperty)
        toggle(SNAP_TO_OTHERS_TOGGLE, snapPreferences.snapToOthers.enabledProperty)
        toggle(SNAP_ROTATION_TOGGLE, snapPreferences.snapRotation.enabledProperty)

        action(CUT) { onCut() }
        action(COPY) { onCopy() }
        action(PASTE) { onPaste() }
        action(DUPLICATE) { onDuplicate() }

        action(UNDO) { onUndo() }
        action(REDO) { onRedo() }

        action(ZOOM_RESET) { sceneView.resetZoom() }
        action(ZOOM_IN1) { sceneView.zoomIn() }
        action(ZOOM_IN2) { sceneView.zoomIn() }
        action(ZOOM_OUT) { sceneView.zoomOut() }

        action(RESET_ZORDERS) { onResetZOrders() }

        STAMPS.forEachIndexed { index, action ->
            action(action) { selectCostumeFromHistory(index) }
        }

        autoAttach(borderPane)
    }

    val toggleGridButton = actionGroup.toggleButton(SNAP_TO_GRID_TOGGLE)
    val toggleGuidesButton = actionGroup.toggleButton(SNAP_TO_GUIDES_TOGGLE)
    val toggleSnapToOthersButton = actionGroup.toggleButton(SNAP_TO_OTHERS_TOGGLE)
    val toggleSnapRotationButton = actionGroup.toggleButton(SNAP_ROTATION_TOGGLE)
    val singleStageButton: MenuButton = MenuButton("").apply {
        graphic = ImageView(Editor.imageResource("layers.png"))
        items.add(createSelectLayerMenuItem(null))
        items.add(SeparatorMenuItem())
        for (es in editingStages) {
            items.add(createSelectLayerMenuItem(es))
        }
    }

    val singleStageProperty: ObjectProperty<EditingStage?> = SimpleObjectProperty<EditingStage?>(null)
    var singleStage: EditingStage?
        get() = singleStageProperty.value
        set(v) {
            singleStageProperty.value = v
        }

    init {

        with(snapPreferences) {
            snapRotation.enabledProperty.addListener { _, _, _ -> firePreferencesChanged() }
            snapToGrid.enabledProperty.addListener { _, _, _ -> firePreferencesChanged() }
            snapToOthers.enabledProperty.addListener { _, _, _ -> firePreferencesChanged() }
            snapToGuides.enabledProperty.addListener { _, _, _ -> firePreferencesChanged() }
        }
        populateStatusBar()
        updateSingleStageButton()
    }

    fun build(): Node {
        actorsBox.build()
        return borderPane
    }

    fun createSelectLayerMenuItem(editingStage: EditingStage?) =
        MenuItem(editingStage?.stageName ?: "All Stages").apply {
            onAction = EventHandler {
                for (es in editingStages) {
                    val newValue = !(editingStage == null || es === editingStage)
                    if (es.isLocked != newValue) {
                        es.isLocked = newValue
                        fireStageChanged(es)
                    }
                }
            }
        }


    fun populateStatusBar() {
        statusBar.items.addAll(
            formattedTextFlow(sceneView.tipProperty).alignMiddle(), // Tip "label"

            expandH(),

            Separator(),
            createPositionIndicator(), // X,Y
            createZoomIndicator() // Zoom
        )
    }

    private fun createPositionIndicator(): Node {
        return VBox().apply {
            prefWidth = 60.0
            children.addAll(
                HBox().apply {
                    isFillWidth = true
                    prefWidth = 60.0
                    children.addAll(
                        Label("X:").apply { HBox.getHgrow(this) },
                        Label("0").apply {
                            sceneView.mouseXProperty.addListener { _, _, value ->
                                text = posFormat.format(value)
                            }
                        }
                    )
                },
                HBox().apply {
                    isFillWidth = true
                    children.addAll(
                        Label("Y:").apply { HBox.getHgrow(this) },
                        Label("0").apply {
                            sceneView.mouseYProperty.addListener { _, _, value ->
                                text = posFormat.format(value)
                            }
                        }
                    )
                }
            )
        }
    }

    /**
     * Note, steps should be the same as in [SceneView.zoomIn] and [SceneView.zoomOut].
     */
    private fun createZoomIndicator(): Node {
        return HBox().apply {
            spacing = 4.0
            alignment = Pos.CENTER
            isFillHeight = true
            children.addAll(
                Label("Z %"),
                Spinner<Float>().apply {
                    isFillHeight = true
                    isEditable = true
                    editor.prefColumnCount = 5
                    valueFactory = object : SpinnerValueFactory<Float>() {
                        var updating = false

                        init {
                            value = 100f
                            valueProperty().addListener { _, _, percentage ->
                                if (!updating) {
                                    sceneView.scale = percentage / 100f
                                }
                            }
                            sceneView.scaleProperty.addListener { _, _, scale ->
                                updating = true
                                value = Math.round(scale.toFloat() * 100f).toFloat()
                                updating = false
                            }
                            converter = object : StringConverter<Float>() {
                                override fun fromString(string: String) = string.toFloatOrNull() ?: 100f
                                override fun toString(value: Float?) = value.toString()
                            }
                            editor.textProperty().addListener { _, _, str ->
                                value = converter.fromString(str)
                            }
                        }

                        override fun decrement(steps: Int) {
                            if (value < 2) {
                                value = 1f
                            } else {
                                value -= if (value <= 10f) {
                                    1f
                                } else if (value <= 100f) {
                                    10f
                                } else if (value <= 200f) {
                                    20f
                                } else {
                                    50f
                                }
                            }
                        }

                        override fun increment(steps: Int) {
                            if (value < 10f) {
                                value = 10f
                            } else {
                                value += if (value >= 199f) {
                                    50f
                                } else if (value >= 99f) {
                                    20f
                                } else {
                                    10f
                                }
                            }
                        }
                    }
                }
            )
        }
    }


    fun fireStageChanged(editingStage: EditingStage) {
        for (l in sceneListeners) {
            l.stageChanged(editingStage)
        }
        updateSingleStageButton()
        sceneView.redraw()
    }

    private fun updateSingleStageButton() {
        var unlockedES: EditingStage? = null
        for (es in editingStages) {
            if (!es.isLocked) {
                if (unlockedES == null) {
                    unlockedES = es
                } else {
                    singleStageButton.text = "Multi-Stage Mode"
                    return
                }
            }
        }
        singleStageButton.text = unlockedES?.stageName ?: "No Stages"
    }

    fun firePreferencesChanged() {
        for (listener in sceneListeners) {
            listener.preferencesUpdated()
        }
        sceneView.redraw()
    }

    fun fireActorsChanged(actors: List<Actor>, modificationType: ModificationType) {
        for (listener in sceneListeners) {
            for (actor in actors) {
                listener.actorModified(scene, actor, modificationType)
            }
        }
        sceneView.redraw()
    }


    fun cleanUp() {
        sceneView.selection.clear() // Will clear the "Properties" box.
    }

    /**
     * Returns a [SelectedActor] from an [Actor], or null if the [EditingStage] cannot be found.
     */
    fun selectedActor(actor: Actor): SelectedActor? {
        editingStages.firstOrNull { it.stage === actor.stage }?.let {
            return SelectedActor(actor, it)
        }
        return null
    }

    fun findActorByName(name: String): Actor? {
        for (es in editingStages) {
            val actor = es.stage.findActorByName(name)
            if (actor != null) return actor
        }
        return null
    }

    private fun onUndo() {
        if (history.canUndo()) {
            history.undo()
        }
    }

    private fun onRedo() {
        if (history.canRedo()) {
            history.redo()
        }
    }

    fun isAutoPositionEnabled(actor: Actor): Boolean {
        val es = editingStages.firstOrNull { it.stage === actor.stage } ?: return false
        val layout = Editor.resources.layouts.find(scene.layoutName) ?: return false
        for (view in layout.layoutViewsForStage(es.stageName)) {
            if (view.position.enableAutoPosition) return true
        }
        return false
    }

    /**
     * Cuts the contents of [SceneView.selection].
     */
    fun onCut() {
        history.beginBatch()
        onCopy()
        deleteActors()
        history.endBatch()
    }

    /**
     * Copies the contents of [SceneView.selection].
     */
    fun onCopy() {
        clipboardActors.clear()
        for (sa in sceneView.selection.items) {
            clipboardActors.add(sa.copy())
        }
    }

    /**
     * Pastes the contents of [SceneView.selection].
     */
    fun onPaste() {
        sceneView.selection.clear()
        history.beginBatch()
        for (sa in clipboardActors) {
            val newSelectedActor = sa.copy()

            val change = AddActor(newSelectedActor)
            sceneView.selection.add(newSelectedActor)
            history.makeChange(change)

        }
        history.endBatch()
    }

    /**
     * Performs [onCopy] and [onPaste]
     */
    fun onDuplicate() {
        onCopy()
        onPaste()
    }

    fun deleteActors() {
        history.beginBatch()
        history.makeChange(DeleteActors(sceneView.selection.items))
        history.endBatch()
        sceneView.selection.clear()
    }

    private fun onResetZOrders() {
        val changedActors = mutableListOf<Actor>()
        history.beginBatch()
        for (es in editingStages) {
            for (actor in es.stage.actors) {
                val costume = actor.costume
                if (actor.zOrder != costume.zOrder) {
                    changedActors.add(actor)
                    history.makeChange(ChangeZOrder(actor, costume.zOrder))
                }
            }
        }
        history.endBatch()
        fireActorsChanged(changedActors, ModificationType.CHANGE)
    }


    fun selectCostume(costume: Costume) {
        sceneView.stampCostume = costume
        if (costumeHistory.isEmpty() || costumeHistory[0] !== costume) {
            costumeHistory.add(0, costume)
            if (costumeHistory.size > 10) {
                costumeHistory.removeAt(costumeHistory.size - 1)
            }
        }
    }

    private fun selectCostumeFromHistory(index: Int) {
        if (index >= 0 && index < costumeHistory.size) {
            sceneView.stampCostume = costumeHistory[index]
        }
    }

    private val adjustments = mutableListOf<SnapToAdjustment>()

    companion object {

        /**
         * Used by [onCopy] and [onPaste].
         * There is ONE instance, so that we can copy/paste between scenes.
         * Care must be taken to set the layer correctly when pasting,
         * so that it uses the layer of the destination scene.
         */
        var clipboardActors = mutableListOf<SelectedActor>()

        private val posFormat = DecimalFormat("0.0")
    }

}

fun SelectedActor.copy() = SelectedActor(actor.copy(), editingStage)

fun Actor.costumeName(): String? = Editor.resources.findNameOrNull(costume)

fun Actor.copy(): Actor {
    val result = Actor(costume, role)
    result.x = x
    result.y = y
    result.tint = tint
    result.name = name
    result.isLocked = isLocked
    result.autoPositionAlignment.set(autoPositionAlignment)
    result.zOrder = zOrder
    result.direction = direction
    result.scale.set(scale)

    when (val appearance = this.appearance) {

        is PoseAppearance -> {
            result.appearance = PoseAppearance(result, appearance.pose)
        }

        is TextAppearance -> {
            result.appearance = TextAppearance(result, appearance.text, appearance.textStyle)
        }

        is NinePatchAppearance -> {
            result.appearance = NinePatchAppearance(result, appearance.pose, appearance.margins)
        }

        is TiledAppearance -> {
            result.appearance = TiledAppearance(result, appearance.pose)
        }
    }
    return result
}

