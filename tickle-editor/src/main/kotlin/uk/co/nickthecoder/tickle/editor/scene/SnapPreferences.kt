package uk.co.nickthecoder.tickle.editor.scene

import uk.co.nickthecoder.tickle.Scene

/**
 * [SnapPreferences] are saved as part of a [Scene].
 */
class SnapPreferences {

    val snapToGrid = SnapToGrid()

    val snapToGuides = SnapToGuides()

    val snapToOthers = SnapToOthers()

    val snapRotation = SnapRotation()

}