package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Role
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.Polar2d
import uk.co.nickthecoder.tickle.util.Vector2
import java.lang.reflect.Field

/**
 * When we change an @Attribute field on Role.
 */
class ChangeRoleAttribute(val role: Role, val field: Field, val oldValue: Any?, var newValue: Any?) : Change {

    override fun redo(sceneEditor: SceneEditor) {
        setField(newValue)
        sceneEditor.fireActorsChanged(listOf(role.actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        setField(oldValue)
        sceneEditor.fireActorsChanged(listOf(role.actor), ModificationType.CHANGE)
    }

    private fun setField(value: Any?) {
        when (value) {
            is Vector2 -> (field.get(role) as Vector2).set(value)
            is Angle -> (field.get(role) as Angle).set(value)
            is Polar2d -> (field.get(role) as Polar2d).set(value)
            is Color -> (field.get(role) as Color).set(value)

            else -> {
                field.set(role, value)
            }
        }
    }

    override fun mergeWith(other: Change): Boolean {
        if (other is ChangeRoleAttribute && other.role === role && other.field === field) {
            other.newValue = newValue
            return true
        }
        return false
    }
}
