/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import uk.co.nickthecoder.paratask.util.requestFocusWhenSceneSet
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.code.CodeEditor
import uk.co.nickthecoder.tickle.editor.resources.MacroStub
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.resources.ScriptStub
import uk.co.nickthecoder.tickle.resources.ResourcesListener
import uk.co.nickthecoder.tickle.scripts.ScriptException
import java.io.File

open class ScriptTab(val file: File, data: Any) : EditTab(
    file.nameWithoutExtension,
    data,
    (if (data is MacroStub) ResourceType.MACRO else ResourceType.SCRIPT).createImageView()
), ResourcesListener, HasExtras {

    constructor(data: ScriptStub) : this(data.file, data)

    protected val codeEditor = CodeEditor()

    var scriptSub = data as? ScriptStub

    init {
        codeEditor.attachSyntaxHighlighter(file.extension)
    }

    private var hasErrors: Boolean = false
        set(v) {
            field = v
            if (v) {
                if (!styleClass.contains("errors")) styleClass.add("errors")
            } else {
                styleClass.remove("errors")
            }
        }

    init {
        borderPane.bottom = null // Don't show ANY of the buttons.

        borderPane.center = codeEditor.borderPane
        Editor.resources.listeners.add(this)

        if (file.exists() && file.isFile) {
            codeEditor.load(file)
        }
        codeEditor.scarea.requestFocusWhenSceneSet()

        codeEditor.scarea.document.textProperty().addListener { _ ->
            needsSaving = true
        }
        needsSaving = false
    }

    fun highlightError(e: ScriptException) {
        hasErrors = true
        codeEditor.highlightError(e)
    }

    fun hideError() {
        hasErrors = false
        codeEditor.hideError()
    }

    override fun justSave(): Boolean {
        saveCode()
        /*
        try {
            ScriptManager.load(file)
            hasErrors = false
            codeEditor.hideError()
        } catch (e: ScriptException) {
            highlightError(e)
        }
        */
        return true
    }

    /**
     * Saves the file, but doesn't reload the script. Any coding errors will NOT be highlighted.
     */
    fun saveCode() {
        codeEditor.save(file)
        needsSaving = false
    }

    override fun resourceRemoved(resource: Any, name: String) {
        if (resource == data) {
            close()
        }
    }
}
