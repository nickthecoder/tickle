/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.parameters

import javafx.stage.Stage
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.tasks.AttributesTask
import uk.co.nickthecoder.tickle.editor.tasks.NewResourceTask
import uk.co.nickthecoder.tickle.editor.util.ClassLister
import uk.co.nickthecoder.tickle.editor.util.DesignAttributeData
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import java.io.File

class ClassAndAttributesParameter(
    name: String,
    private val klass: Class<*>,
    private val isAlternate: Boolean = false,
    value: Class<*>?

) : SimpleGroupParameter(name) {

    val classP = GroupedChoiceParameter<Class<*>>(name + "_class", value = klass)

    private val newButtonP = ButtonParameter("${name}New", buttonText = "New ${klass.simpleName}") { newScript() }

    private val attributesP = ButtonParameter(name + "_attributes", buttonText = "Attributes") { editAttributes() }

    var attributes: DesignAttributes? = null
        set(v) {
            field = v
            v?.let { attributes ->
                attributes.updateAttributesMetaData((classP.value ?: klass).name)
                attributesP.hidden = attributes.data().firstOrNull { it.isAlternate == isAlternate && (it as DesignAttributeData).parameter != null } == null
            }
        }

    var classString: String
        get() {
            return if (classP.value == null) {
                ""
            } else {
                ScriptManager.nameForClass(classP.value!!)
            }
        }
        set(v) {
            try {
                classP.value = ScriptManager.classForName(v)
            } catch (_: Exception) {
            }
        }


    val scriptFile: File?
        get() {
            classP.value?.let {
                return ScriptManager.scriptFile(it.name)
            }
            return null
        }

    private var changeAction: (() -> Unit)? = null

    fun onAttributesChanged(action: () -> Unit) {
        changeAction = action
    }

    init {
        classP.value = value
        addParameters(classP, newButtonP, attributesP)
        attributesP.hidden = true
        asHorizontal(labelPosition = LabelPosition.NONE)
        ClassLister.setChoices(classP, klass)
        classP.listen {
            attributes?.let { attributes ->
                attributes.updateAttributesMetaData((classP.value ?: klass).name)
                attributesP.hidden = attributes.data().firstOrNull { it.isAlternate == isAlternate && (it as DesignAttributeData).parameter != null } == null
            }
        }
    }

    private fun newScript() {
        val task = NewResourceTask(resourceType = ResourceType.SCRIPT, newScriptType = klass)
        task.taskRunner.listen { cancelled ->
            if (!cancelled) {
                try {
                    ClassLister.setChoices(classP, klass)
                    classP.value = ScriptManager.classForName(task.nameP.value)
                } catch (_: ClassNotFoundException) {
                }
            }
        }
        TaskPrompter(task).placeOnStage(Stage())
    }

    private fun editAttributes() {
        val task = AttributesTask(attributes!!, isAlternate)
        changeAction?.let { task.onChange(it) }
        val prompter = TaskPrompter(task)
        prompter.placeOnStage(Stage())
    }
}
