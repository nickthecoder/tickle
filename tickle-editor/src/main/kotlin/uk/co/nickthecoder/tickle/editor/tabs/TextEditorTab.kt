/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import uk.co.nickthecoder.paratask.util.requestFocusWhenSceneSet
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.code.CodeEditor
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import java.io.File

class TextEditorTab(val file: File)

    : EditTab(file.nameWithoutExtension, file, ResourceType.SNIPPET.createImageView()) {

    private val codeEditor = CodeEditor()

    private var hasErrors: Boolean = false
        set(v) {
            field = v
            if (v) {
                if (!styleClass.contains("errors")) styleClass.add("errors")
            } else {
                styleClass.remove("errors")
            }
        }

    init {
        applyButton.text = "Save"
        okButton.isVisible = false

        borderPane.center = codeEditor.borderPane
        Editor.resources.listeners.add(this)

        codeEditor.load(file)
        codeEditor.scarea.requestFocusWhenSceneSet()

        codeEditor.scarea.document.textProperty().addListener { _ ->
            needsSaving = true
        }
        needsSaving = false
    }

    override fun justSave(): Boolean {
        codeEditor.save(file)
        return true
    }

}
