package uk.co.nickthecoder.tickle.editor.code

import org.antlr.v4.runtime.ParserRuleContext

class Operand(
        val startLine: Int,
        val startColumn: Int,
        val endLine: Int,
        val endColumn: Int,
        val type: TypeSense
) {
    constructor(ctx: ParserRuleContext, type: TypeSense) : this(
            ctx.start.line, ctx.start.charPositionInLine,
            ctx.stop.line, ctx.stop.charPositionInLine,
            type
    )


}
