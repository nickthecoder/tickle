/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.ToggleButton
import javafx.scene.image.ImageView
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import uk.co.nickthecoder.tickle.editor.DOCKABLE_STAGES
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.SceneListener
import uk.co.nickthecoder.tickle.editor.scene.EditingStage
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.tabs.SceneTab

class StagesDockable
    : TickleDockable(DOCKABLE_STAGES) {

    private val NONE = Label("N/A")

    internal var stagesBox: StagesBox? = null

    override val dockableContent = SimpleObjectProperty<Node>(NONE)

    init {
        MainWindow.instance.tabPane.selectionModel.selectedItemProperty().addListener { _, _, _ ->
            tabChanged()
        }
        tabChanged()
    }

    private fun tabChanged() {
        val tab = MainWindow.instance.tabPane.selectionModel.selectedItem
        stagesBox?.unregister()

        if (tab is SceneTab) {
            stagesBox = StagesBox(tab.sceneEditor).apply {
                dockableContent.set(build())
            }
        } else {
            stagesBox = null
            dockableContent.set(NONE)
        }
    }
}

class StagesBox(val sceneEditor: SceneEditor) : SceneListener {

    val grid = GridPane()

    init {
        sceneEditor.sceneListeners.add(this)
    }

    fun unregister() {
        sceneEditor.sceneListeners.remove(this)
    }

    fun build(): Node {
        grid.vgap = 6.0
        grid.hgap = 6.0

        val visibilityCC = ColumnConstraints()
        val labelCC = ColumnConstraints()
        val lockedCC = ColumnConstraints()

        labelCC.hgrow = Priority.ALWAYS
        grid.columnConstraints.addAll(visibilityCC, labelCC, lockedCC)

        update()
        return grid
    }


    override fun stageChanged(es: EditingStage) {
        update()
    }

    fun update() {

        val visibleImage = Editor.imageResource("layer-visible.png")
        val invisibleImage = Editor.imageResource("layer-hidden.png")

        val lockedImage = Editor.imageResource("layer-locked.png")
        val unlockedImage = Editor.imageResource("layer-unlocked.png")

        grid.children.clear()

        for ((y, es) in sceneEditor.editingStages.withIndex()) {
            val label = Label(es.stageName)

            val visibility = ToggleButton()
            visibility.isSelected = es.isVisible
            visibility.graphic = ImageView(if (es.isVisible) visibleImage else invisibleImage)
            visibility.onAction = EventHandler {
                es.isVisible = visibility.isSelected
                visibility.graphic = ImageView(if (es.isVisible) visibleImage else invisibleImage)
                sceneEditor.fireStageChanged(es)
            }

            val locked = ToggleButton()
            locked.isSelected = es.isLocked
            locked.graphic = ImageView(if (es.isLocked) lockedImage else unlockedImage)
            locked.onAction = EventHandler {
                es.isLocked = locked.isSelected
                locked.graphic = ImageView(if (es.isLocked) lockedImage else unlockedImage)
                sceneEditor.fireStageChanged(es)
            }

            grid.children.addAll(label, visibility, locked)

            GridPane.setConstraints(visibility, 0, y)
            GridPane.setConstraints(label, 1, y)
            GridPane.setConstraints(locked, 2, y)
        }
    }
}
