/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.util.toTickle
import uk.co.nickthecoder.tickle.util.Vector2
import uk.co.nickthecoder.tickle.warn

/**
 * When we change an Actor via the ActorAttributesForm.
 *
 * This does NOT include the @Attributes of Role, nor the TextStyle.
 */
class ChangeActorAttribute(
    private val actor: Actor,
    private val parameterName: String,
    private val oldValue: Any?,
    private var newValue: Any?
) : Change {

    override fun redo(sceneEditor: SceneEditor) {
        setValue(newValue)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        setValue(newValue)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    private fun setValue(value: Any?) {
        when (parameterName) {
            "name" -> actor.name = value as String
            "position" -> actor.position.set(value as Vector2)
            "zOrder" -> actor.zOrder = value as Float
            "autoPositionAlignment" -> actor.autoPositionAlignment.set(value as Vector2)
            "direction" -> actor.direction.degrees = (value as Double)
            "scale" -> actor.scale.set(value as Vector2)
            "color" -> actor.tint.set((value as javafx.scene.paint.Color).toTickle())
            "size" -> actor.resizeAppearance?.size?.set(value as Vector2)
            "resizeableAlignment" -> actor.resizeAppearance?.sizeAlignment?.set(value as Vector2)
            "text" -> actor.textAppearance?.let { it.text = value as String }
            else -> warn("Unexpected Actor Parameter $parameterName ")
        }
    }

    override fun mergeWith(other: Change): Boolean {
        if (other is ChangeActorAttribute && other.actor === actor && other.parameterName == parameterName) {
            other.newValue = newValue
            return true
        }
        return false
    }
}
