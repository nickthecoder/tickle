package uk.co.nickthecoder.tickle.editor.util

import com.eclipsesource.json.WriterConfig
import javafx.geometry.Side
import uk.co.nickthecoder.harbourfx.Harbour
import uk.co.nickthecoder.tickle.GameInfo
import uk.co.nickthecoder.tickle.editor.*
import uk.co.nickthecoder.tickle.editor.resources.MacroStub
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.resources.SceneStub
import uk.co.nickthecoder.tickle.editor.resources.ScriptStub
import uk.co.nickthecoder.tickle.editor.tabs.EditTab
import uk.co.nickthecoder.tickle.graphics.Colors
import uk.co.nickthecoder.tickle.graphics.Gradient
import uk.co.nickthecoder.tickle.graphics.GradientAlong
import uk.co.nickthecoder.tickle.path.EllipticalArc
import uk.co.nickthecoder.tickle.path.VisibleShape
import uk.co.nickthecoder.tickle.severe
import uk.co.nickthecoder.tickle.util.AutoFlushPreferences
import uk.co.nickthecoder.tickle.util.ImmutableVector2
import uk.co.nickthecoder.tickle.util.Vector2
import java.util.prefs.Preferences

object EditorPreferences {

    var outputFormat: JsonFormat = JsonFormat.PRETTY

    var treeThumbnailSize: Int = 24

    var costumePickerThumbnailSize: Int = 40

    /**
     * The size of the scene used by the editor's MainWindow.
     * This allows the window to be the same size as the last time the editor was run.
     * Note, the size and isMaximized are NOT shown in the EditorPreferencesTask, and therefore
     * cannot be edited directly by the user.
     */
    var windowWidth = 1000.0

    var windowHeight = 600.0

    /**
     * Is the editor's MainWindow maximized? When true, the windowWidth and windowHeight are NOT
     * updated when the window closes
     */
    var isMaximized = false

    /**
     * Should the GUI capture `System.out`, into the "log" Dockable.
     * If false, then stdout will remain as is (not viewable from the Editor).
     */
    var captureStdOut = true

    /**
     * Should the GUI capture `System.err`, into the "log" Dockable.
     * If false, then stderr will remain as is (not viewable from the Editor).
     */
    var captureStdErr = true

    /**
     * API documentation can either use Javadoc or Dokka.
     */
    var tickleApiUseJavadocs: Boolean = true

    /**
     * The URL for the Tickle API Javadoc documentation.
     */
    var tickleJavadocURL: String = "http://nickthecoder.co.uk/public/software/tickle-${GameInfo.tickleVersion}-javadoc/"

    /**
     * The URL for the Tickle API Dokka documentation.
     */
    var tickleDokkaURL: String = "http://nickthecoder.co.uk/public/software/tickle-${GameInfo.tickleVersion}-dokka/"


    var jomlApiURL: String = "https://joml-ci.github.io/JOML/apidocs/"

    /**
     * The tab to show when the SnapEditor is next shown.
     */
    var defaultPreferencesTabIndex = 0

    var sceneEditorPreferences = SceneEditorPreferences()


    override fun toString(): String {
        return "EditorPreferences outputFormat=$outputFormat"
    }

    enum class JsonFormat(val writerConfig: WriterConfig) {
        COMPACT(WriterConfig.MINIMAL),
        PRETTY(WriterConfig.PRETTY_PRINT)
    }


    /**
     * Loads preference ( from [Preferences], not from a file ).
     * Currently, preferences include the position of dockable items in the [Harbour] as well as which tabs
     * are open.
     *
     * Called when the application first starts, as well as when the "Reload" button is pressed.
     * (the latter passes [includeHarbour]=false).
     */
    fun load(includeHarbour: Boolean = true) {
        val topLevel = Preferences.userNodeForPackage(Editor::class.java)

        loadPreferences(topLevel.node("preferences"))

        if (includeHarbour) {
            val harbour = MainWindow.instance.harbour
            if (topLevel.nodeExists("harbour")) {
                harbour.load(topLevel)
            } else {
                // Default dockable positions ...
                harbour.addDockable(DOCKABLE_RESOURCES.name, Side.LEFT, isMain = true, show = true)

                harbour.addDockable(DOCKABLE_COSTUMES.name, Side.LEFT, isMain = false, show = false)
                harbour.addDockable(DOCKABLE_SCENES.name, Side.LEFT, isMain = false, show = false)
                harbour.addDockable(DOCKABLE_SCRIPTS.name, Side.LEFT, isMain = false, show = false)

                harbour.addDockable(DOCKABLE_COSTUME_PICKER.name, Side.RIGHT, isMain = true, show = false)
                harbour.addDockable(DOCKABLE_FILES.name, Side.RIGHT, isMain = true, show = false)
                harbour.addDockable(DOCKABLE_API.name, Side.RIGHT, isMain = true, show = false)
                harbour.addDockable(DOCKABLE_SNIPPETS.name, Side.RIGHT, isMain = true, show = false)

                harbour.addDockable(DOCKABLE_ACTOR_ATTRIBUTES.name, Side.RIGHT, isMain = false, show = false)
                harbour.addDockable(DOCKABLE_STAGES.name, Side.RIGHT, isMain = false, show = false)
                harbour.addDockable(DOCKABLE_ACTORS.name, Side.RIGHT, isMain = false, show = false)

                harbour.addDockable(DOCKABLE_LOG.name, Side.BOTTOM, isMain = true, show = false)

                harbour.setDividerPosition(Side.LEFT, 0.2)
                harbour.setDividerPosition(Side.RIGHT, 0.15)
            }
        }

        loadTabs(topLevel.node("game").node(Editor.resources.gameInfo.id))
    }

    fun save() {
        val topLevel = Preferences.userNodeForPackage(Editor::class.java)

        savePreferences(topLevel.node("preferences"))
        MainWindow.instance.harbour.save(topLevel)
        val gameNode = topLevel.node("game")
        saveTabs(gameNode.node(Editor.resources.gameInfo.id))
        gameNode.flush()
    }

    private fun loadPreferences(preferences: Preferences) {

        with(preferences) {
            outputFormat = JsonFormat.valueOf(get("outputFormat", "PRETTY"))

            treeThumbnailSize = getInt("treeThumbnailSize", treeThumbnailSize)
            costumePickerThumbnailSize = getInt("costumePickerThumbnailSize", costumePickerThumbnailSize)

            isMaximized = getBoolean("isMaximized", isMaximized)
            windowWidth = getDouble("windowWidth", windowWidth)
            windowHeight = getDouble("windowHeight", windowHeight)

            captureStdOut = getBoolean("captureStdOut", captureStdOut)
            captureStdErr = getBoolean("captureStdErr", captureStdErr)

            tickleDokkaURL = get("apiURL", tickleDokkaURL)
            jomlApiURL = get("jomlApiURL", jomlApiURL)
            tickleApiUseJavadocs = getBoolean("useJavadocs", tickleApiUseJavadocs)
            tickleJavadocURL = get("tickleJavadocURL", tickleJavadocURL)
            tickleDokkaURL = get("tickleDokkaURL", tickleDokkaURL)

            defaultPreferencesTabIndex = getInt("preferencesTabIndex", 0)
        }

        with(sceneEditorPreferences) {
            with(AutoFlushPreferences(preferences.node("sceneEditor"))) {
                selectionLineThickness = getFloat("selectionLineThickness", 3f)
                guidesThickness = getFloat("guidesThickness", 2f)
                screenSizeThickness = getFloat("screenSizeThickness", 2f)
                handleSize = getFloat("handleSize", 6f)

                getString("screenSizeColor")?.let { screenSizeColor = Colors[it] }
                getString("guideColor")?.let { guideColor = Colors[it] }
                getString("highlightGuideColor")?.let { highlightGuideColor = Colors[it] }
                getString("selectionColor")?.let { selectionColor = Colors[it] }
                getString("latestSelectionColor")?.let { latestSelectionColor = Colors[it] }
            }
        }
    }

    private fun savePreferences(preferences: Preferences) {

        with(preferences) {
            put("outputFormat", outputFormat.name)

            putInt("treeThumbnailSize", treeThumbnailSize)
            putInt("costumePickerThumbnailSize", costumePickerThumbnailSize)

            putBoolean("isMaximized", isMaximized)
            putDouble("windowWidth", windowWidth)
            putDouble("windowHeight", windowHeight)

            putBoolean("captureStdOut", captureStdOut)
            putBoolean("captureStdErr", captureStdErr)

            put("apiURL", tickleDokkaURL)
            put("jomlApiURL", jomlApiURL)
            putBoolean("useJavadocs", tickleApiUseJavadocs)
            put("tickleJavadocURL", tickleJavadocURL)
            put("tickleDokkaURL", tickleDokkaURL)

            putInt("preferencesTabIndex", defaultPreferencesTabIndex)

            flush()
        }

        with(sceneEditorPreferences) {
            with(preferences.node("sceneEditor")) {
                putFloat("selectionLineThickness", selectionLineThickness)
                putFloat("guidesThickness", guidesThickness)
                putFloat("screenSizeThickness", screenSizeThickness)
                putFloat("handleSize", handleSize)

                put("screenSizeColor", screenSizeColor.toHashRGB())
                put("guideColor", guideColor.toHashRGB())
                put("highlightGuideColor", highlightGuideColor.toHashRGB())
                put("selectionColor", selectionColor.toHashRGB())
                put("latestSelectionColor", latestSelectionColor.toHashRGB())
                flush()
            }
        }
    }

    private fun loadTabs(editorP: Preferences) {
        val tabsP = editorP.node("tabs")
        for (childName in tabsP.childrenNames()) {
            val tabP = tabsP.node(childName)
            val name = tabP.get("name", null)
            val type = tabP.get("type", null)
            if (name != null && type != null) {
                val resourceType = ResourceType.valueOf(type)
                try {
                    MainWindow.instance.openNamedTab(name, resourceType)
                } catch (e: Exception) {
                    severe(e)
                }
            }
        }
    }

    private fun saveTabs(editorP: Preferences) {
        val tabsP = editorP.node("tabs")
        tabsP.childrenNames().forEach { nodeName ->
            tabsP.node(nodeName).removeNode()
        }
        tabsP.clear()
        var tabCounter = 0
        val tabs = MainWindow.instance.tabPane.tabs
        for (tab in tabs) {
            if (tab is EditTab) {
                val type = ResourceType.resourceType(tab.data)
                type?.let {

                    val name = when (tab.data) {
                        is SceneStub -> tab.data.name
                        is ScriptStub -> tab.data.name
                        is MacroStub -> tab.data.name
                        else -> tab.dataName
                    }

                    val tabP = tabsP.node("${tabCounter++}")
                    tabP.put("name", name)
                    tabP.put("type", type.name)
                    tabP.flush()
                }
            }
        }
        tabsP.flush()
    }

}

class SceneEditorPreferences {

    /**
     * Thickness of the dashed lines around selected Actors.
     */
    var selectionLineThickness = 3f

    /**
     * Thickness of the horizontal/vertical guides.
     */
    var guidesThickness = 2f

    /**
     * Thickness of lines which indicate the size of [GameInfo.width] and [GameInfo.height]
     */
    var screenSizeThickness = 2f

    /**
     * The size of the DragHandles (actually this is half their size!)
     */
    var handleSize = 6f
        set(v) {
            field = v
            handleSizeScale = ImmutableVector2(v / 6f, v / 6f)
        }

    /**
     * A scaling factor applied to the Pose used to draw the Drag Handles.
     * The Pose is 12x12 pixels, so the scaling factor is 1 when [handleSize] == 6
     */
    internal var handleSizeScale: ImmutableVector2 = ImmutableVector2(1f, 1f)
        private set

    /**
     * The color of the rectangle showing the size of [GameInfo.width], [GameInfo.height].
     */
    var screenSizeColor = Colors["#888"]

    /**
     * The color of horizontal/vertical guides
     */
    var guideColor = Colors["#449"]

    /**
     * The color of horizontal/vertical guides when you hover over them or drag them.
     */
    var highlightGuideColor = Colors["#ffc"]

    /**
     * The color of the rectangle around selected Actors (which are NOT the top-most).
     */
    var selectionColor = Colors["#990"]
        set(v) {
            field = v
            ::selectionDashed.markAsDirty()
            ::selectionFade.markAsDirty()
            ::selectionStrokePaint.markAsDirty()
            ::selectionRotateLine.markAsDirty()
            MainWindow.instance.firePreferencesChanged()
        }

    /**
     * The color of the rectangle around the top-most selected Actor.
     */
    var latestSelectionColor = Colors["#ffc"]
        set(v) {
            field = v
            ::latestDashed.markAsDirty()
            ::latestFade.markAsDirty()
            ::latestStrokePaint.markAsDirty()
            ::latestRotateLine.markAsDirty()
        }

    /**
     * A [Gradient] used to create a dashed line around the top-most selected Actor.
     */
    internal val latestDashed: Gradient by dirty {
        Gradient(Colors.BLACK, latestSelectionColor).apply {
            addStop(0.5f, Colors.TRANSPARENT_BLACK)
            addStop(0.501f, latestSelectionColor.transparent())
        }
    }

    /**
     * A [Gradient] used to create a dashed line around selected Actor, which are NOT top-most.
     */
    internal val selectionDashed: Gradient by dirty {
        Gradient(Colors.BLACK, selectionColor).apply {
            addStop(0.5f, Colors.TRANSPARENT_BLACK)
            addStop(0.501f, selectionColor.transparent())
        }
    }

    /**
     * A gradient from transparent to [latestSelectionColor].
     */
    val latestFade: Gradient by dirty { Gradient(Colors.TRANSPARENT, latestSelectionColor) }

    /**
     * A gradient from transparent to [selectionColor].
     */
    val selectionFade: Gradient by dirty { Gradient(Colors.TRANSPARENT, selectionColor) }

    /**
     * A [GradientAlong], with a [Gradient] from BLACK to [latestSelectionColor].
     */
    val latestStrokePaint: GradientAlong by dirty { GradientAlong(latestDashed, true) }

    /**
     * A [GradientAlong], with a [Gradient] from BLACK to [selectionColor].
     */
    val selectionStrokePaint: GradientAlong by dirty { GradientAlong(selectionDashed, true) }


    /**
     * This distance of the Rotation Handle from the Actor's position.
     */
    var rotateDistance = 100f
        set(v) {
            field = v
            ::latestRotateLine.markAsDirty()
            ::selectionRotateLine.markAsDirty()
        }

    /**
     * Creates an EllipticalArc for Rotation Handles.
     * See [latestRotateLine], [selectionRotateLine]
     */
    private fun createRotateArc(distance: Float): EllipticalArc {
        val innerDistance = distance - 10f
        val innerDistance45 = innerDistance / Math.sqrt(2.0).toFloat()
        return EllipticalArc(
            Vector2(innerDistance45, innerDistance45),
            Vector2(innerDistance45, -innerDistance45),
            Vector2(innerDistance, innerDistance),
            largeArc = false, sweepArc = false
        )
    }

    internal val latestRotateLine: VisibleShape by dirty {
        VisibleShape(
            createRotateArc(rotateDistance), thickness = 3f,
            strokePaint = GradientAlong(latestFade, isMirrored = true)
        )
    }

    internal val selectionRotateLine: VisibleShape by dirty {
        VisibleShape(
            createRotateArc(rotateDistance), thickness = 3f,
            strokePaint = GradientAlong(selectionFade, isMirrored = true)
        )
    }
}
