package uk.co.nickthecoder.tickle.editor.code

class Import(val line: Int, val name: String, val asName: String?)
