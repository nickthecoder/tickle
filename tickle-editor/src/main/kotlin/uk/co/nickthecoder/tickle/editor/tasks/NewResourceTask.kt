/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tasks

import javafx.stage.Stage
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParameterException
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.UnthreadedTaskRunner
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.CostumeGroup
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.Scene
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.parameters.FontParameter
import uk.co.nickthecoder.tickle.editor.parameters.createFontParameter
import uk.co.nickthecoder.tickle.editor.parameters.createPoseParameter
import uk.co.nickthecoder.tickle.editor.parameters.createTextureParameter
import uk.co.nickthecoder.tickle.editor.resources.*
import uk.co.nickthecoder.tickle.editor.scene.SnapPreferences
import uk.co.nickthecoder.tickle.editor.tabs.MacroTab
import uk.co.nickthecoder.tickle.events.CompoundInput
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.Layout
import uk.co.nickthecoder.tickle.resources.LayoutStage
import uk.co.nickthecoder.tickle.resources.LayoutView
import uk.co.nickthecoder.tickle.scripts.Language
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.sound.Sound
import java.io.File


class NewResourceTask(
    resourceType: ResourceType = ResourceType.ANY,
    defaultName: String = "",
    val newScriptType: Class<*>? = null
)

    : AbstractTask() {

    val nameP = StringParameter("name", label = "${resourceType.label} Name", value = defaultName)

    val textureP = FileParameter(
        "textureFile",
        label = "File",
        value = File(Editor.resources.file.parentFile, "images").absoluteFile
    )

    val poseP = createTextureParameter()

    val costumePoseP = createPoseParameter("costumePose", required = false)
    val costumeFontP = createFontParameter("costumeFont")

    val costumePoseOrFontP = OneOfParameter("newCostume", label = "From", choiceLabel = "From")
        .addChoices("Pose" to costumePoseP, "Font" to costumeFontP)

    val costumeP = SimpleGroupParameter("costume")
        .addParameters(costumePoseOrFontP, costumePoseP, costumeFontP)

    val costumeGroupP = InformationParameter("costumeGroup", information = "")

    val layoutP = InformationParameter("layout", information = "")

    val inputP = InformationParameter("input", information = "")

    val fontP = FontParameter("font")

    val sceneDirectoryInfoP = InformationParameter("sceneDirectory", information = "")

    val sceneDirectoryP = FileParameter(
        "scene",
        label = "Directory",
        expectFile = false,
        value = Editor.resources.sceneDirectory.absoluteFile
    )

    val soundP = FileParameter(
        "soundFile",
        label = "File",
        value = File(Editor.resources.file.parentFile, "sounds").absoluteFile,
        extensions = listOf("ogg")
    )

    val scriptLanguageP = ChoiceParameter<Language>("language")

    val macrosInfoP = InformationParameter("macros", information = "")

    init {
        ScriptManager.languages().forEach { language ->
            scriptLanguageP.addChoice(language.name, language, language.name)
            if (scriptLanguageP.value == null) {
                scriptLanguageP.value = language
            }
        }
        textureP.listen {
            val file = textureP.value
            if (file?.exists() == true && file.isFile && nameP.value.isBlank()) {
                nameP.value = file.nameWithoutExtension
            }
        }
    }

    val resourceTypeP = OneOfParameter("resourceType", label = "Resource", choiceLabel = "Type")


    override val taskD = TaskDescription(
        "newResource",
        label = "New ${resourceType.label}",
        height = if (resourceType == ResourceType.ANY) 300 else null
    )
        .addParameters(nameP, resourceTypeP)

    override val taskRunner = UnthreadedTaskRunner(this)

    constructor(pose: Pose, defaultName: String = "") : this(ResourceType.COSTUME, defaultName) {
        costumePoseOrFontP.value = costumePoseP
        costumePoseP.value = pose
    }

    constructor(fontResource: FontResource, defaultName: String = "") : this(ResourceType.COSTUME, defaultName) {
        costumePoseOrFontP.value = costumeFontP
        costumeFontP.value = fontResource
    }


    init {

        val parameter = when (resourceType) {
            ResourceType.TEXTURE -> textureP
            ResourceType.POSE -> poseP
            ResourceType.COSTUME -> costumeP
            ResourceType.COSTUME_GROUP -> costumeGroupP
            ResourceType.LAYOUT -> layoutP
            ResourceType.INPUT -> inputP
            ResourceType.FONT -> fontP
            ResourceType.SCENE_DIRECTORY -> sceneDirectoryInfoP
            ResourceType.SCENE -> sceneDirectoryP
            ResourceType.SOUND -> soundP
            ResourceType.SCRIPT -> scriptLanguageP
            ResourceType.MACRO -> macrosInfoP
            else -> null

        }
        if (parameter == null) {
            resourceTypeP.addChoices(
                "Texture" to textureP,
                "Pose" to poseP,
                "Font" to fontP,
                "Costume" to costumeP,
                "Costume Group" to costumeGroupP,
                "Layout" to layoutP,
                "Input" to inputP,
                "Scene Directory" to sceneDirectoryInfoP,
                "Scene" to sceneDirectoryP,
                "Sound" to soundP,
                "Script" to scriptLanguageP,
                "Macros" to macrosInfoP
            )

            taskD.addParameters(
                textureP,
                poseP,
                fontP,
                costumeP,
                costumeGroupP,
                layoutP,
                inputP,
                sceneDirectoryInfoP,
                sceneDirectoryP,
                soundP,
                scriptLanguageP,
                macrosInfoP
            )

        } else {
            resourceTypeP.hidden = true
            resourceTypeP.value = parameter
            if (parameter !is InformationParameter) {
                taskD.addParameters(parameter)
            }
        }
    }

    override fun customCheck() {
        val resources = Editor.resources
        val resourceType = when (resourceTypeP.value) {
            textureP -> resources.textures
            poseP -> resources.poses
            costumeP -> resources.costumes
            costumeGroupP -> resources.costumeGroups
            layoutP -> resources.layouts
            inputP -> resources.inputs
            fontP -> resources.fontResources
            soundP -> resources.sounds
            else -> null
        }

        if (resourceType != null) {
            if (resourceType.find(nameP.value) != null) {
                throw ParameterException(nameP, "Already exists")
            }
        }
        // TODO Do similar tests for sceneDirectory name, scene name and script name.
    }

    override fun run() {

        val name = nameP.value
        var data: Any? = null

        when (resourceTypeP.value) {
            textureP -> {
                data = onOpenGLThread {
                    Editor.resources.textures.add(name, Texture.create(textureP.value!!))
                }
            }

            poseP -> {
                val pose = Pose(poseP.value!!)
                Editor.resources.poses.add(name, pose)
                data = pose
            }

            costumeP -> {
                val costume = Costume(Editor.resources)
                if (costumePoseOrFontP.value == costumePoseP) {
                    costumePoseP.value?.let {
                        costume.addPose("default", it)
                    }
                } else if (costumePoseOrFontP.value == costumeFontP) {
                    costumeFontP.value?.let { font ->
                        val textStyle = TextStyle(font, TextHAlignment.LEFT, TextVAlignment.BOTTOM, Color.white())
                        costume.addTextStyle("default", textStyle)
                    }
                }
                Editor.resources.costumes.add(name, costume)
                data = costume
            }

            costumeGroupP -> {
                val costumeGroup = CostumeGroup(Editor.resources)
                Editor.resources.costumeGroups.add(name, costumeGroup)
                data = costumeGroup
            }

            layoutP -> {
                val layout = Layout(Editor.resources)
                layout.layoutStages["main"] = LayoutStage(Editor.resources)
                layout.layoutViews["main"] = LayoutView(Editor.resources, stageName = "main")
                Editor.resources.layouts.add(name, layout)
                data = layout
            }

            inputP -> {
                val input = CompoundInput()
                Editor.resources.inputs.add(name, input)
                data = input
            }

            sceneDirectoryInfoP -> {
                val dir = File(Editor.resources.sceneDirectory, name)
                dir.mkdir()
                Editor.resources.fireAdded(dir, name)
            }

            sceneDirectoryP -> {
                val dir = sceneDirectoryP.value!!
                val file = File(dir, "${name}.scene")
                val newScene = Scene()
                var layoutName: String? = "default"
                if (Editor.resources.layouts.find(layoutName!!) == null) {
                    layoutName = Editor.resources.layouts.items().keys.firstOrNull()
                    if (layoutName == null) {
                        throw ParameterException(
                            sceneDirectoryP,
                            "The resources have no layouts. Create a Layout before creating a Scene."
                        )
                    }
                }
                newScene.layoutName = layoutName
                SceneWriter.save(file, newScene, SnapPreferences(), Editor.resources)
                Editor.resources.fireAdded(SceneStub(file), name)
            }

            fontP -> {
                onOpenGLThread {
                    val fontResource = FontResource()
                    fontP.update(fontResource)
                    Editor.resources.fontResources.add(name, fontResource)
                    data = fontResource
                }
            }

            soundP -> {
                onOpenGLThread {
                    val sound = Sound(soundP.value!!)
                    Editor.resources.sounds.add(name, sound)
                    data = sound
                }
            }

            scriptLanguageP -> {
                scriptLanguageP.value?.createScript(Editor.resources.scriptDirectory(), name, newScriptType)
                    ?.let { file ->
                        data = ScriptStub(file)
                        Editor.resources.fireAdded(data!!, name)
                    }
            }

            macrosInfoP -> {
                val file = MacroTab.createMacro(Editor.resources.macroDirectory(), name)
                data = MacroStub(file)
                Editor.resources.fireAdded(data!!, name)
            }
        }

        data?.let {
            MainWindow.instance.openTab(name, it)
        }
    }

    fun prompt() {
        val taskPrompter = TaskPrompter(this)
        taskPrompter.placeOnStage(Stage())
    }

}
