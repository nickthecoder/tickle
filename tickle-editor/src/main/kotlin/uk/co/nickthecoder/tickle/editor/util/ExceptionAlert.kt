package uk.co.nickthecoder.tickle.editor.util

import javafx.scene.control.Alert
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import java.io.PrintWriter
import java.io.StringWriter

class ExceptionAlert(e: Exception) : Alert(AlertType.ERROR) {

    init {
        val sw = StringWriter()
        e.printStackTrace(PrintWriter(sw))
        val exceptionText = sw.toString()

        val textArea = TextArea(exceptionText).apply {
            isEditable = false
            maxWidth = java.lang.Double.MAX_VALUE
            maxHeight = java.lang.Double.MAX_VALUE
        }
        GridPane.setVgrow(textArea, Priority.ALWAYS)
        GridPane.setHgrow(textArea, Priority.ALWAYS)

        val label = Label("Stack trace : ")
        val expContent = GridPane()
        expContent.maxWidth = java.lang.Double.MAX_VALUE
        expContent.add(label, 0, 0)
        expContent.add(textArea, 0, 1)

        dialogPane.expandableContent = expContent
        dialogPane.isExpanded = true
    }
}
