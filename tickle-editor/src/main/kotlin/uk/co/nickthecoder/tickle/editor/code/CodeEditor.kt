/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.code

import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.control.ContextMenu
import javafx.scene.control.Label
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.scene.input.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import uk.co.nickthecoder.feather.internal.isStatic
import uk.co.nickthecoder.feather.syntax.FeatherSyntaxHighlighter
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import uk.co.nickthecoder.scarea.extra.ui.FindBar
import uk.co.nickthecoder.scarea.extra.ui.ReplaceBar
import uk.co.nickthecoder.scarea.extra.ui.ScareaMatcher
import uk.co.nickthecoder.scarea.extra.util.RemoveHiddenChildren
import uk.co.nickthecoder.scarea.syntax.HighlightMatchedPairs
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.ScriptStub
import uk.co.nickthecoder.tickle.editor.tabs.APITab
import uk.co.nickthecoder.tickle.editor.util.ClassMetaData
import uk.co.nickthecoder.tickle.scripts.ScriptException
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import java.io.File
import java.lang.reflect.Method
import java.util.regex.Pattern

class CodeEditor {

    val borderPane = BorderPane()

    val scarea = Scarea()

    private val error = Label("")

    private val findAndReplaceBox = VBox()

    private val matcher = ScareaMatcher(scarea)

    private val findBar = FindBar(matcher)
    private val replaceBar = ReplaceBar(matcher)

    init {
        with(scarea) {
            styleClass.add("code")
            HighlightMatchedPairs(scarea)
        }

        findBar.toolBar.styleClass.add(".bottom")
        replaceBar.toolBar.styleClass.add(".bottom")

        RemoveHiddenChildren(findAndReplaceBox.children)
        findAndReplaceBox.children.addAll(error, findBar.toolBar, replaceBar.toolBar)

        // Hides the find and replace toolbars.
        matcher.inUse = false

        with(borderPane) {
            center = scarea
            bottom = findAndReplaceBox
        }

        error.isVisible = false
        error.styleClass.add("code-error")

        borderPane.addEventFilter(KeyEvent.KEY_PRESSED) { onKeyPressed(it) }
        scarea.addEventFilter(MouseEvent.MOUSE_PRESSED) { onMousePressedRelease(it) }
        scarea.addEventFilter(MouseEvent.MOUSE_RELEASED) { onMousePressedRelease(it) }
    }

    fun attachSyntaxHighlighter( fileExtension : String ) {
        when (fileExtension) {
            "feather" -> FeatherSyntaxHighlighter.instance.attach(scarea)
        }
    }

    fun highlightError(e: ScriptException) {
        // e.printStackTrace()
        val line = e.line
        if (line != null) {
            error.onMouseClicked = EventHandler {
                scarea.requestFocus()
                Platform.runLater {
                    positionCaret(line, e.column)
                }
            }
            scarea.requestFocus()
            Platform.runLater {
                positionCaret(line, e.column)
            }
        } else {
            error.onMouseClicked = null
        }
        error.text = e.message
        error.isVisible = true
        scarea.requestFocus()
    }

    fun hideError() {
        error.isVisible = false
    }

    fun positionCaret(line: Int, column: Int?) {
        scarea.caretPosition = ScareaPosition(line, column ?: 0)
    }

    fun load(file: File) {
        scarea.text = file.readText()
    }

    fun save(file: File) {
        file.writeText(scarea.text)
    }

    private fun onMousePressedRelease(event: MouseEvent) {
        if (event.isPopupTrigger) {
            scarea.contextMenu = buildContextMenu(event)
            scarea.contextMenu?.let {
                scarea.contextMenu.show(scarea, event.screenX, event.screenY)
                event.consume()
            }
        }
    }

    private fun buildContextMenu(event: MouseEvent): ContextMenu? {
        val menu = ContextMenu()
        fun addSubMenu(item: Menu) {
            if (item.items.isNotEmpty()) {
                menu.items.add(item)
            }
        }

        val word = wordAt(event)
        if (word != null) {
            findClasses(word).firstOrNull()?.let { klass ->
                menu.items.add(MenuItem("API Ref : $klass.simpleName}").apply {
                    onAction = EventHandler {
                        val tabPane = MainWindow.instance.tabPane
                        val tab = APITab(klass.packageName, klass.simpleName)
                        tabPane.tabs.add(tab)
                        tabPane.selectionModel.select(tab)
                    }
                })
                createConstructorsMenu(klass)?.let { addSubMenu(it) }
                createFieldsMenu(klass, static = true)?.let { addSubMenu(it) }
                createMethodsMenu(klass, static = true)?.let { addSubMenu(it) }
                createFieldsMenu(klass, static = false)?.let { addSubMenu(it) }
                createMethodsMenu(klass, static = false)?.let { addSubMenu(it) }
            }

            ScriptManager.scriptFile(word)?.let { scriptFile ->
                menu.items.add(MenuItem("Open Script ${word}").apply {
                    onAction = EventHandler {
                        MainWindow.instance.openTab(word, ScriptStub(scriptFile))
                    }
                })
            }
        }
        return if (menu.items.isEmpty()) null else menu
    }

    private fun isGetter(method: Method): Boolean {
        val name = method.name
        return name.length > 3 && name.startsWith("get") && name[3].isUpperCase() && method.parameterCount == 0
    }

    private fun isGetterOrSetter(method: Method): Boolean {
        if (isGetter(method)) return true
        val name = method.name
        return name.length > 3 && name.startsWith("set") && name[3].isUpperCase() && method.parameterCount == 1
                && method.returnType != Void.TYPE
    }

    private fun getterName(methodName: String): String {
        return methodName[3].lowercase() + methodName.substring(4)
    }

    private fun createConstructorsMenu(klass: Class<*>): Menu? {
        val menu = Menu("Constructors")
        klass.constructors.forEach { constructor ->
            val args = constructor.parameterTypes.joinToString(separator = ",") { it.simpleName }
            val item = MenuItem(if (args.isEmpty()) "<no params>" else args)
            item.onAction = EventHandler {
                Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to "${klass.simpleName}($args)"))
            }
            menu.items.add(item)
        }
        return if (menu.items.isEmpty()) null else menu
    }

    private fun createFieldsMenu(klass: Class<*>, static: Boolean): Menu? {
        val menu = Menu(if (static) "Static Fields" else "Fields")
        val items = mutableListOf<MenuItem>()
        klass.fields.filter { it.isStatic() == static && it.type.simpleName != "Companion" }.forEach { field ->
            val item = MenuItem("${field.name} (${field.type.simpleName})")
            item.onAction = EventHandler {
                Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to field.name))
            }
            items.add(item)
        }
        klass.methods.filter { it.isStatic() == static && isGetter(it) && !it.name.contains("$") }.forEach { method ->
            val item = MenuItem("${getterName(method.name)} (${method.returnType.simpleName})")
            item.onAction = EventHandler {
                Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to method.name))
            }
            items.add(item)
        }
        menu.items.addAll(items.sortedBy { it.text })

        return if (menu.items.isEmpty()) null else menu
    }

    private fun createMethodsMenu(klass: Class<*>, static: Boolean): Menu? {
        val menu = Menu(if (static) "Static Methods" else "Methods")
        klass.methods.filter { it.isStatic() == static && !isGetterOrSetter(it) && !it.name.contains("$") }
            .sortedBy { it.name }
            .forEach { method ->
                val args =
                    method.parameterTypes.joinToString(prefix = " (", postfix = ")", separator = ",") { it.simpleName }
                val item = MenuItem(method.name + args)
                item.onAction = EventHandler {
                    Clipboard.getSystemClipboard().setContent(mapOf(DataFormat.PLAIN_TEXT to method.name))
                }
                menu.items.add(item)
            }
        return if (menu.items.isEmpty()) null else menu
    }

    private fun isImported(className: String): Boolean {
        val im = importPattern.matcher(scarea.text)
        while (im.find()) {
            val imp = im.group("IMPORT")
            if (imp == className || imp == "${packageName(className)}.*") {
                return true
            }
        }
        return false
    }

    private fun packageName(className: String) = className.substring(0, className.lastIndexOf('.'))

    private fun simpleName(className: String): String {
        val dot = className.lastIndexOf('.')
        return className.substring(dot + 1)
    }

    private fun findClassNames(word: String): List<String> {
        return ClassMetaData.simpleClassNameToNames[word] ?: emptyList()
    }

    private fun findClasses(word: String): List<Class<*>> {
        val names = ClassMetaData.simpleClassNameToNames[word] ?: return emptyList()
        val classes = mutableListOf<Class<*>>()
        for (name in names) {
            try {
                classes.add(Class.forName(name))
            } catch (e: Exception) {
                // Do nothing
            }
        }
        return classes
    }

    private fun wordAt(event: MouseEvent): String? {
        val pos = scarea.positionForPoint(event.x, event.y)
        val lineText = scarea.document.paragraphs[pos.line]
        // Find the word at the given point
        val matcher = wordPattern.matcher(lineText)

        while (matcher.find()) {
            if (matcher.start() <= pos.column && matcher.end() >= pos.column) {
                return lineText.substring(matcher.start(), matcher.end())
            }
        }
        return null
    }

    private fun onKeyPressed(event: KeyEvent) {
        var consume = true

        if (event.isControlDown) {
            when (event.code) {
                // Toggle Line Numbers
                KeyCode.L -> scarea.displayLineNumbers = !scarea.displayLineNumbers
                // FIND
                KeyCode.F -> {
                    matcher.inUse = true
                    findBar.find.requestFocus()
                }
                // Replace (R is already used for "Run")
                KeyCode.H -> {
                    val wasInUse = matcher.inUse
                    replaceBar.toolBar.isVisible = true
                    if (wasInUse) {
                        replaceBar.requestFocus()
                    }
                }

                else -> consume = false
            }
        } else {
            when (event.code) {
                KeyCode.ESCAPE -> {
                    matcher.inUse = false
                    scarea.requestFocus()
                }
                KeyCode.F3 -> {
                    with (findBar.matcher) {
                        if (event.isShiftDown) {
                            findBar.matcher.nextMatch(true)
                        } else {
                            findBar.matcher.previousMatch(true)
                        }
                    }
                }
                else -> consume = false
            }
        }

        if (consume) {
            event.consume()
        }
    }

    companion object {
        val wordPattern: Pattern = Pattern.compile("\\b\\w+?\\b")
        val packagePattern: Pattern = Pattern.compile("^\\spackage", Pattern.MULTILINE)
        val importPattern: Pattern = Pattern.compile("^\\s*import\\s(?<IMPORT>.*)\\s", Pattern.MULTILINE)
    }
}
