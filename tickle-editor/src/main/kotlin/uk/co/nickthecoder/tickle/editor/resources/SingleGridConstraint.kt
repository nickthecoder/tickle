package uk.co.nickthecoder.tickle.editor.resources

import uk.co.nickthecoder.tickle.editor.scene.SelectedActor
import uk.co.nickthecoder.tickle.editor.scene.history.Change

open class SingleGridConstraint : GridConstraint() {

    override fun addActor(sa: SelectedActor): List<Change> {
        /* TODO
            val otherActors = stageLayer.stageResource.actorResources.filter {
                it !== actorResource && it.x == actorResource.x && it.y == actorResource.y
            }

            return if (otherActors.isEmpty()) {
                super.addActorResource(actorResource)
            } else {
                val result = super.addActorResource(actorResource).toMutableList()
                result.add(DeleteActors(otherActors.filterIsInstance<DesignActorResource>()))
                result
            }
        */
        return emptyList()
    }

    override fun moveActor(sa: SelectedActor): List<Change> {
        /* TODO
        val otherActors = stageLayer.stageResource.actorResources.filter {
            it !== actor && it.x == actor.x && it.y == actor.y
        }

        return if (otherActors.isEmpty()) {
            super.moveActor(actor)
        } else {
            val result = super.moveActor(actor).toMutableList()
            result.add(DeleteActors(otherActors.filterIsInstance<DesignActorResource>()))
            result
        }
        */
        return emptyList()
    }
}
