package uk.co.nickthecoder.tickle.editor.resources

import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.dockable.ResourcesTree
import java.io.File


/**
 * I want a strongly typed value for the "resource" in [ResourcesTree.ScriptItem].
 */
class ScriptStub(val file: File) {

    val name : String
        get() = Editor.resources.toPath(file).removePrefix("scripts/")

    override fun equals(other: Any?): Boolean {
        if (other is ScriptStub) {
            return file == other.file
        }
        return false
    }

    override fun hashCode() = file.hashCode() + 2

    override fun toString() = "ScriptSub for $file"

}

/**
 *
 */
class MacroStub(val file: File) {

    val name : String
        get() = Editor.resources.toPath(file).removePrefix("macros/")

    override fun equals(other: Any?): Boolean {
        if (other is MacroStub) {
            return file == other.file
        }
        return false
    }

    override fun hashCode() = file.hashCode() + 2

    override fun toString() = "MacroSub for $file"

}
