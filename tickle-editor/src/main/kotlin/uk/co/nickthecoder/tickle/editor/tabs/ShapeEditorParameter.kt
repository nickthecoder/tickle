/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.canvas.Canvas
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import javafx.scene.shape.StrokeLineCap
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.AbstractParameter
import uk.co.nickthecoder.paratask.parameters.FloatParameter
import uk.co.nickthecoder.paratask.parameters.fields.ParameterField
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.editor.parameters.AngleParameter
import uk.co.nickthecoder.tickle.editor.parameters.Vector2Parameter
import uk.co.nickthecoder.tickle.editor.util.*
import uk.co.nickthecoder.tickle.physics.BoxDef
import uk.co.nickthecoder.tickle.physics.CircleDef
import uk.co.nickthecoder.tickle.physics.PolygonDef
import uk.co.nickthecoder.tickle.physics.ShapeDef
import uk.co.nickthecoder.tickle.util.Vector2

class ShapeEditorParameter(name: String, val pose: Pose, val fixtureParameter: CostumeTab.PhysicsTask.FixtureParameter)

    : AbstractParameter(name, label = "", description = "") {

    override fun errorMessage(): String? = null

    override fun copy() = ShapeEditorParameter(name, pose, fixtureParameter)

    override fun isStretchy(): Boolean = true

    private var field = ShapeEditorField(this)

    override fun createField(): ParameterField {
        field.build()
        return field
    }

    fun update(shapedDef: ShapeDef?) {
        field.update(shapedDef)
    }

}

class ShapeEditorTask(pose: Pose, fixtureParameter: CostumeTab.PhysicsTask.FixtureParameter) : AbstractTask() {

    val shapeEditorP = ShapeEditorParameter("shapeEditor", pose, fixtureParameter)

    override val taskD = TaskDescription("editShape")
        .addParameters(shapeEditorP)

    override fun run() {}
}

class ShapeEditorField(shapeEditorParameter: ShapeEditorParameter) : ParameterField(shapeEditorParameter) {

    val fixtureParameter = shapeEditorParameter.fixtureParameter
    val pose = shapeEditorParameter.pose
    val poseWidth = pose.pixelRect.width
    val poseHeight = pose.pixelRect.height

    val margin = 10.0
    val borderColor = Color(0.0, 0.0, 0.0, 0.3)
    val shapeColor = Color(1.0, 0.0, 0.0, 1.0)
    val handleColor = Color(0.0, 0.0, 1.0, 1.0)
    val currentHandleColor = Color(1.0, 1.0, 1.0, 1.0)

    val canvas = Canvas(poseWidth.toDouble() + margin * 2, poseHeight.toDouble() + margin * 2)

    val handles = mutableListOf<ShapeEditorHandle>()

    init {
        with(canvas) {
            graphicsContext2D.transform(1.0, 0.0, 0.0, -1.0, 0.0, canvas.height)

            addEventHandler(MouseEvent.MOUSE_PRESSED) { onMousePressed(it) }
            addEventHandler(MouseEvent.MOUSE_MOVED) { onMouseMoved(it) }
            addEventHandler(MouseEvent.MOUSE_DRAGGED) { onMouseDragged(it) }
            addEventHandler(MouseEvent.MOUSE_RELEASED) { dragging = false }
        }
    }

    override fun createControl(): Node {
        return canvas
    }

    var dirty = false
        set(v) {
            if (v && !field) {
                Platform.runLater {
                    redraw()
                }
            }
            field = v
        }

    var currentHandle: ShapeEditorHandle? = null
        set(v) {
            if (field != v) {
                field = v
                dirty = true
            }
        }

    var dragging = false

    var currentShapedDef: ShapeDef? = null


    fun closestHandle(event: MouseEvent): ShapeEditorHandle? {
        val offsetX = event.x - margin - pose.offsetX
        val offsetY = canvas.height - margin - event.y - pose.offsetY

        var result: ShapeEditorHandle? = null
        var minDist = Double.MAX_VALUE
        handles.forEach { handle ->
            val position = handle.position()
            val dx = Math.abs(position.x - offsetX)
            val dy = Math.abs(position.y - offsetY)

            if (dx <= 6.0 && dy <= 6) {
                val dist = dx * dx + dy * dy
                if (dist < minDist) {
                    minDist = dist
                    result = handle
                }
            }
        }
        return result
    }

    fun onMousePressed(event: MouseEvent) {
        currentHandle = closestHandle(event)
        dragging = currentHandle != null
    }

    fun onMouseMoved(event: MouseEvent) {
        currentHandle = closestHandle(event)
    }

    fun onMouseDragged(event: MouseEvent) {
        val offsetX = event.x - margin - pose.offsetX
        val offsetY = canvas.height - margin - event.y - pose.offsetY
        currentHandle?.moveTo(offsetX.toFloat(), offsetY.toFloat())
    }

    fun update(shapeDef: ShapeDef?) {

        currentShapedDef = shapeDef

        if (!dragging) {
            handles.clear()

            when (shapeDef) {
                is CircleDef -> {
                    handles.add(
                        RadiusHandle(
                            fixtureParameter.circleCenterP.xP,
                            fixtureParameter.circleCenterP.yP,
                            fixtureParameter.circleRadiusP
                        )
                    )
                    handles.add(PositionHandle(fixtureParameter.circleCenterP))
                }

                is BoxDef -> {
                    val corner1 = CornerHandle(
                        fixtureParameter.boxCenterP,
                        fixtureParameter.boxSizeP.xP,
                        fixtureParameter.boxSizeP.yP,
                        fixtureParameter.boxAngleP,
                        null
                    )
                    val corner2 = CornerHandle(
                        fixtureParameter.boxCenterP,
                        fixtureParameter.boxSizeP.xP,
                        fixtureParameter.boxSizeP.yP,
                        fixtureParameter.boxAngleP,
                        corner1
                    )
                    handles.add(corner1)
                    handles.add(corner2)
                }

                is PolygonDef -> {
                    fixtureParameter.polygonPointsP.innerParameters.forEach { pointP ->
                        handles.add(PositionHandle(pointP))
                    }
                }
            }

        }

        dirty = true
    }

    fun redraw() {

        dirty = false

        val shapeDef = currentShapedDef

        with(canvas.graphicsContext2D) {
            save()
            clearRect(0.0, 0.0, canvas.width, canvas.height)
            lineWidth = 1.0
            stroke = borderColor

            translate(margin, margin)
            strokeRect(0.0, 0.0, poseWidth.toDouble(), poseHeight.toDouble())

            save()
            translate(pose.offsetX.toDouble(), pose.offsetY.toDouble())
            save()

            when (shapeDef) {
                is CircleDef -> {
                    drawOutlined(shapeColor) {
                        strokeOval(
                            shapeDef.center.x - shapeDef.radius,
                            shapeDef.center.y - shapeDef.radius,
                            shapeDef.radius * 2,
                            shapeDef.radius * 2
                        )
                    }
                }

                is BoxDef -> {
                    translate(shapeDef.center.x, shapeDef.center.y)
                    rotate(shapeDef.angle.degrees)
                    drawOutlined(shapeColor) {
                        strokeRect(-shapeDef.width / 2, -shapeDef.height / 2, shapeDef.width, shapeDef.height)
                    }
                }

                is PolygonDef -> {
                    lineCap = StrokeLineCap.ROUND
                    drawOutlined(shapeColor) {
                        val xs = DoubleArray(shapeDef.points.size) { i -> shapeDef.points.map { it.x }[i].toDouble() }
                        val ys = DoubleArray(shapeDef.points.size) { i -> shapeDef.points.map { it.y }[i].toDouble() }
                        strokePolygon(xs, ys, shapeDef.points.size)
                    }
                }
            }

            restore()

            handles.forEach { it.draw() }

            restore()

            this.globalAlpha = 0.5
            drawImage(
                pose.texture.cachedImage(),
                pose.pixelRect.left.toDouble(),
                pose.pixelRect.bottom.toDouble(),
                pose.pixelRect.width.toDouble(),
                -pose.pixelRect.height.toDouble(),
                0.0,
                0.0,
                pose.pixelRect.width.toDouble(),
                pose.pixelRect.height.toDouble()
            )
            this.globalAlpha = 1.0

            restore()
        }
    }

    fun drawOutlined(color: Color, shape: () -> Unit) {
        with(canvas.graphicsContext2D) {
            stroke = Color.BLACK
            lineCap = StrokeLineCap.ROUND
            lineWidth = 2.0
            shape()
            stroke = color
            lineWidth = 1.0
            shape()
        }
    }

    abstract inner class ShapeEditorHandle {

        abstract fun position(): Vector2

        fun draw() {
            with(canvas.graphicsContext2D) {
                save()
                val position = position()
                translate(position.x, position.y)
                drawOutlined(if (this@ShapeEditorHandle == currentHandle) currentHandleColor else handleColor) {
                    strokeRect(-3.0, -3.0, 6.0, 6.0)
                }
                restore()
            }
        }

        abstract fun moveTo(x: Float, y: Float)
    }

    inner class PositionHandle(val parameter: Vector2Parameter) : ShapeEditorHandle() {
        override fun position() = Vector2(parameter.x ?: 0f, parameter.y ?: 0f)

        override fun moveTo(x: Float, y: Float) {
            parameter.xP.value = x
            parameter.yP.value = y
        }
    }

    inner class RadiusHandle(
        val centerXP: FloatParameter,
        val centerYP: FloatParameter,
        val radiusParameter: FloatParameter
    ) : ShapeEditorHandle() {
        override fun position() = Vector2(
            (centerXP.value ?: 0f) + (radiusParameter.value ?: 0f), (centerYP.value ?: 0f)
        )

        override fun moveTo(x: Float, y: Float) {
            radiusParameter.value = x - (centerXP.value ?: 0f)
        }
    }

    inner class CornerHandle(
        val centerParameter: Vector2Parameter,
        val widthParameter: FloatParameter,
        val heightParameter: FloatParameter,
        val angleParameter: AngleParameter,
        other: CornerHandle?
    ) : ShapeEditorHandle() {

        lateinit var other: CornerHandle

        var plusX: Boolean = true
        var plusY: Boolean = true

        init {
            if (other != null) {
                this.other = other
                this.other.other = this
                plusX = false
                plusY = false
            }
        }

        override fun position(): Vector2 {
            val scaleX = if (plusX) 0.5f else -0.5f
            val scaleY = if (plusY) 0.5f else -0.5f
            val width = widthParameter.value ?: 0f
            val height = heightParameter.value ?: 0f
            val cos = Math.cos(angleParameter.value.radians).toFloat()
            val sin = Math.sin(angleParameter.value.radians).toFloat()
            val dx = width * scaleX * cos - height * scaleY * sin
            val dy = height * scaleY * cos + width * scaleX * sin
            return Vector2((centerParameter.x ?: 0f) + dx, (centerParameter.y ?: 0f) + dy)
        }


        override fun moveTo(x: Float, y: Float) {
            //val rotate = Matrix3x2d().rotate(-angleParameter.value.radians)
            val radians = -angleParameter.value.radians
            val rotated = Vector2(x - (centerParameter.x ?: 0f), y - (centerParameter.y ?: 0f))
            rotated.setRotateRadians(radians)

            val otherRotated = other.position()
            otherRotated.x -= (centerParameter.x ?: 0f)
            otherRotated.y -= (centerParameter.y ?: 0f)

            otherRotated.setRotateRadians(radians)

            val oldWidth = widthParameter.value ?: 0f
            val oldHeight = heightParameter.value ?: 0f

            var width = Math.round(rotated.x - otherRotated.x).toFloat() * if (plusX) 1 else -1
            var height = Math.round(rotated.y - otherRotated.y).toFloat() * if (plusY) 1 else -1

            if (width < 0) {
                width = -width
                plusX = !plusX
                other.plusX = !plusX
            }
            if (height < 0) {
                height = -height
                plusY = !plusY
                other.plusY = !plusY
            }

            centerParameter.x = (centerParameter.x ?: 0f) + (width - oldWidth) / 2 * if (plusX) 1 else -1
            centerParameter.y = (centerParameter.y ?: 0f) + (height - oldHeight) / 2 * if (plusY) 1 else -1

            widthParameter.value = width
            heightParameter.value = height
        }
    }

}
