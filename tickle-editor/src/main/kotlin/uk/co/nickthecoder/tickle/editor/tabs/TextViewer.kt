package uk.co.nickthecoder.tickle.editor.tabs

import javafx.scene.image.ImageView
import uk.co.nickthecoder.paratask.util.requestFocusWhenSceneSet
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.code.CodeEditor
import uk.co.nickthecoder.tickle.severe
import java.net.URL

class TextViewer(val name: String, url: URL) : EditorTab(name, url) {

    val codeEditor = CodeEditor()

    init {
        codeEditor.scarea.requestFocusWhenSceneSet()
        codeEditor.scarea.editable = false
        if (name.endsWith(".feather")) codeEditor.attachSyntaxHighlighter("feather")

        graphic = ImageView(Editor.imageResource("text.png"))
        text = dataName
        content = codeEditor.borderPane
        try {
            codeEditor.scarea.text = url.readText()
        } catch (e: Exception) {
            severe(e)
        }

    }

}
