/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.scene.SelectedActor

class AddActor(
    private val selectedActor: SelectedActor
) : Change {

    override fun redo(sceneEditor: SceneEditor) {
        selectedActor.editingStage.stage.add(selectedActor.actor)
        sceneEditor.fireActorsChanged(listOf(selectedActor.actor), ModificationType.NEW)
    }

    override fun undo(sceneEditor: SceneEditor) {
        selectedActor.editingStage.stage.remove(selectedActor.actor)
        sceneEditor.fireActorsChanged(listOf(selectedActor.actor), ModificationType.DELETE)
    }

}
