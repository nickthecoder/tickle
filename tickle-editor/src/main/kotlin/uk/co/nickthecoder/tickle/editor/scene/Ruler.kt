package uk.co.nickthecoder.tickle.editor.scene

import javafx.application.Platform
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Point3D
import javafx.geometry.Side
import javafx.scene.layout.Region
import javafx.scene.shape.LineTo
import javafx.scene.shape.MoveTo
import javafx.scene.shape.Path
import javafx.scene.text.Text
import javafx.scene.transform.Rotate

class Ruler : Region() {

    val sideProperty: ObjectProperty<Side> = SimpleObjectProperty(Side.TOP)
    var side: Side
        get() = sideProperty.value
        set(v) {
            sideProperty.value = v
        }

    val lowerBoundProperty = SimpleDoubleProperty(0.0)
    var lowerBound: Double
        get() = lowerBoundProperty.value
        set(v) {
            lowerBoundProperty.value = v
        }

    val upperBoundProperty = SimpleDoubleProperty(100.0)
    var upperBound: Double
        get() = upperBoundProperty.value
        set(v) {
            upperBoundProperty.value = v
        }

    val stepProperty = SimpleDoubleProperty(0.0)
    var step: Double
        get() = stepProperty.value
        set(v) {
            stepProperty.value = v
        }

    var subdivisionsProperty = SimpleIntegerProperty(10)
    var subdivisions: Int
        get() = subdivisionsProperty.value
        set(v) {
            subdivisionsProperty.value = v
        }

    private val tickMarks = Path()

    private var dirty = true

    init {
        sideProperty.addListener { _, _, _ -> update() }
        lowerBoundProperty.addListener { _, _, _ -> update() }
        upperBoundProperty.addListener { _, _, _ -> update() }
        stepProperty.addListener { _, _, _ -> update() }
        subdivisionsProperty.addListener { _, _, _ -> update() }
        val size = 20.0
        minWidth = size
        minHeight = size
        prefHeight = size
        prefWidth = size
        update()
    }

    private fun dirty() {
        if (!dirty) {
            dirty = true
            Platform.runLater { update() }
        }
    }

    private fun update() {

        val width = layoutBounds.width
        val height = layoutBounds.height
        val length = if (side.isHorizontal) width else height
        val thickness = if (side.isHorizontal) height else width

        val upperBound = upperBound
        val lowerBound = lowerBound
        val subdivisions = subdivisions


        val step = if (step > 0) {
            step
        } else {
            val pixelsPer100 = 100 * length / (upperBound - lowerBound)
            if (pixelsPer100 < 10) {
                2000.0
            } else if (pixelsPer100 < 13) {
                1000.0
            } else if (pixelsPer100 < 25) {
                500.0
            } else if (pixelsPer100 < 50) {
                200.0
            } else if (pixelsPer100 < 100) {
                100.0
            } else if (pixelsPer100 < 250) {
                50.0
            } else if (pixelsPer100 < 500) {
                20.0
            } else {
                10.0
            }

        }

        val from = lowerBound - lowerBound.rem(step) - if (lowerBound < 0) step else 0.0
        val to = upperBound - upperBound.rem(step) - if (upperBound < 0) step else 0.0
        val steps = Math.round((to - from) / step)
        val subStep = step / subdivisions
        val highSubIndex = if (subdivisions % 2 == 0) subdivisions / 2 else 0

        val scale = (if (side.isHorizontal) width else height) / (upperBound - lowerBound)

        fun tick(position: Double, size: Double) {

            if (position < 0 || position > length) return

            when (side) {
                Side.TOP -> tickMarks.elements.addAll(
                    MoveTo(position, thickness),
                    LineTo(position, thickness - size)
                )

                Side.BOTTOM -> tickMarks.elements.addAll(
                    MoveTo(position, 0.0),
                    LineTo(position, size)
                )

                Side.LEFT -> tickMarks.elements.addAll(
                    MoveTo(thickness, length - position),
                    LineTo(thickness - size, length - position)
                )

                Side.RIGHT -> tickMarks.elements.addAll(
                    MoveTo(0.0, length - position),
                    LineTo(size, length - position)
                )

            }
        }

        fun createText(value: Double, position: Double) {
            val text = Text(Math.round(value).toString())
            children.add(text)
            when (side) {
                Side.TOP -> {
                    text.x = position + 3
                    text.y = thickness - 6.0
                }

                Side.BOTTOM -> {
                    text.x = position + 3
                    text.y = 16.0
                }

                Side.LEFT -> {
                    text.transforms.add(Rotate(-90.0))
                    text.layoutX = thickness - 6
                    text.layoutY = length - position - 3
                }

                Side.RIGHT -> {
                    text.transforms.add(Rotate(-90.0))
                    text.layoutX = 16.0
                    text.layoutY = length - position - 3
                }
            }
        }

        tickMarks.elements.clear()

        children.clear()
        children.add(tickMarks)

        for (i in 0..steps) {
            val value = from + i * step
            val position = (value - lowerBound) * scale
            tick(position, 12.0)
            createText(value, position)
            for (s in 1 until subdivisions) {
                tick(position + s * subStep * scale, if (s == highSubIndex) 4.0 else 2.0)
            }
        }
        dirty = false
    }

}
