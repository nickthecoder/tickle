/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.Scene
import uk.co.nickthecoder.tickle.editor.DOCKABLE_ACTORS
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.resources.SceneListener
import uk.co.nickthecoder.tickle.editor.scene.EditingStage
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.scene.SelectedActor
import uk.co.nickthecoder.tickle.editor.scene.costumeName
import uk.co.nickthecoder.tickle.editor.scene.history.DeleteActors
import uk.co.nickthecoder.tickle.editor.scene.history.LockActor
import uk.co.nickthecoder.tickle.editor.tabs.SceneTab
import uk.co.nickthecoder.tickle.resources.ResourcesListener
import uk.co.nickthecoder.tickle.stage.Stage

/**
 * A Tree structure showing each [Stage] and all [Actor]s on the stages grouped by [Costume].
 */
class ActorsDockable
    : TickleDockable(DOCKABLE_ACTORS) {

    private val noneLabel = Label("N/A")

    override val dockableContent = SimpleObjectProperty<Node>(noneLabel)

    init {
        MainWindow.instance.tabPane.selectionModel.selectedItemProperty().addListener { _, _, _ ->
            tabChanged()
        }
        tabChanged()
    }

    private var currentActorsBox: ActorsBox? = null
        set(v) {
            if (field !== v) {
                // Only one instance of ActorsBox should listen for changes to Resources.
                field?.let { Editor.resources.listeners.remove(it) }
                v?.let { Editor.resources.listeners.add(it) }

                field = v
            }
        }

    private fun tabChanged() {
        val tab = MainWindow.instance.tabPane.selectionModel.selectedItem
        if (tab is SceneTab) {
            currentActorsBox = ActorsBox(tab.sceneEditor).also {
                dockableContent.set(it.build())
            }
        } else {
            currentActorsBox = null
            dockableContent.set(noneLabel)
        }
    }

}

class ActorsBox(val sceneEditor: SceneEditor) : SceneListener, ResourcesListener {

    val scene: Scene = sceneEditor.scene

    private val tree = TreeView<String>()

    private val root = TreeItem("Stages")

    /**
     * Allows us to find a [TreeItem] for a given [Actor].
     * See [actorModified] and [ActorItem] constructor.
     */
    private val actorItemsByActor = mutableMapOf<Actor, ActorItem>()
    private val costumeGroupItemsByCostume = mutableMapOf<Costume, CostumeGroupItem>()

    init {
        tree.selectionModel.selectedItemProperty().addListener { _, _, newValue ->
            if (newValue is ActorItem) {
                sceneEditor.sceneView.selection.clearAndSelect(SelectedActor(newValue.actor, newValue.editingStage))
            }
        }
        sceneEditor.sceneListeners.add(this)
    }

    /**
     * If a costume is renamed, we need to change the [CostumeGroupItem] value.
     * Using just the GUI, this won't be needed, because when the CostumeTab is open, this
     * ActorsBox will be empty.
     */
    override fun resourceRenamed(resource: Any, oldName: String, newName: String) {
        if (resource is Costume) {
            costumeGroupItemsByCostume[resource]?.let { cgi ->
                // Rename the costume name in the tree.
                cgi.value = newName
                // Ensure the CostumeGroupItems remain alphabetical.
                (cgi.parent as? EditingStageItem)?.let { esi ->
                    esi.children.remove(cgi)
                    esi.addAlphabetically(cgi)
                }
            }
        }
    }

    override fun actorModified(scene: Scene, actor: Actor, type: ModificationType) {
        if (type == ModificationType.NEW) {
            sceneEditor.editingStages.firstOrNull { it.stage === actor.stage }?.let { es ->
                val existingCostumeGroupItem = costumeGroupItemsByCostume[actor.costume]
                if (existingCostumeGroupItem == null) {
                    val costumeName = actor.costumeName() ?: "<Unknown Costume>"
                    val cgi = CostumeGroupItem(es, actor.costume, costumeName, listOf(actor))
                    val esi = root.children.filterIsInstance(EditingStageItem::class.java)
                        .firstOrNull { it.editingStage === es }
                    esi?.addAlphabetically(cgi)
                } else {
                    existingCostumeGroupItem.children.add(ActorItem(actor, es))
                }
            }
        } else {
            actorItemsByActor[actor]?.actorModified(scene, actor, type)
        }
    }

    fun build(): Node {

        tree.root = root
        tree.isShowRoot = false

        for (es in sceneEditor.editingStages) {
            root.children.add(EditingStageItem(es))
        }

        tree.addEventHandler(MouseEvent.MOUSE_PRESSED) { onMouse(it) }
        tree.addEventHandler(MouseEvent.MOUSE_RELEASED) { onMouse(it) }
        return tree

    }

    private fun onMouse(event: MouseEvent) {
        if (event.isPopupTrigger) {
            tree.selectionModel.selectedItem?.let {
                val item = it as MyTreeItem?
                item?.createContextMenu()?.show(MainWindow.instance.scene.window, event.screenX, event.screenY)
                event.consume()
            }
        }
    }


    private abstract class MyTreeItem(label: String) : TreeItem<String>(label) {
        open fun createContextMenu(): ContextMenu? = null
    }

    private inner class EditingStageItem(val editingStage: EditingStage)

        : MyTreeItem(editingStage.stageName), SceneListener {

        init {
            val stage = editingStage.stage

            // Key is Costume name. Value is the Costume, and the list of actors with this costume.
            val groups = mutableMapOf<String, Pair<Costume, List<Actor>>>()

            for ((costume, list) in stage.actors.groupBy { it.costume }) {
                Editor.resources.findNameOrNull(costume)?.let { costumeName ->
                    groups[costumeName] = Pair(costume, list)
                }
            }

            // Add the CostumeGroupItems alphabetically by costume name.
            for (costumeName in groups.keys.sorted()) {
                groups[costumeName]?.let { (costume, items) ->
                    children.add(CostumeGroupItem(editingStage, costume, costumeName, items))
                }
            }
            isExpanded = true
        }

        /**
         * Adds a CostumeGroupItem is the correct place, when [children] is sorted by the costume name.
         */
        fun addAlphabetically(item: CostumeGroupItem) {
            for (i in children.indices) {
                val existing = children[i]
                if (existing.value > item.value) {
                    children.add(i, item)
                    return
                }
            }
            // No existing items were found "after" 'item', so add it to the end.
            children.add(item)
        }

        override fun actorModified(scene: Scene, actor: Actor, type: ModificationType) {
            // Add a new CostumeGroupItem if this is the first actor with this costume (on this stage).
            if (type == ModificationType.NEW && actor.stage === editingStage.stage) {
                actor.costumeName()?.let { costumeName ->
                    if (children.firstOrNull { it.value == costumeName } == null) {
                        addAlphabetically(CostumeGroupItem(editingStage, actor.costume, costumeName, listOf(actor)))
                    }
                }
            }
        }

        override fun isLeaf() = false

    }

    private inner class CostumeGroupItem(
        val editingStage: EditingStage,
        val costume: Costume,
        costumeName: String,
        actors: List<Actor>
    )

        : MyTreeItem(costumeName), SceneListener {

        init {
            costumeGroupItemsByCostume[costume] = this
            actors.forEach { actor ->
                children.add(ActorItem(actor, editingStage))
            }
        }

        override fun actorModified(scene: Scene, actor: Actor, type: ModificationType) {
            if (type == ModificationType.NEW && actor.stage === editingStage.stage && actor.costumeName() == value) {
                children.add(ActorItem(actor, editingStage))
            }
        }

        override fun createContextMenu(): ContextMenu {
            val menu = ContextMenu()
            menu.items.addAll(
                MenuItem("Delete (${children.count()} actors)").also { menuItem ->
                    menuItem.onAction = EventHandler {
                        children.filterIsInstance<ActorItem>().forEach {
                            it.onDelete()
                        }
                        parent?.children?.remove(this)
                        costumeGroupItemsByCostume.remove(costume)
                    }
                },

                CheckMenuItem("Lock").apply {
                    sceneEditor.history.beginBatch()
                    isSelected = children.filterIsInstance<ActorItem>().none { !it.actor.isLocked }
                    onAction = EventHandler {
                        // NOTE, this is not efficient, as the scene will be redrawn for every actor!
                        children.filterIsInstance<ActorItem>().forEach {
                            if (it.actor.isLocked != isSelected) {
                                sceneEditor.history.makeChange(LockActor(it.actor, isSelected))
                            }
                        }
                    }
                    sceneEditor.history.endBatch()
                }
            )

            return menu
        }


        override fun isLeaf() = false
    }

    private inner class ActorItem(val actor: Actor, val editingStage: EditingStage)

        : MyTreeItem(Editor.resources.findNameOrNull(actor.costume) ?: "<unknown>"), SceneListener {

        init {
            graphic = ImageView(Editor.imageResource("actor.png"))
            updateLabel()
            actorItemsByActor[actor] = this
        }

        override fun actorModified(scene: Scene, actor: Actor, type: ModificationType) {
            if (actor === this.actor) {
                when (type) {
                    ModificationType.DELETE -> {
                        val parent = parent
                        parent?.children?.remove(this)
                        actorItemsByActor.remove(actor)

                        // If this is the last item, then remove the parent too.
                        if (parent.children.isEmpty()) {
                            parent.parent?.children?.remove(parent)
                            costumeGroupItemsByCostume.remove(actor.costume)
                        }
                    }

                    ModificationType.CHANGE -> updateLabel()
                    ModificationType.NEW -> Unit
                }
            }
        }

        override fun createContextMenu(): ContextMenu {
            return ContextMenu().apply {
                items.addAll(
                    MenuItem("Delete").apply {
                        onAction = EventHandler {
                            onDelete()
                        }
                    },
                    CheckMenuItem("Lock").apply {
                        sceneEditor.history.beginBatch()
                        isSelected = actor.isLocked
                        onAction = EventHandler {
                            sceneEditor.history.makeChange(LockActor(actor, isSelected))
                        }
                        sceneEditor.history.endBatch()
                    }
                )
            }
        }

        fun onDelete() {
            sceneEditor.history.makeChange(DeleteActors(listOf(SelectedActor(actor, editingStage))))
        }

        fun updateLabel() {
            value = "${Editor.resources.findNameOrNull(actor.costume) ?: "<unknown>"} ${actor.x.toInt()},${actor.y.toInt()}"
        }

        override fun isLeaf() = true
    }
}