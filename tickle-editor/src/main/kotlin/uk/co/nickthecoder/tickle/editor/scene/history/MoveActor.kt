/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor

/**
 * Note. Unlike most Undo/Redo operations, the change has ALREADY occurred when this object is created.
 * This is needed, because of how snapping works.
 * So the actor is moved, then a MoveActor is created, and added to the history.
 */
class MoveActor(
    private val actor: Actor,
    private val oldX: Float,
    private val oldY: Float
) : Change {

    private var newX = actor.x

    private var newY = actor.y

    override fun redo(sceneEditor: SceneEditor) {
        actor.x = newX
        actor.y = newY
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        actor.x = oldX
        actor.y = oldY
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    /**
     * While dragging an actor, this allows all the small increments to be merged into a single Change
     */
    override fun mergeWith(other: Change): Boolean {
        if (other is MoveActor && other.actor == actor) {
            other.newX = actor.x
            other.newY = actor.y
            return true
        }
        return false
    }
}
