/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.resources

import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes
import uk.co.nickthecoder.tickle.resources.Layout
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.severe
import uk.co.nickthecoder.tickle.util.Dependable
import java.io.File
import java.util.regex.Pattern

class DesignResources : Resources() {

    override fun load(resourcesFile: File) {
        ResourcesReaderWriter.load(this, resourcesFile)
    }

    override fun saveAs(resourcesFile: File) {
        if (loadFailed) {
            throw IllegalStateException("Resources did not load correctly. Save disabled")
        } else {
            ResourcesReaderWriter.save(this)
        }
    }

    override fun createAttributes(): DesignAttributes {
        return DesignAttributes()
    }

    override fun findNameOrNull(resource: Any?): String? {
        return if (resource is SceneStub) {
            resource.name
        } else {
            super.findNameOrNull(resource)
        }
    }


    override fun sceneDependables(costume: Costume): List<Dependable> {
        val result = mutableListOf<Dependable>()
        for (stub in allScenesStubs()) {
            if (stub.dependsOn(costume)) {
                result.add(stub)
            }
        }
        return result
    }

    override fun sceneDependables(layout: Layout): List<Dependable> {
        return allScenesStubs().filter { it.dependsOn(this, layout) }
    }

    private fun allScenesStubs(): List<SceneStub> {

        val result = mutableListOf<SceneStub>()

        fun addFrom(dir: File) {
            val files = dir.listFiles() ?: return
            for (file in files) {
                if (file.isDirectory) {
                    addFrom(file)
                } else if (file.extension == "scene") {
                    result.add(SceneStub(file))
                }
            }
        }

        addFrom(sceneDirectory)
        return result
    }

    override fun updateScenesAfterLayoutRenamed(oldName: String, newName: String) {

        // We will be replacing the name in scene files using regular expression
        // This saves us from loading and re-saving the data via JSON.
        val attributeName = "layout"
        // The attribute name may or may not be enclosed with double quotes.
        val attributeNamePatternString = attributeName + "|" + "\"$attributeName\""
        // This is what we will be replacing
        val groupString = "(?<NAME>$oldName)"
        // So, we are looking for one of :
        //     layout : "oldName"
        //     "layout" : "oldName"
        val pattern = Pattern.compile(attributeNamePatternString + "\\s*:\\s*\"" + groupString + "\"")

        for (ss in allScenesStubs()) {
            try {
                val text = ss.file.readText()
                val buffer = StringBuffer(text)
                val matcher = pattern.matcher(text)
                var offset = 0
                while (matcher.find()) {
                    if (matcher.group("NAME") != null) {
                        buffer.replace(offset + matcher.start("NAME"), offset + matcher.end("NAME"), newName)
                        offset += newName.length - oldName.length
                    }
                }

                ss.file.writeText(buffer.toString())

            } catch (e: Exception) {
                severe("Failed to load scene ${ss.file} while renaming a Layout.")
            }
        }

    }
}
