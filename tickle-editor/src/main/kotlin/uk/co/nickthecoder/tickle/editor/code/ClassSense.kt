package uk.co.nickthecoder.tickle.editor.code

class ClassSense(parent: ClassSense?) : BlockSense(parent) {

    var classInterfaceOrEnum: String = "?"
    var name: String = "?"

}

