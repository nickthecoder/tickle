/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.stage.Stage
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParaTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.parameters.fields.TaskForm
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.util.SceneEditorPreferences
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences
import uk.co.nickthecoder.tickle.editor.util.toJavaFX
import uk.co.nickthecoder.tickle.editor.util.toTickle
import uk.co.nickthecoder.tickle.graphics.Colors
import uk.co.nickthecoder.tickle.severe

class EditorPreferencesGUI(val sceneEditor: SceneEditor?) {

    val snapTos = if (sceneEditor == null) emptyMap() else mutableMapOf(
        "Grid" to sceneEditor.snapPreferences.snapToGrid,
        "Guides" to sceneEditor.snapPreferences.snapToGuides,
        "Others" to sceneEditor.snapPreferences.snapToOthers,
        "Rotation" to sceneEditor.snapPreferences.snapRotation
    )

    val whole = BorderPane()

    val tabPane = TabPane()

    val buttons = FlowPane()

    val okButton = Button("Ok")

    val cancelButton = Button("Cancel")

    val taskForms = mutableListOf<TaskForm>()

    val stage = Stage()

    fun show() {

        with(okButton) {
            onAction = EventHandler { onOk() }
            isDefaultButton = true
        }

        with(cancelButton) {
            onAction = EventHandler { onCancel() }
            isCancelButton = true
        }

        with(buttons) {
            children.addAll(okButton, cancelButton)
            styleClass.add("buttons")
        }

        val generalForm = TaskForm(EditorPreferencesTask(EditorPreferences))
        taskForms.add(generalForm)
        tabPane.tabs.add(Tab("General", generalForm.build()))

        val sceneEditorForm = TaskForm(SceneEditorPreferencesTask(EditorPreferences.sceneEditorPreferences))
        taskForms.add(sceneEditorForm)
        tabPane.tabs.add(Tab("Scene Editor", sceneEditorForm.build()))

        snapTos.forEach { (name, snapTo) ->
            val form = TaskForm(snapTo.task())
            tabPane.tabs.add(Tab(name, form.build()))
            taskForms.add(form)
        }
        tabPane.selectionModel.select(EditorPreferences.defaultPreferencesTabIndex)
        tabPane.selectionModel.selectedIndexProperty().addListener { _, _, newValue ->
            EditorPreferences.defaultPreferencesTabIndex = newValue.toInt()
        }

        with(whole) {
            center = tabPane
            bottom = buttons
        }

        val scene = Scene(whole)
        ParaTask.style(scene)

        with(stage) {
            title = "Editor Preferences"
            this.scene = scene
            centerOnScreen()
            show()
        }
    }

    fun onOk() {
        try {
            taskForms.forEach { form ->
                form.check()
            }
            taskForms.forEach { form ->
                form.task.run()
            }
            stage.close()
            EditorPreferences.save()
            sceneEditor?.firePreferencesChanged()
        } catch (e: Exception) {
            severe("SnapEditor.onOk() : $e")
        }
    }

    fun onCancel() {
        stage.close()
    }

}

class EditorPreferencesTask(val editorPreferences: EditorPreferences) : AbstractTask() {

    private val treeThumbnailSizeP = IntParameter("treeThumbnailSize", value = editorPreferences.treeThumbnailSize)

    private val costumePickerSizeP =
        IntParameter("costumePickerThumbnailSize", value = editorPreferences.costumePickerThumbnailSize)

    private val outputFormatP = ChoiceParameter("outputFormat", value = editorPreferences.outputFormat)
        .enumChoices(true)

    private val captureStdOutP = BooleanParameter(
        "captureStdOut",
        label = "Editor's Log Captures System.out (stdout)",
        value = editorPreferences.captureStdOut,
        hint = "(requires restart)"
    )

    private val captureStdErrP = BooleanParameter(
        "captureStdErr",
        label = "Editor's Log Captures System.err (stdErr)",
        value = editorPreferences.captureStdErr,
        hint = "(requires restart)"
    )

    private val javadocOrDokkaP =
        BooleanParameter("javadocOrDokka", value = editorPreferences.tickleApiUseJavadocs).apply {
            asComboBox("Javadoc", "Dokka")
        }

    private val javadocBaseP = StringParameter("javadocBase", value = editorPreferences.tickleJavadocURL)

    private val dokkaBaseP = StringParameter("dokkaBase", value = editorPreferences.tickleDokkaURL)


    override val taskD = TaskDescription("editGameInfo").addParameters(
        treeThumbnailSizeP, costumePickerSizeP, outputFormatP,
        captureStdOutP, captureStdErrP,
        javadocOrDokkaP, javadocBaseP, dokkaBaseP
    )

    init {
        javadocOrDokkaP.listen { hideShowBases() }
        hideShowBases()
    }

    private fun hideShowBases() {
        javadocBaseP.hidden = javadocOrDokkaP.value != true
        dokkaBaseP.hidden = javadocOrDokkaP.value != false
    }

    override fun run() {

        with(editorPreferences) {
            treeThumbnailSize = treeThumbnailSizeP.value!!
            costumePickerThumbnailSize = costumePickerSizeP.value!!
            outputFormat = outputFormatP.value!!
            captureStdOut = captureStdOutP.value!!
            captureStdErr = captureStdErrP.value!!

            tickleApiUseJavadocs = javadocOrDokkaP.value == true
            tickleJavadocURL = javadocBaseP.value
            tickleDokkaURL = dokkaBaseP.value
        }
    }

}

class SceneEditorPreferencesTask(val sceneEditorPreferences: SceneEditorPreferences) : AbstractTask() {

    val selectionColorP =
        ColorParameter("selectionColor", label = "", value = sceneEditorPreferences.selectionColor.toJavaFX())
    val highlightColorP = ColorParameter(
        "highlightColor",
        label = "Highlight", value = sceneEditorPreferences.latestSelectionColor.toJavaFX()
    )
    val selectionP = SimpleGroupParameter("selection").apply {
        addParameters(selectionColorP, highlightColorP)
        asHorizontal()
    }

    val guideColorP = ColorParameter("guideColor", label = "", value = sceneEditorPreferences.guideColor.toJavaFX())
    val highlightGuideColorP = ColorParameter(
        "highlightGuideColor",
        label = "Highlight", value = sceneEditorPreferences.highlightGuideColor.toJavaFX()
    )
    val guidesP = SimpleGroupParameter("guides").apply {
        addParameters(guideColorP, highlightGuideColorP)
        asHorizontal()
    }

    val screenSizeColorP =
        ColorParameter("screenSizeColor", value = sceneEditorPreferences.screenSizeColor.toJavaFX())

    val yellowP = ButtonParameter("yellow", buttonText = "Yellow") { standard("yellow") }
    val redP = ButtonParameter("red", buttonText = "Red") { standard("red") }
    val blueP = ButtonParameter("blue", buttonText = "Blue") { standard("blue") }
    val standardColorsP = SimpleGroupParameter("chooseStandardColors").apply {
        addParameters(yellowP, redP, blueP)
        asHorizontal(labelPosition = LabelPosition.NONE, isBoxed = true)
    }

    val handleSizeP = FloatParameter("dragHandleSize", value = sceneEditorPreferences.handleSize)
    val selectionLineThicknessP =
        FloatParameter("selectionLineThickness", value = sceneEditorPreferences.selectionLineThickness)
    val guidesThicknessP = FloatParameter("guidesThickness", value = sceneEditorPreferences.guidesThickness)
    val screenRectThicknessP = FloatParameter("screenRectThickness", value = sceneEditorPreferences.screenSizeThickness)
    val sizesP = SimpleGroupParameter("sizes").apply {
        addParameters(handleSizeP, selectionLineThicknessP, guidesThicknessP, screenRectThicknessP)
        asBox()
    }

    override val taskD = TaskDescription("colorPreferences").addParameters(
        selectionP, guidesP, screenSizeColorP, standardColorsP, sizesP
    )

    init {
        for (param in taskD.valueParameters()) {
            param.listen { update() }
        }
    }

    private fun standard(name: String) {
        when (name) {
            "yellow" -> {
                selectionColorP.value = Colors["#990"].toJavaFX()
                highlightColorP.value = Colors["#ffc"].toJavaFX()
                guideColorP.value = Colors["#449"].toJavaFX()
                highlightGuideColorP.value = Colors["#ffc"].toJavaFX()
                screenSizeColorP.value = Colors["#888"].toJavaFX()
            }

            "red" -> {
                selectionColorP.value = Colors["#600"].toJavaFX()
                highlightColorP.value = Colors["#f00"].toJavaFX()
                guideColorP.value = Colors["#449"].toJavaFX()
                highlightGuideColorP.value = Colors["#f00"].toJavaFX()
                screenSizeColorP.value = Colors["#888"].toJavaFX()
            }

            "blue" -> {
                selectionColorP.value = Colors["#46c"].toJavaFX()
                highlightColorP.value = Colors["#cef"].toJavaFX()
                guideColorP.value = Colors["#449"].toJavaFX()
                highlightGuideColorP.value = Colors["#cef"].toJavaFX()
                screenSizeColorP.value = Colors["#888"].toJavaFX()
            }
        }
    }

    private fun update() {
        with(sceneEditorPreferences) {
            selectionColor = selectionColorP.value.toTickle()
            latestSelectionColor = highlightColorP.value.toTickle()
            guideColor = guideColorP.value.toTickle()
            highlightGuideColor = highlightGuideColorP.value.toTickle()
            screenSizeColor = screenSizeColorP.value.toTickle()

            handleSizeP.value?.let { handleSize = it }
            selectionLineThicknessP.value?.let { selectionLineThickness = it }
            guidesThicknessP.value?.let { guidesThickness = it }
            screenRectThicknessP.value?.let { screenSizeThickness = it }
        }
        MainWindow.instance.firePreferencesChanged()
    }

    override fun run() {
        update()
    }
}
