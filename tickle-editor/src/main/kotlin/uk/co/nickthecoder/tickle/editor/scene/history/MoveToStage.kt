/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.EditingStage
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.scene.SelectedActor

class MoveToStage(
    private val actor: Actor,
    private val fromEditingStage: EditingStage,
    private val toEditingStage: EditingStage
) : Change {

    private val oldStage = actor.stage

    override fun redo(sceneEditor: SceneEditor) {
        fromEditingStage.stage.remove(actor)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.DELETE)

        toEditingStage.stage.add(actor)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.NEW)
    }

    override fun undo(sceneEditor: SceneEditor) {
        toEditingStage.stage.remove(actor)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.DELETE)

        fromEditingStage.stage.add(actor)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.NEW)
    }
}
