/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.control.cell.TextFieldTreeCell
import javafx.scene.input.*
import javafx.stage.Stage
import uk.co.nickthecoder.fxessentials.ApplicationAction
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.StringParameter
import uk.co.nickthecoder.paratask.util.FileLister
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.MacroStub
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.resources.SceneStub
import uk.co.nickthecoder.tickle.editor.resources.ScriptStub
import uk.co.nickthecoder.tickle.editor.tabs.CostumeTab
import uk.co.nickthecoder.tickle.editor.tasks.NewResourceTask
import uk.co.nickthecoder.tickle.editor.util.copyPrompted
import uk.co.nickthecoder.tickle.editor.util.deletePrompted
import uk.co.nickthecoder.tickle.editor.util.renamePrompted
import uk.co.nickthecoder.tickle.editor.util.toImageView
import uk.co.nickthecoder.tickle.events.Input
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.Layout
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.resources.ResourcesListener
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.sound.Sound
import uk.co.nickthecoder.tickle.misc.CopyableResource
import uk.co.nickthecoder.tickle.util.DeletableResource
import uk.co.nickthecoder.tickle.misc.RenamableResource
import uk.co.nickthecoder.tickle.util.SceneReader
import java.io.File

/**
 * A dockable item which displays resources in a tree structure.
 * There are many such dockables, one for each resource type (which only shows resources of
 * a single type) and also a tree of ALL resource types.
 *
 * In the latter case resourceType is [ResourceType.ANY]
 */
class ResourcesDockable(action: ApplicationAction, resourceType: ResourceType) : TickleDockable(action) {

    val tree = ResourcesTree(resourceType)

    override val dockableContent = SimpleObjectProperty<Node>(tree)

}

/**
 * A tree structure showing all the resources : Textures, Poses, Costumes, etc.
 *
 * The tree is updated using [ResourcesListener].
 * Each time a resource is changed, added or removed,
 * this is notified (because it added itself to [Resources.listeners]).
 * The notification is then passed on to *every* parent item in the tree.
 */
class ResourcesTree(val resourceType: ResourceType)

    : TreeView<String>(), ResourcesListener {

    val resources
        get() = Editor.resources

    init {
        isEditable = false

        addEventFilter(MouseEvent.MOUSE_RELEASED) { onMousePressed(it) }
        addEventFilter(MouseEvent.MOUSE_PRESSED) { onMousePressed(it) }
        addEventFilter(KeyEvent.KEY_PRESSED) { onKeyPressed(it) }
        onDragDetected = EventHandler { dragDetected(it) }
        onDragDone = EventHandler { dragDone(it) }

        resources.listeners.add(this)

        setCellFactory {
            val cell = object : TextFieldTreeCell<String>() {
                override fun updateItem(item: String?, empty: Boolean) {
                    super.updateItem(item, empty)
                    text = if (empty) null else item
                }
            }
            cell.onDragOver = EventHandler { event ->
                if (!cell.isEmpty) {
                    val treeItem = cell.treeItem
                    if (treeItem is DragTarget) {
                        treeItem.dragOver(event)
                    }
                }
            }
            cell.onDragDropped = EventHandler { event ->
                if (!cell.isEmpty) {
                    val treeItem = cell.treeItem
                    if (treeItem is DragTarget) {
                        treeItem.dragDropped(event)
                    }
                }
            }
            cell
        }

        build()
        isShowRoot = root !is RootItem

    }


    override fun toString() = "ResourcesTree : $resourceType"

    private fun build() {
        root = when (resourceType) {
            ResourceType.ANY -> RootItem()
            ResourceType.TEXTURE -> TexturesItem()
            ResourceType.POSE -> PosesItem()
            ResourceType.COSTUME -> CostumesItem()
            ResourceType.FONT -> FontResourcesItem()
            ResourceType.SOUND -> SoundsItem()
            ResourceType.INPUT -> InputsItem()
            ResourceType.LAYOUT -> LayoutsItem()
            ResourceType.SCENE -> ScenesDirectoryItem("Scenes", resources.sceneDirectory.absoluteFile)
            ResourceType.SCRIPT -> ScriptsDirectoryItem("Scripts", resources.scriptDirectory().absoluteFile)
            ResourceType.MACRO -> MacrosDirectoryItem("Macros", resources.macroDirectory().absoluteFile)
            else -> throw IllegalArgumentException("Unexpected resource type : $resourceType")
        }
        root.isExpanded = true
    }

    private fun dragDetected(event: MouseEvent) {
        val item = selectionModel.selectedItem

        if (item != null && item is IResourceItem) {

            val db = startDragAndDrop(TransferMode.LINK)

            val content = ClipboardContent()
            content[ResourceInfo.dataFormat] = ResourceInfo(item.name, ResourceType.resourceType(item.resource))
            db.setContent(content)

            event.consume()
        }
    }

    private fun dragDone(event: DragEvent) {
        event.consume()
    }

    private fun onMousePressed(event: MouseEvent) {
        if (event.eventType == MouseEvent.MOUSE_PRESSED && event.clickCount == 2) {
            editItem()
            event.consume()
        }
        if (event.isPopupTrigger) {
            Platform.runLater { // Give JavaFX time to select the item!
                val item = selectionModel.selectedItem
                if (item != null) {
                    if (item is IResourceItem) {
                        item.createContextMenu()?.show(MainWindow.instance.stage, event.screenX, event.screenY)
                    } else if (item is GroupItem) {
                        item.createContextMenu()?.show(MainWindow.instance.stage, event.screenX, event.screenY)
                    }
                }
            }

        }
    }

    private fun onKeyPressed(event: KeyEvent) {
        if (event.code == KeyCode.ENTER) {
            editItem()
            event.consume()
        }
    }

    private fun editItem() {
        val item = selectionModel.selectedItem ?: return

        if (item is IResourceItem) {
            MainWindow.instance.openTab(item.name, item.resource)
        } else {
            item.isExpanded = !item.isExpanded
        }
    }


    override fun resourceAdded(resource: Any, name: String) {
        fun scan(parent: GroupItem) {
            parent.resourceAdded(resource, name)

            for (child in parent.children) {
                if (child is GroupItem) {
                    scan(child)
                }
            }
        }
        scan(root as GroupItem)
    }

    override fun resourceRemoved(resource: Any, name: String) {
        fun scan(parent: GroupItem) {
            parent.resourceRemoved(resource, name)

            for (child in parent.children) {
                if (child is GroupItem) {
                    scan(child)
                }
            }
        }
        scan(root as GroupItem)
    }

    override fun resourceChanged(resource: Any) {

        fun scan(parent: GroupItem) {
            for (child in parent.children) {
                if (child is GroupItem) {
                    scan(child)
                }
                if (child is IResourceItem && child.resource == resource) {
                    if (child is ResourcesListener) {
                        child.resourceChanged(resource)
                    } else {
                        child.updateItem()
                    }
                    return
                }
            }
        }
        scan(root as GroupItem)
    }

    override fun resourceRenamed(resource: Any, oldName: String, newName: String) {

        fun scan(parent: GroupItem) {
            for (child in parent.children) {
                if (child is GroupItem) {
                    scan(child)
                }
                if (child is IResourceItem && child.resource == resource) {
                    parent.children.remove(child)
                    if (child.resource === resource) {
                        child.name = newName
                        child.updateItem()
                    }
                    child.name = newName
                    child.updateItem()
                    parent.add(child)
                    return
                }
            }
        }
        scan(root as GroupItem)
    }

    override fun resourcesReloaded() {
        build()
    }


    /**
     * The base class for [GroupItem], such as [LayoutsItem], as well as
     * weirder items, such as [CostumeItem], which is a parent for [PoseItem] and [SoundItem].
     *
     * The [children] items are lazily rebuilt, by calling [rebuild] when this item is expanded,
     * and cleared when this item is contracted.
     */
    abstract inner class ParentItem(val resourceType: ResourceType, var name: String) :
        TreeItem<String>(name, resourceType.createImageView()), ResourcesListener {

        /**
         * The [children] are rebuilt whenever we expand this item.
         * The [children] are then cleared when we contract this item.
         * Not only does this improve initialisation time, it also reduces the work required each time
         * a resource is added/renamed etc. (because we only need to update children is they are expanded.
         * Also, if the children get out-of-sync, then a contract and expand will automatically re-sync them.
         */
        private var isChildrenVisible = false

        init {
            expandedProperty().addListener { _, _, newValue ->
                if (newValue == false) {
                    children.clear()
                    isChildrenVisible = false
                } else {
                    children.clear()
                    rebuild()
                }
            }
        }

        abstract fun rebuild()

        abstract fun childCount(): Int

        /**
         * Menu items, which are applicable to this TreeItem, as well as our children.
         * For example [PosesItem] will have a menu "Create Pose", and we want this item
         * to also appear in the context menu for each pose.
         *
         * See [IResourceItem.appendParentMenuItems]
         */
        abstract fun parentMenuItems(): List<MenuItem>

        override fun resourceAdded(resource: Any, name: String) {
        }

        fun updateItem() {
            val count = childCount()
            value = if (count == 0) name else "$name ($count)"
        }

        /**
         * Adds [child] into [children] such that the children remain in alphabetical order.
         */
        fun add(child: TreeItem<String>) {
            val childRI = child as IResourceItem
            for (i in children.indices) {
                val other = children[i]
                if (other is IResourceItem) {
                    if (childRI.name < other.name) {
                        children.add(i, child)
                        updateItem()
                        return
                    }
                }
            }
            children.add(child)

        }

        override fun isLeaf() = false

    }

    /**
     * For "groups" which contain items all of one [ResourceType], and which is a [ResourcesListener],
     * so that my children can be updated/add/removed when a resource is updated/added/removed.
     *
     * [LayoutsItem] is a typical implementation.
     */
    abstract inner class GroupItem(resourceType: ResourceType, name: String) :
        ParentItem(resourceType, name) {

        override fun parentMenuItems(): List<MenuItem> {
            if (resourceType in listOf(
                    ResourceType.TEXTURE,
                    ResourceType.POSE,
                    ResourceType.COSTUME,
                    ResourceType.COSTUME_GROUP,
                    ResourceType.SOUND,
                    ResourceType.FONT,
                    ResourceType.LAYOUT,
                    ResourceType.COSTUME_GROUP,
                    ResourceType.INPUT,
                    ResourceType.SCRIPT,
                    ResourceType.SCENE,
                    ResourceType.MACRO
                )
            ) {
                return listOf(
                    MenuItem("New ${resourceType.label}").apply {
                        onAction = EventHandler {
                            TaskPrompter(NewResourceTask(resourceType)).placeOnStage(Stage())
                        }
                    }
                )
            }
            return emptyList()
        }


        open fun createContextMenu(): ContextMenu? {
            val parentItems = parentMenuItems()
            return if (parentItems.isEmpty()) {
                null
            } else {
                ContextMenu().apply {
                    items.addAll(parentItems)
                }
            }
        }


        override fun resourceAdded(resource: Any, name: String) {
            if (ResourceType.resourceType(resource) == resourceType) {
                add(createResourceItem(name, resource))
            }
        }

        abstract fun createResourceItem(name: String, resource: Any): TreeItem<String>

        override fun resourceChanged(resource: Any) {
            if (ResourceType.resourceType(resource) == resourceType) {
                if (ResourceType.resourceType(resource) == resourceType) {
                    for (child in children) {
                        if (child is IResourceItem && child.resource === resource) {
                            child.updateItem()
                        }
                    }
                }
            }
        }

        override fun resourceRemoved(resource: Any, name: String) {
            if (ResourceType.resourceType(resource) == resourceType) {
                for (child in children) {
                    if (child is IResourceItem && child.resource === resource) {
                        children.remove(child)
                        break
                    }
                }
            }
        }

        override fun resourceRenamed(resource: Any, oldName: String, newName: String) {
            if (ResourceType.resourceType(resource) == resourceType) {
                for (child in children) {
                    if (child is IResourceItem && child.resource === resource) {
                        child.updateItem()
                    }
                }
            }
        }

    }

    /**
     * TreeItems which are for a resource. If they are a simple item, then they will be a suclass of TreeItem<String>.
     * However, if they are also a PARENT, then they are a subclass of [GroupItem].
     * Because TreeItem is a class, and not an interface, we can't inherit from both :-(
     */
    interface IResourceItem {
        val resource: Any

        var name: String

        /**
         * Called when the resource has changed, or when children have been added, so the label
         * needs to be updated.
         */
        fun updateItem()

        fun createContextMenu(): ContextMenu?

        /**
         * A child [TreeItem], such as [PoseItem] will call this to include menu items from its parent [TreeItem]
         * For example, a [PoseItem] will append the menu item "Create Pose" from its parent : [PosesItem].
         *
         * I do this, so that we never need to scroll up the tree to find the parent node. Instead, we
         * can create a pose from any existing pose (as well as the [PosesItem])
         */
        fun appendParentMenuItems(menu: ContextMenu?): ContextMenu? {
            val menu2 = menu ?: ContextMenu()
            if (this is TreeItem<*>) {
                val par = this.parent
                if (par is ParentItem) {
                    // Yep, we can merge them!
                    val extraItems = par.parentMenuItems()
                    if (extraItems.isNotEmpty() && menu2.items.isNotEmpty()) {
                        menu2.items.add(SeparatorMenuItem())
                    }
                    menu2.items.addAll(extraItems)
                }
            }
            return if (menu2.items.isEmpty()) null else menu2
        }

    }


    /**
     * A item in the tree which has no children.
     */
    abstract inner class SimpleResourceItem(
        override var name: String,
        override val resource: Any,
        graphic: Node? = null

    ) : TreeItem<String>(name, graphic ?: graphicForResource(resource)), IResourceItem {

        override fun updateItem() {
            value = name
        }

        override fun isLeaf() = true

        override fun createContextMenu(): ContextMenu? {
            return createContextMenu(name, resource)
        }

    }

    /**
     * The root, for the "All Resources" tree.
     * The root of the single-type trees does NOT use this, instead, they used [CostumesItem] etc.
     * (which are the also used as children of this item).
     */
    inner class RootItem : GroupItem(ResourceType.ANY, resources.file.nameWithoutExtension) {


        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            throw java.lang.IllegalArgumentException("Root does not have Resources")
        }

        override fun parentMenuItems() = emptyList<MenuItem>()

        override fun childCount() = 0 // We don't show a count for the root item.

        override fun rebuild() {
            children.clear()
            children.addAll(
                GameInfoItem(),
                TexturesItem(),
                PosesItem(),
                CostumesItem(),
                FontResourcesItem(),
                SoundsItem(),
                InputsItem(),
                LayoutsItem(),
                ScenesDirectoryItem("Scenes", resources.sceneDirectory.absoluteFile),
                ScriptsDirectoryItem("Scripts", resources.scriptDirectory().absoluteFile),
                MacrosDirectoryItem("Macros", resources.macrosDirectory.absoluteFile)
            )

        }

        override fun isLeaf() = false

    }

    inner class GameInfoItem : SimpleResourceItem("Game Info", resources.gameInfo)


    inner class TexturesItem : GroupItem(ResourceType.TEXTURE, "Textures") {

        init {
            updateItem()
        }

        override fun childCount() = resources.textures.items().size

        override fun rebuild() {
            resources.textures.items().map { it }.sortedBy { it.key }
                .forEach { (name, texture) ->
                    children.add(TextureItem(name, texture))
                }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            val texture = resource as Texture
            return TextureItem(name, texture)
        }
    }

    inner class TextureItem(name: String, val texture: Texture) :
        GroupItem(ResourceType.TEXTURE, name),
        IResourceItem {

        override val resource: Any
            get() = texture

        init {
            updateItem()
        }

        override fun childCount() = resources.poses.items().count { it.value.texture === texture }

        override fun rebuild() {
            children.clear()
            resources.poses.items().filter { it.value.texture === texture }.map { it }
                .sortedBy { it.key }
                .forEach { (name, pose) ->
                    children.add(PoseItem(name, pose))
                }
        }

        override fun createContextMenu(): ContextMenu? {
            val menu = super.createContextMenu() ?: ContextMenu()
            menu.merge(createContextMenu(name, resource))

            return appendParentMenuItems(menu)
        }

        override fun resourceAdded(resource: Any, name: String) {
            // Do nothing
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            val pose = resource as Pose
            return PoseItem(name, pose)
        }

    }

    inner class CostumesItem : GroupItem(ResourceType.COSTUME, "Costumes") {

        init {
            updateItem()
        }

        override fun childCount() = resources.costumes.items().size

        override fun rebuild() {
            resources.costumeGroups.items().map { it }.sortedBy { it.key }
                .forEach { (name, costume) ->
                    children.add(CostumeGroupItem(name, costume))
                }

            resources.costumes.items().filter { it.value.costumeGroup == null }.map { it }
                .sortedBy { it.key }
                .forEach { (name, costume) ->
                    children.add(CostumeItem(name, costume))
                }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            val costume = resource as Costume
            return CostumeItem(name, costume)
        }

        override fun resourceAdded(resource: Any, name: String) {
            if (ResourceType.resourceType(resource) == resourceType) {
                val costume = resource as? Costume ?: return
                updateItem()
                if (costume.costumeGroup == null) {
                    add(createResourceItem(name, resource))
                }
            }
        }

        override fun resourceRemoved(resource: Any, name: String) {
            if (ResourceType.resourceType(resource) == resourceType) {
                val costume = resource as? Costume ?: return
                updateItem()
                if (costume.costumeGroup == null) {
                    super.resourceRemoved(resource, name)
                }
            }
        }
    }

    inner class CostumeGroupItem(name: String, override val resource: CostumeGroup) :
        GroupItem(ResourceType.COSTUME_GROUP, name), IResourceItem {

        init {
            updateItem()
        }

        override fun childCount() = resource.items().size

        override fun rebuild() {
            resource.items().map { it }.sortedBy { it.key }
                .forEach { (name, costume) ->
                    children.add(CostumeItem(name, costume))
                }
        }

        override fun createContextMenu(): ContextMenu? {
            val menu = super.createContextMenu() ?: ContextMenu()
            menu.items.add(0, MenuItem("Rename $name").apply {
                onAction = EventHandler {

                    (resource as RenamableResource).renamePrompted(name)
                }
            })

            menu.items.add(1, MenuItem("Delete $name").apply {
                onAction = EventHandler {
                    resource.deletePrompted(name)
                }
            })

            return appendParentMenuItems(menu)
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return CostumeItem(name, resource as Costume)
        }


        override fun resourceAdded(resource: Any, name: String) {
            if (ResourceType.resourceType(resource) == resourceType) {
                val costume = resource as? Costume ?: return
                if (costume.costumeGroup === this.resource) {
                    add(createResourceItem(name, resource))
                }
            }
        }

        override fun resourceRemoved(resource: Any, name: String) {
            if (ResourceType.resourceType(resource) == resourceType) {
                val costume: Costume = resource as? Costume ?: return
                if (costume.costumeGroup === this.resource) {
                    super.resourceRemoved(resource, name)
                }
            }
        }
    }

    inner class CostumeItem(name: String, val costume: Costume) :
        ParentItem(ResourceType.COSTUME, name),
        IResourceItem {

        override val resource: Any
            get() = costume

        override fun childCount() = 0 // Do not show a count for events or Role script.

        override fun rebuild() {
            children.clear()
            // Add the Role's script as a child item.
            ScriptManager.scriptFile(costume.roleString)?.let { scriptFile ->
                val scriptStub = ScriptStub(scriptFile)
                children.add(ScriptItem(scriptStub.name, scriptStub))
            }

            costume.events.forEach { (eventName, costumeEvent) ->
                for (pose in costumeEvent.poses) {
                    val resourceName = Editor.resources.findNameOrNull(pose) ?: "?"
                    val simple = PoseItem(resourceName, pose)
                    children.add(CostumeEventItem(costume, eventName, simple))
                }
                for (sound in costumeEvent.sounds) {
                    val resourceName = Editor.resources.findNameOrNull(sound) ?: "?"
                    val simple = SoundItem(resourceName, sound)
                    children.add(CostumeEventItem(costume, eventName, simple))
                }
            }
        }

        override fun parentMenuItems(): List<MenuItem> = emptyList()

        override fun resourceChanged(resource: Any) {
            if (resource === costume) {
                rebuild()
                super.resourceChanged(resource)
            } else if (resource is Pose || resource is Sound) {
                // We have different types of children, so we cannot use super's implementation.
                for (child in children) {
                    if (child is IResourceItem && child.resource === resource) {
                        child.updateItem()
                    }
                }
            }
        }

        override fun createContextMenu(): ContextMenu? {
            val menu = createContextMenu(name, resource) ?: ContextMenu()

            val klass = costume.roleClass()
            if (klass != null) {
                val scriptFile = ScriptManager.scriptFile(klass.name)
                if (scriptFile != null) {
                    menu.items.add(MenuItem("Edit Script").apply {
                        onAction = EventHandler {
                            MainWindow.instance.openTab(costume.roleString, ScriptStub(scriptFile))
                        }
                    })
                }
            }
            return appendParentMenuItems(menu)
        }

    }

    /**
     * Wraps a SimpleResourceItem, so that it is suitable as a child of CostumeItem for each of the [Costume]'s
     * events.
     *
     * For example a mockItem could be a [PoseItem] or [SoundItem].
     */
    inner class CostumeEventItem(
        private val costume: Costume,
        private val eventName: String,
        private val mockItem: SimpleResourceItem
    ) :
        SimpleResourceItem(mockItem.name, mockItem.resource) {

        init {
            updateItem()
        }

        override fun updateItem() {
            value = "$eventName->${mockItem.name}"
            if (resource is Pose) {
                val iv = resource.toImageView()
                graphic = if (iv != null) {
                    iv.isPreserveRatio = true
                    if (iv.viewport.width > iv.viewport.height) {
                        iv.fitWidth = 24.0
                    } else {
                        iv.fitHeight = 24.0
                    }
                    iv
                } else {
                    graphicForResource(resource)
                }
            }
        }

        override fun createContextMenu(): ContextMenu {
            val menu = mockItem.createContextMenu() ?: ContextMenu()
            with(menu.items) {
                if (size != 0) {
                    add(0, SeparatorMenuItem())
                }
                add(0, MenuItem("Remove $value").apply {
                    onAction = EventHandler {
                        val tab = MainWindow.instance.findTab(costume) as? CostumeTab
                        if (tab == null) {

                            // CostumeTab is not open, so we can delete the event directly
                            costume.events[eventName]?.let { event ->
                                when (resource) {
                                    is Sound -> event.soundEvents.removeIf { it.sound === resource }
                                    is Pose -> event.poses.removeIf { it === resource }
                                    else -> Unit // Do nothing
                                }
                            }
                            Editor.resources.fireChanged(costume)
                        } else {
                            tab.removeEvent(eventName, resource)
                            // The CostumeTab is open, so tell the tab to remove the event.
                            // We don't fire a change event, because the change will only occur when
                            // the "Save" or "Apply" buttons are pressed.
                            // Instead, select the costume tab.
                            MainWindow.instance.openTab(tab.dataName, costume)
                        }
                    }
                })
            }
            return menu
        }
    }

    inner class PosesItem : GroupItem(ResourceType.POSE, "Poses") {

        init {
            updateItem()
        }

        override fun childCount() = resources.poses.items().size

        override fun rebuild() {
            resources.poses.items().map { it }.sortedBy { it.key }
                .forEach { (name, pose) ->
                    children.add(PoseItem(name, pose))
                }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return PoseItem(name, resource as Pose)
        }
    }

    inner class PoseItem(name: String, val pose: Pose) : SimpleResourceItem(name, pose) {

        init {
            updateItem() // Set the graphic.
        }

        /**
         * Also update the graphic! This is called whenever the [Pose] changes, so if the rect changes, the
         * graphic will change too.
         */
        override fun updateItem() {
            super.updateItem()
            val iv = pose.toImageView()
            graphic = if (iv != null) {
                iv.isPreserveRatio = true
                if (iv.viewport.width > iv.viewport.height) {
                    iv.fitWidth = 24.0
                } else {
                    iv.fitHeight = 24.0
                }
                iv
            } else {
                graphicForResource(resource)
            }
        }

        override fun createContextMenu(): ContextMenu? {
            val menu = super.createContextMenu() ?: ContextMenu()
            with(menu.items) {
                add(MenuItem("Create Costume").apply {

                    onAction = EventHandler {
                        val task = NewResourceTask(pose, name)
                        task.prompt()
                    }
                })
            }
            return appendParentMenuItems(menu)
        }

    }


    inner class FontResourcesItem : GroupItem(ResourceType.FONT, "Fonts") {

        init {
            updateItem()
        }

        override fun childCount() = resources.fontResources.items().size
        override fun rebuild() {
            resources.fontResources.items().map { it }
                .sortedBy { it.key }
                .forEach { (name, fontResource) ->
                    children.add(FontResourceItem(name, fontResource))
                }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return FontResourceItem(name, resource as FontResource)
        }

    }

    inner class FontResourceItem(name: String, val fontResource: FontResource) :
        SimpleResourceItem(name, fontResource) {

        override fun createContextMenu(): ContextMenu? {
            return appendParentMenuItems(super.createContextMenu())
        }
    }


    inner class SoundsItem : GroupItem(ResourceType.SOUND, "Sounds") {

        init {
            updateItem()
        }

        override fun childCount() = resources.sounds.items().size

        override fun rebuild() {
            resources.sounds.items().map { it }.sortedBy { it.key }
                .forEach { (name, sound) ->
                    children.add(SoundItem(name, sound))
                }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return SoundItem(name, resource as Sound)
        }
    }

    inner class SoundItem(name: String, val sound: Sound) : SimpleResourceItem(name, sound) {

        override fun createContextMenu(): ContextMenu? {
            return appendParentMenuItems(super.createContextMenu())
        }

    }


    inner class InputsItem : GroupItem(ResourceType.INPUT, "Inputs") {

        init {
            resources.inputs.items().map { it }.sortedBy { it.key }
                .forEach { (name, input) ->
                    children.add(InputItem(name, input))
                }
            updateItem()
        }

        override fun childCount() = resources.inputs.items().size

        override fun rebuild() {
            children.clear()
            resources.inputs.items().map { it }.sortedBy { it.key }
                .forEach { (name, input) ->
                    children.add(InputItem(name, input))
                }
            updateItem()
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return InputItem(name, resource as Input)
        }
    }

    inner class InputItem(name: String, val input: Input) : SimpleResourceItem(name, input) {

        override fun createContextMenu(): ContextMenu? {
            return appendParentMenuItems(super.createContextMenu())
        }

    }


    inner class LayoutsItem : GroupItem(ResourceType.LAYOUT, "Layouts") {

        init {
            updateItem()
        }

        override fun childCount() = resources.layouts.items().size

        override fun rebuild() {
            resources.layouts.items().map { it }.sortedBy { it.key }
                .forEach { (name, layout) ->
                    children.add(LayoutItem(name, layout))
                }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return LayoutItem(name, resource as Layout)
        }
    }

    inner class LayoutItem(name: String, val layout: Layout) : SimpleResourceItem(name, layout) {

        override fun createContextMenu(): ContextMenu? {
            return appendParentMenuItems(super.createContextMenu())
        }

    }


    inner class ScenesDirectoryItem(name: String, private val dir: File) :
        GroupItem(ResourceType.SCENE_DIRECTORY, name) {

        override fun childCount() = 0

        override fun rebuild() {
            children.clear()
            for (subDir in FileLister(depth = 1, onlyFiles = false).listFiles(dir)) {
                children.add(ScenesDirectoryItem(subDir.name, subDir))
            }
            for (scene in FileLister(depth = 1, onlyFiles = true).listFiles(dir)) {
                children.add(SceneItem(scene.nameWithoutExtension, SceneStub(scene)))
            }
            updateItem()
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return SceneItem(name, resource as SceneStub)
        }

        override fun resourceAdded(resource: Any, name: String) {
            if (resource is SceneStub) {
                if (resource.file.parentFile == dir) {
                    add(createResourceItem(name, resource))
                }
            }
        }

        override fun resourceRemoved(resource: Any, name: String) {
            if (resource is SceneStub) {
                if (resource.file.parentFile == dir) {
                    super.resourceRemoved(resource, name)
                }
            }
        }

        override fun createContextMenu(): ContextMenu {
            return ContextMenu().apply {
                items.add(
                    MenuItem("Refresh").apply {
                        onAction = EventHandler { rebuild() }
                    }
                )
                items.addAll(parentMenuItems())
            }
        }

        override fun parentMenuItems(): List<MenuItem> {
            return listOf(
                MenuItem("New Scene").apply {
                    onAction = EventHandler {
                        val nst = NewResourceTask(ResourceType.SCENE).apply {
                            sceneDirectoryP.value = dir
                        }
                        TaskPrompter(nst).placeOnStage(Stage())
                    }
                },
                MenuItem("New Directory").apply {
                    onAction = EventHandler {
                        val nst = NewResourceTask(ResourceType.SCENE_DIRECTORY).apply {
                            sceneDirectoryP.value = dir
                        }
                        TaskPrompter(nst).placeOnStage(Stage())
                    }
                }
            )
        }

    }

    inner class SceneItem(name: String, private val sceneStub: SceneStub) :
        ParentItem(ResourceType.SCENE, name), IResourceItem {

        override val resource
            get() = sceneStub

        override fun createContextMenu(): ContextMenu? {
            return appendParentMenuItems(createContextMenu(name, sceneStub))
        }

        override fun childCount() = 0

        override fun rebuild() {
            val scene = SceneReader.load(sceneStub.file, Editor.resources, mergeIncludes = false)
            val scriptFile = ScriptManager.scriptFile(scene.director.javaClass.name)
            if (scriptFile != null) {
                children.add(ScriptItem(scriptFile.nameWithoutExtension, ScriptStub(scriptFile)))
            }
        }

        override fun parentMenuItems(): List<MenuItem> = emptyList()
    }


    inner class ScriptsDirectoryItem(name: String, private val dir: File) :
        GroupItem(ResourceType.SCRIPT, name) {

        init {
            updateItem()
        }

        override fun childCount() = 0

        override fun rebuild() {
            children.clear()
            for (subDir in FileLister(depth = 1, onlyFiles = false).listFiles(dir)) {
                children.add(ScriptsDirectoryItem(subDir.name, subDir))
            }
            for (script in FileLister(depth = 1, onlyFiles = true).listFiles(dir)) {
                children.add(ScriptItem(script.nameWithoutExtension, ScriptStub(script)))
            }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return ScriptItem(name, resource as ScriptStub)
        }

        override fun createContextMenu(): ContextMenu {
            val menu = super.createContextMenu() ?: ContextMenu()
            with(menu.items) {
                add(MenuItem("Refresh").apply {
                    onAction = EventHandler {
                        rebuild()
                    }
                })
            }
            return menu
        }
    }

    inner class ScriptItem(name: String, scriptStub: ScriptStub) : SimpleResourceItem(name, scriptStub) {
        override fun createContextMenu(): ContextMenu? {
            return appendParentMenuItems(ContextMenu())
        }
    }


    inner class MacrosDirectoryItem(name: String, private val dir: File) :
        GroupItem(ResourceType.MACRO, name) {

        init {
            updateItem()
        }

        override fun childCount() = 0

        override fun rebuild() {
            children.clear()
            for (subDir in FileLister(depth = 1, onlyFiles = false).listFiles(dir)) {
                children.add(MacrosDirectoryItem(subDir.name, subDir))
            }
            for (macro in FileLister(depth = 1, onlyFiles = true).listFiles(dir)) {
                children.add(MacroItem(macro.nameWithoutExtension, MacroStub(macro)))
            }
        }

        override fun createResourceItem(name: String, resource: Any): TreeItem<String> {
            return MacroItem(name, resource as MacroStub)
        }

        override fun createContextMenu(): ContextMenu {
            val menu = super.createContextMenu() ?: ContextMenu()
            with(menu.items) {
                add(MenuItem("Refresh").apply {
                    onAction = EventHandler {
                        rebuild()
                    }
                })
            }
            return menu
        }
    }

    inner class MacroItem(name: String, scriptStub: MacroStub) : SimpleResourceItem(name, scriptStub) {
        override fun createContextMenu(): ContextMenu? {
            return appendParentMenuItems(ContextMenu())
        }
    }

}

/**
 * The "resource" for an API page.
 */
object APIStub

/**
 * I think I used this when drag/drop Costumes/Poses/Sounds onto a Costume, to add it as a Costume event.
 */
class NewCostumeEventTask(private val costume: Costume, private val resourceInfo: ResourceInfo) : AbstractTask(false) {


    private val eventNameP = StringParameter("eventName", required = true)

    override val taskD = TaskDescription("newEvent")
        .addParameters(eventNameP)

    override fun run() {
        val tab =
            MainWindow.instance.tabPane.tabs.firstOrNull { it is CostumeTab && it.costume === costume } as? CostumeTab
        tab?.save()

        val ea = if (costume.events.contains(eventNameP.value)) {
            costume.events[eventNameP.value]!!
        } else {
            val newEvent = CostumeEvent(costume)
            costume.events[eventNameP.value] = newEvent
            newEvent
        }

        when (resourceInfo.resourceType) {
            ResourceType.COSTUME -> Editor.resources.costumes.find(resourceInfo.name)?.let { ea.costumes.add(it) }
            ResourceType.POSE -> Editor.resources.poses.find(resourceInfo.name)?.let { ea.poses.add(it) }
            ResourceType.SOUND -> Editor.resources.sounds.find(resourceInfo.name)
                ?.let { ea.soundEvents.add(SoundEvent(it)) }

            ResourceType.FONT -> Editor.resources.fontResources.find(resourceInfo.name)?.let {
                ea.textStyles.add(
                    TextStyle(
                        it,
                        TextHAlignment.CENTER,
                        TextVAlignment.CENTER,
                        Color.white(),
                        null
                    )
                )
            }

            else -> throw IllegalArgumentException("Unexpected resource type : ${resourceInfo.resourceType}")
        }

        Editor.resources.fireChanged(costume)

        tab?.eventsTask?.buildEventsP()
    }

}


private fun graphicForResource(resource: Any?) = ResourceType.resourceType(resource)?.createImageView()

interface DragTarget {

    fun dragOver(event: DragEvent)
    fun dragDropped(event: DragEvent)
}


/**
 * Code common to SimpleResourceItem and other items, which cannot extend SimpleResourceItem,
 * because they are a parent too.
 */
private fun createContextMenu(name: String, resource: Any): ContextMenu? {
    val menu = ContextMenu()
    if (resource is CopyableResource<*>) {
        menu.items.add(MenuItem("Copy $name").apply {
            onAction = EventHandler {
                resource.copyPrompted(name)
            }
        })
    }
    if (resource is RenamableResource) {
        menu.items.add(MenuItem("Rename $name").apply {
            onAction = EventHandler {
                resource.renamePrompted(name)
            }
        })
    }
    if (resource is DeletableResource) {
        menu.items.add(MenuItem("Delete $name").apply {
            onAction = EventHandler {
                resource.deletePrompted(name)
            }
        })
    }
    return if (menu.items.isEmpty()) null else menu
}

private fun ContextMenu.merge(other: ContextMenu?) {
    if (other == null) return

    if (items.isNotEmpty() && other.items.isNotEmpty()) {
        items.add(SeparatorMenuItem())
    }

    for (item in other.items.toList()) {
        other.items.remove(item)
        items.add(item)
    }
}
