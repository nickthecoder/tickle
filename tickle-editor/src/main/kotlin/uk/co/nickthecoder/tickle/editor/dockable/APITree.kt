/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.event.EventHandler
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.image.ImageView
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.tabs.APITab
import uk.co.nickthecoder.tickle.editor.tabs.TextViewer
import uk.co.nickthecoder.tickle.editor.util.ClassMetaData
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences
import uk.co.nickthecoder.tickle.events.Input
import uk.co.nickthecoder.tickle.feather.FeatherLanguage
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.neighbourhood.Neighbourhood
import uk.co.nickthecoder.tickle.path.Path
import uk.co.nickthecoder.tickle.physics.TickleBody
import uk.co.nickthecoder.tickle.physics.TickleMouseJoint
import uk.co.nickthecoder.tickle.physics.TicklePinJoint
import uk.co.nickthecoder.tickle.physics.TickleWorld
import uk.co.nickthecoder.tickle.stage.Stage
import uk.co.nickthecoder.tickle.stage.StageView
import uk.co.nickthecoder.tickle.stage.View
import uk.co.nickthecoder.tickle.util.TagManager
import uk.co.nickthecoder.tickle.util.Vector2
import java.io.File

/**
 * Lists packages and classes which make up Tickle.
 * The tree is placed inside an [APIDockable], which is in turn used by the [APITab].
 * Clicking on one of the package or classes in the list shows the appropriate API documentation
 * via the web (the API documentation is NOT stored locally).
 */
class APITree : HBox() {

    private val baseUrl = EditorPreferences.tickleDokkaURL
    val packageTree = TreeView<String>()
    val byTypeTree = TreeView<String>()
    val mainTree = TreeView<String>()

    init {
        with(packageTree) {
            isEditable = false
            root = RootOfPackageTree()
            root.children
            root.isExpanded = true
            isShowRoot = false
            onMouseClicked = EventHandler { onTreeClicked(it, packageTree) }
        }
        with(byTypeTree) {
            isEditable = false
            root = RootOfByTypeTree()
            root.children
            root.isExpanded = true
            isShowRoot = false
            onMouseClicked = EventHandler { onTreeClicked(it, byTypeTree) }
        }
        with(mainTree) {
            isEditable = false
            isShowRoot = false
            root = RootOfMainTree()
            onMouseClicked = EventHandler { onTreeClicked(it, mainTree) }
            root.children.add(FeatherExtrasItem())
            root.children.addAll(
                listOf(
                    Game::class.java, Scene::class.java, Actor::class.java, Appearance::class.java, Role::class.java,
                    Director::class.java, Producer::class.java,
                    Stage::class.java, View::class.java, StageView::class.java,
                    Pose::class.java, Costume::class.java, Input::class.java,
                    ActionRole::class.java,
                    Action::class.java, Extras::class.java, SceneTransition::class.java,
                    TagManager::class.java, Neighbourhood::class.java, Path::class.java,
                    TickleWorld::class.java, TickleBody::class.java,
                    TicklePinJoint::class.java, TickleMouseJoint::class.java,
                    Color::class.java, Vector2::class.java, Window::class.java,
                    MouseEvent::class.java, KeyEvent::class.java, Input::class.java

                ).sortedBy { it.simpleName }.map { ClassItem(it.packageName, it.simpleName) }
            )
        }

        val packageTab = Tab("All Classes").apply { content = packageTree }
        val byTypeTab = Tab("By Type").apply { content = byTypeTree }
        val mainTab = Tab("Main Classes").apply { content = mainTree }

        val tabPane = TabPane().apply {
            tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE
            tabs.addAll(mainTab, packageTab, byTypeTab)
        }


        this.children.add(tabPane)
        this.isFillHeight = true
        setHgrow(tabPane, Priority.ALWAYS)

    }

    private fun onTreeClicked(event: MouseEvent, tree: TreeView<String>) {
        if (event.clickCount == 2) {
            when (val item = tree.selectionModel.selectedItem) {
                is ClassItem -> {
                    // Cannot use Ctrl key, because JavaFX uses that to deselect the item. Grr.
                    item.show(newTab = event.isShiftDown)
                }

                is FeatherExtrasItem -> {
                    item.show(newTab = event.isShiftDown)
                }
            }
            event.consume()
        }
    }


    inner class RootOfPackageTree : PackageItem("") {
        init {
            children.add(ProjectItem("uk.co.nickthecoder.tickle").apply { isExpanded = true })
        }
    }

    inner class ProjectItem(value: String) : PackageItem(value) {

        val packageItems = mutableMapOf<String, PackageItem>(value to this)

        init {
            for (classNames in ClassMetaData.simpleClassNameToNames.values) {
                for (className in classNames) {
                    if (className.startsWith(value)) {
                        createClassItem(className)
                    }
                }
            }
            children.addAll(packageItems.values.filter { it != this }.sortedBy { it.value })
            packageItems.values.forEach { it.createChildren() }
        }

        fun accept(className: String): Boolean {
            // Ignore inner classes
            if (className.contains('$')) {
                return false
            }
            // Ignore classes from certain packages (which are of no use to game programmers).
            for (pack in excludePackages) {
                if (className.startsWith(pack)) {
                    return false
                }
            }
            return true
        }

        private fun createClassItem(className: String) {
            val lastDot = className.lastIndexOf(".")
            val packageName = className.substring(0, lastDot)

            val simpleName = className.substring(lastDot + 1)
            var packageItem = packageItems[packageName]
            if (packageItem == null) {
                packageItem = PackageItem(packageName, packageName.replace("$value.", ""))
                packageItems[packageName] = packageItem
            }
            packageItem.addClass(simpleName)

        }
    }


    abstract inner class APITreeItem(val packageName: String, name: String)

        : TreeItem<String>(name) {

        open fun show(newTab: Boolean) {
            val tab = MainWindow.instance.tabPane.selectionModel.selectedItem
            if (newTab || tab !is APITab) {
                val apitab = APITab(packageName, className())
                MainWindow.instance.tabPane.tabs.add(apitab)
                MainWindow.instance.tabPane.selectionModel.select(apitab)
            } else {
                tab.goto(packageName, className())
            }
        }

        abstract fun className(): String?
    }

    inner class FeatherExtrasItem : APITreeItem("uk.co.nickthecoder.tickle.feather", "FeatherExtras") {

        init {
            graphic = ImageView(Editor.imageResource("script.png"))
        }

        override fun show(newTab: Boolean) {
            val url = FeatherLanguage::class.java.getResource("FeatherExtras.feather")
            val tab = TextViewer("FeatherExtras.feather", url!!)
            MainWindow.instance.tabPane.tabs.add(tab)
            MainWindow.instance.tabPane.selectionModel.select(tab)
        }

        override fun className() = "FeatherExtras"

    }

    open inner class PackageItem(packageName: String, displayName: String = packageName)

        : APITreeItem(packageName, displayName) {

        private val classItems = mutableListOf<ClassItem>()

        init {
            graphic = ImageView(Editor.imageResource("folder.png"))
        }

        fun addClass(simpleName: String) {
            classItems.add(ClassItem(packageName, simpleName))
        }

        fun createChildren() {
            children.addAll(classItems.sortedBy { it.value })
        }

        override fun className(): String? = null
    }

    inner class RootOfByTypeTree : APITreeItem("", "") {
        init {
            for (type in ClassMetaData.types.keys.sorted()) {
                val dot = type.lastIndexOf(".")
                children.add(TypeItem(type.substring(0, dot), type.substring(dot + 1)))
            }
        }

        override fun className(): String? = null

    }

    inner class RootOfMainTree : APITreeItem("", "") {

        override fun className(): String? = null

    }

    open inner class TypeItem(packageName: String, simpleName: String)

        : APITreeItem(packageName, simpleName) {

        init {
            graphic = ImageView(Editor.imageResource("folder.png"))
            for (className in ClassMetaData.types["${packageName}.${simpleName}"]!!) {
                val file = File(className)
                children.add(ClassItem(file.nameWithoutExtension, file.extension))
            }
        }

        override fun className(): String? = null
    }

    inner class ClassItem(packageName: String, val simpleName: String)

        : APITreeItem(packageName, simpleName) {

        init {
            graphic = ImageView(Editor.imageResource("class.png"))
        }

        override fun className() = simpleName
    }


    companion object {
        val excludePackages = listOf(
            "uk.co.nickthecoder.tickle.editor",
            "uk.co.nickthecoder.tickle.kotlin",
            "uk.co.nickthecoder.tickle.demo"
        )
    }
}
