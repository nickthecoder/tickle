package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.editor.dockable.ActorAttributesForm
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.util.toTickle
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.TextHAlignment
import uk.co.nickthecoder.tickle.graphics.TextVAlignment
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.warn

/**
 * A Change to the TextStyle on the [ActorAttributesForm].
 */
class ChangeTextStyleAttribute(
    private val actor: Actor,
    private val parameterName: String,
    private val oldValue: Any?,
    private var newValue: Any?
) : Change {

    override fun redo(sceneEditor: SceneEditor) {
        setValue(newValue)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        setValue(newValue)
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    private fun setValue(value: Any?) {
        actor.textAppearance?.let { textAppearance ->
            val style = textAppearance.textStyle
            when (parameterName) {
                "font" -> style.fontResource = value as FontResource
                "color" -> style.color = (value as javafx.scene.paint.Color).toTickle()
                "outlineColor" -> style.outlineColor = (value as javafx.scene.paint.Color).toTickle()
                "hAlignment" -> style.halignment = value as TextHAlignment
                "vAlignment" -> style.valignment = value as TextVAlignment
                else -> warn("Unexpected Actor Parameter $parameterName ")
            }
        }
    }

    override fun mergeWith(other: Change): Boolean {
        if (other is ChangeTextStyleAttribute && other.actor === actor && other.parameterName == parameterName) {
            other.newValue = newValue
            return true
        }
        return false
    }
}
