/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.resources

import uk.co.nickthecoder.tickle.editor.scene.SelectedActor
import uk.co.nickthecoder.tickle.editor.scene.history.Change
import uk.co.nickthecoder.tickle.util.Attribute

/**
 * Snaps actors to a grid
 */
open class GridConstraint : NoStageConstraint() {

    @Attribute
    var xSpacing: Float = 40f

    @Attribute
    var ySpacing: Float = 40f

    override fun snapActor(sa :SelectedActor, dragX: Float, dragY: Float): Boolean {
        // TODO adjust(actorResource, dragX, dragY)
        return true
    }

    override fun addActor(sa : SelectedActor): List<Change> {
        // TODO adjust(actorResource, actorResource.x, actorResource.y)
        return super.addActor(sa)
    }

    //open fun adjust(actorResource: ActorResource, x: Float, y: Float) {
    //    actorResource.x = Math.round((x / xSpacing)) * xSpacing
    //    actorResource.y = Math.round((y / ySpacing)) * ySpacing
    //}

    override fun toString(): String = "${javaClass.simpleName}($xSpacing,$ySpacing)"
}
