/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import javafx.beans.property.SimpleBooleanProperty
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.BooleanParameter
import uk.co.nickthecoder.paratask.parameters.FloatParameter
import uk.co.nickthecoder.paratask.parameters.InformationParameter
import uk.co.nickthecoder.paratask.parameters.MultipleParameter
import uk.co.nickthecoder.tickle.editor.SNAP_TO_GUIDES_TOGGLE
import uk.co.nickthecoder.tickle.util.Vector2

class SnapToGuides : Snap {

    override val enabledProperty = SimpleBooleanProperty(true)

    val xGuides = mutableListOf<Float>()

    val yGuides = mutableListOf<Float>()

    var closeness = 5f

    private val adjustment = SnapToAdjustment()

    fun snapPosition(position: Vector2, adjustments: MutableList<SnapToAdjustment>) {
        if (enabledProperty.value == false) return

        adjustment.reset()

        xGuides.forEach { xGuide ->
            val dx = xGuide - position.x
            if (dx > -closeness && dx < closeness) {
                adjustment.dx = dx
                adjustment.score = Math.abs(dx) + closeness
            }
        }

        yGuides.forEach { yGuide ->
            val dy = yGuide - position.y
            if (dy > -closeness && dy < closeness) {
                adjustment.dy = dy
                adjustment.score =
                    if (adjustment.score == Float.MAX_VALUE) Math.abs(dy) + closeness else adjustment.score + Math.abs(
                        dy
                    )
            }
        }

        if (adjustment.score != Float.MAX_VALUE) {
            adjustments.add(adjustment)
        }
    }

    override fun task() = GuidesTask()

    override fun toString(): String {
        return "Guides enabled=$enabledProperty closeness=$closeness x=$xGuides y=$yGuides"
    }

    inner class GuidesTask : AbstractTask() {

        val enabledP = BooleanParameter("enabled", value = enabledProperty.value)

        val toggleInfoP = InformationParameter(
            "toggleInfo",
            information = "Note. You can toggle guide snapping using the keyboard shortcut : ${SNAP_TO_GUIDES_TOGGLE.shortcutLabel() ?: "<NONE>"}\n${snapInfo()}"
        )

        val xGuidesP = MultipleParameter("xGuides", value = xGuides, isBoxed = true) {
            FloatParameter("x")
        }

        val yGuidesP = MultipleParameter("yGuides", value = yGuides, isBoxed = true) {
            FloatParameter("y")
        }

        val closenessP = FloatParameter("closeness", value = closeness)

        override val taskD = TaskDescription("editGuides")
            .addParameters(enabledP, toggleInfoP, xGuidesP, yGuidesP, closenessP)

        init {
            xGuidesP.value = xGuides
        }

        override fun run() {
            xGuides.clear()
            yGuides.clear()

            xGuidesP.value.forEach { xGuides.add(it!!) }
            yGuidesP.value.forEach { yGuides.add(it!!) }

            closeness = closenessP.value!!
            enabledProperty.value = enabledP.value!!
        }
    }

}
