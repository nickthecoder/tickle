/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tasks

import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.tickle.editor.util.DesignAttributeData
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes

class AttributesTask(val attributes: DesignAttributes, val isAlternate: Boolean = false) : AbstractTask(threaded = false) {

    override val taskD = TaskDescription("attributes")

    init {
        attributes.data().forEach { data ->
            data as DesignAttributeData
            if (data.isAlternate == isAlternate) {
                data.parameter?.let { p ->
                    val param = p.copy()
                    data.value?.let { param.stringValue = it }
                    taskD.addParameters(param)
                }
            }
        }
    }

    private var changeAction: (() -> Unit)? = null

    fun onChange(action: () -> Unit) {
        changeAction = action
    }

    override fun run() {

        taskD.valueParameters().forEach { p ->
            val name = p.name.substring(10) // Strip leading "attribute_"
            val attData = (attributes.map()[name]) as? DesignAttributeData
            if (attData != null && attData.isAlternate == isAlternate) {
                attData.value = p.stringValue
            }

        }
        changeAction?.let { it() }
    }
}
