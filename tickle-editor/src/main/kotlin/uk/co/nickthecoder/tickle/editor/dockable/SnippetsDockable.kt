/*
Tickle
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import uk.co.nickthecoder.harbourfx.DualSplitPane
import uk.co.nickthecoder.tickle.editor.DOCKABLE_SNIPPETS
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.tabs.TextEditorTab
import java.io.File

class SnippetsDockable
    : TickleDockable(DOCKABLE_SNIPPETS) {

    private var split = DualSplitPane()

    private var tree = TreeView<String>()

    private var snippet = TextArea()

    private val openBelow = SimpleBooleanProperty(false)

    override val dockableContent = SimpleObjectProperty<Node>(split)


    init {
        split.top = tree
        split.orientation = Orientation.VERTICAL
        buildTree()
        tree.onMouseClicked = EventHandler { onMouseClicked(it) }

        openBelow.addListener { _, _, newValue ->
            if (newValue == false) {
                split.bottom = null
            }
        }
    }

    override fun settingsMenu(): ContextMenu {
        val menu = ContextMenu()
        menu.items.add(CheckMenuItem("Open Snippets Below").apply { selectedProperty().bindBidirectional(openBelow) })
        menu.items.add(MenuItem("Hide Snippets").apply { onAction = EventHandler { split.bottom = null } })
        menu.items.add(MenuItem("Refresh").apply { onAction = EventHandler { buildTree() } })
        return menu
    }

    private fun buildTree() {
        val dir = findSnippetsDirectory() ?: return
        tree.root = DirectoryItem(dir)
        tree.isShowRoot = false
    }

    private fun onMouseClicked(event: MouseEvent) {

        if (event.clickCount == 2) {
            val item = tree.selectionModel.selectedItem as? FileItem
                    ?: return
            if (openBelow.value) {
                snippet.text = item.file.readText()
                split.bottom = snippet
            } else {
                MainWindow.instance.openTab(TextEditorTab(item.file))
            }
            event.consume()
        }
    }

    inner class DirectoryItem(dir: File) : TreeItem<String>(dir.name) {

        init {
            graphic = ImageView(Editor.imageResource("folder.png"))
            dir.listFiles()?.let { files ->
                for (file in files) {
                    if (file.isDirectory) {
                        children.add(DirectoryItem(file))
                    }
                }
                for (file in files) {
                    if (file.isFile && file.extension == "feather") {
                        children.add(FileItem(file))
                    }
                }
            }
        }
    }

    inner class FileItem(val file: File) : TreeItem<String>(file.nameWithoutExtension) {

        init {
            graphic = ImageView(Editor.imageResource("snippet.png"))
        }

    }

    private fun findSnippetsDirectory(): File? {
        try {
            var dir = File(javaClass.protectionDomain?.codeSource?.location?.toURI() ?: return null)

            while (dir.exists()) {
                val stock = File(dir, "snippets")
                if (stock.exists()) {
                    return stock
                }
                // When running from my IDE, and also from "gradle run", dir is initially ./out/production/classes,
                // and our snippets directory is in ./src/dist/snippets
                val stock2 = File(File(File(dir, "src"), "dist"), "snippets")
                if (stock2.exists()) {
                    return stock2
                }
                dir = dir.parentFile ?: break
            }
            return null
        } catch (e: Exception) {
            return null
        }
    }
}
