/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.Tooltip
import javafx.scene.image.ImageView
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import javafx.stage.Stage
import uk.co.nickthecoder.fxessentials.fireTabToFocusNext
import uk.co.nickthecoder.paratask.gui.ShortcutHelper
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.gui.defaultWhileFocusWithin
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.tasks.CopyResourceTask
import uk.co.nickthecoder.tickle.editor.util.deletePrompted
import uk.co.nickthecoder.tickle.resources.ResourcesListener
import uk.co.nickthecoder.tickle.misc.CopyableResource
import uk.co.nickthecoder.tickle.util.DeletableResource


abstract class EditTab(
        dataName: String,
        data: Any,
        icon: ImageView? = null)

    : EditorTab(dataName, data), ResourcesListener {

    protected val borderPane = BorderPane()

    val leftButtons = FlowPane()

    val rightButtons = FlowPane()

    protected val buttons = HBox()

    protected val okButton = Button("Ok")

    protected val applyButton = Button("Apply")

    protected val cancelButton = Button("Cancel")

    private val shortcuts = ShortcutHelper("Edit Tab", MainWindow.instance.borderPane)

    var needsSaving = false
        set(v) {
            okButton.isDisable = !v
            applyButton.isDisable = !v
            cancelButton.text = if (v) "Cancel" else "Close"
            field = v
            if (v) {
                if (!styleClass.contains("dirty")) styleClass.add("dirty")
            } else {
                styleClass.remove("dirty")
            }
        }

    init {
        okButton.isDisable = true
        applyButton.isDisable = true

        graphic = icon
        ResourceType.resourceType(data)?.let { tooltip = Tooltip(it.label) }

        content = borderPane

        with(borderPane) {
            styleClass.add("prompt")
            bottom = buttons
            ResourceType.resourceType(data)?.let { rt ->
                if (rt != ResourceType.SCRIPT && rt != ResourceType.SCENE) {
                    top = Label(rt.label).apply { styleClass.add("heading") }
                }
            }
        }

        with(cancelButton) {
            onAction = EventHandler { onCancel() }
        }

        with(applyButton) {
            onAction = EventHandler { save() }
        }

        with(okButton) {
            onAction = EventHandler {
                if (save()) {
                    close()
                }
            }
            defaultWhileFocusWithin(borderPane)
        }


        with(leftButtons) {
            styleClass.addAll("buttons", "left")
            if (data is CopyableResource<*>) {
                children.add(Button("Copy").apply {
                    onAction = EventHandler {
                        TaskPrompter(CopyResourceTask(data, dataName)).placeOnStage(Stage())
                    }
                })
            }
        }

        with(rightButtons) {
            children.addAll(okButton, applyButton, cancelButton)
            styleClass.add("buttons")
        }

        with(buttons) {
            children.addAll(leftButtons, rightButtons)
        }

        if (data is DeletableResource) {
            addDeleteButton(data)
        }

        Editor.resources.listeners.add(this)
    }

    open fun focus() {
        borderPane.requestFocus()
        Platform.runLater{ // Give requestFocus a chance to take effect.
            borderPane.fireTabToFocusNext()
        }
    }

    override fun resourceRemoved(resource: Any, name: String) {
        if (resource === data) {
            close()
        }
    }

    override fun removed() {
        super.removed()
        Editor.resources.listeners.remove(this)
    }

    private fun addDeleteButton(deletable: DeletableResource) {
        val button = Button("Delete")

        button.setOnAction { deletable.deletePrompted(dataName) }
        leftButtons.children.add(button)
    }


    open fun save(): Boolean {
        return if (needsSaving) {
            val result = justSave()
            if (result) {
                styleClass.remove("error")
                needsSaving = false
                Editor.resources.fireChanged(data)
                Editor.resources.save()
            } else {
                if (!styleClass.contains("error")) styleClass.add("error")
            }
            result
        } else {
            true
        }
    }

    protected abstract fun justSave(): Boolean

    protected fun onCancel() {
        close()
    }

}
