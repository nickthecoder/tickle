/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.resources

import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.PrettyPrint
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.ResizeType
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences
import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.misc.JsonUtil
import uk.co.nickthecoder.tickle.misc.ResourcesReader
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.ResourceMap
import uk.co.nickthecoder.tickle.stage.FlexHAlignment
import uk.co.nickthecoder.tickle.stage.FlexVAlignment
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter

/**
 * Loads [DesignResources] from a file, as well as saving them back to a file.
 *
 * Note, do NOT use [ResourcesReader] directly from the Editor, because `attributes` are handled somewhat differently.
 */
internal class ResourcesReaderWriter private constructor(resources: DesignResources)

    : ResourcesReader(resources) {

    fun save(file: File) {

        resources.file = file.absoluteFile

        val jroot = JsonObject()
        jroot.add("info", saveInfo())
        addArray(jroot, "layouts", saveLayouts())
        addArray(jroot, "textures", saveTextures())
        addArray(jroot, "fonts", saveFonts())
        addArray(jroot, "poses", savePoses())
        addArray(jroot, "sounds", saveSounds())
        addArray(jroot, "costumeGroups", saveCostumeGroups())
        addArray(jroot, "costumes", saveCostumes(resources.costumes, false))
        addArray(jroot, "inputs", saveInputs())

        BufferedWriter(OutputStreamWriter(FileOutputStream(file))).use {
            jroot.writeTo(it, EditorPreferences.outputFormat.writerConfig)
        }
    }


    private fun saveInfo(): JsonObject {
        val jinfo = JsonObject()

        with(resources.gameInfo) {
            jinfo.add("title", title)
            jinfo.add("id", id)
            jinfo.add("width", width)
            jinfo.add("height", height)
            jinfo.add("resizable", resizable)
            jinfo.add("fullScreen", fullScreen)
            jinfo.add("initialScene", resources.sceneFileToPath(initialScenePath))
            jinfo.add("testScene", resources.sceneFileToPath(testScenePath))

            jinfo.add("enableFeather", enableFeather)

            jinfo.add("producer", producerString)
            jinfo.add( "comments", comments, "")
        }

        if (resources.gameInfo.physicsEngine) {
            val jphysics = JsonObject()
            jinfo.add("physics", jphysics)
            with(resources.gameInfo.physicsInfo) {
                jphysics.add("gravity_x", gravity.x)
                jphysics.add("gravity_y", gravity.y)
                jphysics.add("velocityIterations", velocityIterations)
                jphysics.add("positionIterations", positionIterations)
                jphysics.add("scale", scale)
                jphysics.add("filterGroups", filterGroupsString)
                jphysics.add("filterBits", filterBitsString)
            }

        }

        return jinfo
    }

    private fun saveLayouts(): JsonArray {
        val jlayouts = JsonArray()
        resources.layouts.items().forEach { (name, layout) ->
            val jlayout = JsonObject()
            jlayout.add("comments", layout.comments, "")
            jlayouts.add(jlayout)
            jlayout.add("name", name)

            val jstages = JsonArray()
            jlayout.add("stages", jstages)

            layout.layoutStages.forEach { (stageName, layoutStage) ->
                val jstage = JsonObject()
                jstages.add(jstage)

                jstage.add("name", stageName)
                if (layoutStage.isDefault) {
                    jstage.add("isDefault", true)
                }
                jstage.add("stage", layoutStage.stageString)
                JsonUtil.saveAttributes(jstage, layoutStage.stageAttributes, "stageAttributes")

                jstage.add("constraint", layoutStage.stageConstraintString)
                JsonUtil.saveAttributes(jstage, layoutStage.constraintAttributes, "constraintAttributes")
            }

            val jviews = JsonArray()
            jlayout.add("views", jviews)

            layout.layoutViews.forEach { (viewName, layoutView) ->
                val jview = JsonObject()
                jviews.add(jview)

                jview.add("name", viewName)

                jview.add("view", layoutView.viewString)
                JsonUtil.saveAttributes(jview, layoutView.viewAttributes, "viewAttributes")

                if (layoutView.stageName.isNotBlank()) {
                    jview.add("stage", layoutView.stageName)
                }
                jview.add("zOrder", layoutView.zOrder)
                jview.add("autoScale", layoutView.position.autoScale.name, "NONE")
                jview.add("letterboxAlignmentX", layoutView.position.letterboxAlignment.x, 0.5f)
                jview.add("letterboxAlignmentY", layoutView.position.letterboxAlignment.y, 0.5f)
                jview.add("enableAutoPosition", layoutView.position.enableAutoPosition, false)

                with(layoutView.position) {
                    jview.add("hAlignment", hAlignment.name)
                    if (hAlignment == FlexHAlignment.MIDDLE) {
                        jview.add("hPosition", hPosition)
                    } else {
                        jview.add("leftRightMargin", leftRightMargin)
                    }
                    width?.let { jview.add("width", it) }
                    widthRatio?.let { jview.add("widthRatio", it) }

                    jview.add("vAlignment", vAlignment.name)
                    if (vAlignment == FlexVAlignment.MIDDLE) {
                        jview.add("vPosition", vPosition)
                    } else {
                        jview.add("topBottomMargin", topBottomMargin)
                    }
                    height?.let { jview.add("height", it) }
                    heightRatio?.let { jview.add("heightRatio", it) }
                }
            }
        }
        return jlayouts
    }

    private fun saveTextures(): JsonArray {
        val jtextures = JsonArray()
        resources.textures.items().forEach { (name, texture) ->
            texture.file?.let { file ->
                val jtexture = JsonObject()
                jtexture.add("name", name)
                jtexture.add("file", resources.toPath(file))
                jtexture.add("comment", texture.comment, "")
                jtextures.add(jtexture)
            }
        }
        return jtextures
    }


    private fun savePoses(): JsonArray {
        val jposes = JsonArray()
        resources.poses.items().forEach { (name, pose) ->
            resources.textures.findName(pose.texture)?.let { textureName ->
                val jpose = JsonObject()
                jpose.add("name", name)
                jpose.add("texture", textureName)
                jpose.add("left", pose.pixelRect.left)
                jpose.add("bottom", pose.pixelRect.bottom)
                jpose.add("right", pose.pixelRect.right)
                jpose.add("top", pose.pixelRect.top)
                jpose.add("offsetX", pose.offsetX)
                jpose.add("offsetY", pose.offsetY)
                jpose.add("direction", pose.direction.degrees, 0.0)
                jpose.add("comment", pose.comment, "")
                jpose.addEnum("resizeType", pose.resizeType, ResizeType.SCALE)

                if (pose.resizeType == ResizeType.NINE_PATCH) {
                    jpose.add("marginL", pose.ninePatchMargins.left)
                    jpose.add("marginR", pose.ninePatchMargins.right)
                    jpose.add("marginT", pose.ninePatchMargins.top)
                    jpose.add("marginB", pose.ninePatchMargins.bottom)
                }

                if (pose.snapPoints.isNotEmpty()) {
                    val jsnaps = JsonArray()
                    jpose.add("snapPoints", jsnaps)
                    pose.snapPoints.forEach { point ->
                        val jpoint = JsonObject()
                        jpoint.add("x", point.x)
                        jpoint.add("y", point.y)
                        jsnaps.add(jpoint)
                    }
                }

                jposes.add(jpose)
            }
        }
        return jposes

    }

    private fun saveCostumeGroups(): JsonArray {
        val jgroups = JsonArray()

        resources.costumeGroups.items().forEach { (name, group) ->
            val jgroup = JsonObject()
            jgroup.add("name", name)
            jgroup.add("costumes", saveCostumes(group, true))

            jgroup.add("showInSceneEditor", group.showInSceneEditor)
            jgroup.add("initiallyExpanded", group.initiallyExpanded)
            jgroup.add("sortOrder", group.sortOrder)
            jgroups.add(jgroup)
        }
        return jgroups
    }

    private fun saveCostumes(costumes: ResourceMap<Costume>, all: Boolean): JsonArray {
        val jcostumes = JsonArray()

        costumes.items().forEach { (name, costume) ->

            if (all || resources.findCostumeGroup(name) == null) {

                val jcostume = JsonObject()
                jcostume.add("name", name)
                jcostume.add("role", costume.roleString)
                jcostume.add("canRotate", costume.canRotate)
                jcostume.add("canScale", costume.canScale)
                jcostume.add("zOrder", costume.zOrder)
                jcostume.add("stageName", costume.stageName)
                jcostume.add("initialEvent", costume.initialEventName)
                jcostume.add("showInSceneEditor", costume.showInSceneEditor)
                jcostume.add("comments", costume.comments, "")
                if (costume.inheritEventsFrom != null) {
                    jcostume.add("inheritsEventsFrom", resources.costumes.findName(costume.inheritEventsFrom))
                }

                val jevents = JsonArray()
                jcostume.add("events", jevents)
                costume.events.forEach { (eventName, event) ->
                    val jevent = JsonObject()
                    jevents.add(jevent)
                    jevent.add("name", eventName)

                    if (event.poses.isNotEmpty()) {
                        val jposes = JsonArray()
                        event.poses.forEach { pose ->
                            resources.poses.findName(pose)?.let { poseName ->
                                jposes.add(poseName)
                            }
                        }
                        jevent.add("poses", jposes)
                    }

                    if (event.costumes.isNotEmpty()) {
                        val jcos = JsonArray()
                        event.costumes.forEach { cos ->
                            resources.costumes.findName(cos)?.let { cosName ->
                                jcos.add(cosName)
                            }
                        }
                        jevent.add("costumes", jcos)
                    }

                    if (event.textStyles.isNotEmpty()) {
                        val jtextStyles = JsonArray()
                        event.textStyles.forEach { textStyle ->
                            val jtextStyle = JsonObject()
                            jtextStyles.add(jtextStyle)
                            jtextStyle.add("font", resources.fontResources.findName(textStyle.fontResource))
                            jtextStyle.add("halign", textStyle.halignment.name)
                            jtextStyle.add("valign", textStyle.valignment.name)
                            jtextStyle.add("color", textStyle.color.toHashRGBA())
                            if (textStyle.fontResource.outlineFontTexture != null) {
                                textStyle.outlineColor?.let {
                                    jtextStyle.add("outlineColor", it.toHashRGBA())
                                }
                            }
                        }
                        jevent.add("textStyles", jtextStyles)
                    }

                    if (event.strings.isNotEmpty()) {
                        val jstrings = JsonArray()
                        event.strings.forEach { str ->
                            jstrings.add(str)
                        }
                        jevent.add("strings", jstrings)
                    }

                    if (event.soundEvents.isNotEmpty()) {
                        val jsoundEvents = JsonArray()
                        event.soundEvents.forEach { soundEvent ->
                            val jsoundEvent = JsonObject()
                            jsoundEvents.add(jsoundEvent)
                            resources.sounds.findName(soundEvent.sound)?.let { soundName ->
                                jsoundEvent.add("sound", soundName)
                                jsoundEvent.add("gain", soundEvent.gain)
                                jsoundEvent.add("priority", soundEvent.newPriority)
                                jsoundEvent.add("threshold", soundEvent.priorityThreshold)
                                jsoundEvent.add("onlyOnce", soundEvent.onlyOnce)
                                jsoundEvent.add("onlyWhenVisible", soundEvent.onlyWhenVisible)
                            }
                        }
                        jevent.add("soundEvents", jsoundEvents)
                    }

                    if (event.ninePatches.isNotEmpty()) {
                        val jninePatches = JsonArray()
                        event.ninePatches.forEach { ninePatch ->
                            val jninePatch = JsonObject()
                            jninePatches.add(jninePatch)
                            jninePatch.add("pose", resources.poses.findName(ninePatch.pose))
                            jninePatch.add("left", ninePatch.left)
                            jninePatch.add("bottom", ninePatch.bottom)
                            jninePatch.add("right", ninePatch.right)
                            jninePatch.add("top", ninePatch.top)
                        }
                        jevent.add("ninePatches", jninePatches)
                    }
                }
                JsonUtil.saveAttributes(jcostume, costume.attributes, "attributes", true)

                jcostumes.add(jcostume)
                costume.bodyDef?.let { saveBody(jcostume, it) }
            }
        }

        return jcostumes
    }

    private fun saveInputs(): JsonArray {
        val jinputs = JsonArray()
        resources.inputs.items().forEach { (name, input) ->
            val jinput = JsonObject()
            jinput.add("name", name)
            jinput.add("comments", input.comments, "")

            val jkeys = JsonArray()
            addKeyInputs(input, jkeys)
            if (!jkeys.isEmpty) {
                jinput.add("keys", jkeys)
            }

            val jmouseButtons = JsonArray()
            addMouseInputs(input, jmouseButtons)
            if (!jmouseButtons.isEmpty) {
                jinput.add("mouse", jmouseButtons)
            }

            val jjoystickButtons = JsonArray()
            addJoystickButtonInputs(input, jjoystickButtons)
            if (!jjoystickButtons.isEmpty) {
                jinput.add("joystick", jjoystickButtons)
            }

            val jjoystickAxis = JsonArray()
            addJoystickAxisInputs(input, jjoystickAxis)
            if (!jjoystickAxis.isEmpty) {
                jinput.add("joystickAxis", jjoystickAxis)
            }

            jinputs.add(jinput)
        }
        return jinputs
    }

    private fun addMouseInputs(input: Input, toArray: JsonArray) {

        if (input is MouseInput) {
            val jmouse = JsonObject()
            jmouse.add("button", input.mouseButton)
            jmouse.add("state", input.state.name)
            input.shift?.let { jmouse.add("shift", it) }
            input.control?.let { jmouse.add("control", it) }
            input.alt?.let { jmouse.add("alt", it) }
            toArray.add(jmouse)

        } else if (input is CompoundInput) {
            input.inputs.forEach {
                addMouseInputs(it, toArray)
            }
        }
    }

    private fun addKeyInputs(input: Input, toArray: JsonArray) {

        if (input is KeyInput) {
            val jkey = JsonObject()
            jkey.add("key", input.key.label)
            jkey.add("state", input.state.name)
            input.shift?.let { jkey.add("shift", it) }
            input.control?.let { jkey.add("control", it) }
            input.alt?.let { jkey.add("alt", it) }
            toArray.add(jkey)

        } else if (input is CompoundInput) {
            input.inputs.forEach {
                addKeyInputs(it, toArray)
            }
        }
    }

    private fun addJoystickButtonInputs(input: Input, toArray: JsonArray) {

        if (input is JoystickButtonInput) {
            val jjoystick = JsonObject()
            jjoystick.add("joystickID", input.joystickID)
            jjoystick.add("button", input.button.name)
            toArray.add(jjoystick)

        } else if (input is CompoundInput) {
            input.inputs.forEach {
                addJoystickButtonInputs(it, toArray)
            }
        }
    }

    private fun addJoystickAxisInputs(input: Input, toArray: JsonArray) {

        if (input is JoystickAxisInput) {
            val jjoystick = JsonObject()
            jjoystick.add("joystickID", input.joystickID)
            jjoystick.add("axis", input.axis.name)
            jjoystick.add("positive", input.positive)
            jjoystick.add("threshold", input.threshold)
            toArray.add(jjoystick)

        } else if (input is CompoundInput) {
            input.inputs.forEach {
                addJoystickAxisInputs(it, toArray)
            }
        }
    }


    private fun saveFonts(): JsonArray {
        val jfonts = JsonArray()
        resources.fontResources.items().forEach { (name, fontResource) ->
            val jfont = JsonObject()
            jfont.add("name", name)
            if (fontResource.file == null) {
                jfont.add("fontName", fontResource.fontName)
                jfont.add("style", fontResource.style.name)
            } else {
                jfont.add("file", resources.toPath(fontResource.file!!))
            }
            fontResource.pngFile?.let {
                jfont.add("pngFile", resources.toPath(it))
            }
            jfont.add("size", fontResource.size)
            jfont.add("xPadding", fontResource.xPadding)
            jfont.add("yPadding", fontResource.yPadding)
            jfont.add("comments", fontResource.comments, "")
            jfonts.add(jfont)
        }

        return jfonts

    }


    private fun saveSounds(): JsonArray {
        val jsounds = JsonArray()
        resources.sounds.items().forEach { (name, sound) ->
            sound.file?.let { file ->
                val jsound = JsonObject()
                jsound.add("name", name)
                jsound.add("file", resources.toPath(file))
                jsound.add("defaultGain", sound.defaultGain, 1.0)
                jsound.add("comments", sound.comments, "")
                jsounds.add(jsound)
            }
        }
        return jsounds
    }


    companion object {

        @JvmStatic
        fun load(into: DesignResources, file: File) {
            into.file = file
            ResourcesReaderWriter(into).loadResources()
        }

        @JvmStatic
        fun save(resources: DesignResources) {
            ResourcesReaderWriter(resources).save(resources.file)
        }

        @JvmStatic
        fun saveFontMetrics(file: File, fontResource: FontResource) {
            val fontTexture = fontResource.fontTexture
            val jroot = JsonObject()
            jroot.add("lineHeight", fontTexture.lineHeight)
            jroot.add("leading", fontTexture.leading)
            jroot.add("ascent", fontTexture.ascent)
            jroot.add("descent", fontTexture.descent)
            jroot.add("xPadding", fontResource.xPadding)
            jroot.add("yPadding", fontResource.yPadding)
            val jglyphs = JsonArray()
            jroot.add("glyphs", jglyphs)
            fontTexture.glyphs.forEach { (c, data) ->
                val jglyph = JsonObject()
                jglyph.add("c", c.toString())
                jglyph.add("left", data.pose.pixelRect.left)
                jglyph.add("top", data.pose.pixelRect.top)
                jglyph.add("right", data.pose.pixelRect.right)
                jglyph.add("bottom", data.pose.pixelRect.bottom)
                jglyph.add("advance", data.advance)

                jglyphs.add(jglyph)
            }

            BufferedWriter(OutputStreamWriter(FileOutputStream(file))).use {
                jroot.writeTo(it, PrettyPrint.indentWithSpaces(4))
            }
        }

    }


}

/**
 * Helper functions to make it easier to skip items, which have the default value.
 * This makes the .tickle and .scene files smaller.
 */
fun JsonObject.add(name: String, value: String, defaultValue: String) {
    if (value != defaultValue) {
        add(name, value)
    }
}

fun JsonObject.add(name: String, value: Int, defaultValue: Int) {
    if (value != defaultValue) {
        add(name, value)
    }
}

fun JsonObject.add(name: String, value: Float, defaultValue: Float) {
    if (value != defaultValue) {
        add(name, value)
    }
}
fun JsonObject.add(name: String, value: Double, defaultValue: Double) {
    if (value != defaultValue) {
        add(name, value)
    }
}
fun JsonObject.add(name: String, value: Boolean, defaultValue: Boolean) {
    if (value != defaultValue) {
        add(name, value)
    }
}
fun JsonObject.add(name: String, value: Color?, defaultValue: Color?) {
    if (value != defaultValue) {
        if (value != null) {
            add(name, value.toHashRGBA())
        }
    }
}
