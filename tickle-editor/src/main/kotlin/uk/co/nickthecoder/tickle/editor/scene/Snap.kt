/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import javafx.beans.property.BooleanProperty
import uk.co.nickthecoder.paratask.Task
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * The base interface for all snapping behaviours.
 */
interface Snap {

    val enabledProperty: BooleanProperty
    val isEnabled: Boolean
        get() = enabledProperty.value

    fun task(): Task

    fun snapInfo() = "You can temporarily disable snapping by holding down the ctrl key while dragging."
}

/**
 * [dx], [dy] are how much we should adjust each [Actor]'s position by.
 *
 * [score] is how "important" this adjustment is.
 * We can snap in many ways (e.g. SnapToGrid, SnapToOther...), [score] is how we choose which one to use.
 * Lower values are more "important".
 *
 * See [bestAdjustment].
 */
data class SnapToAdjustment(var dx: Float = 0f, var dy: Float = 0f, var score: Float = Float.MAX_VALUE) {
    fun reset() {
        dx = 0f
        dy = 0f
        score = Float.MAX_VALUE
    }
}

/**
 * Returns the lowest scoring adjustment, or null if there are none.
 */
fun bestAdjustment(adjustments: List<SnapToAdjustment>): SnapToAdjustment? {
    val lowest = adjustments.minByOrNull { it.score }
    return if (lowest?.score == Float.MAX_VALUE) null else lowest
}
