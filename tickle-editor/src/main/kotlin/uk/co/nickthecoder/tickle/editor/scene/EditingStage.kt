package uk.co.nickthecoder.tickle.editor.scene

import uk.co.nickthecoder.tickle.stage.Stage
import uk.co.nickthecoder.tickle.stage.StageView

class EditingStage(val sceneEditor: SceneEditor, val stageName: String, val stage: Stage, val view: StageView) {

    var isVisible = true
        set(v) {
            field = v
            sceneEditor.fireStageChanged(this)
        }

    var isLocked = false
        set(v) {
            field = v
            sceneEditor.fireStageChanged(this)
        }
}
