package uk.co.nickthecoder.tickle.editor.dockable

import javafx.scene.input.DataFormat
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import java.io.Serializable

data class ResourceInfo(
    val name: String,
    val resourceType: ResourceType?
) : Serializable {

    companion object {
        val dataFormat = DataFormat("application/tickle-resource-info")
    }

}
