package uk.co.nickthecoder.tickle.editor.scene

import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import org.joml.Matrix3x2f
import org.lwjgl.opengl.GL11
import uk.co.nickthecoder.fxessentials.requestFocusSoon
import uk.co.nickthecoder.scarea.comsun.BehaviorBase
import uk.co.nickthecoder.scarea.comsun.BehaviorSkinBase
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.*
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.resources.SceneListener
import uk.co.nickthecoder.tickle.editor.scene.history.*
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.path.Polyline
import uk.co.nickthecoder.tickle.path.VisibleShape
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.stage.AbstractHandle
import uk.co.nickthecoder.tickle.stage.Handle
import uk.co.nickthecoder.tickle.util.*

/**
 * Node hierarchy :
 *
 *     SceneView
 *         scrollPane
 *             grid
 *                 hRuler
 *                 vRuler
 *                 imageView containing imageTransfers image
 */
class SceneViewSkin(

    private val sceneView: SceneView

) : BehaviorSkinBase<SceneView, SceneViewBehavior>(sceneView, SceneViewBehavior(sceneView)), SelectionListener,
    SceneListener {

    private val imageTransfer = onOpenGLThread { ImageTransfer(800, 600) }
    private val imageView = ImageView(imageTransfer.image).apply {
        addEventHandler(MouseEvent.MOUSE_CLICKED) { onMouseClicked(it) }
        addEventHandler(MouseEvent.MOUSE_PRESSED) { onMousePressed(it) }
        addEventHandler(MouseEvent.MOUSE_MOVED) { onMouseMoved(it) }
        addEventHandler(MouseEvent.MOUSE_DRAGGED) { onMouseDragged(it) }
        addEventHandler(MouseEvent.MOUSE_RELEASED) { onMouseReleased(it) }

        GridPane.setConstraints(this, 1, 1)
        GridPane.setHgrow(this, Priority.ALWAYS)
        GridPane.setVgrow(this, Priority.ALWAYS)
    }

    private val viewMatrix = Matrix3x2f()

    private var mouseHandler: MouseHandler = Select().apply {
        sceneView.tipProperty.bind(tipProperty)
    }
        set(v) {
            sceneView.tipProperty.unbind()
            field.end()
            field = v
            if (v !is Stamp) {
                sceneView.stampCostume = null
            }
            sceneView.tipProperty.bind(v.tipProperty)

            if (v !is MoveGuide) {
                selection.highlightXGuide = null
                selection.highlightYGuide = null
            }
            redraw()
        }


    private var left: Float
        get() = -imageTransfer.width / 2 / scale + centerX
        set(v) {
            centerX = v + imageTransfer.width / 2 / scale
        }

    private val bottom: Float get() = -imageTransfer.height / 2 / scale + centerY
    private val right: Float get() = (imageTransfer.width - imageTransfer.width / 2) / scale + centerX
    private var top: Float
        get() = (imageTransfer.height - imageTransfer.height / 2) / scale + centerY
        set(v) {
            centerY = v - (imageTransfer.height - imageTransfer.height / 2) / scale
        }


    private val corner = Region().apply {
        GridPane.setConstraints(this, 0, 0)
    }

    private val vRuler = Ruler().apply {
        GridPane.setConstraints(this, 0, 1)
        GridPane.setVgrow(this, Priority.ALWAYS)
        side = Side.LEFT
        addEventHandler(MouseEvent.MOUSE_PRESSED) { onDragRuler(it, isVertical = true) }
        addEventHandler(MouseEvent.MOUSE_DRAGGED) { onMouseDragged(it) }
        addEventHandler(MouseEvent.MOUSE_RELEASED) { onMouseReleased(it) }
    }

    private val hRuler = Ruler().apply {
        GridPane.setConstraints(this, 1, 0)
        GridPane.setHgrow(this, Priority.ALWAYS)
        side = Side.TOP
        addEventHandler(MouseEvent.MOUSE_PRESSED) { onDragRuler(it, isVertical = false) }
        addEventHandler(MouseEvent.MOUSE_DRAGGED) { onMouseDragged(it) }
        addEventHandler(MouseEvent.MOUSE_RELEASED) { onMouseReleased(it) }
    }


    private val grid = GridPane().apply {
        children.addAll(corner, hRuler, vRuler, imageView)
    }

    private val scrollPane = ScrollPane().apply {
        content = grid
        hbarPolicy = ScrollPane.ScrollBarPolicy.ALWAYS
        vbarPolicy = ScrollPane.ScrollBarPolicy.ALWAYS
        isFitToHeight = true
        isFitToWidth = true
        hvalueProperty().addListener { _, _, _ -> onScrollValueChanged() }
        vvalueProperty().addListener { _, _, _ -> onScrollValueChanged() }

        GridPane.setConstraints(this, 1, 1)
        GridPane.setVgrow(this, Priority.ALWAYS)
        GridPane.setHgrow(this, Priority.ALWAYS)
    }

    private val sceneEditor: SceneEditor get() = sceneView.sceneEditor
    private val resources: Resources get() = Editor.resources

    private val scale: Float get() = sceneView.scale

    private var centerX: Float
        get() = sceneView.centerX
        set(v) {
            sceneView.centerX = v
        }

    private var centerY: Float
        get() = sceneView.centerY
        set(v) {
            sceneView.centerY = v
        }

    private val selection: Selection get() = sceneView.selection
    private val editingStages get() = sceneView.editingStages
    private val history get() = sceneView.history

    /**
     * The world coordinates where the drag operation started.
     */
    private val dragStart = Vector2()

    private var dragging = false

    /**
     * The amount that we have dragged (relative to [dragStart])
     */
    private var dragDelta = Vector2()

    init {
        selection.listeners.add(this)
        sceneEditor.sceneListeners.add(this)

        children.add(scrollPane)

        sceneView.centerXProperty.addListener { _, _, _ -> updatePanAndScale() }
        sceneView.centerYProperty.addListener { _, _, _ -> updatePanAndScale() }
        sceneView.scaleProperty.addListener { _, _, _ -> updatePanAndScale() }
        sceneView.dirtyProperty.addListener { _, _, isDirty -> if (isDirty) redraw() }
        sceneView.stampCostumeProperty.addListener { _, _, costume ->
            if (costume != null) mouseHandler = Stamp(costume)
        }

        sceneView.addEventFilter(KeyEvent.KEY_PRESSED) { onKeyPressed(it) }

        // We need runLater, otherwise we get the old ScrollPane's viewport size.
        sceneView.widthProperty().addListener { _, _, _ -> Platform.runLater { resizeScrollPane() } }
        sceneView.heightProperty().addListener { _, _, _ -> Platform.runLater { resizeScrollPane() } }
        resizeScrollPane()
        updatePanAndScale()

        redraw()
        sceneView.requestFocusSoon()
    }

    private fun redraw() {
        sceneView.dirty = true
        Platform.runLater {
            if (sceneView.dirty) {
                sceneView.dirty = false
                draw()
            }
        }
    }

    /**
     * Redraws the scene using OpenGL, and transfer it using [ImageTransfer].
     */
    internal fun draw() {
        imageTransfer.draw {
            with(Renderer.instance()) {

                clip(0, 0, imageTransfer.width, imageTransfer.height)
                clear(sceneEditor.scene.background)

                // Highlight the gameInfo size
                beginView(0.0, 0.0, imageTransfer.width.toDouble(), imageTransfer.height.toDouble())
                val origin = worldToViewport(0f, 0f)
                val gameInfoExtent =
                    worldToViewport(resources.gameInfo.width.toFloat(), resources.gameInfo.height.toFloat())
                strokeRect(
                    Rect(origin.x, origin.y, gameInfoExtent.x, gameInfoExtent.y),
                    EditorPreferences.sceneEditorPreferences.screenSizeThickness,
                    EditorPreferences.sceneEditorPreferences.screenSizeColor
                )
                endView()

                beginView(viewMatrix)


                // Draw all the actors
                for (es in sceneView.editingStages.reversed()) {
                    if (es.isVisible) {
                        for (actor in es.view.actorsToDraw()) {
                            globalOpacity = if (es.isLocked || actor.isLocked) 0.5f else 1f
                            actor.appearance.draw(this)
                        }
                    }
                }
                globalOpacity = 1f

                endView()

                // Above the actors are the selection boxes, handles, guides
                beginView(0.0, 0.0, imageTransfer.width.toDouble(), imageTransfer.height.toDouble())

                // Draw SnapToGuides lines
                with(sceneEditor.snapPreferences.snapToGuides) {
                    if (isEnabled) {
                        val thickness = EditorPreferences.sceneEditorPreferences.guidesThickness
                        for (x in this.xGuides) {
                            val viewX = worldToViewportX(x)
                            val color = if (x == selection.highlightXGuide) {
                                EditorPreferences.sceneEditorPreferences.highlightGuideColor
                            } else {
                                EditorPreferences.sceneEditorPreferences.guideColor
                            }
                            line(Vector2(viewX, 0f), Vector2(viewX, imageTransfer.height.toFloat()), thickness, color)
                        }
                        for (y in this.yGuides) {
                            val viewY = worldToViewportY(y)
                            val color = if (y == selection.highlightYGuide) {
                                EditorPreferences.sceneEditorPreferences.highlightGuideColor
                            } else {
                                EditorPreferences.sceneEditorPreferences.guideColor
                            }
                            line(Vector2(0f, viewY), Vector2(imageTransfer.width.toFloat(), viewY), thickness, color)
                        }
                    }
                }

                // Draw rectangles around each selected Actor.
                val latest = selection.latest()

                for (sa in selection.items) {

                    val actor = sa.actor
                    val app = actor.appearance
                    val left = -app.offsetX() * actor.scale.x * scale
                    val bottom = -app.offsetY() * actor.scale.y * scale
                    val right = left + app.width() * actor.scale.x * scale
                    val top = bottom + app.height() * actor.scale.y * scale

                    val matrix = Matrix3x2f().translate(worldToViewportX(actor.x), worldToViewportY(actor.y))
                    val radians = actor.direction.radians
                    if (radians != 0.0) {
                        matrix.rotate(radians.toFloat())
                    }
                    val polyline = Polyline(
                        Vector2(left, bottom),
                        Vector2(left, top),
                        Vector2(right, top),
                        Vector2(right, bottom),
                        Vector2(left, bottom)
                    ).apply { joinEnd(start) }

                    val paint = if (sa === latest) {
                        EditorPreferences.sceneEditorPreferences.latestStrokePaint
                    } else {
                        EditorPreferences.sceneEditorPreferences.selectionStrokePaint
                    }

                    paint.repeat = Math.ceil(polyline.length() / 30.0).toFloat()
                    strokeAntiAlias(
                        VisibleShape(
                            polyline,
                            EditorPreferences.sceneEditorPreferences.selectionLineThickness,
                            strokePaint = paint
                        ), 1f, matrix
                    )
                }

                // Now draw all the handles
                for (sa in selection.items) {
                    sa.handles?.let { handles ->
                        for (handle in handles) {
                            val point = worldToViewport(handle.x(), handle.y())
                            handle.draw(point, handle === selection.highlightHandle)
                        }
                    }
                }

                mouseHandler.draw(this)

                endView()
            }

        }
    }


    override fun actorModified(scene: Scene, actor: Actor, type: ModificationType) {
        for (sa in selection.items) {
            if (sa.actor === actor) {
                sa.handles?.let {
                    sa.updateMatrix()
                    for (handle in it) {
                        handle.update()
                    }
                }
            }
        }
    }

    private fun createHandles(clearExisting: Boolean) {
        for (sa in selection.items) {
            if (sa.handles == null || clearExisting) {
                sa.handles?.clear()

                val actor = sa.actor
                val costume = actor.costume
                val appearance = actor.appearance
                val handles = mutableListOf<Handle>()

                if (costume.canRotate) {
                    handles.add(RotateHandle(actor, sceneEditor.snapPreferences.snapRotation))
                }

                if (appearance is ResizeAppearance) {
                    handles.add(ResizeHandle(actor, isLeft = false, isBottom = false))
                    handles.add(ResizeHandle(actor, isLeft = false, isBottom = true))
                    handles.add(ResizeHandle(actor, isLeft = true, isBottom = false))
                    handles.add(ResizeHandle(actor, isLeft = true, isBottom = true))
                } else if (costume.canScale) {
                    handles.add(ScaleHandle(actor, isLeft = false, isBottom = false))
                    handles.add(ScaleHandle(actor, isLeft = false, isBottom = true))
                    handles.add(ScaleHandle(actor, isLeft = true, isBottom = false))
                    handles.add(ScaleHandle(actor, isLeft = true, isBottom = true))
                }

                sa.handles = handles
            }
        }
    }

    override fun selectionChanged() {
        createHandles(false)

        redraw()
    }

    /**
     * When the scroll page changes size we need to change the size of the [Image], and redraw it.
     *
     * NOTE. by changing the [imageTransfer] to just the right size, the contents of the [scrollPane] is
     * exactly the same size as its Viewport, and therefore the contents ([grid]) do NOT MOVE when we
     * scroll.
     */
    fun resizeScrollPane() {
        val bounds = scrollPane.viewportBounds
        imageTransfer.resize((bounds.width - vRuler.width).toInt(), (bounds.height - hRuler.height).toInt())
        ignoreScrollbarChanges = true
        imageView.image = imageTransfer.image
        ignoreScrollbarChanges = false
        updatePanAndScale()
    }

    // When we change scrollPane values from code, we do NOT want to update centerX, centerY
    // (because we used centerX and centerY to set the scrollPane's values!)
    private var ignoreScrollbarChanges = true

    internal fun updatePanAndScale(updateScrollbar: Boolean = true) {
        for (view in sceneEditor.scene.stageViews()) {
            view.worldFocal.set(centerX, centerY)
            view.viewportFocal.set(imageTransfer.width / 2f, imageTransfer.height / 2f)
            view.scaleXY = scale
            view.rect = RectInt(0, 0, imageTransfer.width, imageTransfer.height)
        }

        if (updateScrollbar) {
            setScrollBarValues()
        }

        viewMatrix.set(
            2f / (right - left),
            0f,
            0f,
            2f / (top - bottom),
            -(right + left) / (right - left),
            -(top + bottom) / (top - bottom)
        )

        with(hRuler) {
            lowerBound = left.toDouble()
            upperBound = right.toDouble()
        }
        with(vRuler) {
            lowerBound = bottom.toDouble()
            upperBound = top.toDouble()
        }
        createHandles(true)
        redraw()
    }

    private fun setScrollBarValues() {
        val bounds = calculateBounds()
        // So we want to be able to see from minX to maxX in the viewport.
        // How much is outside the viewport? i.e. how much can we move the scrollbar?
        val scrollableX = bounds.width() - imageTransfer.width / scale
        val scrollableY = bounds.height() - imageTransfer.height / scale

        // Now we can position the scrollbars based on the left and top.
        with(scrollPane) {
            hmin = 0.0
            vmin = 0.0
            hmax = scrollableX.toDouble()
            vmax = scrollableY.toDouble()
            ignoreScrollbarChanges = true
            hvalue = (left - bounds.left).toDouble()
            vvalue = (bounds.top - top).toDouble()
            ignoreScrollbarChanges = false
        }
    }


    /**
     * Do the opposite of updatePanAndScale.
     *
     * NOTE. Due to the weird way I'm using the scrollPane (the contents are always the same size as the viewport,
     * and therefore the content never moves), when we resize the stage,
     * the ScrollPane recalculates its [ScrollPane.hvalue] [ScrollPane.vvalue], and ends up with the answer of
     * 1.0. In fact, it could pick ANY value. We ignore them!
     */
    private fun onScrollValueChanged() {
        if (!ignoreScrollbarChanges) {
            if (scrollPane.hvalue <= 1.0 || scrollPane.vvalue <= 1.0) {
                updatePanAndScale()
            }

            val bounds = calculateBounds()
            left = scrollPane.hvalue.toFloat() + bounds.left
            top = bounds.top - scrollPane.vvalue.toFloat()

            updatePanAndScale(false)
        }
    }

    /**
     * Find the min/max of x a7 y of all actors (with an initial bounds of (0,0) .. (GameInfo.width,height).
     * Also add an extra margin on all four sides (GameInfo.width,height).
     */
    private fun calculateBounds(): SceneBounds {
        return SceneBounds().apply {
            for (es in editingStages) {
                for (actor in es.stage.actors) {
                    if (actor.x < left) left = actor.x
                    if (actor.x > right) right = actor.x
                    if (actor.y < bottom) bottom = actor.y
                    if (actor.y > top) top = actor.y
                }
            }
            // Add a margin of 1 "gameInfo size"
            left -= resources.gameInfo.width
            bottom -= resources.gameInfo.height
            right += resources.gameInfo.width
            top += resources.gameInfo.height
        }
    }

    private class SceneBounds {
        var left: Float = 0f
        var right: Float = Editor.resources.gameInfo.width.toFloat()
        var bottom: Float = 0f
        var top: Float = Editor.resources.gameInfo.height.toFloat()
        fun width() = right - left
        fun height() = top - bottom
    }

    /**
     * Converts a mouse position to a world position.
     */
    fun mouseToWorld(event: MouseEvent): Vector2 {
        val local = imageView.sceneToLocal(event.sceneX, event.sceneY)
        return Vector2(
            (centerX + (local.x - imageTransfer.width / 2) / scale).toFloat(),
            (centerY + (imageTransfer.height / 2 - local.y) / scale).toFloat()
        )
    }


    /**
     * Converts a world x coordinate into a position in pixels from the left edge of the viewport
     */
    fun worldToViewportX(x: Float): Float {
        return (x - centerX) * scale + imageTransfer.width / 2
        //return x * scale + imageTransfer.width / 2 - centerX //(x - centerX) * scale + imageTransfer.width / 2
    }
    // world = centerX + (view.x - imageTransfer.width / 2) / scale
    // world * scale + it.width/2 - centerX = view.x

    /**
     * Converts a world x coordinate into a position in pixels from the bottom edge of the viewport
     */
    fun worldToViewportY(y: Float): Float {
        return (y - centerY) * scale + imageTransfer.height / 2
    }

    fun worldToViewport(x: Float, y: Float): Vector2 {
        return Vector2(worldToViewportX(x), worldToViewportY(y))
    }

    private fun onDragRuler(event: MouseEvent, isVertical: Boolean) {
        mouseHandler = CreateGuide(mouseToWorld(event), isVertical)
        event.consume()
    }

    fun onKeyPressed(event: KeyEvent) {

        if (DELETE.match(event)) {
            with(sceneEditor.snapPreferences.snapToGuides) {
                val x = selection.highlightXGuide
                if (x != null) {
                    xGuides.remove(x)
                    selection.highlightXGuide = null
                    sceneEditor.firePreferencesChanged()
                    event.consume()
                    return
                }
                val y = selection.highlightYGuide
                if (y != null) {
                    yGuides.remove(y)
                    selection.highlightYGuide = null
                    sceneEditor.firePreferencesChanged()
                    event.consume()
                    return
                }
            }
            sceneEditor.deleteActors()
            event.consume()
            return
        }

        if (ESCAPE.match(event)) {
            onEscape()
            event.consume()
            return
        }

        // Allow arrow keys to move the selected actor. Ctrl moves by 10, Shift+Ctrl by 100
        if (mouseHandler is Select) {
            if (selection.isNotEmpty()) {
                val delta = when (event.code) {
                    KeyCode.UP -> Vector2(0f, 1f)
                    KeyCode.DOWN -> Vector2(0f, -1f)
                    KeyCode.LEFT -> Vector2(-1f, 0f)
                    KeyCode.RIGHT -> Vector2(1f, 0f)
                    else -> Vector2()
                }
                if (delta != ImmutableVector2.ZERO) {
                    if (event.isControlDown) delta *= 10f
                    if (event.isShiftDown) delta *= 10f

                    sceneEditor.history.beginBatch()
                    for (sa in selection.items) {
                        sa.actor.x += delta.x
                        sa.actor.y += delta.y
                        sceneEditor.history.makeChange(
                            MoveActor(sa.actor, sa.actor.x - delta.x, sa.actor.y - delta.y)
                        )
                    }
                    sceneEditor.history.endBatch()
                    event.consume()
                }
            }
        }
    }

    private fun onMouseClicked(event: MouseEvent) {
        if (!dragging) {
            event.consume()
            mouseHandler.onMouseClicked(event)
        }
        dragging = false
    }

    private fun onMousePressed(event: MouseEvent) {
        event.isDragDetect = false
        // Ensure that our onKeyPressed handler works!
        imageView.requestFocus()

        dragStart.set(mouseToWorld(event))

        if (event.isPopupTrigger) {
            event.consume()

            val menu = buildContextMenu()
            menu.show(MainWindow.instance.scene.window, event.screenX, event.screenY)

        } else {
            event.consume()
            mouseHandler.onMousePressed(event)
        }
    }

    private fun onMouseReleased(event: MouseEvent) {

        if (event.isPopupTrigger) {

            val menu = buildContextMenu()
            menu.show(MainWindow.instance.scene.window, event.screenX, event.screenY)
            event.consume()

        } else {
            event.consume()
            mouseHandler.onMouseReleased(event)
        }
    }

    private fun onMouseMoved(event: MouseEvent) {
        val world = mouseToWorld(event)
        sceneView.mouseX = world.x
        sceneView.mouseY = world.y
        mouseHandler.onMouseMoved(event)
    }


    private fun onEscape() {
        mouseHandler.abandon()
        selection.clear()
        mouseHandler = Select()
    }

    private fun onMouseDragged(event: MouseEvent) {
        val mouseWorld = mouseToWorld(event)
        sceneView.mouseX = mouseWorld.x
        sceneView.mouseY = mouseWorld.y

        dragDelta = mouseWorld - dragStart

        // Prevent dragging unless the drag amount is more than a few pixels. This prevents accidental tiny movements
        if (!dragging) {
            if (Math.abs(dragDelta.x) > 5 || Math.abs(dragDelta.y) > 5) {
                dragging = true
            }
        }
        if (dragging) {
            mouseHandler.onMouseDragged(event)
        }
    }


    private fun buildContextMenu(): ContextMenu {
        val menu = ContextMenu()
        val latestSA = selection.latest()

        val pasteLabel = when (SceneEditor.clipboardActors.size) {
            0 -> "Paste"
            1 -> "Paste '${resources.findNameOrNull(SceneEditor.clipboardActors.first().actor.costume) ?: "<Unknown Costume>"}'"
            else -> "Paste ${SceneEditor.clipboardActors.size} actors"
        }

        if (latestSA == null) {

            // Paste
            if (SceneEditor.clipboardActors.isNotEmpty()) {
                menu.items.add(MenuItem(pasteLabel).apply {
                    onAction = EventHandler {
                        sceneEditor.onPaste()
                    }
                })
            }

        } else {

            val actor = latestSA.actor
            val costume = actor.costume
            val costumeName = resources.findName(costume)

            fun menuItemLabel(prefix: String) = prefix + if (selection.size > 1) {
                "(${selection.size} actors)"
            } else {
                "'$costumeName'"
            }

            // Show Attributes
            if (MainWindow.instance.harbour.findDockable(DOCKABLE_ACTOR_ATTRIBUTES.name) == null) {
                menu.items.add(MenuItem("Show Attributes").apply {
                    onAction = EventHandler { MainWindow.instance.harbour.show(DOCKABLE_ACTOR_ATTRIBUTES.name) }
                })
            }

            // Move to Stage
            if (editingStages.size > 1) {
                val moveToStageMenu = Menu("Move to Stage")
                for (es in editingStages) {
                    val stageItem = CheckMenuItem(es.stageName)
                    stageItem.onAction = EventHandler { moveSelectionToStage(es) }
                    moveToStageMenu.items.add(stageItem)

                    // Add a tick if ALL the selected ActorResources are on this layer.
                    if (selection.items.none { it.editingStage !== es }) {
                        stageItem.isSelected = true
                    }
                }
                menu.items.add(moveToStageMenu)
            }

            // CostumeDetails
            menu.items.add(MenuItem("Costume $costumeName").apply {
                onAction = EventHandler {
                    MainWindow.instance.openNamedTab(costumeName, ResourceType.COSTUME)
                }
            })

            // Pose Details / Texture Details
            (actor.appearance as? AppearanceWithPose)?.let {
                resources.findNameOrNull(it.pose)?.let { poseName ->
                    menu.items.add(MenuItem("Pose $poseName").apply {
                        onAction = EventHandler {
                            MainWindow.instance.openNamedTab(poseName, ResourceType.POSE)
                        }
                    })
                }
                resources.findNameOrNull(it.pose.texture)?.let { textureName ->
                    menu.items.add(MenuItem("Texture ${textureName}").apply {
                        onAction = EventHandler {
                            MainWindow.instance.openNamedTab(textureName, ResourceType.TEXTURE)
                        }
                    })
                }
            }

            // Font Details
            actor.textAppearance?.let {
                val fontResource = it.textStyle.fontResource
                resources.findNameOrNull(fontResource)?.let { fontResourceName ->
                    menu.items.add(MenuItem("Font Details").apply {
                        onAction = EventHandler {
                            MainWindow.instance.openNamedTab(fontResourceName, ResourceType.FONT)
                        }
                    })
                }
            }

            menu.items.add(SeparatorMenuItem())

            // Cut
            menu.items.add(MenuItem(menuItemLabel("Cut ")).apply {
                onAction = EventHandler {
                    sceneEditor.onCut()
                }
            })

            // Copy
            menu.items.add(MenuItem(menuItemLabel("Copy ")).apply {
                onAction = EventHandler {
                    sceneEditor.onCopy()
                }
            })

            // Paste
            menu.items.add(MenuItem(pasteLabel).apply {
                isDisable = SceneEditor.clipboardActors.isEmpty()
                onAction = EventHandler {
                    sceneEditor.onPaste()
                }
            })

            // Duplicate
            menu.items.add(MenuItem(menuItemLabel("Duplicate ")).apply {
                onAction = EventHandler {
                    sceneEditor.onDuplicate()
                }
            })

            // Delete
            menu.items.add(MenuItem(menuItemLabel("Delete ")).apply {
                onAction = EventHandler { sceneEditor.deleteActors() }
            })

            menu.items.add(SeparatorMenuItem())

            // Lock
            val isLocked = selection.items.any { it.actor.isLocked }
            menu.items.add(CheckMenuItem(menuItemLabel("Lock ")).apply {
                sceneEditor.history.beginBatch()
                isSelected = isLocked
                onAction = EventHandler {
                    // NOTE, this is not efficient, as the scene will be redrawn for every actor!
                    for (sa in selection.items) {
                        if (sa.actor.isLocked != isSelected) {
                            sceneEditor.history.makeChange(LockActor(sa.actor, isSelected))
                        }
                    }
                }
                sceneEditor.history.endBatch()
            })

            menu.items.add(SeparatorMenuItem())
        }

        val resetAllZOrders = sceneEditor.actionGroup.menuItem(RESET_ZORDERS)

        menu.items.addAll(resetAllZOrders)

        return menu
    }

    private fun moveSelectionToStage(es: EditingStage) {
        history.beginBatch()
        for (sa in selection.items) {
            if (sa.editingStage !== es) {
                history.makeChange(MoveToStage(sa.actor, sa.editingStage, es))
            }
        }
        history.endBatch()
    }

    fun findActorsAt(world: Vector2, ignoreStageLock: Boolean = false): List<SelectedActor> {
        val list = mutableListOf<SelectedActor>()
        onOpenGLThread {
            editingStages.filter { ignoreStageLock || !it.isLocked }.forEach { es ->
                val actors = es.view.findActorsAt(world)
                list.addAll(actors.map { SelectedActor(it, es) })
            }
        }
        return list
    }


// ****** MouseHandlers *********

    open inner class MouseHandler(initialTip: String) {

        val tipProperty = SimpleStringProperty(initialTip)

        open fun draw(renderer: Renderer) {}

        open fun onMouseClicked(event: MouseEvent) {
        }

        open fun onMousePressed(event: MouseEvent) {
        }

        open fun onDragDetected(event: MouseEvent) {
        }

        open fun onMouseMoved(event: MouseEvent) {
        }

        open fun onMouseDragged(event: MouseEvent) {
        }

        open fun onMouseReleased(event: MouseEvent) {
        }

        /**
         * Called when ESCAPE is pressed.
         * Abandon the changes.
         * Note, [end] will still be called after [abandon].
         */
        open fun abandon() {}

        open fun end() {}
    }

    inner class Select : MouseHandler("") {

        /**
         * Set in [startDragging].
         */
        var selectDragging = false


        private fun updateTip() {
            tipProperty.value = if (selectDragging) {
                "[Ctrl]: Ignore snapping"
            } else {
                val handle = selection.highlightHandle
                if (handle == null) {
                    val xGuide = selection.highlightXGuide
                    if (xGuide == null) {
                        val yGuide = selection.highlightXGuide
                        if (yGuide == null) {

                            if (selection.isEmpty()) {
                                "[Click] to select. [Drag] to select in a rectangular area"
                            } else {
                                "[Ctrl+Click]: Select lower Actors. [Shift+Click]: Adjust selection"
                            }
                        } else {
                            "[Guideline]: horizontal, at Y=$yGuide. [Drag] to move. [Del] to Delete"
                        }
                    } else {
                        "[Guideline]: vertical, at X=$xGuide. [Drag] to move. [Del] to Delete"
                    }
                } else {
                    handle.name
                }
            }
        }

        init {
            updateTip()
        }

        override fun onMousePressed(event: MouseEvent) {
            val mouseWorld = mouseToWorld(event)

            if (event.button == MouseButton.MIDDLE) {
                mouseHandler = Pan()
                return
            }

            selection.findHandle(mouseWorld, EditorPreferences.sceneEditorPreferences.handleSize / scale)
                ?.let { handle ->
                    mouseHandler = AdjustDragHandle(handle)
                    return
                }

            selection.highlightXGuide?.let {
                if (Math.abs(mouseWorld.x - it) < 3.0) {
                    mouseHandler = MoveGuide(isVertical = true, initialValue = it)
                    return
                }
            }
            selection.highlightYGuide?.let {
                if (Math.abs(mouseWorld.y - it) < 3.0) {
                    mouseHandler = MoveGuide(isVertical = false, initialValue = it)
                    return
                }
            }

            var actorsAtPoint = findActorsAt(mouseWorld, ignoreStageLock = event.isAltDown)
            if (!event.isAltDown) actorsAtPoint = actorsAtPoint.filter { !it.actor.isLocked }

            // If we don't press on an already selected actor, then
            // we may be starting a SelectArea (by dragging a rectangular bounding box).
            if (actorsAtPoint.isEmpty()) {
                selection.clear()
                mouseHandler = SelectArea(mouseWorld)
                return
            }

            // Check if any of the actors at this point are already selected.
            // If so, then we can being dragging them.
            val selectedActors = selection.actors
            val atPoint = actorsAtPoint.map { it.actor }
            if (atPoint.any { selectedActors.contains(it) }) {
                startDragging()
            }
        }

        override fun onMouseClicked(event: MouseEvent) {
            val world = mouseToWorld(event)

            if (event.clickCount == 2) {
                MainWindow.instance.harbour.show(DOCKABLE_ACTOR_ATTRIBUTES.name)
                return
            }

            if (event.button == MouseButton.PRIMARY) {

                for (sa in selection.items) {
                    sa.drag.set(world)
                }

                var actorsAtPoint = findActorsAt(world, ignoreStageLock = event.isAltDown)
                if (!event.isAltDown) actorsAtPoint = actorsAtPoint.filter { !it.actor.isLocked }


                val highestActorClicked = actorsAtPoint.firstOrNull() ?: return


                if (event.isShiftDown) {
                    // Add or remove the top-most actor to/from the selection
                    if (selection.contains(highestActorClicked.actor)) {
                        selection.remove(highestActorClicked.actor)
                    } else {
                        selection.add(highestActorClicked)
                    }

                } else if (event.isControlDown) {
                    val actors = actorsAtPoint.map { it.actor }
                    val latest = selection.latest()?.actor
                    if (latest != null && actors.contains(latest)) {
                        // Select the actor below the currently selected actor (i.e. with a higher index)
                        // This is useful when there are many actors on top of each other. We can get to any of them, by repeatedly shift-clicking
                        val i = actors.indexOf(latest)
                        if (i == actorsAtPoint.size - 1) {
                            selection.clearAndSelect(actorsAtPoint.first())
                        } else {
                            selection.clearAndSelect(actorsAtPoint[i + 1])
                        }

                    } else {
                        selection.clearAndSelect(highestActorClicked)
                    }

                } else {
                    selection.clearAndSelect(highestActorClicked)

                }
            }
            updateTip()
        }

        override fun onMouseMoved(event: MouseEvent) {

            if (!selectDragging) {
                val world = mouseToWorld(event)

                // Are we hovering over a Handle?
                val handle = selection.findHandle(world, EditorPreferences.sceneEditorPreferences.handleSize / scale)
                if (handle != selection.highlightHandle) {
                    selection.highlightHandle = handle
                    updateTip()
                    redraw()
                    if (handle != null) return
                }

                // Are we hovering over a Guide?
                with(sceneEditor.snapPreferences.snapToGuides) {
                    var newXGuide: Float? = null
                    for (xGuide in xGuides) {
                        if (Math.abs(world.x - xGuide) < 3.0) {
                            newXGuide = xGuide
                            break
                        }
                    }
                    if (newXGuide != selection.highlightXGuide) {
                        selection.highlightXGuide = newXGuide
                        updateTip()
                        redraw()
                        if (newXGuide != null) return
                    }
                    var newYGuide: Float? = null
                    for (yGuide in yGuides) {
                        if (Math.abs(world.y - yGuide) < 3.0) {
                            newYGuide = yGuide
                            break
                        }
                    }
                    if (newYGuide != selection.highlightYGuide) {
                        selection.highlightYGuide = newYGuide
                        updateTip()
                        redraw()
                    }

                }

            }
        }

        private fun startDragging() {
            for (sa in selection.items) {
                sa.oldX = sa.actor.x
                sa.oldY = sa.actor.y
            }

            selectDragging = true
            updateTip()
        }

        /**
         * Move the selected actors.
         * Note, we are being "naughty" by updating the Actors without using [History].
         * History is updated in [onMouseReleased].
         * If ESCAPE is pressed, then we need to undo the changes manually in [abandon].
         */
        override fun onMouseDragged(event: MouseEvent) {

            if (selectDragging) {

                // Move all the selected actors.
                for (sa in selection.items) {
                    sa.actor.x = (sa.oldX + dragDelta.x).toInt().toFloat()
                    sa.actor.y = (sa.oldY + dragDelta.y).toInt().toFloat()
                }

                // Hold down Ctrl to temporarily turn off snapping.
                if (!event.isControlDown) {
                    // Find snap adjustments for the top-most selected actor only??
                    val adjustments = mutableListOf<SnapToAdjustment>()
                    selection.latest()?.let { latest ->
                        with(sceneEditor.snapPreferences) {
                            snapToGrid.snapPosition(latest.actor.position, adjustments)
                            snapToGuides.snapPosition(latest.actor.position, adjustments)
                            snapToOthers.snapActor(sceneEditor, latest.actor, adjustments)
                        }
                    }

                    // Move all actors by the same amount, so that the top-most selected actor is snapped.
                    bestAdjustment(adjustments)?.let { adjustment ->
                        for (sa in selection.items) {
                            sa.actor.x += adjustment.dx
                            sa.actor.y += adjustment.dy
                        }
                    }
                }

                sceneEditor.fireActorsChanged(selection.actors, ModificationType.CHANGE)
                redraw()
            }
        }

        override fun onMouseReleased(event: MouseEvent) {

            if (selectDragging) {
                selectDragging = false
                // Have any actors moved?
                if (selection.items.any { it.actor.x != it.oldX || it.actor.y != it.oldY }) {
                    // NOTE. Unlike other Changes, MoveActor is created AFTER we have changed the Actor's x,y
                    // (without using History).
                    sceneEditor.history.beginBatch()
                    for (sa in selection.items) {
                        sceneEditor.history.makeChange(MoveActor(sa.actor, sa.oldX, sa.oldY))
                    }
                    sceneEditor.history.endBatch()
                }
            }
            updateTip()
        }

        override fun abandon() {
            if (selectDragging) {
                selectDragging = false
                for (sa in selection.items) {
                    sa.actor.x = sa.oldX
                    sa.actor.y = sa.oldY
                }
                redraw()
            }
        }

    }


    inner class SelectArea(
        val start: Vector2
    ) : MouseHandler("Drag to select Actors in a rectangular area") {

        private var to = Vector2(start)

        override fun draw(renderer: Renderer) {

            val left = worldToViewportX(Math.min(to.x, start.x))
            val bottom = worldToViewportY(Math.min(to.y, start.y))
            val right = worldToViewportX(Math.max(to.x, start.x))
            val top = worldToViewportY(Math.max(to.y, start.y))

            renderer.strokeRect(
                Rect(left, bottom, right, top),
                1 / scale,
                EditorPreferences.sceneEditorPreferences.selectionColor
            )
        }

        override fun onMouseDragged(event: MouseEvent) {
            to = mouseToWorld(event)

            selection.clear()

            val left = Math.min(to.x, start.x)
            val bottom = Math.min(to.y, start.y)
            val right = Math.max(to.x, start.x)
            val top = Math.max(to.y, start.y)

            val toSelect = mutableListOf<SelectedActor>()
            for (es in editingStages) {
                if (es.isVisible && (event.isAltDown || !es.isLocked)) {
                    for (actor in es.stage.actors) {
                        if ((event.isAltDown || !actor.isLocked) && actor.x >= left && actor.y >= bottom && actor.x <= right && actor.y <= top) {
                            toSelect.add(SelectedActor(actor, es))
                        }
                    }
                }
            }
            selection.replace(toSelect)
        }

        override fun onMouseReleased(event: MouseEvent) {
            super.onMouseReleased(event)
            mouseHandler = Select()
            redraw()
        }

        override fun abandon() {
            selection.clear()
        }
    }

    inner class Pan
        : MouseHandler("Drag with the Middle Button to Pan") {

        override fun onMouseDragged(event: MouseEvent) {
            centerX -= dragDelta.x
            centerY -= dragDelta.y
        }

        override fun onMouseReleased(event: MouseEvent) {
            super.onMouseReleased(event)
            mouseHandler = Select()
        }
    }

    inner class AdjustDragHandle(
        val handle: Handle
    ) : MouseHandler(handle.dragTip) {

        init {
            history.beginBatch()
        }

        override fun onMouseDragged(event: MouseEvent) {
            handle.moveTo(mouseToWorld(event), event.isShiftDown, !event.isControlDown)
            sceneEditor.fireActorsChanged(listOf(handle.actor), ModificationType.CHANGE)
        }

        override fun onMouseReleased(event: MouseEvent) {
            handle.end(mouseToWorld(event), event.isShiftDown, !event.isControlDown)
            sceneEditor.fireActorsChanged(listOf(handle.actor), ModificationType.CHANGE)
            super.onMouseReleased(event)
            mouseHandler = Select()
        }

        override fun abandon() {
            sceneEditor.history.abandonBatch()
        }

        override fun end() {
            sceneEditor.history.endBatch()
        }
    }

    inner class Stamp(
        val costume: Costume
    ) : MouseHandler("[Click] to create an Actor. [Shift+Click] to add more") {

        /**
         * [Stamp] Creates a new Actor, which should be drawn semi-transparent while dragging.
         */
        private var stampSelectedActor: SelectedActor = createActor()

        override fun draw(renderer: Renderer) {
            stampSelectedActor.actor.appearance.draw(renderer)
        }

        private fun createActor(): SelectedActor {
            val newActor = costume.createActor("New Actor")

            // Work out which layer the newActor should be placed on.
            var es = editingStages.firstOrNull { it.stageName == costume.stageName }
            if (es == null) {
                val layout = Editor.resources.layouts.find(sceneEditor.scene.layoutName)
                es = editingStages.firstOrNull { it.stageName == layout?.defaultStageName }
            }
            if (es == null) {
                es = editingStages.first()
            }
            return SelectedActor(newActor, es)
        }

        override fun end() {
            redraw()
        }

        override fun onMouseMoved(event: MouseEvent) {
            val world = mouseToWorld(event)
            stampSelectedActor.actor.x = world.x
            stampSelectedActor.actor.y = world.y
            redraw()
        }

        override fun onMousePressed(event: MouseEvent) {
            sceneEditor.history.beginBatch()
            selection.clearAndSelect(stampSelectedActor)
            sceneEditor.history.makeChange(AddActor(stampSelectedActor))
            sceneEditor.history.endBatch()

            if (event.isShiftDown) {

                // Hold down shift to stamp the actor onto the stage, and reset,
                // allowing another copy to be stamped.
                stampSelectedActor = createActor().apply {
                    actor.position.set(mouseToWorld(event))
                }

                selection.clear()

            } else {
                //selection.clearAndSelect(stampSelectedActor)
                mouseHandler = Select()
            }

            redraw()
        }
    }

    inner class CreateGuide(
        val pos: Vector2,
        val isVertical: Boolean
    ) : MouseHandler("[Ctrl]: Both horizontal and vertical guides") {
        var both: Boolean = false


        override fun onMouseDragged(event: MouseEvent) {
            pos.set(mouseToWorld(event))
            both = event.isControlDown
            redraw()
        }

        override fun onMouseReleased(event: MouseEvent) {
            mouseHandler = Select()
            if (isVertical || both) {
                sceneEditor.snapPreferences.snapToGuides.xGuides.add(pos.x)
            }
            if (!isVertical || both) {
                sceneEditor.snapPreferences.snapToGuides.yGuides.add(pos.y)
            }
            redraw()
            sceneEditor.firePreferencesChanged()
        }

        override fun draw(renderer: Renderer) {
            if (isVertical || both) {
                renderer.line(
                    Vector2(pos.x, bottom),
                    Vector2(pos.x, top),
                    1f,
                    EditorPreferences.sceneEditorPreferences.selectionColor
                )
            }
            if (!isVertical || both) {
                renderer.line(
                    Vector2(left, pos.y),
                    Vector2(right, pos.y),
                    1f,
                    EditorPreferences.sceneEditorPreferences.selectionColor
                )
            }
        }
    }


    inner class MoveGuide(
        val initialValue: Float,
        val isVertical: Boolean
    ) : MouseHandler("") {

        val list = if (isVertical) {
            sceneEditor.snapPreferences.snapToGuides.xGuides
        } else {
            sceneEditor.snapPreferences.snapToGuides.yGuides
        }
        val index = list.indexOf(initialValue)

        init {
            if (index < 0) {
                mouseHandler = Select()
            }
        }

        override fun onMouseDragged(event: MouseEvent) {
            val world = mouseToWorld(event)
            if (isVertical) {
                selection.highlightXGuide = Math.round(world.x).toFloat()
                list[index] = world.x
            } else {
                selection.highlightYGuide = Math.round(world.y).toFloat()
                list[index] = world.y
            }
            redraw()
        }

        override fun onMouseReleased(event: MouseEvent) {
            mouseHandler = Select()
            redraw()
            sceneEditor.firePreferencesChanged()
        }

        override fun abandon() {
            if (isVertical) {
                list[index] = initialValue
            } else {
                list[index] = initialValue
            }
        }

    }


// ***** HANDLES *****

    abstract inner class AngleHandle(
        name: String,
        actor: Actor,
        dragTip: String,
        val distance: Float,
        val offset: Float = 0f

    ) : AbstractHandle(name, actor, dragTip) {

        val matrix = Matrix3x2f()

        abstract val radians: Double

        override fun x() =
            actor.x + distance / scale * Math.cos(radians).toFloat()

        override fun y() =
            actor.y + distance / scale * Math.sin(radians).toFloat()

        fun updateMatrix(radians: Float) {
            matrix.identity()
            matrix.translate(worldToViewportX(actor.position.x), worldToViewportY(actor.position.y))
            matrix.rotate(radians)
        }
    }

    inner class RotateHandle(
        actor: Actor,
        val snapRotation: SnapRotation

    ) : AngleHandle(
        "Rotate",
        actor,
        "Rotate Actor. [Ctrl]: Ignore snapping",
        EditorPreferences.sceneEditorPreferences.rotateDistance
    ) {

        init {
            updateMatrix(actor.direction.radians.toFloat())
        }

        override val radians: Double
            get() = actor.direction.radians

        override fun moveTo(pos: Vector2, symmetric: Boolean, snap: Boolean) {

            val dx = pos.x - actor.x
            val dy = pos.y - actor.y

            val atan = Math.atan2(dy.toDouble(), dx.toDouble())
            val degrees = snapRotation.snapRotation(Math.toDegrees(atan), snap)

            val diff = degrees - actor.direction.degrees

            for (sa in selection.items) {
                history.makeChange(RotateActor(sa.actor, sa.actor.direction.degrees + diff))
            }
        }

        override fun update() {
            updateMatrix(actor.direction.radians.toFloat())
        }

        override fun draw(viewportPosition: Vector2, highlight: Boolean) {
            with(Renderer.instance()) {
                val color =
                    if (highlight) EditorPreferences.sceneEditorPreferences.latestSelectionColor else EditorPreferences.sceneEditorPreferences.selectionColor
                strokeAntiAlias(
                    if (highlight) EditorPreferences.sceneEditorPreferences.latestRotateLine else EditorPreferences.sceneEditorPreferences.selectionRotateLine,
                    0.5f,
                    matrix
                )
                drawPose(arrowPose, EditorPreferences.sceneEditorPreferences.rotateDistance, 0f, color, matrix)
            }
        }
    }

    abstract inner class ResizeOrScaleHandle(
        actor: Actor,
        dragTip: String,
        val isLeft: Boolean,
        val isBottom: Boolean
    ) : AbstractHandle("Resize", actor, dragTip) {

        override fun draw(viewportPosition: Vector2, highlight: Boolean) {
            with(Renderer.instance()) {
                val color = if (highlight) {
                    EditorPreferences.sceneEditorPreferences.latestSelectionColor
                } else {
                    EditorPreferences.sceneEditorPreferences.selectionColor
                }
                drawPose(
                    resizeHandlePose,
                    viewportPosition.x, viewportPosition.y,
                    color,
                    EditorPreferences.sceneEditorPreferences.handleSizeScale
                )
            }
        }

        override fun update() {
        }

    }

    inner class ResizeHandle(actor: Actor, isLeft: Boolean, isBottom: Boolean) :
        ResizeOrScaleHandle(actor, "Resize. [Ctrl]: Ignore Snapping. [Shift]: Symmetric", isLeft, isBottom) {

        val appearance = actor.resizeAppearance!!

        override fun moveTo(pos: Vector2, symmetric: Boolean, snap: Boolean) {
            adjust(pos, symmetric, snap, isEnd = false)
        }

        override fun end(pos: Vector2, symmetric: Boolean, snap: Boolean) {
            adjust(pos, symmetric, snap, isEnd = true)
        }

        private fun adjust(world: Vector2, symmetric: Boolean, snap: Boolean, isEnd: Boolean) {
            val snapped = Vector2(world)
            // Hold down Ctrl to temporarily turn off snapping.
            if (snap) {
                // Snap?
                val adjustments = mutableListOf<SnapToAdjustment>()
                with(sceneEditor.snapPreferences) {
                    snapToGrid.snapPosition(snapped, adjustments)
                    snapToGuides.snapPosition(snapped, adjustments)
                    snapToOthers.snapPointToSnapPoints(sceneEditor, snapped, actor, adjustments)
                }
                bestAdjustment(adjustments)?.let { adjustment ->
                    snapped.x += adjustment.dx
                    snapped.y += adjustment.dy
                }
            }
            val oldX = actor.x
            val oldY = actor.y
            val oldSizeX = appearance.size.x
            val oldSizeY = appearance.size.y

            val now = Vector2(snapped.x, snapped.y)
            val was = Vector2(x(), y())
            if (actor.direction.radians != 0.0) {
                now.setRotate(actor.direction)
                was.setRotate(actor.direction)
            }

            val dx = now.x - was.x
            val dy = now.y - was.y

            if (symmetric) {
                appearance.size.x += if (isLeft) -dx * 2 else dx * 2
                appearance.size.y += if (isLeft) -dx * 2 else dx * 2
            } else {
                appearance.size.x += if (isLeft) -dx else dx
                appearance.size.y += if (isBottom) -dy else dy

                actor.x -= (x() - snapped.x)
                actor.y -= (y() - snapped.y)
            }

            if (isEnd) {
                actor.x = Math.round(actor.x).toFloat()
                actor.y = Math.round(actor.y).toFloat()

                appearance.size.x = Math.round(appearance.size.x).toFloat()
                appearance.size.y = Math.round(appearance.size.y).toFloat()
            }

            sceneEditor.history.makeChange(ResizeActor(actor, oldX, oldY, oldSizeX, oldSizeY))
        }

        override fun x(): Float {
            val vector = Vector2(
                ((if (isLeft) 0f else appearance.size.x) - appearance.sizeAlignment.x * appearance.size.x) * actor.scale.x,
                ((if (isBottom) 0f else appearance.size.y) - appearance.sizeAlignment.y * appearance.size.y) * actor.scale.y
            )
            if (actor.direction.radians != 0.0) {
                vector.setRotate(-actor.direction)
            }
            return actor.x + vector.x
        }

        override fun y(): Float {
            val vector = Vector2(
                ((if (isLeft) 0f else appearance.size.x) - appearance.sizeAlignment.x * appearance.size.x) * actor.scale.x,
                ((if (isBottom) 0f else appearance.size.y) - appearance.sizeAlignment.y * appearance.size.y) * actor.scale.y
            )
            if (actor.direction.radians != 0.0) {
                vector.setRotate(-actor.direction)
            }
            return actor.y + vector.y
        }
    }

    inner class ScaleHandle(actor: Actor, isLeft: Boolean, isBottom: Boolean) :
        ResizeOrScaleHandle(actor, "Resize. [Ctrl]: Ignore Snapping. [Shift]: Symmetric", isLeft, isBottom) {

        override fun moveTo(pos: Vector2, symmetric: Boolean, snap: Boolean) {
            adjust(pos, symmetric, snap, isEnd = false)
        }

        override fun end(pos: Vector2, symmetric: Boolean, snap: Boolean) {
            adjust(pos, symmetric, snap, isEnd = true)
        }

        private fun adjust(pos: Vector2, symmetric: Boolean, snap: Boolean, isEnd: Boolean) {
            val snapped = Vector2(pos)
            // Hold down Ctrl to temporarily turn off snapping.
            if (snap) {
                // Snap?
                val adjustments = mutableListOf<SnapToAdjustment>()
                with(sceneEditor.snapPreferences) {
                    snapToGrid.snapPosition(snapped, adjustments)
                    snapToGuides.snapPosition(snapped, adjustments)
                    snapToOthers.snapPointToSnapPoints(sceneEditor, snapped, actor, adjustments)
                }
                bestAdjustment(adjustments)?.let { adjustment ->
                    snapped.x += adjustment.dx
                    snapped.y += adjustment.dy
                }
            }
            val appearance = actor.appearance

            val oldX = actor.x
            val oldY = actor.y
            val oldScaleX = actor.scale.x
            val oldScaleY = actor.scale.y

            val now = Vector2(snapped.x, snapped.y)
            val was = Vector2(x(), y())
            if (actor.direction.radians != 0.0) {
                now.setRotate(actor.direction)
                was.setRotate(actor.direction)
            }

            // Avoiding divide by zero, by making scale small, but non-zero!
            val width = appearance.width() * if (actor.scale.x == 0f) 0.001f else actor.scale.x
            val height = appearance.height() * if (actor.scale.y == 0f) 0.001f else actor.scale.y

            val factorX = if (isLeft) (width + was.x - now.x) / width else (width + now.x - was.x) / width
            val factorY = if (isBottom) (height + was.y - now.y) / height else (height + now.y - was.y) / height

            if (symmetric) {
                actor.scale.x *= factorX * factorX
                actor.scale.y *= factorY * factorY
            } else {
                actor.scale.x *= factorX
                actor.scale.y *= factorY

                actor.x -= x() - snapped.x
                actor.y -= y() - snapped.y
            }

            if (isEnd) {
                actor.x = Math.round(actor.x).toFloat()
                actor.y = Math.round(actor.y).toFloat()
            }

            sceneEditor.history.makeChange(ScaleActor(actor, oldX, oldY, oldScaleX, oldScaleY))
        }

        override fun x(): Float {
            val appearance = actor.appearance
            val vector = Vector2(
                (if (isLeft) -appearance.offsetX() else appearance.width() - appearance.offsetX()) * actor.scale.x,
                (if (isBottom) -appearance.offsetY() else appearance.height() - appearance.offsetY()) * actor.scale.y
            )
            if (actor.direction.radians != 0.0) {
                vector.setRotate(-actor.direction)
            }
            return actor.x + vector.x
        }

        override fun y(): Float {
            val appearance = actor.appearance
            val vector = Vector2(
                (if (isLeft) -appearance.offsetX() else appearance.width() - appearance.offsetX()) * actor.scale.x,
                (if (isBottom) -appearance.offsetY() else appearance.height() - appearance.offsetY()) * actor.scale.y
            )
            if (actor.direction.radians != 0.0) {
                vector.setRotate(-actor.direction)
            }
            return actor.y + vector.y
        }
    }

    companion object {

        val guiTexture: Texture = try {
            onOpenGLThread { Texture.load(Editor::class.java.getResourceAsStream("gui.png")!!) }
        } catch (e: Exception) {
            severe("Failed to load gui.png : $e")
            Texture(1, 1, GL11.GL_RGBA, null)
        }

        val resizeHandlePose = Pose(guiTexture, YDownRect(0, 0, 12, 12)).apply {
            offsetX = 6f
            offsetY = 6f
        }
        val arrowPose = Pose(guiTexture, YDownRect(12, 0, 24, 12)).apply {
            offsetX = 6f
            offsetY = 6f
        }


    }

}

/**
 * I'm not using a Behaviour, all input is done through [SceneViewSkin].
 */
class SceneViewBehavior(sceneView: SceneView) : BehaviorBase<SceneView>(sceneView, emptyList())
