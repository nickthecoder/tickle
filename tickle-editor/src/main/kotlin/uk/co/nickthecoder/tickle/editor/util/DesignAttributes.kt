/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.util

import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.parameters.*
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.misc.SimpleInstance
import uk.co.nickthecoder.tickle.util.*
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.jvmErasure


class DesignAttributes : RuntimeAttributes() {

    /**
     * Gets the values of the [name]d attribute, returning its value
     */
    fun getValue(name: String): Any? {
        val data = getOrCreateData(name)
        return data.parameter?.copy()?.let { p ->
            p.stringValue = data.value ?: ""
            p.value
        }
    }

    /**
     * Gets the values of the [name]d attribute.
     * Note, [value] should be of the correct type. e.g. don't try to assign a Float to a Vector2
     */
    fun setValue(name: String, value: Any) {
        val data = getOrCreateData(name)
        data.parameter?.copy()?.let {
            it.coerce(value)
            data.value = it.stringValue
        }
    }

    fun updateParameterValues(instance: Any, alternate: Boolean = false) {
        val klass = instance.javaClass.kotlin
        for (ad in map.values) {
            for ((name, data) in map) {
                if (data.isAlternate == alternate) {
                    val dad = data as DesignAttributeData
                    val property =
                        klass.memberProperties.filterIsInstance<KProperty1<Any?, Any?>>()
                            .firstOrNull { it.name == name }
                    if (property == null) {
                        warn("Could not find a property called '$name' on class '${klass.qualifiedName}'")
                        continue
                    }
                    try {
                        dad.parameter?.coerce(property.get(instance))
                    } catch (e: Exception) {
                        warn("Failed to update parameter ${dad.parameter?.name} of ${instance.javaClass}")
                    }
                }
            }
        }
    }

    fun updateAttributesMetaData(instance: Any) {
        val klass = instance.javaClass
        val kClass = klass.kotlin

        /*
         * Copy the list of attribute names, and as we find their corresponding property, remove them from the list
         * Any names remaining are old attributes, but the class no longer has that property, so the attribute should
         * be removed.
         */
        val toDiscard = map.keys.toMutableSet()

        // Should this be memberProperties rather than members?
        kClass.members.forEach { property ->
            property.annotations.filterIsInstance<Attribute>().firstOrNull()?.let { annotation ->
                val hasExistingValue = map[property.name]?.value != null
                val attributeData = getOrCreateData(property.name)
                attributeData.isRelative = annotation.isRelative
                attributeData.isPosition = annotation.isPosition
                attributeData.isDirection = annotation.isDirection
                attributeData.order = annotation.order
                attributeData.scale = annotation.scale
                createParameter(
                    property.name,
                    annotation.about,
                    property.returnType.jvmErasure,
                    hasAlpha = annotation.hasAlpha,
                    rows = annotation.rows,
                    isSceneName = annotation.isSceneName,
                    isActorName = annotation.isActorName
                )?.let { parameter ->

                    attributeData.parameter = parameter
                    // When the parameter changes, update the String representation of the value in [data] (AttributeData)
                    parameter.listen {
                        attributeData.value = parameter.stringValue
                        try {
                            @Suppress("UNCHECKED_CAST")
                            (property as KMutableProperty1<Any, Any?>).set(instance, parameter.value)
                        } catch (e: Exception) {
                            // Do nothing
                        }
                    }

                    if (!hasExistingValue) {
                        // I believe this is safe, because this class creates the parameters based on the return type
                        // of the property. So this is safe as long as createParameter is correct.
                        @Suppress("UNCHECKED_CAST")
                        val theValue = (property as KProperty1<Any, Any>).get(instance)
                        @Suppress("UNCHECKED_CAST")
                        (attributeData.parameter as ValueParameter<Any>).value = theValue
                        attributeData.value = attributeData.parameter!!.stringValue
                    } else {
                        attributeData.value?.let { attributeData.parameter!!.stringValue = it }
                    }
                }
                toDiscard.remove(property.name)
            }
            property.annotations.filterIsInstance<CostumeAttribute>().firstOrNull()?.let { annotation ->
                val data = getOrCreateData(property.name)
                data.isAlternate = true
                data.order = annotation.order

                createParameter(
                    property.name,
                    annotation.about,
                    property.returnType.jvmErasure,
                    hasAlpha = annotation.hasAlpha,
                    rows = annotation.rows,
                    isSceneName = annotation.isSceneName,
                    isActorName = false

                )?.let { parameter ->

                    data.parameter = parameter
                    parameter.listen { data.value = parameter.stringValue }
                }
                toDiscard.remove(property.name)
            }
        }

        toDiscard.forEach { name ->
            warn("Removing attribute : $name. Not used by class '$klass'.")
            map.remove(name)
        }

    }

    /**
     * Uses reflection to scan the Class for fields using the @Attribute or @CostumeAttribute annotations.
     * Creates Parameters for each annotated field. The Parameter is given a ParameterListener which updates the
     * string representation of the data whenever the parameter changes.
     *
     * When isDesigning == true, an instance of the Role (or other class that uses Attributes) is created, so that
     * we can find the default classValue the field has immediately after creation. In this way, we can show the default
     * classValue in the Editor/SceneEditor.
     */
    override fun updateAttributesMetaData(klass: Class<*>) {
        val instance: Any = try {
            klass.getDeclaredConstructor().newInstance()
        } catch (e: Exception) {
            severe(e)
            return
        }
        updateAttributesMetaData(instance)
    }

    private fun createParameter(
        name: String,
        about: String,
        klass: KClass<*>,
        hasAlpha: Boolean,
        rows: Int,
        isSceneName: Boolean,
        isActorName: Boolean
    ): ValueParameter<*>? {

        return when (klass) {

            Boolean::class -> {
                BooleanParameter("attribute_$name", required = false, label = name, description = about)
            }

            Int::class -> {
                IntParameter("attribute_$name", required = false, label = name, description = about)
            }

            Float::class -> {
                FloatParameter(
                    "attribute_$name",
                    required = false,
                    minValue = -Float.MAX_VALUE,
                    label = name,
                    description = about
                )
            }

            Double::class -> {
                DoubleParameter(
                    "attribute_$name",
                    required = false,
                    minValue = -Double.MAX_VALUE,
                    label = name,
                    description = about
                )
            }

            String::class -> {
                when {
                    isSceneName -> {
                        SceneNameParameter("attribute_$name", label = name, description = about)
                    }

                    isActorName -> {
                        ActorNameParameter("attribute_$name", label = name, description = about)
                    }

                    else -> {
                        StringParameter(
                            "attribute_$name", required = false, label = name,
                            rows = rows, description = about
                        )
                    }
                }
            }

            Polar2d::class -> {
                PolarParameter("attribute_$name", label = name, description = about)
            }

            Vector2::class -> {
                Vector2Parameter("attribute_$name", label = name, description = about)
            }

            Angle::class -> {
                AngleParameter("attribute_$name", label = name, description = about)
            }

            Color::class -> {
                if (hasAlpha) {
                    TickleAlphaColorParameter("attribute_$name", label = name, description = about)
                } else {
                    TickleColorParameter("attribute_$name", label = name, description = about)
                }
            }

            else -> {
                if (klass.java.isEnum) {
                    @Suppress("UNCHECKED_CAST")
                    createEnumParameter(klass as KClass<out Enum<*>>, "attribute_$name", name, description = about)

                } else if (SimpleInstance::class.java.isAssignableFrom(klass.java)) {
                    ClassInstanceParameter("attribute_$name", label = name, type = klass.java)
                } else {
                    severe("Type $klass (for attribute $name) is not currently supported.")
                    null
                }
            }
        }
    }


    override fun getOrCreateData(name: String): DesignAttributeData {
        map[name]?.let { return it as DesignAttributeData }
        val data = DesignAttributeData()
        map[name] = data
        return data
    }

    override fun copy(): DesignAttributes {
        val copied = DesignAttributes()
        map.forEach { (key, data) ->
            if (data is DesignAttributeData) {
                copied.map[key] = DesignAttributeData(
                    data.value, data.order, data.scale, data.isAlternate, null,
                    isRelative = data.isRelative, isPosition = data.isPosition,
                    isDirection = data.isDirection
                )
            }
        }
        return copied
    }
}

inline fun <reified T : Enum<T>> printAllValues() {
    print(enumValues<T>().joinToString { it.name })
}

class DesignAttributeData(

    value: String? = null,
    order: Int = 0,
    scale: Float = 1f,
    isAlternate: Boolean = false, // True for CostumeAttributes
    var parameter: ValueParameter<*>? = null,
    // The following give hints to the SceneEditor to add draggable control handles which can be use to set the value.
    var isRelative: Boolean = false, // For Vector2
    var isPosition: Boolean = false, // For Vector2
    var isDirection: Boolean = false // For Angle

) : AttributeData(value, order, scale, isAlternate) {

    override fun toString() =
        "${super.toString()} isRelative($isRelative} isPosition($isPosition) isDirection($isDirection)"
}

