/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor

import javafx.application.Application
import javafx.application.Platform
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.stage.Stage
import uk.co.nickthecoder.paratask.util.AutoExit
import uk.co.nickthecoder.tickle.DelayedErrorHandler
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.editor.resources.DesignResources
import uk.co.nickthecoder.tickle.editor.resources.ResourcesReaderWriter
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.graphics.ignoreErrors
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.info
import java.io.File

/**
 * Grr. To access HostServices, we need the Application instance, so
 * let's keep a reference here. Bloody JavaFX making me have litter in my application! Grr.
 */
lateinit var application: Application

class Editor : Application() {

    init {
        application = this
    }

    override fun start(primaryStage: Stage) {
        startEditor(primaryStage)
    }

    override fun stop() {
        onOpenGLThread {
            info("Stopping JavaFX Application, deleting GL context")
            ignoreErrors { glWindow.delete() }
            info("Deleted GL Window")

            ignoreErrors { OpenGL.end() }
            info("Ended OpenGL")
        }
        info("GL Has been clean up, now stop JavaFX")

        super.stop() // Does nothing, but I think this is good form anyway.
        info("The application should end now")
    }

    companion object {

        lateinit var glWindow: Window

        @JvmStatic
        val resources = DesignResources()

        @JvmStatic
        var resourceFile: File? = null

        /**
         * Starts the Editor Application.
         *
         * Note, we ALSO need to start the "Editor", but from a DIFFERENT [Application] instance
         * (the launcher), in this case, call [startEditor].
         */
        fun startEditorApplication(file: File? = null) {
            // Errors and warnings are stored, so that when the main window is created, we can display
            // them.
            TickleErrorHandler.instance = DelayedErrorHandler(TickleErrorHandler.instance)

            OpenGL.begin()
            onOpenGLThread {
                glWindow = Window("Tickle Editor Hidden Window", 100, 100)
            }
            resourceFile = file
            launch(Editor::class.java)
        }

        /**
         * Starts the editor, assuming that the JavaApplication has already started (i.e. from the LauncherApp)
         */
        fun startEditor(glWindow : Window, file: File) {
            Editor.glWindow = glWindow
            resourceFile = file
            startEditor(Stage())
        }

        /**
         * Starts the editor, from an already started [Application]. This can be [Editor],
         * of the Launcher.
         */
        private fun startEditor(primaryStage: Stage) {
            try {
                primaryStage.icons.add(Image(Game::class.java.getResource("tickle.png")!!.toExternalForm()))
                val mainWindow = MainWindow(primaryStage, glWindow)

                onOpenGLThread {
                    try {
                        info("Loading resources")
                        ResourcesReaderWriter.load(resources, resourceFile!!)
                        info("Loaded resources")

                    } catch (e: Exception) {
                        e.printStackTrace()
                        Platform.exit()
                    }
                }
                info("Finishing setting up the main window...")
                mainWindow.resourcesLoaded()
                info("Finished setting up the main window")
                AutoExit.disable()

            } catch (e: Exception) {
                e.printStackTrace()
                Platform.exit()
            }
        }

        fun imageResource(name: String): Image? {
            val res = Editor::class.java.getResource(name) ?: Editor::class.java.getResource("$name.png")
            ?: Editor::class.java.getResource("icons/$name") ?: Editor::class.java.getResource("icons/$name.png")
            ?: return null
            return Image(res.toExternalForm())
        }

        fun imageView(name: String): ImageView? {
            val image = imageResource(name)
            return if (image == null) null else ImageView(image)
        }
    }
}

