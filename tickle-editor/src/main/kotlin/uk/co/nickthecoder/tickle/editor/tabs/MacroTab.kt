package uk.co.nickthecoder.tickle.editor.tabs

import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import javafx.scene.control.ToolBar
import javafx.scene.paint.Color
import javafx.util.StringConverter
import uk.co.nickthecoder.feather.CompilationFailed
import uk.co.nickthecoder.feather.StringScript
import uk.co.nickthecoder.feather.internal.isStatic
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.MacroStub
import uk.co.nickthecoder.tickle.feather.FeatherLanguage
import uk.co.nickthecoder.tickle.scripts.ScriptException
import uk.co.nickthecoder.tickle.severe
import java.io.File
import java.lang.reflect.Method

class MacroTab(data: MacroStub) : ScriptTab(data.file, data) {

    private val methodChoiceBox = ChoiceBox<Method>()

    private var classLoader: ClassLoader? = null

    private val dummyMethod = MacroTab::class.java.getMethod("dummyMethod")

    private val runButton = Button("Run").apply {
        onAction = EventHandler { run() }
    }

    fun dummyMethod() {}

    init {
        methodChoiceBox.items.add(dummyMethod)
        methodChoiceBox.value = dummyMethod

        methodChoiceBox.converter = object : StringConverter<Method>() {
            override fun fromString(string: String): Method? {
                val dot = string.lastIndexOf('.')
                if (classLoader == null || dot <= 0) return null
                val className = string.substring(0, dot)
                val methodName = string.substring(dot + 1)
                return try {
                    val klass = classLoader!!.loadClass(className)
                    klass.getMethod(methodName)
                } catch (e: Exception) {
                    dummyMethod
                }

            }

            override fun toString(method: Method?): String {
                if (method == null || method === dummyMethod) return "<None>"
                return "${method.declaringClass.name}.${method.name}"
            }

        }

        val toolBar = ToolBar().apply {
            items.addAll(
                Label("Methods"),
                methodChoiceBox,
                Label("WARNING! Macros have full access to your computer! Do you trust it?").apply {
                    textFill = Color.DARKRED
                },
                runButton
            )
        }
        borderPane.top = toolBar

    }

    /**
     * Recompile the macro, even when the code hasn't been changed.
     * Without this, the first time a macro is opened, the [methodChoiceBox] won't be populated.
     */
    override fun save(): Boolean {
        val result = super.save()
        compileMacro()
        runButton.requestFocus()
        return result
    }

    fun compileMacro() {
        MainWindow.instance.log.clear()

        try {
            val oldMethodString = methodChoiceBox.converter.toString(methodChoiceBox.value)
            methodChoiceBox.items.clear()

            val (cl, classNames) = FeatherLanguage.compileMacroScript(StringScript("macro", codeEditor.scarea.text))
            classLoader = cl
            for (className in classNames.toList().sorted()) {
                val klass = cl.loadClass(className)
                for (method in klass.declaredMethods.sortedBy { it.name }) {
                    if (method.isStatic() && method.parameterCount == 0) {
                        methodChoiceBox.items.add(method)
                    }
                }
            }
            if (methodChoiceBox.items.isEmpty()) {
                methodChoiceBox.items.add(dummyMethod)
            }

            if (oldMethodString.isNullOrBlank()) {
                methodChoiceBox.value = methodChoiceBox.items.firstOrNull()
            } else {
                methodChoiceBox.value =
                    methodChoiceBox.converter.fromString(oldMethodString) ?: methodChoiceBox.items.firstOrNull()
            }

        } catch (e: CompilationFailed) {
            val first = e.errors.first()
            severe(
                ScriptException(
                    first.message ?: e.message ?: "",
                    e,
                    first.pos.source?.let { File(it) },
                    first.pos.line,
                    first.pos.column
                )
            )
        } catch (e: Exception) {
            severe(e)
            e.printStackTrace()
        }
    }

    fun run() {
        MainWindow.instance.log.clear()
        val method = methodChoiceBox.value ?: return
        val result = method.invoke(null)
        if (result != null) {
            println("Result type : ${result.javaClass.name}")
            println(result)
        }
    }

    companion object {
        fun createMacro(dir: File, name: String): File {
            dir.mkdirs()
            val file = File(dir, "${name}.feather")
            file.writeText(MACRO_TEMPLATE)
            return file
        }
    }
}

private const val MACRO_TEMPLATE = """
// Ctrl+S to save & compile
// Select a method from the pull-down above, and then click Run.
// The return value is printed as a string in the Log (dockable).

// All static methods with no parameters are placed in the pull-down.

class Macro {
    static fun hello() : String {
        return "Hello World"
    }
    static fun math() = 1 + 2
}
"""
