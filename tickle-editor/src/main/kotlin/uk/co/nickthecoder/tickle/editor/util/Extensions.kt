/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.util

import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.geometry.Rectangle2D
import javafx.scene.Node
import javafx.scene.canvas.GraphicsContext
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.image.WritableImage
import javafx.scene.layout.*
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import javafx.stage.Stage
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.tasks.CopyResourceTask
import uk.co.nickthecoder.tickle.editor.tasks.RenameResourceTask
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.misc.CopyableResource
import uk.co.nickthecoder.tickle.util.DeletableResource
import uk.co.nickthecoder.tickle.misc.PixelArray
import uk.co.nickthecoder.tickle.misc.RenamableResource


/*
 Contains many extension functions, used from within the SceneEditor.
 These are functions that I don't want on the actual objects, because I don't want them available
 from a running Game, and some may have dependencies only available from the editor module, which will not
 be in the class path during an actual game.
 e.g. I may not want a dependency on awt, or javafx during an actual game, which will make porting a game to
 Android easier.
*/


// Texture

fun Texture.toImage() = PixelArray(this).toImage()

fun Texture.cachedImage() = ImageCache.image(this)

fun Texture.toImageView(cacheImage: Boolean = true): ImageView {
    val image = if (cacheImage) cachedImage() else toImage()
    return ImageView(image)
}

fun PixelArray.toImage(): Image {
    val image = WritableImage(width, height)
    val writer = image.pixelWriter
    for (y in 0 until height) {
        for (x in 0 until width) {
            writer.setArgb(x, y, argbColorAt(x, y))
        }
    }
    return image
}

// POSE

fun Pose.toImageView(cacheImage: Boolean = true): ImageView {
    val iv = texture.toImageView(cacheImage)
    iv.viewport = Rectangle2D(
        pixelRect.left.toDouble(),
        pixelRect.top.toDouble(),
        pixelRect.width.toDouble(),
        pixelRect.height.toDouble()
    )
    return iv
}

fun Pose.thumbnail(size: Int): ImageView {
    val iv = toImageView()

    if (iv.viewport.width > size || iv.viewport.height > size) {
        iv.isPreserveRatio = true
        if (iv.viewport.width > iv.viewport.height) {
            iv.fitWidth = size.toDouble()
        } else {
            iv.fitHeight = size.toDouble()
        }
    }

    return iv
}

fun Pose.isOverlapping(x: Float, y: Float): Boolean {
    return x > -offsetX && x < pixelRect.width - offsetX &&
            y > -offsetY && y < pixelRect.height - offsetY
}

/**
 * x,y are relative to the "offset point" of the pose, with the Y axis pointing upwards
 */
fun Pose.isPixelIsOpaque(x: Float, y: Float, threshold: Float = 0.05f): Boolean {
    val px = pixelRect.left + x + offsetX
    val py = pixelRect.top + pixelRect.height - (y + offsetY)
    texture.file?.let { file ->
        return ImageCache.image(file).pixelReader.getColor(px.toInt(), py.toInt()).opacity > threshold
    }
    return false
}

// COLOR Conversions

fun Color.toJavaFX(): javafx.scene.paint.Color {
    return javafx.scene.paint.Color(red.toDouble(), green.toDouble(), blue.toDouble(), alpha.toDouble())
}

fun javafx.scene.paint.Color.toTickle(): Color {
    return Color(red.toFloat(), green.toFloat(), blue.toFloat(), opacity.toFloat())
}

private val squareCorners = CornerRadii(0.0)

private val noInsets = Insets(0.0)

fun Color.background() = Background(BackgroundFill(toJavaFX(), squareCorners, noInsets))

fun Alert.addStyleSheet() {
    dialogPane.stylesheets.add(MainWindow::class.java.getResource("tickle.css")!!.toExternalForm())
}

fun RenamableResource.renamePrompted(oldName: String) {
    TaskPrompter(RenameResourceTask(this, oldName)).placeOnStage(Stage())
}

fun CopyableResource<*>.copyPrompted(oldName: String) {
    TaskPrompter(CopyResourceTask(this, oldName)).placeOnStage(Stage())
}

fun DeletableResource.deletePrompted(name: String) {

    val usedBy = dependables(Editor.resources)

    val breakables = FlowPane()
    val unbreakables = FlowPane()
    val resourceLabel = ResourceType.resourceType(this)?.label ?: ""

    for (dependency in usedBy) {
        val editButton = createEditButton(dependency)
        if (dependency.isBreakable(this)) {
            breakables.children.add(editButton)
        } else {
            unbreakables.children.add(editButton)
        }
    }

    if (unbreakables.children.isNotEmpty()) {

        val vBox = VBox().apply { styleClass.add("form") }
        val heading = Label("Because of the following dependencies : ").apply { styleClass.add("heading") }
        vBox.children.addAll(heading, unbreakables)

        if (breakables.children.isNotEmpty()) {
            vBox.children.addAll(Label("These dependencies also exist, but can be broken : "), breakables)
        }

        val alert = Alert(Alert.AlertType.INFORMATION)

        with(alert) {
            addStyleSheet()
            title = "Cannot Delete $resourceLabel '$name'"
            headerText = title
            dialogPane.content = vBox
            showAndWait()
        }

    } else {

        val alert = Alert(Alert.AlertType.CONFIRMATION)

        with(alert) {
            addStyleSheet()
            title = "Delete $resourceLabel '$name'"
            headerText = title

            if (breakables.children.isNotEmpty()) {

                val vBox = VBox().apply { styleClass.add("form") }
                val label = Label("This will break the following dependencies : ")
                vBox.children.addAll(label, breakables)
                dialogPane.content = vBox
            }

            showAndWait()

        }

        if (alert.result == ButtonType.OK) {
            usedBy.forEach { it.breakDependency(this) }
            delete(Editor.resources)
        }
    }
}

fun createEditButton(resource: Any): Button {
    val name = Editor.resources.findNameOrNull(resource) ?: "<unknown>"

    val button = Button(name)
    button.onAction = EventHandler {
        MainWindow.instance.openTab(name, resource)
    }

    val resourceType = ResourceType.resourceType(resource)
    resourceType?.let {
        button.text += " (${resourceType.label})"
        button.graphic = resourceType.createImageView()
    }
    return button
}

// Graphics

fun GraphicsContext.scale(x: Float, y: Float) {
    scale(x.toDouble(), y.toDouble())
}

fun GraphicsContext.translate(x: Float, y: Float) {
    translate(x.toDouble(), y.toDouble())
}

fun GraphicsContext.strokeLine(x1: Float, y1: Float, x2: Float, y2: Float) {
    strokeLine(x1.toDouble(), y1.toDouble(), x2.toDouble(), y2.toDouble())
}

fun GraphicsContext.strokeRect(x: Float, y: Float, width: Float, height: Float) {
    strokeRect(x.toDouble(), y.toDouble(), width.toDouble(), height.toDouble())
}

fun GraphicsContext.strokeOval(x: Float, y: Float, width: Float, height: Float) {
    strokeOval(x.toDouble(), y.toDouble(), width.toDouble(), height.toDouble())
}

/**
 * Listens to [listenTo], and update `this` property value based on the result of the [transformation].
 */
fun <S, R> Property<R>.transformedBind(listenTo: Property<S>, transformation: (S) -> R) {
    listenTo.addListener { _, _, newValue ->
        value = transformation(newValue)
    }
}

/**
 * Returns a TextFlow, whose contents come from a StringProperty,
 * where the value can contain formatting information to apply bold text.
 */
fun formattedTextFlow(sp: StringProperty): TextFlow {
    val result = TextFlow()

    fun update(fromIndex: Int, str: String) {
        val openIndex = str.indexOf("[", fromIndex)
        if (openIndex >= 0) {
            val closeIndex = str.indexOf("]", fromIndex + 1)
            if (closeIndex > 0) {
                if (openIndex != fromIndex) {
                    result.children.add(Text(str.substring(fromIndex, openIndex)))
                }
                result.children.add(
                    Text(str.substring(openIndex+1, closeIndex)).apply {
                        style = "-fx-font-weight: bold"
                    }
                )
                update(closeIndex + 1, str)
                return
            }
        }
        val remainder = str.substring(fromIndex)
        if (remainder.isNotBlank()) {
            result.children.add(Text(remainder))
        }
    }

    update(0, sp.value)
    sp.addListener { _, _, newValue ->
        result.children.clear()
        update(0, newValue)
    }
    return result
}

fun Node.alignMiddle() = VBox().apply {
    alignment = Pos.CENTER
    children.addAll(this@alignMiddle)
}

fun expandH() = HBox().apply { HBox.setHgrow(this, Priority.ALWAYS) }
