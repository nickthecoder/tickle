/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.control.*
import javafx.stage.Modality
import javafx.stage.Stage
import org.jbox2d.dynamics.BodyType
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParameterException
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.parameters.fields.TaskForm
import uk.co.nickthecoder.paratask.util.FileLister
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.parameters.*
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.resources.SceneWriter
import uk.co.nickthecoder.tickle.editor.resources.ScriptStub
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes
import uk.co.nickthecoder.tickle.graphics.TextStyle
import uk.co.nickthecoder.tickle.physics.*
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.sound.Sound
import uk.co.nickthecoder.tickle.util.SceneReader
import uk.co.nickthecoder.tickle.util.Vector2

class CostumeTab(val name: String, val costume: Costume)

    : EditTab(name, costume, ResourceType.COSTUME.createImageView()) {

    val detailsTask = CostumeDetailsTask()
    val detailsForm = TaskForm(detailsTask)

    val eventsTask = CostumeEventsTask()
    val eventsForm = TaskForm(eventsTask)

    val physicsTask = PhysicsTask()
    val physicsForm = TaskForm(physicsTask)

    val minorTabs = TabPane()

    val detailsTab = Tab("Details", detailsForm.build())
    val eventsTab = Tab("Events", eventsForm.build())
    val physicsTab = Tab("Physics", physicsForm.build())

    val posesButton = MenuButton("Poses")

    val scriptButton = Button("Edit Script")

    init {
        minorTabs.side = Side.BOTTOM
        minorTabs.tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

        minorTabs.tabs.add(detailsTab)
        minorTabs.tabs.add(eventsTab)
        if (Editor.resources.gameInfo.physicsEngine) {
            minorTabs.tabs.add(physicsTab)
        }

        borderPane.center = minorTabs

        buildPosesButton()
        buildScriptButton()

        detailsTask.taskD.root.listen { needsSaving = true }
        eventsTask.taskD.root.listen { needsSaving = true }
        physicsTask.taskD.root.listen { needsSaving = true }
        detailsTask.roleClassP.listen { buildScriptButton() }
        detailsTask.roleClassP.onAttributesChanged { needsSaving = true }
    }

    private fun buildScriptButton() {
        val file = detailsTask.roleClassP.scriptFile
        if (file == null) {
            leftButtons.children.remove(scriptButton)
        } else {
            scriptButton.onAction = EventHandler { MainWindow.instance.openTab(costume.roleString, ScriptStub(file)) }
            if (!leftButtons.children.contains(scriptButton)) {
                leftButtons.children.add(scriptButton)
            }
        }
    }

    private fun buildPosesButton() {
        val poses = mutableSetOf<Pose>()
        costume.events.values.forEach { event ->
            poses.addAll(event.poses)
            poses.addAll(event.ninePatches.map { it.pose })
        }

        if (poses.isEmpty()) {
            leftButtons.children.remove(posesButton)
        } else {
            posesButton.items.clear()
            poses.forEach { pose ->
                Editor.resources.poses.findName(pose)?.let { name ->
                    val menuItem = MenuItem(name)
                    posesButton.items.add(menuItem)
                    menuItem.onAction = EventHandler { MainWindow.instance.openTab(name, pose) }
                }
            }
            if (!leftButtons.children.contains(posesButton)) {
                leftButtons.children.add(posesButton)
            }
        }
    }

    /**
     * Costumes are "removed" when they change group, so we need to make sure that the
     * costume HAS actually been removed before closing.
     */
    override fun resourceRemoved(resource: Any, name: String) {
        if (resource === data && Editor.resources.costumes.find(name) == null) {
            super.resourceRemoved(resource, name)
        }
    }

    override fun justSave(): Boolean {
        if (detailsForm.check()) {
            if (eventsForm.check()) {
                if (physicsForm.check()) {
                    detailsTask.run()
                    eventsTask.run()
                    physicsTask.run()

                    return true
                } else {
                    tabPane.selectionModel.select(physicsTab)
                }
            } else {
                tabPane.selectionModel.select(eventsTab)
            }
        } else {
            tabPane.selectionModel.select(detailsTab)
        }
        return false
    }

    /**
     * [resource] can be a [Pose] or [Sound].
     * Other event types cannot be removed this way (yet).
     * Called from ResourcesTree to allow events to be removed from there.
     */
    fun removeEvent(eventName: String, resource: Any) {
        minorTabs.selectionModel.select(eventsTab)
        eventsTask.eventsP.innerParameters.toList().forEach { eventParameter ->
            if (eventParameter.eventNameP.value == eventName) {
                if (eventParameter.soundEventP.soundP.value === resource
                    || eventParameter.poseP.value === resource
                    || eventParameter.costumeP.value === resource
                ) {
                    eventsTask.eventsP.remove(eventParameter)
                    //eventsTask.eventsP.innerParameters.remove(eventParameter)
                }
            }
        }
    }


    inner class CostumeDetailsTask : AbstractTask(false) {

        private val nameP = StringParameter("name", value = name)

        val roleClassP = ClassAndAttributesParameter("role", Role::class.java, true, value = NoRole::class.java)

        private val canRotateP = BooleanParameter("canRotate")

        private val canScaleP = BooleanParameter("canScale")

        private val zOrderP = FloatParameter("zOrder")

        private val stageNameP = ChoiceParameter<String>("stageName")

        private val costumeGroupP = CostumeGroupParameter { chooseCostumeGroup(it) }

        private val showInSceneEditorP = BooleanParameter("showInSceneEditor", value = costume.showInSceneEditor)

        private var commentsP = StringParameter("comments", required = false, rows = 5)

        override val taskD = TaskDescription("costumeDetails")
            .addParameters(
                nameP,
                roleClassP,
                canRotateP,
                canScaleP,
                zOrderP,
                stageNameP,
                costumeGroupP,
                showInSceneEditorP,
                commentsP
            )

        init {
            nameP.value = name
            roleClassP.classString = costume.roleString
            roleClassP.attributes = (costume.attributes as DesignAttributes).copy()
            canRotateP.value = costume.canRotate
            canScaleP.value = costume.canScale
            zOrderP.value = costume.zOrder
            stageNameP.value = costume.stageName
            costumeGroupP.costumeGroupValue = costume.costumeGroup
            commentsP.value = costume.comments

            // Populate the stageNameP combobox list with all the stage names from all of the layouts.
            stageNameP.addChoice("", "", "<None>")
            val stageNames = mutableSetOf<String>()
            Editor.resources.layouts.items().values.forEach { layout ->
                layout.layoutStages.keys.forEach { stageName ->
                    stageNames.add(stageName)
                }
            }
            stageNames.sorted().forEach { stageName ->
                stageNameP.addChoice(stageName, stageName, stageName)
            }

        }

        override fun run() {
            costume.roleString = roleClassP.classString
            costume.attributes = roleClassP.attributes!!
            costume.canRotate = canRotateP.value == true
            costume.canScale = canScaleP.value == true
            costume.zOrder = zOrderP.value!!
            costume.stageName = stageNameP.value ?: ""
            costume.showInSceneEditor = showInSceneEditorP.value == true
            costume.comments = commentsP.value

            if (costume.costumeGroup != costumeGroupP.costumeGroupValue) {
                if (costume.costumeGroup == null) {
                    Editor.resources.fireRemoved(costume, name)
                } else {
                    costume.costumeGroup?.remove(costume)
                }
                costume.costumeGroup = costumeGroupP.costumeGroupValue
                if (costume.costumeGroup == null) {
                    Editor.resources.fireAdded(costume, name)
                } else {
                    costume.costumeGroup?.add(nameP.value, costume)
                }
            }
            if (nameP.value != name) {
                val cg = costumeGroupP.costumeGroupP.value
                if (cg == null) {
                    Editor.resources.costumes.rename(name, nameP.value)
                } else {
                    cg.rename(name, nameP.value)
                }
                TaskPrompter(RenameCostumeTask(name, nameP.value)).placeOnStage(Stage())
            }

        }


        fun chooseCostumeGroup(groupName: String) {
            costumeGroupP.costumeGroupP.value = Editor.resources.costumeGroups.find(groupName)
        }

    }

    inner class CostumeEventsTask : AbstractTask(false) {

        val initialEventP = StringParameter("initialEvent", value = costume.initialEventName)

        val inheritEventsFromP =
            createCostumeParameter("inheritEventsFrom", required = false, value = costume.inheritEventsFrom)

        val eventsP = MultipleParameter("events") {
            EventParameter()
        }.asListDetail(isBoxed = true, allowReordering = false) { it.toString() }

        override val taskD = TaskDescription("costumeEvents")
            .addParameters(initialEventP, inheritEventsFromP, eventsP)

        init {
            buildEventsP()
        }

        fun buildEventsP() {

            eventsP.clear()

            costume.events.forEach { eventName, event ->
                event.poses.forEach { pose ->
                    val inner = eventsP.newValue()
                    inner.eventNameP.value = eventName
                    inner.poseP.value = pose
                    inner.typeP.value = inner.poseP
                }
                event.costumes.forEach { costume ->
                    val inner = eventsP.newValue()
                    inner.eventNameP.value = eventName
                    inner.costumeP.value = costume
                    inner.typeP.value = inner.costumeP
                }
                event.textStyles.forEach { textStyle ->
                    val inner = eventsP.newValue()
                    inner.eventNameP.value = eventName
                    inner.textStyleP.from(textStyle)
                    inner.typeP.value = inner.textStyleP
                }
                event.strings.forEach { str ->
                    val inner = eventsP.newValue()
                    inner.eventNameP.value = eventName
                    inner.stringP.value = str
                    inner.typeP.value = inner.stringP
                }
                event.soundEvents.forEach { soundEvent ->
                    val inner = eventsP.newValue()
                    inner.eventNameP.value = eventName
                    inner.soundEventP.from(soundEvent)
                    inner.typeP.value = inner.soundEventP
                }
                event.ninePatches.forEach { ninePatch ->
                    val inner = eventsP.newValue()
                    inner.eventNameP.value = eventName
                    inner.ninePatchP.from(ninePatch)
                    inner.typeP.value = inner.ninePatchP
                }
            }
        }

        override fun run() {
            costume.initialEventName = initialEventP.value
            costume.inheritEventsFrom = inheritEventsFromP.value

            costume.events.clear()
            eventsP.innerParameters.forEach { inner ->
                when (inner.typeP.value) {
                    inner.poseP -> addPose(inner.eventNameP.value, inner.poseP.value!!)
                    inner.costumeP -> addCostume(inner.eventNameP.value, inner.costumeP.value!!)
                    inner.textStyleP -> addTextStyle(inner.eventNameP.value, inner.textStyleP.createTextStyle())
                    inner.stringP -> addString(inner.eventNameP.value, inner.stringP.value)
                    inner.soundEventP -> addSoundEvent(inner.eventNameP.value, inner.soundEventP.createSoundEvent())
                    inner.ninePatchP -> addNinePatch(inner.eventNameP.value, inner.ninePatchP.createNinePatch())
                }
            }
        }

        fun addPose(eventName: String, pose: Pose) {
            var event = costume.events[eventName]
            if (event == null) {
                event = CostumeEvent(costume)
                costume.events[eventName] = event
            }
            event.poses.add(pose)
        }

        fun addCostume(eventName: String, cos: Costume) {
            var event = costume.events[eventName]
            if (event == null) {
                event = CostumeEvent(costume)
                costume.events[eventName] = event
            }
            event.costumes.add(cos)
        }

        fun addTextStyle(eventName: String, textStyle: TextStyle) {
            var event = costume.events[eventName]
            if (event == null) {
                event = CostumeEvent(costume)
                costume.events[eventName] = event
            }
            event.textStyles.add(textStyle)
        }

        fun addString(eventName: String, str: String) {
            var event = costume.events[eventName]
            if (event == null) {
                event = CostumeEvent(costume)
                costume.events[eventName] = event
            }
            event.strings.add(str)
        }

        fun addSoundEvent(eventName: String, soundEvent: SoundEvent) {
            var event = costume.events[eventName]
            if (event == null) {
                event = CostumeEvent(costume)
                costume.events[eventName] = event
            }
            event.soundEvents.add(soundEvent)
        }

        fun addNinePatch(eventName: String, ninePatch: NinePatch) {
            var event = costume.events[eventName]
            if (event == null) {
                event = CostumeEvent(costume)
                costume.events[eventName] = event
            }
            event.ninePatches.add(ninePatch)
        }
    }

    inner class EventParameter : MultipleGroupParameter("event") {

        val eventNameP = StringParameter("eventName")

        val poseP = createPoseParameter()

        val textStyleP = TextStyleParameter("style")

        val costumeP = createCostumeParameter()

        val stringP = StringParameter("string")

        val soundEventP = createSoundParameter()

        val ninePatchP = createNinePatchParameter()

        val typeP = OneOfParameter("type", value = null, choiceLabel = "Type")
            .addChoices(poseP, costumeP, textStyleP, stringP, soundEventP, ninePatchP)

        init {
            addParameters(eventNameP, typeP, poseP, ninePatchP, costumeP, textStyleP, stringP, soundEventP)
        }

        override fun toString(): String {
            val eventName = if (eventNameP.value.isBlank()) "<no name>" else eventNameP.value
            val type = typeP.value?.label ?: ""
            val dataName = when (typeP.value) {
                poseP -> Editor.resources.poses.findName(poseP.value)
                costumeP -> Editor.resources.costumes.findName(costumeP.value)
                textStyleP -> Editor.resources.fontResources.findName(textStyleP.fontP.value)
                stringP -> stringP.value
                soundEventP -> Editor.resources.sounds.findName(soundEventP.soundP.value)
                ninePatchP -> "Nine Patch (${Editor.resources.poses.findName(ninePatchP.poseP.value)})"
                else -> ""
            }
            return "$eventName ($type) $dataName"
        }
    }

    inner class PhysicsTask : AbstractTask(false) {

        val bodyTypeP = ChoiceParameter<BodyType?>("bodyType", required = false, value = costume.bodyDef?.type)
            .nullableEnumChoices(mixCase = true, nullLabel = "None")

        val linearDampingP = DoubleParameter(
            "linearDamping", value = costume.bodyDef?.linearDamping
                ?: 0.0, minValue = 0.0, maxValue = 10.0
        )

        val angularDampingP = DoubleParameter(
            "angularDamping", value = costume.bodyDef?.angularDamping
                ?: 0.0, minValue = 0.0, maxValue = 10.0
        )

        val fixedRotationP = BooleanParameter("fixedRotation", value = costume.bodyDef?.fixedRotation ?: false)

        val bulletP = BooleanParameter("bullet", value = costume.bodyDef?.bullet ?: false)

        val physicsDetailsP = SimpleGroupParameter("physicsDetails1", label = "").apply {
            addParameters(bodyTypeP, linearDampingP, angularDampingP, fixedRotationP, bulletP)
            asHorizontal(labelPosition = LabelPosition.LEFT)
        }
        val physicsDetailsBoxP = SimpleGroupParameter("physicsDetails").apply {
            addParameters(physicsDetailsP)
            asBox()
        }


        val fixturesP = MultipleParameter("fixtures", minItems = 1) {
            FixtureParameter()
        }.asListDetail(isBoxed = true) { param ->
            param.toString()
        }

        override val taskD = TaskDescription("physics")
            .addParameters(physicsDetailsBoxP, fixturesP)

        init {
            costume.bodyDef?.fixtureDefs?.forEach { fixtureDef ->
                val fixtureParameter = fixturesP.newValue()
                fixtureParameter.initParameters(fixtureDef)
            }
            updateHiddenFields()
            bodyTypeP.listen {
                updateHiddenFields()
            }
        }

        fun updateHiddenFields() {
            val static = bodyTypeP.value == BodyType.STATIC || bodyTypeP.value == null
            linearDampingP.hidden = static
            angularDampingP.hidden = static
            fixedRotationP.hidden = static
            bulletP.hidden = static

            fixturesP.hidden = !(bodyTypeP.value?.hasFixtures() ?: false)
        }

        override fun run() {
            if (bodyTypeP.value == null) {
                costume.bodyDef = null
            } else {
                val bodyDef = costume.bodyDef ?: TickleBodyDef()
                costume.bodyDef = bodyDef

                with(bodyDef) {
                    type = bodyTypeP.value!!
                    linearDamping = linearDampingP.value!!
                    angularDamping = angularDampingP.value!!
                    bullet = bulletP.value == true
                    fixedRotation = fixedRotationP.value!!
                }

                bodyDef.fixtureDefs.clear()
                fixturesP.innerParameters.forEach { fixtureParameter ->
                    bodyDef.fixtureDefs.add(fixtureParameter.createCostumeFixtureDef())
                }
            }
        }

        inner class FixtureParameter() : MultipleGroupParameter("fixture") {

            val densityP = FloatParameter("density", minValue = 0f, value = 1f)
            val frictionP = FloatParameter("friction", minValue = 0f, maxValue = 1f, value = 0f)
            val restitutionP = FloatParameter("restitution", minValue = 0f, maxValue = 1f, value = 1f)
            val isSensorP = BooleanParameter("isSensor", value = false)
            val dfrGroupP = SimpleGroupParameter("densityFrictionRestitution", label = "").apply {
                addParameters(densityP, frictionP, restitutionP, isSensorP)
                asHorizontal(labelPosition = LabelPosition.LEFT)
            }

            val circleCenterP = Vector2Parameter("circleCenter", label = "Center")
            val circleRadiusP = FloatParameter("circleRadius", label = "Radius")

            val circleP = SimpleGroupParameter("circle")
                .addParameters(circleCenterP, circleRadiusP)

            val boxSizeP = Vector2Parameter("boxSize")
            val boxCenterP = Vector2Parameter("boxCenter", label = "Center")
            val boxAngleP = AngleParameter("boxAngle", label = "Angle")

            val boxP = SimpleGroupParameter("box")
                .addParameters(boxSizeP, boxCenterP, boxAngleP)

            val polygonInfo = InformationParameter(
                "polygonInfo",
                information = "Note. The polygon must be convex. Create more than one fixture to build concave objects."
            )
            val polygonPointsP = MultipleParameter("polygonPoints", minItems = 2) {
                Vector2Parameter("point").asHorizontal()
            }
            val polygonP = SimpleGroupParameter("polygon")
                .addParameters(polygonInfo, polygonPointsP)

            val shapeP = OneOfParameter("shape", choiceLabel = "Type")
                .addChoices(
                    "Circle" to circleP,
                    "Box" to boxP,
                    "Polygon" to polygonP
                )

            val shapeEditorButtonP =
                ButtonParameter("editShape", label = "", buttonText = "Edit Shape") { onEditShape() }
            val shapeGroupP = SimpleGroupParameter("").apply {
                addParameters(shapeP)
                asHorizontal(labelPosition = LabelPosition.LEFT)
            }

            val fixtureDetailsP = SimpleGroupParameter("fixtureDetails").apply {
                addParameters(dfrGroupP, shapeGroupP)
                asBox()
            }

            val filterGroupP = createFilterGroupParameter()
            val filterCategoriesP = createFilterBitsParameter("categories", "I Am")
            val filterMaskP = createFilterBitsParameter("mask", "I Collide With")

            init {
                addParameters(fixtureDetailsP, circleP, boxP, polygonP, filterGroupP, filterCategoriesP, filterMaskP)
                asPlain()
                (costume.pose() ?: costume.chooseNinePatch("default")?.pose)?.let {
                    shapeGroupP.addParameters(shapeEditorButtonP)

                    circleRadiusP.value = 15f
                    boxSizeP.xP.value = 15f
                    boxSizeP.yP.value = 15f
                }

                bodyTypeP.listen {
                    densityP.hidden = bodyTypeP.value != BodyType.DYNAMIC && bodyTypeP.value != BodyType.STATIC
                    frictionP.hidden = bodyTypeP.value != BodyType.DYNAMIC && bodyTypeP.value != BodyType.STATIC
                }
                // Why are we firing a change event?
                bodyTypeP.parameterListeners.fireValueChanged(bodyTypeP, bodyTypeP.value)
            }

            fun initParameters(fixtureDef: TickleFixtureDef) {
                with(fixtureDef) {
                    densityP.value = density
                    frictionP.value = friction
                    restitutionP.value = restitution
                    isSensorP.value = isSensor
                    filterGroupP.value = filter.groupIndex
                    filterCategoriesP.value = filter.categoryBits
                    filterMaskP.value = filter.maskBits
                }
                with(fixtureDef.shapeDef) {
                    when (this) {
                        is CircleDef -> {
                            shapeP.value = circleP
                            circleCenterP.x = center.x
                            circleCenterP.y = center.y
                            circleRadiusP.value = radius
                        }

                        is BoxDef -> {
                            shapeP.value = boxP
                            boxSizeP.xP.value = width
                            boxSizeP.yP.value = height
                            boxCenterP.value = center
                            boxAngleP.value = angle
                        }

                        is PolygonDef -> {
                            shapeP.value = polygonP
                            polygonPointsP.clear()
                            points.forEach { point ->
                                polygonPointsP.addValue(point)
                            }
                        }
                    }
                }
            }

            override fun check() {
                super.check()

                // Check if the shape is concave.
                // Compute the dot product of the each line with the previous line's normal. For a convex shape
                // the dot products should all be positive or all be negative.
                // BTW, despite what people think, maths isn't my strong suit, so there may be a more efficient method.
                // Note. this doesn't detect "star" shapes, such as a witches pentagram, but it is unlikely somebody
                // would create such a shape! Summing the interior angles, and test if it is significantly over 2 PI
                // would fix that, but I can't be bothered. Sorry.
                var negativeCount = 0
                val points = polygonPointsP.innerParameters
                if (shapeP.value == polygonP) {
                    points.forEachIndexed { index, pointP ->
                        val point = pointP.value
                        val prev = points[if (index == 0) points.size - 1 else index - 1].value
                        val next = points[if (index == points.size - 1) 0 else index + 1].value
                        val line1 = prev - point
                        val line2 = next - point
                        val normal1 = Vector2(line1.y, -line1.x)
                        val dotProduct = normal1.dot(line2)
                        if (dotProduct < 0.0) {
                            negativeCount++
                        }
                        // If any lines are straight the dot product will be zero. That is redundant point and will just a waste of CPU!
                        if (dotProduct == 0f) {
                            throw ParameterException(shapeP, "Contains a redundant point #$index")
                        }
                    }
                }
                if (negativeCount > 0 && negativeCount != points.size) {
                    throw ParameterException(
                        shapeP,
                        "The shape is concave. Create 2 (or more) fixtures which are all convex."
                    )
                }
            }

            fun createShapeDef(): ShapeDef? {
                try {
                    return when (shapeP.value) {
                        circleP -> {
                            CircleDef(Vector2(circleCenterP.x!!, circleCenterP.y!!), circleRadiusP.value!!)
                        }

                        boxP -> {
                            BoxDef(boxSizeP.xP.value!!, boxSizeP.yP.value!!, boxCenterP.value, boxAngleP.value)
                        }

                        polygonP -> {
                            PolygonDef(polygonPointsP.value)
                        }

                        else -> {
                            null
                        }
                    }
                } catch (e: KotlinNullPointerException) {
                    return null
                }
            }

            fun createCostumeFixtureDef(): TickleFixtureDef {
                val shapeDef = createShapeDef() ?: throw IllegalStateException("Not a valid shape")

                val fixtureDef = TickleFixtureDef(shapeDef)
                with(fixtureDef) {
                    density = densityP.value!!
                    friction = frictionP.value!!
                    restitution = restitutionP.value!!
                    isSensor = isSensorP.value == true
                    filter.groupIndex = filterGroupP.value!!
                    filter.categoryBits = filterCategoriesP.value
                    filter.maskBits = filterMaskP.value
                }

                return fixtureDef
            }

            fun onEditShape() {
                (costume.pose() ?: costume.chooseNinePatch("default")?.pose)?.let { pose ->
                    val task = ShapeEditorTask(pose, this)
                    val stage = Stage()
                    // always on top didn't work for me (on linux using open jdk and the open javafx.
                    // stage.isAlwaysOnTop = true
                    // So using a modal dialog instead (not what I wanted. grrr).
                    stage.initModality(Modality.APPLICATION_MODAL)
                    TaskPrompter(task, showCancel = false).placeOnStage(stage)

                    task.shapeEditorP.update(createShapeDef())
                    listen { task.shapeEditorP.update(createShapeDef()) }
                }
            }

            override fun toString(): String {
                return when (shapeP.value) {
                    circleP -> {
                        "Circle @ ${circleCenterP.x} , ${circleCenterP.y}"
                    }

                    boxP -> {
                        "Box @ ${boxCenterP.value.x} , ${boxCenterP.value.y}"
                    }

                    polygonP -> {
                        "Polygon (${polygonPointsP.value.size} points)"
                    }

                    else -> {
                        "Unknown"
                    }
                }
            }
        }

    }

}


fun createFilterGroupParameter(): ChoiceParameter<Int> {
    val choiceP = ChoiceParameter("collisionFilterGroup", value = 0)

    val filterGroup = ScriptManager.classForName(Editor.resources.gameInfo.physicsInfo.filterGroupsString)
        .getDeclaredConstructor().newInstance() as FilterGroups

    filterGroup.values().forEach { name, value ->
        choiceP.addChoice(name, value, name)
    }
    if (filterGroup.values().size <= 1) {
        choiceP.hidden = true
    }
    return choiceP
}

fun createFilterBitsParameter(name: String, label: String) = FilterBitsParameter(name, label)

class FilterBitsParameter(
    name: String,
    label: String
) : SimpleGroupParameter(name, label) {

    val filterMasks = ScriptManager.classForName(Editor.resources.gameInfo.physicsInfo.filterBitsString)
        .getDeclaredConstructor().newInstance() as FilterBits

    init {
        filterMasks.values().forEach { maskName, _ ->
            val param = BooleanParameter("${name}_$maskName", label = maskName, value = false)
            addParameters(param)
        }
        asGrid(labelPosition = LabelPosition.LEFT, columns = filterMasks.columns(), isBoxed = true)
        if (filterMasks.values().isEmpty()) {
            hidden = true
        }
    }

    var value: Int
        get() {
            if (hidden) return 0xFFFF
            var value = 0
            filterMasks.values().forEach { maskName, bit ->
                val param = find("${name}_$maskName") as BooleanParameter
                if (param.value == true) {
                    value += bit
                }
            }
            return value
        }
        set(v) {
            filterMasks.values().forEach { maskName, bit ->
                val param = find("${name}_$maskName") as BooleanParameter
                param.value = (v and bit) != 0
            }
        }

}

class RenameCostumeTask(val oldCostumeName: String, val newCostumeName: String) : AbstractTask(false) {

    val informationP = InformationParameter(
        "infoP",
        information = "To rename a costume requires loading and saving all scene.\nThis may take a few seconds."
    )

    override val taskD = TaskDescription("renameCostume")
        .addParameters(informationP)

    override fun run() {
        val fileLister = FileLister(extensions = listOf("scene"), depth = 10)
        fileLister.listFiles(Editor.resources.sceneDirectory).forEach { file ->
            val scene = SceneReader.load(file, Editor.resources, mergeIncludes = false)
            var changed = false

            for (stage in scene.stages()) {
                for (actor in stage.actors) {
                    val costumeName = Editor.resources.findNameOrNull(actor.costume)
                    if (costumeName == oldCostumeName) {
                        changed = true
                    }
                }
            }
            if (changed) {
                val preferences = SceneWriter.loadPreferences(file)
                SceneWriter.save(file, scene, preferences, Editor.resources)
            }

        }
    }

}