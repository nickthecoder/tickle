/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.parameters

import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.SoundEvent
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.sound.Sound


fun createTextureParameter(parameterName: String = "texture"): ChoiceParameter<Texture?> {

    val parameter = ChoiceParameter<Texture?>(name = parameterName, required = true, value = null)

    Editor.resources.textures.items().forEach { name, texture ->
        parameter.addChoice(name, texture, name)
    }
    return parameter
}


fun createPoseParameter(parameterName: String = "pose", label: String = "Pose", required: Boolean = true): GroupedChoiceParameter<Pose?> {

    val choiceParameter = GroupedChoiceParameter<Pose?>(parameterName, label = label, required = required, value = null)

    if (!required) {
        choiceParameter.addChoice("", null, "None")
    }
    Editor.resources.textures.items().forEach { textureName, texture ->
        val group = choiceParameter.group(textureName)
        Editor.resources.poses.items().filter { it.value.texture === texture }.forEach { poseName, pose ->
            group.choice(poseName, pose, poseName)
        }

    }
    return choiceParameter
}


fun createFontParameter(parameterName: String = "font", label: String = "Font"): ChoiceParameter<FontResource?> {

    val choice = ChoiceParameter<FontResource?>(parameterName, label = label, required = true, value = null)

    Editor.resources.fontResources.items().forEach { name, fontResource ->
        choice.addChoice(name, fontResource, name)
    }
    return choice
}


fun createCostumeParameter(parameterName: String = "costume", required: Boolean = true, value: Costume? = null): GroupedChoiceParameter<Costume?> {

    val choiceParameter = GroupedChoiceParameter<Costume?>(parameterName, required = required, value = value)

    if (!required) {
        choiceParameter.addChoice("", null, "None")
    }

    val defaultGroup = choiceParameter.group("")
    Editor.resources.costumes.items().filter { it.value.costumeGroup == null }.forEach { costumeName, costume ->
        defaultGroup.choice(costumeName, costume, costumeName)
    }
    Editor.resources.costumeGroups.items().forEach { groupName, costumeGroup ->
        val group = choiceParameter.group(groupName)
        costumeGroup.items().forEach { costumeName, costume ->
            group.choice(costumeName, costume, costumeName)
        }
    }
    return choiceParameter
}

class SoundEventParameter(parameterName: String, label: String) : SimpleGroupParameter(parameterName, label = label) {

    val soundP = ChoiceParameter<Sound?>("$parameterName-sound", label = "Sound", required = true, value = null)
    val gainP = FloatParameter("$parameterName-gain", label = "Gain", required = true, value = 1.0f)
    val playP = ButtonParameter("$parameterName-play", label = "", buttonText = "Play") { onPlay() }


    val onlyWhenVisibleP = BooleanParameter(
        "$parameterName-onlyWhenVisible",
        label ="Only play when visible?",
        required = true,
        value = false
    )
    val newPriorityP = FloatParameter(
        "$parameterName-priority",
        label = "Priority",
        required = true,
        value = SoundEvent.DEFAULT_PRIORITY,
        minValue = 0.0f,
        maxValue = SoundEvent.MAX_PRIORITY
    )

    val priorityThresholdP = FloatParameter(
        "$parameterName-threshold",
        label = "Threshold",
        required = true,
        value = SoundEvent.DEFAULT_PRIORITY,
        minValue = 0.0f,
        maxValue = SoundEvent.MAX_PRIORITY
    )

    val onlyOnceP = BooleanParameter(
        "$parameterName-onlyOnce",
        label ="Only one at a time?",
        required = true,
        value = false
    )

    init {
        addParameters(soundP, gainP, playP, onlyWhenVisibleP, onlyOnceP, newPriorityP, priorityThresholdP)
        Editor.resources.sounds.items().forEach { name, sound ->
            soundP.addChoice(name, sound, name)
        }

    }

    fun from(soundEvent: SoundEvent) {
        soundP.value = soundEvent.sound
        gainP.value = soundEvent.gain
        onlyWhenVisibleP.value = soundEvent.onlyWhenVisible
        newPriorityP.value = soundEvent.newPriority
        priorityThresholdP.value = soundEvent.priorityThreshold
        onlyOnceP.value = soundEvent.onlyOnce
    }

    fun createSoundEvent() =
        SoundEvent(soundP.value!!, gainP.value!!, onlyWhenVisibleP.value!!, newPriorityP.value!!, priorityThresholdP.value!!, onlyOnceP.value!!)


    private fun onPlay() {
        onOpenGLThread {
            soundP.value?.play(gainP.value ?: 1.0f)
        }
    }
}

fun createSoundParameter(parameterName: String = "soundEvent") =
    SoundEventParameter(parameterName, label = "Sound Event")

fun createNinePatchParameter(parameterName: String = "ninePatch", label: String = "NinePatch") =
    NinePatchParameter(parameterName, label)
