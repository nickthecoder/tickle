package uk.co.nickthecoder.tickle.editor.scene

import javafx.geometry.Rectangle2D
import javafx.scene.image.PixelBuffer
import javafx.scene.image.PixelFormat
import javafx.scene.image.WritableImage
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.*
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import java.nio.ByteBuffer

/**
 * Renders anything using OpenGL, and then the results are available via [image], which is a
 * [WritableImage], suitable for use in JavaFX.
 * This is used to draw a Scene in the [SceneEditor], so we have a JavaFX application
 * rendering using OpenGL.
 *
 * NOTE. This implementation isn't optimal, because the image is transferred from the Texture in the GPU
 * into main memory, and then JavaFX will renderer it (putting it back onto the GPU however it see fit).
 * On the plus side, it is cross-platform, and we do get to use both JavaFX and OpenGL together. ;-)
 */
class ImageTransfer(width: Int, height: Int) {

    var width: Int = width
        private set

    var height: Int = height
        private set

    private var texture = Texture(width, height, GL_RGB, null)
    private var buffer = ByteBuffer.allocateDirect(width * height * 4)
    private val pixelFormat: PixelFormat<ByteBuffer> = PixelFormat.getByteBgraPreInstance()
    private var pixelBuffer = PixelBuffer(width, height, buffer, pixelFormat)
    private var rect = Rectangle2D(0.0, 0.0, width.toDouble(), height.toDouble())

    var image = WritableImage(pixelBuffer)
        private set

    fun resize(width: Int, height: Int) {
        if (width == 0 || height == 0) return

        onOpenGLThread {
            this.width = width
            this.height = height
            texture.destroy()
            texture = Texture(width, height, GL_RGB, null)
            buffer = ByteBuffer.allocateDirect(width * height * 4)
            pixelBuffer = PixelBuffer(width, height, buffer, pixelFormat)
            image = WritableImage(pixelBuffer)
            rect = Rectangle2D(0.0, 0.0, width.toDouble(), height.toDouble())
        }
    }

    fun draw(action: () -> Unit) {
        onOpenGLThread {
            with(Renderer.instance()) {
                outputTexture = texture
                action()
                outputTexture = null
            }

            texture.bind()
            glGetTexImage(GL_TEXTURE_2D, 0, GL_BGRA, GL_UNSIGNED_BYTE, buffer)
            Texture.unbind()
        }
        pixelBuffer.updateBuffer { rect }
    }

}
