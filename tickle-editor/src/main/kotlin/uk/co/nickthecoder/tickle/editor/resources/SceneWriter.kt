/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.resources

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import uk.co.nickthecoder.tickle.Scene
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.scene.SnapPreferences
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.TextHAlignment
import uk.co.nickthecoder.tickle.graphics.TextVAlignment
import uk.co.nickthecoder.tickle.misc.JsonUtil
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.stage.Stage
import java.io.*

object SceneWriter {

    fun loadPreferences(file: File): SnapPreferences {

        val jRoot = Json.parse(InputStreamReader(FileInputStream(file))).asObject()
        val preferences = SnapPreferences()
        jRoot.get("snapToGrid")?.let {
            val jGrid = it.asObject()

            with(preferences.snapToGrid) {
                enabledProperty.value = jGrid.getBoolean("enabled", false)

                spacing.x = jGrid.getFloat("xSpacing", 50f)
                spacing.y = jGrid.getFloat("ySpacing", 50f)

                offset.x = jGrid.getFloat("xOffset", 0f)
                offset.y = jGrid.getFloat("yOffset", 0f)

                closeness.x = jGrid.getFloat("xCloseness", 10f)
                closeness.y = jGrid.getFloat("yCloseness", 10f)
            }
        }

        jRoot.get("snapToGuides")?.let { jSnapToGuides ->
            val jGuides = jSnapToGuides.asObject()
            with(preferences.snapToGuides) {
                enabledProperty.value = jGuides.getBoolean("enabled", true)
                closeness = jGuides.getFloat("closeness", 10f)

                jGuides.get("x")?.let { jXValue ->
                    val jx = jXValue.asArray()
                    jx.forEach { xGuides.add(it.asFloat()) }
                }
                jGuides.get("y")?.let { jYValue ->
                    val jy = jYValue.asArray()
                    jy.forEach { yGuides.add(it.asFloat()) }
                }
            }
        }

        jRoot.get("snapToOthers")?.let { jSnapToOthers ->
            val jOthers = jSnapToOthers.asObject()
            with(preferences.snapToOthers) {
                enabledProperty.value = jOthers.getBoolean("enabled", true)
                closeness.x = jOthers.getFloat("xCloseness", 10f)
                closeness.y = jOthers.getFloat("yCloseness", 10f)
            }

        }

        jRoot.get("snapRotation")?.let { jSnapRotation ->
            val jRotation = jSnapRotation.asObject()
            with(preferences.snapRotation) {
                enabledProperty.value = jRotation.getBoolean("enabled", true)
                stepDegrees = jRotation.getDouble("step", 15.0)
                closeness = jRotation.getDouble("closeness", 15.0)
                roundWhenNotSnapping = jRotation.getBoolean("roundWhenNotSnapping", true)
            }

        }

        return preferences
    }

    fun save(file: File, scene: Scene, preferences: SnapPreferences, resources: Resources) {

        val jRoot = JsonObject()
        jRoot.add("director", scene.director.javaClass.name)
        val directorAttributes = DesignAttributes()
        directorAttributes.updateAttributesMetaData(scene.director)
        JsonUtil.saveAttributes(jRoot, directorAttributes, "directorAttributes")

        jRoot.add("background", scene.background.toHashRGB())
        jRoot.add("showMouse", scene.showMouse)
        jRoot.add("comments", scene.comments, "")
        jRoot.add("layout", scene.layoutName)

        val jIncludes = JsonArray()
        jRoot.add("include", jIncludes)
        scene.includedScenes.keys.forEach { include ->
            jIncludes.add(include)
        }

        savePreferences(jRoot, preferences)

        val jStages = JsonArray()
        jRoot.add("stages", jStages)
        for ((stageName, stage) in scene.stages) {
            if (stage.actors.isNotEmpty()) {
                jStages.add(saveStage(resources, stageName, stage))
            }
        }

        BufferedWriter(OutputStreamWriter(FileOutputStream(file))).use {
            jRoot.writeTo(it, EditorPreferences.outputFormat.writerConfig)
        }

    }

    private fun savePreferences(jRoot: JsonObject, preferences: SnapPreferences) {

        val jGrid = JsonObject()
        jRoot.add("snapToGrid", jGrid)
        with(preferences.snapToGrid) {
            jGrid.add("enabled", enabledProperty.value == true)

            jGrid.add("xSpacing", spacing.x)
            jGrid.add("ySpacing", spacing.y)

            jGrid.add("xOffset", offset.x)
            jGrid.add("yOffset", offset.y)

            jGrid.add("xCloseness", closeness.x)
            jGrid.add("yCloseness", closeness.y)
        }

        val jGuides = JsonObject()
        jRoot.add("snapToGuides", jGuides)
        with(preferences.snapToGuides) {
            jGuides.add("enabled", enabledProperty.value)
            jGuides.add("closeness", closeness)

            val jx = JsonArray()
            xGuides.forEach { jx.add(it) }
            jGuides.add("x", jx)

            val jy = JsonArray()
            yGuides.forEach { jy.add(it) }
            jGuides.add("y", jy)
        }

        val jOthers = JsonObject()
        jRoot.add("snapToOthers", jOthers)
        with(preferences.snapToOthers) {
            jOthers.add("enabled", enabledProperty.value)

            jOthers.add("xCloseness", closeness.x)
            jOthers.add("yCloseness", closeness.y)
        }

        val jRotation = JsonObject()
        jRoot.add("snapRotation", jRotation)
        with(preferences.snapRotation) {
            jRotation.add("enabled", enabledProperty.value)
            jRotation.add("step", stepDegrees)
            jRotation.add("closeness", closeness)
            jRotation.add("roundWhenNotSnapping", roundWhenNotSnapping)
        }
    }

    private fun saveStage(resources: Resources, stageName: String, stage: Stage): JsonObject {

        val jStage = JsonObject()
        jStage.add("name", stageName)
        val jActors = JsonArray()
        jStage.add("actors", jActors)

        for (actor in stage.actors) {
            val costume = actor.costume
            val costumeName = resources.findNameOrNull(costume) ?: continue

            val jActor = JsonObject()
            jActors.add(jActor)
            with(actor) {
                jActor.add("costume", costumeName)
                if (name.isNotBlank()) jActor.add("name", name)
                jActor.add("x", x)
                jActor.add("y", y)
                if (zOrder != costume.zOrder) { // Only save zOrders which are NOT the default zOrder for the Costume.
                    jActor.add("zOrder", zOrder)
                }

                jActor.add("color", tint, Color.white())

                jActor.add("isLocked", isLocked, false)

                val defaultStyle = actor.costume.chooseTextStyle(actor.costume.initialEventName)
                actor.textAppearance?.let { textAppearance ->
                    val textStyle = textAppearance.textStyle
                    jActor.add("text", textAppearance.text)
                    jActor.addFont("font", textStyle.fontResource, defaultStyle?.fontResource)
                    jActor.add("textHAlignment", textStyle.halignment, defaultStyle?.halignment)
                    jActor.add("textVAlignment", textStyle.valignment, defaultStyle?.valignment)
                    jActor.addColor("textColor", textStyle.color, defaultStyle?.color)
                    jActor.addColor("textOutlineColor", textStyle.outlineColor, defaultStyle?.outlineColor)
                }
                jActor.add("autoPositionAlignmentX", autoPositionAlignment.x, 0f)
                jActor.add("autoPositionAlignmentY", autoPositionAlignment.y, 0f)
                jActor.add("direction", direction.degrees, 0.0)
                jActor.add("scaleX", scale.x, 1f)
                jActor.add("scaleY", scale.y, 1f)

                actor.resizeAppearance?.let { resizeAppearance ->
                    jActor.add("sizeX", resizeAppearance.size.x, 1f)
                    jActor.add("sizeY", resizeAppearance.size.y, 1f)
                    jActor.add("alignmentX", resizeAppearance.sizeAlignment.x, 0.5f)
                    jActor.add("alignmentY", resizeAppearance.sizeAlignment.y, 0.5f)
                }
            }

            actor.role?.let { role ->
                val roleAttributes = DesignAttributes().apply {
                    updateAttributesMetaData(role)
                }
                JsonUtil.saveAttributes(jActor, roleAttributes)
            }
        }
        return jStage
    }

    private fun JsonObject.addFont(name: String, value: FontResource, defaultValue: FontResource?) {
        if (value != defaultValue) {
            Editor.resources.fontResources.findName(defaultValue)?.let { fontName ->
                add(name, fontName)
            }
        }
    }
}

inline fun <reified T : Enum<*>> JsonObject.addEnum(name: String, value: T?, defaultValue: T) {
    if (value !== defaultValue) {
        add(name, value?.name ?: defaultValue.name)
    }
}

private fun JsonObject.add(name: String, value: TextHAlignment, defaultValue: TextHAlignment?) {
    addEnum(name, value, defaultValue ?: TextHAlignment.CENTER)
}

private fun JsonObject.add(name: String, value: TextVAlignment, defaultValue: TextVAlignment?) {
    addEnum(name, value, defaultValue ?: TextVAlignment.BASELINE)
}

fun JsonObject.addColor(name: String, value: Color?, defaultValue: Color?) {
    if (value != defaultValue) {
        (value ?: defaultValue)?.let {
            add(name, it.toHashRGBA())
        }
    }
}
