/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tasks

import javafx.application.Platform
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.StringParameter
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.misc.RenamableResource

class RenameResourceTask(val resource: RenamableResource, oldName: String)

    : AbstractTask() {

    val newNameP = StringParameter("newName", value = oldName)

    override val taskD = TaskDescription("rename" + (ResourceType.resourceType(resource)?.label ?: ""))
        .addParameters(newNameP)

    override fun run() {
        Platform.runLater {
            resource.rename(Editor.Companion.resources, newNameP.value)
        }
    }

}
