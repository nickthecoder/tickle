/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.control.cell.TextFieldTreeCell
import javafx.scene.input.MouseEvent
import uk.co.nickthecoder.fxessentials.ApplicationAction
import uk.co.nickthecoder.fxessentials.openFile
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.application
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.resources.ResourcesListener
import java.io.File


class FilesDockable(action: ApplicationAction) : TickleDockable(action) {

    val tree = FilesTree()

    override val dockableContent = SimpleObjectProperty<Node>(tree)

}

class FilesTree
    : TreeView<File>(), ResourcesListener {

    init {
        isEditable = false
        isShowRoot = true

        addEventFilter(MouseEvent.MOUSE_RELEASED) { onMousePressed(it) }
        addEventFilter(MouseEvent.MOUSE_PRESSED) { onMousePressed(it) }

        setCellFactory {
            val cell = object : TextFieldTreeCell<File>() {
                override fun updateItem(item: File?, empty: Boolean) {
                    super.updateItem(item, empty)
                    if (empty) {
                        text = null
                    } else {
                        text = item?.name
                    }
                }
            }
            cell
        }

        rebuild()
    }

    private fun onMousePressed(event: MouseEvent) {
        if (event.eventType == MouseEvent.MOUSE_PRESSED && event.clickCount == 2) {
            val item = selectionModel.selectedItem
            if (item != null && item.value.isFile) {
                event.consume()
                application.openFile(item.value)
            }
        }
        if (event.isPopupTrigger) {
            Platform.runLater { // Give JavaFX time to select the item!
                createContextMenu().show(MainWindow.instance.stage, event.screenX, event.screenY)
            }
        }
    }

    private fun rebuild() {
        val dir = Editor.resources.file.parentFile
        root = FileTreeItem(dir).apply {
            isExpanded = true
        }
    }

    fun createContextMenu(): ContextMenu {
        val menu = ContextMenu()
        val item = selectionModel.selectedItem

        with(menu.items) {
            if (item != null) {
                add(MenuItem("Open").apply {
                    onAction = EventHandler {
                        application.openFile(item.value)
                    }
                })
            }
            add(MenuItem("Refresh").apply {
                onAction = EventHandler { rebuild() }
            })
        }

        return menu
    }

    inner class FileTreeItem(file: File) : TreeItem<File>(file) {
        override fun isLeaf() = value.isFile

        init {
            expandedProperty().addListener { _, _, newValue ->
                if (newValue && children.isEmpty()) {
                    value.listFiles { child: File -> !child.isHidden && child.isDirectory }?.sorted()
                        ?.forEach { subDir ->
                            val item = FileTreeItem(subDir)
                            children.add(item)
                        }
                    value.listFiles { child: File -> !child.isHidden && child.isFile }?.sorted()?.forEach { file ->
                        val item = FileTreeItem(file)
                        children.add(item)
                    }
                }
            }
        }

    }

}
