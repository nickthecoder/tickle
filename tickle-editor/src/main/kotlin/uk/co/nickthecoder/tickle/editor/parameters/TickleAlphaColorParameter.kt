/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.parameters

import javafx.util.StringConverter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.util.uncamel
import uk.co.nickthecoder.tickle.editor.util.toJavaFX
import uk.co.nickthecoder.tickle.editor.util.toTickle
import uk.co.nickthecoder.tickle.graphics.Color

class TickleAlphaColorParameter(
        name: String,
        label: String = name.uncamel(),
        value: Color = Color(),
        description: String = "")

    : CompoundParameter<Color>(
        name, label, description) {

    val fxColorP = AlphaColorParameter("${name}_fx")
    var fxColor by fxColorP

    override val converter = object : StringConverter<Color>() {
        override fun fromString(string: String): Color {
            return Color.create(string)
        }

        override fun toString(obj: Color): String {
            return obj.toHashRGBA()
        }
    }

    /**
     * Protects [value]'s setter from infinite recursion.
     * When a value is changed, it causes the color and opacity parameter values to be changed.
     * Their listeners then attempt to reflect these new values by setting the value again!
     * This is a nasty hack to avoid the problem, and as all the Paratask code is going to be
     * re-written at some point, I have chosen not to find a more elegant solution.
     */
    private var changing = false

    override var value: Color
        get() {
            return fxColor.toTickle()
        }
        set(value) {
            if (!changing) {
                changing = true
                try {
                    fxColor = value.toJavaFX()
                } finally {
                    changing = false
                }
            }
        }


    init {
        this.value = value

        addParameters(fxColorP)
        asHorizontal(LabelPosition.NONE)
    }

    override fun toString(): String {
        return "TickleAlphaColorParameter : $value"
    }

    override fun copy(): TickleAlphaColorParameter {
        val copy = TickleAlphaColorParameter(name, label, value, description)
        return copy
    }
}
