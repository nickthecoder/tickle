/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor

import javafx.application.Platform
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.ButtonBar.ButtonData
import javafx.scene.input.TransferMode
import javafx.scene.layout.BorderPane
import javafx.stage.FileChooser
import javafx.stage.Stage
import javafx.stage.WindowEvent
import uk.co.nickthecoder.fxessentials.ActionGroup
import uk.co.nickthecoder.fxessentials.contentVisibleWhenSelected
import uk.co.nickthecoder.harbourfx.Dockable
import uk.co.nickthecoder.harbourfx.DockableFactory
import uk.co.nickthecoder.harbourfx.Harbour
import uk.co.nickthecoder.paratask.ParaTask
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.dockable.*
import uk.co.nickthecoder.tickle.editor.resources.*
import uk.co.nickthecoder.tickle.editor.scene.EditorPreferencesGUI
import uk.co.nickthecoder.tickle.editor.tabs.*
import uk.co.nickthecoder.tickle.editor.tasks.NewResourceTask
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences
import uk.co.nickthecoder.tickle.editor.util.ExceptionAlert
import uk.co.nickthecoder.tickle.editor.util.ImageCache
import uk.co.nickthecoder.tickle.editor.util.expandH
import uk.co.nickthecoder.tickle.events.CompoundInput
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.Layout
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.scripts.ScriptException
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.sound.Sound
import java.io.File
import java.io.PrintStream

/**
 * The controller for the main JavaFX [Scene] for the [Editor].
 * This class is NOT a [Node] or subclass of any other JavaFX component.
 *
 * We have a [borderPane], whose top is [tabPane], and whose center is [harbour].
 * A [Harbour] is a place where [Dockable] items can live around the edges
 * (from the HarbourFX library).
 *
 * The center of the [Harbour] is the [tabPane].
 *
 * NOTE, I don't have a menu-bar (as I tend to use toolbars and shortcuts).
 *
 * Shortcuts are managed via the `fxssentials` library attached to the [borderPane].
 * So these shortcuts are accessible no matter where the `input focus` is.
 *
 * BEFORE [MainWindow] is created, [OpenGL] is initialised, and we keep a reference
 * to a [glWindow], which is hidden, until [startGame]. At which point, the window is
 * displayed, and hidden again, when the game ends. We reuse the same GL [Window]
 * the next time we [startGame].
 */
class MainWindow(val stage: Stage, val glWindow: Window)

    : DockableFactory, TickleErrorHandler, GameErrorHandler {

    /**
     * Note, this is NOT the center of
     */
    val borderPane = BorderPane()

    val toolBar = ToolBar()

    /**
     * The [BorderPane.center] node.
     */
    val tabPane = TabPane()

    // Each tab's content's isVisible property is only true when the tab is selected.
    // This allows SceneEditor to use fxessential's autoAttach method.
    // NOTE, we need to keep a reference to this listener, to prevent it from being garbage collected.
    val tabPaneListener = tabPane.contentVisibleWhenSelected()

    /**
     * Holds all the dockable panels, such as the ResourcesTrees.
     */
    val harbour = Harbour(tabPane, this)

    /**
     * The JavaFX Scene for the MainWindow. NOTHING to do with Tickle's Scene class!
     */
    val scene = Scene(borderPane)

    /**
     * Some tabs can add additional buttons on the right hand side of the toolbar.
     * We store them in a list, so that we can easily remove them when we switch to a different tab.
     */
    private var extraButtons: Collection<Node> = emptyList()


    /**
     * Unlike all the other dockables, we keep a reference to this one, so that we can log errors to it.
     * I made MainWindow the [TickleErrorHandler], rather than the [LogDockable], because then we can also
     * highlight compilation errors, by changing to the tab with the incorrect script.
     */
    val log = LogDockable()

    private val actionGroup = ActionGroup().apply {
        action(SAVE) { saveAllTabs() }
        action(RELOAD) { reload() }
        action(RELOAD_SCRIPTS) { reloadScripts() }
        action(NEW) { newResource() }
        action(QUICK_OPEN_ANY) { QuickOpen(stage, ResourceType.ANY) }
        action(QUICK_OPEN_SCRIPT) { QuickOpen(stage, ResourceType.SCRIPT) }
        action(QUICK_OPEN_COSTUME) { QuickOpen(stage, ResourceType.COSTUME) }
        action(QUICK_OPEN_POSE) { QuickOpen(stage, ResourceType.POSE) }
        action(QUICK_OPEN_TEXTURE) { QuickOpen(stage, ResourceType.TEXTURE) }
        action(QUICK_OPEN_SOUND) { QuickOpen(stage, ResourceType.SOUND) }
        action(QUICK_OPEN_API) { QuickOpen(stage, ResourceType.API_DOCUMENTATION) }
        action(QUICK_OPEN_SCENE) { QuickOpen(stage, ResourceType.SCENE) }
        action(IMPORT) { importResource() }
        action(RUN) { startGame() }
        action(TEST) { startCurrentScene() }
        // There used to be 2 different ways run a "test" scene - either from GameInfo.testScenePath,
        // or the current tab (if it is a SceneTab). These have been merged into one!
        //action(TEST) { startTestScene() }
        //action(TEST_CURRENT) { startCurrentScene() }
        action(DOCKABLE_MENU) {} // This for a [MenuButton], and has not action.
        action(EDIT_PREFERENCES) { onEditPreferences() }

        action(TAB_CLOSE) { onCloseTab() }

        for (ea in dockables) {
            action(ea) {
                harbour.toggle(ea.name)
            }
        }
        attachTo(borderPane)

    }

    private val toolbarSpacer = expandH()

    init {
        stage.title = "Tickle Resources Editor"
        stage.scene = scene
        ParaTask.style(scene)
        Scarea.style(scene)

        val resource = MainWindow::class.java.getResource("tickle.css")
        scene.stylesheets.add(resource!!.toExternalForm())
        scene.stylesheets.add(Harbour.cssUrl)

        with(borderPane) {
            top = toolBar
            center = harbour
        }

        with(actionGroup) {
            toolBar.items.addAll(
                labelledButton(SAVE),
                labelledButton(RELOAD),
                labelledButton(NEW),
                labelledSplitMenuButton(QUICK_OPEN_ANY).apply {
                    items.addAll(
                        menuItem(QUICK_OPEN_TEXTURE),
                        menuItem(QUICK_OPEN_POSE),
                        menuItem(QUICK_OPEN_COSTUME),
                        menuItem(QUICK_OPEN_SOUND),
                        menuItem(QUICK_OPEN_TEXTURE),
                        menuItem(QUICK_OPEN_SCRIPT),
                        menuItem(QUICK_OPEN_API)
                    )
                },
                labelledButton(IMPORT),
                labelledButton(RUN),
                labelledButton(TEST),

                toolbarSpacer,

                Separator(),
                menuButton(DOCKABLE_MENU).apply {
                    for (ea in dockables) {
                        items.add(menuItem(ea))
                    }
                },
                button(EDIT_PREFERENCES)
            )
        }


        tabPane.selectionModel.selectedItemProperty().addListener { _, _, newValue ->
            onTabChanged(newValue)
        }
        tabPane.tabs.addListener(ListChangeListener { c ->
            while (c.next()) {
                if (c.wasRemoved()) {
                    c.removed.filterIsInstance<EditorTab>().forEach { it.removed() }
                }
            }
        })

        instance = this
        stage.onCloseRequest = EventHandler { onCloseRequest(it) }

        // Allow drag and drop of files to import as a Texture, Font or Sound resources.
        borderPane.onDragOver = EventHandler { event ->
            if (event.dragboard.hasFiles()) {
                event.acceptTransferModes(*TransferMode.COPY_OR_MOVE)
            }
            event.consume()
        }
        borderPane.onDragDropped = EventHandler { event ->
            val db = event.dragboard
            var fileIgnored = false
            if (db.hasFiles()) {
                db.files.forEach { file ->
                    if (!importFile(file)) fileIgnored = true
                }
            }
            event.isDropCompleted = !fileIgnored
            event.consume()
        }

    }

    fun resourcesLoaded() {
        EditorPreferences.load()
        stage.width = EditorPreferences.windowWidth
        stage.height = EditorPreferences.windowHeight

        if (EditorPreferences.isMaximized) {
            stage.isMaximized = true
        }
        stage.show()

        if (EditorPreferences.captureStdOut) {
            info("output is now being set to the Log (no longer on the console)")
            System.setOut(PrintStream(log.out))
        }
        if (EditorPreferences.captureStdErr) {
            info("error output is now being set to the Log (no longer on the console)")
            System.setErr(PrintStream(log.out))
        }

        // Any messages that were sent to the error handler, and now shown in the log.
        val eh = TickleErrorHandler.instance
        if (eh is DelayedErrorHandler) {
            for (e in eh.exceptions) {
                log.out.println(e)
            }
            for (m in eh.messages) {
                log.out.println(m)
            }
        }
        TickleErrorHandler.instance = this
        GameErrorHandler.instance = this

        if (ScriptManager.languages().isNotEmpty()) {
            toolBar.items.add(toolBar.items.indexOf(toolbarSpacer), actionGroup.button(RELOAD_SCRIPTS))
        }

    }

    private fun onCloseTab() {
        val selectedTab = tabPane.selectionModel.selectedItem
        if (selectedTab is EditTab) {

            if (selectedTab.needsSaving) {
                val alert = Alert(Alert.AlertType.CONFIRMATION)
                with(alert) {
                    headerText = "Close tab without saving?"
                    showAndWait()
                    if (result == ButtonType.OK) {
                        selectedTab.close()
                    }
                }
                return
            }
        }
        (selectedTab as? EditTab)?.close()
    }

    private fun onCloseRequest(event: WindowEvent) {
        if (isRunning) {
            Game.instance.runLater { Game.instance.quit() }

            event.consume() // Ignore the close request!
            // If we close it AGAIN, then [isRunning] should be false (now that the game has ended),
            // and we WILL be able to close the window.
            return
        }

        // Check if there are tabs open, and if so, ask if they should be saved.
        if (tabPane.tabs.filterIsInstance<EditTab>().any { it.needsSaving }) {
            val save = ButtonType("Save")
            val ignore = ButtonType("Ignore")
            val cancel = ButtonType("Cancel", ButtonData.CANCEL_CLOSE)

            val alert = Alert(Alert.AlertType.CONFIRMATION).apply {
                title = "Save Changes?"
                contentText = "Save changes before closing?"

                buttonTypes.setAll(save, ignore, cancel)
            }

            when (alert.showAndWait().get()) {
                save -> {
                    // Note. this is inefficient, as the resources are saved to disk for every opened tab.
                    // It is quick though, so I haven't bothered to make it save only once.
                    tabPane.tabs.forEach { tab ->
                        if (tab is EditTab) {
                            if (!tab.save()) {
                                tabPane.selectionModel.selectedItem === tab
                                // Tab not saved, so abort the closing of the main window.
                                event.consume()
                                return
                            }
                        }
                    }
                }

                cancel -> {
                    event.consume()
                    return
                }
            }
        }

        with(EditorPreferences) {
            isMaximized = stage.isMaximized
            if (!isMaximized) {
                windowWidth = stage.width
                windowHeight = stage.height
            }
        }
        Editor.resources.save()
        EditorPreferences.save()
    }

    fun findTab(data: Any): EditorTab? {
        return tabPane.tabs.filterIsInstance<EditorTab>().firstOrNull { it.data == data }
    }

    private fun reloadScripts() {
        Runtime.getRuntime().gc()
        /*
        info(
            "Free Memory ${Runtime.getRuntime().freeMemory() / 1000000}MB. Total : ${
                Runtime.getRuntime().totalMemory() / 1000000
            }MB   Max ${Runtime.getRuntime().maxMemory() / 1000000}MB"
        )
        */
        ScriptManager.reload()
        Runtime.getRuntime().gc()
        /*
        info(
            "Free Memory ${Runtime.getRuntime().freeMemory() / 1000000}MB. Total : ${
                Runtime.getRuntime().totalMemory() / 1000000
            }MB   Max ${Runtime.getRuntime().maxMemory() / 1000000}MB"
        )
        */
    }

    private fun reload() {
        if (!Editor.resources.loadFailed) {

            val backup = File(Editor.resources.file.path + "~")
            if (backup.exists()) backup.delete()

            Editor.resources.file.copyTo(backup)

            tabPane.tabs.forEach { tab ->
                if (tab is ScriptTab) {
                    tab.saveCode()
                } else if (tab is EditTab) {
                    if (tab.needsSaving) {
                        tab.save()
                    }
                }
            }
        }

        tabPane.tabs.clear()
        onOpenGLThread {
            Editor.resources.destroy()
            ImageCache.clear()
            ScriptManager.reload()

            try {
                ResourcesReaderWriter.load(Editor.resources, Editor.resources.file)
                Editor.resources.loadFailed = false
            } catch (e: Exception) {
                e.printStackTrace()
                Platform.runLater {
                    Editor.resources.loadFailed = true

                    ExceptionAlert(e).apply {
                        title = "Load Failed"
                        headerText = "Load Failed"
                        contentText = "Correct the problem, and then click Reload."
                        show()
                    }
                }
            }
        }
        EditorPreferences.load(false)

        Editor.resources.listeners.toList().forEach {
            it.resourcesReloaded()
        }
    }

    private fun newResource() {
        TaskPrompter(NewResourceTask()).placeOnStage(Stage())
    }

    private fun importResource() {
        val file = FileChooser().apply {
            title = "Import Texture/Sound/Font"
            initialDirectory = Editor.resources.resourceDirectory
            extensionFilters.addAll(
                FileChooser.ExtensionFilter(
                    "Texture/Font/Sound",
                    listOf("*.png", "*.ttf", "*.otf", "*.wav", "*.ogg", "*.mp3")
                ),
                FileChooser.ExtensionFilter("Texture", listOf("*.png")),
                FileChooser.ExtensionFilter("Font", listOf("*.ttf", "*.otf")),
                FileChooser.ExtensionFilter("Sound", listOf("*.wav", "*.ogg", "*.mp3")),
                FileChooser.ExtensionFilter("All Files", listOf("*"))

            )
        }.showOpenDialog(stage)
        if (file != null) {
            importFile(file)
        }
    }

    private fun importFile(file: File): Boolean {
        val resourceType = when (file.extension) {
            "png", "jpg", "jpeg" -> ResourceType.TEXTURE
            "ttf", "otf" -> ResourceType.FONT
            "wav", "ogg", "mp3" -> ResourceType.SOUND
            else -> null
        }
        val resources = Editor.resources
        var copiedFile: File = file
        if (!file.canonicalPath.startsWith(resources.resourceDirectory.canonicalPath)) {
            // The file is NOT in the game's directory, so we need to copy it
            val destDir = when (resourceType) {
                ResourceType.TEXTURE -> resources.texturesDirectory
                ResourceType.FONT -> resources.fontDirectory
                ResourceType.SOUND -> resources.soundsDirectory
                else -> null
            }
            if (destDir != null) {
                if (!destDir.exists()) {
                    destDir.mkdirs()
                }
                copiedFile = File(destDir, file.name)
                file.copyTo(copiedFile)
            }
        }
        if (resourceType != null) {
            TaskPrompter(
                NewResourceTask(resourceType).apply {
                    nameP.value = copiedFile.nameWithoutExtension
                    textureP.value = copiedFile
                    fontP.fontFileP.value = copiedFile
                    soundP.value = copiedFile
                }
            ).placeOnStage(Stage())
        }
        return resourceType != null
    }

    private fun onEditPreferences() {
        val sceneTab = tabPane.selectionModel.selectedItem as? SceneTab
        EditorPreferencesGUI(sceneTab?.sceneEditor).show()
    }

    /**
     * Update all opened [SceneTab]s when preferences are changed.
     */
    internal fun firePreferencesChanged() {
        for (tab in tabPane.tabs) {
            if (tab is SceneTab) {
                tab.sceneEditor.firePreferencesChanged()
            }
        }
    }


    private fun saveAllTabs() {
        if (isRunning) {
            Alert(Alert.AlertType.WARNING, "Cannot save resources while the game is running").apply {
                title = "Cannot Save"
                show()
            }
            return
        }
        EditorPreferences.save()
        // Using toList, because I was getting co-modification exceptions (maybe when the tabs text changed?)
        tabPane.tabs.toList().forEach { tab ->
            (tab as? EditTab)?.save()
            if (tab is ScriptTab) {
                tab.hideError()
            }
        }

        ScriptManager.reload()
    }

    /**
     * Ensures we do not try to run two copies of the game at the same time!
     */
    private var isRunning = false

    /**
     * Runs the game.
     *
     * The game runs on the OpenGL thread, while the JavaFX GUI remains running on the JavaFX Thread.
     *
     * Whenever we run the game, we reload the resources.
     */
    private fun startGame(scenePath: String = Editor.resources.sceneFileToPath(Editor.resources.gameInfo.initialScenePath)) {
        // Don't allow two games to be run at once!
        if (isRunning) {
            warn("Game is already running")
            return
        }
        // Calls to deprecated methods will issue warnings again.
        deprecatedMessages.clear()

        gameErrorCount = 0
        log.clear()

        saveAllTabs()
        ScriptManager.reload()

        OpenGL.runLater {

            val resourcesCopy = Resources()

            try {
                isRunning = true
                // The easiest way to duplicate the resources safely is to re-load them.
                // Not ideal, as it is slower than it should be.
                resourcesCopy.load(Editor.resources.file)

                with(resourcesCopy.gameInfo) {
                    glWindow.change(title, width, height, resizable)
                }
                glWindow.show()


                val game = Game(glWindow, resourcesCopy)
                try {
                    game.pollEvents = false
                    glWindow.makeContextCurrent()
                    game.run(scenePath)
                } finally {
                    isRunning = false
                    glWindow.hide()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                resourcesCopy.destroy()
            }
            isRunning = false
        }

    }

    /**
     * Runs the test scene specified in [GameInfo.testScenePath]
     */
    private fun startTestScene() {
        info("Run Test Scene (GameInfo.testScenePath = ${Editor.resources.gameInfo.testScenePath})")
        testSceneFile = Editor.resources.gameInfo.testScenePath
        startGame(Editor.resources.sceneFileToPath(testSceneFile ?: File("test")))
    }

    /**
     * If the current tab is a Scene, then run that scene (and remember it for next time).
     * Otherwise, we either use the previously remembered scene, or [GameInfo.testScenePath]
     */
    private fun startCurrentScene() {
        val tab = tabPane.selectionModel.selectedItem
        if (tab is SceneTab) {
            info("Test current scene")
            testSceneFile = tab.sceneFile
        } else if (testSceneFile == null) {
            testSceneFile = Editor.resources.gameInfo.testScenePath
            info("Test scene ${testSceneFile}")
        }
        val sceneName = Editor.resources.sceneFileToPath(testSceneFile ?: File("test"))
        startGame(sceneName)
    }

    private var testSceneFile: File? = null


    fun openTab(tab: EditorTab) {
        tabPane.tabs.add(tab)
        tabPane.selectionModel.select(tab)
    }

    fun openTab(dataName: String, data: Any): Tab? {

        val tab = findTab(data)
        return if (tab == null) {
            val newTab = createTab(dataName, data)
            newTab?.let { openTab(it) }
            newTab
        } else {
            tabPane.selectionModel.select(tab)
            tab
        }

    }

    /**
     * Returns false if the tab exists, but couldn't be saved. Else true.
     */
    fun closeTab(data: Any): Boolean {
        val tab = findTab(data) ?: return true
        if (tab is EditTab) {
            if (!tab.save()) return false
        }
        tabPane.tabs.remove(tab)
        return true
    }

    fun openNamedTab(dataName: String, type: ResourceType): Tab? {
        val data: Any = type.findResource(dataName) ?: return null
        return openTab(dataName, data)
    }

    private fun createTab(name: String, data: Any): EditorTab? {

        return when (data) {
            is GameInfo -> return GameInfoTab()

            is Texture -> TextureTab(name, data)
            is Pose -> PoseTab(name, data)
            is Layout -> LayoutTab(name, data)
            is CompoundInput -> InputTab(name, data)
            is Costume -> CostumeTab(name, data)
            is CostumeGroup -> CostumeGroupTab(name, data)
            is SceneStub -> SceneTab(name, data)
            is FontResource -> FontTab(name, data)
            is Sound -> SoundTab(name, data)
            is ScriptStub -> ScriptTab(data)
            is MacroStub -> MacroTab(data)
            is Class<*> -> APITab(data.packageName, data.simpleName)
            else -> null
        }
    }

    private fun onTabChanged(tab: Tab?) {
        extraButtons.forEach {
            toolBar.items.remove(it)
        }

        if (tab is HasExtras) {
            extraButtons = tab.extraButtons()
            toolBar.items.addAll(toolBar.items.indexOf(toolbarSpacer) + 1, extraButtons)
        }

        if (tab is EditTab) {
            tab.focus()
        }
    }

    override fun error(e: Exception) {
        log.out.println(e)
        if (e is ScriptException) {
            e.file?.let { file ->
                if (file.exists()) {
                    val tab = openTab(file.nameWithoutExtension, ScriptStub(file))
                    if (tab is ScriptTab) { // It should be!
                        tab.highlightError(e)
                        return
                    }
                } else {
                    info("ScriptError in file '$file' line ${e.line}: File not found")
                }
            }
        }
    }


    /**
     * Count the number of errors, and stop reporting after 10
     */
    private var gameErrorCount = 0
    override fun error(owner: Any?, e: Exception) {
        gameErrorCount++
        if (gameErrorCount == 10) {
            info("Too many errors. No more will be reported")
        }
        if (gameErrorCount < 10) {
            e.printStackTrace()
            try {
                if (owner != null) {
                    System.err.println("Exception thrown by : ${owner}.")
                }
                if (owner is Role) {
                    owner.actor.stage?.remove(owner.actor)
                }
            } catch (e2: Exception) {
                // Just in case. "remove" could fail, and so could println( $owner )
                System.err.println("Error handler failed : $e2")
            }
        }
    }

    override fun info(message: String) {
        log.out.println("INFO : $message")
    }

    override fun warn(message: String) {
        log.out.println("WARNING : $message")
    }

    /**
     * Only issue a warning ONCE for each deprecated warning message.
     * This holds list message that have already been reported.
     */
    private val deprecatedMessages = mutableSetOf<String>()

    /**
     * Issue a warning ONCE for each deprecated warning message.
     * Running the game again, will give the warning again.
     * This lets the game developer update their code without spamming too many warnings.
     */
    override fun deprecated(message: String, replacement: String) {
        if (!deprecatedMessages.contains(message)) {
            deprecatedMessages.add(message)
            System.err.println("DEPRECATED : $message Replacement : $replacement")
            System.err.println("You will only see this message ONCE (until you run the game again).")
            Thread.dumpStack()
        }
    }

    override fun error(message: String) {
        log.out.println("ERROR : $message")
    }

    override fun createDockable(dockableId: String): Dockable? {

        return when (dockableId) {
            DOCKABLE_RESOURCES.name -> ResourcesDockable(DOCKABLE_RESOURCES, ResourceType.ANY)
            DOCKABLE_ACTOR_ATTRIBUTES.name -> ActorAttributesDockable()
            DOCKABLE_COSTUME_PICKER.name -> CostumePicker()
            DOCKABLE_STAGES.name -> StagesDockable()
            DOCKABLE_ACTORS.name -> ActorsDockable()
            DOCKABLE_API.name -> APIDockable()
            DOCKABLE_SNIPPETS.name -> SnippetsDockable()

            DOCKABLE_TEXTURES.name -> ResourcesDockable(DOCKABLE_TEXTURES, ResourceType.TEXTURE)
            DOCKABLE_POSES.name -> ResourcesDockable(DOCKABLE_POSES, ResourceType.POSE)
            DOCKABLE_COSTUMES.name -> ResourcesDockable(DOCKABLE_COSTUMES, ResourceType.COSTUME)
            DOCKABLE_FONTS.name -> ResourcesDockable(DOCKABLE_FONTS, ResourceType.FONT)
            DOCKABLE_INPUTS.name -> ResourcesDockable(DOCKABLE_INPUTS, ResourceType.INPUT)
            DOCKABLE_SOUNDS.name -> ResourcesDockable(DOCKABLE_SOUNDS, ResourceType.SOUND)
            DOCKABLE_SCRIPTS.name -> ResourcesDockable(DOCKABLE_SCRIPTS, ResourceType.SCRIPT)
            DOCKABLE_SCENES.name -> ResourcesDockable(DOCKABLE_SCENES, ResourceType.SCENE)

            DOCKABLE_LOG.name -> log
            DOCKABLE_FILES.name -> FilesDockable(DOCKABLE_FILES)

            else -> null
        }
    }

    companion object {

        lateinit var instance: MainWindow

    }

}
