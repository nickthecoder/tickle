package uk.co.nickthecoder.tickle.editor.dockable

import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.control.TextArea
import uk.co.nickthecoder.tickle.editor.DOCKABLE_LOG
import java.io.OutputStream
import java.io.PrintStream


class LogDockable
    : TickleDockable(DOCKABLE_LOG) {

    private val textArea = TextArea()

    override val dockableContent = SimpleObjectProperty<Node>(textArea)

    private var buffer = StringBuffer()
    private var adding = false

    override fun settingsMenu(): ContextMenu? {
        return ContextMenu().apply {
            items.add(MenuItem("Clear").apply {
                onAction = EventHandler { textArea.clear() }
            })
        }
    }

    var os: OutputStream = object : OutputStream() {
        override fun write(b: Int) {
            addText(b.toChar().toString())
        }

        override fun write(b: ByteArray, off: Int, len: Int) {
            addText(String(b, off, len))
        }
    }

    val out: PrintStream = PrintStream(os)

    init {
        System.out
        textArea.isWrapText = false
        textArea.isEditable = false
    }

    private fun addText(text: String) {
        synchronized(this) {
            buffer.append(text)
            if (!adding) {
                adding = true
                Platform.runLater {
                    synchronized(this) {
                        textArea.appendText(buffer.toString())
                        buffer = StringBuffer()
                        adding = false
                    }
                }
            }
        }
    }

    fun clear() {
        textArea.clear()
    }

}
