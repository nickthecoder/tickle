/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.stage.Handle
import uk.co.nickthecoder.tickle.util.Vector2


class SelectedActor(val actor: Actor, val editingStage: EditingStage) {
    var oldX = actor.x
    var oldY = actor.y
    var drag = Vector2()
    /**
     * A transformation matrix, so that the bounding box of selected actors can be drawn
     * as if (0,0) is the actor's position, and without caring about rotations.
     */
    val modelMatrix = Matrix3x2f()
    var handles: MutableList<Handle>? = null

    init {
        updateMatrix()
    }

    fun updateMatrix() {
        modelMatrix.identity()
        modelMatrix.translate(actor.x, actor.y)
        modelMatrix.rotate(actor.direction.radians.toFloat())
    }

}

class Selection(val sceneView: SceneView) {

    private val privateItems = mutableSetOf<SelectedActor>()

    val items: Set<SelectedActor>
        get() = privateItems

    val actors: List<Actor>
        get() = privateItems.map { it.actor }

    private var latest: SelectedActor? = null

    var highlightHandle: Handle? = null
        set(v) {
            v?.actor?.let { actor ->
                for (sa in items) {
                    if (sa.actor === actor) {
                        latest = sa
                        fireChange()
                        break
                    }
                }
            }
            field = v

        }
    var highlightXGuide: Float? = null
    var highlightYGuide: Float? = null

    val listeners = mutableListOf<SelectionListener>()
    val size
        get() = privateItems.size

    fun isEmpty() = privateItems.isEmpty()

    fun isNotEmpty() = privateItems.isNotEmpty()

    fun contains(actor: Actor) = privateItems.any { it.actor === actor }

    fun clear() {
        privateItems.clear()
        latest = null
        highlightHandle = null
        fireChange()
    }

    fun add(selectedActor: SelectedActor) {
        privateItems.add(selectedActor)
        latest = selectedActor
        fireChange()
    }

    fun remove(actor: Actor?) {
        actor ?: return
        val sa = privateItems.firstOrNull { it.actor === actor } ?: return
        privateItems.remove(sa)
        if (sa === latest) {
            latest = null
        }
        fireChange()
    }

    fun replace(sas: List<SelectedActor>) {
        privateItems.clear()
        privateItems.addAll(sas)
        latest = sas.lastOrNull()
        fireChange()
    }

    fun actors() = privateItems.map { it.actor }

    fun latest(): SelectedActor? = latest

    fun findHandle(world: Vector2, slop: Float): Handle? {
        for (sa in privateItems) {
            sa.handles?.let { handles ->
                for (handle in handles) {
                    val dx = world.x - handle.x()
                    if (dx > -slop && dx < slop) {
                        val dy = world.y - handle.y()
                        if (dy > -slop && dy < slop) {
                            return handle
                        }
                    }
                }
            }
        }
        return null
    }

    fun clearAndSelect(actor: Actor) {
        sceneView.sceneEditor.selectedActor(actor)?.let {
            clearAndSelect(it)
        }
    }

    fun clearAndSelect(selectedActor: SelectedActor) {
        clear()
        add(selectedActor)
        fireChange()
    }

    fun fireChange() {
        listeners.forEach { it.selectionChanged() }
    }

}

interface SelectionListener {

    fun selectionChanged()

}
