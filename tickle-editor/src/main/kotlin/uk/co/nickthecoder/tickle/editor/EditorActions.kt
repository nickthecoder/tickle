/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor

import javafx.scene.input.KeyCode
import uk.co.nickthecoder.fxessentials.AllActions

val actions = AllActions(Editor::class.java, "")

val NEW = actions.create("new", "New", KeyCode.N, control = true, tooltip = "Create a New Resource")
val RUN = actions.create("run", "Run", KeyCode.R, control = true, tooltip = "Run the game")
val TEST = actions.create("test", "Test", KeyCode.T, control = true, shift = false, tooltip = "Run the Test Scene")

val SAVE = actions.create("document-save", "Save", KeyCode.S, control = true, shift = false)
val RELOAD =
    actions.create("reload", "Reload", KeyCode.F5, control = false, tooltip = "Reload textures, fonts & sounds")
val RELOAD_SCRIPTS =
    actions.create("scripts.reload", "Reload Scripts", KeyCode.F5, shift = true, tooltip = "Reload all scripts")
// Note, Ctrl+F5 is hard-coded into Toolbar to focus on the first item, so I don't want to use that.

val QUICK_OPEN_SCRIPT = actions.create("quickOpenScript", "Quick Open Script", KeyCode.O, control = true, shift = false)
val QUICK_OPEN_ANY = actions.create("quickOpenAny", "Quick Open", KeyCode.O, control = true, shift = true)
val QUICK_OPEN_COSTUME =
    actions.create("quickOpenCostume", "Quick Open Costume", KeyCode.C, control = true, shift = true)
val QUICK_OPEN_SCENE =
    actions.create("quickOpenScene", "Quick Open Scene", KeyCode.S, control = true, shift = true)
val QUICK_OPEN_POSE =
    actions.create("quickOpenPose", "Quick Open Pose", KeyCode.P, control = true, shift = true)
val QUICK_OPEN_TEXTURE =
    actions.create("quickOpenTexture", "Quick Open Texture", KeyCode.T, control = true, shift = true)
val QUICK_OPEN_SOUND =
    actions.create("quickOpenSound", "Quick Open Sound", KeyCode.A, control = true, shift = true)
val QUICK_OPEN_API =
    actions.create("quickOpenAPI", "Quick Open API", KeyCode.J, control = true, shift = true)

val IMPORT = actions.create("import", "Import", KeyCode.I, control = true)

val ESCAPE = actions.create("escape", "Escape", KeyCode.ESCAPE)
val DELETE = actions.create("delete", "Delete", KeyCode.DELETE)

val CUT = actions.create("cut", "Cut", KeyCode.X, control = true, shift = false)
val COPY = actions.create("copy", "Copy", KeyCode.C, control = true, shift = false)
val PASTE = actions.create("paste", "Paste", KeyCode.V, control = true, shift = false)
val DUPLICATE = actions.create("duplicate", "Duplicate", KeyCode.D, control = true, shift = false)

val UNDO = actions.create("undo", "Undo", KeyCode.Z, control = true)
val REDO = actions.create("redo", "Redo", KeyCode.Z, control = true, shift = true)

val ZOOM_RESET = actions.create("zoom.reset", "Reset Zoom", KeyCode.DIGIT0)
val ZOOM_IN1 = actions.create("zoom.in", "Zoom In", KeyCode.PLUS)
val ZOOM_IN2 = actions.create("zoom.in2", "Zoom In", KeyCode.EQUALS)
val ZOOM_OUT = actions.create("zoom.out", "Zoom Out", KeyCode.MINUS)

val EDIT_PREFERENCES =
    actions.create("preferences", "Preferences", KeyCode.NUMBER_SIGN, control = true, shift = true)

val SNAP_TO_GRID_TOGGLE = actions.create("snap.grid.toggle", "Snap to Grid", KeyCode.NUMBER_SIGN, control = true)
val SNAP_TO_GUIDES_TOGGLE = actions.create("snap.guides.toggle", "Snap to Guides", KeyCode.G, control = true)
val SNAP_TO_OTHERS_TOGGLE = actions.create("snap.others.toggle", "Snap to Others", KeyCode.O, control = true)
val SNAP_ROTATION_TOGGLE = actions.create("snap.rotation.toggle", "Snap to Rotation", KeyCode.R, control = true)

val RESET_ZORDERS = actions.create("zOrders.reset", "Reset All Z-Orders", KeyCode.Z, control = true, shift = true)

val STAMPS = listOf(
    KeyCode.DIGIT1,
    KeyCode.DIGIT2,
    KeyCode.DIGIT3,
    KeyCode.DIGIT4,
    KeyCode.DIGIT5,
    KeyCode.DIGIT6,
    KeyCode.DIGIT7,
    KeyCode.DIGIT8,
    KeyCode.DIGIT9
)
    .mapIndexed { index, keyCode -> actions.create("stamp$index", "Stamp #$index", keyCode, control = true) }

val TAB_CLOSE = actions.create("tab.close", "Close Tab", KeyCode.W, control = true)

val DOCKABLE_RESOURCES = actions.create("dockable.resources", "Resources", KeyCode.DIGIT1, alt = true)
val DOCKABLE_COSTUME_PICKER = actions.create("dockable.costume.picker", "Costume Picker", KeyCode.DIGIT2, alt = true)
val DOCKABLE_ACTOR_ATTRIBUTES = actions.create("dockable.actor.attributes", "Attributes", KeyCode.DIGIT3, alt = true)
val DOCKABLE_STAGES = actions.create("dockable.stages", "Stages", KeyCode.DIGIT4, alt = true)
val DOCKABLE_ACTORS = actions.create("dockable.actors", "Actors", KeyCode.DIGIT5, alt = true)
val DOCKABLE_API = actions.create("dockable.api", "API", KeyCode.DIGIT6, alt = true)
val DOCKABLE_SNIPPETS = actions.create("dockable.snippets", "Snippets", KeyCode.DIGIT7, alt = true)

val DOCKABLE_TEXTURES = actions.create("dockable.textures", "Textures", KeyCode.T, alt = true)
val DOCKABLE_POSES = actions.create("dockable.poses", "Poses", KeyCode.P, alt = true)
val DOCKABLE_COSTUMES = actions.create("dockable.costumes", "Costumes", KeyCode.C, alt = true)
val DOCKABLE_INPUTS = actions.create("dockable.inputs", "Inputs", KeyCode.I, alt = true)
val DOCKABLE_FONTS = actions.create("dockable.fonts", "Fonts", KeyCode.F, alt = true)
val DOCKABLE_SOUNDS = actions.create("dockable.sounds", "Sounds", KeyCode.M, alt = true)
val DOCKABLE_SCRIPTS = actions.create("dockable.scripts", "Scripts", KeyCode.OPEN_BRACKET, alt = true)
val DOCKABLE_SCENES = actions.create("dockable.scenes", "Scenes", KeyCode.S, alt = true)

val DOCKABLE_FILES = actions.create("dockable.files", "Files", KeyCode.F, alt = true)
val DOCKABLE_LOG = actions.create("dockable.log", "Log", KeyCode.DIGIT0, alt = true)

val DOCKABLE_MENU = actions.create("dockable.menu", "⛴", null)
val dockables = listOf(
    DOCKABLE_RESOURCES, DOCKABLE_COSTUME_PICKER, DOCKABLE_ACTOR_ATTRIBUTES, DOCKABLE_STAGES, DOCKABLE_ACTORS,
    DOCKABLE_API, DOCKABLE_SNIPPETS, DOCKABLE_TEXTURES, DOCKABLE_POSES, DOCKABLE_COSTUMES, DOCKABLE_FONTS,
    DOCKABLE_SOUNDS, DOCKABLE_INPUTS, DOCKABLE_SCENES, DOCKABLE_SCRIPTS, DOCKABLE_LOG, DOCKABLE_FILES
)

val WEBVIEW_BACK = actions.create("api.back", "<", KeyCode.LEFT, alt = true)
val WEBVIEW_FORWORD = actions.create("api.forward", ">", KeyCode.RIGHT, alt = true)

