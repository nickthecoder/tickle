/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.beans.property.SimpleObjectProperty
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.StackPane
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.DOCKABLE_ACTOR_ATTRIBUTES
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.scene.SelectionListener
import uk.co.nickthecoder.tickle.editor.tabs.SceneTab

class ActorAttributesDockable
    : TickleDockable(DOCKABLE_ACTOR_ATTRIBUTES) {

    private val noContent = Label("N/A")

    override val dockableContent = SimpleObjectProperty<Node>(noContent)

    private var box: ActorAttributesBox? = null

    init {
        MainWindow.instance.tabPane.selectionModel.selectedItemProperty().addListener { _, _, _ ->
            tabChanged()
        }
        tabChanged()
    }

    private fun tabChanged() {
        val tab = MainWindow.instance.tabPane.selectionModel.selectedItem
        if (tab is SceneTab) {

            val content = ActorAttributesBox(tab.sceneEditor)
            box?.unregister()
            box = content

            dockableContent.set(content.build())
            content.actor = tab.sceneEditor.sceneView.selection.latest()?.actor
        } else {
            dockableContent.set(noContent)
        }
    }

    private class ActorAttributesBox(val sceneEditor: SceneEditor) : SelectionListener {

        val stack = StackPane()

        var actorAttributesForm: ActorAttributesForm? = null

        var actor: Actor?
            get() = actorAttributesForm?.actor
            set(v) {
                if (actorAttributesForm?.actor != v) {
                    actorAttributesForm?.unregister()
                    stack.children.clear()
                    if (v == null) {
                        actorAttributesForm = null
                    } else {
                        val showAlignment = sceneEditor.isAutoPositionEnabled(v)
                        actorAttributesForm = ActorAttributesForm(sceneEditor, v, sceneEditor.scene, showAlignment)
                        val form = actorAttributesForm!!.build()
                        stack.children.add(form)
                    }
                }
            }

        init {
            sceneEditor.sceneView.selection.listeners.add(this)
        }

        fun unregister() {
            sceneEditor.sceneView.selection.listeners.remove(this)
        }

        fun build(): Node {
            return stack
        }

        override fun selectionChanged() {
            actor = sceneEditor.sceneView.selection.latest()?.actor
        }

    }
}

