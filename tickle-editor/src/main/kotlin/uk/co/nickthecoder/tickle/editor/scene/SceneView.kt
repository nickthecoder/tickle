package uk.co.nickthecoder.tickle.editor.scene

import javafx.beans.property.*
import javafx.scene.control.*
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.scene.history.*
import uk.co.nickthecoder.tickle.resources.Resources

/**
 * Renders a Scene using OpenGL, but displays it within a JavaFX ScrollPane.
 */
class SceneView(val sceneEditor: SceneEditor) : Control() {

    val scene: Scene get() = sceneEditor.scene

    val editingStages get() = sceneEditor.editingStages

    val selection = Selection(this)

    val history: History get() = sceneEditor.history

    val resources: Resources get() = Editor.resources

    // PROPERTIES

    val mouseXProperty: FloatProperty = SimpleFloatProperty(0f)
    var mouseX: Float
        get() = mouseXProperty.value
        set(v) {
            mouseXProperty.value = v
        }

    val mouseYProperty: FloatProperty = SimpleFloatProperty(0f)
    var mouseY: Float
        get() = mouseYProperty.value
        set(v) {
            mouseYProperty.value = v
        }

    /**
     * The world point at the center of the view.
     */
    val centerXProperty: FloatProperty = SimpleFloatProperty(Editor.resources.gameInfo.width / 2.toFloat())
    var centerX: Float
        get() = centerXProperty.value
        set(v) {
            centerXProperty.value = v
        }

    val centerYProperty: FloatProperty = SimpleFloatProperty(Editor.resources.gameInfo.height / 2.toFloat())
    var centerY: Float
        get() = centerYProperty.value
        set(v) {
            centerYProperty.value = v
        }

    val dirtyProperty: BooleanProperty = SimpleBooleanProperty(false)
    var dirty: Boolean
        get() = dirtyProperty.value
        set(v) {
            dirtyProperty.value = v
        }

    /**
     * The scaling factor for the view.  1 = No Zoom, 2 = Twice as big as normal etc.
     */
    val scaleProperty: FloatProperty = SimpleFloatProperty(1f)
    var scale: Float
        get() = Math.max(0.01f, scaleProperty.value)
        set(v) {
            scaleProperty.value = v
        }

    val stampCostumeProperty: ObjectProperty<Costume> = SimpleObjectProperty(null)
    var stampCostume: Costume?
        get() = stampCostumeProperty.value
        set(v) {
            stampCostumeProperty.value = v
        }

    /**
     * Help for the user (for the status bar) on how to use the currently selected mode.
     */
    val tipProperty: StringProperty = SimpleStringProperty("")
    private var tip: String
        get() = tipProperty.value
        set(v) {
            tipProperty.value = v
        }

    override fun createDefaultSkin(): Skin<*> = SceneViewSkin(this)

    fun redraw() {
        dirty = true
    }

    fun resetZoom() {
        // If we reset zoom twice, then pan to the center.
        if (scale == 1.0f) {
            centerX = resources.gameInfo.width / 2f
            centerY = resources.gameInfo.height / 2f
        }
        scale = 1.0f
    }

    /**
     * Note, steps should be the same as in [SceneEditor.createZoomIndicator]
     */
    fun zoomIn() {
        if (scale < 0.1f) {
            scale = 0.1f
        } else {
            scale += if (scale >= 1.99f) {
                .50f
            } else if (scale > 0.99f) {
                .20f
            } else {
                .10f
            }
        }
    }

    /**
     * Note, steps should be the same as in [SceneEditor.createZoomIndicator]
     */
    fun zoomOut() {
        if (scale < 0.02f) {
            scale = 0.01f
        } else {
            scale -= if (scale <= 0.1f) {
                .01f
            } else if (scale <= 1f) {
                .1f
            } else if (scale <= 2f) {
                .2f
            } else {
                .5f
            }
        }

    }

}
