/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.geometry.Side
import javafx.scene.control.Alert
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParameterException
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.parameters.fields.TaskForm
import uk.co.nickthecoder.paratask.util.FileLister
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.parameters.ClassAndAttributesParameter
import uk.co.nickthecoder.tickle.editor.parameters.Vector2Parameter
import uk.co.nickthecoder.tickle.editor.resources.NoStageConstraint
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.resources.SceneWriter
import uk.co.nickthecoder.tickle.editor.resources.StageConstraint
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes
import uk.co.nickthecoder.tickle.resources.*
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.stage.*
import uk.co.nickthecoder.tickle.util.SceneReader
import uk.co.nickthecoder.tickle.util.Vector2

class LayoutTab(val name: String, val layout: Layout)

    : EditTab(name, layout, ResourceType.LAYOUT.createImageView()) {

    // Keeps track of stages that have been renamed. Key is the old name, value is the new name.
    private val stagesRenames = mutableMapOf<String, String>()

    val stagesTask = StagesTask()
    val viewsTask = ViewsTask()

    private val stagesForm = TaskForm(stagesTask)
    private val viewsForm = TaskForm(viewsTask)

    private val minorTabs = TabPane()

    private val stagesTab = Tab("Stages", stagesForm.build())
    private val viewsTab = Tab("Views", viewsForm.build())


    init {
        minorTabs.side = Side.BOTTOM
        minorTabs.tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

        minorTabs.tabs.add(stagesTab)
        minorTabs.tabs.add(viewsTab)
        borderPane.center = minorTabs

        stagesTask.taskD.root.listen { needsSaving = true }
        viewsTask.taskD.root.listen { needsSaving = true }
    }

    override fun justSave(): Boolean {
        if (stagesForm.check()) {
            if (viewsForm.check()) {

                if (stagesRenames.isNotEmpty() || name != stagesTask.nameP.value) {
                    val alert = Alert(Alert.AlertType.INFORMATION).apply {
                        title = "Scanning"
                        contentText = "Scanning scenes which use this layout.\nThis may take a moment."
                        show()
                    }
                    rename()
                    alert.hide()
                }

                stagesTask.run()
                viewsTask.run()

                return true
            } else {
                minorTabs.selectionModel.select(viewsTab)
            }
        } else {
            minorTabs.selectionModel.select(stagesTab)
        }
        return false
    }

    internal fun rename() {
        val lister = FileLister(10, extensions = listOf("scene"))

        lister.listFiles(Editor.resources.sceneDirectory).forEach { file ->
            val scene = SceneReader.load(file, Editor.resources, mergeIncludes = false)
            var needsSaving = false

            if (scene.layoutName == name && scene.layoutName != stagesTask.nameP.value) {
                scene.layoutName = stagesTask.nameP.value
                needsSaving = true
            }

            if (scene.layoutName == stagesTask.nameP.value) {
                for (oldName in scene.stages.keys.toList()) {
                    val newName = stagesRenames[oldName]
                    if (newName != null) {
                        needsSaving = true
                        scene.renameStage(oldName, newName)
                    }
                }
            }

            if (needsSaving) {
                val preferences = SceneWriter.loadPreferences(file)
                SceneWriter.save(file, scene, preferences, Editor.resources)
            }
        }
    }

    inner class StagesTask() : AbstractTask(threaded = false) {

        val nameP = StringParameter("name", value = name)
        private var commentsP = StringParameter("comments", required = false, rows = 5)

        val stagesP = MultipleParameter("stages", minItems = 1, isBoxed = true) {
            LayoutStageParameter()
        }

        override val taskD = TaskDescription("editLayout")
            .addParameters(nameP, commentsP, stagesP)

        init {
            commentsP.value = layout.comments

            stagesP.asListDetail(
                width = 200,
                height = 200
            ) { if (it.stageNameP.value.isBlank()) "<new>" else it.stageNameP.value }

            layout.layoutStages.forEach { name, layoutStage ->
                val inner = stagesP.newValue()
                inner.stageNameP.listen {
                    stagesRenames[name] = inner.stageNameP.value
                }

                inner.stageNameP.value = name
                inner.defaultStageP.value = layout.defaultLayoutStage === layoutStage

                inner.stageClassP.classString = layoutStage.stageString
                inner.stageClassP.attributes = (layoutStage.stageAttributes as DesignAttributes).copy()
                inner.stageClassP.onAttributesChanged { needsSaving = true }

                inner.stageConstraintP.classP.value = ScriptManager.classForName(layoutStage.stageConstraintString)
                inner.stageConstraintP.attributes = (layoutStage.constraintAttributes as DesignAttributes).copy()
                inner.stageConstraintP.onAttributesChanged { needsSaving = true }
            }

        }

        override fun customCheck() {
            val l = Editor.resources.layouts.find(nameP.value)
            if (l != null && l != layout) {
                throw ParameterException(nameP, "This name is already used.")
            }

        }

        override fun run() {
            if (nameP.value != name) {
                Editor.resources.layouts.rename(name, nameP.value)
            }
            layout.comments = commentsP.value

            layout.layoutStages.clear()

            stagesP.innerParameters.forEach { inner ->
                val layoutStage = LayoutStage(Editor.resources)
                layout.layoutStages[inner.stageNameP.value] = layoutStage
                if (inner.defaultStageP.value == true) {
                    layout.defaultLayoutStage = layoutStage
                }

                layoutStage.stageString = inner.stageClassP.classString
                layoutStage.stageAttributes = inner.stageClassP.attributes ?: Editor.resources.createAttributes()

                layoutStage.stageConstraintString = inner.stageConstraintP.classP.value!!.name
                layoutStage.constraintAttributes = inner.stageConstraintP.attributes
                    ?: Editor.resources.createAttributes()
            }
        }

    }

    inner class ViewsTask() : AbstractTask(threaded = false) {

        val viewsP = MultipleParameter("views", label = "", minItems = 1) {
            LayoutViewParameter()
        }

        override val taskD = TaskDescription("editLayoutViews")
            .addParameters(viewsP)

        init {
            viewsP.asListDetail(
                width = 200,
                height = 350
            ) { if (it.viewNameP.value.isBlank()) "<new>" else it.viewNameP.value }

            layout.layoutViews.forEach { name, layoutView ->
                val inner = viewsP.newValue()
                inner.viewNameP.value = name
                inner.stageNameP.value = layoutView.stageName

                inner.viewClassP.classString = layoutView.viewString
                inner.viewClassP.attributes = (layoutView.viewAttributes as DesignAttributes).copy()
                inner.viewClassP.onAttributesChanged { needsSaving = true }

                inner.zOrderP.value = layoutView.zOrder

                with(layoutView.position) {
                    inner.autoScaleP.value = autoScale
                    inner.alignmentP.value = letterboxAlignment
                    inner.enableAutoPositionP.value = enableAutoPosition

                    inner.hAlignmentP.value = hAlignment
                    inner.leftRightMarginP.value = leftRightMargin
                    inner.hPositionP.value = hPosition
                    inner.widthP.value = width
                    inner.widthRatioP.value = widthRatio
                    if (width != null) {
                        inner.hOneOfP.value = inner.widthP
                    } else if (widthRatio != null) {
                        inner.hOneOfP.value = inner.widthRatioP
                    } else {
                        inner.hOneOfP.value = inner.remainingWidthP
                    }

                    inner.vAlignmentP.value = vAlignment
                    inner.topBottomMarginP.value = topBottomMargin
                    inner.vPositionP.value = vPosition
                    inner.heightP.value = height
                    inner.heightRatioP.value = heightRatio
                    if (height != null) {
                        inner.vOneOfP.value = inner.heightP
                    } else if (heightRatio != null) {
                        inner.vOneOfP.value = inner.heightRatioP
                    } else {
                        inner.vOneOfP.value = inner.remainingHeightP
                    }
                }
            }
        }


        override fun customCheck() {
            viewsP.innerParameters.forEach {
                val stageName = it.stageNameP.value
                if (!stagesTask.stagesP.innerParameters.map { it.stageNameP.value }.contains(stageName)) {
                    throw ParameterException(it.stageNameP, "This is not a name of a stage in this layout")
                }
            }
        }

        override fun run() {

            layout.layoutViews.clear()

            viewsP.innerParameters.forEach { inner ->
                val layoutView = LayoutView(Editor.resources)
                layout.layoutViews[inner.viewNameP.value] = layoutView
                layoutView.position.autoScale = inner.autoScaleP.value ?: AutoScale.NONE
                layoutView.position.letterboxAlignment.set(inner.alignmentP.value)
                layoutView.position.enableAutoPosition = inner.enableAutoPositionP.value == true

                with(layoutView) {
                    stageName = inner.stageNameP.value
                    viewString = inner.viewClassP.classString
                    viewAttributes = inner.viewClassP.attributes ?: Editor.resources.createAttributes()
                    zOrder = inner.zOrderP.value!!
                }

                with(layoutView.position) {

                    // X
                    hAlignment = inner.hAlignmentP.value!!
                    if (hAlignment == FlexHAlignment.MIDDLE) {
                        hPosition = inner.hPositionP.value!!
                    } else {
                        leftRightMargin = inner.leftRightMarginP.value!!
                    }
                    width = if (inner.hOneOfP.value == inner.widthP) inner.widthP.value!! else null
                    widthRatio = if (inner.hOneOfP.value == inner.widthRatioP) inner.widthRatioP.value!! else null

                    // Y
                    vAlignment = inner.vAlignmentP.value!!
                    if (vAlignment == FlexVAlignment.MIDDLE) {
                        vPosition = inner.vPositionP.value!!
                    } else {
                        topBottomMargin = inner.topBottomMarginP.value!!
                    }
                    height = if (inner.vOneOfP.value == inner.heightP) inner.heightP.value!! else null
                    heightRatio = if (inner.vOneOfP.value == inner.heightRatioP) inner.heightRatioP.value!! else null
                }
            }
        }
    }


    inner class LayoutStageParameter() : MultipleGroupParameter("stage") {

        val stageNameP = StringParameter("stageName")
        val defaultStageP = BooleanParameter("defaultStage", value = false)
        val stageClassP =
            ClassAndAttributesParameter(
                "class",
                Stage::class.java,
                value = GameStage::class.java
            ).apply { classP.value = GameStage::class.java }
        val stageConstraintP =
            ClassAndAttributesParameter("constraint", StageConstraint::class.java, value = null).apply {
                classP.value = NoStageConstraint::class.java
            }
        val createViewP =
            ButtonParameter("createView", label = "", buttonText = "Create Whole Screen View") { createView() }

        init {
            addParameters(stageNameP, defaultStageP, stageClassP, stageConstraintP, createViewP)
        }

        fun createView() {
            viewsTask.viewsP.innerParameters.forEach { inner ->
                if (inner.viewNameP.value == stageNameP.value) {
                    return
                }
            }
            val inner = viewsTask.viewsP.newValue()
            inner.viewNameP.value = stageNameP.value
            inner.stageNameP.value = stageNameP.value
            inner.zOrderP.value = 50
            inner.viewClassP.classP.value = ZOrderStageView::class.java
        }
    }


    inner class LayoutViewParameter() : MultipleGroupParameter("view") {

        val viewNameP = StringParameter("viewName")
        val stageNameP = StringParameter("stageName")
        val viewClassP =
            ClassAndAttributesParameter("class", StageView::class.java, value = ZOrderStageView::class.java)
        val zOrderP = IntParameter("zOrder")
        val autoScaleP = ChoiceParameter("autoScale", value = AutoScale.NONE).enumChoices()
        val alignmentP = Vector2Parameter("alignment", value = Vector2(0.5f, 0.5f))
        val enableAutoPositionP = BooleanParameter("enableAutoPosition", required = true, value = false)

        // X
        val hAlignmentP = ChoiceParameter("hAlignment", label = "", value = FlexHAlignment.LEFT).enumChoices()
        val leftRightMarginP = IntParameter("leftRightMargin", label = "Margin", value = 0)
        val hPositionP = DoubleParameter("hPosition", label = "Position", value = 0.5)
        val hAlignGroupP = SimpleGroupParameter("hAlignGroup", label = "Align")
            .addParameters(hAlignmentP, leftRightMarginP, hPositionP).asHorizontal(LabelPosition.LEFT)

        val remainingWidthP = InformationParameter("remainingWidth", label = "Remaining", information = "")
        val widthP = IntParameter("width", label = "Fixed Width")
        val widthRatioP = DoubleParameter("widthRatio", label = "Ratio")
        val hOneOfP = OneOfParameter("hOneOf", label = "Width", value = remainingWidthP, choiceLabel = "Choose")
            .addChoices(remainingWidthP, widthP, widthRatioP)

        val xGroup = SimpleGroupParameter("x")
            .addParameters(hAlignGroupP, hOneOfP, remainingWidthP, widthP, widthRatioP)

        // Y
        val vAlignmentP =
            ChoiceParameter<FlexVAlignment>("vAlignment", label = "", value = FlexVAlignment.TOP).enumChoices()
        val topBottomMarginP = IntParameter("topBottomMargin", label = "Margin", value = 0)
        val vPositionP = DoubleParameter("vPosition", label = "Position", value = 0.5)
        val vAlignGroupP = SimpleGroupParameter("vAlignGroup", label = "Align")
            .addParameters(vAlignmentP, topBottomMarginP, vPositionP).asHorizontal(LabelPosition.LEFT)

        val remainingHeightP = InformationParameter("remainingHeight", label = "Remaining", information = "")
        val heightP = IntParameter("height", label = "Fixed Height")
        val heightRatioP = DoubleParameter("heightRatio", label = "Ratio")
        val vOneOfP = OneOfParameter("vOneOf", label = "Height", value = remainingHeightP, choiceLabel = "Choose")
            .addChoices(remainingHeightP, heightP, heightRatioP)

        val yGroup = SimpleGroupParameter("y")
            .addParameters(vAlignGroupP, vOneOfP, remainingHeightP, heightP, heightRatioP)


        init {

            asPlain()
            hPositionP.hidden = true
            vPositionP.hidden = true
            alignmentP.hidden = true

            hAlignmentP.listen {
                leftRightMarginP.hidden = hAlignmentP.value == FlexHAlignment.MIDDLE
                hPositionP.hidden = !leftRightMarginP.hidden
            }
            vAlignmentP.listen {
                topBottomMarginP.hidden = vAlignmentP.value == FlexVAlignment.MIDDLE
                vPositionP.hidden = !topBottomMarginP.hidden
            }
            autoScaleP.listen {
                alignmentP.hidden = autoScaleP.value != AutoScale.LETTERBOX && autoScaleP.value != AutoScale.BEST_FIT
            }

            addParameters(
                viewNameP,
                stageNameP,
                viewClassP,
                zOrderP,
                autoScaleP,
                alignmentP,
                enableAutoPositionP,
                xGroup,
                yGroup
            )
        }
    }

}
