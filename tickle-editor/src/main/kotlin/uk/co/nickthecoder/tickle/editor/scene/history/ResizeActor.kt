/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor

class ResizeActor(
    private val actor: Actor,
    private val oldX: Float,
    private val oldY: Float,
    private val oldSizeX: Float,
    private val oldSizeY: Float

) : Change {

    private var newX = actor.x
    private var newY = actor.y
    private var newSizeX = actor.resizeAppearance!!.size.x
    private var newSizeY = actor.resizeAppearance!!.size.y

    override fun redo(sceneEditor: SceneEditor) {
        actor.x = newX
        actor.y = newY
        actor.resizeAppearance!!.size.x = newSizeX
        actor.resizeAppearance!!.size.y = newSizeY
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        actor.x = oldX
        actor.y = oldY
        actor.resizeAppearance!!.size.x = oldSizeX
        actor.resizeAppearance!!.size.y = oldSizeY
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun mergeWith(other: Change): Boolean {
        if (other is ResizeActor && other.actor == actor) {
            other.newX = newX
            other.newY = newY
            other.newSizeX = newSizeX
            other.newSizeY = newSizeY
            return true
        }
        return false
    }

}
