package uk.co.nickthecoder.tickle.editor.code

import java.lang.reflect.ParameterizedType

class TypeSense(
        val type: Class<*>,
        val pt: ParameterizedType? = null
) {
    companion object {
        val VOID = TypeSense(Unit::class.java) // Is this right?
        val BOOLEAN = TypeSense(Boolean::class.java)
        val BYTE = TypeSense(Byte::class.java)
        val NUMBER = TypeSense(Number::class.java)
        val STRING = TypeSense(String::class.java)
    }
}
