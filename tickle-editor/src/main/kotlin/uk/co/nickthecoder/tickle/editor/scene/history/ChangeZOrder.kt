/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor

/**
 * Used when we reset Z Orders from the default values in Costume.
 */
class ChangeZOrder(
    private val actor: Actor,
    private val newZOrder: Float
) : Change {

    private val oldZOrder = actor.zOrder

    override fun redo(sceneEditor: SceneEditor) {
        actor.zOrder = newZOrder
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        actor.zOrder = oldZOrder
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }
}
