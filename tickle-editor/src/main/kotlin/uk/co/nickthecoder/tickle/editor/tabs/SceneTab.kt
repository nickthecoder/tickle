/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.Separator
import javafx.scene.layout.BorderPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.stage.Stage
import uk.co.nickthecoder.fxessentials.requestFocusLater
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.parameters.fields.TaskForm
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.parameters.ClassAndAttributesParameter
import uk.co.nickthecoder.tickle.editor.resources.*
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor
import uk.co.nickthecoder.tickle.editor.tasks.DeleteSceneTask
import uk.co.nickthecoder.tickle.editor.tasks.RenameSceneTask
import uk.co.nickthecoder.tickle.editor.util.DesignAttributes
import uk.co.nickthecoder.tickle.editor.util.toJavaFX
import uk.co.nickthecoder.tickle.editor.util.toTickle
import uk.co.nickthecoder.tickle.util.SceneReader
import java.io.File

class SceneTab(sceneName: String, sceneStub: SceneStub)

    : EditTab(sceneName, sceneStub, ResourceType.SCENE.createImageView()),
    HasExtras, SceneListener {

    val scene = SceneReader.load(sceneStub.file, Editor.resources, mergeIncludes = false)

    private val task = SceneDetailsTask(this, sceneName, scene)
    private val taskForm = TaskForm(task)

    val sceneEditor = SceneEditor(scene, SceneWriter.loadPreferences(sceneStub.file))

    val sceneFile = sceneStub.file

    val detailsNode = buildDetails()
    val editorNode = sceneEditor.build()

    init {
        // Don't show any of the buttons of a "regular" EditTab.
        borderPane.bottom = null
        borderPane.center = editorNode
        sceneEditor.statusBar.items.add(0, Button("Details").apply {
            onAction = EventHandler { onDetails() }
        })

        sceneEditor.sceneListeners.add(this)
    }

    override fun preferencesUpdated() {
        needsSaving = true
    }

    override fun actorModified(scene: Scene, actor: Actor, type: ModificationType) {
        needsSaving = true
    }

    fun buildDetails(): Node {

        return BorderPane().apply {
            styleClass.add("prompt")

            center = taskForm.build()

            bottom = HBox().apply {
                children.addAll(
                    FlowPane().apply {
                        styleClass.addAll("buttons", "left")
                        HBox.setHgrow(this, Priority.ALWAYS)

                        children.addAll(
                            Button("Scene Editor").apply {
                                onAction = EventHandler { onEditScene() }
                            },
                            Button("Copy").apply {
                                onAction = EventHandler { onCopy() }
                            },

                            Button("Rename").apply {
                                onAction = EventHandler { onRename() }
                            },

                            Button("Delete").apply {
                                onAction = EventHandler { onDelete() }
                            }
                        )
                    },
                    FlowPane().apply {
                        styleClass.addAll("buttons", "right")

                        children.addAll(
                            Button("Save").apply {
                                onAction = EventHandler { justSave() }
                            },
                            Button("Cancel").apply {
                                onAction = EventHandler { onCancel() }
                            }
                        )
                    }
                )
            }
        }
    }

    override fun focus() {
        sceneEditor.sceneView.requestFocusLater()
    }

    private fun onEditScene() {
        borderPane.center = editorNode
    }

    private fun onDetails() {
        borderPane.center = detailsNode
    }


    override fun extraButtons() = listOf(
        sceneEditor.singleStageButton,
        Separator(),
        sceneEditor.toggleGridButton,
        sceneEditor.toggleGuidesButton,
        sceneEditor.toggleSnapToOthersButton,
        sceneEditor.toggleSnapRotationButton
    )

    override fun justSave(): Boolean {
        if (taskForm.check()) {
            task.run()
            SceneWriter.save(sceneFile, scene, sceneEditor.snapPreferences, Editor.resources)
            return true
        }
        return false
    }

    fun onCopy() {
        TaskPrompter(CopySceneTask()).placeOnStage(Stage())
    }

    fun onDelete() {
        val task = DeleteSceneTask(sceneFile)
        task.taskRunner.listen { cancelled ->
            if (!cancelled) {
                close()
            }
        }
        TaskPrompter(task).placeOnStage(Stage())
    }

    fun onRename() {
        val task = RenameSceneTask(sceneFile)
        task.taskRunner.listen { cancelled ->
            if (!cancelled) {
                close()
                // TODO Re-implement this
                //sceneResource.file = task.newFile()
                //MainWindow.instance.openTab(task.newNameP.value, sceneResource)
            }
        }
        TaskPrompter(task).placeOnStage(Stage())
    }

    override fun removed() {
        super.removed()
        sceneEditor.cleanUp()
    }

    fun selectCostume(costume: Costume) {
        sceneEditor.selectCostume(costume)
    }


    inner class CopySceneTask : AbstractTask(threaded = false) {

        val newNameP = StringParameter("newName")
        val directoryP =
            FileParameter("directory", mustExist = true, expectFile = false, value = sceneFile.parentFile)

        override val taskD = TaskDescription("copyScene")
            .addParameters(newNameP, directoryP)

        override fun run() {
            val newFile = File(directoryP.value!!, "${newNameP.value}.scene")
            sceneFile.copyTo(newFile)
            Editor.resources.fireAdded(newFile, newFile.nameWithoutExtension)
        }
    }


}


class SceneDetailsTask(val sceneTab: SceneTab, val name: String, val scene: Scene) : AbstractTask() {

    val directorP =
        ClassAndAttributesParameter("director", Director::class.java, value = NoDirector::class.java).apply {
            classString = scene.director.javaClass.name
            attributes = DesignAttributes().apply {
                updateAttributesMetaData(scene.director)
            }
        }

    val backgroundColorP = ColorParameter("backgroundColor")

    val showMouseP = BooleanParameter("showMouse")

    val layoutP = ChoiceParameter<String>("layout", value = "")

    val infoP = InformationParameter(
        "info",
        information = "The Director has no fields with the '@Attribute' annotation, and therefore, this scene has no attributes."
    )

    val includesP = MultipleParameter("includes") {
        StringParameter("include")
    }

    val commentsP = StringParameter("comments", required = false, rows = 5)

    override val taskD = TaskDescription("sceneDetails")
        .addParameters(directorP, backgroundColorP, showMouseP, layoutP, includesP, commentsP)

    init {
        Editor.resources.layouts.items().forEach { name, _ ->
            layoutP.choice(name, name, name)
        }

        with(scene) {
            backgroundColorP.value = background.toJavaFX()
            showMouseP.value = showMouse
            layoutP.value = layoutName
            includedScenes.keys.forEach { scenePath ->
                includesP.addValue(scenePath)
            }
            commentsP.value = comments
        }

        taskD.root.listen { sceneTab.needsSaving = true }
        directorP.onAttributesChanged { sceneTab.needsSaving = true }

    }


    override fun run() {
        with(scene) {
            if (director.javaClass.name != directorP.classString) {
                director = Director.createDirector(directorP.classString)
                // TODO Update
            }
            layoutName = layoutP.value!!
            showMouse = showMouseP.value == true
            background = backgroundColorP.value.toTickle()
            //includedScenes.clear()
            includesP.value.forEach { str ->
                if (includedScenes[str] == null) {
                    includedScenes[str] =
                        SceneReader.load(Editor.resources.scenePathToFile(str), Editor.resources, false)
                }
            }
            // TODO Remove included scenes too
            comments = commentsP.value
        }
        // TODO Update the SceneEditor
    }

}
