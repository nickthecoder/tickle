/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.resources

import javafx.scene.control.Tooltip
import javafx.scene.image.ImageView
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.events.Input
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.Layout
import uk.co.nickthecoder.tickle.sound.Sound
import java.io.File

enum class ResourceType(val label: String, val graphicName: String) {
    ANY("Any Resource", "folder2.png"),
    GAME_INFO("Game Info", "gameInfo.png"),
    API_DOCUMENTATION("API Documentation", "api.png"),
    TEXTURE("Texture", "texture.png"),
    POSE("Pose", "pose.png"),
    COSTUME("Costume", "costume.png"),
    COSTUME_GROUP("Costume Group", "costumeGroup.png"),
    LAYOUT("Layout", "layout.png"),
    INPUT("Input", "input.png"),
    FONT("Font", "font.png"),
    SOUND("Sound", "sound.png"),
    SCENE_DIRECTORY("Scene Directory", "folder.png"),
    SCENE("Scene", "scene.png"),
    SCRIPT_DIRECTORY("Script Directory", "folder.png"),
    SCRIPT("Script", "script.png"),
    MACRO("Macro", "macro.png"),
    SNIPPET("Snippet", "snippet.png");

    fun canCreate(): Boolean = this != ANY && this != GAME_INFO

    fun createImageView() = ImageView(Editor.imageResource(graphicName)).apply {
        Tooltip.install(this, Tooltip(label))
    }

    fun findResource(name: String): Any? {
        val resources = Editor.resources

        return when (this) {
            TEXTURE -> resources.textures.find(name)
            POSE -> resources.poses.find(name)
            COSTUME -> resources.costumes.find(name)
            COSTUME_GROUP -> resources.costumeGroups.find(name)
            LAYOUT -> resources.layouts.find(name)
            INPUT -> resources.inputs.find(name)
            FONT -> resources.fontResources.find(name)
            SOUND -> resources.sounds.find(name)
            SCENE -> {
                val file = resources.scenePathToFile(name)
                if (file.exists()) {
                    SceneStub(file)
                } else {
                    null
                }
            }
            SCRIPT -> {
                val file = File(Editor.resources.scriptDirectory(), name)
                if (file.exists()) {
                    ScriptStub(file)
                } else {
                    null
                }
            }
            MACRO -> {
                val file = File(Editor.resources.macrosDirectory, name)
                if (file.exists()) {
                    MacroStub(file)
                } else {
                    null
                }
            }
            API_DOCUMENTATION -> {
                try {
                    Class.forName(name)
                } catch (e: Exception) {
                    null
                }
            }
            else -> null
        }
    }

    companion object {

        fun resourceType(resource: Any?): ResourceType? {
            return when (resource) {
                is GameInfo -> GAME_INFO
                is Texture -> TEXTURE
                is Pose -> POSE
                is Costume -> COSTUME
                is CostumeGroup -> COSTUME_GROUP
                is Layout -> LAYOUT
                is Input -> INPUT
                is FontResource -> FONT
                is Sound -> SOUND
                is Scene -> SCENE
                is SceneStub -> SCENE
                is ScriptStub -> SCRIPT
                is MacroStub -> MACRO
                else -> null
            }
        }
    }

}
