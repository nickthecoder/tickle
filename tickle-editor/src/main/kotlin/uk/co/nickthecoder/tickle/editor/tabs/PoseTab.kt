/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Rectangle2D
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.MenuButton
import javafx.scene.control.MenuItem
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParameterException
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.ResizeType
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.parameters.*
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.tasks.NewResourceTask
import uk.co.nickthecoder.tickle.editor.util.ImageCache
import uk.co.nickthecoder.tickle.util.Vector2

class PoseTab(name: String, val pose: Pose) :
    EditTaskTab(PoseTask(name, pose), name, pose, ResourceType.POSE.createImageView()) {

    val costumesButton = MenuButton("Costumes")

    init {

        val createCostumeButton = Button("Create Costume")
        createCostumeButton.setOnAction { (task as PoseTask).createCostume() }
        leftButtons.children.add(createCostumeButton)

        buildCostumesButton()
    }

    private fun buildCostumesButton() {
        val costumes = mutableMapOf<String, Costume>()
        Editor.resources.costumes.items().forEach { name, costume ->
            if (costume.dependsOn(pose)) {
                costumes[name] = costume
            }
        }

        if (costumes.isEmpty()) {
            leftButtons.children.remove(costumesButton)
        } else {
            costumesButton.items.clear()
            costumes.forEach { costumeName, costume ->
                val menuItem = MenuItem(costumeName)
                costumesButton.items.add(menuItem)
                menuItem.onAction = EventHandler { MainWindow.instance.openTab(costumeName, costume) }
            }
            if (!leftButtons.children.contains(costumesButton)) {
                leftButtons.children.add(costumesButton)
            }
        }
    }

}

class PoseTask(val name: String, val pose: Pose) : AbstractTask() {

    val nameP = StringParameter("name", value = name)
    val textureNameP =
        ButtonParameter("texture", buttonText = Editor.resources.textures.findName(pose.texture) ?: "<none>") {
            editTexture()
        }

    val positionInfoP = InformationParameter(
        "positionInfo",
        information = "Positions are measured from the TOP left of the Texture's image"
    )

    val positionP = RectiParameter("position", bottomUp = false)

    val offsetInfoP =
        InformationParameter("offsetInfo", information = "Offsets are measured from the BOTTOM left of the Pose")

    val offsetP = XYParameter("offset")

    val directionP = DoubleParameter(
        "direction",
        description = "The direction of the pose in degrees. 0 is to the right, and +ve numbers are anti-clockwise."
    )

    //val tiledP = BooleanParameter("tiled")
    val resizeTypeP = ChoiceParameter<ResizeType>("resizeType").enumChoices()

    val marginLP = IntParameter("marginLeft", label = "Left")
    val marginRP = IntParameter("marginRight", label = "Right")
    val marginTP = IntParameter("marginTop", label = "Top")
    val marginBP = IntParameter("marginBottom", label = "Bottom")
    val marginsP = SimpleGroupParameter("ninePatchMargins").apply {
        addParameters(marginLP, marginTP, marginRP, marginBP)
        asHorizontal(LabelPosition.TOP)
    }

    val imageP = ImageParameter("image", image = ImageCache.image(pose.texture.file!!)) { PoseImageField(it) }

    val infoP = InformationParameter(
        "info",
        information = "Click the image to set the offsets. Hold down Ctrl to add a Snap Point. Right click to change the background colour."
    )

    val snapPointsP = MultipleParameter("snapPoints", isBoxed = true) {
        Vector2Parameter("snapPoint").asHorizontal()
    }

    val commentP = StringParameter("comment", required = false, rows = 5)

    override val taskD = TaskDescription("editPose")
        .addParameters(
            nameP,
            textureNameP,
            positionInfoP,
            positionP,
            offsetInfoP,
            offsetP,
            directionP,
            resizeTypeP,
            marginsP,
            commentP,
            imageP,
            infoP,
            snapPointsP
        )

    init {

        offsetP.x = pose.offsetX
        offsetP.y = pose.offsetY

        positionP.left = pose.pixelRect.left
        positionP.right = pose.pixelRect.right
        positionP.top = pose.pixelRect.top
        positionP.bottom = pose.pixelRect.bottom

        directionP.value = pose.direction.degrees
        resizeTypeP.value = pose.resizeType
        commentP.value = pose.comment

        marginLP.value = pose.ninePatchMargins.left
        marginRP.value = pose.ninePatchMargins.right
        marginTP.value = pose.ninePatchMargins.top
        marginBP.value = pose.ninePatchMargins.bottom

        snapPointsP.clear()
        pose.snapPoints.forEach { snapPointsP.addValue(it) }

        resizeTypeP.listen {
            marginsP.hidden = resizeTypeP.value != ResizeType.NINE_PATCH
        }
        marginsP.hidden = resizeTypeP.value != ResizeType.NINE_PATCH

        positionP.listen {
            updateViewport()
        }
        updateViewport()
    }

    override fun customCheck() {
        val p = Editor.resources.poses.find(nameP.value)
        if (p != null && p != pose) {
            throw ParameterException(nameP, "This name is already used.")
        }
    }

    override fun run() {
        if (nameP.value != name) {
            Editor.resources.poses.rename(name, nameP.value)
        }
        pose.pixelRect.left = positionP.left!!
        pose.pixelRect.bottom = positionP.bottom!!
        pose.pixelRect.right = positionP.right!!
        pose.pixelRect.top = positionP.top!!

        pose.offsetX = offsetP.x!!
        pose.offsetY = offsetP.y!!
        pose.comment = commentP.value

        pose.direction.degrees = directionP.value!!
        pose.resizeType = resizeTypeP.value!!

        pose.ninePatchMargins.left = marginLP.value!!
        pose.ninePatchMargins.right = marginRP.value!!
        pose.ninePatchMargins.top = marginTP.value!!
        pose.ninePatchMargins.bottom = marginBP.value!!

        pose.snapPoints.clear()
        pose.snapPoints.addAll(snapPointsP.value)
    }

    fun editTexture() {
        val trName = Editor.resources.textures.findName(pose.texture)
        if (trName != null) {
            MainWindow.instance.openTab(trName, pose.texture)
        }
    }

    fun createCostume() {
        val task = NewResourceTask(pose, nameP.value)
        task.prompt()
    }


    fun updateViewport() {
        val left = positionP.left
        val top = positionP.top
        val width = positionP.width
        val height = positionP.height

        if (left == null || top == null || width == null || height == null) {
            imageP.viewPort = Rectangle2D.EMPTY
        } else {
            imageP.viewPort = Rectangle2D(left.toDouble(), top.toDouble(), width.toDouble(), height.toDouble())
        }

    }

    inner class PoseImageField(imageParameter: ImageParameter) : ImageParameterField(imageParameter) {

        override fun createControl(): Node {
            val iv = super.createControl()
            val stack = StackPane()

            val colors = listOf(Color.LIGHTGRAY, Color.DARKGRAY, Color.BLACK, Color.WHITE)
            var colorIndex = 0

            stack.addEventHandler(MouseEvent.MOUSE_PRESSED) { event ->
                if (event.button == MouseButton.SECONDARY) {
                    colorIndex++
                    if (colorIndex >= colors.size) colorIndex = 0

                    stack.background = Background(BackgroundFill(colors[colorIndex], CornerRadii(0.0), Insets(0.0)))
                } else {
                    if (event.isControlDown) {
                        positionP.height?.let { height ->
                            snapPointsP.addValue(Vector2(event.x.toFloat(), height - event.y.toFloat()))
                        }
                    } else {
                        positionP.height?.let { height ->
                            offsetP.x = event.x.toFloat()
                            offsetP.y = (height - event.y).toFloat()
                        }
                    }
                }
            }

            stack.background = Background(BackgroundFill(colors[colorIndex], CornerRadii(0.0), Insets(0.0)))
            stack.style = "-fx-cursor: crosshair;"
            stack.children.add(iv)

            return stack
        }
    }

}
