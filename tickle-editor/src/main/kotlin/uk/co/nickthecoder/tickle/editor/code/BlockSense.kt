package uk.co.nickthecoder.tickle.editor.code

import org.antlr.v4.runtime.ParserRuleContext

open class BlockSense(val parent: BlockSense?) {

    val operandStack = mutableListOf<Operand>()

    val variables = mutableMapOf<String, VariableSense>()

    fun declareVariable(ctx: ParserRuleContext, type: TypeSense) {
        val name = ctx.text
        variables[name] = VariableSense(ctx.start.line, ctx.start.charPositionInLine, name, type)
    }

}

class VariableSense(val line: Int, val column: Int, val name: String, val type: TypeSense)
