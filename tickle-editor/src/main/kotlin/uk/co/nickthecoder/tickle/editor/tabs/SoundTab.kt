/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.stage.Stage
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.ParameterException
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.UnthreadedTaskRunner
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.tasks.RenameFileTask
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.sound.Sound

class SoundTab(name: String, sound: Sound) :
    EditTaskTab(SoundTask(name, sound), name, sound, ResourceType.SOUND.createImageView())

class SoundTask(val name: String, val sound: Sound) : AbstractTask() {

    override val taskRunner = UnthreadedTaskRunner(this)

    private val nameP = StringParameter("name", value = name)

    private val fileP = StringParameter("file", value = sound.file?.path ?: "")

    private val renameP = ButtonParameter("rename", buttonText = "Rename") { onRename() }

    private val fileAndRenameP = SimpleGroupParameter("fileAndRename", label = "Filename")
            .addParameters(fileP, renameP).asHorizontal(labelPosition = LabelPosition.NONE)

    private val playP = ButtonParameter("play", buttonText = "Play") { onPlay() }

    private val defaultGainP = DoubleParameter("defaultGain", value = sound.defaultGain, minValue = 0.0)

    private val commentsP = StringParameter("comments", value=sound.comments, required=false, rows=5)

    override val taskD = TaskDescription("editSound")
            .addParameters(nameP, fileAndRenameP, defaultGainP, playP, commentsP)


    override fun customCheck() {
        val p = Editor.resources.sounds.find(nameP.value)
        if (p != null && p != sound) {
            throw ParameterException(nameP, "This name is already used.")
        }
    }

    private fun attemptSave(): Boolean {
        try {
            taskRunner.run()
            return true
        } catch (e: Exception) {
            return false
        }
    }

    override fun run() {
        if (nameP.value != name) {
            Editor.resources.sounds.rename(name, nameP.value)
        }
        sound.defaultGain = defaultGainP.value ?: 1.0
        sound.comments = commentsP.value
    }

    private fun onPlay() {
        attemptSave()
        onOpenGLThread {
            sound.play()
        }
    }

    private fun onRename() {
        if (!attemptSave()) return

        sound.file?.let { file ->
            val renameTask = RenameFileTask(file)

            renameTask.taskRunner.listen { cancelled ->
                if (!cancelled) {
                    renameTask.newNameP.value?.path?.let { fileP.value = it }
                    run()
                    Editor.resources.save()
                }
            }
            val tp = TaskPrompter(renameTask)
            tp.placeOnStage(Stage())
        }
    }

}
