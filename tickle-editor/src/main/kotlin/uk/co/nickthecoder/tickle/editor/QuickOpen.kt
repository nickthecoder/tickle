package uk.co.nickthecoder.tickle.editor

import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.geometry.Side
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.util.StringConverter
import uk.co.nickthecoder.fxessentials.ActionGroup
import uk.co.nickthecoder.paratask.util.FileLister
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.util.ClassMetaData
import java.io.File

class QuickOpen(parentStage: Stage, resourceT: ResourceType) : Stage() {

    private val borderPane = BorderPane()

    private val resourceTypeChoice = ChoiceBox<ResourceType>()
    private val textField = TextField()

    private val resources = Editor.resources

    private var resourceType: ResourceType
        get() = resourceTypeChoice.value
        set(v) {
            if (v != resourceTypeChoice.value) {
                resourceTypeChoice.value = v
            }
        }

    private val actionGroup = ActionGroup().apply {
        action(ESCAPE) { hide() }
        action(QUICK_OPEN_ANY) {
            resourceType = ResourceType.ANY
            textChanged()
        }
        action(QUICK_OPEN_SCRIPT) {
            resourceType = ResourceType.SCRIPT
            textChanged()
        }
        action(QUICK_OPEN_SCENE) {
            resourceType = ResourceType.SCENE
            textChanged()
        }
        action(QUICK_OPEN_COSTUME) {
            resourceType = ResourceType.COSTUME
            textChanged()
        }
        action(QUICK_OPEN_POSE) {
            resourceType = ResourceType.POSE
            textChanged()
        }
        action(QUICK_OPEN_TEXTURE) {
            resourceType = ResourceType.TEXTURE
            textChanged()
        }
        action(QUICK_OPEN_SCRIPT) {
            resourceType = ResourceType.SCRIPT
            textChanged()
        }
        action(QUICK_OPEN_API) {
            resourceType = ResourceType.API_DOCUMENTATION
            textChanged()
        }
        attachTo(borderPane)
    }

    init {
        textField.prefColumnCount = 30

        title = "Quick Open"
        with(borderPane) {
            center = HBox().apply {
                spacing = 6.0
                alignment = Pos.CENTER
                children.addAll(textField, resourceTypeChoice)
            }
        }
        scene = Scene(borderPane)

        textField.textProperty().addListener { _, _, _ -> textChanged() }

        resourceTypeChoice.converter = object : StringConverter<ResourceType>() {
            override fun fromString(string: String) = ResourceType.valueOf(string)
            override fun toString(resourceType: ResourceType) = resourceType.label
        }
        resourceTypeChoice.valueProperty().addListener { _ ->
            textChanged()
        }

        for (rt in ResourceType.values()) {
            if (rt in listOf(
                    ResourceType.GAME_INFO,
                    ResourceType.SCENE_DIRECTORY,
                    ResourceType.SNIPPET,
                    ResourceType.SCRIPT_DIRECTORY
                )
            ) continue
            resourceTypeChoice.items.add(rt)
        }
        resourceType = resourceT

        initOwner(parentStage);
        initModality(Modality.APPLICATION_MODAL)
        textChanged()
        showAndWait()
    }

    private fun textChanged() {
        title = "Quick Open ${resourceType.label}"

        val list = mutableListOf<MatchedResource>()
        val upperSearch = textField.text.toUpperCase()

        fun matchScore(name: String): Double {
            val index = name.toUpperCase().indexOf(upperSearch)
            if (index < 0) return Double.MAX_VALUE

            return index.toDouble() + (name.length - upperSearch.length) * 0.1
        }

        val contextMenu = ContextMenu()
        resourceTypeChoice.contextMenu?.hide()
        // If I don't create a new context menu every time, then when a menu is too big, and is scrolled, then
        // the scroll position is remembered the next time (and the menu appears empty). Bug in JavaFX. Grr.
        resourceTypeChoice.contextMenu = contextMenu

        if (upperSearch.isNotBlank()) {
            for (rt in ResourceType.values()) {
                if (resourceType != ResourceType.ANY && rt != resourceType) continue

                fun addMatches(names: Set<String>) {
                    for (name in names) {
                        val score = matchScore(name)
                        if (score != Double.MAX_VALUE) {
                            list.add(MatchedResource(name, rt, score))
                        }
                    }
                }

                fun addFiles(dir: File, extensions: List<String>) {
                    val lister = FileLister(10, true, extensions)

                    lister.listFiles(dir).forEach { file ->
                        val score = matchScore(file.nameWithoutExtension)
                        if (score != Double.MAX_VALUE) {
                            val file2 = file.relativeToOrSelf(resources.file.parentFile.absoluteFile)
                            val adjustedName = when (rt) {
                                ResourceType.SCRIPT -> file2.path.removePrefix("scripts${File.separatorChar}")
                                ResourceType.SCENE -> file2.path.removePrefix("scenes${File.separatorChar}")
                                    .removeSuffix(".scene")
                                else -> file2.path
                            }
                            list.add(MatchedResource(adjustedName, rt, score))
                        }
                    }
                }

                when (rt) {
                    ResourceType.POSE -> addMatches(resources.poses.items().keys)
                    ResourceType.COSTUME -> addMatches(resources.costumes.items().keys)
                    ResourceType.COSTUME_GROUP -> addMatches(resources.costumeGroups.items().keys)
                    ResourceType.TEXTURE -> addMatches(resources.textures.items().keys)
                    ResourceType.FONT -> addMatches(resources.fontResources.items().keys)
                    ResourceType.SOUND -> addMatches(resources.sounds.items().keys)
                    ResourceType.INPUT -> addMatches(resources.inputs.items().keys)
                    ResourceType.LAYOUT -> addMatches(resources.layouts.items().keys)

                    ResourceType.SCENE -> addFiles(resources.sceneDirectory, listOf("scene"))
                    ResourceType.SCRIPT -> addFiles(resources.scriptDirectory(), listOf("feather"))

                    else -> Unit
                }

            }

            if (resourceType == ResourceType.ANY || resourceType == ResourceType.API_DOCUMENTATION) {
                // Now look for a "java" class (not a script)
                ClassMetaData.simpleClassNameToNames.forEach { (simpleName, qualifiedName) ->
                    val score = matchScore(simpleName)
                    if (score != Double.MAX_VALUE) {
                        list.add(MatchedResource(qualifiedName.first(), ResourceType.API_DOCUMENTATION, score))
                    }
                }
            }
        }

        contextMenu.items.clear()
        list.sortedBy { it.score }.forEach { (name, resourceType) ->
            val item = MenuItem(name, resourceType.createImageView())
            item.onAction = EventHandler { selectItem(name, resourceType) }
            item.graphic?.let { Tooltip.install(it, Tooltip(resourceType.label)) }
            contextMenu.items.add(item)
        }

        if (list.isNotEmpty()) {
            // Grr, I have to put the menu on the right, because it seems that context menus cannot be set to a
            // minimum size, and therefore a long menu would obscure the text field when using Side.BOTTOM or TOP.
            contextMenu.show(resourceTypeChoice, Side.RIGHT, 0.0, 0.0)
        }
    }

    private fun selectItem(name: String, resourceType: ResourceType) {
        MainWindow.instance.openNamedTab(name, resourceType)
        hide()
    }

    data class MatchedResource(val name: String, val resourceType: ResourceType, val score: Double)
}
