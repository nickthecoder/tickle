package uk.co.nickthecoder.tickle.editor.parameters

import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.ContextMenu
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.util.StringConverter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.parameters.fields.ParameterField
import uk.co.nickthecoder.paratask.util.uncamel
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.tabs.SceneTab

class ActorNameParameter(

    name: String,
    label: String = name.uncamel(),
    value: String = "",
    description: String = ""

) : CompoundParameter<String>(name, label, description) {

    val stringP = StringParameter("${name}_string", label = "", columns = 10)
    val selectP = ActorNameSelectorButton("${name}_select", stringP)
    val goP = ButtonParameter("${name}_go", buttonText = "->", label = "") {
        (MainWindow.instance.tabPane.selectionModel.selectedItem as? SceneTab)?.let { sceneTab ->
            sceneTab.sceneEditor.findActorByName(stringP.value)?.let { actor ->
                sceneTab.sceneEditor.sceneView.selection.clearAndSelect(actor)
            }
        }
    }

    override val converter = object : StringConverter<String>() {
        override fun fromString(string: String) = string

        override fun toString(obj: String) = obj
    }

    override var value: String
        get() = stringP.value
        set(value) {
            stringP.value = value
        }

    override fun copy() = ActorNameParameter(name, label, value, description)

    init {
        this.value = value
        asHorizontal()
        addParameters(stringP, selectP, goP)
    }

}

class ActorNameSelectorButton(
    override val name: String,
    val stringParameter: StringParameter
) : AbstractParameter(name, "", "", "", false) {

    override fun createField(): ParameterField {
        return object : ParameterField(this) {
            override fun createControl(): Node {
                return Button("...").apply {
                    onAction = EventHandler {
                        popupMenu(this)
                    }
                }
            }
        }.build()
    }

    private fun popupMenu(button: Button) {
        val contextMenu = ContextMenu()

        (MainWindow.instance.tabPane.selectionModel.selectedItem as? SceneTab)?.let { sceneTab ->
            for (es in sceneTab.sceneEditor.editingStages) {
                val subMenu = Menu(es.stageName)
                for (actor in es.stage.actors) {

                    if (actor.name != "") {
                        subMenu.items.add(MenuItem(actor.name).apply {
                            onAction = EventHandler {
                                stringParameter.value = actor.name
                            }
                        })
                    }
                }
                if (subMenu.items.isNotEmpty()) {
                    contextMenu.items.add(subMenu)
                }
            }
            // If there is only one stage, then there is no need for sub-menus.
            if (contextMenu.items.size == 1) {
                val subMenu = (contextMenu.items[0] as Menu)
                val items = subMenu.items.toList()
                contextMenu.items.clear()
                contextMenu.items.addAll(items)
            }
        }

        if (contextMenu.items.isEmpty()) {
            contextMenu.items.add(MenuItem("No named actors").apply {
                isDisable = true
            })
        }
        contextMenu.show(button, Side.RIGHT, 0.0, 0.0)
    }

    override fun errorMessage(): String? = null
    override fun isStretchy() = false
    override fun copy() = throw NotImplementedError()
}
