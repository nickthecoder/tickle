package uk.co.nickthecoder.tickle.editor.parameters

import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.ContextMenu
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.util.StringConverter
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.parameters.fields.ParameterField
import uk.co.nickthecoder.paratask.util.FileLister
import uk.co.nickthecoder.paratask.util.uncamel
import uk.co.nickthecoder.tickle.editor.Editor
import java.io.File

class SceneNameParameter(

    name: String,
    label: String = name.uncamel(),
    value: String = "",
    description: String = ""

) : CompoundParameter<String>(name, label, description) {

    val stringP = StringParameter("${name}_string", label = "", columns = 10)
    val selectP = SceneSelectorButton(stringP, "${name}_select")

    override val converter = object : StringConverter<String>() {
        override fun fromString(string: String) = string

        override fun toString(obj: String) = obj
    }

    override var value: String
        get() = stringP.value
        set(value) {
            stringP.value = value
        }

    override fun copy() = SceneNameParameter(name, label, value, description)

    init {
        this.value = value
        asHorizontal()
        addParameters(stringP, selectP)
    }

}

class SceneSelectorButton(
    val stringParameter: StringParameter,
    override val name: String
) : AbstractParameter(name, "", "", "", false) {

    override fun createField(): ParameterField {
        return object : ParameterField(this) {
            override fun createControl(): Node {
                return Button("...").apply {
                    onAction = EventHandler {
                        popupMenu(this)
                    }
                }
            }
        }.build()
    }

    private fun popupMenu(button: Button) {
        val resources = Editor.resources
        val dir = resources.sceneDirectory.absoluteFile
        val contextMenu = ContextMenu()

        fun addItems(prefix: String, items: ObservableList<MenuItem>, dir: File) {
            for (subDir in FileLister(depth = 1, onlyFiles = false).listFiles(dir)) {
                val subMenu = Menu(subDir.name)
                addItems("${prefix}${subDir.name}/", subMenu.items, subDir)
                if (subMenu.items.isNotEmpty()) {
                    items.add(subMenu)
                }
            }
            for (file in FileLister(depth = 1, onlyFiles = true, extensions = listOf("scene")).listFiles(dir)) {
                items.add(MenuItem(file.nameWithoutExtension).apply {
                    onAction = EventHandler { stringParameter.value = "${prefix}${file.nameWithoutExtension}" }
                })
            }
        }

        addItems("", contextMenu.items, dir)
        contextMenu.show(button, Side.RIGHT, 0.0, 0.0)
    }

    override fun errorMessage(): String? = null
    override fun isStretchy() = false
    override fun copy() = throw NotImplementedError()
}
