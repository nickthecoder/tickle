/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene

import javafx.beans.property.SimpleBooleanProperty
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.BooleanParameter
import uk.co.nickthecoder.paratask.parameters.DoubleParameter

class SnapRotation : Snap {

    override val enabledProperty = SimpleBooleanProperty(true)

    var stepDegrees = 90.0

    var closeness = 5.0

    var roundWhenNotSnapping = true

    fun snapRotation(degrees: Double, snap: Boolean): Double {
        if (!snap || !enabledProperty.value) {
            return if (roundWhenNotSnapping) {
                Math.round(degrees).toDouble()
            } else {
                degrees
            }
        }

        var remainder = degrees.rem(stepDegrees)
        if (remainder > stepDegrees / 2) {
            remainder -= stepDegrees
        } else if (remainder < -stepDegrees/2) {
            remainder += stepDegrees
        }

        return if (Math.abs(remainder) < closeness) {
            degrees - remainder
        } else {
            if (roundWhenNotSnapping) {
                Math.round(degrees).toDouble()
            } else {
                degrees
            }
        }
    }

    override fun task() = SnapRotationTask()


    inner class SnapRotationTask() : AbstractTask() {

        val enabledP = BooleanParameter("enabled", value = enabledProperty.value)

        val stepP = DoubleParameter("step", value = stepDegrees)

        val closenessP = DoubleParameter("closeness", value = closeness)

        val integerDegreesWhenDisabledP =
            BooleanParameter("roundWhenNotSnapping", value = roundWhenNotSnapping)

        override val taskD = TaskDescription("editGrid")
            .addParameters(enabledP, integerDegreesWhenDisabledP, stepP, closenessP)

        override fun run() {
            enabledProperty.value = enabledP.value!!
            roundWhenNotSnapping = integerDegreesWhenDisabledP.value == true
            stepDegrees = stepP.value!!
            closeness = closenessP.value!!
        }
    }

}
