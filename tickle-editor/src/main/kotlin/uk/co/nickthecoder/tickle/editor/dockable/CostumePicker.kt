/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.dockable

import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.layout.FlowPane
import javafx.scene.layout.VBox
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.CostumeGroup
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.editor.DOCKABLE_COSTUME_PICKER
import uk.co.nickthecoder.tickle.editor.Editor
import uk.co.nickthecoder.tickle.editor.MainWindow
import uk.co.nickthecoder.tickle.editor.resources.ScriptStub
import uk.co.nickthecoder.tickle.editor.tabs.SceneTab
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences
import uk.co.nickthecoder.tickle.editor.util.thumbnail
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.ResourceMap
import uk.co.nickthecoder.tickle.resources.ResourcesListener
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.sound.Sound

/**
 * Shows buttons with thumbnail images of a Costume's default pose. Click the button to then "stamp" it into the scene.
 */
class CostumePicker

    : TickleDockable(DOCKABLE_COSTUME_PICKER),
    ResourcesListener {

    val vbox = VBox()

    val scrollPane = ScrollPane(vbox)

    override val dockableContent = SimpleObjectProperty<Node>(scrollPane)

    init {
        build()
        scrollPane.isFitToWidth = true
        Editor.resources.listeners.add(this)
    }

    private fun build() {
        vbox.children.clear()
        vbox.children.add(buildGroup(Editor.resources.costumes, false))

        Editor.resources.costumeGroups.items().entries.sortedWith(CGComparator()).forEach { entry ->
            val groupName = entry.key
            val costumeGroup = entry.value
            if (costumeGroup.showInSceneEditor) {
                val pane = TitledPane(groupName, buildGroup(costumeGroup, true))
                pane.styleClass.add("pickGroup")
                pane.isAnimated = false // The animation is too slow, and there's no API to change the speed. Grr.
                pane.isExpanded = costumeGroup.initiallyExpanded
                vbox.children.add(pane)
            }
        }
    }


    override fun resourcesReloaded() {
        Platform.runLater {
            build()
        }
    }

    private fun resourceEvent(resource: Any) {
        if (resource is Costume || resource is CostumeGroup) {
            Platform.runLater {
                build()
            }
        }
    }

    override fun resourceAdded(resource: Any, name: String) {
        resourceEvent(resource)
    }

    override fun resourceRemoved(resource: Any, name: String) {
        resourceEvent(resource)
    }

    override fun resourceChanged(resource: Any) {
        resourceEvent(resource)
    }

    override fun resourceRenamed(resource: Any, oldName: String, newName: String) {
        resourceEvent(resource)
    }

    private class CGComparator : Comparator<Map.Entry<String, CostumeGroup>> {
        override fun compare(o1: Map.Entry<String, CostumeGroup>, o2: Map.Entry<String, CostumeGroup>): Int {
            return if (o1.value.sortOrder == o2.value.sortOrder) {
                o1.key.compareTo(o2.key)
            } else {
                o1.value.sortOrder - o2.value.sortOrder
            }
        }
    }

    private class CostumeComparator : Comparator<Map.Entry<String, Costume>> {
        override fun compare(o1: Map.Entry<String, Costume>, o2: Map.Entry<String, Costume>): Int {
            //return if (o1.value.sortOrder == o2.value.sortOrder) {
            return o1.key.compareTo(o2.key)
            //} else {
            //    o1.value.sortOrder - o2.value.sortOrder
            //}
        }
    }

    private fun buildGroup(group: ResourceMap<Costume>, all: Boolean): Node {

        val poseButtons = FlowPane()
        val textButtons = VBox()
        val result = VBox()

        result.children.addAll(poseButtons, textButtons)

        poseButtons.styleClass.add("pickPose")
        textButtons.styleClass.add("pickText")
        textButtons.isFillWidth = false

        group.items().entries.sortedWith(CostumeComparator()).forEach { entry ->
            val costumeName = entry.key
            val costume = entry.value

            if (costume.showInSceneEditor && (all || Editor.resources.findCostumeGroup(costumeName) == null)) {
                val pose = costume.editorPose()

                if (pose == null) {

                    if (costume.chooseTextStyle(costume.initialEventName) != null) {
                        textButtons.children.add(
                            createButton(
                                costumeName,
                                costume,
                                ImageView(Editor.imageResource("font.png")),
                                true
                            )
                        )
                    }

                } else {
                    pose.thumbnail(EditorPreferences.costumePickerThumbnailSize).let { iv ->
                        poseButtons.children.add(createButton(costumeName, costume, iv, false))
                    }
                }
            }
        }
        return result
    }

    private fun createButton(costumeName: String, costume: Costume, icon: Node, isFont: Boolean): Button {
        val button = Button()
        val roleName = costume.roleString.split(".").lastOrNull()

        val name = if (!roleName.isNullOrBlank() && roleName.lowercase() != costumeName.lowercase()) {
            "$costumeName ($roleName)"
        } else {
            costumeName
        }

        if (isFont) {
            button.text = name
            button.maxWidth = Double.MAX_VALUE
        } else {
            val size = EditorPreferences.costumePickerThumbnailSize + 4.0 // 2 pixel padding
            button.prefWidth = size
            button.prefHeight = size
            button.tooltip = Tooltip(name)
        }
        button.graphic = icon
        button.onAction = EventHandler {
            val tab = MainWindow.instance.tabPane.selectionModel.selectedItem
            if (tab is SceneTab) {
                tab.selectCostume(costume)
            }
        }

        button.onMouseClicked = EventHandler { event ->
            if (event.clickCount == 2) {
                MainWindow.instance.openTab(costumeName, costume)
            }
        }

        button.onContextMenuRequested = EventHandler { event ->
            createContextMenu(costumeName, costume).show(button, event.screenX, event.screenY)
            event.consume()
        }

        return button
    }

    private fun createContextMenu(costumeName: String, costume: Costume): ContextMenu {

        val menu = ContextMenu()

        val editCostume = MenuItem("Edit Costume").apply {
            onAction = EventHandler {
                MainWindow.instance.openTab(costumeName, costume)
            }
        }
        menu.items.add(editCostume)

        val scriptFile = ScriptManager.scriptFile(costume.roleString)
        if (scriptFile != null) {
            val scriptMenu = MenuItem("Edit Script").apply {
                onAction = EventHandler {
                    MainWindow.instance.openTab(costume.roleString, ScriptStub(scriptFile))
                }
            }
            menu.items.add(scriptMenu)
        }

        fun createPoseMenuItem(pose: Pose, only: Boolean): MenuItem {
            val poseName = Editor.resources.poses.findName(pose) ?: "Pose"
            val menuItem = MenuItem(if (only) "Edit Pose" else poseName)
            menuItem.onAction = EventHandler { MainWindow.instance.openTab(poseName, pose) }
            return menuItem
        }

        fun createTextureMenuItem(texture: Texture, only: Boolean): MenuItem {
            val textureName = Editor.resources.textures.findName(texture) ?: "Texture"
            val menuItem = MenuItem(if (only) "Edit Texture" else textureName)
            menuItem.onAction = EventHandler { MainWindow.instance.openTab(textureName, texture) }
            return menuItem
        }


        val poses = mutableSetOf<Pose>()
        val fonts = mutableSetOf<FontResource>()
        val linkedCostumes = mutableSetOf<Costume>()
        val sounds = mutableSetOf<Sound>()

        costume.events.values.forEach { ev ->
            poses.addAll(ev.poses)
            poses.addAll(ev.ninePatches.map { it.pose })
            fonts.addAll(ev.textStyles.map { it.fontResource })
            linkedCostumes.addAll(ev.costumes)
            sounds.addAll(ev.sounds)
        }

        var addedSeparator = false
        fun addSeparator() {
            if (!addedSeparator) {
                menu.items.add(SeparatorMenuItem())
                addedSeparator = true
            }
        }

        if (poses.size == 1) {
            menu.items.add(createPoseMenuItem(poses.first(), true))
        } else {
            addSeparator()
            val posesMenu = Menu("Poses")
            menu.items.add(posesMenu)
            poses.forEach { pose ->
                posesMenu.items.add(createPoseMenuItem(pose, false))
            }
            posesMenu.items.sortBy { it.text }
        }

        val textures = poses.map { it.texture }.toSet()
        if (textures.size == 1) {
            menu.items.add(createTextureMenuItem(textures.first(), true))
        } else {
            addSeparator()
            val texturesMenu = Menu("Textures")
            menu.items.add(texturesMenu)
            textures.forEach { texture ->
                texturesMenu.items.add(createTextureMenuItem(texture, false))
            }
            texturesMenu.items.sortBy { it.text }
        }

        if (fonts.isNotEmpty()) {
            addSeparator()
            val soundsMenu = Menu("Sounds")
            menu.items.add(soundsMenu)
            sounds.forEach { sound ->
                val soundName = Editor.resources.sounds.findName(sound) ?: "Sound"
                soundsMenu.items.add(MenuItem(soundName).apply {
                    onAction = EventHandler { MainWindow.instance.openTab(soundName, sound) }
                })
            }
        }

        if (linkedCostumes.isNotEmpty()) {
            addSeparator()
            val costumesMenu = Menu("Costume Events")
            menu.items.add(costumesMenu)
            linkedCostumes.forEach { linkedCostume ->
                val linkedCostumeName = Editor.resources.costumes.findName(linkedCostume) ?: "Costume"
                costumesMenu.items.add(MenuItem(linkedCostumeName).apply {
                    onAction = EventHandler { MainWindow.instance.openTab(linkedCostumeName, linkedCostume) }
                })
            }
        }

        return menu
    }
}
