/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor

class ScaleActor(
    private val actor: Actor,
    private val oldX: Float,
    private val oldY: Float,
    private val oldScaleX: Float,
    private val oldScaleY: Float

) : Change {

    private var newX = actor.x
    private var newY = actor.y
    private var newScaleX = actor.scale.x
    private var newScaleY = actor.scale.y

    override fun redo(sceneEditor: SceneEditor) {
        actor.x = newX
        actor.y = newY
        actor.scale.x = newScaleX
        actor.scale.y = newScaleY
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        actor.x = oldX
        actor.y = oldY
        actor.scale.x = oldScaleX
        actor.scale.y = oldScaleY
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun mergeWith(other: Change): Boolean {
        if (other is ScaleActor && other.actor === actor) {
            other.newX = newX
            other.newY = newY
            other.newScaleX = newScaleX
            other.newScaleY = newScaleY
            return true
        }
        return false
    }

}
