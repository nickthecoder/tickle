/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.editor.tabs

import javafx.scene.web.WebView
import uk.co.nickthecoder.fxessentials.ActionGroup
import uk.co.nickthecoder.tickle.editor.WEBVIEW_BACK
import uk.co.nickthecoder.tickle.editor.WEBVIEW_FORWORD
import uk.co.nickthecoder.tickle.editor.dockable.APIDockable
import uk.co.nickthecoder.tickle.editor.dockable.APIStub
import uk.co.nickthecoder.tickle.editor.dockable.APITree
import uk.co.nickthecoder.tickle.editor.resources.ResourceType
import uk.co.nickthecoder.tickle.editor.util.EditorPreferences

/**
 * Shows the API documentation using HTML pages from the web.
 * The main content of the tab contains a [WebView] (which you can think of as a web browser.
 * In addition, a side panel (to the left of the main content) shows a list of packages and classes.
 * This is implemented in the [APIDockable] and [APITree] classes.
 * Clicking on a class or a package from the list updates the [WebView] to show the appropriate content.
 */
class APITab(packageName: String = "uk.co.nickthecoder.tickle", className: String? = null) :
    EditTab("API Documentation", APIStub, ResourceType.API_DOCUMENTATION.createImageView()),
    HasExtras {

    val webView = WebView()
    private val webEngine = webView.engine

    val actionGroup = ActionGroup().apply {
        action(WEBVIEW_BACK) {
            val history = webEngine.history
            val entryList = history.entries
            val currentIndex = history.currentIndex
            if (entryList.size > 1 && currentIndex > 0) {
                history.go(-1)
            }
        }

        action(WEBVIEW_FORWORD) {
            val history = webEngine.history
            val entryList = history.entries
            val currentIndex = history.currentIndex
            if (entryList.size > 1 && currentIndex < entryList.size - 1) {
                history.go(1)
            }
        }

        attachTo(borderPane)
    }

    init {
        println("API TAB url ${classNameToApiUrl(packageName, className)} ")
        webEngine.load(classNameToApiUrl(packageName, className))
        borderPane.center = webView
        okButton.isVisible = false
        applyButton.isVisible = false
        cancelButton.text = "Close"

        with(actionGroup) {
            leftButtons.children.addAll(
                button(WEBVIEW_BACK),
                button(WEBVIEW_FORWORD)
            )
        }

        webEngine.loadWorker.runningProperty().addListener { _, _, newValue ->
            if (!newValue) {
                text = webEngine.title
                    .replace(" - Kotlin Programming Language", "")
                    .replace(" - tickle-core", "")
            }
        }
    }

    fun goto(packageName: String, className: String?) {
        webView.engine.load(classNameToApiUrl(packageName, className))
    }

    override fun justSave() = true

    companion object {

        fun classNameToApiUrl(packageName: String, className: String?): String {

            return if (packageName.startsWith("uk.co.nickthecoder.tickle")) {

                if (EditorPreferences.tickleApiUseJavadocs) {
                    val base = EditorPreferences.tickleJavadocURL
                    if (className == null) {
                        base + encodeJavadocName("$packageName/package-summary.html")
                    } else {
                        base + encodeJavadocName("$packageName.$className") + ".html"
                    }
                } else {
                    val base = EditorPreferences.tickleDokkaURL
                    val p = base + (if (!base.endsWith('/')) "/" else "") + "tickle-core/" + packageName + "/"

                    if (className == null) {
                        p + "index.html"
                    } else {
                        p + encodeKotlinName(className) + "/index.html"
                    }
                }

            } else if (packageName.startsWith("org.joml")) {
                val base = EditorPreferences.jomlApiURL
                base + encodeJavadocName("$packageName.$className") + ".html"

            } else {
                EditorPreferences.tickleDokkaURL
            }

        }

        private fun encodeKotlinName(name: String): String {
            val buffer = StringBuffer()
            name.forEach {
                when {
                    it == '.' -> {
                        buffer.append("/")
                    }

                    it.isUpperCase() -> {
                        buffer.append("-").append(it.lowercase())
                    }

                    else -> {
                        buffer.append(it)
                    }
                }
            }
            return buffer.toString()
        }

        private fun encodeJavadocName(name: String): String {
            return name.replace(".", "/")
        }
    }
}
