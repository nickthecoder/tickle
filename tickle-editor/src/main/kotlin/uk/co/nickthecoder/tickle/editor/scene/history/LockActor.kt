package uk.co.nickthecoder.tickle.editor.scene.history

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.editor.resources.ModificationType
import uk.co.nickthecoder.tickle.editor.scene.SceneEditor

class LockActor(val actor: Actor, val newValue: Boolean) : Change {
    override fun redo(sceneEditor: SceneEditor) {
        actor.isLocked = newValue
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }

    override fun undo(sceneEditor: SceneEditor) {
        actor.isLocked = !newValue
        sceneEditor.fireActorsChanged(listOf(actor), ModificationType.CHANGE)
    }
}
