package uk.co.nickthecoder.tickle.stage

import junit.framework.TestCase

class TestFlexPosition : TestCase() {

    fun testWholeScreen() {
        val flex = FlexPosition().apply {
            widthRatio = 1.0
            heightRatio = 1.0
        }
        assertEquals(640, flex.width(640))
        assertEquals(480, flex.height(480))

        assertEquals(0, flex.left(640))
        assertEquals(640, flex.right(640))
        assertEquals(0, flex.bottom(480))
        assertEquals(480, flex.top(480))
    }

    /**
     * A 100 pixel slice at the top of the screen, using a margin from the bottom.
     */
    fun testTopSlice() {
        val flex = FlexPosition().apply {
            widthRatio = 1.0
            height = 100
            topBottomMargin = 380
            vAlignment = FlexVAlignment.BOTTOM
        }
        assertEquals(640, flex.width(640))
        assertEquals(100, flex.height(480))

        assertEquals(0, flex.left(640))
        assertEquals(640, flex.right(640))
        assertEquals(380, flex.bottom(480))
        assertEquals(480, flex.top(480))
    }

    /**
     * A 100 pixel slice at the top of the screen, using zero margin from the top.
     */
    fun testTopSlice2() {
        val flex = FlexPosition().apply {
            widthRatio = 1.0
            height = 100
            topBottomMargin = 0
            vAlignment = FlexVAlignment.TOP
        }
        assertEquals(640, flex.width(640))
        assertEquals(100, flex.height(480))

        assertEquals(0, flex.left(640))
        assertEquals(640, flex.right(640))
        assertEquals(380, flex.bottom(480))
        assertEquals(480, flex.top(480))
    }

    fun testBottomSlice() {
        val flex = FlexPosition().apply {
            widthRatio = 1.0
            height = 100
            topBottomMargin = 0
            vAlignment = FlexVAlignment.BOTTOM
        }
        assertEquals(640, flex.width(640))
        assertEquals(100, flex.height(480))

        assertEquals(0, flex.left(640))
        assertEquals(640, flex.right(640))
        assertEquals(0, flex.bottom(480))
        assertEquals(100, flex.top(480))
    }

    fun testBottomSlice2() {
        val flex = FlexPosition().apply {
            widthRatio = 1.0
            height = 100
            topBottomMargin = 380
            vAlignment = FlexVAlignment.TOP
        }
        assertEquals(640, flex.width(640))
        assertEquals(100, flex.height(480))

        assertEquals(0, flex.left(640))
        assertEquals(640, flex.right(640))
        assertEquals(0, flex.bottom(480))
        assertEquals(100, flex.top(480))
    }
}
