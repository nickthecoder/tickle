package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.action.Eases
import uk.co.nickthecoder.tickle.events.ButtonState
import uk.co.nickthecoder.tickle.events.Key
import uk.co.nickthecoder.tickle.events.KeyEvent
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.util.Vector2

class PlayVisibleShape : PlayMatrix() {

    val path = Path(0f, -250f).apply {
        lineBy(50f, 30f)
        lineBy(0f, 500f)
        circularArcBy(Vector2(100f, 100f), 100f, largeArc = false, sweep = false)
        lineBy(100f, 0f)
        lineBy(0f, -300f)
    }

    val gradient = Gradient(Colors.RED, Colors.BLUE, ease = Eases.easeInOut).apply {
        addStop(0.5f, Colors.WHITE)
    }
    var strokePaint: StrokePaint = GradientAcross(gradient)

    var endCap = CapStyle.SQUARE
    var joinStyle = JoinStyle.MITER

    val blackPath = VisibleShape(path, 50f, capStyle = endCap, strokePaint = FlatColor(Color.black()))
    val innerPath = VisibleShape(path, 40f, capStyle = endCap, strokePaint = strokePaint)

    var antiAlias = true

    init {
        println("A : Toggle Anti-Alias")
        println("E : Change EndCapStyle")
        println("G : Toggle Gradient Along/Across")
        println("J : Change JointStyle")


        background.set(Color.white())
    }

    override fun draw() {
        if (turnBy != 0f) {
            angle += turnBy
            redoMatrix()
        }
        if (moveBy.x != 0f || moveBy.y != 0f) {
            translation += moveBy
            redoMatrix()
        }

        with(Renderer.instance()) {
            if (antiAlias) {
                strokeAntiAlias(blackPath, zoom, modelMatrix)
                strokeAntiAlias(innerPath, zoom, modelMatrix)
            } else {
                stroke(blackPath, modelMatrix)
                stroke(innerPath, modelMatrix)
            }
        }
    }

    override fun onKey(event: KeyEvent) {
        super.onKey(event)

        if (event.state == ButtonState.PRESSED) {
            if (event.key == Key.E) {
                endCap = when (endCap) {
                    CapStyle.SQUARE -> CapStyle.BUTT
                    CapStyle.BUTT -> CapStyle.ROUND
                    CapStyle.ROUND -> CapStyle.SQUARE
                }
                blackPath.capStyle = endCap
                innerPath.capStyle = endCap
            }

            if (event.key == Key.J) {
                joinStyle = when (joinStyle) {
                    JoinStyle.MITER -> JoinStyle.BEVEL
                    JoinStyle.BEVEL -> JoinStyle.ROUND
                    JoinStyle.ROUND -> JoinStyle.MITER
                }
                blackPath.joinStyle = joinStyle
                innerPath.joinStyle = joinStyle
            }

            if (event.key == Key.A) {
                antiAlias = !antiAlias
                println("Anti-Alias : $antiAlias")
            }

            if (event.key == Key.G) {

                var sp = strokePaint
                sp = when (sp) {
                    is GradientAcross -> GradientAlong(gradient, sp.isMirrored)
                    is GradientAlong -> GradientAcross(gradient, sp.isMirrored)
                    else -> sp
                }
                strokePaint = sp
                innerPath.strokePaint = sp
            }

            if (event.key == Key.M) {
                var sp = strokePaint
                if (sp is GradientAcross) {
                    sp = GradientAcross(gradient, !sp.isMirrored)
                }
                if (sp is GradientAlong) {
                    sp = GradientAlong(gradient, !sp.isMirrored)
                }
                strokePaint = sp
                innerPath.strokePaint = sp
            }
        }
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Play.run { PlayVisibleShape() }
        }
    }
}
