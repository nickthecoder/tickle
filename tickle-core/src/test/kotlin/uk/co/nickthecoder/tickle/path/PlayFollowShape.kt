package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.Clock
import uk.co.nickthecoder.tickle.action.Delay
import uk.co.nickthecoder.tickle.action.movement.FollowShape
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Colors
import uk.co.nickthecoder.tickle.graphics.FlatColor
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * The white circle [visibleBall1] follows the [path].
 * The grey circle [visibleBall2] follows the same route, but using an approximate [Polyline].
 * It is delayed by 1 second.
 *
 * In both circles should travel at constant speed.
 * Note [Bezier] curves do not currently do this correctly, so the white circle will change speed
 * when the bezier curves have sharp curves (this is a known bug!).
 *
 * As gray's route is an approximation, the distance between the two may drift ever so slightly,
 * but as 1 circuit is 8 seconds for both, they should not get out of sync, even when left running for ages.
 *
 * The Azure (light blue) ball uses a different mechanism to update, but should still follow the path at the
 * same speed. This technique is appropriate if you want to speed up / slow down. (which I'm not doing here).
 *
 * NOTE, it is possible for ball3 to catch up / lose ground to the other two balls over long periods of time,
 * due to accumulated rounding errors. In this demo it catches up.
 */
class PlayFollowShape : Play() {

    val ball = EllipticalArc.circle(Vector2(600f, 400f), 40f).apply { joinEnd(start) }
    val visibleBall1 = VisibleShape(ball, thickness = 20f)
    val visibleBall2 = VisibleShape(ball, thickness = 20f, strokePaint = FlatColor(Colors.GRAY))
    val visibleBall3 = VisibleShape(ball, thickness = 20f, strokePaint = FlatColor(Colors.AZURE))

    val path = Path(100f, 100f).apply {
        lineBy(0f, 400f)
        lineBy(100f, 0f)
        bezierBy(Vector2(-100f, 0f), Vector2(300f, -300f))
        circularArcBy(Vector2(100f, 100f), 100f, false, true)
        lineBy(0f, 50f)
        circularArcBy(Vector2(0f, 0f), 200f, true, false)
        lineBy(0f, 300f)
        circularArcBy(Vector2(0f, -1f), 100f, true, false)
        lineBy(0f, 200f)
        // Check that small lines keep constant speed.
        for (i in 1..50) {
            lineBy(-8f, 0f)
        }
        lineTo(100f, 100f)
    }.apply { joinEnd(start) }

    val polyline = path.toPolyline()

    val visiblePath = VisibleShape(polyline, 2f, strokePaint = FlatColor(Colors.GRAY))

    val ball1Point = Vector2()
    val ball2Point = Vector2()
    var ball3Distance = -100f // Start ball3 behind the other two.

    val secondsPerLap = 10f

    val action1 = FollowShape(ball1Point, secondsPerLap, path).forever()
    val action2 = Delay(0.1).then(FollowShape(ball2Point, secondsPerLap, polyline).forever())

    /**
     * The offset so that the center of the balls are on the path.
     * Without this, the "east" edge of the ball would follow the path.
     */
    val offset = Vector2(ball.radius.x, 0f)

    init {
        Clock.gameStartSeconds = System.nanoTime() / 1_000_000_000.0
        background.set(Color.black())
        action1.begin()
        action2.begin()
    }

    override fun draw() {
        val oldSeconds = Clock.seconds
        // The actions use Clock.seconds (i.e. ball1 and ball2)
        // As we aren't running a Game, we need to update the Clock manually :-(
        Clock.seconds = System.nanoTime() / 1_000_000_000.0 - Clock.gameStartSeconds
        // The time step in seconds for this frame (used by ball3 only)
        val deltaTime = Clock.seconds - oldSeconds

        // Change the position of each of the balls.
        // ball1 and ball2 use Actions, ball3 uses "old-fashioned" addition of distance
        // based on the time step for this frame.
        action1.act()
        action2.act()
        val speed = polyline.length() / secondsPerLap * deltaTime.toFloat() // In world units per tick.
        ball3Distance += speed

        visibleBall1.shape = ball.moveTo(ball1Point - offset)
        visibleBall2.shape = ball.moveTo(ball2Point - offset)
        visibleBall3.shape = ball.moveTo(polyline.alongDistance(ball3Distance) - offset)

        // Draw...

        with(Renderer.instance()) {
            stroke(visiblePath)
            stroke(visibleBall1)
            stroke(visibleBall2)
            stroke(visibleBall3)
        }
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Play.run { PlayFollowShape() }
        }
    }
}