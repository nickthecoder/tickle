package uk.co.nickthecoder.tickle.stage

import junit.framework.TestCase
import uk.co.nickthecoder.tickle.Clock
import uk.co.nickthecoder.tickle.action.movement.Elliptical
import uk.co.nickthecoder.tickle.action.movement.EllipticalArcBy
import uk.co.nickthecoder.tickle.util.Vector2

class EllipticalArcBaseTest : TestCase() {

    /**
     * Move in a circle in 4 time jumps, so that position forms a diamond.
     * This hits the "special case" code, because the offset is zero.
     */
    fun testEllipticalArcBy() {
        val position = Vector2(1f, 0f)
        Clock.seconds = 0.0
        val circle = EllipticalArcBy(position, 4f, Vector2(0f, 0f), Vector2(1f, 2f), largeArc = true, sweepArc = false)

        // action hasn't ended, and the position hasn't changed
        assertFalse(circle.begin())
        assertAlmostEquals(Vector2(1f, 0f), position)

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(1f, 0f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(0f, 2f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(-1f, 0f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(0f, -2f), position)
        Clock.seconds++

        assertTrue(circle.act())
        assertAlmostEquals(Vector2(1f, 0f), position)

    }

    /**
     * Does not hit the special-case code.
     * Quarter of an ellipse, step by 45°
     */
    fun testEllipticalArcByQuarter() {
        val position = Vector2(1f, 0f)
        Clock.seconds = 0.0
        val circle = EllipticalArcBy(position, 2f, Vector2(-1f, 2f), Vector2(1f, 2f), largeArc = false, sweepArc = true)
        val root2 = Math.sqrt(2.0).toFloat()

        // action hasn't ended, and the position hasn't changed
        assertFalse(circle.begin())
        assertAlmostEquals(Vector2(1f, 0f), position)

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(1f, 0f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(1 / root2, root2), position)
        Clock.seconds++

        assertTrue(circle.act())
        assertAlmostEquals(Vector2(0f, 2f), position)

    }

    fun testEllipticalArcByQuarterWithOffset() {
        val position = Vector2(11f, 20f)
        Clock.seconds = 0.0
        val circle = EllipticalArcBy(position, 2f, Vector2(-1f, 2f), Vector2(1f, 2f), largeArc = false, sweepArc = true)
        val root2 = Math.sqrt(2.0).toFloat()

        // action hasn't ended, and the position hasn't changed
        assertFalse(circle.begin())
        assertAlmostEquals(Vector2(11f, 20f), position)

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(11f, 20f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(10 + 1 / root2, 20 + root2), position)
        Clock.seconds++

        assertTrue(circle.act())
        assertAlmostEquals(Vector2(10f, 22f), position)

    }

    /**
     * Like [testEllipticalArcBy] but not centered at the origin.
     * This hits the "special case" code, because the offset is zero.
     */
    fun testEllipticalArcByWithOffset() {
        val position = Vector2(11f, 20f)
        Clock.seconds = 0.0
        val circle = EllipticalArcBy(position, 4f, Vector2(0f, 0f), Vector2(1f, 2f), largeArc = true, sweepArc = false)

        // action hasn't ended, and the position hasn't changed
        assertFalse(circle.begin())
        assertAlmostEquals(Vector2(11f, 20f), position)

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(11f, 20f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(10f, 22f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(9f, 20f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(10f, 18f), position)
        Clock.seconds++

        assertTrue(circle.act())
        assertAlmostEquals(Vector2(11f, 20f), position)

    }

    /**
     * When the time slightly exceeds the limit, the final position is still exactly at the end point.
     */
    fun testEllipticalArcByOvershoot() {
        val position = Vector2(1f, 0f)
        Clock.seconds = 0.0
        val circle = EllipticalArcBy(position, 4f, Vector2(0f, 0f), Vector2(1f, 1f), largeArc = true, sweepArc = false)
        assertFalse(circle.begin())

        // Exactly half way round
        Clock.seconds = 2.0
        assertFalse(circle.act())
        assertAlmostEquals(Vector2(-1f, 0f), position)

        Clock.seconds = 4.1 // Just over the allotted 4 seconds
        assertTrue(circle.act())
        // But we should be BANG ON the end point.
        assertAlmostEquals(Vector2(1f, 0f), position)

    }

    fun testEllipse() {
        val position = Vector2(1f, 0f)
        Clock.seconds = 0.0
        val circle = Elliptical(position, 4f, Vector2(1f, 2f))

        // action hasn't ended, and the position hasn't changed
        assertFalse(circle.begin())
        assertAlmostEquals(Vector2(1f, 0f), position)

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(1f, 0f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(0f, 2f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(-1f, 0f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(0f, -2f), position)
        Clock.seconds++

        assertTrue(circle.act())
        assertAlmostEquals(Vector2(1f, 0f), position)
    }

    /**
     * Like [testEllipticalArcBy] but not centered at the origin.
     */
    fun testEllipseWithOffset() {
        val position = Vector2(11f, 20f)
        Clock.seconds = 0.0
        val circle = Elliptical(position, 4f, Vector2(1f, 2f))

        // action hasn't ended, and the position hasn't changed
        assertFalse(circle.begin())
        assertAlmostEquals(Vector2(11f, 20f), position)

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(11f, 20f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(10f, 22f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(9f, 20f), position)
        Clock.seconds++

        assertFalse(circle.act())
        assertAlmostEquals(Vector2(10f, 18f), position)
        Clock.seconds++

        assertTrue(circle.act())
        assertAlmostEquals(Vector2(11f, 20f), position)

    }

}
