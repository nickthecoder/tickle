package uk.co.nickthecoder.tickle.stage

import junit.framework.TestCase
import uk.co.nickthecoder.tickle.AbstractAppearance
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.util.Vector2

class TestAppearance(actor: Actor) : AbstractAppearance(actor) {
    override val directionRadians = 0.0
    override fun draw(renderer: Renderer) {}
    override fun width() = 100f
    override fun height() = 100f
    override fun offsetX() = 50f
    override fun offsetY() = 50f
    override fun touching(point: Vector2) = false
}

class TestWrappedStage : TestCase() {

    var gameStage: Stage = GameStage()
    var view = WrappedStageView()
    val costume = Costume(Resources())

    fun createStageAndView() {
        gameStage = GameStage()
        view = WrappedStageView().apply {
            joinLeft = 0f
            joinBottom = 0f
            joinRight = 800f
            joinTop = 600f
        }
        gameStage.addView(view)
    }

    fun createActor(ix : Float, iy : Float ) = Actor(costume).apply {
        gameStage.add(this)
        appearance = TestAppearance(this)
        x = ix
        y = iy
    }

    fun testCenter() {

        createStageAndView()
        val actor = createActor(400f, 300f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            count ++
        }
        assertEquals(1, count)
        assertEquals( 400f, actor.x)
        assertEquals( 300f, actor.y)

    }

    fun testLeftEdge() {

        createStageAndView()
        val actor = createActor(10f, 300f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( 810f, actor.x)
                assertEquals( 300f, actor.y)
            } else {
                assertEquals( 10f, actor.x)
                assertEquals( 300f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 10f, actor.x)
        assertEquals( 300f, actor.y)

    }

    fun testLeftEdge2() {

        createStageAndView()
        val actor = createActor(-10f, 300f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( -10f, actor.x)
                assertEquals( 300f, actor.y)
            } else {
                assertEquals( 790f, actor.x)
                assertEquals( 300f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 790f, actor.x)
        assertEquals( 300f, actor.y)

    }

    fun testRightEdge() {

        createStageAndView()
        val actor = createActor(790f, 300f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( -10f, actor.x)
                assertEquals( 300f, actor.y)
            } else {
                assertEquals( 790f, actor.x)
                assertEquals( 300f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 790f, actor.x)
        assertEquals( 300f, actor.y)

    }
    fun testRightEdge2() {

        createStageAndView()
        val actor = createActor(810f, 300f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( 810f, actor.x)
                assertEquals( 300f, actor.y)
            } else {
                assertEquals( 10f, actor.x)
                assertEquals( 300f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 10f, actor.x)
        assertEquals( 300f, actor.y)

    }



    fun testBottomEdge() {

        createStageAndView()
        val actor = createActor(400f, 10f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( 400f, actor.x)
                assertEquals( 610f, actor.y)
            } else {
                assertEquals( 400f, actor.x)
                assertEquals( 10f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 400f, actor.x)
        assertEquals( 10f, actor.y)

    }

    fun testBottomEdge2() {

        createStageAndView()
        val actor = createActor(400f, -10f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( 400f, actor.x)
                assertEquals( -10f, actor.y)
            } else {
                assertEquals( 400f, actor.x)
                assertEquals( 590f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 400f, actor.x)
        assertEquals( 590f, actor.y)

    }


    fun testTopEdge() {

        createStageAndView()
        val actor = createActor(400f, 590f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( 400f, actor.x)
                assertEquals( -10f, actor.y)
            } else {
                assertEquals( 400f, actor.x)
                assertEquals( 590f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 400f, actor.x)
        assertEquals( 590f, actor.y)

    }

    fun testTopEdge2() {

        createStageAndView()
        val actor = createActor(400f, 610f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            if (count == 0) {
                assertEquals( 400f, actor.x)
                assertEquals( 610f, actor.y)
            } else {
                assertEquals( 400f, actor.x)
                assertEquals( 10f, actor.y)
            }

            count ++
        }
        assertEquals(2, count)
        assertEquals( 400.0, actor.x)
        assertEquals( 10.0, actor.y)

    }

    fun testCorner() {

        createStageAndView()
        val actor = createActor(10f, 10f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            when (count) {
                0 -> {
                    assertEquals( 810f, actor.x)
                    assertEquals( 610f, actor.y)
                }
                1 -> {
                    assertEquals( 10f, actor.x)
                    assertEquals( 610f, actor.y)
                }
                2 -> {
                    assertEquals( 810f, actor.x)
                    assertEquals( 10f, actor.y)
                }
                3 -> {
                    assertEquals( 10f, actor.x)
                    assertEquals( 10f, actor.y)
                }
            }

            count ++
        }
        assertEquals(4, count)
        assertEquals( 10f, actor.x)
        assertEquals( 10f, actor.y)

    }

    fun testCorner2() {

        createStageAndView()
        val actor = createActor(-10f, -10f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            when (count) {
                0 -> {
                    assertEquals( -10f, actor.x)
                    assertEquals( -10f, actor.y)
                }
                1 -> {
                    assertEquals( 790f, actor.x)
                    assertEquals( -10f, actor.y)
                }
                2 -> {
                    assertEquals( -10f, actor.x)
                    assertEquals( 590f, actor.y)
                }
                3 -> {
                    assertEquals( 790f, actor.x)
                    assertEquals( 590f, actor.y)
                }
            }

            count ++
        }
        assertEquals(4, count)
        assertEquals( 790f, actor.x)
        assertEquals( 590f, actor.y)

    }

    fun testCorner3() {

        createStageAndView()
        val actor = createActor(10f, 10f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            when (count) {
                0 -> {
                    assertEquals( 810f, actor.x)
                    assertEquals( 610f, actor.y)
                }
                1 -> {
                    assertEquals( 10f, actor.x)
                    assertEquals( 610f, actor.y)
                }
                2 -> {
                    assertEquals( 810f, actor.x)
                    assertEquals( 10f, actor.y)
                }
                3 -> {
                    assertEquals( 10f, actor.x)
                    assertEquals( 10f, actor.y)
                }
            }

            count ++
        }
        assertEquals(4, count)
        assertEquals( 10f, actor.x)
        assertEquals( 10f, actor.y)

    }


    fun testCorner4() {

        createStageAndView()
        val actor = createActor(-10f, -10f)

        var count = 0
        for ( a in view.actorsToDraw() ) {
            when (count) {
                0 -> {
                    assertEquals( -10f, actor.x)
                    assertEquals( -10f, actor.y)
                }
                1 -> {
                    assertEquals( 790f, actor.x)
                    assertEquals( -10f, actor.y)
                }
                2 -> {
                    assertEquals( -10f, actor.x)
                    assertEquals( 590f, actor.y)
                }
                3 -> {
                    assertEquals( 790f, actor.x)
                    assertEquals( 590f, actor.y)
                }
            }

            count ++
        }
        assertEquals(4, count)
        assertEquals( 790f, actor.x)
        assertEquals( 590f, actor.y)

    }

}
