package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.events.ButtonState
import uk.co.nickthecoder.tickle.events.Key
import uk.co.nickthecoder.tickle.events.KeyEvent
import uk.co.nickthecoder.tickle.graphics.Colors
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.util.Rect
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Quick and dirty visual test of [ContiguousShape.toPolyline] for [LineSegment], [Bezier], [EllipticalArc] and [Path].
 *
 * Arrow keys change the distance and angle tolerances. Hold Shift for finer grained control.
 * NOTE. Ellipse only uses distance tolerance, Bezier uses both.
 */
class PlayPath : Play() {

    var distanceTolerance = 4f
    var angleTolerance = 2.0
    var adjustment = 1.25f

    val lineSegment = LineSegment(Vector2(50f, 50f), Vector2(110f, 400f))

    val bezier = Bezier(
        Vector2(200f, 50f), Vector2(400f, 200f), Vector2(100f, 450f), Vector2(400f, 450f)
    )

    val polyline = Polyline(
        listOf(
            Vector2(200f, 50f), Vector2(400f, 200f), Vector2(100f, 450f), Vector2(400f, 450f)
        )
    ).translate(0f, 500f)

    val ellipticalArc = EllipticalArc(
        Vector2(500f, 150f), Vector2(450f, 600f),
        Vector2(100f, 400f), largeArc = true, sweepArc = true
    )

    // NOTE The button semicircle is actually elliptical, so that we can compare ellipses, with the
    // special case of circles.
    val path = Path(Vector2(730f, 50f)).apply {
        lineBy(60f, 0f)
        bezierBy(Vector2(200f, 0f), Vector2(-200f, 0f), Vector2(0f, 500f))
        lineBy(60f, 0f)
        ellipticalArcBy(Vector2(0f, 200f), Vector2(100f, 99f), largeArc = true, sweepArc = true)
        circularArcBy(Vector2(0f, 180f), 90f, largeArc = false, sweep = false)
    }

    val routes = listOf(bezier, lineSegment, polyline, ellipticalArc, path)
    val polylines = routes.map { it.toPolyline() }
    val meshes = polylines.map { it.createStrokeMesh(60f, JoinStyle.ROUND, CapStyle.ROUND) }

    val path2 = path.translate(250f, 0f)

    val handle = 4f

    fun drawPolyline(polyline: Polyline) {
        with(Renderer.instance()) {
            // Lines between the vertices
            for (i in 0 until polyline.points.size - 1) {
                line(polyline.points[i], polyline.points[i + 1], 2f, Colors.WHITE)
            }
            // Dots at each vertex
            for (point in polyline.points) {
                val rect = Rect(point.x - handle, point.y - handle, point.x + handle, point.y + handle)
                fillRect(rect, Colors.WHITE)
            }
        }
    }

    override fun draw() {
        val path2Polyline = path2.toPolyline(distanceTolerance = distanceTolerance, angleTolerance = angleTolerance)
        val path2Meth = path2Polyline.createStrokeMesh(60f, JoinStyle.ROUND, CapStyle.ROUND)

        with(Renderer.instance()) {
            for (mesh in meshes) {
                fillTriangles(mesh, Colors.BLACK)
            }
            fillTriangles(path2Meth, Colors["#424"])

            for (polyline in polylines) {
                drawPolyline(polyline)
            }
            drawPolyline(path2Polyline)

        }

    }

    override fun onKey(event: KeyEvent) {
        if (event.key == Key.LEFT_SHIFT || event.key == Key.RIGHT_SHIFT) {
            if (event.state == ButtonState.PRESSED) {
                adjustment = 1.05f
            }
            if (event.state == ButtonState.RELEASED) {
                adjustment = 1.25f
            }
        }
        if (event.state == ButtonState.PRESSED) {
            if (event.key == Key.LEFT) {
                distanceTolerance /= adjustment
                println("DistanceTolerance : $distanceTolerance")
            }
            if (event.key == Key.RIGHT) {
                distanceTolerance *= adjustment
                println("DistanceTolerance : $distanceTolerance")
            }
            if (event.key == Key.UP) {
                angleTolerance *= adjustment
                println("AngleTolerance : $angleTolerance")
            }
            if (event.key == Key.DOWN) {
                angleTolerance /= adjustment
                println("AngleTolerance : $angleTolerance")
            }
        }
    }

    companion object {

        @JvmStatic
        fun main(vararg args: String) {
            println("Keys :")
            println("    Up/Down    : Adjust angleTolerance")
            println("    Left/Right : Adjust distanceTolerance")
            Play.run { PlayPath() }
        }
    }
}
