package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Draws an S shaped [Polyline] using all [JoinStyle]s and [CapStyle]s.
 * (9 combinations).
 *
 * Each is drawn 3 times : A black outline, a colored inside, and a black thin line.
 *
 * Press ENTER to toggle between gradient fill ALONG the line, and gradient fill ACROSS the line.
 * Press A to toggle anti-aliasing on/off.
 */
class PlayPolyline : Play() {

    var along = true

    val original = Polyline(
        listOf(
            Vector2(60f, 40f),
            Vector2(100f, 180f),
            Vector2(60f, 310f),
            Vector2(120f, 450f)
        )
    )
    val gradient = Gradient(Colors.RED, Colors.BLUE)
    val black = FlatColor(Color.black())
    var antiAlias = false

    init {
        background.set(Colors.GREY)
    }

    override fun draw() {
        with(Renderer.instance()) {

            var polyline = original
            for (mirrored in listOf(false, true)) {
                for (endCap in CapStyle.values()) {
                    for (js in JoinStyle.values()) {
                        val strokePaint =
                            if (along) GradientAlong(gradient, mirrored) else GradientAcross(gradient, mirrored)
                        val thickOutline = VisibleShape(polyline, 45f, endCap, js, strokePaint)
                        val outline = VisibleShape(polyline, 60f, endCap, js, black)
                        val thin = VisibleShape(polyline.translate(60f,0f), 2f, endCap, js, black)

                        if (antiAlias) {
                            strokeAntiAlias(outline, 1f)
                            strokeAntiAlias(thickOutline, 1f)
                            strokeAntiAlias(thin, 1f)
                        } else {
                            stroke(outline)
                            stroke(thickOutline)
                            stroke(thin)
                        }
                        polyline = polyline.translate(120f, 0f)
                    }
                }
                polyline = original.translate(0f, 500f)
            }

            endView()
        }
    }

    override fun onKey(event: KeyEvent) {
        if (event.state == ButtonState.RELEASED) {
            if (event.key == Key.ENTER) {
                along = !along
            }
            if (event.key == Key.A) {
                antiAlias = !antiAlias
                println("Anti-alias? $antiAlias")
            }
        }
    }

    companion object {

        @JvmStatic
        fun main(vararg args: String) {
            println("Press ENTER to toggle between 'along' and 'across' directions for the gradient.")
            println("Press A to toggle anti-aliasing on/off")
            Play.run { PlayPolyline() }
        }
    }
}
