package uk.co.nickthecoder.tickle.stage

import org.junit.Assert
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * When working with maths, the test cases shouldn't care about rounding errors
 * inerrant with floats!
 * This assumes we are using "world" units, where being off by 0.001 doesn't matter.
 */
fun assertAlmostEquals(message: String?, expected: Float, actual: Float) {
    val diff = expected - actual
    if (diff < -0.001f || diff > 0.001f) {
        Assert.assertEquals(message, expected, actual)
    }
}

fun assertAlmostEquals(expected: Float, actual: Float) {
    assertAlmostEquals(null, expected, actual)
}


fun assertAlmostEquals(message: String?, expected: Vector2, actual: Vector2) {
    var diff = expected.x - actual.x
    if (diff < -0.001f || diff > 0.001f) {
        Assert.assertEquals(message, expected, actual)
    }
    diff = expected.y - actual.y
    if (diff < -0.001f || diff > 0.001f) {
        Assert.assertEquals(message, expected, actual)
    }
}

fun assertAlmostEquals(expected: Vector2, actual: Vector2) {
    assertAlmostEquals(null, expected, actual)
}