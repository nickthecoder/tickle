package uk.co.nickthecoder.tickle.stage

import junit.framework.TestCase
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.TextAppearance
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.util.Button
import uk.co.nickthecoder.tickle.util.MessageButton
import uk.co.nickthecoder.tickle.util.RectInt
import uk.co.nickthecoder.tickle.util.Vector2

class TestStageView : TestCase() {

    val view: StageView
    val stage: GameStage
    val button: Button

    val glWindow: Window

    init {
        OpenGL.makeOpenGLThread()
        glWindow = Window("Test Stage View", 640, 480)

        Resources().apply {
            gameInfo.width = 640
            gameInfo.height = 480
        }
    }

    init {

        stage = GameStage()
        view = ZOrderStageView()
        view.stage = stage
        view.apply {
            rect = RectInt(0, 0, 640, 480)
            //touching = BoundingRectangleTouching.instance
            //overlapping = BoundingRectangleOverlapping.instance
        }

        val font = FontResource()
        val textStyle = TextStyle(font, TextHAlignment.LEFT, TextVAlignment.BOTTOM, Color.white())
        button = MessageButton().apply {
            actor = Actor(Costume(Game.instance.resources), this)
            actor.x = 10f
            actor.y = 10f
            actor.appearance = TextAppearance(actor, "Hello", textStyle)
        }
        stage.add(button.actor)

    }

    fun testWholeScreen() {

        assertEquals(0, view.rect.left)
        assertEquals(640, view.rect.right)
        assertEquals(0, view.rect.bottom)
        assertEquals(480, view.rect.top)

        assertEquals(Vector2(0f, 480f), view.windowToViewport(Vector2(0f, 0f)))
        assertEquals(Vector2(0f, 0f), view.windowToViewport(Vector2(0f, 480f)))

        assertEquals(Vector2(0f, 0f), view.viewportToWorld(Vector2(0f, 0f)))
        assertEquals(Vector2(0f, 480f), view.viewportToWorld(Vector2(0f, 480f)))

        assertTrue(isTouching(Vector2(11f, 480 - 11f), button.actor))
        assertFalse(isTouching(Vector2(11f, 11f), button.actor))
    }

    private fun isTouching(screenPoint: Vector2, actor: Actor): Boolean {
        val viewPos = view.windowToViewport(screenPoint)
        assertTrue(viewPos.x >= 0)
        assertTrue(viewPos.y >= 0)
        assertTrue(viewPos.x < 640.0)
        assertTrue(viewPos.y <= 480.0)
        val worldPoint = view.viewportToWorld(viewPos)

        view.findActorsAt(worldPoint).forEach { foundActor ->
            if (foundActor == actor) {
                if (view.touching.touching(foundActor, worldPoint)) {
                    return true
                }
            }
        }

        return false
    }

}
