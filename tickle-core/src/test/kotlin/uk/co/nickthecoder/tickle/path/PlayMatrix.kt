package uk.co.nickthecoder.tickle.path

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.events.ButtonState
import uk.co.nickthecoder.tickle.events.Key
import uk.co.nickthecoder.tickle.events.KeyEvent
import uk.co.nickthecoder.tickle.util.Vector2

abstract class PlayMatrix : Play() {

    var angle = 0.0f
    var turnBy = 0f
    val moveBy = Vector2()
    var shiftDown = false
    val translation = Vector2(500f, 500f)
    var zoom = 1.0f
    val modelMatrix = Matrix3x2f()

    init {
        println("Arrow Keys : Pan")
        println("Brackets, comma, period : Rotate")
        println("Plus, Minus, 0 : Zoom / Reset Zoom")
        println("Hold Shift to increase speed of pan / rotation")

        redoMatrix()
    }


    override fun onKey(event: KeyEvent) {

        if (event.key == Key.LEFT_SHIFT || event.key == Key.RIGHT_SHIFT) {
            if (event.state == ButtonState.PRESSED) {
                turnBy *= 10f
                moveBy *= 10f
            } else {
                turnBy /= 10f
                moveBy /= 10f
            }
        }

        if (event.state == ButtonState.PRESSED) {

            if (event.key == Key.LEFT_BRACKET || event.key == Key.COMMA) {
                turnBy = if (event.shiftPressed()) 0.1f else 0.01f
            }
            if (event.key == Key.RIGHT_BRACKET || event.key == Key.PERIOD) {
                turnBy = -if (event.shiftPressed()) 0.1f else 0.01f
            }

            if (event.key == Key.UP) {
                moveBy.y = if (event.shiftPressed()) 10f else 1f
            }
            if (event.key == Key.DOWN) {
                moveBy.y = -if (event.shiftPressed()) 10f else 1f
            }
            if (event.key == Key.LEFT) {
                moveBy.x -= if (event.shiftPressed()) 10f else 1f
            }
            if (event.key == Key.RIGHT) {
                moveBy.x = if (event.shiftPressed()) 10f else 1f
            }

            if (event.key == Key.KEY_0) {
                zoom = 1f
                redoMatrix()
            }
            if (event.key == Key.MINUS) {
                zoom /= 1.5f
                redoMatrix()
            }
            if (event.key == Key.EQUAL) {
                zoom *= 1.5f
                redoMatrix()
            }
        }
        if (event.state == ButtonState.RELEASED) {
            if (event.key == Key.LEFT || event.key == Key.RIGHT) {
                moveBy.x = 0f
            }
            if (event.key == Key.UP || event.key == Key.DOWN) {
                moveBy.y = 0f
            }
            if (event.key == Key.COMMA || event.key == Key.PERIOD || event.key == Key.LEFT_BRACKET || event.key == Key.RIGHT_BRACKET) {
                turnBy = 0f
            }
        }
    }


    fun redoMatrix() {
        with(modelMatrix) {
            identity()
            translate(translation.x, translation.y)
            scale(zoom)
            rotate(angle)
        }
    }

}