package uk.co.nickthecoder.tickle.path

import org.lwjgl.glfw.GLFW
import uk.co.nickthecoder.tickle.events.KeyEvent
import uk.co.nickthecoder.tickle.events.MouseEvent
import uk.co.nickthecoder.tickle.events.ResizeEvent
import uk.co.nickthecoder.tickle.events.WindowListener
import uk.co.nickthecoder.tickle.graphics.*

abstract class Play : WindowListener {

    val viewWidth = 1280
    val viewHeight = 1024

    var width = viewWidth
    var height = viewHeight

    val window = Window(javaClass.simpleName, width, height, resizable = true, multiSample = 1).apply {
        show()
        enableVSync()
        makeContextCurrent()
    }

    var background = Colors.ALICEBLUE

    fun loop() {
        window.listeners.add(this)

        while (!window.shouldClose()) {
            GLFW.glfwPollEvents()
            with(Renderer.instance()) {
                clip(0, 0, width, height)
                beginView(0f, 0f, viewWidth.toFloat(), viewHeight.toFloat())
                clear(background)

                draw()

                endView()
                window.swap()
            }
        }
    }

    abstract fun draw()

    override fun onKey(event: KeyEvent) {}

    override fun onResize(event: ResizeEvent) {
        width = event.width
        height = event.height
    }

    override fun onMouseButton(event: MouseEvent) {}

    companion object {
        fun run(create: () -> Play) {
            OpenGL.begin()
            onOpenGLThread {
                try {
                    create().loop()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                OpenGL.end()

            }
        }
    }
}
