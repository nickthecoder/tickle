/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.action.animation.TurnBy
import uk.co.nickthecoder.tickle.action.animation.TurnTo
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Changes [position] over time ([seconds]), so that its final position is [by] units forwards in the direction
 * of [heading].
 * When changing an Actor's position, the heading will often be the Actor's direction, in which case, the
 * Actor will always be pointing in the direction of the motion.
 *
 * Note you may change [heading] during this Action, in which case, the final destination will change, and
 * the path will no longer be in a straight line.
 * For example, if the [heading] is changed constantly, (using [TurnTo] or [TurnBy]), then the path
 * will form a spiral.
 */
class ForwardsBy(
    val position: Vector2,
    seconds: Float,
    val by: Float,
    val heading: Angle,
    ease: Ease
)

    : TimedAnimation(seconds, ease) {

    private val initialPosition = Vector2()

    constructor(position: Vector2, seconds: Float, by: Float, heading: Angle, )
            : this(position, seconds, by, heading,  LinearEase.instance)

    override fun storeInitialValue() {
        initialPosition.set( position )
    }

    override fun update(t: Float) {
        position.set( heading.vector().apply {
            this *= by
            this *= t
            this += initialPosition
        })
    }
}
