package uk.co.nickthecoder.tickle.events

interface KeyListener {

    fun onKey(event: KeyEvent)

    fun onChar(event: CharEvent) {}

}

interface FocusListener : KeyListener {
    fun gainedFocus()
    fun lostFocus()
}
