/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.collision

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.stage.WrappedStageView
import uk.co.nickthecoder.tickle.stage.StageView

/**
 * If your [StageView] is a [WrappedStageView]
 * (i.e. one edge is joined to the opposite edge, such as in the classic game Asteroids),
 * then "normal" implementations of [Overlapping] will not work when the two actors
 * are close, but on opposite sides of the Stage.
 *
 * This fixes the problem.
 *
 * To use this class, create a "normal" [Overlapping], and then pass that
 * as [normal], when only use [WrappedOverlapping].
 */
class WrappedOverlapping(val view: WrappedStageView, val normal: Overlapping)
    : Overlapping {

    override fun overlapping(actorA: Actor, actorB: Actor): Boolean {
        val dx = actorB.position.x - actorA.position.x
        val dy = actorB.position.y - actorA.position.y
        val width = (view.joinRight - view.joinLeft)
        val height = (view.joinTop - view.joinBottom)

        val adjustX : Float = when {
            (dx > width / 2) -> width
            (dx < -width / 2) -> -width
            else -> 0f
        }
        val adjustY : Float = when {
            (dy > height / 2) -> height
            (dy < -height / 2) -> -height
            else -> 0f
        }
        actorA.x += adjustX
        actorA.y += adjustY
        val result = normal.overlapping(actorA, actorB)
        actorA.x -= adjustX
        actorA.y -= adjustY

        return result
    }

}
