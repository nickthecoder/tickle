/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.misc

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import org.jbox2d.dynamics.BodyType
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.physics.*
import uk.co.nickthecoder.tickle.resources.*
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.sound.Sound
import uk.co.nickthecoder.tickle.stage.FlexHAlignment
import uk.co.nickthecoder.tickle.stage.FlexVAlignment
import uk.co.nickthecoder.tickle.stage.GameStage
import uk.co.nickthecoder.tickle.stage.ZOrderStageView
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.Vector2
import uk.co.nickthecoder.tickle.util.YDownRect
import uk.co.nickthecoder.tickle.util.getEnum
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.util.*

open class ResourcesReader protected constructor(val resources: Resources) {

    protected constructor(resources: Resources, file: File) : this(resources) {
        resources.file = file
    }

    /**
     * While loading the costumes, one costume event may depend upon a costume that hasn't been loaded yet.
     * So we store the events in a list, and process them once all costumes have been loaded.
     */
    protected val costumeEvents = mutableListOf<CostumeEventData>()

    /**
     * When a costume inherits events from another costume, store the costume names in this map, and then
     * update the costume once all the costumes have been loaded.
     */
    protected val inheritedCostumeEvents = mutableMapOf<String, String>()


    fun addArray(obj: JsonObject, name: String, array: JsonArray) {
        if (!array.isEmpty) {
            obj.add(name, array)
        }
    }

    fun loadResources() {
        val startTime = Date().time
        info("Loading ${resources.file}")
        val jroot = Json.parse(InputStreamReader(FileInputStream(resources.file))).asObject()

        val jinfo = jroot.get("info")
        if (jinfo is JsonObject) {
            loadInfo(jinfo)
        } else {
            try {
                ScriptManager.setClasspath(File(resources.file.parent, "scripts"))
            } catch (e: Exception) {
                uk.co.nickthecoder.tickle.severe("Error loading scripts")
                e.printStackTrace()
            }
        }

        val jlayouts = jroot.get("layouts")
        val jtextures = jroot.get("textures")
        val jfonts = jroot.get("fonts")
        val jsounds = jroot.get("sounds")
        val jposes = jroot.get("poses")
        val jcostumeGroups = jroot.get("costumeGroups")
        val jcostumes = jroot.get("costumes")
        val jinputs = jroot.get("inputs")

        if (jlayouts is JsonArray) {
            info("Loading layouts")
            loadLayouts(jlayouts)
        }
        if (jtextures is JsonArray) {
            info("Loading textures")
            loadTextures(jtextures)
        }
        if (jfonts is JsonArray) {
            info("Loading fonts")
            loadFonts(jfonts)
        }
        if (jsounds is JsonArray) {
            info("Loading sounds")
            loadSounds(jsounds)
        }
        if (jposes is JsonArray) {
            info("Loading poses")
            loadPoses(jposes)
        }
        if (jcostumes is JsonArray) {
            info("Loading costumes")
            loadCostumes(jcostumes, resources.costumes)
        }
        if (jcostumeGroups is JsonArray) {
            info("Loading costume groups")
            loadCostumeGroups(jcostumeGroups)
        }
        if (jinputs is JsonArray) {
            info("Loading inputs")
            loadInputs(jinputs)
        }

        // Now all costume have been loaded, lets add the costume events.
        costumeEvents.forEach { data ->
            data.costumeEvent.costumes.add(resources.costumes.find(data.costumeName)!!)
        }

        inheritedCostumeEvents.forEach { (costumeName, inheritsFromCostumeName) ->
            val from = resources.costumes.find(costumeName)
            val to = resources.costumes.find(inheritsFromCostumeName)
            if (from != null && to != null) {
                from.inheritEventsFrom = to
            }
        }
        info("Loaded resources (${(Date().time - startTime) / 1000}s)")
    }

    // INFO

    /**
     * NOTE, there is a minimal version of this in the companion object, so that we can load the
     * bare-minimum parts of [GameInfo]
     */
    private fun loadInfo(jinfo: JsonObject) {
        with(resources.gameInfo) {

            enableFeather = jinfo.getBoolean("enableFeather", false)

            if (enableFeather) {
                ScriptManager.registerLanguage("uk.co.nickthecoder.tickle.feather.FeatherLanguage")
            }
            ScriptManager.setClasspath(File(resources.file.parent, "scripts"))

            title = jinfo.getString("title", "Tickle Game")
            id = jinfo.getString("id", "ticklegame")
            width = jinfo.getInt("width", 800)
            height = jinfo.getInt("height", 600)
            resizable = jinfo.getBoolean("resizable", true)
            fullScreen = jinfo.getBoolean("fullScreen", false)
            comments = jinfo.getString("comments", "")

            initialScenePath = resources.scenePathToFile(jinfo.getString("initialScene", "menu"))
            testScenePath = resources.scenePathToFile(jinfo.getString("testScene", "menu"))
            val producerStr = jinfo.getString("producer", NoProducer::class.java.name)
            try {
                producerString = ScriptManager.renamedClass(producerStr)
            } catch (e: Exception) {
                warn("Failed to set producer : ${producerStr}. Ignoring")

            }
        }

        val jphysics = jinfo.get("physics")?.asObject()
        if (jphysics != null) {
            resources.gameInfo.physicsEngine = true
            if (resources.gameInfo.physicsEngine) {
                with(resources.gameInfo.physicsInfo) {
                    gravity.x = jphysics.getFloat("gravity_x", 0f)
                    gravity.y = jphysics.getFloat("gravity_y", 0f)
                    velocityIterations = jphysics.getInt("velocityIterations", 8)
                    positionIterations = jphysics.getInt("positionIterations", 3)
                    scale = jphysics.getDouble("scale", 100.0)
                    filterGroupsString = ScriptManager.renamedClass(
                        jphysics.getString(
                            "filterGroups",
                            NoFilterGroups::class.java.name
                        )
                    )
                    filterBitsString = ScriptManager.renamedClass(
                        jphysics.getString(
                            "filterBits",
                            NoFilterBits::class.java.name
                        )
                    )
                }
            }
        }
    }


    // LAYOUTS

    fun loadLayouts(jlayouts: JsonArray) {
        jlayouts.forEach { jele ->
            val jLayout = jele.asObject()
            val name = jLayout.get("name").asString()
            val layout = Layout(resources)
            layout.comments = jLayout.getString("comments", "")

            jLayout.get("stages")?.let {
                val jStages = it.asArray()
                jStages.forEach {
                    val jStage = it.asObject()
                    val layoutStage = LayoutStage(resources)

                    val stageName = jStage.get("name").asString()

                    layoutStage.stageString =
                        ScriptManager.renamedClass(jStage.getString("stage", GameStage::class.java.name))
                    JsonUtil.loadAttributes(jStage, layoutStage.stageAttributes, "stageAttributes")
                    layoutStage.stageAttributes.updateAttributesMetaData(layoutStage.stageString)

                    layoutStage.isDefault = jStage.getBoolean("isDefault", false)

                    val stageConstraintStr =
                        jStage.getString("constraint", "uk.co.nickthecoder.tickle.editor.resources.NoStageConstraint")
                    try {
                        layoutStage.stageConstraintString = ScriptManager.renamedClass(stageConstraintStr)
                    } catch (e: Exception) {
                        warn("Failed to set Layout's stage constraint : ${stageConstraintStr}. Ignoring")
                    }
                    JsonUtil.loadAttributes(jStage, layoutStage.constraintAttributes, "constraintAttributes")

                    layout.layoutStages[stageName] = layoutStage
                }
            }

            jLayout.get("views")?.let {
                val jviews = it.asArray()
                jviews.forEach {
                    val jview = it.asObject()
                    val layoutView = LayoutView(resources)

                    val viewName = jview.get("name").asString()

                    layoutView.viewString =
                        ScriptManager.renamedClass(jview.getString("view", ZOrderStageView::class.java.name))
                    JsonUtil.loadAttributes(jview, layoutView.viewAttributes, "viewAttributes")
                    layoutView.viewAttributes.updateAttributesMetaData(layoutView.viewString)
                    layoutView.stageName = jview.getString("stage", "")
                    layoutView.zOrder = jview.getInt("zOrder", 50)
                    layoutView.position.autoScale = AutoScale.valueOf(jview.getString("autoScale", AutoScale.NONE.name))
                    if (layoutView.position.autoScale == AutoScale.LETTERBOX) {
                        layoutView.position.letterboxAlignment.x = jview.getFloat("letterboxAlignmentX", 0.5f)
                        layoutView.position.letterboxAlignment.y = jview.getFloat("letterboxAlignmentY", 0.5f)
                    }
                    layoutView.position.enableAutoPosition = jview.getBoolean("enableAutoPosition", false)

                    // X
                    val hAlignmentString = jview.getString("hAlignment", FlexHAlignment.LEFT.name)
                    layoutView.position.hAlignment = FlexHAlignment.valueOf(hAlignmentString)
                    if (layoutView.position.hAlignment == FlexHAlignment.MIDDLE) {
                        layoutView.position.hPosition = jview.get("hPosition").asDouble()
                    } else {
                        layoutView.position.leftRightMargin = jview.getInt("leftRightMargin", 0)
                    }
                    layoutView.position.width = jview.get("width")?.asInt()
                    layoutView.position.widthRatio = jview.get("widthRatio")?.asDouble()

                    // Y
                    val vAlignmentString = jview.getString("vAlignment", FlexVAlignment.BOTTOM.name)
                    layoutView.position.vAlignment = FlexVAlignment.valueOf(vAlignmentString)
                    if (layoutView.position.vAlignment == FlexVAlignment.MIDDLE) {
                        layoutView.position.vPosition = jview.get("vPosition").asDouble()
                    } else {
                        layoutView.position.topBottomMargin = jview.getInt("topBottomMargin", 0)
                    }
                    layoutView.position.height = jview.get("height")?.asInt()
                    layoutView.position.heightRatio = jview.get("heightRatio")?.asDouble()

                    layout.layoutViews[viewName] = layoutView
                }
            }
            resources.layouts.add(name, layout)

        }
    }

    // TEXTURES

    fun loadTextures(jtextures: JsonArray) {
        jtextures.forEach { jele ->
            val jtexture = jele.asObject()
            val name = jtexture.get("name").asString()
            val file = resources.fromPath(jtexture.get("file").asString())
            val texture = Texture.create(file)
            texture.comment = jtexture.getString("comment", "")

            info("Loading texture $name : $file")
            onOpenGLThread { resources.textures.add(name, texture) }
        }
    }

    // POSES

    fun loadPoses(jposes: JsonArray) {
        for (jele in jposes) {
            val jpose = jele.asObject()
            val name = jpose.get("name").asString()
            val textureName = jpose.get("texture").asString()
            val texture = resources.textures.find(textureName)
            if (texture == null) {
                error("Texture $textureName not found for pose $name.")
                continue
            }
            val pose = Pose(texture)

            pose.pixelRect.left = jpose.getInt("left", 0)
            pose.pixelRect.bottom = jpose.getInt("bottom", 0)
            pose.pixelRect.right = jpose.getInt("right", 0)
            pose.pixelRect.top = jpose.getInt("top", 0)

            pose.offsetX = jpose.getFloat("offsetX", 0f)
            pose.offsetY = jpose.getFloat("offsetY", 0f)

            pose.direction.degrees = jpose.getDouble("direction", 0.0)

            if (jpose.get("tiled") != null) {
                // Backwards compatible. We used to have a boolean called "tiled"
                pose.resizeType = if (jpose.getBoolean("tiled", false)) ResizeType.TILE else ResizeType.SCALE
            } else {
                pose.resizeType = jpose.getEnum("resizeType", ResizeType.SCALE)
            }

            if (pose.resizeType == ResizeType.NINE_PATCH) {
                pose.ninePatchMargins.left = jpose.getInt("marginL", 0)
                pose.ninePatchMargins.right = jpose.getInt("marginR", 0)
                pose.ninePatchMargins.top = jpose.getInt("marginT", 0)
                pose.ninePatchMargins.bottom = jpose.getInt("marginB", 0)
            }

            pose.comment = jpose.getString("comment", "")

            pose.updateRect()

            jpose.get("snapPoints")?.let {
                val jsnaps = it.asArray()
                jsnaps.forEach {
                    val jsnap = it.asObject()
                    val x = jsnap.getFloat("x", 0f)
                    val y = jsnap.getFloat("y", 0f)
                    pose.snapPoints.add(Vector2(x, y))
                }
            }

            resources.poses.add(name, pose)
        }
    }

    // Costume Groups

    fun loadCostumeGroups(jgroups: JsonArray) {

        jgroups.forEach {
            val jgroup = it.asObject()
            val group = CostumeGroup(resources)
            val groupName = jgroup.get("name").asString()
            group.showInSceneEditor = jgroup.getBoolean("showInSceneEditor", true)
            group.initiallyExpanded = jgroup.getBoolean("initiallyExpanded", true)
            group.sortOrder = jgroup.getInt("sortOrder", 0)

            val jcostumes = jgroup.get("costumes").asArray()
            loadCostumes(jcostumes, group)

            group.items().forEach { costumeName, costume ->
                resources.costumes.add(costumeName, costume)
            }
            resources.costumeGroups.add(groupName, group)
        }
    }

    // COSTUMES

    fun saveBody(jcostume: JsonObject, bodyDef: TickleBodyDef) {
        val jbody = JsonObject()

        with(bodyDef) {
            jbody.add("bodyType", type.name)
            if (type != BodyType.STATIC) {
                jbody.add("linearDamping", linearDamping)
                jbody.add("angularDamping", angularDamping)
                jbody.add("bullet", bullet)
                jbody.add("fixedRotation", fixedRotation)
            }
        }

        val jfixtures = JsonArray()
        jbody.add("fixtures", jfixtures)
        bodyDef.fixtureDefs.forEach { fixtureDef ->
            val jfixture = JsonObject()

            with(fixtureDef) {
                jfixture.add("friction", friction)
                jfixture.add("density", density)
                jfixture.add("restitution", restitution)
                jfixture.add("isSensor", isSensor)
            }

            with(fixtureDef.filter) {
                jfixture.add("group", groupIndex)
                jfixture.add("category", categoryBits)
                jfixture.add("mask", maskBits)
            }

            with(fixtureDef.shapeDef) {
                when (this) {
                    is CircleDef -> {
                        val jcircle = JsonObject()
                        jfixture.add("circle", jcircle)
                        jcircle.add("x", center.x)
                        jcircle.add("y", center.y)
                        jcircle.add("radius", radius)
                    }

                    is BoxDef -> {
                        val jbox = JsonObject()
                        jfixture.add("box", jbox)
                        jbox.add("x", center.x)
                        jbox.add("y", center.y)
                        jbox.add("width", width)
                        jbox.add("height", height)
                        jbox.add("angle", angle.degrees)
                    }

                    is PolygonDef -> {
                        val jpolygon = JsonObject()
                        jfixture.add("polygon", jpolygon)
                        val jpoints = JsonArray()
                        jpolygon.add("points", jpoints)
                        points.forEach { point ->
                            val jpoint = JsonObject()
                            jpoint.add("x", point.x)
                            jpoint.add("y", point.y)
                            jpoints.add(jpoint)
                        }
                    }

                    else -> {
                        warn("ERROR. Unknown shape ${this.javaClass}")
                    }
                }
            }
            jfixtures.add(jfixture)

        }

        jcostume.add("body", jbody)
    }

    data class CostumeEventData(val costumeEvent: CostumeEvent, val costumeName: String)

    fun loadCostumes(jcostumes: JsonArray, group: ResourceMap<Costume>) {

        jcostumes.forEach {
            val jcostume = it.asObject()
            val name = jcostume.get("name").asString()
            val costume = Costume(resources)
            val roleStr = jcostume.getString("role", "")
            try {
                costume.roleString = ScriptManager.renamedClass(roleStr)
            } catch (e: Exception) {
                warn("Failed to set Costume's role : ${roleStr}. Ignoring")
            }
            costume.canRotate = jcostume.getBoolean("canRotate", false)
            costume.canScale = jcostume.getBoolean("canScale", false)
            costume.zOrder = jcostume.getFloat("zOrder", 0f)
            costume.stageName = jcostume.getString("stageName", "")
            costume.initialEventName = jcostume.getString("initialEvent", "default")
            costume.showInSceneEditor = jcostume.getBoolean("showInSceneEditor", true)
            costume.comments = jcostume.getString("comments", "")

            val inheritsFrom: String? = jcostume.getString("inheritsEventsFrom", null)
            if (inheritsFrom != null) {
                inheritedCostumeEvents[name] = inheritsFrom
            }

            jcostume.get("events")?.let {
                val jevents = it.asArray()
                jevents.forEach {
                    val jevent = it.asObject()
                    val eventName = jevent.get("name").asString()
                    val event = CostumeEvent(costume)

                    jevent.get("poses")?.let {
                        val jposes = it.asArray()
                        jposes.forEach {
                            val poseName = it.asString()
                            resources.poses.find(poseName)?.let { event.poses.add(it) }
                        }
                    }

                    jevent.get("costumes")?.let {
                        val jcoses = it.asArray()
                        jcoses.forEach {
                            val costumeName = it.asString()
                            // We cannot add the costume event at this point, because the costume may not have been loaded yet
                            // So instead, save the details, and process it afterwards.
                            costumeEvents.add(CostumeEventData(event, costumeName))
                        }
                    }

                    jevent.get("textStyles")?.let {
                        val jtextStyles = it.asArray()
                        jtextStyles.forEach {
                            val jtextStyle = it.asObject()
                            val fontResource = resources.fontResources.find(jtextStyle.get("font").asString())!!
                            val halign =
                                TextHAlignment.valueOf(jtextStyle.getString("halign", TextHAlignment.LEFT.name))
                            val valign =
                                TextVAlignment.valueOf(jtextStyle.getString("valign", TextVAlignment.BASELINE.name))
                            val color = Color.create(jtextStyle.getString("color", "#FFFFFF"))
                            val textStyle = TextStyle(fontResource, halign, valign, color)
                            if (textStyle.fontResource.outlineFontTexture != null) {
                                val outlineColor = Color.create(jtextStyle.getString("outlineColor", "#000000"))
                                textStyle.outlineColor = outlineColor
                            }
                            event.textStyles.add(textStyle)
                        }
                    }

                    jevent.get("strings")?.let {
                        val jstrings = it.asArray()
                        jstrings.forEach {
                            event.strings.add(it.asString())
                        }
                    }

                    // Backward compatibility
                    // Sounds used to be a simple array of sound names.
                    jevent.get("sounds")?.let {
                        val jsounds = it.asArray()
                        jsounds.forEach {
                            val soundName = it.asString()
                            resources.sounds.find(soundName)
                                ?.let { event.soundEvents.add(SoundEvent(it, it.defaultGain.toFloat())) }
                        }
                    }
                    jevent.get("soundEvents")?.let {
                        val jsoundEvents = it.asArray()
                        jsoundEvents.forEach {
                            val jsoundEvent = it.asObject()
                            val soundName = jsoundEvent.get("sound").asString()
                            val sound = resources.sounds.find(soundName)
                            if (sound != null) {
                                val priority = jsoundEvent.getFloat("priority", SoundEvent.DEFAULT_PRIORITY)
                                val threshold = jsoundEvent.getFloat("threshold", SoundEvent.DEFAULT_PRIORITY)
                                val gain = jsoundEvent.getFloat("gain", sound.defaultGain.toFloat())
                                val onlyOnce = jsoundEvent.getBoolean("onlyOnce", false)
                                val onlyWhenVisible = jsoundEvent.getBoolean("onlyWhenVisible", false)
                                val soundEvent = SoundEvent(sound, gain, onlyWhenVisible, priority, threshold, onlyOnce)
                                event.soundEvents.add(soundEvent)
                            }
                        }
                    }

                    jevent.get("ninePatches")?.let {
                        val jninePatches = it.asArray()
                        jninePatches.forEach {
                            val jninePatch = it.asObject()
                            val pose = resources.poses.find(jninePatch.get("pose").asString())
                            if (pose != null) {
                                val ninePatch = NinePatch(
                                    pose,
                                    jninePatch.getInt("left", 0),
                                    jninePatch.getInt("bottom", 0),
                                    jninePatch.getInt("right", 0),
                                    jninePatch.getInt("top", 0)
                                )
                                event.ninePatches.add(ninePatch)
                            }

                        }
                    }

                    costume.events[eventName] = event
                }
            }

            JsonUtil.loadAttributes(jcostume, costume.attributes, isAlternate = true)

            val jbody = jcostume.get("body")
            if (jbody != null && jbody.isObject) {
                loadBody(jbody.asObject(), costume)
            }

            group.add(name, costume)
            if (group is CostumeGroup) {
                costume.costumeGroup = group
            }
        }

    }

    fun loadBody(jbody: JsonObject, costume: Costume) {
        val bodyDef = TickleBodyDef()
        with(bodyDef) {
            type = BodyType.valueOf(jbody.getString("bodyType", BodyType.DYNAMIC.name))
            linearDamping = jbody.getDouble("linearDamping", 0.0)
            angularDamping = jbody.getDouble("angularDamping", 0.0)
            bullet = jbody.getBoolean("bullet", false)
            fixedRotation = jbody.getBoolean("fixedRotation", false)
        }
        jbody.get("fixtures")?.let {
            val jfixtures = it.asArray()
            jfixtures.forEach {
                val jfixture = it.asObject()
                var shape: ShapeDef? = null
                jfixture.get("circle")?.let {
                    val jcircle = it.asObject()
                    val x = jcircle.getFloat("x", 0f)
                    val y = jcircle.getFloat("y", 0f)
                    val radius = jcircle.getFloat("radius", 0f)
                    val circle = CircleDef(Vector2(x, y), radius)
                    shape = circle
                }
                jfixture.get("box")?.let {
                    val jbox = it.asObject()
                    val x = jbox.getFloat("x", 0f)
                    val y = jbox.getFloat("y", 0f)
                    val width = jbox.getFloat("width", 0f)
                    val height = jbox.getFloat("height", 0f)
                    val angle = jbox.getDouble("angle", 0.0)
                    val box = BoxDef(width, height, Vector2(x, y), Angle.degrees(angle))
                    shape = box
                }
                jfixture.get("polygon")?.let {
                    val jpolygon = it.asObject()
                    val jpoints = jpolygon.get("points").asArray()
                    val polygon = PolygonDef()
                    jpoints.forEach {
                        val jpoint = it.asObject()
                        val x = jpoint.getFloat("x", 0f)
                        val y = jpoint.getFloat("y", 0f)
                        polygon.points.add(Vector2(x, y))
                    }
                    shape = polygon
                }

                if (shape != null) {
                    val fixtureDef = TickleFixtureDef(shape!!)
                    with(fixtureDef) {
                        density = jfixture.getFloat("density", 1f)
                        restitution = jfixture.getFloat("restitution", 0f)
                        friction = jfixture.getFloat("friction", 0f)
                        isSensor = jfixture.getBoolean("isSensor", false)
                    }
                    with(fixtureDef.filter) {
                        groupIndex = jfixture.getInt("group", 0)
                        categoryBits = jfixture.getInt("category", 0xFFFF)
                        maskBits = jfixture.getInt("mask", 0xFFFF)
                    }
                    bodyDef.fixtureDefs.add(fixtureDef)
                }
            }
        }
        costume.bodyDef = bodyDef
    }

    // INPUTS

    fun loadInputs(jinputs: JsonArray) {
        jinputs.forEach { jele ->
            val jinput = jele.asObject()
            val name = jinput.get("name").asString()
            val input = CompoundInput()
            input.comments = jinput.getString("comments", "")

            jinput.get("keys")?.let {
                val jkeys = it.asArray()
                jkeys.forEach {
                    val jkey = it.asObject()
                    val key = jkey.get("key").asString()
                    val stateString = jkey.getString("state", "PRESSED")
                    val state = ButtonState.valueOf(stateString)
                    val shift = jkey.getBoolean("shift")
                    val control = jkey.getBoolean("control")
                    val alt = jkey.getBoolean("alt")
                    input.add(KeyInput(Key.forLabel(key), state, shift = shift, control = control, alt = alt))
                }
            }

            jinput.get("mouse")?.let {
                val jmice = it.asArray()
                jmice.forEach {
                    val jmouse = it.asObject()
                    val button = jmouse.getInt("button", 1)
                    val stateString = jmouse.getString("state", "PRESSED")
                    val state = ButtonState.valueOf(stateString)
                    val shift = jmouse.getBoolean("shift")
                    val control = jmouse.getBoolean("control")
                    val alt = jmouse.getBoolean("alt")
                    input.add(MouseInput(button, state, shift = shift, control = control, alt = alt))
                }
            }

            jinput.get("joystick")?.let {
                val jbuttons = it.asArray()
                jbuttons.forEach {
                    val jbutton = it.asObject()
                    val joystickID = jbutton.getInt("joystickID", 0)
                    val buttonString = jbutton.getString("button", JoystickButton.A.name)
                    val button = JoystickButton.valueOf(buttonString)
                    input.add(JoystickButtonInput(joystickID, button))
                }
            }

            jinput.get("joystickAxis")?.let {
                val jaxes = it.asArray()
                jaxes.forEach {
                    val jaxis = it.asObject()
                    val joystickID = jaxis.getInt("joystickID", 0)
                    val axisString = jaxis.getString("axis", JoystickAxis.LEFT_X.name)
                    val positive = jaxis.getBoolean("positive", true)
                    val threshold = jaxis.getDouble("threshold", 0.5)
                    val axis = JoystickAxis.valueOf(axisString)
                    input.add(JoystickAxisInput(joystickID, axis, positive, threshold))
                }
            }

            resources.inputs.add(name, input)
        }
    }


    // FONTS

    fun loadFonts(jfonts: JsonArray) {

        jfonts.forEach { jele ->
            val jfont = jele.asObject()
            val name = jfont.get("name").asString()
            val fontResource = FontResource()
            val pngFilename = jfont.getString("pngFile", File(resources.texturesDirectory, "$name.png").path)
            val fontPath = jfont.getString("file", "")

            if (fontPath.isBlank()) {
                fontResource.fontName = jfont.get("fontName").asString()
                val styleString = jfont.getString("style", "PLAIN")
                fontResource.style = FontResource.FontStyle.valueOf(styleString)
            } else {
                fontResource.file = resources.fromPath(fontPath)
            }

            fontResource.size = jfont.getDouble("size", 22.0)
            fontResource.xPadding = jfont.getInt("xPadding", 1)
            fontResource.yPadding = jfont.getInt("yPadding", 1)
            fontResource.comments = jfont.getString("comments", "")

            val pngFile = resources.fromPath(pngFilename)
            if (pngFile.exists()) {
                fontResource.pngFile = pngFile
                fontResource.loadFromFile(pngFile)
            }

            resources.fontResources.add(name, fontResource)

        }
    }


    // SOUNDS

    fun loadSounds(jsounds: JsonArray) {
        jsounds.forEach {
            val jsound = it.asObject()
            val name = jsound.get("name").asString()
            val file = resources.fromPath(jsound.get("file").asString())

            val sound = Sound(file)
            sound.defaultGain = jsound.getDouble("defaultGain", 1.0)
            sound.comments = jsound.getString("comments", "")

            resources.sounds.add(name, sound)

        }
    }

    fun error(message: String) {
        severe(JsonLoadException(message))
    }

    fun error(e: Exception, message: String) {
        severe(JsonLoadException(e, message))
    }

    companion object {

        fun copyGlyphs(texture: Texture, glyphs: Map<Char, Glyph>): Map<Char, Glyph> {
            val result = mutableMapOf<Char, Glyph>()
            glyphs.forEach { c, glyph ->
                val pose = Pose(texture, glyph.pose.pixelRect)
                pose.offsetX = glyph.pose.offsetX
                pose.offsetY = glyph.pose.offsetY
                result[c] = Glyph(pose, glyph.advance)
            }
            return result
        }

        fun loadFontMetrics(file: File, texture: Texture): FontTexture {
            val jroot = Json.parse(InputStreamReader(FileInputStream(file))).asObject()

            val lineHeight = jroot.get("lineHeight").asFloat()
            val leading = jroot.get("leading").asFloat()
            val ascent = jroot.get("ascent").asFloat()
            val descent = jroot.get("descent").asFloat()
            val xPadding = jroot.getFloat("xPadding", 1.0f)
            val yPadding = jroot.getFloat("yPadding", 1.0f)

            val glyphs = mutableMapOf<Char, Glyph>()

            val jglyphs = jroot.get("glyphs").asArray()
            jglyphs.forEach {
                val jglyph = it.asObject()
                val c = jglyph.get("c").asString().first()
                val left = jglyph.get("left").asInt()
                val top = jglyph.get("top").asInt()
                val right = jglyph.get("right").asInt()
                val bottom = jglyph.get("bottom").asInt()
                val advance = jglyph.get("advance").asFloat()
                val rect = YDownRect(left, top, right, bottom)
                val pose = Pose(texture, rect)
                pose.offsetX = xPadding
                pose.offsetY = rect.height - yPadding
                val glyph = Glyph(pose, advance)
                glyphs[c] = (glyph)
            }

            return FontTexture(glyphs, lineHeight, leading = leading, ascent = ascent, descent = descent)
        }

        /**
         * JsonObject doesn't have a method which returns an NULLABLE boolean.
         */
        private fun JsonObject.getBoolean(name: String): Boolean? {
            this.get(name) ?: return null
            return this.getBoolean(name, true)
        }

        fun load(into: Resources, resourcesFile: File) {
            into.file = resourcesFile
            ResourcesReader(into, resourcesFile).loadResources()
        }

        /**
         * We may want to read the [GameInfo], without reading the entire file. For example, we may want to
         * get the size of the window, so that the GL Window can be created with the correct size.
         *
         * (The GL Window must be created before we load a texture!)
         */
        fun loadGameInfo(resourcesFile: File): GameInfo {
            val gameInfo = GameInfo("Dummy", "dummy", 1024, 800, false, false)
            val jroot = Json.parse(InputStreamReader(FileInputStream(resourcesFile))).asObject()

            jroot.get("info")?.let {
                val jinfo = it as JsonObject
                with(gameInfo) {

                    title = jinfo.getString("title", "Tickle Game")
                    id = jinfo.getString("id", "ticklegame")
                    width = jinfo.getInt("width", 800)
                    height = jinfo.getInt("height", 600)
                    resizable = jinfo.getBoolean("resizable", true)
                    fullScreen = jinfo.getBoolean("fullScreen", false)
                    comments = jinfo.getString("comments", "")

                }
            }
            return gameInfo
        }

    }

}

class JsonLoadException : Exception {
    constructor(message: String) : super(message)
    constructor(e: Exception, message: String) : super(message, e)
}
