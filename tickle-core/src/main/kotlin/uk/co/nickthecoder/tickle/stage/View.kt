/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.stage

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.Role
import uk.co.nickthecoder.tickle.events.MouseButtonListener
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.Rect
import uk.co.nickthecoder.tickle.util.RectInt
import uk.co.nickthecoder.tickle.util.Vector2

private val tempVector = Vector2()

interface View : MouseButtonListener {

    /**
     * Determines how this View should be automatically repositioned when the window is resized
     */
    var flexPosition : FlexPosition?

    var zOrder: Int

    var rect: RectInt

    val width: Int
        get() = rect.width

    val height: Int
        get() = rect.height

    fun begin()

    fun tick()

    fun draw(renderer: Renderer)


    /**
     * Convert the x and y values in window units (where 0,0 is the top left edge of the window)
     * to the viewport's x,y coordinates - i.e. the coordinate system where (0,0) is the bottom left of the
     * viewport, and the y axis points upwards.
     *
     * Note this is NOT related to world coordinates (which StageView uses).
     *
     * The result is returned by changing the [viewport] Vector2 (and therefore avoiding creating a new Vector2 object).
     *
     * Note, [window] and [viewport] may be the SAME instance
     */
    fun windowToViewport(window: Vector2, viewport: Vector2) {
        viewport.x = window.x - rect.left
        viewport.y = (Game.instance.window.height) - window.y - rect.bottom
    }

    /**
     * Similar to the other [windowToViewport] method, but returning a NEW Vector2,
     * rather than setting an existing instance.
     */
    fun windowToViewport(window: Vector2): Vector2 {
        val result = Vector2()
        windowToViewport(window, result)
        return result
    }

    /**
     * Convert the x and y values in the viewports coordinates (where 0,0 is the bottom left edge of the view)
     * to the window's coordinates - i.e. where (0,0) is the top left of the
     * window.
     * Note that the window's y axis points DOWN while the view's y axis points UP.
     *
     * Note this is NOT related to world coordinates (which StageView uses).
     *
     * The result is returned by changing the [window] Vector2 (and therefore avoiding creating a new Vector2 object).
     *
     * Note, [window] and [viewport] may be the SAME instance
     */
    fun viewportToWindow(viewport: Vector2, window : Vector2) {
        window.x = viewport.x + rect.left
        window.y = (Game.instance.window.height) - viewport.y - rect.bottom
    }

    /**
     * Similar to the other [viewportToWindow] method, but returning a NEW Vector2,
     * rather than setting an existing instance.
     */
    fun viewportToWindow(viewport: Vector2): Vector2 {
        val result = Vector2()
        viewportToWindow(viewport, result)
        return result
    }

    /**
     * Finds the position of the mouse pointer in pixels, relative to the view's bottom left.
     */
    fun getMousePositionViewport(result: Vector2) {
        Game.instance.window.getMousePosition(result)
        windowToViewport(result, result)
    }

    /**
     * Sets the position of the mouse pointer in pixels, relative to the view's bottom left.
     */
    fun setMousePositionViewport(position: Vector2) {
        Game.instance.window.setMousePosition(viewportToWindow(position))
    }
}

/**
 * A [View] which has world coordinates.
 *
 * At present all [WorldView] implementations also implement [StageView]
 * i.e. they contain [Actor]s.
 *
 * However, it is conceivable that you could create a view with world coordinates without [Actor]s.
 * For example, you could create a view containing `particles` (for special effects), which don't have the overheads
 * of [Actor] and [Role]. In which case, the view would implement [WorldView], but not [StageView].
 */
interface WorldView : View {

    val scale: Vector2

    /**
     * A convenience, setting scale.x and scale.y to the same value.
     * When getting, if scale.x and scale.y differ, then only scale.x is returned.
     */
    var scaleXY: Float
        get() = scale.x
        set(v) {
            scale.x = v
            scale.y = v
        }

    var direction : Angle

    var handleMouseButtons: Boolean

    val worldWidth: Float

    val worldHeight: Float

    /**
     * The point of rotation and scaling in world coordinates.
     * This world point maps to [viewportFocal] in viewport coordinates.
     *
     * If you want to keep an Actor as the center of attention in a scrolling view, then
     * this should be the actor's position, and [viewportFocal] should be the center of the view.
     *
     * However, the default is for this to be (0,0) and viewportFocal to be (0,0).
     * This won't work well if you want to scale, or rotate the view though, because the scaling will be
     * around the bottom left corner of view.
     */
    val worldFocal: Vector2

    /**
     * The point of rotation and scaling in viewport coordinates.
     * This world point maps to [worldFocal] in world coordinates.
     */
    val viewportFocal: Vector2


    /**
     * The extent of the View in world coordinates.
     *
     * WARNING. This does NOT work when the view is rotated.
     * It simply converts the bottomLeft and topRight points of the viewport into world coordinates,
     * and places them into a [Rect].
     */
    fun worldRect(): Rect {
        val bottomLeft = viewportToWorld(Vector2())
        val topRight = viewportToWorld(Vector2(width.toFloat(), height.toFloat()))
        return Rect(bottomLeft.x, bottomLeft.y, topRight.x, topRight.y)
    }


    /**
     * Translates world coordinates (as used by Actor.position) into pixel values relative to the bottom left
     * of the viewport.
     * If the view covers the whole screen, then [into] wll also be relative to the bottom left of the screen.
     */
    fun worldToViewport(world: Vector2, into: Vector2)

    fun worldToViewport(world: Vector2): Vector2 {
        val result = Vector2()
        worldToViewport(world, result)
        return result
    }

    /**
     * Converts the [world] position into coordinates relative to the [Window].
     */
    fun worldToWindow(world: Vector2, window: Vector2)

    fun worldToWindow(world: Vector2) : Vector2 {
        val result = Vector2()
        worldToWindow(world, result)
        return result
    }
    /**
     * Converts the [window] position into world coordinates.
     */
    fun windowToWorld(window: Vector2, world: Vector2)

    fun windowToWorld(window: Vector2) : Vector2 {
        val result = Vector2()
        windowToWorld(window, result)
        return result
    }


    /**
     * Translates pixel values relative to the bottom left of the viewport into world coordinates (as used by Actor.position).
     * of the view.
     * If the view covers the whole screen, then [into] wll also be relative to the bottom left of the screen.
     */
    fun viewportToWorld(viewport: Vector2, into: Vector2)

    fun viewportToWorld(viewport: Vector2): Vector2 {
        val result = Vector2()
        viewportToWorld(viewport, result)
        return result
    }

    /**
     * Finds the position of the mouse pointer in the view's world coordinate system.
     */
    fun getMousePositionWorld(result: Vector2) {
        Game.instance.window.getMousePosition(result)
        windowToWorld(result, result)
    }

    fun getMousePositionWorldBounded(result: Vector2) {
        Game.instance.window.getMousePixelsBounded(result)
        windowToWorld(result, result)
        return
    }

    fun setMousePositionWorld(position: Vector2) {
        Game.instance.window.setMousePosition(worldToWindow(position))
    }


    fun centerView() {
        val worldRect = worldRect()
        viewportFocal.x = width/2f
        viewportFocal.y = height/2f

        worldFocal.x = (worldRect.left + worldRect.right ) / 2
        worldFocal.y = (worldRect.top + worldRect.bottom ) / 2
    }

    fun visible(world: Vector2): Boolean {
        worldToViewport(world, tempVector)
        return tempVector.x > 0 && tempVector.y > 0 && tempVector.x < rect.width && tempVector.y < rect.height
    }
}
