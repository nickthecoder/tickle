/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.physics

import org.jbox2d.dynamics.joints.MouseJoint
import org.jbox2d.dynamics.joints.MouseJointDef
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.ImmutableVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Attracts an actor towards a point (which is usually the mouse pointer's position).
 *
 * Each frame, you can update the target position. See [target].
 *
 * @param targetPoint The initial target point (the same as the [target] field.
 * However, I don't understand how MouseJoints works, and there's a chance that I'm initialising them
 * incorrectly.
 *
 * @param maxForce A low value will give a very elastic feel, a high value may cause actorA to be accelerated
 *        too much. The force should be in proportion to the mass of the actor's body.
 *
 * @param actorA In the Box2D TestBed, this is set to the "Ground" body. i.e. a "dummy", non moving object,
 *
 * @param actorB The actor which is affected
 */
class TickleMouseJoint(

    actorA: Actor,
    actorB: Actor,
    targetPoint: Vector2,
    maxForce: Float,
) : TickleJoint<MouseJoint, MouseJointDef>(actorA, actorB, Vector2(), targetPoint) {
// NOTE I'm passing targetPoint to localAnchorB of TickleJoint.
// That's very weird, because targetPoint is supposed to be an ABSOLUTE position, not relative.
// However, when I use a relative position, the joint goes wrong???
// In Box2S's Test bed, it doesn't use localAnchor points at all, so no help there!
// https://github.com/erincatto/box2d/blob/main/testbed/test.cpp

    init {
        actorA.body?.jBox2DBody?.isAwake = true
        actorB.body?.jBox2DBody?.isAwake = true
        create()
        jBox2dJoint?.maxForce = maxForce
        jBox2dJoint?.target = tickleWorld.tickleToPhysics(targetPoint)
    }

    /**
     * Note, maxForce is in JBox2D's units.
     */
    var maxForce: Float
        get() = jBox2dJoint?.maxForce ?: 0f
        set(v) {
            jBox2dJoint?.maxForce = v
        }

    var target: IVector2
        get() = tickleWorld.physicsToImmutableTickle(jBox2dJoint!!.target)
        set(v) {
            tickleWorld.tickleToPhysics(jBox2dJoint!!.target, v)
        }


    var frequency: Float
        get() = jBox2dJoint!!.frequency
        set(v) {
            jBox2dJoint!!.frequency = v
        }

    var damping: Float
        get() = jBox2dJoint!!.dampingRatio
        set(v) {
            jBox2dJoint!!.dampingRatio = v
        }


    override fun createDef(): MouseJointDef {
        val jointDef = MouseJointDef()
        jointDef.bodyA = actorA.body!!.jBox2DBody
        jointDef.bodyB = actorB.body!!.jBox2DBody
        jointDef.maxForce = maxForce
        tickleWorld.tickleToPhysics(jointDef.target, localAnchorB)
        return jointDef
    }

    override fun replace() {
        val tmpMaxForce = maxForce
        super.replace()
        maxForce = tmpMaxForce
    }
}
