package uk.co.nickthecoder.tickle.control

import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.misc.clamp
import uk.co.nickthecoder.tickle.warn
import java.lang.ref.WeakReference
import java.util.*

/**
 * Used as part of a [TextArea].
 *
 * Note, two [TextArea]s can share the same [TextDocument].
 *
 * Each [TextArea] will have its own [TextArea.caretPosition] and [TextArea.markPosition].
 * These are NOT part of the undo/redo history.
 *
 */
class TextDocument {

    private val privateParagraphs = mutableListOf<String>("")

    /**
     * A read-only list of [String] which form define the [TextDocument].
     */
    val paragraphs: List<String> = Collections.unmodifiableList(privateParagraphs)

    /**
     * The full text of the document.
     * Note, the document is NOT stored as a plain String, so using this for read or write is expensive in both
     * time and memory.
     * Therefore consider using [paragraphs] to read the document and [replace], [insert], [delete] to update the
     * document.
     */
    var text: String
        get() = privateParagraphs.joinToString(separator = "\n")
        set(v) {
            makeChange(Replacement(Position(0, 0), paragraphs, v.split("\n")))
        }

    val indentSize = 4

    /**
     * The maximum number of items stored in the [history].
     * When the history exceeds this value, older items cannot be undone.
     */
    val maxHistory = 100

    private val highlightRanges = mutableListOf<HighlightRange>()

    val ranges: List<HighlightRange> = Collections.unmodifiableList(highlightRanges)

    private val listeners = mutableListOf<WeakReference<Listener>>()

    fun charAt(position: Position): Char {
        val str = paragraphs[position.line]
        return if (position.column == str.length) '\n' else str[position.column]
    }

    private var endPosition = Position.START

    fun endPosition() = endPosition

    fun lineCount() = privateParagraphs.size

    fun position(line: Int, column: Int): Position {
        val l = clamp(line, 0, paragraphs.size - 1)
        val c = clamp(column, 0, paragraphs[l].length)
        return Position(l, c)
    }

    /**
     * clamps the position within the valid range [Position.START] .. [endPosition].
     * If the column was invalid, it is clamped between 0 and the line's length.
     * This does NOT adjust the line due to an invalid column.
     *
     * e.g. safe( Position( 1, -1 ) returns Position( 1, 0 )
     *
     * See also
     */
    fun safe(position: Position) = position.clamp(Position.START, endPosition())

    /**
     * Takes a [Position], and returns a new Position after adding [n] lines.
     * [n] may be negative.
     *
     * The returned position's line is clamped within the range 0..paragraphs.size -1.
     *
     * The returned position's column is clamped within the range 0..paragraph's string length.
     */
    fun addLines(position: Position, n: Int): Position {
        val l = clamp(position.line + n, 0, paragraphs.size - 1)
        val c = clamp(position.column, 0, privateParagraphs[l].length)

        return Position(l, c)
    }

    /**
     * Takes a [Position], and returns a new Position after adding [columns].
     * If this takes it beyond the start or end of the line, then the line is adjusted too.
     *
     * The returned position's line is clamped within the range 0..paragraphs.size -1.
     *
     * The returned position's column is clamped within the range 0..paragraph's string length.
     */
    fun addColumns(position: Position, columns: Int): Position {
        var l = position.line
        var c = position.column + columns
        while (c < 0) {
            if (l <= 0) return Position(0, 0)
            l--
            c += privateParagraphs[l].length + 1
        }
        while (c > privateParagraphs[l].length) {
            c -= privateParagraphs[l].length + 1
            if (l >= privateParagraphs.size - 1) {
                return Position(privateParagraphs.size - 1, privateParagraphs.last().length)
            }
            l++
        }
        return Position(l, c)
    }

    fun addHighlightRange(item: HighlightRange) {
        require(item.from === safe(item.from)) { "Invalid from position ${item.from}" }
        require(item.to === safe(item.to)) { "Invalid from position ${item.to}" }

        if (item.transient) {
            highlightRanges.add(item)
        } else {
            makeChange(AddHighlightRanges(listOf(item)))
        }
    }

    fun removeHighlightRange(item: HighlightRange) {
        if (item.transient) {
            highlightRanges.remove(item)
        } else {
            makeChange(RemoveHighlightRanges(listOf(item)))
        }
    }

    fun addHighlightRanges(items: List<HighlightRange>) {
        for (item in items) {
            require(item.from === safe(item.from)) { "Invalid from position ${item.from}" }
            require(item.to === safe(item.to)) { "Invalid from position ${item.to}" }
        }

        val transient = items.filter { it.transient }
        for (hr in transient) {
            highlightRanges.add(hr)
        }

        val nonTransient = items.filter { !it.transient }
        if (nonTransient.isNotEmpty()) {
            makeChange(AddHighlightRanges(nonTransient))
        }
    }

    fun removeHighlightRanges(items: List<HighlightRange>) {
        val transient = items.filter { it.transient }
        for (hr in transient) {
            highlightRanges.remove(hr)
        }

        val nonTransient = items.filter { !it.transient }
        if (nonTransient.isNotEmpty()) {
            makeChange(RemoveHighlightRanges(nonTransient))
        }
    }

    fun removeHighlightRanges(owner: Any) {
        highlightRanges.removeAll(highlightRanges.filter { it.owner === owner && it.transient })
        val nonTransient = highlightRanges.filter { it.owner === owner && !it.transient }
        if (nonTransient.isNotEmpty()) {
            makeChange(RemoveHighlightRanges(nonTransient))
        }
    }


    fun addListener(listener: Listener) {
        listeners.add(WeakReference(listener))
    }

    fun removeListener(listener: Listener) {
        listeners.removeAll { it.get() == listener }
    }

    private fun replaced(from: Position, to: Position, removedTo: Position) {
        for (wl in listeners) {
            wl.get()?.replaced(this, from, to, removedTo)
        }
        listeners.removeAll { it.get() == null }
    }

    private val history = mutableListOf<Change>()

    private var historyIndex = 0

    fun delete(from: Position, to: Position) {
        val safeFrom = safe(from)
        val safeTo = safe(to)

        makeChange(Replacement(safeFrom, part(safeFrom, safeTo), listOf("")))
    }

    fun insert(from: Position, newText: String) {
        val safeFrom = safe(from)
        makeChange(Replacement(safeFrom, listOf(""), newText.split("\n")))
    }

    fun replace(from: Position, to: Position, newText: String) {
        if (from > to) {
            replace(to, from, newText)
            return
        }
        val safeFrom = safe(from)
        val safeTo = safe(to)

        makeChange(Replacement(safeFrom, part(safeFrom, safeTo), newText.split("\n")))
    }

    fun substring(from: Position) = substring(from, endPosition())

    fun substring(from: Position, to: Position): String {

        return if (from < to) {
            val safeFrom = safe(from)
            val safeTo = safe(to)

            if (safeFrom.line == safeTo.line) {
                privateParagraphs[safeFrom.line].substring(safeFrom.column, safeTo.column)
            } else {
                val buffer = StringBuffer()
                // The end part of the "from" paragraph
                buffer.append(privateParagraphs[safeFrom.line].substring(safeFrom.column))
                buffer.append('\n')
                // All of the in between paragraphs
                for (l in safeFrom.line + 1 until safeTo.line) {
                    buffer.append(privateParagraphs[l])
                    buffer.append('\n')
                }
                // The start of the "to" paragraph
                buffer.append(privateParagraphs[safeTo.line].substring(0, safeTo.column))

                buffer.toString()
            }
        } else {
            ""
        }
    }

    fun part(from: Position, to: Position): List<String> {
        val safeFrom = safe(from)
        val safeTo = safe(to)

        return if (safeFrom < safeTo) {
            if (safeFrom.line == safeTo.line) {
                listOf(privateParagraphs[safeFrom.line].substring(safeFrom.column, safeTo.column))
            } else {
                val result = mutableListOf<String>()
                // The end part of the first line
                result.add(privateParagraphs[safeFrom.line].substring(safeFrom.column))

                // All of the in between paragraphs
                for (l in safeFrom.line + 1 until safeTo.line) {
                    result.add(privateParagraphs[l])
                }
                // The start of the "to" paragraph
                result.add(privateParagraphs[safeTo.line].substring(0, safeTo.column))

                result
            }
        } else {
            listOf("")
        }
    }

    private fun makeChange(change: Change) {
        while (canRedo()) {
            history.removeAt(history.size - 1)
        }
        // See if this change can be merged with the previous one.
        // For example, typing a few characters will generate multiple Change objects
        // which should be merged, so that undo deletes all of them in one go.
        val merged = if (historyIndex <= 0) {
            null
        } else {
            change.mergeWith(history[historyIndex - 1])
        }
        if (merged == null) {
            history.add(change)
            historyIndex = history.size
        } else {
            history[historyIndex - 1] = merged
        }
        change.redo()

        while (history.size > maxHistory) {
            history.removeAt(0)
            historyIndex--
        }
    }

    fun clearHistory() {
        history.clear()
        historyIndex = 0
    }

    fun canUndo() = historyIndex > 0

    fun canRedo() = historyIndex < history.size

    fun undo() {
        if (canUndo()) {
            history[historyIndex - 1].undo()
            historyIndex--
        }
    }

    fun redo() {
        if (canRedo()) {
            history[historyIndex].redo()
            historyIndex++
        }
    }

    /**
     * A item in the undo/redo history.
     * All changes to the [TextDocument] are performed using a [Change].
     * Altering the [TextDocument] outside of a [Change] will cause unpredictable behaviour.
     */
    private interface Change {
        fun undo()
        fun redo()
        /**
         * If the two changes cannot be merged, then return null.
         */
        fun mergeWith(older: Change): Change?
    }

    /**
     * A [Change] with a timestamp, so that changes with a delay between them will NOT be merged.
     */
    private abstract inner class TimedChange : Change {
        val time = Date().time
    }

    private inner class AddHighlightRanges(
            val items: List<HighlightRange>
    ) : Change {

        override fun mergeWith(older: Change): Change? = null

        override fun redo() {
            highlightRanges.addAll(items)
        }

        override fun undo() {
            highlightRanges.removeAll(items)
        }
    }

    private inner class RemoveHighlightRanges(
            items: List<HighlightRange>
    ) : Change {

        val items = items.filter { highlightRanges.contains(it) }

        override fun mergeWith(older: Change): Change? = null

        override fun redo() {
            highlightRanges.removeAll(items)
        }

        override fun undo() {
            highlightRanges.addAll(items)
        }
    }


    private class AlteredRange(
            val range: HighlightRange,
            val oldFrom: Position,
            val oldTo: Position,
            val newFrom: Position,
            val newTo: Position
    )

    /**
     * [insert], [delete] and [replace] all create [Replacement]s.
     */
    private inner class Replacement(
            val position: Position,
            val oldText: List<String>,
            val newText: List<String>

    ) : TimedChange() {

        private var firstRedo = true

        private val alteredRanges = mutableListOf<AlteredRange>()

        /**
         * Looks at all of the [highlightRanges], and adjust their from/to positions if this change
         * affects them.
         *
         * If [HighlightRange.transient], then simply make the change, but if not transient, then
         * make a note of how the range was changed, so that redo can reverse it. This is only done
         * when [firstRedo] == true.
         */
        private fun updateRanges(from: Position, removedTo: Position, addedTo: Position) {

            for (r in highlightRanges) {

                fun alterRange(from: Position, to: Position) {
                    if (!r.transient) {
                        if (firstRedo) {
                            alteredRanges.add(AlteredRange(r, r.from, r.to, from, to))
                        }
                    } else {
                        r.from = from
                        r.to = to
                    }
                }

                /**
                 * Given [pos], how should it be adjusted so that it refers to the same part of the document
                 * after the replacement has occurred. (Based on [removedTo] and [addedTo].
                 */
                fun adjust(pos: Position): Position {
                    return if (pos.line == removedTo.line) {
                        if (pos.line == addedTo.line) {
                            // All on the same line
                            Position(pos.line, pos.column + addedTo.column - removedTo.column)
                        } else {
                            // pos is the same line as removeTo, but not the same line as addedTo
                            Position(addedTo.line, addedTo.column + pos.column - removedTo.column)
                        }
                    } else {
                        // pos is not on the same line as removeTo, so we just need to adjust the line
                        Position(pos.line + addedTo.line - removedTo.line, pos.column)
                    }
                }

                if (r.from == from && r.to == removedTo) {
                    // The range is the same size as the deletion, so make it the size of the addition.
                    alterRange(r.from, addedTo)
                } else if (r.from > from && r.to < removedTo) {
                    // The range is completely within the deleted part
                    alterRange(r.from, r.from) // Make is zero size
                } else if (r.to == from) {
                    // The range ends where the replacement starts
                    if (r.expand && removedTo == from) {
                        // Added (no deletion), Include the new text as part of the range
                        alterRange(r.from, addedTo)
                    }
                } else if (r.to < from) {
                    // Before, so no change
                } else if (r.from >= removedTo) {
                    // Range is after the replacement, so adjust the start and end by the same amount.
                    alterRange(adjust(r.from), adjust(r.to))
                } else if (r.from < position && r.to >= removedTo) {
                    // The replaced section is completely inside the range, so increase the range's to.
                    alterRange(r.from, adjust(r.to))
                } else if (removedTo > r.from && removedTo < r.to) {
                    // Cut the front off of the range
                    alterRange(addedTo, adjust(r.to))
                } else if (position < r.to) {
                    // Cut the end off of the range
                    alterRange(r.from, position)
                } else {
                    warn("Hmm, range=${r.from}..${r.to} and removed ${from}..${removedTo}")
                }

            }
        }

        private fun delete(lines: List<String>) {
            require(lines.isNotEmpty())

            if (lines.size == 1) {
                if (lines.first().isEmpty()) return
                val old = privateParagraphs[position.line]
                privateParagraphs[position.line] = old.substring(0, position.column) + old.substring(position.column + lines.first().length)
            } else {
                // Cut off the end of the first line, and join on the end of the last line
                privateParagraphs[position.line] =
                        privateParagraphs[position.line].substring(0, position.column) +
                                privateParagraphs[position.line + lines.size - 1].substring(lines.last().length)

                // Delete the remaining lines
                privateParagraphs.subList(position.line + 1, Math.min(position.line + lines.size, privateParagraphs.size)).clear()
            }
            // I don't think it is possible to delete all the paragraphs, but just in case!
            if (privateParagraphs.isEmpty()) {
                warn("paragraphs were empty")
                privateParagraphs.add("")
            }

            endPosition = Position(privateParagraphs.size - 1, privateParagraphs.last().length)
        }

        private fun add(lines: List<String>) {
            require(lines.isNotEmpty())

            if (lines.size == 1) {
                if (lines.first().isEmpty()) return

                val old = privateParagraphs[position.line]
                privateParagraphs[position.line] =
                        old.substring(0, position.column) +
                                lines.first() +
                                old.substring(position.column)


            } else {
                // Append the first line
                val old = privateParagraphs[position.line]
                val oldEnd = old.substring(position.column) // This will be part of the last line...
                privateParagraphs[position.line] = old.substring(0, position.column) + lines.first()

                // Add the middle lines
                if (lines.size > 2) {
                    privateParagraphs.addAll(position.line + 1, lines.subList(1, lines.size - 1))
                }

                // Add the final line
                privateParagraphs.add(position.line + lines.size - 1, lines.last() + oldEnd)
            }

            endPosition = Position(privateParagraphs.size - 1, privateParagraphs.last().length)
        }

        private fun toPosition(lines: List<String>): Position {
            return if (lines.size == 1) {
                Position(position.line, position.column + lines.first().length)
            } else {
                Position(position.line + lines.size - 1, lines.last().length)
            }
        }

        override fun redo() {
            delete(oldText)
            add(newText)

            val removedTo = toPosition(oldText)
            val addedTo = toPosition(newText)
            replaced(position, addedTo, removedTo)

            updateRanges(position, removedTo, addedTo)
            firstRedo = false
            // Update ranges will NOT change non-transient ranges, so let's do those now...
            alteredRanges.forEach {
                it.range.from = it.newFrom
                it.range.to = it.newTo
            }
        }

        override fun undo() {
            delete(newText)
            add(oldText)

            val removedTo = toPosition(newText)
            val addedTo = toPosition(oldText)
            replaced(position, addedTo, removedTo)

            updateRanges(position, removedTo, addedTo)
            // Update ranges will NOT change non-transient ranges, so let's do those now...
            alteredRanges.forEach {
                it.range.from = it.oldFrom
                it.range.to = it.oldTo
            }
        }

        override fun mergeWith(older: Change): Change? {
            if (older !is Replacement) return null

            if (time - older.time > 1000) return null // Too long a delay for them to be merged.

            fun merge(a: List<String>, b: List<String>): List<String> {
                val result = ArrayList<String>(a.size + b.size)
                result.addAll(a.subList(0, a.size - 1))
                result.add(a.last() + b.first())
                result.addAll(b.subList(1, b.size))
                return result
            }

            fun isNothing(a: List<String>) = a.size == 1 && a.first().isEmpty()

            // Two insertions? The older change may optionally have deleted text. This change cannot have deleted text.
            if (this.position == older.toPosition(older.newText) && isNothing(oldText)) {
                return Replacement(older.position, older.oldText, merge(older.newText, newText))
            }

            // Two deletions? (e.g. press the delete key twice)
            if (isNothing(newText) && isNothing(older.newText) && position == older.position) {
                return Replacement(older.position, merge(older.oldText, oldText), newText)
            }
            // Two deletions (but in the opposite direction to the above (e.g.press backspace twice)
            if (isNothing(newText) && isNothing(older.newText) && toPosition(oldText) == older.position) {
                return Replacement(position, merge(oldText, older.oldText), newText)
            }

            return null
        }

        override fun toString() = "Replacement @ $position = $newText"
    }


    interface Listener {
        /**
         * @param document The document that was altered
         * @param from The start position where text was altered
         * @param to The end of the newly added text
         * @param removedTo Where the end of the removed text was
         *
         * Note, [removedTo] may be useful, in conjunction with [to] to adjust markers etc,
         * but it shouldn't be used to access parts of the document (as it may refer to an invalid position).
         */
        fun replaced(document: TextDocument, from: Position, to: Position, removedTo: Position)
    }

    class Position(val line: Int, val column: Int)
        : Comparable<Position> {

        override fun compareTo(other: Position): Int {
            return if (line == other.line) {
                column - other.column
            } else {
                line - other.line
            }
        }

        override fun equals(other: Any?): Boolean {
            return if (other is Position) {
                line == other.line && column == other.column
            } else {
                false
            }
        }

        override fun hashCode() = Objects.hash(line, column)

        override fun toString() = "($line,$column)"

        operator fun minus(other: Position) = Position(line - other.line, column - other.column)

        operator fun plus(other: Position) = Position(line + other.line, column + other.column)

        fun clamp(min: Position, max: Position) = if (this < min) min else if (this > max) max else this

        companion object {
            val START = Position(0, 0)
        }
    }

}
