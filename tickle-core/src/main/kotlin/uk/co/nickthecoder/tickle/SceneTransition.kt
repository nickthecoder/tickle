package uk.co.nickthecoder.tickle

import org.lwjgl.opengl.GL11.GL_RGB
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.stage.GameStage
import uk.co.nickthecoder.tickle.stage.Stage
import uk.co.nickthecoder.tickle.stage.StageView
import uk.co.nickthecoder.tickle.stage.ZOrderStageView
import uk.co.nickthecoder.tickle.util.RectInt

/**
 * Draws the scene to a [Texture].
 *
 * If [clear] == false, then the scene's background color is NOT used to clear the buffer.
 */
fun drawToTexture(): Texture {

    val aTexture = Texture(Game.instance.window.width, Game.instance.window.height, GL_RGB, null)

    val renderer = Renderer.instance()
    renderer.outputTexture = aTexture
    Game.instance.scene.draw(renderer)
    renderer.outputTexture = null

    return aTexture
}

/**
 * Take a screenshot of the game as a texture when a scene ends.
 * Then, when a new scene has started, draw the old scene on top, to create a scene-transition.
 * e.g. the old scene may slide to the side, and fade as it does so.
 *
 * Here's example code to add to your [Producer].
 *
 *      val sceneTransition = SceneTransition()
 *
 *      override fun sceneEnding() {
 *          sceneTransition.prepareTransition() // Takes a screenshot of the old scene
 *      }
 *
 *      override fun sceneLoaded() {
 *          val actor = sceneTransition.actor
 *          if (actor != null) {
 *              sceneTransition.beginTransition( Fade( actor, 0.5, 0 ) ) // Becomes transparent in 0.5 seconds.
 *          }
 *      }
 *
 * Another common transition, is to slide the old scene to the right. Use this instead of Fade() ...
 *
 *             MoveXBy( actor.position, 0.5, actor.appearance.width() )
 *
 * You can also use [SceneTransition]s for special effects during a game. Here's a snippet which shakes
 * the screen (I use it in DrunkInvaders, when your ship is destroyed) :
 *
 *      sceneTransition.apply {
 *          prepareTransition()
 *          actor.color.alpha = 0.5 // Make the copy of the screen 50% transparent, so we see two copies.
 *          val seconds = 0.03
 *          val distance = 20
 *          beginTransition(
 *               (
 *                   MoveXBy( actor.position, seconds, distance ) then
 *                   MoveXBy( actor.position, seconds*2, -2 * distance ) then
 *                   MoveXBy( actor.position, seconds, distance )
 *               ).repeat(5)
 *           )
 *       }
 *
 * NOTE. You should only have one instance of [SceneTransition] active at any time.
 */
class SceneTransition {

    /**
     * Is the scene's background cleared first. If false, then the resulting texture will
     * contain transparent pixels where the background color would have been.
     */
    var clear = true

    /**
     * An [Actor], whose appearance is the screenshot taken during [prepareTransition].
     * Create an [Action], which move and/ fade this [Actor], and pass that [Action] to
     * [beginTransition] when the new scene has been loaded.
     *
     * This will be null, if [prepareTransition] has yet to be called (i.e. when the very first
     * scene is loaded). It will also be null once the scene-transition is complete
     * (i.e. after the [Action] passed to [beginTransition] has finished).
     */
    var actor: Actor? = null
        private set

    private var texture: Texture? = null
    private var stage: Stage? = null
    private var stageView: StageView? = null

    val stageName = "SceneTransition"
    val viewName = "SceneTransition"

    /**
     * Call this before the old scene has been destroyed, typically from [Producer.sceneEnding].
     */
    fun prepareTransition() {
        // If prepareTransition has been called for a seconds time, without a cleanUp in between,
        // ensure that the old transition has been cleaned.
        // This will happen if the new scene ends before the the scene transition is complete.
        cleanUp()

        val texture = drawToTexture()
        val pose = Pose(texture).apply {
            offsetX = texture.width / 2f
            offsetY = texture.height / 2f
        }
        val costume = Costume(Game.instance.resources)
        costume.events["default"] = CostumeEvent(costume).apply { poses.add(pose) }
        val actor = Actor(costume)
        actor.changeAppearance(pose)

        this.texture = texture
        this.actor = actor
    }

    /**
     * This is automatically called once you action (passed to [beginTransition]) finishes.
     * Therefore, you only need to call this, if you called [prepareTransition], but didn't
     * call [beginTransition].
     */
    fun cleanUp() {
        val scene = Game.instance.scene

        actor?.let { stage?.remove(it) }

        scene.removeStage(stageName)
        scene.removeView(viewName)

        actor = null
        stage = null
        stageView = null

        texture?.destroy()
        texture = null
    }

    /**
     * Call this when the new scene has been loaded, typically from [Producer.sceneLoaded].
     * It will create a new Stage (and View), and adds [actor] to it.
     *
     * The [action] will typical move and/or fade the [actor].
     * When the action is complete, the actor will be deleted, along with the stage, stage view and texture.
     */
    fun beginTransition(action: Action) {
        beginTransition(action, false)
    }

    fun beginTransition(action: Action, below: Boolean) {
        val actor = actor ?: return

        val window = Game.instance.window
        val scene = Game.instance.scene

        val stage = GameStage()
        this.stage = stage

        val stageView = ZOrderStageView().apply {
            rect = RectInt(0, 0, window.width, window.height)
            zOrder = if (below) -1000 else 1000
        }
        this.stageView = stageView

        stage.addView(stageView)

        scene.addStage("SceneTransition", stage)
        scene.addView("SceneTransition", stageView)

        stage.add(actor)
        actor.x = stageView.worldWidth / 2
        actor.y = stageView.worldHeight / 2

        val cleanUpAction = object : Action {
            override fun act() = true
            override fun begin(): Boolean {
                // We run later, because we shouldn't delete views during a tick.
                Game.instance.runLater { cleanUp() }
                return true
            }
        }

        actor.role = ActionRole(action.then(cleanUpAction))
    }

}
