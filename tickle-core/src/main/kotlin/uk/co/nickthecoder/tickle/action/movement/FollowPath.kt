package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.path.Path
import uk.co.nickthecoder.tickle.util.Vector2


/**
 * An [Action], which updates [position] to follow the path in a given number of [seconds].
 */
class FollowPath(val position: Vector2, seconds: Float, val path: Path, ease: Ease) :
    TimedAnimation(seconds, ease) {

    private var currentIndex = 0

    override fun begin(): Boolean {
        return if (path.shapes.isEmpty()) {
            true
        } else {
            position.set(path.shapes[0].start)
            super.begin()
        }
    }

    /**
     */
    override fun storeInitialValue() {
        currentIndex = 0
    }

    override fun update(t: Float) {
        // defensive??? : if (currentIndex >= parts.size) return
        var sectionCache = path.sections[currentIndex]
        // It is possible that we may skip some sections if the time step is large,
        // and the section is short.
        while (t >= sectionCache.endT && currentIndex < path.shapes.size - 1) {
            sectionCache = path.sections[++currentIndex]
        }
        // Note, it is possible for t to be negative, or > 1.
        // (e.g. when using Eases : easeInBack easeOutBack and easeInOutBack)
        // In which case the overshoot is applied to the first/last Shape.

        // We need to convert `t` (which is the ratio along the whole path),
        // to the ratio along the current section.
        // t should be between part.startT and start.endT, and partT should be
        // the amount between those two values. e.g. if t == (startT + endT) / 2,
        // then partT = 0.5 (because we are halfway through the section).
        val partT = (t - sectionCache.startT) / (sectionCache.endT - sectionCache.startT)

        // Finally, update the [position].
        path.sections[currentIndex].shape.along(partT, /*into*/ position)
    }

}
