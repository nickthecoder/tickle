package uk.co.nickthecoder.tickle.graphics.programs

import org.joml.Matrix3x2f
import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.util.IVector2

class StrokeProgram(val mirrored: Boolean) : ShaderProgram(
    VERTEX,
    if (mirrored) MIRRORED else REGULAR,
    3
) {
    val repeatLocation = getUniformLocation("repeat")

    fun setup(texture: Texture, repeat: Float, modelMatrix: Matrix3x2f? = null) {
        enablePositionU()
        setUniform(repeatLocation, repeat)
        setModelMatrix(modelMatrix)
        texture.bind()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        if (!mirrored) {
            // Prevent "bleeding" from one end of the gradient texture to the other end.
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        } else {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        }
    }

    fun draw(points: List<IVector2>, uvs: List<IVector2>, along: Boolean) {
        if (points.size != uvs.size) {
            throw IllegalArgumentException("vertices size ${points.size} != uvs size ${uvs.size}")
        }
        requiredVertices(points.size)
        for ((i, point) in points.withIndex()) {
            val uv = uvs[i]
            Renderer.floatBuffer.put(point.x).put(point.y).put(if (along) uv.x else uv.y)
        }
    }

    companion object {

        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute float u;
            
            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;
            
            varying float t;
            
            void main() {
                t = u;
                gl_Position = vec4( viewMatrix * modelMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val REGULAR = """
            #version 120
            
            uniform float repeat; // Number of times to repeat the gradient.

            varying float t; // 0 = Left/Start 1 = Right/End (for the gradient)

            uniform sampler2D gradient; // The 1D gradient texture
                                 
            void main() {
                // Calculate the color based on `t` value and the gradient texture
                float tColor = repeat * t;
                tColor -= floor(tColor);
                gl_FragColor = texture2D( gradient, vec2(tColor, 0.5) );
            }
            """.trimIndent()

        private val MIRRORED = """
            #version 120
            
            uniform float repeat; // Number of times to repeat the gradient.

            varying float t; // 0 = Left/Start 1 = Right/End (for the gradient)

            uniform sampler2D gradient; // The 1D gradient texture
            
            void main() {
                 // Calculate the color based on `t` value and the gradient texture
                float tColor = repeat * t;
                tColor -= floor(tColor);
                float tMirrored = 1 - abs(tColor - 0.5) * 2;
                gl_FragColor = texture2D( gradient, vec2(tMirrored, 0.5) );
            }
            """.trimIndent()

    }

}
