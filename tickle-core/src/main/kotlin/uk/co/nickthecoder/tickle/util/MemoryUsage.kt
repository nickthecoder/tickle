package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.ActionRole
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.Delay
import uk.co.nickthecoder.tickle.action.Once

class MemoryUsage : ActionRole() {

    override fun createAction(): Action {
        return Once {
            val free = Runtime.getRuntime().freeMemory() / 1000_000
            val total = Runtime.getRuntime().totalMemory() / 1000_000
            val max = Runtime.getRuntime().maxMemory() / 1000_000
            actor.textAppearance?.text = "$free free / $total ($max) MB"
        }.then(Delay(1.0)).forever()
    }
}
