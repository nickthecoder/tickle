package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.graphics.*

class VisibleShape(
    shape: Shape<*>,
    thickness: Float,
    capStyle: CapStyle = CapStyle.BUTT,
    joinStyle: JoinStyle = JoinStyle.MITER,
    strokePaint: StrokePaint = FlatColor(Color.white())
) {

    var shape: Shape<*> = shape
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var thickness: Float = thickness
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var joinStyle: JoinStyle = joinStyle
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var capStyle: CapStyle = capStyle
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var strokePaint: StrokePaint = strokePaint
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var miterMinAngle: Float = 0.349066f
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var roundMinAngle: Float = 0.174533f
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var distanceTolerance: Float = ContiguousShape.DEFAULT_DISTANCE_TOLERANCE
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var angleTolerance: Double = ContiguousShape.DEFAULT_ANGLE_TOLERANCE
        set(v) {
            if (v != field) {
                strokeDirty = true
                field = v
            }
        }

    var strokeDirty = false
        private set(v) {
            if (v) {
                cachedMeshes = null
            }
            field = v
        }

    private var cachedMeshes: List<Mesh>? = null

    val strokeMeshes: List<Mesh>
        get() = cachedMeshes ?: updateStrokeMesh()

    /**
     * Invalidates the cache of triangular meshes.
     *
     * You must call this after you change the geometry of the [shape], but is not necessary after
     * changing any attributes of this class, such as [thickness], [joinStyle] etc.
     */
    fun dirty() {
        shape.dirty()
        strokeDirty = true
    }

    private fun updateStrokeMesh(): List<Mesh> {
        val meshes = createStrokeMeshes()
        cachedMeshes = meshes
        return meshes
    }

    fun createStrokeMeshes(): List<Mesh> {
        val polylines = shape.toPolylines(distanceTolerance, angleTolerance)
        val includeUVs = true
        val strokePaint = strokePaint
        val mirroredAcross = if (strokePaint is GradientAcross) strokePaint.isMirrored else false
        return polylines.map { polyline ->
            polyline.createStrokeMesh(
                thickness, joinStyle, capStyle,
                true, miterMinAngle, roundMinAngle,
                includeUVs = includeUVs,
                mirroredAcross = mirroredAcross
            )
        }
    }

}