/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import org.lwjgl.glfw.GLFW.*
import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.loop.DefaultGameLoop
import uk.co.nickthecoder.tickle.loop.GameLoop
import uk.co.nickthecoder.tickle.misc.ResourcesReader
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.sound.SoundManager
import uk.co.nickthecoder.tickle.stage.StageView
import uk.co.nickthecoder.tickle.stage.View
import uk.co.nickthecoder.tickle.stage.WorldView
import uk.co.nickthecoder.tickle.util.AutoFlushPreferences
import uk.co.nickthecoder.tickle.util.SceneReader
import uk.co.nickthecoder.tickle.util.TagManager
import uk.co.nickthecoder.tickle.util.Vector2
import java.io.File
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.prefs.Preferences


class Game(

    /**
     * The OpenGL Window for the running Game.
     */
    val window: Window,
    /**
     * The collection of textures, poses, costumes etc. which were loaded from the `.tickle` file.
     */
    val resources: Resources

) {

    /**
     * When true, prevent all stage, views and roles from receiving tick events.
     *
     * NOTE. [Director] and [Producer] have their tick methods called whether paused or not
     *
     * If you want some role to continue to tick while paused, then add extra code into your
     * [Director]'s or [Producer]'s preTick, tick or postTick methods, manually calling those
     * role's tick method.
     */
    var paused: Boolean = false

    /**
     * The [Producer] is set from [GameInfo]'s : [GameInfo.producerString] when the Game is started.
     *
     * Set the [Producer] via the `Game Info` section in the `Resources Tree` in Tickle's Editor.
     * Do NOT set this via code!
     */
    var producer: Producer = NoProducer()
        private set

    /**
     * The [Director] for the current scene. This is set each time a new scene is started ([Game.startScene])
     *
     * Note the director is held by [Scene]; this is here for backward compatibility (and will remain).
     * It merely returns [Scene.director] from [scene].
     */
    val director: Director
        get() = scene.director

    /**
     * The name of the current scene (set in [startScene]).
     */
    var sceneName: String = ""
        private set

    /**
     * The name of the previous scene. Initially blank
     */
    var previousSceneName = ""
        private set

    /**
     * Details, such as the stages, and views of the current scene.
     * It is set in [startScene]. Old [Scene] objects will be garbage collected when a new scene is started.
     */
    var scene: Scene = Scene()
        private set

    /**
     * Controls the main `loop` for the Game.
     * You don't need to change, or use this, unless you want a non-standard behaviour.
     * For example, if you want more precise control over the frame rate, or control what to do when frames are dropped,
     * due to running on a slow machine.
     */
    var gameLoop: GameLoop

    /**
     * A measure of time in seconds. Updated once per frame, It is actually just System.nano converted to
     * seconds.
     *
     */
    @Deprecated(message = "Use Clock.seconds")
    var seconds: Double
        get() = Clock.seconds
        set(v) {
            Clock.seconds = v
        }

    /**
     * The actual time spent on the latest `tick`.
     */
    var tickDuration = 1.0 / 60.0

    /**
     * A [Role] (or something), may "capture" the mouse, so that future mouse events are sent to them.
     * For example, a `Button`-like object will capture the mouse when pressed, and release it again when the
     * mouse button is released. In this way, the button will still receieve the mouse events, even when the
     * mouse is NOT over the button.
     *
     * The mouse can only be `captured` by one object. When set, this is the FIRST listener to be informed
     * of the mouse event.
     */
    private var mouseCapturedBy: MouseButtonListener? = null

    /**
     * Similar to [mouseCapturedBy], but for keyboard events.
     */
    private var keyCapturedBy: KeyListener? = null

    private val previousScreenMousePosition = Vector2(-1f, -1f)

    private val currentScreenMousePosition = Vector2()

    internal val mouseListeners = mutableListOf<MouseListener>()

    val preferences by lazy { AutoFlushPreferences(producer.preferencesRoot()) }

    /**
     * When a Game is launched from the Editor, then we need two threads, one for the game, and another
     * for JavaFX application. The JavaFX thread must do the polling, and therefore it sets this to false,
     * and performs the polling itself.
     */
    var pollEvents = true


    init {
        info("Game.init")
        instance = this
        producer = resources.gameInfo.createProducer()

        Clock.seconds = System.nanoTime() / 1_000_000_000.0
        Clock.gameStartSeconds = Clock.seconds

        gameLoop = DefaultGameLoop(this)
        gameLoop.resetStats()
        info("Game.init complete")
    }

    /**
     * Wraps code in a try-catch, with any exceptions forwarded to [GameErrorHandler].
     * In this way, badly coded scripts won't bring down the whole game.
     */
    internal fun tryCatch(owner: Any, action: () -> Unit) {
        try {
            action()
        } catch (e: Exception) {
            GameErrorHandler.instance.error(owner, e)
        }
    }

    /**
     * Starts the Game. This should only be called ONCE.
     */
    fun run(scenePath: String) {
        tryCatch(producer) { producer.begin() }
        glfwPollEvents()
        processRunLater()

        tryCatch(this) { startScene(scenePath) }

        loop()

        // Clean up

        processRunLater()

        // Do NOT delete the renderer, because we will reuse it many times, when running the game from the editor.
        //renderer.delete()

        window.listeners.remove(windowListener)
        tryCatch(producer) { producer.end() }

        quitting = true
    }

    /**
     * The main game-loop. Called from [run]
     */
    private fun loop() {
        while (isRunning()) {
            gameLoop.loopBody()

            if (pollEvents) {
                glfwPollEvents()
            }

            val now = System.nanoTime() / 1_000_000_000.0
            Clock.tickCount++
            tickDuration = now - Clock.seconds
            Clock.seconds = now
        }
        SoundManager.stopAll()
    }

    private fun loadScene(sceneFile: File): Scene {
        return SceneReader.load(sceneFile, resources)
    }

    /**
     * Adds actors from another scene to the current one.
     * Another way of achieving the same thing, is to use the `include` from the `Details` tab of the Scene Editor.
     * However, using [mergeScene] may be more appropriate if you want to add the same scene to many/all
     * of your actual scenes.
     * Call this from [Director.begin].
     *
     * NOTE, If the current scene's layout is different to the layout of the additional scene, then "weird"
     * things may happen. Tickle tries its best, by adding actor to stages with the same NAME, but if there are
     * actors on stages without a similar named stage, then they will be placed on the "default" stage.
     */
    fun mergeScene(scenePath: String): Scene {
        val newScene = SceneReader.load(resources.scenePathToFile(scenePath), resources)
        scene.merge(resources, newScene)
        return newScene
    }


    /**
     * If a new scene is started, while we are still setting up an old one,
     * then don't do anymore initialising of the old scene.
     */
    private var sceneCounter = 0

    /**
     * Start a new scene.
     *
     * NOTE, it is bad form to start a new scene while the existing scene is still being initialised.
     * i.e. from [Role.begin], [Role.activated] or similar methods on [Director] and [Producer].
     * You can often get away with it though! But to be on the safe side,
     * consider using the feather code :
     *
     *      runLater( Game.instance:>startScene.curry( newScenePath ) )
     *
     */
    fun startScene(scenePath: String) {
        Clock.sceneStartSeconds = Clock.seconds
        Clock.tickCount = 0

        // No scene has been started yet, then we don't need to end it!!!
        if (sceneName != "") {
            endScene()
        }
        sceneName = scenePath
        try {
            startScene(SceneReader.load(resources.scenePathToFile(scenePath), resources, mergeIncludes = true))
            //startScene(loadScene(resources.scenePathToFile(scenePath)))
        } catch (e: Exception) {
            sceneName = previousSceneName
            GameErrorHandler.instance.error(this, e)
        }
        previousSceneName = sceneName
    }

    private fun startScene(scene: Scene) {
        // After each part of scene initialisation, we check if the sceneCounter is the same.
        // If it isn't, it means that a new scene has been started, before the old scene has
        // had a chance to fully initialise.
        // In which case, we abort further initialisation of the old scene.
        val currentSceneCounter = ++sceneCounter
        mouseCapturedBy = null
        keyCapturedBy = null

        TagManager.instance.clear()

        this.scene = scene

        // layout
        tryCatch(producer) { producer.layout() }
        if (currentSceneCounter != sceneCounter) return
        tryCatch(director) { director.layout() }
        if (currentSceneCounter != sceneCounter) return

        // Loaded
        tryCatch(producer) { producer.sceneLoaded() }
        if (currentSceneCounter != sceneCounter) return
        tryCatch(director) { director.sceneLoaded() }
        if (currentSceneCounter != sceneCounter) return

        // begin
        tryCatch(producer) { producer.sceneBegin() }
        if (currentSceneCounter != sceneCounter) return
        tryCatch(director) { director.begin() }
        if (currentSceneCounter != sceneCounter) return
        scene.begin()
        if (currentSceneCounter != sceneCounter) return

        // activated
        scene.activated()
        if (currentSceneCounter != sceneCounter) return
        tryCatch(producer) { producer.sceneActivated() }
        if (currentSceneCounter != sceneCounter) return
        tryCatch(director) { director.activated() }
        if (currentSceneCounter != sceneCounter) return

        Clock.seconds = System.nanoTime() / 1_000_000_000.0
        gameLoop.sceneStarted()
    }

    private fun endScene() {
        tryCatch(director) { director.ending() }
        tryCatch(producer) { producer.sceneEnding() }
        scene.end()
        tryCatch(director) { director.ended() }
        tryCatch(producer) { producer.sceneEnded() }

        TagManager.instance.clear()
        mouseCapturedBy = null
        keyCapturedBy = null
    }


    private var quitting = false

    fun quit() {
        quitting = true
    }

    /**
     * Returns true until the game is about to end. The game can end either by calling [quit], or
     * by closing the window (e.g. Alt+F4, or by clicking the window's close button).
     *
     * In both cases, the game doesn't stop immediately. The game loop is completed, allowing
     * everything to end cleanly.
     *
     * NOTE. isRunning ignores the [paused] boolean. i.e. a paused game is still considered to be running.
     */
    fun isRunning() = !quitting && !window.shouldClose()

    fun mouseCapturedBy() = mouseCapturedBy

    /**
     * Called by the [GameLoop]. Do NOT call this directly from elsewhere!
     *
     * If the mouse position has moved, then create a MouseEvent, and call all mouse listeners.
     *
     * Note, if the mouse has been captured, then only the listener that has captured the mouse
     * will be notified of the event.
     */
    internal fun mouseMoveTick() {
        window.getMousePosition(currentScreenMousePosition)

        if (currentScreenMousePosition != previousScreenMousePosition) {
            previousScreenMousePosition.set(currentScreenMousePosition)

            var button = -1
            for (b in 0..2) {
                val state = glfwGetMouseButton(window.handle, b)
                if (state == GLFW_PRESS) {
                    button = b
                }
            }
            val event = MouseEvent(window, button, if (button == -1) ButtonState.UNKNOWN else ButtonState.PRESSED, 0)
            event.windowPosition.set(currentScreenMousePosition)

            mouseCapturedBy?.let { capturedBy ->
                if (capturedBy is MouseListener) {
                    if (capturedBy is View) {
                        capturedBy.windowToViewport(event.windowPosition, event.viewportPosition)
                        if (capturedBy is WorldView) {
                            capturedBy.viewportToWorld(event.viewportPosition, event.worldPosition)
                        }
                    }

                    event.captured = true
                    tryCatch(capturedBy) { capturedBy.onMouseMove(event) }
                    if (!event.captured) {
                        mouseCapturedBy = null
                    }
                    event.consume()
                }
            }
            if (!event.isConsumed()) {
                for (ml in mouseListeners) {
                    if (ml is View) {
                        ml.windowToViewport(event.windowPosition, event.viewportPosition)
                    }
                    ml.onMouseMove(event)
                    if (event.captured) {
                        mouseCapturedBy = ml
                        break
                    }
                    if (event.isConsumed()) break
                }
            }
        }
    }

    fun requestFocus(keyListener: KeyListener?) {
        (keyCapturedBy as? FocusListener)?.lostFocus()
        keyCapturedBy = keyListener
        (keyListener as? FocusListener)?.gainedFocus()
    }

    fun focusCapturedBy() = keyCapturedBy

    fun hasFocus(keyListener: KeyListener): Boolean {
        if (keyCapturedBy === keyListener) return true
        return (keyCapturedBy as? StageView)?.hasFocus(keyListener) ?: false
    }

    fun addMouseListener(listener: MouseListener) {
        mouseListeners.add(listener)
    }

    fun removeMouseListener(listener: MouseListener) {
        mouseListeners.remove(listener)
    }

    /**
     * Game use to implement the WindowListener interface, but this class is used instead becase :
     * * It allows the onKey, onMouseButton etc methods to be private
     * * It is a good place to add [runLater] calls (previously these were inside the [Window] class, which made no sense.
     */
    private val windowListener = object : WindowListener {

        init {
            window.listeners.add(this)
        }

        override fun onKey(event: KeyEvent) {
            runLater { this@Game.onKey(event) }
        }

        override fun onChar(event: CharEvent) {
            runLater { this@Game.onChar(event) }
        }

        override fun onMouseButton(event: MouseEvent) {
            runLater { this@Game.onMouseButton(event) }
        }

        override fun onResize(event: ResizeEvent) {
            runLater { this@Game.onResize(event) }
        }
    }

    private fun onKey(event: KeyEvent) {
        keyCapturedBy?.let {
            event.captured = true
            tryCatch(it) {
                it.onKey(event)
            }
            if (!event.captured) {
                keyCapturedBy = null
            }
        }

        if (event.isConsumed()) return
        tryCatch(producer) { producer.onKey(event) }

        if (event.isConsumed()) return
        tryCatch(director) { director.onKey(event) }
    }

    private fun onChar(event: CharEvent) {
        keyCapturedBy?.let {
            event.captured = true
            tryCatch(it) {
                it.onChar(event)
            }
            if (!event.captured) {
                keyCapturedBy = null
            }
        }
    }

    private fun onMouseButton(event: MouseEvent) {
        mouseCapturedBy?.let {
            if (it is View) {
                it.windowToViewport(event.windowPosition, event.viewportPosition)
            }
            event.captured = true
            tryCatch(it) { it.onMouseButton(event) }
            if (!event.captured) {
                mouseCapturedBy = null
            }
            return
        }

        if (sendMouseButtonEvent(event, producer)) {
            return
        }

        if (sendMouseButtonEvent(event, director)) {
            return
        }

        if (event.state == ButtonState.PRESSED) {
            // TODO Need to iterate BACKWARDS
            scene.views().forEach { view ->
                view.windowToViewport(event.windowPosition, event.viewportPosition)
                if (sendMouseButtonEvent(event, view)) {
                    return
                }
            }

        }
    }

    private fun sendMouseButtonEvent(event: MouseEvent, to: MouseButtonListener): Boolean {
        tryCatch(to) { to.onMouseButton(event) }
        if (event.captured) {
            event.captured = false
            mouseCapturedBy = to
            previousScreenMousePosition.set(event.windowPosition)
        }
        return event.isConsumed()
    }

    private fun onResize(event: ResizeEvent) {
        tryCatch(producer) { producer.layout() }
        tryCatch(director) { director.layout() }
        tryCatch(producer) { producer.onResize(event) }
        tryCatch(director) { director.onResize(event) }
    }

    /**
     * Uses a ConcurrentLinkedQueue, rather than a simple list, so that [runLater] can be called within
     * a runLater lambda without fear of a concurrent modification exception.
     */
    private var runLaterQueue = ConcurrentLinkedQueue<Runnable>()

    /**
     * Called by the [GameLoop] (do NOT call this directly from elsewhere!)
     *
     * Runs all of the lambdas sent to [runLater] since the last call to [processRunLater].
     */
    internal fun processRunLater() {
        var entry = runLaterQueue.poll()
        while (entry != null) {
            entry.run()
            entry = runLaterQueue.poll()
        }
    }

    fun runLater(runnable: Runnable) {
        runLaterQueue.add(runnable)
    }


    /**
     * Load the window's size or maximised state
     *
     * See [saveWindowSize]
     */
    fun loadWindowSize() {
        val preferences = preferences.node("window")
        val isMaximised = preferences.getBoolean("isMaximised", true)
        if (isMaximised) {
            window.maximise()
        } else {
            val width = preferences.getInt("width", window.width)
            val height = preferences.getInt("height", window.height)
            window.resize(width, height)
            window.center()
        }
    }


    /**
     * Save the window's size or maximised state.
     *
     * See [loadWindowSize]
     */
    fun saveWindowSize() {
        val preferences = preferences.node("window")
        if (!window.fullScreen) {
            if (window.maximised) {
                preferences.putBoolean("isMaximised", true)
                // Don't overwrite the width/height values.
            } else {
                preferences.putBoolean("isMaximised", false)
                preferences.putInt("width", window.width)
                preferences.putInt("height", window.height)
            }
        }
    }

    companion object {

        lateinit var instance: Game

        /**
         * After parsing the command line arguments, start the game.
         *
         * NOTE. Game developers should 'NOT' call this directly
         * (unless you are using a non-scripted language and have written your own 'main' function).
         */
        fun play(resourcesFile: File, sceneName: String?, fullScreen: Boolean?) {

            // Play the game without starting the JavaFX GUI.
            val log = TickleErrorHandler.instance
            log.info("Starting OpenGL")
            OpenGL.begin()

            onOpenGLThread {

                try {
                    log.info("Loading GameInfo")
                    val gameInfo = ResourcesReader.loadGameInfo(resourcesFile)

                    // There are 3 sources for the "isFullScreen" boolean.
                    // The order in which they are considered are :
                    //     1) Command line options --windowed / --fullscreen
                    //     2) Preferences (See PersistentOptions)
                    //     3) GameInfo.fullScreen
                    // Note, when launching the game from the Editor, it is ALWAYS windowed.

                    val fullScreen2 = fullScreen ?: Preferences.userNodeForPackage(Game::class.java)
                        .node(resourcesFile.nameWithoutExtension)
                        .node("options") // This must be the same node name as in PersistentOptions
                        .getBoolean("isFullScreen", gameInfo.fullScreen)

                    val window = with(gameInfo) {
                        Window(title, width, height, resizable, fullScreen = fullScreen2)
                    }
                    log.info("Loading resources")
                    val resources = Resources()
                    resources.load(resourcesFile)

                    log.info("Creating game object")
                    val game = Game(window, resources)
                    val scenePath: String = sceneName ?: resources.sceneFileToPath(resources.gameInfo.initialScenePath)
                    log.info("Showing OpenGL Window")
                    window.show()
                    log.info("Running the game")
                    game.run(scenePath)
                    log.info("Game ended")

                    // Clean up OpenGL and OpenAL
                    log.info("Cleaning up OpenGL")
                    window.delete()
                } catch (e: Exception) {
                    log.error(e)
                } finally {
                    OpenGL.end()
                    log.info("OpenGL ended")
                }
            }
        }

        fun printThreads() {
            println("")
            println("Threads : ")
            for (thread in Thread.getAllStackTraces().keys) {
                val prefix = if (!thread.isAlive) {
                    "X"
                } else if (thread === Thread.currentThread()) {
                    "#"
                } else if (thread.isDaemon) {
                    "."
                } else {
                    "*"
                }
                println("$prefix $thread")
            }
            println("")
            println("Key")
            println("  # : This thread. Expect exactly one.")
            println("  X : Dead.")
            println("  - : Deamon thread, which should not cause problems")
            println("  * : Still running - may cause problems.")
            println("      However, it is normal for a JavaFX thread to still be running")
            println("      when using the Launcher Window or the Editor")
            println("")

        }
    }

}
