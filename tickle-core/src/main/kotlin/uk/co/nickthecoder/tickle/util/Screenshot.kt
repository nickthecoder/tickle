package uk.co.nickthecoder.tickle.util

import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.info
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.awt.image.BufferedImage
import java.io.File
import java.nio.ByteBuffer
import javax.imageio.ImageIO


/**
 * Makes a Texture from the contents of the Window.
 *
 * Code inspired from :
 * https://stackoverflow.com/questions/13819030/how-to-make-a-simple-screenshot-method-using-lwjgl
 */
class Screenshot private constructor() {

    companion object {
        @JvmStatic
        fun save(window: Window, toFile: File) {
            save(window, toFile, 1.0)
        }

        @JvmStatic
        fun localFile(filename: String): File {
            if (File(filename).isAbsolute || filename.contains("..")) {
                throw IllegalArgumentException("Only relative filenames are allowed for security reasons")
            }
            return File(Game.instance.resources.resourceDirectory, filename)
        }

        @JvmStatic
        fun saveWidth(window: Window, filename: String, width: Int) {
            val file = localFile(filename)
            val scale = width.toDouble() / Game.instance.window.width.toDouble()
            save(window, file, scale)
        }

        @JvmStatic
        fun saveHeight(window: Window, filename: String, height: Int) {
            val file = localFile(filename)
            val scale = height.toDouble() / Game.instance.window.height.toDouble()
            save(window, file, scale)
        }

        @JvmStatic
        fun save(window: Window, toFile: File, scale: Double) {
            OpenGL.ensureOpenGLThread()
            info("Creating Screenshot ${window.width} x ${window.height}")

            //Creating an rbg array of total pixels
            val pixels = IntArray(window.width * window.height)

            // allocate space for RBG pixels
            val fb: ByteBuffer = ByteBuffer.allocateDirect(window.width * window.height * 4)

            // grab a copy of the current frame contents as RGB
            glReadPixels(0, 0, window.width, window.height, GL_RGB, GL_UNSIGNED_BYTE, fb)

            val imageIn = BufferedImage(window.width, window.height, BufferedImage.TYPE_INT_RGB)
            // convert RGB data in fb to integer array
            var bindex: Int
            for (i in pixels.indices) {
                bindex = i * 3
                pixels[i] = (fb.get(bindex + 0).toInt() shl 16) +
                        (fb.get(bindex + 1).toInt() shl 8) +
                        (fb.get(bindex + 2).toInt() shl 0)
            }
            //Allocate colored pixel to buffered Image
            imageIn.setRGB(0, 0, window.width, window.height, pixels, 0, window.width)

            // Scale the image, as well as flipping it up the right way up.
            val atScaled = AffineTransform.getScaleInstance(scale, -scale)
            atScaled.translate(0.0, -imageIn.getHeight(null).toDouble())
            val opScaled = AffineTransformOp(atScaled, AffineTransformOp.TYPE_BICUBIC)
            val imageOut = opScaled.filter(imageIn, null)

            ImageIO.write(imageOut, "png", toFile)
            info("Saved Screenshot to $toFile")
        }
    }
}
