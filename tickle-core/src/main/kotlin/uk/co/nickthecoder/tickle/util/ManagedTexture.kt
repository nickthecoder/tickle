/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.GL_RGBA
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.misc.PixelArray
import uk.co.nickthecoder.tickle.warn

/**
 * A Texture for holding many [Pose]s, which can be dynamically amended
 * i.e. new Poses can be added into spare space within the texture.
 *
 * The constructor is given the `initial` size of the [Texture],
 * but when adding a [Pose], if there is no suitable spare space,
 * then the texture is made larger.
 *
 * The space from removed [Pose]s __is__ reused, but fragmentation can occur.
 * e.g. if a pose is removed, and then a smaller pose is added, it will reuse
 * the space, but maybe the `margin` isn't enough to fit any more poses.
 *
 * Even when [Pose]s are not removed, the packing of the poses is very simplistic,
 * and there can still be lots of unused space in the [Texture].
 *
 * In all of my use cases, fragmentation isn't a problem, so I haven't bothered
 * to perfect the algorithm. If this were a problem, then an option to `repack`
 * would probably suffice. (Copy the texture, remove all the poses, and put
 * them back in again).
 *
 * NOTE. I've had problems with crashes in the past, when the texture is resized.
 * This was caused by the buffer sent to [Texture.write] not being flipped.
 *
 */
class ManagedTexture(width: Int, height: Int) {

    private val spareSpace = mutableListOf<YDownRect>()

    val texture = createTexture(width, height)

    init {
        spareSpace.add(YDownRect(0, 0, width, height))
    }

    fun remove(pose: Pose) {
        if (pose.texture != texture) throw IllegalStateException("This pose does not use this texture")
        addSpace(YDownRect(pose.pixelRect.left, pose.pixelRect.top, pose.pixelRect.right, pose.pixelRect.bottom))
    }

    fun add(image: PixelArray): Pose {
        val space = findSpareSpace(image.width, image.height)
        val xGap = space.width - image.width
        val yGap = space.height - image.height

        spareSpace.remove(space)
        val gap1: YDownRect
        val gap2: YDownRect

        // Do we want to cut the spare space horizontally or vertically?
        if (xGap > yGap) {
            // X2
            // 12
            gap1 = YDownRect(space.left, space.top + image.height, space.left + image.width, space.bottom)
            gap2 = YDownRect(space.left + image.width, space.top, space.right, space.bottom)
        } else {
            // X1
            // 22
            gap1 = YDownRect(space.left + image.width, space.top, space.right, space.top + image.height)
            gap2 = YDownRect(space.left, space.top + image.height, space.right, space.bottom)
        }
        if (gap1.width != 0 && gap1.height != 0) {
            spareSpace.add(gap1)
        }
        if (gap2.width != 0 && gap2.height != 0) {
            spareSpace.add(gap2)
        }

        val texturePixels = PixelArray(texture)
        //texturePixels.blit(image)
        // Copy the image data onto texturePixels
        for (y in 0 until image.height) {
            val py = y + space.top
            for (x in 0 until image.width) {
                val px = x + space.left
                texturePixels.setPixel(px, py, image.pixelAt(x, y))
            }
        }
        // Update the texture with the new image
        texture.write(texture.width, texture.height, texturePixels.toBuffer())

        // Create a pose
        val rect = YDownRect(space.left, space.top, space.left + image.width, space.top + image.height)
        val pose = Pose(texture, rect)
        return pose
    }

    private fun addSpace(rect: YDownRect) {
        // Can this be merged with an existing space?
        for (ss in spareSpace) {
            val merged = merge(ss, rect)
            if (merged != null) {
                spareSpace.remove(ss)
                // Recurse, as this merged rectangle could be merged with others
                addSpace(merged)
                return
            }
        }
        // Nope, couldn't be merged.
        spareSpace.add(rect)
    }

    private fun merge(a: YDownRect, b: YDownRect): YDownRect? {
        if (a.top == b.top && a.bottom == b.bottom && (a.right == b.left || a.left == b.right)) {
            return YDownRect(Math.min(a.left, b.left), a.top, Math.max(a.right, b.right), a.bottom)
        }
        if (a.left == b.left && a.right == b.right && (a.top == b.bottom || a.bottom == b.top)) {
            return YDownRect(a.left, Math.min(a.top, b.top), a.right, Math.max(a.bottom, b.bottom))
        }
        return null
    }

    private fun findSpareSpace(w: Int, h: Int): YDownRect {
        var best: YDownRect? = null
        var bestGap: Int = Int.MAX_VALUE

        for (ss in spareSpace) {
            val dx = ss.width - w
            val dy = ss.height - h
            if (dx >= 0 && dy >= 0) {
                val gap = Math.max(dx, dy)
                if (gap < bestGap) {
                    best = ss
                    bestGap = gap
                }
            }
        }
        if (best == null) {
            // Damn, there's no suitable gap
            return resize(h)
        } else {
            return best
        }
    }

    private fun resize(extraHeight: Int): YDownRect {
        warn("ManagedTexture needs resizing. Consider increasing the initial size.")

        val newSpace = YDownRect(0, texture.height, texture.width, texture.height + extraHeight)
        spareSpace.add(newSpace)

        val oldArray = texture.read()
        val newArray = ByteArray(texture.width * (texture.height + extraHeight) * 4)

        for (i in 0 until oldArray.size) {
            newArray[i] = oldArray[i]
        }
        for (i in oldArray.size until newArray.size) {
            newArray[i] = 0
        }
        val direct = BufferUtils.createByteBuffer(newArray.size)
        for ( i in 0 until newArray.size ) {
            direct.put( newArray[i] )
        }
        texture.write(texture.width, texture.height + extraHeight, direct)
        return newSpace
    }

    companion object {
        private fun createTexture(width: Int, height: Int): Texture {
            val bb =  BufferUtils.createByteBuffer(width * height * 4)
            bb.clear()
            return Texture(width, height, GL_RGBA, bb)
        }
    }
}
