/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.stage

import uk.co.nickthecoder.tickle.resources.AutoScale
import uk.co.nickthecoder.tickle.util.Margins
import uk.co.nickthecoder.tickle.util.RectInt
import uk.co.nickthecoder.tickle.util.Vector2

class FlexPosition {

    var autoScale = AutoScale.NONE

    /**
     * If [autoScale] is [AutoScale.LETTERBOX], then this determines where the margins go.
     * 0 = left/bottom 0.5 = center 1 = right/top
     */
    var letterboxAlignment = Vector2(0.5f, 0.5f)
    var enableAutoPosition = false

    var hAlignment: FlexHAlignment = FlexHAlignment.LEFT
    var hPosition: Double = 0.5
    var leftRightMargin: Int = 0
    var width: Int? = null
    var widthRatio: Double? = null

    var vAlignment: FlexVAlignment = FlexVAlignment.TOP
    var vPosition: Double = 0.5
    var topBottomMargin: Int = 0
    var height: Int? = null
    var heightRatio: Double? = null


    fun width(totalWidth: Int): Int {
        return width ?: widthRatio?.let {
            (totalWidth / it).toInt()
        } ?: totalWidth - if (hAlignment == FlexHAlignment.MIDDLE) 0 else leftRightMargin
    }

    fun left(totalWidth: Int): Int {

        return when (hAlignment) {
            FlexHAlignment.LEFT -> leftRightMargin
            FlexHAlignment.RIGHT -> right(totalWidth) - width(totalWidth)
            FlexHAlignment.MIDDLE -> ((hPosition * totalWidth - width(totalWidth) / 2)).toInt()
        }
    }

    fun right(totalWidth: Int): Int {

        return when (hAlignment) {
            FlexHAlignment.LEFT -> left(totalWidth) + width(totalWidth)
            FlexHAlignment.RIGHT -> totalWidth - leftRightMargin
            FlexHAlignment.MIDDLE -> ((hPosition * totalWidth + width(totalWidth) / 2)).toInt()
        }
    }


    fun height(totalHeight: Int): Int {
        return height ?: heightRatio?.let {
            (totalHeight / it).toInt()
        } ?: totalHeight - if (vAlignment == FlexVAlignment.MIDDLE) 0 else topBottomMargin
    }

    fun bottom(totalHeight: Int): Int {
        return when (vAlignment) {
            FlexVAlignment.BOTTOM -> topBottomMargin
            FlexVAlignment.TOP -> top(totalHeight) - height(totalHeight)
            FlexVAlignment.MIDDLE -> ((vPosition * totalHeight - height(totalHeight) / 2)).toInt()
        }
    }

    fun top(totalHeight: Int): Int {

        return when (vAlignment) {
            FlexVAlignment.BOTTOM -> bottom(totalHeight) + height(totalHeight)
            FlexVAlignment.TOP -> totalHeight - topBottomMargin
            FlexVAlignment.MIDDLE -> ((vPosition * totalHeight + height(totalHeight) / 2)).toInt()
        }
    }

    fun calculateRectangle(totalWidth: Int, totalHeight: Int) =
        RectInt(left(totalWidth), bottom(totalHeight), right(totalWidth), top(totalHeight))

    fun calculateMargins(stageView: View, defaultRect: RectInt): Margins {
        if (autoScale == AutoScale.BEST_FIT) {
            val scaleX = stageView.width.toFloat() / defaultRect.width.toFloat()
            val scaleY = stageView.height.toFloat() / defaultRect.height.toFloat()
            if (scaleX > scaleY) {
                // Fit in the Y directions, with margins on the left and right
                val margin = ((stageView.width.toFloat() - defaultRect.width.toFloat() * scaleY)).toInt()
                return Margins(margin * letterboxAlignment.x, 0f, margin * (1 - letterboxAlignment.x), 0f)
            } else {
                // Fit in the X directions, with margins at the top and bottom
                val margin = ((stageView.height.toFloat() - defaultRect.height.toFloat() * scaleX)).toInt()
                return Margins(0f, margin * letterboxAlignment.y, 0f, margin * (1 - letterboxAlignment.y))
            }
        } else {
            return Margins(0f, 0f, 0f, 0f)
        }
    }


    fun adjustScale(stageView: WorldView, defaultRect: RectInt) {
        // We don't want to change the scale.
        // The game writer may be calculating the scale manually, so we should not change it back to 1.0.
        if (autoScale == AutoScale.NONE) return

        val newRect = RectInt(stageView.rect)
        val scaleX = newRect.width.toFloat() / defaultRect.width
        val scaleY = newRect.height.toFloat() / defaultRect.height

        val scale: Float = when (autoScale) {
            AutoScale.NONE -> {
                // Never gets here, but it is good practice to cover all cases of an enum.
                // so that when new values are added, this code will be flagged as incomplete.
                1f
            }
            AutoScale.LETTERBOX -> {
                Math.min(scaleX, scaleY)
            }
            AutoScale.CROP -> {
                Math.max(scaleX, scaleY)
            }
            AutoScale.BEST_FIT -> {
                Math.min(scaleX, scaleY)
            }
            AutoScale.FIT_X -> {
                scaleX
            }
            AutoScale.FIT_Y -> {
                scaleY
            }
            AutoScale.FIT_AVERAGE -> {
                (scaleX + scaleY) / 2
            }
            AutoScale.FIT_GEOMETRIC -> {
                Math.sqrt((scaleX * scaleY).toDouble()).toFloat()
            }
        }

        // LETTERBOX is a special case - we also make the view narrower or shorter
        if (autoScale == AutoScale.LETTERBOX) {
            if (scaleX > scaleY) {
                // Make the view narrower, with margins on the left and right
                val margin = ((newRect.width.toDouble() - defaultRect.width.toDouble() * scale)).toInt()
                stageView.rect.left += (margin * letterboxAlignment.x).toInt()
                stageView.rect.right -= (margin * (1 - letterboxAlignment.x)).toInt()
            } else {
                // Make the view shorter, with margins at the top and bottom
                val margin = ((newRect.height.toDouble() - defaultRect.height.toDouble() * scale) / 2).toInt()
                stageView.rect.bottom += (margin * letterboxAlignment.y).toInt()
                stageView.rect.top -= (margin * (1 - letterboxAlignment.y)).toInt()
            }
        }

        stageView.scaleXY = scale
    }

    fun copy () : FlexPosition {
        val copy = FlexPosition()
        copy.autoScale = autoScale
        copy.enableAutoPosition = enableAutoPosition
        copy.letterboxAlignment.set(letterboxAlignment)

        copy.hAlignment = hAlignment
        copy.hPosition = hPosition
        copy.leftRightMargin = leftRightMargin
        copy.width = width

        copy.vAlignment = vAlignment
        copy.vPosition = vPosition
        copy.topBottomMargin = topBottomMargin
        copy.height = height
        copy.heightRatio = heightRatio

        return copy
    }

    override fun toString(): String {
        return "hAlign=$hAlignment hPosition=$hPosition leftRightMargin=$leftRightMargin width=$width widthRatio=$widthRatio\n" +
                " valign=$vAlignment vPosition=$vPosition topBottomMargin=$topBottomMargin height=$height heightRation=$heightRatio"
    }
}

enum class FlexHAlignment {
    LEFT, MIDDLE, RIGHT
}

enum class FlexVAlignment {
    TOP, MIDDLE, BOTTOM
}
