package uk.co.nickthecoder.tickle.graphics.programs

import org.joml.Matrix3x2f
import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.util.IVector2

/**
 * Same as [StrokeProgram], but for a flat color.
 */
class FlatStrokeProgram() : ShaderProgram(
    VERTEX, REGULAR,
    2
) {
    val colorLocation = getUniformLocation("color")

    fun setup(color: Color, modelMatrix: Matrix3x2f? = null) {
        enablePosition()
        setModelMatrix(modelMatrix)
        setUniform(colorLocation, color)
    }

    fun draw(points: List<IVector2>) {
        requiredVertices(points.size)
        for (point in points) {
            Renderer.floatBuffer.put(point.x).put(point.y)
        }
    }

    companion object {

        private val VERTEX = """
            #version 120

            attribute vec2 position;
            
            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;
                        
            void main() {
                t = u;
                gl_Position = vec4( viewMatrix * modelMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val REGULAR = """
            #version 120
            
            varying vec4 color;
                                 
            void main() {
                gl_FragColor = color;
            }
            """.trimIndent()
    }

}
