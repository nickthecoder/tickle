package uk.co.nickthecoder.tickle.control

import org.lwjgl.opengl.EXTFramebufferObject.*
import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.action.Delay
import uk.co.nickthecoder.tickle.action.Once
import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.misc.TickleClipboard
import uk.co.nickthecoder.tickle.misc.clamp
import uk.co.nickthecoder.tickle.misc.max
import uk.co.nickthecoder.tickle.misc.min
import uk.co.nickthecoder.tickle.resources.FontResource
import uk.co.nickthecoder.tickle.util.*

/**
 * Allows the player to edit text while playing a game (i.e. it is not part of the editor, and doesn't use JavaFX).
 *
 * Create a Costume based on a font (not a Pose), and use [TextArea] as the Role.
 * Note, the font MUST be fixed width (I designed it to be used as a code editor!).
 *
 * Then add an Actor to your scene using that Costume.
 * Set the width and height appropriately within the scene editor.
 *
 * Note that the font styles (such as alignment and the text/outline color will be ignored).
 *
 * This is still in active development. The API is likely to change. Bugs/missing features :
 *
 * * No Search (and replace) tool
 *
 * And here are bugs/missing features I don't plan on implementing any time soon :
 *
 * * Variable width fonts aren't supported.
 * * Highlight ranges can only change the foreground/background color. It cannot change styles such as bold, italics.
 * * No line wrap option
 *
 */
open class TextArea

    : AbstractRole(), MouseListener, FocusListener, TextDocument.Listener {

    /**
     * If you want to have two views of the same text (e.g. split horizontally of vertically),
     * then set the document of the 2nd TextArea to the document of the first.
     */
    var document: TextDocument = TextDocument()
        set(v) {
            field = v
            dirty = true
        }

    @Attribute
    var width = 640f
        set(v) {
            field = v
            texture = null
            dirty = true
        }

    @Attribute
    var height = 480f
        set(v) {
            field = v
            texture = null
            dirty = true
        }

    var fontResource: FontResource? = null
        set(v) {
            v ?: throw IllegalArgumentException("Cannot be null")
            field = v
            fontWidth = v.fontTexture.advance('M')
            fontHeight = v.fontTexture.lineHeight
            dirty = true
        }

    private var fontWidth = 10f
    private var fontHeight = 10f

    var text
        set(v) {
            document.text = v
            caretPosition = document.position(0, 0)
        }
        get() = document.text


    @Attribute
    var showLineNumbers = true
        set(v) {
            field = v
            dirty = true
        }

    internal var texture: Texture? = null

    private val bufferId = glGenFramebuffersEXT()

    @Attribute
    var background = Color.white()

    @Attribute
    var foreground = Color.black()

    @Attribute
    var focusColor = Color.create("#5974AB") // Blueish

    @Attribute
    var caretColor = Color(0.9, 0.1, 0.1) // Redish

    @Attribute
    var selectionForeground = Color.white()

    @Attribute
    var selectionBackground = Color.create("#5974AB") // Blueish

    @Attribute
    var lineNumberForeground = Color.create("#888888")

    @Attribute
    var lineNumberBackground = Color.create("#eeeeee")

    @Attribute
    var scrollbarColor = Color.create("#eeeeee66")

    /**
     * The width of the gutter (where line numbers are displayed
     */
    internal var gutterWidth = 0f

    @Attribute
    var marginTop = 4
        set(v) {
            field = v
            dirty = true
        }
    @Attribute
    var marginRight = 12
        set(v) {
            field = v
            dirty = true
        }
    @Attribute
    var marginBottom = 12
        set(v) {
            field = v
            dirty = true
        }
    @Attribute
    var marginLeft = 4
        set(v) {
            field = v
            dirty = true
        }

    /**
     * Whenever you change the [caretPosition], the selection will be cleared.
     * If you attempt to set to position outside the range 0..text.length, then the value will be
     * clamped within the valid range (0 or text.length).
     */
    var caretPosition = TextDocument.Position(0, 0)
        set(v) {
            if (markPosition != field) dirty = true
            field = document.safe(v)
            markPosition = field
        }


    /**
     * In conjunction with caretPosition, forms a selection.
     * There is no selection if caretPosition == markPosition.
     * Note, the markPosition can be < = or > caretPosition
     */
    private var markPosition = caretPosition
        set(v) {
            if (v != caretPosition) dirty = true
            field = v
        }

    private val selectionHighlight = Highlight(selectionForeground, selectionBackground, 100)

    private var caretVisible = false

    /**
     * Blinks the caret. Note that the delays are not the same.
     */
    private var caretAction = Delay(0.6)
        .then( Once { caretVisible = false } )
        .then( Delay(0.4))
        .then( Once { caretVisible = true } )
        .forever()

    private var scrollX = 0f
        set(v) {
            field = Math.max(0f, v)
            dirty = true
        }

    private var scrollY = 0f
        set(v) {
            val max =
                Math.max(0f, (document.lineCount() - (height - marginTop - marginBottom) / fontHeight) * fontHeight)
            field = clamp(v, 0f, max)
            dirty = true
        }

    /**
     * The width of the Y scrollbar and height of the X scrollbar
     */
    var scrollbarSize = 10f
        set(v) {
            field = v
            dirty = true
        }

    /**
     * The amount of document visible in the x direction. 1.0 means all is visible (no x scroll bar necessary)
     */
    private var scrollXRatio = 1f

    /**
     * The width of the longest line. Used for the x scroll bar.
     */
    private var maxLineWidth = 0f

    /**
     * The amount of document visible in the y direction. 1.0 means all is visible (no y scroll bar necessary)
     */
    private var scrollYRatio = 1f

    private var dirty = true

    /**
     * A map of [Input]s, and code to perform when a matching event is fired.
     */
    private val actions = hashMapOf(

            left to {
                caretPosition = if (caretPosition != markPosition) {
                    min(caretPosition, markPosition)
                } else {
                    document.addColumns(caretPosition, -1)
                }
                ensureCaretVisible()
            },

            right to {
                caretPosition = if (caretPosition != markPosition) {
                    max(caretPosition, markPosition)
                } else {
                    document.addColumns(caretPosition, 1)
                }
                ensureCaretVisible()
            },

            up to {
                caretPosition = document.addLines(caretPosition, -1)
                ensureCaretVisible()
            },

            down to {
                caretPosition = document.addLines(caretPosition, 1)
                ensureCaretVisible()
            },

            pageUp to {
                val lines = (height / fontHeight).toInt()
                scrollY -= height
                caretPosition = document.addLines(caretPosition, -lines)
                ensureCaretVisible()
            },
            pageDown to {
                val lines = (height / fontHeight).toInt()
                scrollY += height
                caretPosition = document.addLines(caretPosition, lines)
                ensureCaretVisible()
            },
            growPageUp to {
                val lines = (height / fontHeight).toInt()
                scrollY -= height
                setSelection(markPosition, document.addLines(caretPosition, -lines))
                ensureCaretVisible()
            },
            growPageDown to {
                val lines = (height / fontHeight).toInt()
                scrollY += height
                setSelection(markPosition, document.addLines(caretPosition, lines))
                ensureCaretVisible()
            },

            wordLeft to { jumpWord(-1, false) },
            wordRight to { jumpWord(1, false) },
            growWordLeft to { jumpWord(-1, true) },
            growWordRight to { jumpWord(1, true) },

            startOfLine to {
                caretPosition = document.position(caretPosition.line, 0)
                ensureCaretVisible()
            },

            endOfLine to {
                caretPosition = document.position(caretPosition.line, Int.MAX_VALUE)
                ensureCaretVisible()
            },

            growStartOfLine to {
                setSelection(markPosition, document.position(caretPosition.line, 0))
                ensureCaretVisible()
            },
            growEndOfLine to {
                setSelection(markPosition, document.position(caretPosition.line, Int.MAX_VALUE))
                ensureCaretVisible()
            },

            startOfText to {
                caretPosition = document.position(0, 0)
                ensureCaretVisible()
            },

            endOfText to {
                caretPosition = document.endPosition()
                ensureCaretVisible()
            },

            growLeft to {
                setSelection(markPosition, document.addColumns(caretPosition, -1))
                ensureCaretVisible()
            },

            growRight to {
                setSelection(markPosition, document.addColumns(caretPosition, 1))
                ensureCaretVisible()
            },

            growUp to {
                setSelection(markPosition, document.position(caretPosition.line - 1, caretPosition.column))
                ensureCaretVisible()
            },

            growDown to {
                setSelection(markPosition, document.position(caretPosition.line + 1, caretPosition.column))
                ensureCaretVisible()
            },

            enter to {
                replaceSelection("\n")
            },

            backspace to {
                if (markPosition == caretPosition) {
                    setSelection(document.addColumns(caretPosition, -1), caretPosition)
                }
                replaceSelection("")
                ensureCaretVisible()
            },

            delete to {
                if (markPosition == caretPosition) {
                    setSelection(document.addColumns(caretPosition, 1), caretPosition)
                }
                replaceSelection("")
                ensureCaretVisible()
            },

            indent to {
                indent()
                ensureCaretVisible()
            },

            unindent to {
                unindent()
                ensureCaretVisible()
            },

            undo to {
                document.undo()
                ensureCaretVisible()
            },
            redo to {
                document.redo()
                ensureCaretVisible()
            },

            copy to {
                val selection = getSelection()
                TickleClipboard.instance.set(document.substring(selection.first, selection.second))
                ensureCaretVisible()
            },
            cut to {
                val selection = getSelection()
                TickleClipboard.instance.set(document.substring(selection.first, selection.second))
                replaceSelection("")
                ensureCaretVisible()
            },
            paste to {
                replaceSelection(TickleClipboard.instance.get())
                ensureCaretVisible()
            },

            selectAll to {
                setSelection(document.position(0, 0), document.endPosition())
            },
            selectNone to {
                setSelection(caretPosition, caretPosition)
            },

            gotoLine to {
                GotoLine.show(this)
            }


    )


    override fun begin() {
        super.begin()
        document.addListener(this)

        val a = actor.appearance
        if (a is TextAppearance) {
            fontResource = a.textStyle.fontResource
            text = a.text
            document.clearHistory()
        }
    }

    override fun end() {
        document.removeListener(this)
        glDeleteFramebuffersEXT(bufferId)
    }


    override fun tick() {
        if (dirty) {
            updateTexture(Renderer.instance())
        }
        if (hasFocus()) {
            caretAction.act()
        }
    }

    fun ensureCaretVisible() {
        val topLine = scrollY / fontHeight
        if (caretPosition.line < topLine) {
            scrollY -= (topLine - caretPosition.line) * fontHeight
        } else {
            val bottomLine = topLine + (height - marginTop - marginBottom) / fontHeight - 1
            if (caretPosition.line > bottomLine) {
                scrollY += (caretPosition.line - bottomLine) * fontHeight
            }
        }

        val leftColumn = scrollX / fontWidth
        if (caretPosition.column < leftColumn) {
            scrollX -= (leftColumn - caretPosition.column) * fontWidth
        } else {
            val rightColumn = leftColumn + (width - marginLeft - marginRight - gutterWidth) / fontWidth - 1
            if (caretPosition.column > rightColumn) {
                scrollX += (caretPosition.column - rightColumn) * fontWidth
            }
        }
    }

    fun hasFocus() = Game.instance.hasFocus(this)

    override fun replaced(document: TextDocument, from: TextDocument.Position, to: TextDocument.Position, removedTo: TextDocument.Position) {
        if (hasFocus()) {

            caretPosition = to
            ensureCaretVisible()

        } else if (caretPosition > from) {
            if (caretPosition < to) {
                // caret was within the text being deleted
                caretPosition = to
            } else {
                // Untested code, as I haven't used 2 TextAreas with the same TextDocument.
                val lineDiff = removedTo.line - to.line
                val temp = document.addLines(caretPosition, lineDiff)
                caretPosition = if (lineDiff != 0) {
                    document.addColumns(temp, removedTo.column - to.column)
                } else {
                    temp
                }
            }
        }
        markPosition = caretPosition
        dirty = true
    }

    override fun onKey(event: KeyEvent) {
        for (entry in actions) {
            if (entry.key.matches(event)) {
                event.consume()
                entry.value()
            }
        }
    }

    override fun onChar(event: CharEvent) {
        val c = event.codePoint.toChar()

        if ((c >= ' ' || c == '\n') && (c < 'z')) {
            replaceSelection(c.toString())
            event.consume()
            return
        }
    }

    override fun lostFocus() {
        dirty = true
        caretVisible = false
    }

    override fun gainedFocus() {
        caretVisible = true
        caretAction.begin()
        dirty = true
    }


    fun requestFocus() {
        dirty = true
        Game.instance.requestFocus(this)
    }

    fun indent() {

        val indent = " ".repeat(document.indentSize)

        if (markPosition == caretPosition) {
            replaceSelection(indent)
        } else {
            val fromLine = Math.min(caretPosition.line, markPosition.line)
            val toLine = Math.max(caretPosition.line, markPosition.line)

            val selectedLines = document.paragraphs.subList(fromLine, toLine)
            setSelection(document.position(fromLine, 0), document.position(toLine, Int.MAX_VALUE))

            val replacement = selectedLines.joinToString(separator = "\n") { indent + it }
            replaceSelection(replacement)
            setSelection(document.position(fromLine, 0), document.position(toLine, Int.MAX_VALUE))
        }
    }

    fun unindent() {

        val indent = " ".repeat(document.indentSize)

        val fromLine = Math.min(caretPosition.line, markPosition.line)
        val toLine = Math.max(caretPosition.line, markPosition.line)

        val selectedLines = document.paragraphs.subList(fromLine, toLine)
        setSelection(document.position(fromLine, 0), document.position(toLine, Int.MAX_VALUE))

        val replacement = selectedLines.joinToString(separator = "\n") {
            if (it.startsWith(indent)) {
                it.substring(indent.length)
            } else {
                it.trimStart()
            }
        }
        replaceSelection(replacement)
        setSelection(document.position(fromLine, 0), document.position(toLine, Int.MAX_VALUE))
    }

    /**
     * Used while drawing - it marks the start/end of a HighlightRange.
     * [addHighlights] are all of the highlights that start at this transition, and
     * [removeHighlights] are all of the highlights that finish at this transition.
     */
    private class Transition(val position: TextDocument.Position) : Comparable<Transition> {

        val addHighlights = mutableListOf<Highlight>()
        val removeHighlights = mutableListOf<Highlight>()

        override fun compareTo(other: Transition): Int {
            return position.compareTo(other.position)
        }

        override fun toString() = "$position"
    }

    private fun updateTexture(renderer: Renderer) {
        val fontResource = fontResource ?: return

        if (dirty) {
            dirty = false

            if (texture == null) {

                val appearance = TextAreaAppearance()
                texture = appearance.pose.texture
                actor.appearance = appearance

            }

            texture?.let { texture ->

                val lineCount = document.lineCount()

                // Begin rendering to an off-screen buffer backed by 'texture'.
                glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, bufferId)
                glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, texture.handle, 0)

                glViewport(0, 0, width.toInt(), height.toInt())
                renderer.beginView(0.0f, height, width, 0.0f)
                renderer.clear(background)

                gutterWidth = if (showLineNumbers) {
                    fontResource.fontTexture.lineWidth(lineCount.toString()) + 8
                } else {
                    0f
                }

                val firstLine = Math.max(0, (scrollY / fontHeight).toInt() - 1)
                val lastLine = Math.min(lineCount, firstLine + (height / fontHeight).toInt() + 2)

                val firstPosition = TextDocument.Position(firstLine, 0)
                val lastPosition = TextDocument.Position(lastLine, 0)

                val transitions = mutableMapOf<TextDocument.Position, Transition>(
                        firstPosition to Transition(firstPosition), lastPosition to Transition(lastPosition)
                )

                fun findTransition(pos: TextDocument.Position): Transition {
                    val clamped = pos.clamp(firstPosition, lastPosition)
                    return transitions[clamped]
                            ?: (Transition(clamped).apply {
                                transitions[clamped] = this
                            })
                }

                for (r in document.ranges) {
                    if (r.to < firstPosition || r.from > lastPosition) {
                        // Don't bother including it (won't be visible)
                    } else {
                        findTransition(r.from).addHighlights.add(r.highlight)
                        findTransition(r.to).removeHighlights.add(r.highlight)
                    }
                }
                if (markPosition != caretPosition) {
                    val (from, to) = getSelection()
                    if (to < firstPosition || from > lastPosition) {
                        // Don't bother including it (won't be visible)
                    } else {
                        findTransition(from).addHighlights.add(selectionHighlight)
                        findTransition(to).removeHighlights.add(selectionHighlight)
                    }
                }

                val ordered = transitions.values.toSortedSet()
                val highlights = mutableListOf<Highlight>()

                maxLineWidth = 0f // Used for the x scroll bar
                var y: Float = height - marginTop + scrollY.toInt() - firstLine * fontHeight
                var x: Float = gutterWidth + marginLeft - scrollX
                var previousPosition = firstPosition

                for (t in ordered) {

                    if (t.position > previousPosition) {

                        highlights.sortBy { -it.importance }
                        val fc = highlights.firstOrNull()?.foreground ?: foreground
                        val bc = highlights.firstOrNull { it.background != null }?.background

                        fun drawText(str: String) {
                            if (str.isNotEmpty()) {
                                if (bc != null) {
                                    val width = fontResource.fontTexture.lineWidth(str)
                                    // TODO
                                    /*
                                    renderer.drawRect(
                                        Rect(
                                            x,
                                            y - fontHeight,
                                            x + width,
                                            y
                                        ), bc
                                    )
                                    */
                                }

                                fontResource.fontTexture.draw(renderer, str, x, y, fc)

                                val endOfLine = x + str.length * fontWidth
                                if (endOfLine > maxLineWidth) maxLineWidth = endOfLine
                            }
                        }

                        val part = document.part(previousPosition, t.position)
                        for (i in 0 until part.size) {
                            val line = part[i]
                            drawText(line)
                            if (i < part.size - 1) {
                                x = gutterWidth + marginLeft - scrollX
                                y -= fontHeight
                            }
                        }
                        x += part.last().length * fontWidth

                    }

                    t.addHighlights.forEach { highlights.add(it) }
                    t.removeHighlights.forEach { highlights.remove(it) }
                    previousPosition = t.position

                }

                // Line numbers in a "gutter" on the left edge.
                if (showLineNumbers) {
                    // TODO renderer.drawRect(Rect(0f, 0f, gutterWidth, height), lineNumberBackground)
                    for (l in firstLine until Math.max(lastLine, document.lineCount())) {
                        fontResource.fontTexture.draw(
                            renderer,
                            (l + 1).toString(),
                            4f,
                            height - marginTop + scrollY - l * fontHeight,
                            lineNumberForeground
                        )
                    }
                }

                // A highlight around the text area when it has focus
                if (hasFocus()) {
                    val line = Rect(0f, 0f, 1f, 0f + height) // Left edge
                    //TODO renderer.drawRect(line, focusColor)
                    line.set(0f, 0f, 0f + width, 1f) // Bottom edge
                    //TODO renderer.drawRect(line, focusColor)
                    line.set(width - 1f, 0f, width - 0f, 0f + height) // Right edge
                    //TODO renderer.drawRect(line, focusColor)
                    line.set(0f, height - 1f, 0f + width, height - 0f) // Top edge
                    //TODO renderer.drawRect(line, focusColor)
                }

                // X Scroll bar
                maxLineWidth -= marginLeft + gutterWidth - scrollX
                if (scrollX != 0f || width < maxLineWidth) {
                    val availWidth = width - gutterWidth - marginLeft - marginRight
                    scrollXRatio = availWidth / maxLineWidth
                    val left = gutterWidth + (scrollX / maxLineWidth) * availWidth
                    val rect = Rect(left, 0f, left + availWidth * scrollXRatio, scrollbarSize)
                    //TODO renderer.drawRect(rect, scrollbarColor)
                } else {
                    scrollXRatio = 1f
                }

                // Y Scroll bar
                if (scrollY != 0f || height < lineCount * fontHeight) {
                    val availHeight = height - marginTop - marginBottom
                    scrollYRatio = availHeight / (lineCount * fontHeight)
                    val top = height - (scrollY / (lineCount * fontHeight)) * availHeight
                    val rect = Rect(width - scrollbarSize, top - availHeight * scrollYRatio, width, top)
                    //TODO renderer.drawRect(rect, scrollbarColor)
                }

                renderer.endView()

                // Stop using the off-screen buffer, and delete the buffer (but keep the texture that it rendered to).
                glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

            }
        }
    }


    /**
     * Used to detect double clicks.
     */
    private var previousClickTime = 0L
    private var previousX = 0f
    private var previousY = 0f

    override fun onMouseButton(event: MouseEvent) {

        if (!hasFocus()) {
            requestFocus()
        }

        if (mouseMoveHandler != null && event.state == ButtonState.RELEASED) {
            mouseMoveHandler = null
            event.release()
            event.consume()
            return
        }

        val clickTime = Game.instance.gameLoop.tickCount
        val doubleClick =
            clickTime - previousClickTime < 30 && Math.abs(previousX - event.localPosition.x) < 10f && Math.abs(
                previousY - event.localPosition.y
            ) < 10f
        previousClickTime = clickTime
        previousX = event.localPosition.x
        previousY = event.localPosition.y

        val column = Math.round((event.localPosition.x + scrollX - marginLeft - gutterWidth) / fontWidth)
        val line = (Math.round((height - event.localPosition.y + scrollY - marginTop)) / fontHeight).toInt()

        if (click.matches(event)) {
            event.consume()

            // Clicked in the Y scroll bar?
            if (scrollYRatio != 1f && event.localPosition.x > width - scrollbarSize) {

                val lineCount = document.lineCount()
                val availHeight = height - marginTop - marginBottom
                val top = height - (scrollY / (lineCount * fontHeight)) * availHeight
                val bottom = top - availHeight * scrollYRatio

                if (event.localPosition.y > top) {
                    scrollY -= height
                    event.consume()
                    return

                } else if (event.localPosition.y < bottom) {
                    scrollY += height
                    event.consume()
                    return

                } else {
                    beginDrag(event, ::dragYScrollbar)
                    return
                }

                // Clicked in the X scroll bar ?
            } else if (scrollXRatio != 1f && event.localPosition.y < scrollbarSize) {
                val availWidth = width - gutterWidth - marginLeft - marginRight
                val left = gutterWidth + (scrollX / maxLineWidth) * availWidth
                val right = left + availWidth * scrollXRatio

                if (event.localPosition.x < left) {
                    scrollX -= availWidth / 2
                    event.consume()
                    return

                } else if (event.localPosition.x > right) {
                    val max = maxLineWidth - availWidth
                    scrollX = Math.min(scrollX + availWidth / 2, max)
                    event.consume()
                    return

                } else {
                    beginDrag(event, ::dragXScrollbar)
                    return
                }

            } else {

                if (doubleClick) {
                    widenSelection()
                } else {
                    caretPosition = document.position(line, column)
                    ensureCaretVisible()
                    beginDrag(event, ::dragSelection)
                }
            }
        }

        if (shiftClick.matches(event)) {
            event.consume()
            val oldMark = markPosition
            caretPosition = document.position(line, column)
            setSelection(oldMark, caretPosition)
            beginDrag(event, ::dragSelection)
        }

        if (scrollUp.matches(event)) {
            event.consume()
            scrollY -= 4 * fontHeight
        }

        if (scrollDown.matches(event)) {
            event.consume()
            scrollY += 4 * fontHeight
        }

        event.consume()
    }


    private var dragStartPosition = Vector2()

    private fun beginDrag(event: MouseEvent, handler: ((MouseEvent) -> Unit)) {
        dragStartPosition.set(event.localPosition)
        mouseMoveHandler = handler
        event.capture()
        event.consume()
    }

    private var mouseMoveHandler: ((MouseEvent) -> Unit)? = null

    override fun onMouseMove(event: MouseEvent) {
        mouseMoveHandler?.let { it(event) }
    }

    private fun dragYScrollbar(event: MouseEvent) {
        val availHeight = height - marginTop - marginBottom
        val diff = event.localPosition.y - dragStartPosition.y
        val total = document.lineCount() * fontHeight
        // The height of the play in the scrollbar is height - height * scrollYRatio or height * (1-scrollYRatio)
        // This corresponds to (total - availHeight) pixels of the document.
        // So if we move the scrollbar by diff pixels, then that is diff/(height * (1-scrollYRatio)) of the play,
        // and we multiply that by (total - availHeight) to get the amount to scroll
        val delta = diff / (height * (1f - scrollYRatio)) * (total - availHeight)

        scrollY -= delta
        dragStartPosition.y = event.localPosition.y
    }

    private fun dragXScrollbar(event: MouseEvent) {
        val availWidth = width - marginTop - marginBottom - gutterWidth
        val diff = event.localPosition.x - dragStartPosition.x
        val total = maxLineWidth
        val delta = diff / (width * (1f - scrollXRatio)) * (total - availWidth)

        val max = maxLineWidth - availWidth
        scrollX = Math.min(scrollX + delta, max)
        dragStartPosition.x = event.localPosition.x
    }

    private fun dragSelection(event: MouseEvent) {
        val column = Math.round((event.localPosition.x + scrollX - marginLeft - gutterWidth) / fontWidth)
        val line = Math.round((height - event.localPosition.y + scrollY - marginTop) / fontHeight)

        setSelection(markPosition, document.position(line, column))
    }

    private fun jumpCategory(c: Char): Int = if (c.isWhitespace()) 0 else if (c.isLetterOrDigit()) 1 else 2

    fun jumpWord(direction: Int, growSelection: Boolean) {

        if (direction == 0) return

        val oldMark = markPosition
        val d = if (direction < 0) -1 else 1

        if (d < 0 && caretPosition.line <= 0 && caretPosition.column <= 0) return
        if (d > 0 && caretPosition.line >= document.lineCount() - 1 && caretPosition.column >= document.paragraphs.last().length - 1) return

        val first = document.charAt(if (d == -1) document.addColumns(caretPosition, d) else caretPosition)
        val cat = jumpCategory(first)

        while (true) {
            val nextPosition = document.addColumns(caretPosition, d)
            if (cat != jumpCategory(document.charAt(if (d == 1) caretPosition else nextPosition))) break
            if (nextPosition == caretPosition) break
            caretPosition = nextPosition
        }

        // The caret has now jumped to the correct place.

        if (growSelection) {
            setSelection(oldMark, caretPosition)
        }
    }

    fun setSelection(mark: TextDocument.Position, caret: TextDocument.Position) {
        caretPosition = caret
        markPosition = mark
    }

    fun getSelection() = Pair(min(caretPosition, markPosition), max(caretPosition, markPosition))

    fun widenSelection() {
        if (caretPosition == markPosition) {
            jumpWord(-1, true)
            val sel = getSelection()
            setSelection(sel.first, sel.second)
            jumpWord(1, true)
        } else {
            // Select the whole line
            setSelection(document.position(caretPosition.line, 0), document.position(caretPosition.line, Int.MAX_VALUE))
        }
    }

    fun replaceSelection(s: String) {
        document.replace(caretPosition, markPosition, s)
    }


    companion object {

        private val click = MouseInput(0, ButtonState.PRESSED, shift = false)
        private val shiftClick = MouseInput(0, ButtonState.PRESSED, shift = true)

        private val scrollUp = MouseInput(4, ButtonState.PRESSED)
        private val scrollDown = MouseInput(5, ButtonState.PRESSED)


        internal val enter = KeyInput(Key.ENTER, state = ButtonState.REPEATED, shift = false, control = false)
        private val backspace = KeyInput(Key.BACKSPACE, state = ButtonState.REPEATED, shift = false, control = false)
        private val delete = KeyInput(Key.DELETE, state = ButtonState.REPEATED, shift = false, control = false)

        internal val indent = KeyInput(Key.TAB, shift = false)
        internal val unindent = KeyInput(Key.TAB, shift = true)

        private val left = KeyInput(Key.LEFT, state = ButtonState.REPEATED, shift = false, control = false)
        private val right = KeyInput(Key.RIGHT, state = ButtonState.REPEATED, shift = false, control = false)
        private val up = KeyInput(Key.UP, state = ButtonState.REPEATED, shift = false, control = false)
        private val down = KeyInput(Key.DOWN, state = ButtonState.REPEATED, shift = false, control = false)

        private val wordLeft = KeyInput(Key.LEFT, shift = false, control = true)
        private val wordRight = KeyInput(Key.RIGHT, shift = false, control = true)

        private val startOfLine = KeyInput(Key.HOME, shift = false, control = false)
        private val endOfLine = KeyInput(Key.END, shift = false, control = false)

        private val pageUp = KeyInput(Key.PAGE_UP, shift = false, control = false)
        private val pageDown = KeyInput(Key.PAGE_DOWN, shift = false, control = false)

        private val growLeft = KeyInput(Key.LEFT, state = ButtonState.REPEATED, shift = true, control = false)
        private val growRight = KeyInput(Key.RIGHT, state = ButtonState.REPEATED, shift = true, control = false)
        private val growUp = KeyInput(Key.UP, state = ButtonState.REPEATED, shift = true, control = false)
        private val growDown = KeyInput(Key.DOWN, state = ButtonState.REPEATED, shift = true, control = false)

        private val growWordLeft = KeyInput(Key.LEFT, shift = true, control = true)
        private val growWordRight = KeyInput(Key.RIGHT, shift = true, control = true)

        private val growStartOfLine = KeyInput(Key.HOME, state = ButtonState.REPEATED, shift = true, control = false)
        private val growEndOfLine = KeyInput(Key.END, state = ButtonState.REPEATED, shift = true, control = false)

        private val growPageUp = KeyInput(Key.PAGE_UP, state = ButtonState.REPEATED, shift = true, control = false)
        private val growPageDown = KeyInput(Key.PAGE_DOWN, state = ButtonState.REPEATED, shift = true, control = false)

        private val startOfText = CompoundInput().apply {
            add(KeyInput(Key.HOME, shift = false, control = true))
            add(KeyInput(Key.PAGE_UP, shift = false, control = false))
        }
        private val endOfText = CompoundInput().apply {
            add(KeyInput(Key.END, shift = false, control = true))
            add(KeyInput(Key.PAGE_DOWN, shift = false, control = true))
        }

        private val undo = KeyInput(Key.Z, control = true, shift = false)
        private val redo = CompoundInput().apply {
            add(KeyInput(Key.Z, shift = true, control = true))
            add(KeyInput(Key.Y, shift = false, control = true))
        }

        private val copy = KeyInput(Key.C, shift = false, control = true)
        private val cut = KeyInput(Key.X, shift = false, control = true)
        private val paste = KeyInput(Key.V, shift = false, control = true)

        private val selectAll = KeyInput(Key.A, shift = false, control = true)
        private val selectNone = KeyInput(Key.A, shift = true, control = true)

        internal val escape = KeyInput(Key.ESCAPE, shift = false, control = false)
        internal val gotoLine = KeyInput(Key.G, shift = false, control = true)
    }

    private inner class TextAreaAppearance()
        : PoseAppearance(actor, Pose(Texture(width.toInt(), height.toInt(), GL_RGBA, null), YDownRect(0, 0, width.toInt(), height.toInt()))) {

        override fun draw(renderer: Renderer) {

            super.draw(renderer)
            if (caretVisible) {
                val x = caretPosition.column * fontWidth + marginLeft + gutterWidth - scrollX
                val y = height - (caretPosition.line * fontHeight) + scrollY - marginTop
                val w = Math.max(1f, fontWidth / 16)
                val rect = Rect(
                    actor.x + clamp(x - w, 0f, width),
                    actor.y + clamp(y, 0f, height),
                    actor.x + clamp(x + w, 0f, width),
                    actor.y + clamp(y - fontHeight, 0f, height)
                )
                // TODO renderer.drawRect(rect, caretColor)
            }
        }
    }
}
