package uk.co.nickthecoder.tickle.action.animation

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.Eases

class FrameAnimation(
        val actor: Actor,
        val events: List<String>,
        seconds: Float,
        ease: Ease
)
    : FrameAction(events.size, seconds, ease) {

    constructor(actor: Actor, events: List<String>, seconds: Float) : this(actor, events, seconds, Eases.linear)

    override fun frame(frame: Int) {
        actor.event(events[frame])
    }

}
