package uk.co.nickthecoder.tickle.control

import uk.co.nickthecoder.tickle.graphics.Color


/**
 * Defines the appearance of a [HighlightRange].
 *
 * [importance] is used to resolve which [Highlight] takes precedence when ranges overlap.
 *
 * The range of values you use is up to you, but to give you a hint :
 * * -9 Syntax highlighting
 * * -5 Find tool
 * * 100 Selection (Your values should all be below this!)
 *
 * That leaves the values 0..99 for general styles.
 *
 * Currently there is no way to change the font, but at a later date I hope to allow font styles, such as
 * bold and italics. I do NOT plan on allowing the font size to be changed though.
 */
class Highlight(
        val foreground: Color,
        val background: Color?,
        val importance: Int = 0
) {
    override fun toString() = "Highlight #$importance fc=${foreground.toHashRGB()} bg=${background?.toHashRGB()}"
}
