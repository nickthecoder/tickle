/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.events

import org.lwjgl.glfw.GLFW
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.graphics.Window

interface Input {

    fun isPressed(): Boolean

    fun matches(event: KeyEvent): Boolean

    fun matches(event: MouseEvent): Boolean

    companion object {

        val dummyInput = CompoundInput()

    }

}

/**
 * Used by KeyInput and MouseInput, and handles the modifier keys, shift, control and alt.
 *
 * Each of [shift], [control], [alt] can be null, in which case, the modifier is ignore.
 * If it is true, then that modifier key must be down for [modifierMatches] to return true.
 * If it is false, then that modifier key must NOT be down for [modifierMatches] to return true.
 *
 * Note, there are two versions of [modifierMatches], one which takes `mods: Int` and another which has not parameters.
 * The latter is used within [isPressed], where an [Event] is not present.
 */
abstract class ModifierInput(
        val shift: Boolean? = null,
        val control: Boolean? = null,
        val alt: Boolean? = null
) : Input {

    fun modifierMatches(): Boolean {
        val windowHandle = Game.instance.window.handle

        if (shift != null) {
            if (((GLFW.glfwGetKey(windowHandle, GLFW.GLFW_KEY_LEFT_SHIFT) == GLFW.GLFW_PRESS) || (GLFW.glfwGetKey(windowHandle, GLFW.GLFW_KEY_RIGHT_SHIFT) == GLFW.GLFW_PRESS)) != shift) return false
        }
        if (control != null) {
            if (((GLFW.glfwGetKey(windowHandle, GLFW.GLFW_KEY_LEFT_CONTROL) == GLFW.GLFW_PRESS) || (GLFW.glfwGetKey(windowHandle, GLFW.GLFW_KEY_RIGHT_CONTROL) == GLFW.GLFW_PRESS)) != control) return false
        }
        if (alt != null) {
            if (((GLFW.glfwGetKey(windowHandle, GLFW.GLFW_KEY_LEFT_ALT) == GLFW.GLFW_PRESS) || (GLFW.glfwGetKey(windowHandle, GLFW.GLFW_KEY_RIGHT_ALT) == GLFW.GLFW_PRESS)) != alt) return false
        }
        return true
    }

    fun modifierMatches(mods: Int): Boolean {
        return modifier(shift, (mods and GLFW.GLFW_MOD_SHIFT) != 0) &&
                modifier(control, (mods and GLFW.GLFW_MOD_CONTROL) != 0) &&
                modifier(alt, (mods and GLFW.GLFW_MOD_ALT) != 0)
    }

    companion object {

        fun modifier(required: Boolean?, actual: Boolean): Boolean {
            return if (required == null) {
                // We required is null, we don't care what the actual value is.
                true
            } else if (required) {
                actual
            } else {
                !actual
            }
        }
    }
}
