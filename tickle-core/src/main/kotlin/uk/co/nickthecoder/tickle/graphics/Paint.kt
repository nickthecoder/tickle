package uk.co.nickthecoder.tickle.graphics

import uk.co.nickthecoder.tickle.path.CapStyle
import uk.co.nickthecoder.tickle.path.VisibleShape

/**
 * Defines the colors when drawing solid shapes.
 *
 * See [StrokePaint] when drawing outlines of shapes.
 */
sealed interface Paint

sealed interface StrokePaint

interface PaintWithGradient<T : PaintWithGradient<T>> : Paint {

    var gradient: Gradient

    /**
     * Note, this must be immutable, because it affects the UVs, and therefore [VisibleShape]
     * must clear its cached mesh when we change from mirrored to regular.
     * For [CapStyle.ROUND] with a gradient ACROSS the line, the UVs change depending on
     * if the [Gradient] is mirrored.
     */
    val isMirrored: Boolean

    /**
     * The number of times the gradient is repeated. 1 for no repeat.
     */
    var repeat: Float

    fun mirrored(value: Boolean = true): T
}

interface StrokePaintWithGradient<T : StrokePaintWithGradient<T>> : PaintWithGradient<T>


class FlatColor(color: Color) : Paint, StrokePaint {
    var color: Color = color
        set(v) {
            dirty = true
            field = v
        }

    private var dirty = true
        set(v) {
            if (!v) {
                cachedTexture?.destroy()
                cachedTexture = null
            }
            field = v
        }

    private var cachedTexture: Texture? = null

    fun finalize() {
        cachedTexture?.destroy()
    }

    override fun toString() = "FlatColor $color"
}

/**
 * A gradient which follows the path being drawn.
 * The first [GradientStop] is at the start of the path, and the last [GradientStop] is at the end of the path.
 */
class GradientAlong(
    override var gradient: Gradient,
    override val isMirrored: Boolean = false,
    override var repeat: Float = 1f

) : StrokePaint, StrokePaintWithGradient<GradientAlong> {

    constructor(from: Color, to: Color, isMirrored: Boolean = false) : this(Gradient(from, to), isMirrored)

    val texture: Texture
        get() = gradient.texture

    override fun mirrored(value: Boolean) = if (value == isMirrored) this else GradientAlong(gradient, value)

    override fun toString() = "GradientAlong $gradient"
}


/**
 * A gradient across the path being drawn.
 * The first [GradientStop] is at the left edge of the path, and the last [GradientStop] is at the right edge of the path.
 */
class GradientAcross(
    override var gradient: Gradient,
    override val isMirrored: Boolean = false,
    override var repeat: Float = 1f

) : StrokePaint, StrokePaintWithGradient<GradientAcross> {

    constructor(from: Color, to: Color, isMirrored: Boolean = false) : this(Gradient(from, to), isMirrored)

    override fun mirrored(value: Boolean) = if (value == isMirrored) this else GradientAcross(gradient, value)

    override fun toString() = "GradientAcross $gradient"
}
