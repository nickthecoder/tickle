/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.collision

import uk.co.nickthecoder.tickle.Actor

/**
 * An implementation of [Overlapping], which only considers the distance between the two actors.
 * An example use : Set off an explosion when something gets too close.
 *
 * The simplest way to use this is using a fixed distance as the threshold. e.g.
 *
 *      val closeTo = CloseTo.fixedDistance( 10.0 )
 *      if (closeTo.overlapping( actorA, actorB )) {
 *          // The centers of actorA and actorB are within a distance of 10.0
 *      }
 *
 * However, you could make the threshold dynamic, based on the two Actors. For example, the threshold distance
 * may be depend on the size of one (or both) Actors.
 * In this case, you need to implement the [CloseToThreshold], and pass that to CloseTo's constructor.
 */
class CloseTo(val threshold: CloseToThreshold) : Overlapping {

    override fun overlapping(actorA: Actor, actorB: Actor): Boolean {
        val dx = actorA.x - actorB.x
        val dy = actorA.y - actorB.y
        val d2 = dx * dx + dy * dy
        return Math.sqrt(d2.toDouble()) < threshold.threshold(actorA, actorB)
    }

    companion object {
        @JvmStatic
        fun fixedDistance(distance: Double): CloseTo {
            return CloseTo( CloseToFixedDistance(distance) )
        }
    }
}

interface CloseToThreshold {
    fun threshold(a: Actor, b: Actor): Double
}

class CloseToFixedDistance( val distance : Double ) : CloseToThreshold {
    override fun threshold(a: Actor, b: Actor) = distance
}
