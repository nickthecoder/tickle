/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.collision

import uk.co.nickthecoder.tickle.util.Vector2
import uk.co.nickthecoder.tickle.misc.sqrt

class CircularCollision {
    companion object {

        @JvmStatic
        fun collision(
            positionA: Vector2, velocityA: Vector2, massA: Float = 1f,
            positionB: Vector2, velocityB: Vector2, massB: Float = 1f
        ): Float {

            return collision(
                positionA, velocityA, massA,
                positionB, velocityB, massB,
                1f
            )
        }

        /**
         * @param elasticity In the range 0..1 inclusive.
         * 0 = fully inelastic (like clay). Kinetic energy will be lost. The actors will tend to stick together
         * 1 = fully elastic (like steel balls).
         */
        @JvmStatic
        fun collision(
            positionA: Vector2, velocityA: Vector2, massA: Float = 1f,
            positionB: Vector2, velocityB: Vector2, massB: Float = 1f,
            elasticity: Float
        ): Float {

            val dx = positionA.x - positionB.x
            val dy = positionA.y - positionB.y

            val dist = sqrt(dx * dx + dy * dy)

            val dvx = velocityB.x - velocityA.x
            val dvy = velocityB.y - velocityA.y

            // The speed of the collision in the direction of the line between their centres.
            val collision = (elasticity / 2 + 0.5f) * (dvx * dx + dvy * dy) / dist

            if (collision < 0) {
                // They are moving away from each other
                return 0f
            }

            val massSum = massA + massB
            velocityA.x += (dx / dist) * collision * 2f * massB / massSum
            velocityB.x -= (dx / dist) * collision * 2f * massA / massSum

            velocityA.y += (dy / dist) * collision * 2f * massB / massSum
            velocityB.y -= (dy / dist) * collision * 2f * massA / massSum

            return collision
        }

        @JvmStatic
        fun distanceSquared(a: Vector2, b: Vector2) = a.distanceSquared(b)

        @JvmStatic
        fun overlapping(combinedRadius: Float, a: Vector2, b: Vector2): Boolean {
            return distanceSquared(a, b) < combinedRadius * combinedRadius
        }
    }

}
