package uk.co.nickthecoder.tickle.graphics.programs

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.Rect
import uk.co.nickthecoder.tickle.util.Vector2

class SimpleTriangleProgram : ShaderProgram(SG_VERTEX, SG_FRAGMENT, 2) {

    val colorLocation = getUniformLocation("color")

    fun setup(color: Color, modelMatrix: Matrix3x2f? = null) {
        enablePosition()
        setModelMatrix(modelMatrix)
        setUniform(colorLocation, color)
    }

    fun draw(vertices: List<IVector2>) {
        requiredVertices(vertices.size)

        for (point in vertices) {
            Renderer.floatBuffer.put(point.x).put(point.y)
        }
    }


    fun drawLine(from: Vector2, to: Vector2, thickness: Float) {
        val delta = (to - from).unit().perpendicular() * (thickness / 2f)

        requiredTriangles(2)
        Renderer.floatBuffer

            .put(from.x + delta.x).put(from.y + delta.y)
            .put(from.x - delta.x).put(from.y - delta.y)
            .put(to.x + delta.x).put(to.y + delta.y)

            .put(to.x + delta.x).put(to.y + delta.y)
            .put(to.x - delta.x).put(to.y - delta.y)
            .put(from.x - delta.x).put(from.y - delta.y)
    }

    fun setupAndStrokeRect(rect: Rect, thickness: Float, color: Color, modelMatrix: Matrix3x2f? = null) {
        setup(color, modelMatrix)
        strokeRect(rect, thickness)
    }

    fun strokeRect(rect: Rect, thickness: Float) {
        val left = rect.left
        val bottom = rect.bottom
        val right = rect.right
        val top = rect.top

        val outerLeft = left - thickness
        val outerBottom = bottom - thickness
        val outerRight = right + thickness
        val outerTop = top + thickness

        requiredTriangles(8)
        Renderer.floatBuffer

            // Left edge
            .put(left).put(bottom)
            .put(outerLeft).put(outerBottom)
            .put(outerLeft).put(outerTop)

            .put(outerLeft).put(outerTop)
            .put(left).put(top)
            .put(left).put(bottom)

            // Top edge
            .put(outerLeft).put(outerTop)
            .put(outerRight).put(outerTop)
            .put(left).put(top)

            .put(left).put(top)
            .put(outerRight).put(outerTop)
            .put(right).put(top)


            // Right Edge
            .put(right).put(top)
            .put(outerRight).put(outerTop)
            .put(right).put(bottom)

            .put(right).put(bottom)
            .put(outerRight).put(outerTop)
            .put(outerRight).put(outerBottom)

            // Bottom Edge
            .put(outerRight).put(outerBottom)
            .put(outerLeft).put(outerBottom)
            .put(left).put(bottom)

            .put(left).put(bottom)
            .put(right).put(bottom)
            .put(outerRight).put(outerBottom)

    }

    fun setupAndFillRect(rect: Rect, color: Color, modelMatrix: Matrix3x2f? = null) {
        setup(color, modelMatrix)
        fillRect(rect)
    }

    fun fillRect(rect: Rect) {

        val left = rect.left
        val bottom = rect.bottom
        val right = rect.right
        val top = rect.top

        requiredTriangles(2)
        Renderer.floatBuffer
            .put(left).put(bottom)
            .put(right).put(bottom)
            .put(right).put(top)

            .put(right).put(top)
            .put(left).put(top)
            .put(left).put(bottom)
    }

    companion object {

        private val SG_VERTEX = """
            #version 120

            attribute vec2 position;
            
            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;
            
            void main() {
                gl_Position = vec4( viewMatrix * modelMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val SG_FRAGMENT = """
            #version 120
            
            uniform vec4 color;

            void main() {
                gl_FragColor = color;
            }
            """.trimIndent()
    }

}