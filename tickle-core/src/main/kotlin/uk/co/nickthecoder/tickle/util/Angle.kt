/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

/**
 * The shared interface for [ImmutableAngle] and [Angle].
 */
interface IAngle {

    val radians: Double
    val degrees: Double
        get() = Math.toDegrees(radians)

    fun mutable(): Angle
    fun immutable(): ImmutableAngle

    /**
     * Allows feather to use the unary minus operator. e.g. myAngle = -otherAngle
     */
    operator fun unaryMinus() = Angle.radians(-radians)

    operator fun times(by: Double) = Angle.radians(radians * by)
    operator fun div(by: Double) = Angle.radians(radians / by)


    operator fun plus(other: IAngle): Angle {
        return Angle.radians(radians + other.radians)
    }

    operator fun minus(other: IAngle): Angle {
        return Angle.radians(radians - other.radians)
    }

    fun vector() = Vector2(Math.cos(radians).toFloat(), Math.sin(radians).toFloat())

    fun cos() = Math.cos(radians)
    fun sin() = Math.sin(radians)
    fun tan() = Math.tan(radians)

}

class ImmutableAngle(override val radians: Double) : IAngle {

    constructor(other: IAngle) : this(other.radians)

    override fun mutable() = Angle.radians(radians)
    override fun immutable() = this

    override fun hashCode(): Int {
        return radians.hashCode() + 170
    }

    override fun equals(other: Any?): Boolean {
        return if (other is IAngle) {
            other.radians == radians
        } else {
            false
        }
    }

    override fun toString() = "$degrees°"

}

open class Angle private constructor(override var radians: Double) : IAngle {

    constructor() : this(0.0)
    constructor(other: IAngle) : this(other.radians)

    override var degrees: Double
        get() = Math.toDegrees(radians)
        set(v) {
            radians = Math.toRadians(v)
        }

    override fun mutable() = this
    override fun immutable() = ImmutableAngle(radians)

    fun set(other: IAngle) {
        this.radians = other.radians
    }

    fun set(vector: IVector2) {
        radians = vector.angleRadians()
    }

    fun set(dx: Double, dy: Double) {
        radians = Math.atan2(dy, dx)
    }


    operator fun plusAssign(other: IAngle) {
        radians += other.radians
    }

    operator fun minusAssign(other: IAngle) {
        radians += other.radians
    }

    operator fun timesAssign(by: Double) {
        radians *= by
    }

    operator fun divAssign(by: Double) {
        radians /= by
    }


    override fun equals(other: Any?): Boolean {
        return if (other is IAngle) {
            other.radians == radians
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        return radians.hashCode() + 170
    }

    override fun toString() = "$degrees°"

    companion object {

        const val TAU = Math.PI * 2

        @JvmStatic
        fun toRadians(degrees: Double) = Math.toRadians(degrees)

        @JvmStatic
        fun toDegrees(radians: Double) = Math.toDegrees(radians)

        @JvmStatic
        fun degrees(degrees: Double) = Angle().apply { this.degrees = degrees }

        @JvmStatic
        fun radians(radians: Double) = Angle(radians)


        @JvmStatic
        fun radiansOf(vector: Vector2) = Math.atan2(vector.y.toDouble(), vector.x.toDouble())

        @JvmStatic
        fun of(vector: Vector2) = Angle(radiansOf(vector))


        @JvmStatic
        fun radiansOf(a: Vector2, from: Vector2) = Math.atan2((a.y - from.y).toDouble(), (a.x - from.x).toDouble())

        @JvmStatic
        fun of(a: Vector2, from: Vector2) = Angle().apply { this.radians = radiansOf(a, from) }
    }
}
