/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.sandbox

import org.lwjgl.opengl.GL11.glViewport
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.collision.PixelOverlapping
import uk.co.nickthecoder.tickle.graphics.Color

/**
 * Not finished!
 * Moves two items over each other, and at the same time, shows the intermediate results of pixel based overlap detection.
 */
class OverlapSandbox : Sandbox() {

    lateinit var coinA: Actor
    lateinit var soilA: Actor

    lateinit var pixelOverlapping: PixelOverlapping

    var frame = 0

    var speed = 4

    override fun prepare() {
        pixelOverlapping = PixelOverlapping(40, 30).apply {
            debug = true
        }
        coinA = createEllipseActor(
            "coin",
            createEllipsePose("coin", 30, 30, 1.0)
        )
        soilA = createEllipseActor(
            "soil",
            createEllipsePose("soil", 30, 30, 1.0, 120.toByte())
        )
        coinA.x = 60f
        coinA.y = 60f

        soilA.x = 400f
        soilA.y = 60f
    }

    override fun tick() {
        println()
        println("Frame $frame")
        println()

        glViewport(0, 0, window.width, window.height)
        renderer.beginView(0.0, 0.0, window.width.toDouble(), window.height.toDouble())
        renderer.clear(Color(0.1, 0.1, 0.1, 1.0))

        coinA.appearance.draw(renderer)
        soilA.appearance.draw(renderer)

        if (pixelOverlapping.overlapping(coinA, soilA)) {
            println("Yes overlapping ;-)")
            speed = -speed/2
        }

        renderer.endView()

        coinA.x += speed

        frame++
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            OverlapSandbox().start()
        }
    }

}
