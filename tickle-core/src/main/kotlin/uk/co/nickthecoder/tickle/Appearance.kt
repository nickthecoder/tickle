/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import org.joml.Vector2f
import uk.co.nickthecoder.tickle.collision.PixelTouching
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.TextStyle
import uk.co.nickthecoder.tickle.util.Rect
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * An [Appearance] is responsible for drawing an [Actor]. The most common type of Appearance is a [PoseAppearance],
 * which draw a single image for each Actor. There are also [NinePatchAppearance], [TextAppearance] and [InvisibleAppearance].
 */
interface Appearance {

    val actor: Actor

    val directionRadians: Double

    val directionDegrees
        get() = Math.toDegrees(directionRadians)

    /**
     * The natural direction. i.e. if the Actor moved "forward", which mathematical angle would that be?
     * For the default value of 0, the image is pointing to the right.
     */
    fun draw(renderer: Renderer)

    /**
     * The width of the pose or the text (before scaling/rotating)
     */
    fun width(): Float

    /**
     * The height of the pose or the text (before scaling/rotating)
     */
    fun height(): Float

    /**
     * The offset position of the pose or the text (before scaling/rotating/flipping)
     */
    fun offsetX(): Float

    /**
     * The offset position of the pose or the text (before scaling/rotating/flipping)
     */
    fun offsetY(): Float

    /**
     * The bounding rectangle of the actor after scaling, rotating, flipping etc.
     * Note, that this is a rectangle aligned with the x,y axes, and therefore when a long, thin object
     * is rotated, the rectangle can be substantially larger that the object.
     */
    fun worldRect(): Rect

    /**
     * Is [point] contained withing the non-axis aligned bounding rectangle.
     * As this is a non-axis aligned bounding rectangle, it still works well with rotated objects.
     *
     * [point] is in Tickle's coordinate system. i.e. (0,0) will be the bottom left of the screen when the view hasn't
     * been panned.
     */
    fun contains(point: Vector2): Boolean

    /**
     * Is the Actor's pixel at [point] opaque.
     * Note, this uses [PixelTouching.instance], which has a default threshold of 0. If you wish to change this
     * threshold :
     *     PixelTouching.instance = PixelTouching( myThreshold )
     *
     * [point] is in Tickle's coordinate system. i.e. (0,0) will be the bottom left of the screen when the view hasn't
     * been panned.
     */
    fun pixelTouching(point: Vector2): Boolean

    /**
     * Is [point] touching the actor. For TextAppearances, this is the same as [contains]. For PoseAppearance, it
     * the same as [pixelTouching].
     *
     * [point] is in Tickle's coordinate system. i.e. (0,0) will be the bottom left of the screen when the view hasn't
     * been panned.
     */
    fun touching(point: Vector2): Boolean

    fun resize(width: Float, height: Float)

    fun updateBody() {}
}

interface AppearanceWithPose : Appearance {
    val pose: Pose
}

class InvisibleAppearance(override val actor: Actor) : Appearance {

    override val directionRadians = 0.0

    override fun draw(renderer: Renderer) {
        // Do nothing
    }

    override fun width(): Float = 0f

    override fun height(): Float = 0f

    override fun offsetX(): Float = 0f

    override fun offsetY(): Float = 0f

    override fun worldRect() = INVISIBLE_RECT

    override fun contains(point: Vector2): Boolean = false

    override fun pixelTouching(point: Vector2): Boolean = false

    override fun touching(point: Vector2): Boolean = false

    override fun resize(width: Float, height: Float) {}

    override fun toString() = "InvisibleAppearance"

    companion object {
        private val INVISIBLE_RECT = Rect(0f, 0f, 0f, 0f)
    }
}

abstract class AbstractAppearance(override val actor: Actor) : Appearance {

    private val worldRect = Rect()

    override fun worldRect(): Rect {
        worldRect.left = actor.x - offsetX()
        worldRect.bottom = actor.y - offsetY()
        worldRect.right = worldRect.left + width()
        worldRect.top = worldRect.bottom + height()
        if (!actor.isSimpleImage()) {
            // Rotates/scale/flip each corner of the rectangle...
            val a = Vector2f(worldRect.left, worldRect.top)
            val b = Vector2f(worldRect.left, worldRect.bottom)
            val c = Vector2f(worldRect.right, worldRect.top)
            val d = Vector2f(worldRect.right, worldRect.bottom)
            val matrix = actor.calculateModelMatrix()
            a.mulPosition(matrix)
            b.mulPosition(matrix)
            c.mulPosition(matrix)
            d.mulPosition(matrix)
            // Find the rectangle containing the transformed corners.
            worldRect.left = Math.min(Math.min(a.x, b.x), Math.min(c.x, d.x))
            worldRect.bottom = Math.min(Math.min(a.y, b.y), Math.min(c.y, d.y))
            worldRect.right = Math.max(Math.max(a.x, b.x), Math.max(c.x, d.x))
            worldRect.top = Math.max(Math.max(a.y, b.y), Math.max(c.y, d.y))
        }
        return worldRect
    }

    override fun contains(point: Vector2): Boolean {
        tempVector.set(point)
        tempVector -= actor.position

        if (actor.direction.radians != directionRadians) {
            tempVector.setRotateRadians(-actor.direction.radians + directionRadians)
        }

        tempVector.x /= actor.scale.x
        tempVector.y /= actor.scale.y

        val offsetX = offsetX()
        val offsetY = offsetY()

        return tempVector.x >= -offsetX && tempVector.x < width() - offsetX && tempVector.y > -offsetY && tempVector.y < height() - offsetY
    }

    override fun pixelTouching(point: Vector2): Boolean =
        contains(point) && PixelTouching.instance.touching(actor, point)

    override fun resize(width: Float, height: Float) {
        actor.scale.x = width / this.width()
        actor.scale.y = height / this.height()
    }

    companion object {
        private val tempVector = Vector2()
    }
}

private val WHITE = Color.white()
private val SEMI_TRANSPARENT = Color.white().setHalfAlpha()

open class PoseAppearance(actor: Actor, override var pose: Pose)

    : AbstractAppearance(actor), AppearanceWithPose {

    override val directionRadians
        get() = pose.direction.radians

    override fun draw(renderer: Renderer) {
        if (actor.isSimpleImage()) {
            if (actor.tint == WHITE) {
                renderer.drawPose(pose, actor.x, actor.y)
            } else {
                renderer.drawPose(pose, actor.x, actor.y, tint = actor.tint)
            }

        } else {
            renderer.drawPose(
                pose, actor.x, actor.y,
                tint = actor.tint,
                modelMatrix = actor.calculateModelMatrix()
            )
        }
    }


    override fun width(): Float = pose.pixelRect.width.toFloat()

    override fun height(): Float = pose.pixelRect.height.toFloat()

    override fun offsetX() = pose.offsetX

    override fun offsetY() = pose.offsetY

    override fun touching(point: Vector2): Boolean = pixelTouching(point)

    override fun toString() = "PoseAppearance pose=$pose"
}

open class TextAppearance(actor: Actor, var text: String, var textStyle: TextStyle)

    : AbstractAppearance(actor) {

    override val directionRadians = 0.0

    override fun draw(renderer: Renderer) {
        textStyle.draw(renderer, text, actor)
    }

    override fun width(): Float = textStyle.width(text)
    override fun height(): Float = textStyle.height(text)

    override fun offsetX(): Float = textStyle.offsetX(text)
    override fun offsetY(): Float = textStyle.height(text) - textStyle.offsetY(text)

    override fun touching(point: Vector2): Boolean = contains(point)

    override fun toString() = "TextAppearance '$text'"
}
