package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.util.IAngle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.ImmutableVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * 2D paths, inspired by SVG.
 * NOTE, This is a [ContiguousShape], and therefore cannot have jumps.
 * If you wish to create shapes with holes, or routes with jumps, then use [CompoundPath].
 */
class Path(val startPoint: Vector2 = Vector2()) : ContiguousShape<Path> {

    constructor(x: Float, y: Float) : this(Vector2(x, y))

    private var sectionsCache: List<SectionCache>? = null
    internal val sections: List<SectionCache>
        get() {
            return sectionsCache ?: updateSectionsCache()
        }

    private val mutableShapes = mutableListOf<ContiguousShape<*>>()

    val shapes: List<ContiguousShape<*>>
        get() = mutableShapes

    private var dirty = true
        set(v) {
            if (v) {
                totalLength = -1f

            }
            field = v
        }

    private var totalLength = -1f
        get() {
            if (field < 0) {
                field = shapes.sumOf { it.length().toDouble() }.toFloat()
            }
            return field
        }

    override val start: Vector2
        get() = shapes.firstOrNull()?.start ?: startPoint
    override val end: Vector2
        get() = shapes.lastOrNull()?.end ?: startPoint


    override fun joinEnd(to: Vector2) {
        shapes.last().joinEnd(to)
        dirty()
    }

    override fun length() = totalLength

    override fun copy(): Path {
        val result = Path()
        for (section in shapes) {
            result.add(section.copy())
        }
        return openOrClosed(result)
    }

    override fun buildPoints(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {
        into.add(start)
        for (segment in shapes) {
            segment.buildPoints(into, distanceTolerance, angleTolerance)
        }
    }

    /**
     * Adds [shape] to the end of this Path.
     */
    private fun add(shape: ContiguousShape<*>) {
        mutableShapes.add(shape)
        dirty()
    }

    fun lineTo(to: Vector2) = add(LineSegment(end, to))
    fun lineTo(x: Float, y: Float) = lineTo(Vector2(x, y))

    fun lineBy(delta: IVector2) = add(LineSegment(end, end + delta))
    fun lineBy(x: Float, y: Float) = lineTo(Vector2(end.x + x, end.y + y))


    fun ellipticalArcTo(to: Vector2, radius: Vector2, largeArc: Boolean, sweepArc: Boolean) =
        add(EllipticalArc(end, to, radius, largeArc, sweepArc))

    fun ellipticalArcTo(to: Vector2, radius: Vector2, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        add(EllipticalArc(end, to, radius, largeArc, sweepArc, tilt))

    fun ellipticalArcBy(delta: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean) =
        add(EllipticalArc(end, end + delta, radius, largeArc, sweepArc))

    fun ellipticalArcBy(delta: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        add(EllipticalArc(end, end + delta, radius, largeArc, sweepArc, tilt))


    fun circularArcTo(to: Vector2, radius: Float, largeArc: Boolean, sweepArc: Boolean) =
        add(EllipticalArc(end, to, ImmutableVector2(radius, radius), largeArc, sweepArc))

    fun circularArcTo(to: Vector2, radius: Float, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        add(EllipticalArc(end, to, ImmutableVector2(radius, radius), largeArc, sweepArc, tilt))

    fun circularArcBy(delta: IVector2, radius: Float, largeArc: Boolean, sweep: Boolean) =
        add(EllipticalArc(end, end + delta, ImmutableVector2(radius, radius), largeArc, sweep))

    fun circularArcBy(delta: IVector2, radius: Float, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        add(EllipticalArc(end, end + delta, ImmutableVector2(radius, radius), largeArc, sweepArc, tilt))


    /**
     */
    fun bezierTo(c1: Vector2, c2: Vector2, end: Vector2) =
        add(Bezier(end, c1, c2, end))

    /**
     */
    fun bezierTo(c2: Vector2, end: Vector2) =
        add(Bezier(end, symmetricControl(), c2, end))

    /**
     * Add a bezier curve.
     * The start of the bezier curve is this path's current end point.
     *
     * NOTE. The order of the parameters are the 'normal' : `Start`, `C1`, `C2`, `End`
     * but `Start` is omitted, because we already know it.
     *
     * @param deltaC1 The 1st control point relative to this path's current point.
     * @param deltaC2 The 2nd control point relative to the bezier's end point.
     * @param delta The end point of the bezier relative to this path's current point.
     */
    fun bezierBy(deltaC1: IVector2, deltaC2: IVector2, delta: IVector2) {
        val newEnd = end + delta
        add(Bezier(end, end + deltaC1, newEnd + deltaC2, newEnd))
    }

    /**
     * Add a symmetric bezier curve (the previous section of this Path must be a bezier curve).
     * The start of the bezier curve is this path's current end point.
     * The first control point `C1` is calculated from the previous bezier curve
     *
     * @param deltaC2 The 2nd control point relative to the bezier's end point.
     * @param delta The end point of the bezier curve relative to the path's current point.
     *
     * @throws IllegalStateException If this part doesn't currently end with a bezier curve.
     */
    fun bezierBy(deltaC2: IVector2, delta: IVector2) {
        val newEnd = end + delta
        add(Bezier(end, symmetricControl(), newEnd + deltaC2, newEnd))
    }

    private fun symmetricControl(): Vector2 {
        if (shapes.isEmpty()) throw IllegalStateException("No previous PathSections to obtain a symmetric control point")

        return when (val prev = shapes.last()) {
            is Bezier -> prev.end * 2f - prev.c2
            is LineSegment -> prev.end * 2f - prev.start
            else -> throw IllegalStateException("${prev.javaClass.simpleName} doesn't support symmetric control point")
        }
    }

    /**
     * If [moveEnd] is true, then the end point of the last segment is moved.
     *
     * Otherwise,
     * If [start] and [end] are the same point, then [close] causes them to share the same Vector2 instance.
     * If they differ, then a [LineSegment] is added.
     */
    fun close(moveEnd: Boolean = false) {
        if (moveEnd) {
            shapes.last().joinEnd(shapes.first().start)
        } else if (start == end) {
            shapes.last().joinEnd(shapes.first().start)
        } else {
            add(LineSegment(end, start))
        }
    }


    /**
     * Adds the sections of [path] to this, but first translating them, so that
     * there is no jump.
     */
    fun addRelativePath(path: Path) {
        val delta = path.start - end
        val translated = if (delta.x == 0f && delta.y == 0f) {
            path
        } else {
            path.translate(delta)
        }
        add(translated)
    }

    override fun translate(by: IVector2): Path {
        val result = Path()
        for (shape in shapes) {
            result.add(shape.translate(by))
        }
        return openOrClosed(result)
    }

    override fun scale(by: IVector2): Path {
        val result = Path()
        for (shape in shapes) {
            result.add(shape.scale(by))
        }
        return openOrClosed(result)
    }

    override fun rotate(by: IAngle): Path {
        val result = Path()
        for (shape in shapes) {
            result.add(shape.rotate(by))
        }
        return openOrClosed(result)
    }

    override fun reverse(): Path {
        val result = Path()
        for (shape in shapes.asReversed()) {
            result.add(shape.reverse())
        }
        return openOrClosed(result)
    }

    override fun along(t: Float, into: Vector2) {
        // Allow for cases where t is outside the normal range of 0..1
        val realT = t.rem(1f)

        val sections = sections
        if (dirty) {
            updateSectionsCache()
        }
        var currentIndex = 0
        var section = sections[currentIndex]
        // Find the appropriate section
        while (realT >= section.endT && currentIndex < sections.size - 1) {
            section = sections[++currentIndex]
        }
        val partT = (realT - section.startT) / (section.endT - section.startT)

        // Finally, update the [position].
        sections[currentIndex].shape.along(partT, into)
    }

    /**
     */
    internal fun updateSectionsCache(): MutableList<SectionCache> {
        val result = mutableListOf<SectionCache>()
        var t = 0f // In the range 0..1
        var travelled = 0f // In the range 0 to totalLength

        val totalLength = shapes.sumOf { it.length().toDouble() }.toFloat()

        val lastShape = shapes.last()
        for (shape in shapes) {
            val fromT = t
            val shapeLength = shape.length()
            travelled += shapeLength
            t = if (shape === lastShape) {
                1f // Ensure we don't have any rounding errors for the last endT value.
            } else {
                travelled / totalLength
            }
            result.add(SectionCache(shape, shapeLength, fromT, t))
        }
        dirty = false
        sectionsCache = result
        return result
    }

    /*
    To copy a part correctly, we must join each section to the next!
    fun copy(): Path {
        val copy = Path()
        for (section in sectionCaches) {
            copy.add(section.section)
        }
        return copy
    }
    */

    /**
     * A longer form of [toString], which includes details of all [ContiguousShape]s.
     */
    fun description(): String {
        updateSectionsCache()
        return StringBuilder().apply {
            append(toString())
            append("\n")
            for (shape in shapes) {
                append("    ${shape}\n")
            }
        }.toString()

    }

    override fun dirty() {
        dirty = true
    }

    override fun toString() = "Path of ${shapes.size} sections\n    " +
            shapes.joinToString(separator = "\n    ")


    internal class SectionCache(
        val shape: ContiguousShape<*>,
        val length: Float,
        val startT: Float,
        val endT: Float
    ) {
        override fun toString() = "$startT .. $endT = $shape"
    }

}
