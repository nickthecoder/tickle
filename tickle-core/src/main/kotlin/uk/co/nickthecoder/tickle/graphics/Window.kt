/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.graphics

import org.lwjgl.BufferUtils
import org.lwjgl.glfw.Callbacks
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWVidMode
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL11.glEnable
import org.lwjgl.opengl.GL13.GL_MULTISAMPLE
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.info
import uk.co.nickthecoder.tickle.stage.WorldView
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * The OpenGL window for the Game.
 * Only one [Window] is created, even when using the Editor, and running the game several times,
 * the same [Window] is reused (it is merely hidden when not being used).
 *
 * At the moment, [fullScreen] cannot be toggled during the game. You can ONLY set it in the
 * `Game Info`. I need to upgrade to a newer version of LWJGL to allow [fullScreen] toggling.
 */
class Window(
    title: String,
    width: Int,
    height: Int,
    resizable: Boolean = false,
    multiSample: Int = 1,
    val fullScreen: Boolean = false
) {

    val handle: Long

    var maximised = false
        private set

    var width = width
        private set

    var height = height
        private set

    val listeners = mutableListOf<WindowListener>()

    private var videoMode: GLFWVidMode? = null

    private val xBuffer = BufferUtils.createDoubleBuffer(1)
    private val yBuffer = BufferUtils.createDoubleBuffer(1)
    private val tmpVector = Vector2()

    init {
        OpenGL.ensureOpenGLThread()

        if (fullScreen) {
            glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE)
        } else {
            glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE)
        }

        if (multiSample > 1) {
            glfwWindowHint(GLFW_SAMPLES, multiSample)
        }

        val monitor = if (fullScreen) glfwGetPrimaryMonitor() else MemoryUtil.NULL
        val mode = if (fullScreen) {
            if (resizable) {
                // Use the current size of the primary monitor
                glfwGetVideoMode(glfwGetPrimaryMonitor())
            } else {
                // Change to a video mode, best fitting width and height
                findBestMode(monitor, width, height)
            }
        } else {
            null
        }

        handle = if (mode == null) {
            this.width = width
            this.height = height
            glfwCreateWindow(width, height, title, monitor, MemoryUtil.NULL)
        } else {
            this.width = mode.width()
            this.height = mode.height()
            glfwCreateWindow(mode.width(), mode.height(), title, monitor, MemoryUtil.NULL)
        }

        if (handle == MemoryUtil.NULL) {
            throw RuntimeException("Failed to create the GLFW window")
        }

        if (multiSample > 1) {
            glEnable(GL_MULTISAMPLE)
        }

        glfwMakeContextCurrent(handle)
        if (!fullScreen) {
            glfwSetWindowAttrib(handle, GLFW_RESIZABLE, if (resizable) GLFW_TRUE else GLFW_FALSE)
        }
        GL.createCapabilities()

        videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor())

        enableVSync()

        glfwSetKeyCallback(handle) { _, keyCode, scanCode, action, mods ->
            val event = KeyEvent(this, Key.forCode(keyCode), scanCode, ButtonState.of(action), mods)
            listeners.forEach {
                it.onKey(event)
            }
        }

        glfwSetCharCallback(handle) { _, codePoint: Int ->
            val event = CharEvent(this, codePoint)
            listeners.forEach {
                it.onChar(event)
            }
        }

        glfwSetWindowMaximizeCallback(handle) { _, isMax ->
            this.maximised = isMax
        }

        glfwSetWindowSizeCallback(handle) { _, _, _ ->
            // I've stopped trusting the last two values (which should be the new width and height,
            // because the callback is sometimes called *after* another window size change has occurred.
            // e.g. Tickle changes the window size at startup, and then it is changed again when the
            // game's producer begins (from Game.loadWindowSizePreferences).
            MemoryStack.stackPush().use { stack ->
                val pWidth = stack.mallocInt(1)
                val pHeight = stack.mallocInt(1)
                glfwGetWindowSize(handle, pWidth, pHeight)
                val newWidth = pWidth.get()
                val newHeight = pHeight.get()
                resized(newWidth, newHeight)
            }
        }

        glfwSetMouseButtonCallback(handle) { _, button, action, mods ->
            val event = MouseEvent(this, button, ButtonState.of(action), mods)
            getMousePosition(event.windowPosition)

            listeners.forEach {
                it.onMouseButton(event)
            }
        }

        // Scroll events get converted into old-school button events with button numbers :
        // 4=up, 5=down, 6=left, 7=right
        // A PRESSED and RELEASED event are generated one after the other.
        glfwSetScrollCallback(handle) { _, xOffset, yOffset ->
            if (yOffset != 0.0) {
                val pressed = MouseEvent(this, if (yOffset < 0.0) 5 else 4, ButtonState.PRESSED, 0)
                getMousePosition(pressed.windowPosition)
                listeners.forEach {
                    it.onMouseButton(pressed)
                }
                val released = MouseEvent(this, if (yOffset < 0.0) 5 else 4, ButtonState.RELEASED, 0)
                getMousePosition(released.windowPosition)
                listeners.forEach {
                    it.onMouseButton(released)
                }

            }
            if (xOffset != 0.0) {
                val pressed = MouseEvent(this, if (xOffset < 0.0) 7 else 6, ButtonState.PRESSED, 0)
                getMousePosition(pressed.windowPosition)
                listeners.forEach {
                    it.onMouseButton(pressed)
                }
                val released = MouseEvent(this, if (xOffset < 0.0) 7 else 6, ButtonState.RELEASED, 0)
                getMousePosition(released.windowPosition)
                listeners.forEach {
                    it.onMouseButton(released)
                }

            }
        }

    }

    fun pollEvents() {
        glfwPollEvents()
    }

    /**
     * Updates [width] and [height] and calls the [listeners] onResize event.
     */
    private fun resized(newWidth: Int, newHeight: Int) {
        if (newWidth != width || newHeight != height) {
            info("Window resized to $newWidth, $newHeight")
            val event = ResizeEvent(this, this.width, this.height, newWidth, newHeight)
            this.width = newWidth
            this.height = newHeight
            listeners.forEach {
                it.onResize(event)
            }
        }
    }

    fun makeContextCurrent() {
        glfwMakeContextCurrent(handle)
        GL.createCapabilities()
    }

    fun findBestMode(monitor: Long, width: Int, height: Int): GLFWVidMode? {

        if (monitor == MemoryUtil.NULL) return null

        fun scoreMode(mode: GLFWVidMode): Int {
            return Math.abs(width - mode.width()) + Math.abs(height - mode.height()) +
                    if (mode.width() < width || mode.height() < height) 1000 else 0
        }

        var bestMode: GLFWVidMode? = null
        var bestScore = Int.MAX_VALUE
        val modes = glfwGetVideoModes(monitor)
        modes.forEach { mode ->
            val score = scoreMode(mode)
            if (score < bestScore) {
                bestScore = score
                bestMode = mode
            }
        }
        return bestMode
    }


    fun showMouse(value: Boolean = true) {
        glfwSetInputMode(handle, GLFW_CURSOR, if (value) GLFW_CURSOR_NORMAL else GLFW_CURSOR_HIDDEN)
    }

    fun close() {
        glfwSetWindowShouldClose(handle, true)
    }

    fun show() {
        glfwSetWindowShouldClose(handle, false)

        center()

        // Make the OpenGL context instance
        glfwMakeContextCurrent(handle)

        // Make the window visible
        glfwShowWindow(handle)
        glfwFocusWindow(handle)
    }

    fun hide() {
        glfwHideWindow(handle)
    }

    fun change(title: String, width: Int, height: Int, resizable: Boolean) {

        MemoryStack.stackPush().use { stack ->
            val titleEncoded = stack.UTF8(title)

            glfwSetWindowTitle(handle, titleEncoded)
        }

        // Cannot change the window size while in fullscreen mode.
        if (!fullScreen) {
            glfwSetWindowSize(handle, width, height)

            // Center the window on the screen
            center()
            glfwSetWindowAttrib(handle, GLFW_RESIZABLE, if (resizable) GLFW_TRUE else GLFW_FALSE)
            resized(width, height)
        }
    }

    private var preventResizeCount = 0

    /**
     * There are times when resizing the window will screw up the positions of some actors.
     * In such cases, call [preventResize], and then [allowResize] when the action is finished.
     *
     * There is a counter mechanism, so that multiple actions can call these two methods,
     * without them interacting badly (i.e. only when both actions have completed will the
     * window be resizable again).
     */
    fun preventResize() {
        if (preventResizeCount == 0 && !fullScreen) {
            glfwSetWindowAttrib(handle, GLFW_RESIZABLE, GLFW_FALSE)
        }
        preventResizeCount++
    }

    /**
     * See [preventResize]
     */
    fun allowResize() {
        preventResizeCount--
        if (preventResizeCount == 0 && !fullScreen) {
            glfwSetWindowAttrib(handle, GLFW_RESIZABLE, GLFW_TRUE)
        }
    }


    fun maximise(maximise: Boolean) {
        if (!fullScreen) {
            if (maximise) {
                glfwMaximizeWindow(handle)
            } else {
                glfwRestoreWindow(handle)
            }
            maximised = maximise
        }
    }

    fun maximise() {
        if (!fullScreen) {
            glfwMaximizeWindow(handle)
            maximised = true
        }
    }

    fun resize(width: Int, height: Int) {

        // Cannot resize a window in fullscreen mode.
        if (!fullScreen) {
            glfwSetWindowSize(handle, width, height)
            resized(width, height)
        }
    }

    // Center the window
    fun center() {
        if (!fullScreen) {
            // Get the resolution of the primary monitor
            val vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor())
            glfwSetWindowPos(handle, (vidmode.width() - width) / 2, (vidmode.height() - height) / 2)
        }
    }

    fun wholeViewport() {
        GL11.glViewport(0, 0, width, height)
    }

    /**
     * The default is for VSync to be enabled, disable it by calling this with interval = 0.
     *
     * @param interval 0 to disable vsync, 1 for syncing every time swap is called.
     */
    fun enableVSync(interval: Int = 1) {
        glfwSwapInterval(interval)
    }

    fun shouldClose(): Boolean = glfwWindowShouldClose(handle)


    /**
     * Gets the mouse position in pixels, relative to the top left of the [Window].
     * The result is placed into [result].
     */
    fun getMousePosition(result: Vector2) {
        glfwGetCursorPos(handle, xBuffer, yBuffer)

        result.x = xBuffer.get(0).toFloat()
        result.y = yBuffer.get(0).toFloat()
    }

    fun getMousePosition(): Vector2 {
        val result = Vector2()
        getMousePosition(result)
        return result
    }

    /**
     * Moves the mouse to [position], which is measured in pixels from the top left of the window.
     */
    fun setMousePosition(position: Vector2) {
        glfwSetCursorPos(handle, position.x.toDouble(), position.y.toDouble())
    }

    /**
     * Gets the position of the mouse pointer relative to the top left of the window.
     * The result is returned by changing the [result] Vector2 (which avoids creating a new object).
     * If the mouse is outside of the window, then the result is bounded to (0,0) ... (width,height)
     *
     * Often it is easier, and more useful to find the position of the mouse pointer in a view's coordinate system
     * using [WorldView.mousePosition].
     */
    fun getMousePixelsBounded(result: Vector2) {
        getMousePosition(result)

        if (result.x < 0) result.x = 0f
        if (result.y < 0) result.y = 0f
        if (result.x >= width) result.x = width - 1f
        if (result.y >= height) result.y = height - 1f
    }

    fun getMousePixelsBounded(): Vector2 {
        val result = Vector2()
        getMousePixelsBounded(result)
        return result
    }

    fun isMouseWithinWindow(): Boolean {
        getMousePosition(tmpVector)
        return tmpVector.x >= 0 && tmpVector.y >= 0 && tmpVector.x < width && tmpVector.y < height
    }

    fun refreshRate(): Int {
        return videoMode?.refreshRate() ?: 60
    }

    fun swap() {
        glfwSwapBuffers(handle)
    }

    fun delete() {
        Callbacks.glfwFreeCallbacks(handle)
        // glfwDestroyWindow(handle)
    }

    override fun toString() = "Window #$handle ($width x $height)"
}
