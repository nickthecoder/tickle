/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.sound

import org.lwjgl.openal.AL
import org.lwjgl.openal.ALC
import org.lwjgl.openal.ALC10
import org.lwjgl.openal.ALCCapabilities
import org.lwjgl.system.MemoryUtil.NULL
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.SoundEvent
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.warn
import java.nio.ByteBuffer
import java.nio.IntBuffer

/**
 * We keep track of who initiated each sound, and the priority of that sound,
 * so that later, we can choose to stop a playing sound if a higher priority sound
 * needs to be played.
 *
 * NOTE, [forActor] is most frequently an [Actor], but it can be any Object.
 * It is also possible not to specify "who" initiates a sound, in which case,
 * the priority alone will determine if a sound is stopped, or a new sound is
 * prevented from playing.
 */
internal data class SoundSlot(
    val source: SoundSource,
) {
    var priority: Float = 0.0f
}

object SoundManager {

    var device: Long = 0L
        private set

    lateinit var capabilities: ALCCapabilities
        private set

    var context: Long = 0L
        private set

    private val MAX_SOUNDS = 10

    private val slots = mutableListOf<SoundSlot>()

    private fun initialise() {
        OpenGL.ensureOpenGLThread()

        if (device != 0L) {
            warn("SoundManager has already been initialised.")
            return
        }

        device = ALC10.alcOpenDevice(null as ByteBuffer?)
        if (device == NULL) {
            throw IllegalStateException("Failed to open the default OpenAL device.")
        }
        capabilities = ALC.createCapabilities(device)
        context = ALC10.alcCreateContext(device, null as IntBuffer?)
        if (context == NULL) {
            throw IllegalStateException("Failed to create OpenAL context.")
        }
        ALC10.alcMakeContextCurrent(context)
        AL.createCapabilities(capabilities)

        for (i in 0..MAX_SOUNDS) {
            slots.add(SoundSlot(SoundSource()))
        }
    }

    fun ensureInitialised() {
        if (device == 0L) {
            initialise()
        }
    }

    /**
     * If [priorityThreshold] is the special value of Int.MAXVALUE,
     * then any non-playing slot is chosen, and [forActor] is ignored.
     *
     * Otherwise, we look for a slot still playing for the actor, and
     * failing that, the lowest priority slot regardless of who initiated the sound.
     *
     * Once a slot has been chosen, if it is already free, then the new sound is played.
     * However, if the slot is still playing a previous sound, then one of two things happens :
     *
     * If the currently playing sound has a LOWER priority, then it is stopped, and the new
     * sound is played. Otherwise, nothing is stopped, and no new sound is played.
     */
    private fun findBestSlot(priorityThreshold: Float): SoundSlot? {

        var best: SoundSlot?


        // Find any free slot.
        best = slots.firstOrNull { slot -> !slot.source.isPlaying() }

        // If we haven't found a free slot. Pick the lowest priority slot,
        if (best == null) {
            slots.sortBy { it.priority }
            best = slots.first()
        }

        // At this point best must be non-null. Now we have to choose if we play the new sound,
        // and if we should stop the existing sound.
        return if (best.source.isPlaying()) {
            if (priorityThreshold >= SoundEvent.MAX_PRIORITY) {
                // Always play this new sound.
                best.source.stop()
                best
            } else {
                if (best.priority < priorityThreshold) {
                    best.source.stop()
                    best
                } else {
                    null
                }
            }
        } else {
            // The slot was already free, so there is nothing to stop.
            best
        }
    }

    fun play(sound: Sound) {
        OpenGL.ensureOpenGLThread()
        play(sound, sound.defaultGain.toFloat(), SoundEvent.DEFAULT_PRIORITY, SoundEvent.MAX_PRIORITY, false)
    }

    fun play(sound: Sound, gain: Float) {
        OpenGL.ensureOpenGLThread()
        play(sound, gain, SoundEvent.DEFAULT_PRIORITY, SoundEvent.MAX_PRIORITY, false)
    }

    fun play(sound: Sound, gain: Float, newPriority: Float, priorityThreshold: Float, onlyOnce: Boolean) {
        OpenGL.ensureOpenGLThread()
        if (onlyOnce) {
            // If the sound is already playing, then don't play it again.
            for (slot in slots) {
                if (slot.source.playing() === sound) {
                    return
                }
            }
        }
        findBestSlot(priorityThreshold)?.let { slot ->
            slot.source.gain(gain)
            slot.source.play(sound)
            slot.priority = newPriority
        }
    }

    fun stop(sound: Sound) {
        OpenGL.ensureOpenGLThread()
        slots.filter { it.source.playing() === sound }.forEach {
            it.source.stop()
        }
    }

    fun stopAll() {
        slots.filter { it.source.isPlaying() }.forEach {
            it.source.stop()
        }
    }

    fun cleanUp() {
        if (context != 0L) {
            ALC10.alcDestroyContext(context)
            context = 0L
        }
        if (device != 0L) {
            ALC10.alcCloseDevice(device)
            device = 0L
        }
    }

}
