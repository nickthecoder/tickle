package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.misc.lerp
import uk.co.nickthecoder.tickle.util.IAngle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.ImmutableVector2
import uk.co.nickthecoder.tickle.util.Vector2
import java.util.*

/**
 * A contiguous set of line segments.
 *
 * When rendering more complicated shapes, such as [Bezier] curves, these are approximated to
 * a [Polyline] first, and then converted to a mesh using [createStrokeMesh].
 */
class Polyline(vararg points: Vector2) : ContiguousShape<Polyline> {

    constructor(points: List<Vector2>) : this(* points.toTypedArray())

    val points: MutableList<Vector2> = mutableListOf<Vector2>().apply { addAll(points) }

    override val start: Vector2
        get() = points.first()

    override val end: Vector2
        get() = points.last()

    /**
     * -1 is a sentinel value, meaning the length hasn't been calculated.
     * Set when [length] is called, and reset back to -1 when [dirty] is called.
     */
    private var cachedLength = -1f

    /**
     * Caches data, so that [along] can be calculated quickly O(log n), instead of O(n)
     */
    private var alongMap: TreeMap<Float, AlongCache>? = null

    override fun length() = if (cachedLength >= 0) cachedLength else calculateLength()

    private fun calculateLength(): Float {
        var result = 0f
        var prev = start
        for (i in 1 until points.size) {
            val current = get(i)
            result += (current - prev).length()
            prev = current
        }
        cachedLength = result
        return result
    }

    /**
     * See [alongMap].
     */
    private inner class AlongCache(
        val index: Int, // The index in [points] of the start of the line
        val fromT: Float, // The lowest `t` value
        val ratio: Float // The line length as a ratio of the total length
    ) : Comparable<AlongCache> {
        override fun compareTo(other: AlongCache): Int {
            return fromT.compareTo(other.fromT)
        }
    }

    private fun updateTreeCache(): TreeMap<Float, AlongCache> {
        val result = TreeMap<Float, AlongCache>()
        val totalLength = length()
        var a = start
        var travelled = 0f
        for (i in 1 until points.size) {
            val b = points[i]
            val lineLength = (b - a).length()
            val fromT = travelled / totalLength
            result[travelled / totalLength] = (AlongCache(i - 1, fromT, lineLength / totalLength))
            travelled += lineLength
            a = b
        }

        alongMap = result
        return result
    }

    override fun along(t: Float, into: Vector2) {
        // Handle cases where t is outside the normal range 0..1
        val realT = t.rem(1f)

        val tree = alongMap ?: updateTreeCache()
        val entry = tree.floorEntry(realT)
        if (entry == null) {

            // t is less than 0??? Lerp BACKWARDS along the first line segment.
        } else {
            val alongCache = entry.value
            val s = (realT - alongCache.fromT) / alongCache.ratio
            lerp(points[alongCache.index], points[alongCache.index + 1], s, into)
        }
    }

    override fun dirty() {
        cachedLength = -1f
        alongMap = null
    }

    override fun joinEnd(to: Vector2) {
        points[points.size - 1] = points.first()
    }

    override fun reverse() = openOrClosed(Polyline(points.reversed()))

    override fun translate(by: IVector2) = openOrClosed(Polyline(points.map { it + by }))

    override fun scale(by: IVector2) = openOrClosed(Polyline(points.map { it * by }))

    override fun rotate(by: IAngle) = openOrClosed(Polyline(points.map { it.rotate(by) }))

    override fun buildPoints(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {
        into.addAll(points.subList(1, points.size))
    }

    override fun toPolyline(distanceTolerance: Float, angleTolerance: Double) = this

    override fun copy() = openOrClosed(Polyline(points.map { Vector2(it) }))

    override fun toString() = "Polyline $start -> $end\n    " +
            points.subList(1, points.size - 1).joinToString(separator = "\n    ")

    /**
     * @param index The index into the [points] list.
     *        This may be negative, or greater than the size of the list, and still gives sensible results.
     *        e.g. -1 is the last point, -2 is the penultimate point etc.
     * @return The nth point of the polyline.
     */
    operator fun get(index: Int): Vector2 {
        return points[index.mod(points.size)]
    }

    /**
     * Used by [createStrokeMesh]
     */
    private class PolySegment(val center: LineSegment, halfThickness: Float) {
        val midPoint = (center.start + center.end) / 2f

        // calculate the segment's outer edges by offsetting
        // the central line by the normal vector
        // multiplied with the thickness
        val edge1 = center + center.normal() * halfThickness
        val edge2 = center - center.normal() * halfThickness

        override fun toString() = "PolySegment e1 : ${edge1}  e2 : ${edge2}"
    }

    /**
     * Ported from https://github.com/CrushedPixel/Polyline2D
     *
     * @param joinStyle When gradient filling across the line, [JoinStyle.MITER] doesn't look very good.
     * @param allowOverlap
     * @param miterMinAngle The threshold for mitered joints.
     *     If the joint's angle is smaller than this angle, the joint will be drawn beveled instead.
     *     The default value is ~10°
     * @param roundMinAngle The minimum angle of a round joint's triangles.
     *     The default value is ~20°
     * @param mirroredAcross When using GradientAcross, the gradient should bend round,
     *     instead of going straight ahead.
     * @return A list of points, where each 3 points in turn form a triangle
     *     A list of UVs, where U is the length along the path (range 0..1)
     *     and V is the distance from the center line (range -1..1)
     *
     * Note, in the C++ original, there was an extra [JoinStyle] to join the end to the start.
     * I've implemented this using [isClosed] instead.
     * The C++ original didn't include UVs.
     *
     */
    fun createStrokeMesh(
        thickness: Float, joinStyle: JoinStyle, capStyle: CapStyle,
        allowOverlap: Boolean = true, miterMinAngle: Float = 0.349066f, roundMinAngle: Float = 0.174533f,
        includeUVs: Boolean, mirroredAcross: Boolean
    ): Mesh {

        // operate on half the thickness to make our lives easier
        val halfThickness = thickness / 2

        val points = mutableListOf<IVector2>()
        val uvs = mutableListOf<IVector2>()

        val totalLength = if (includeUVs) {
            length() + if (capStyle == CapStyle.BUTT) 0f else thickness
        } else {
            1f
        }
        val endCapTravel = if (capStyle == CapStyle.BUTT) 0f else halfThickness / totalLength


        val segments = mutableListOf<PolySegment>()
        for (i in 0 until this.points.size - 1) {
            val point1 = this.points[i]
            val point2 = this.points[i + 1]

            // to avoid division-by-zero errors,
            // only create a line segment for non-identical points
            if (point1 != point2) {
                segments.add(PolySegment(LineSegment(point1, point2), halfThickness))
            }
        }

        if (segments.isEmpty()) {
            // handle the case of insufficient input points
            return Mesh(emptyList(), emptyList(), length = 0f, thickness = thickness)
        }

        if (isClosed()) {
            // create a connecting segment from the last to the first point

            // to avoid division-by-zero errors, only create a line segment for non-identical points
            if (start != end) {
                segments.add(PolySegment(LineSegment(end, start), halfThickness))
            }
        }

        var nextStart1: IVector2 = ImmutableVector2.ZERO
        var nextStart2: IVector2 = ImmutableVector2.ZERO
        var start1: IVector2 = ImmutableVector2.ZERO
        var start2: IVector2 = ImmutableVector2.ZERO
        var end1: IVector2 = ImmutableVector2.ZERO
        var end2: IVector2 = ImmutableVector2.ZERO


        fun createTriangleFan(
            connectTo: IVector2, origin: IVector2,
            start: Vector2, end: Vector2, clockwise: Boolean,
            travelledFrom: Float, isEndCap: Boolean
        ) {

            val point1 = start - origin
            val point2 = end - origin

            // calculate the angle between the two points
            var angle1 = Math.atan2(point1.y.toDouble(), point1.x.toDouble())
            var angle2 = Math.atan2(point2.y.toDouble(), point2.x.toDouble())

            // ensure the outer angle is calculated
            if (clockwise) {
                if (angle2 > angle1) {
                    angle2 -= 2 * Math.PI
                }
            } else {
                if (angle1 > angle2) {
                    angle1 -= 2 * Math.PI
                }
            }

            val jointAngle = angle2 - angle1

            // calculate the amount of triangles to use for the joint
            val numTriangles = Math.max(1, Math.floor(Math.abs(jointAngle) / roundMinAngle).toInt())

            // calculate the angle of each triangle
            val triAngle = jointAngle / numTriangles

            var startPoint = start
            for (t in 1..numTriangles) {
                val rot = t * triAngle

                val cos = Math.cos(rot).toFloat()
                val sin = Math.sin(rot).toFloat()

                val endPoint = if (t == numTriangles) {
                    // it's the last triangle - ensure it perfectly connects to the next line
                    end
                } else {
                    // re-add the rotation origin to the target point
                    origin.plus(
                        (cos * point1.x - sin * point1.y),
                        (sin * point1.x + cos * point1.y)
                    )
                }

                // Add the triangle
                points.add(startPoint)
                points.add(endPoint)
                points.add(connectTo)
                startPoint = endPoint

                if (includeUVs) {
                    if (isEndCap) {

                        if (mirroredAcross) {
                            if (travelledFrom == 0f) {
                                // The cap for the start of the polyline when mirroredAcross==true
                                uvs.add(Vector2(0f, 0f))
                                uvs.add(Vector2(0f, 0f))
                                uvs.add(Vector2(endCapTravel, 0.5f))
                            } else {
                                // The cap for the end of the polyline when mirroredAcross==true
                                uvs.add(Vector2(1f, 0f))
                                uvs.add(Vector2(1f, 0f))
                                uvs.add(Vector2(1f - endCapTravel, 0.5f))
                            }
                        } else {
                            // TODO, This isn't quite right, should be bases on angles, but doesn't look bad.
                            val foo1 = (t - 1).toFloat() / numTriangles
                            val foo2 = t.toFloat() / numTriangles

                            // TODO UV.x isn't quite right. The outer points of the fan have the same UV.x value,
                            // but they should be ever so slightly different - based on the start and end angles
                            // of the triangle.
                            if (travelledFrom == 0f) {
                                // The cap for the start of the polyline when mirroredAcross==false
                                uvs.add(Vector2(endCapTravel * Math.abs(cos), foo1))
                                uvs.add(Vector2(endCapTravel * Math.abs(cos), foo2))
                                uvs.add(Vector2(endCapTravel, 0.5f))
                            } else {
                                // The cap for the end of the polyline when mirroredAcross==false
                                uvs.add(Vector2(1f - endCapTravel * Math.abs(cos), foo1))
                                uvs.add(Vector2(1f - endCapTravel * Math.abs(cos), foo2))
                                uvs.add(Vector2(1f - endCapTravel, 0.5f))
                            }
                        }
                    } else {

                        if (clockwise) {
                            // JoinStyle.ROUND (clockwise)
                            uvs.add(Vector2(travelledFrom, 0f))
                            uvs.add(Vector2(travelledFrom, 0f))
                            uvs.add(Vector2(travelledFrom, 1f))
                        } else {
                            // JoinStyle.ROUND (anticlockwise)
                            uvs.add(Vector2(travelledFrom, 1f))
                            uvs.add(Vector2(travelledFrom, 1f))
                            uvs.add(Vector2(travelledFrom, 0f))
                        }

                    }
                }
            }
        }

        fun createJoint(segment1: PolySegment, segment2: PolySegment, travelled: Float) {

            // calculate the angle between the two line segments
            val dir1 = segment1.center.direction()
            val dir2 = segment2.center.direction()

            val angle = dir1.angleRadians(dir2)
            // wrap the angle around the 180° mark if it exceeds 90° for minimum angle detection
            var wrappedAngle = angle
            if (wrappedAngle > Math.PI / 2) {
                wrappedAngle = Math.PI - wrappedAngle
            }

            val actualJoinStyle = if (joinStyle == JoinStyle.MITER && wrappedAngle < miterMinAngle) {
                // The joint is too pointy (the angle is too small).
                // To avoid the intersection point being extremely far out, we render the joint beveled instead.
                JoinStyle.BEVEL
            } else {
                joinStyle
            }

            when (actualJoinStyle) {

                JoinStyle.MITER -> {
                    // calculate both edges' intersection point with the next segment's edges
                    val sec1 = LineSegment.intersection(segment1.edge1, segment2.edge1, true)
                    val sec2 = LineSegment.intersection(segment1.edge2, segment2.edge2, true)

                    end1 = sec1 ?: segment1.edge1.end
                    end2 = sec2 ?: segment1.edge2.end

                    nextStart1 = end1
                    nextStart2 = end2
                }

                JoinStyle.BEVEL, JoinStyle.ROUND -> {

                    // find out which are the inner edges for this joint
                    val x1 = dir1.x
                    val x2 = dir2.x
                    val y1 = dir1.y
                    val y2 = dir2.y

                    val clockwise = x1 * y2 - x2 * y1 < 0

                    val inner1: LineSegment
                    val inner2: LineSegment
                    val outer1: LineSegment
                    val outer2: LineSegment

                    // as the normal vector is rotated counter-clockwise, the first edge lies to the left
                    // from the central line's perspective, and the second one to the right.
                    if (clockwise) {
                        outer1 = segment1.edge1
                        outer2 = segment2.edge1
                        inner1 = segment1.edge2
                        inner2 = segment2.edge2
                    } else {
                        outer1 = segment1.edge2
                        outer2 = segment2.edge2
                        inner1 = segment1.edge1
                        inner2 = segment2.edge1
                    }

                    // calculate the intersection point of the inner edges
                    val innerSecOpt = LineSegment.intersection(inner1, inner2, allowOverlap)

                    // for parallel lines, simply connect them directly
                    val innerSec = innerSecOpt ?: inner1.end

                    // if there's no inner intersection, flip the next start position for near-180° turns
                    val innerStart = if (innerSecOpt != null) {
                        innerSec
                    } else if (angle > Math.PI / 2) {
                        outer1.end
                    } else {
                        inner1.end
                    }

                    if (clockwise) {
                        end1 = outer1.end
                        end2 = innerSec

                        nextStart1 = outer2.start
                        nextStart2 = innerStart

                    } else {
                        end1 = innerSec
                        end2 = outer1.end

                        nextStart1 = innerStart
                        nextStart2 = outer2.start
                    }

                    // connect the intersection points according to the joint style

                    if (actualJoinStyle == JoinStyle.BEVEL) {
                        // simply connect the intersection points
                        points.add(outer1.end)
                        points.add(outer2.start)
                        points.add(innerSec)

                        if (includeUVs) {
                            if (clockwise) {
                                uvs.add(Vector2(travelled, 0f))
                                uvs.add(Vector2(travelled, 0f))
                                uvs.add(Vector2(travelled, 1f))
                            } else {
                                uvs.add(Vector2(travelled, 1f))
                                uvs.add(Vector2(travelled, 1f))
                                uvs.add(Vector2(travelled, 0f))
                            }
                        }

                    } else {
                        // draw a circle between the ends of the outer edges,
                        // centered at the actual point
                        // with half the line thickness as the radius
                        createTriangleFan(
                            innerSec, segment1.center.end, outer1.end, outer2.start,
                            clockwise, travelled, false
                        )
                    }
                }
            }
        }

        val firstSegment = segments.first()
        val lastSegment = segments.last()
        val pathStart1 = firstSegment.edge1.start
        val pathStart2 = firstSegment.edge2.start
        val pathEnd1: Vector2 = lastSegment.edge1.end
        val pathEnd2: Vector2 = lastSegment.edge2.end

        if (!isClosed()) {

            when (capStyle) {
                CapStyle.SQUARE -> {
                    // extend the start/end points by half the thickness
                    pathStart1 -= firstSegment.edge1.normalisedDirection() * halfThickness
                    pathStart2 -= firstSegment.edge2.normalisedDirection() * halfThickness
                    pathEnd1 += lastSegment.edge1.normalisedDirection() * halfThickness
                    pathEnd2 += lastSegment.edge2.normalisedDirection() * halfThickness
                }

                CapStyle.ROUND -> {
                    // Draw half circle end caps.
                    // Note, isEndCap=false when symmetric==true. This causes a circular gradient at the ends
                    // (which only looks nice when the gradient is made symmetrical).
                    createTriangleFan(
                        firstSegment.center.start, firstSegment.center.start,
                        firstSegment.edge1.start, firstSegment.edge2.start, false,
                        0f, true
                    )
                    createTriangleFan(
                        lastSegment.center.end, lastSegment.center.end,
                        lastSegment.edge1.end, lastSegment.edge2.end, true,
                        1f, true
                    )
                }

                CapStyle.BUTT -> {
                    // Do nothing.
                }
            }
        } else {
            createJoint(lastSegment, firstSegment, 1f)
            // createJoint only updates nextStart and nextEnd. Update the segments starts/ends.
            // The original didn't need this extra "bodge"!
            firstSegment.edge1.start.set(nextStart1)
            firstSegment.edge2.start.set(nextStart2)
            lastSegment.edge1.end.set(end1)
            lastSegment.edge2.end.set(end2)
        }

        var travelled = 0f

        for (i in 0 until segments.size) {
            val segment = segments[i]

            val segmentLength = if (includeUVs) segment.center.length() else 0f
            var nextTravelled = travelled + segmentLength / totalLength

            if (i == 0) {
                start1 = pathStart1
                start2 = pathStart2
                if (capStyle != CapStyle.BUTT) {
                    travelled += endCapTravel
                    nextTravelled += endCapTravel
                }
            }

            if (i + 1 == segments.size) {
                // this is the last segment
                end1 = pathEnd1
                end2 = pathEnd2

            } else {
                createJoint(segment, segments[i + 1], nextTravelled)
            }

            // The two triangles for the line of segment.

            points.add(start1)
            points.add(start2)
            points.add(end1)

            points.add(end1)
            points.add(start2)
            points.add(end2)

            if (includeUVs) {
                uvs.add(Vector2(travelled, 0f))
                uvs.add(Vector2(travelled, 1f))
                uvs.add(Vector2(nextTravelled, 0f))

                uvs.add(Vector2(nextTravelled, 0f))
                uvs.add(Vector2(travelled, 1f))
                uvs.add(Vector2(nextTravelled, 1f))
            }

            start1 = nextStart1
            start2 = nextStart2
            travelled = nextTravelled
        }

        return Mesh(points, uvs, length = totalLength, thickness = thickness)
    }

    fun createStrokeMesh(
        thickness: Float, joinStyle: JoinStyle, capStyle: CapStyle,
        allowOverlap: Boolean = true, miterMinAngle: Float = 0.349066f, roundMinAngle: Float = 0.174533f
    ): List<IVector2> {
        return createStrokeMesh(
            thickness, joinStyle, capStyle,
            allowOverlap, miterMinAngle, roundMinAngle, includeUVs = false, mirroredAcross = true
        ).points
    }

    companion object {

        fun polygon(vertices: List<Vector2>) = Polyline(vertices).apply {
            points.add(points.first())
        }

    }

}
