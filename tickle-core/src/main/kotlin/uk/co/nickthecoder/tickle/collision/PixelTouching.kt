/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.collision

import org.lwjgl.opengl.EXTFramebufferObject.*
import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.OpenGL
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Tests if one of the Actor's pixels is opaque.
 */
class PixelTouching(val threshold: Int = 0) : Touching {

    init {
        OpenGL.ensureOpenGLThread()
    }

    private val pixelFrameBufferId = glGenFramebuffersEXT()
    private val pixelTexture = Texture(1, 1, GL_RGBA, null)

    private val transparent = Color.white().setTransparent()

    init {
        // Create a Texture, which holds the overlap of the the two actors
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, pixelFrameBufferId)
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, pixelTexture.handle, 0)

        // Don't use either of the frame buffers yet
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
    }

    /**
     * [point] is in Tickle's world coordinates.
     */
    override fun touching(actor: Actor, point: Vector2): Boolean {
        val renderer = Renderer.instance()

        // Begin writing to the pixel frame buffer rather than the screen's frame buffer.tt
        glBindTexture(GL_TEXTURE_2D, 0)
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, pixelFrameBufferId)

        // Clear the pixel
        renderer.clear(transparent)

        // Draw the actor onto the pixel frame buffer.
        glViewport(0, 0, 1, 1)
        renderer.beginView(point.x, point.y, point.x + 1, point.y + 1)

        actor.appearance.draw(renderer)
        renderer.endView()

        // Stop using the pixel frame buffer, and return to rendering on the screen's frame buffer.
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

        val pixels = pixelTexture.read()
        // Get the alpha channel byte of the first and only pixel. (i.e. pixels[3], which is the 4th byte)
        // and with 0xff to convert to unsigned.
        val pixel = pixels[3].toInt() and 0xff
        return pixel > threshold
    }

    companion object {
        /**
         * The default PixelTouching instance used by
         */
        var instance = PixelTouching()
    }

}
