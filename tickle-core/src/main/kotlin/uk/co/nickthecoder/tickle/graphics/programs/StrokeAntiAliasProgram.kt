package uk.co.nickthecoder.tickle.graphics.programs

import org.joml.Matrix3x2f
import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.path.Mesh
import uk.co.nickthecoder.tickle.util.IVector2

/**
 * Similar to [StrokeProgram], but the shaders also have an `edge` attribute in the range 0..1.
 *
 * 0 would give the same results as [StrokeProgram] (i.e. with alias effects)
 *
 * 1 would make the line fully fuzzy (and isn't useful).
 *
 * The "perfect" value is when `edge` represents between 0.5 and 1 pixels.
 * My maths say 0.5, but experimentation say 1.0. Hmm.
 *
 * If we assume that the model and view matrices do NOT scale, then `edge` should be 1/thickness.
 *
 * If the model or view matrices scale x & y independently, then this Renderer isn't useful :-(
 *
 * The UVs from the [Mesh] have the distance along the line in X,
 * and the distance across the line in Y. Both normalised to 0..1
 * Anti-aliasing always uses [Mesh.uvs].y.
 * The gradient uses [Mesh.uvs].x when the gradient is along the line, and
 * [Mesh.uvs].y when the gradient is across the line.
 */
class StrokeAntiAliasProgram(val mirrored: Boolean) : ShaderProgram(
    VERTEX,
    if (mirrored) MIRRORED else REGULAR,
    4
) {

    val edgeLocation = getUniformLocation("edge")
    val repeatLocation = getUniformLocation("repeat")

    fun setup(texture: Texture, edge: Float, repeat: Float, modelMatrix: Matrix3x2f? = null) {
        setUniform(edgeLocation, edge * 2)
        setUniform(repeatLocation, repeat)
        enablePositionUV()
        setModelMatrix(modelMatrix)
        texture.bind()

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        if (!mirrored) {
            // Prevent "bleeding" from one end of the gradient texture to the other end.
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        } else {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        }

    }

    fun draw(points: List<IVector2>, uvs: List<IVector2>, along: Boolean) {
        if (points.size != uvs.size) {
            throw IllegalArgumentException("vertices size ${points.size} != uvs size ${uvs.size}")
        }
        requiredVertices(points.size)
        if (along) {
            for ((i, point) in points.withIndex()) {
                val uv = uvs[i]
                Renderer.floatBuffer.put(point.x).put(point.y)
                    .put(uv.x) // Using UV.x for the color
                    .put(uv.y) // Using UV.y for antialiasing
            }
        } else {
            for ((i, point) in points.withIndex()) {
                val uv = uvs[i]
                Renderer.floatBuffer.put(point.x).put(point.y)
                    .put(uv.y) // Using UV.y for both the color and antialiasing
                    .put(uv.y)
            }
        }
    }

    companion object {

        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute vec2 uv;
            
            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;
            
            varying float t;
            varying float s;
            
            void main() {
                t = uv.x;
                s = uv.y;
                gl_Position = vec4( viewMatrix * modelMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val REGULAR = """
            #version 120
            
            uniform float edge; // 0..1 Thickness of the fuzzy edge as a ratio of the line thickness.
            uniform float repeat; // Number of times to repeat the gradient.
            
            varying float t; // 0 = Left/Start 1 = Right/End (for the gradient)
            varying float s; // 0 = Left 1 = Right (for antialiasing)

            uniform sampler2D gradient; // The 1D gradient texture
                                 
            void main() {
                // Calculate the color based on `t` value and the gradient texture
                float tColor = repeat * t;
                tColor -= floor(tColor);
                vec4 color = texture2D( gradient, vec2(tColor, 0.5) );

                // Fuzz = 1 when s in the range (edge)..(1-edge)
                // But approaches 0 when s approaches 0 or 1
                float fuzz = clamp( 0, 1, ( (1 - abs(s*2 - 1)) / edge) );
                
                gl_FragColor = vec4( color.xyz, color.w * fuzz );
            }
            """.trimIndent()

        private val MIRRORED = """
            #version 120
            
            uniform float edge; // 0..1 Thickness of the fuzzy edge as a ratio of the line thickness.
            uniform float repeat; // Number of times to repeat the gradient.
            
            varying float t; // 0 = Left/Start 1 = Right/End (for the gradient)
            varying float s; // 0 = Left 1 = Right (for antialiasing)
            
            uniform sampler2D gradient; // The 1D gradient texture
            
            void main() {
                // Calculate the color based on `t` value and the gradient texture
                float tColor = repeat * t;
                tColor -= floor(tColor);
                float tMirrored = 1 - abs(tColor - 0.5) * 2;
                vec4 color = texture2D( gradient, vec2(tMirrored, 0.5) );

                // Fuzz = 1 when s in the range (edge)..(1-edge)
                // But approaches 0 when s approaches 0 or 1
                float fuzz = clamp( 0, 1, ( (1 - abs(s*2 - 1)) / edge) );

                gl_FragColor = vec4(color.xyz, color.w * fuzz );
            }
            """.trimIndent()
    }

}
