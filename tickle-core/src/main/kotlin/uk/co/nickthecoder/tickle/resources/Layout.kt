/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.resources

import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.misc.Copyable
import uk.co.nickthecoder.tickle.misc.CopyableResource
import uk.co.nickthecoder.tickle.misc.RenamableResource
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.stage.*
import uk.co.nickthecoder.tickle.util.*

class Layout private constructor(val createAttributes: () -> Attributes)

    : CopyableResource<Layout>, DeletableResource, RenamableResource {

    constructor(resources: Resources) : this({ resources.createAttributes() })

    val layoutStages = mutableMapOf<String, LayoutStage>()
    val layoutViews = mutableMapOf<String, LayoutView>()
    var comments = ""

    var defaultLayoutStage: LayoutStage?
        get() {
            return layoutStages.values.firstOrNull { it.isDefault == true } ?: layoutStages.values.firstOrNull()
        }
        set(v) {
            layoutStages.values.forEach { it.isDefault = (it === v) }
        }

    val defaultStageName: String
        get() {
            for ((name, layoutStage) in layoutStages) {
                if (layoutStage.isDefault) {
                    return name
                }
            }
            return layoutStages.entries.firstOrNull()?.key ?: ""
        }

    fun layoutViewsForStage(stageName: String): List<LayoutView> {
        return layoutViews.values.filter { it.stageName == stageName }
    }

    fun createScene(resources: Resources): Scene {
        val scene = Scene()
        layoutStages.forEach { (stageName, layoutStage) ->
            scene.addStage(stageName, layoutStage.createStage())
        }

        layoutViews.forEach { (viewName, layoutView) ->
            val view = layoutView.createView()
            val gi = resources.gameInfo
            view.flexPosition?.let { flexPosition ->
                // The initial size of the view is set to the Game's preferred size.
                // After all the actors have been added, we will call Scene.arrangeView,
                // which will reposition all the views according to the window's actual size.
                // (which may also move Actors, based on their autoPositionAlignment).
                view.rect = flexPosition.calculateRectangle(gi.width, gi.height)
            }
            view.zOrder = layoutView.zOrder
            if (view is StageView) {
                val stage = scene.stages[layoutView.stageName]
                if (stage == null) {
                    throw IllegalArgumentException("Stage ${layoutView.stageName} not found - cannot create the view")
                } else {
                    stage.addView(view)
                }
            }
            scene.addView(viewName, view)
        }

        return scene
    }

    override fun dependables(resources: Resources): List<Dependable> = resources.sceneDependables(this)

    override fun copy(resources: Resources, newName: String): Layout {
        val copy = Layout(createAttributes)
        copy.comments = comments

        layoutStages.forEach { (name, layoutStage) ->
            copy.layoutStages[name] = layoutStage.copy()
        }
        layoutViews.forEach { (name, layoutView) ->
            copy.layoutViews[name] = layoutView.copy()
        }

        resources.layouts.add(newName, copy)
        return copy
    }

    override fun delete(resources: Resources) {
        resources.layouts.remove(this)
    }

    override fun rename(resources: Resources, newName: String) {

        val oldName = resources.findNameOrNull(this)
        oldName ?: throw IllegalStateException("Could not find the existing name for the Layout, before renaming")

        resources.layouts.rename(oldName, newName)


        resources.updateScenesAfterLayoutRenamed(oldName, newName)
        resources.fireRenamed(this, oldName, newName)
        resources.save()
    }

}

class LayoutStage(val createAttributes: () -> Attributes) : Copyable<LayoutStage> {

    constructor(resources: Resources) : this({ resources.createAttributes() })

    var stageString: String = GameStage::class.java.name

    var stageAttributes: Attributes = createAttributes()

    var isDefault: Boolean = false

    var stageConstraintString: String = "uk.co.nickthecoder.tickle.editor.resources.NoStageConstraint"

    var constraintAttributes = createAttributes()

    fun createStage(): Stage {

        try {
            val klass = ScriptManager.classForName(stageString)
            val newStage = klass.getDeclaredConstructor().newInstance()
            if (newStage is Stage) {
                stageAttributes.applyToObject(newStage)
                return newStage
            } else {
                severe("'$newStage' is not a type of Stage")
            }
        } catch (e: Exception) {
            severe("Failed to create a Stage from : '$stageString'")
            e.printStackTrace()
        }
        return GameStage()

    }

    override fun copy(): LayoutStage {
        val copy = LayoutStage(createAttributes)
        copy.stageString = stageString
        copy.isDefault = false
        copy.stageConstraintString = stageConstraintString
        return copy
    }

}

class LayoutView(
    var viewString: String = ZOrderStageView::class.java.name,
    var stageName: String = "",
    val createAttributes: () -> Attributes
) : Copyable<LayoutView> {

    constructor(
        resources: Resources,
        viewString: String = ZOrderStageView::class.java.name,
        stageName: String = ""
    ) : this(
        viewString,
        stageName,
        { resources.createAttributes() }
    )

    var viewAttributes: Attributes = createAttributes()

    var zOrder: Int = 50

    var position = FlexPosition()

    fun createView(): View {

        try {
            val klass = ScriptManager.classForName(viewString)
            val newView = klass.getDeclaredConstructor().newInstance()
            if (newView is View) {
                newView.flexPosition = position.copy()
                viewAttributes.applyToObject(newView)
                return newView
            } else {
                severe("'$newView' is not a type of Stage")
            }
            if (newView is WorldView) {
                newView.flexPosition = position.copy()
            }
        } catch (e: Exception) {
            severe("Failed to create a View from : '$viewString'")
            e.printStackTrace()
        }
        return ZOrderStageView()
    }

    override fun copy(): LayoutView {
        val copy = LayoutView(viewString, stageName, createAttributes)
        copy.zOrder = zOrder
        copy.position = position.copy()

        return copy
    }
}

/**
 * Determines how a view is scaled, when [Scene.arrangeViews] is called.
 */
enum class AutoScale {
    /**
     * No scaling. Use this if you want to take care of scaling yourself. Or if the view of the world
     * should get bigger/smaller when the window is resized.
     */
    NONE,

    /**
     * Compares the aspect ratio of the view NOW, with the aspect ratio when using [GameInfo]'s window size.
     * If the view is now stretched in the X axis, then the scaling is based on the Y axis (and vice versa).
     *
     * This is useful when you must NEVER loose sight of any of the view's corners.
     * This differs from all the other AutoScale types, because the view's viewport will also get SMALLER.
     * i.e. the rectangle which is calculated by [FlexPosition] is reduced in size.
     */
    LETTERBOX,

    /**
     * The opposite of [LETTERBOX].
     * If the view is stretched wide, then the X axis remains "good", but less of the world is seen in the
     * Y direction.
     */
    CROP,

    /**
     * Similar to [LETTERBOX], but instead of blank space around the edges, the view extends into those
     * regions. I use this on menu scenes, which has a background image which is larger than the view.
     * If the window is stretched wide, then the menu will occupy the middle area, but the background
     * image can still be seen on the left and right (assuming it is big enough).
     */
    BEST_FIT,

    /**
     * Scales based on the view's current width, compared to its width using [GameInfo]'s window size.
     * It the window is stretched wide, then less will be seen in the Y direction.
     *
     * This could also be called `CROP_Y`.
     */
    FIT_X,

    /**
     * Scales based on the view's current height, compared to its height using [GameInfo]'s window size.
     * It the window is stretched tall, then less will be seen in the X direction.
     *
     * This could also be called `CROP_X`.
     */
    FIT_Y,

    /**
     * A compromise. Scales based on the average scaling required for a perfect fit in X and Y.
     * If the window is stretched wide, the a little bit MORE will be seen in the X direction, and a little bit
     * LESS in the Y direction.
     *
     * e.g. If the perfect scale factor in X is 1, and the perfect scale factor in Y is 4,
     * then the actual scaling factor is 2.5 : (1 + 4) / 2
     */
    FIT_AVERAGE,

    /**
     * Very similar to [FIT_AVERAGE], but instead of `(scaleX + scaleY)/2`, we do `Math.sqrt( scaleX * scaleY )`.
     *
     * e.g. If the perfect scale factor in X is 1, and the perfect scale factor in Y is 4,
     * then the actual scaling factor is 2 : Sqrt(1 * 4)
     */
    FIT_GEOMETRIC;

}
