package uk.co.nickthecoder.tickle.graphics.programs

import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.util.IVector2

class TextureProgram : ShaderProgram(VERTEX, FRAGMENT, 4) {

    fun setup(texture: Texture) {
        Texture.unbind() // TODO Needed?
        enablePositionUV()
        texture.bind()
    }

    fun drawPose(pose: Pose, x: Float, y: Float) {
        val left = (x - pose.offsetX)
        val bottom = (y - pose.offsetY)
        val right = left + pose.pixelRect.width
        val top = bottom + pose.pixelRect.height

        val tLeft = pose.rect.left
        val tRight = pose.rect.right
        val tBottom = pose.rect.bottom
        val tTop = pose.rect.top

        requiredTriangles(2)
        Renderer.floatBuffer
            .put(left).put(bottom).put(tLeft).put(tBottom)
            .put(right).put(bottom).put(tRight).put(tBottom)
            .put(right).put(top).put(tRight).put(tTop)

            .put(right).put(top).put(tRight).put(tTop)
            .put(left).put(top).put(tLeft).put(tTop)
            .put(left).put(bottom).put(tLeft).put(tBottom)
    }

    fun drawPose(pose: Pose, x: Float, y: Float, scale: IVector2) {
        val left = x - pose.offsetX * scale.x
        val bottom = y - pose.offsetY * scale.y
        val right = left + pose.pixelRect.width * scale.x
        val top = bottom + pose.pixelRect.height * scale.y

        val tLeft = pose.rect.left
        val tRight = pose.rect.right
        val tBottom = pose.rect.bottom
        val tTop = pose.rect.top

        requiredTriangles(2)
        Renderer.floatBuffer
            .put(left).put(bottom).put(tLeft).put(tBottom)
            .put(right).put(bottom).put(tRight).put(tBottom)
            .put(right).put(top).put(tRight).put(tTop)

            .put(right).put(top).put(tRight).put(tTop)
            .put(left).put(top).put(tLeft).put(tTop)
            .put(left).put(bottom).put(tLeft).put(tBottom)
    }

    fun draw(
        left: Float, bottom: Float, right: Float, top: Float,
        tLeft: Float, tBottom: Float, tRight: Float, tTop: Float
    ) {
        requiredTriangles(2)
        Renderer.floatBuffer
            .put(left).put(bottom).put(tLeft).put(tBottom)
            .put(right).put(bottom).put(tRight).put(tBottom)
            .put(right).put(top).put(tRight).put(tTop)

            .put(right).put(top).put(tRight).put(tTop)
            .put(left).put(top).put(tLeft).put(tTop)
            .put(left).put(bottom).put(tLeft).put(tBottom)

    }

    companion object {
        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute vec2 uv;

            uniform mat3 viewMatrix;
            
            varying vec2 textureCoord;

            void main() {
                textureCoord = uv;

                gl_Position = vec4( viewMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val FRAGMENT = """
            #version 120
            
            varying vec2 textureCoord;

            uniform sampler2D texImage;

            void main() {
                gl_FragColor = texture2D(texImage, textureCoord);
            }

            """.trimIndent()
    }
}