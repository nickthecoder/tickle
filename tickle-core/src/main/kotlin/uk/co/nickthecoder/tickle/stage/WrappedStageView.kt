/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.stage

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.ActorDetails
import uk.co.nickthecoder.tickle.collision.Overlapping
import uk.co.nickthecoder.tickle.collision.Touching
import uk.co.nickthecoder.tickle.collision.WrappedOverlapping
import uk.co.nickthecoder.tickle.collision.WrappedTouching
import uk.co.nickthecoder.tickle.util.Attribute
import uk.co.nickthecoder.tickle.util.Vector2
import java.util.*

/**
 * A Wrapped Stage View is a StageView like the one seen on the class game Asteroids.
 *
 * Rocks that move to the right of the screen appear on the left.
 * While the rock is near the edge, it appears partially on the left AND the right.
 * Likewise with the top and bottom edges.
 *
 * If you only want the view to wrap at the left and right, then set [joinBottom] and [joinTop]
 * way beyond the stage's extent (so that the join will never be seen) e.g. -10000 and 10000
 *
 * Often the bounds is the same as the view's size (as in the case of Asteroids).
 * But you may set your own size by creating a sub-class and overriding
 * [joinLeft], [joinRight], [joinTop] and [joinBottom].
 *
 * Fun fact, a world that is joined at only one pair of edges is a cylinder
 * (imagine bending a piece of paper into a cylinder).
 * A world that it joined a at all edges is a torus (doughnut) - Imagine
 * deforming a cylinder so that its ends meet.
 *
 * While the view is drawn (calling [actorsToDraw]), the Actor's position will jump about when they are near the edge.
 * For example, if [joinLeft] = 0 and [joinRight]=1000, then an actor at x=10 will jump from x=10 to x=990,
 * and back again.
 * At the end of the iteration, every actor will be `normalised`, such that its position is within the range
 * x in [joinLeft]..[joinRight] and y in [joinBottom]..[joinTop].
 *
 * You can NOT use WrappedStage in combination with the physics engine (because the physics engine
 * would get *very* confused when the Actor's positions jump so much).
 *
 * NOTE, when an [Actor] is in a corner it will appear 4 times, and at an edge, 2 times.
 *
 **/
open class WrappedStageView(comparator: Comparator<ActorDetails>) : AbstractStageView(comparator) {

    constructor() : this(ZOrderComparator())

    override fun defaultOverlapping(): Overlapping = WrappedOverlapping(this, super.defaultOverlapping())
    override fun defaultTouching(): Touching = WrappedTouching(this, super.defaultTouching())

    /**
     * The world x value where the left edge wraps around to the right edge.
     * The default value is 0.0.
     *
     * If you don't want the left/right edges to be joined, set [joinLeft] to e.g. -10,000
     * and [joinRight] to 10,000.
     */
    @Attribute
    var joinLeft = 0f

    /**
     * The world x value where the top edge wraps around to the bottom edge.
     *
     * If you don't want the top/bottom edges to be joined, set bottom to e.g. -10,000
     * and [joinTop] to 10,000.
     */
    @Attribute
    var joinTop = 0f

    /**
     * The world x value where the right edge wraps around to the left edge.
     *
     * If you don't want the left/right edges to be joined, set [joinLeft] to e.g. -10,000
     * and [joinRight] to 10,000.
     */
    @Attribute
    var joinRight = 0f

    @Attribute
    var joinBottom = 0f


    override fun findActorAt(point: Vector2): Actor? {
        val movedPoint = Vector2(point)
        for (y in listOf(0, -1, 1)) {
            movedPoint.y = point.y + rect.height * y
            for (x in listOf(0, -1, 1)) {
                movedPoint.x = point.x + rect.width * x
                val actor = super.findActorAt(movedPoint)
                if (actor != null) return actor
            }
        }
        return null
    }

    override fun findActorsAt(point: Vector2): Iterable<Actor> {
        val result = mutableSetOf<Actor>()
        val movedPoint = Vector2(point)
        for (y in listOf(0, -1, 1)) {
            movedPoint.y = point.y + rect.height * y
            for (x in listOf(0, -1, 1)) {
                movedPoint.x = point.x + rect.width * x
                result.addAll(super.findActorsAt(movedPoint))
            }
        }
        return result
    }

    /**
     * Returns an Iterator of all actors, but each actor may be repeated more than once.
     * If an actor is not near a wrapped edge, it will appear in the iterator only once.
     * However, if it is near a join, it will appear in the iterator twice (or four times
     * if it is near a joined top/bottom edge and a joined left/right edge).
     * When appearing multiple times, its position is adjusted accordingly each time
     * it is returned from the iterator.
     */
    override fun actorsToDraw(): Iterator<Actor> {
        return WrappedActorsIterator(super.actorsToDraw())
    }

    /**
     * The same as [actorsToDraw], but backwards. i.e. an actor in a corner will be returned FOUR times,
     * with different positions each time.
     *
     * Therefore this is suitable to find an [Actor] at a given point.
     */
    override fun actorsBackwards(all: Boolean): Iterator<Actor> {
        return WrappedActorsIterator(super.actorsBackwards(all))
    }


    /**
     * A wrapped view : if actors move outside of the wrapped bounds, then they appear on the other side.
     * If an actor is not straddling a join, then it will only be returned from the iterator once.
     * If it straddling a corner, it will be returned 4 times.
     * If it is straddling a left/right or top/bottom edge, then it will be returned twice.
     *
     * When returning an actor from this iterator more than once, the actor's position will be "normalised"
     * on the last iteration, but will be outside these bounds on the previous iterations.
     */
    private inner class WrappedActorsIterator(private val iterator: Iterator<Actor>) : Iterator<Actor> {

        private val width = joinRight - joinLeft
        private val height = joinTop - joinBottom

        private var nextActor: Actor? = null

        /**
         * Defines if we need to adjust x and y to duplicate the actor in another location.
         */
        private var columns: Int = 1
        private var rows: Int = 1

        /**
         * The column/row for the next call to [next].
         */
        private var column: Int = 0
        private var row: Int = 0

        // Either -width or width
        private var adjustmentX = 0f
        private var adjustmentY = 0f

        init {
            // We are always one step ahead, so that hasNext() is know without further calculation.
            // next() will return [nextActor] and also "increment" it to the next one, ready for the next call.
            nextActor()
        }

        private fun nextActor() {

            nextActor = if (iterator.hasNext()) iterator.next() else null
            nextActor?.let { nextActor ->

                val worldRect = nextActor.appearance.worldRect()

                // Work out how many columns and rows.
                // Also work out which direction we should be moving in
                // (if there are more than one column/row)
                // Also, ensure that the FINAL time that each actor is returned, its position is "normalised"
                // In half the cases, we will need to "un-normalise" first

                columns = if (worldRect.right > joinRight) {
                    adjustmentX = if (nextActor.x > width) {
                        -width
                    } else {
                        nextActor.x -= width
                        width
                    }
                    2
                } else if (worldRect.left < joinLeft) {
                    adjustmentX = if (nextActor.x < 0) {
                        width
                    } else {
                        nextActor.x += width
                        -width
                    }
                    2
                } else {
                    // We don't care about dx, as it won't be used.
                    adjustmentX = 0f
                    1
                }

                rows = if (worldRect.top > joinTop) {
                    adjustmentY = if (nextActor.y > joinTop) {
                        -height
                    } else {
                        nextActor.y -= height
                        height
                    }
                    2
                } else if (worldRect.bottom < joinBottom) {
                    adjustmentY = if (nextActor.y < 0) {
                        height
                    } else {
                        nextActor.y += height
                        -height
                    }
                    2
                } else {
                    1
                }

            }
            column = 0
            row = 0

        }

        override fun hasNext(): Boolean {
            return nextActor != null
        }

        override fun next(): Actor {

            val result = nextActor!!

            // If this actor is in a corner, then we will be here 4 times.
            // AB
            // CD
            // If we are a left/right join, then
            // AB
            // And if we are at a top/right join, then
            // A
            // C
            // If not near an edge, then just
            // A

            if (columns == 1) {
                if (rows == 1) {
                    // Easiest cast. Not near a join
                    nextActor()
                } else {
                    // Two rows, 1 column (At the top/bottom join)
                    if (row++ != 0) {
                        result.position.y += adjustmentY
                        nextActor()
                    }
                }
            } else {
                if (rows == 1) {
                    // 2 columns 1 row (At the left/right join)
                    if (column++ != 0) {
                        result.position.x += adjustmentX
                        nextActor()
                    }
                } else {
                    // 2 columns 2 rows ) At the corners
                    if (column++ == 0) {
                        if (row == 1) {
                            result.position.x -= adjustmentX
                            result.position.y += adjustmentY
                        }
                    } else {
                        result.position.x += adjustmentX
                        if (row++ == 0) {
                            column = 0
                        } else {
                            nextActor()
                        }
                    }
                }
            }

            return result

        }

    }
}
