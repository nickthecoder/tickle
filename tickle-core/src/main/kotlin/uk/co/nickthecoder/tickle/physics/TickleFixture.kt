package uk.co.nickthecoder.tickle.physics

import org.jbox2d.dynamics.Filter
import org.jbox2d.dynamics.Fixture

class TickleFixture(private val fixture: Fixture) {

    /**
     * Collision groups allow a certain group of objects to never collide (negative) or always collide (positive).
     * Zero means no collision group. Non-zero group filtering always wins against the mask bits.
     */
    var filterGroup: Int
        get() = fixture.filterData.groupIndex
        set(v) {
            setFilterData(v, fixture.filterData.categoryBits, fixture.filterData.maskBits)
        }

    /**
     * In the Editor, this is labelled as "I Am".
     *
     * This is a set of bits, held as a single Int.
     * For example, if the value is 5, it means bits 1 and 4 are set,
     * and therefore it will collide with anything with bits 1 and 4 set in [filterMask].
     */
    var filterCategory: Int
        get() = fixture.filterData.categoryBits
        set(v) {
            setFilterData(fixture.filterData.groupIndex, v, fixture.filterData.maskBits)
        }

    /**
     * In the Editor, this is labelled as "I Collide With".
     *
     * This is a set of bits, held as a single Int.
     * For example, if the value is 5, it means bits 1 and 4 are set,
     * and therefore it will collide with anything with bits 1 or 4 set in [filterCategory].
     */
    var filterMask: Int
        get() = fixture.filterData.maskBits
        set(v) {
            setFilterData(fixture.filterData.groupIndex, fixture.filterData.categoryBits, v)
        }


    /**
     * In JBox2d, we cannot change individual values of [Filter], we can only change the whole
     * [Filter] object.
     */
    fun setFilterData(group: Int, iAm: Int, iCollideWith: Int) {
        val filter = Filter().apply {
            categoryBits = iAm
            maskBits = iCollideWith
            groupIndex = group
        }

        fixture.filterData = filter
    }

}
