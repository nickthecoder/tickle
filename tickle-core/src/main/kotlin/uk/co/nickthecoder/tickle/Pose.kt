/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.ResizeType.*
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.misc.Copyable
import uk.co.nickthecoder.tickle.misc.CopyableResource
import uk.co.nickthecoder.tickle.misc.RenamableResource
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.util.*

/**
 * Determines how a [Pose] should be drawn.
 *
 *  * [SCALE] -> [PoseAppearance]
 *  * [TILE] -> [TiledAppearance]
 *  * [NINE_PATCH] -> [NinePatchAppearance]
 *
 * [Actor.scale] is only used with [PoseAppearance].
 * [TiledAppearance] and [NinePatchAppearance] have their own `size` property.
 */
enum class ResizeType { SCALE, TILE, NINE_PATCH }


/**
 * Rather than having every image in its own .png (or .jpg) file, it is very common to have lots of
 * images in a single file. A [Texture] hold information about the composite, and [Pose] contains the
 * information about each image within that [Texture].
 *
 * In other contexts, [Texture] is referred to as a `Sprite Sheet`, and [Pose] is referred to as a `Sprite`.
 *
 * For simple game objects, which are not animated, [Costume] and [Pose] have a 1 : 1 relationship.
 * In such cases, I suggest you use the same name for the [Costume] and the [Pose].
 *
 * However, if a game object is animated, then a [Costume] will have many [Pose]s (one for each frame of the animation).
 *
 */
class Pose(
    val texture: Texture,
    rect: YDownRect = YDownRect(0, 0, texture.width, texture.height)
) : CopyableResource<Pose>, Copyable<Pose>, DeletableResource, RenamableResource, Dependable {

    /**
     * The position of this Pose within the [texture]. This is rare case, where the Y axis points
     * 'downwards'. This is because it is normal for images to have the (0,0) pixel in the top left corner.
     */
    var pixelRect: YDownRect = rect
        set(v) {
            field = v
            updateRect()
        }

    var offsetX: Float = 0f
    var offsetY: Float = 0f

    /**
     * Points other than the offsetX,Y, which can be snapped to.
     * This is used within the `Editor` only, and has no bearing during game play.
     */
    var snapPoints = mutableListOf<Vector2>()

    /**
     * The natural direction. i.e. if the [Actor] moved "forward", which mathematical angle would that be?
     * For the default value of 0, the image is pointing to the right.
     *
     * If the [Actor] does not move or rotate, the [direction] is often of no importance.
     */
    val direction = Angle()

    /**
     * The [Actor]'s [Appearance] will differ based on the resizeType :
     *
     *  * [ResizeType.TILE] -> [TiledAppearance]
     *  * [ResizeType.NINE_PATCH] -> [NinePatchAppearance]
     *  * otherwise -> [PoseAppearance]
     *
     * If this pose will not be resized, you should leave this with the default value of [ResizeType.SCALE].
     */
    var resizeType = SCALE

    /**
     * The margins for the nine patch, when [resizeType] == [ResizeType.NINE_PATCH]
     */
    val ninePatchMargins = IntegerMargins(0, 0, 0, 0)

    /**
     * For documentation purposes only. Often left blank!
     */
    var comment = ""

    /**
     * A rectangle relative to the size of the whole texture. Used for rendering only.
     */
    internal val rect = Rect(0f, 0f, 1f, 1f)

    init {
        updateRect()
    }

    internal fun updateRect() {
        rect.left = pixelRect.left.toFloat() / texture.width
        rect.bottom = pixelRect.bottom.toFloat() / texture.height
        rect.right = pixelRect.right.toFloat() / texture.width
        rect.top = pixelRect.top.toFloat() / texture.height
    }

    private val WHITE = Color.white()

    @Deprecated(message = "", replaceWith = ReplaceWith(""))
    fun draw(renderer: Renderer, x: Float, y: Float, color: Color = WHITE, modelMatrix: Matrix3x2f? = null) {
        renderer.drawPose(this, x, y, color, modelMatrix)
    }


    // Dependency

    // Deletable
    override fun dependables(resources: Resources): List<Costume> {
        return resources.costumes.items().values.filter { it.dependsOn(this) }
    }

    override fun delete(resources: Resources) {
        resources.poses.remove(this)
    }


    // Renamable
    override fun rename(resources: Resources, newName: String) {
        resources.poses.rename(this, newName)
    }


    // Copyable
    override fun copy(resources: Resources, newName: String): Pose {
        val copy = copy()
        resources.poses.add(newName, copy)
        return copy
    }

    override fun copy(): Pose {
        val copy = Pose(texture, YDownRect(pixelRect.left, pixelRect.top, pixelRect.right, pixelRect.bottom))
        copy.offsetX = offsetX
        copy.offsetY = offsetY
        copy.direction.radians = direction.radians
        copy.resizeType = resizeType
        with(copy.ninePatchMargins) {
            left = ninePatchMargins.left
            bottom = ninePatchMargins.bottom
            right = ninePatchMargins.right
            top = ninePatchMargins.top
        }
        copy.comment = comment
        return copy
    }

    override fun toString(): String {
        return "Pose rect=$pixelRect offset=($offsetX , $offsetY) direction=$direction"
    }
}
