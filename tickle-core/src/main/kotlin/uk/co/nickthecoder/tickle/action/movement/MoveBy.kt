/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.util.Vector2
import uk.co.nickthecoder.tickle.misc.lerp

open class MoveBy(
    val position: Vector2,
    seconds: Float,
    val amount: Vector2,
    ease: Ease
)

    : TimedAnimation(seconds, ease) {

    constructor(position: Vector2, seconds: Float, amount: Vector2) : this(position, seconds, amount, LinearEase.instance)

    private var initialPosition = Vector2()
    private var finalPosition = Vector2()

    override fun storeInitialValue() {
        initialPosition.set( position )
        finalPosition.setPlus(position, amount)
    }

    override fun update(t: Float) {
        lerp(initialPosition, finalPosition, t, position)
    }

}


open class MoveXBy(
        val position: Vector2,
        seconds: Float,
        val amount: Float,
        ease: Ease = LinearEase.instance)

    : TimedAnimation(seconds, ease) {

    constructor(position: Vector2, seconds: Float, amount: Float) : this(position, seconds, amount, LinearEase.instance)

    private var initialPosition = 0f
    private var finalPosition = 0f

    override fun storeInitialValue() {
        initialPosition = position.x
        finalPosition = initialPosition + amount
    }

    override fun update(t: Float) {
        position.x = lerp(initialPosition, finalPosition, t)
    }

}

open class MoveYBy(
        val position: Vector2,
        seconds: Float,
        val amount: Float,
        ease: Ease
)

    : TimedAnimation(seconds, ease) {

    constructor(position: Vector2, seconds: Float, amount: Float) : this(position, seconds, amount, LinearEase.instance)

    private var initialPosition = 0f
    private var finalPosition = 0f

    override fun storeInitialValue() {
        initialPosition = position.y
        finalPosition = initialPosition + amount
    }

    override fun update(t: Float) {
        position.y = lerp(initialPosition, finalPosition, t)
    }

}
