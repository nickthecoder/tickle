/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action

object Eases {

    @JvmStatic
    val linear: Ease = LinearEase.instance

    @JvmStatic
    val defaultEase: Ease = BezierEase(0.250f, 0.100f, 0.250f, 1.000f)

    @JvmStatic
    val easeIn: Ease = BezierEase(0.420f, 0.000f, 1.000f, 1.000f)

    @JvmStatic
    val easeInQuad: Ease = BezierEase(0.550f, 0.085f, 0.680f, 0.530f)

    @JvmStatic
    val easeInCubic: Ease = BezierEase(0.895f, 0.030f, 0.685f, 0.220f)

    @JvmStatic
    val easeInExpo: Ease = BezierEase(0.950f, 0.050f, 0.795f, 0.035f)

    @JvmStatic
    val easeInCirc: Ease = BezierEase(0.600f, 0.040f, 0.980f, 0.335f)

    /**
     * Take a `run-up` (returning -ve values for a input values near 0).
     */
    @JvmStatic
    val easeInBack: Ease = BezierEase(0.610f, -0.255f, 0.730f, 0.015f)

    @JvmStatic
    val easeOut: Ease = BezierEase(0.000f, 0.000f, 0.580f, 1.000f)

    @JvmStatic
    val easeOutQuad: Ease = BezierEase(0.250f, 0.460f, 0.450f, 0.940f)

    @JvmStatic
    val easeOutCubic: Ease = BezierEase(0.215f, 0.610f, 0.355f, 1.000f)

    @JvmStatic
    val easeOutExpo: Ease = BezierEase(0.190f, 1.000f, 0.220f, 1.000f)

    @JvmStatic
    val easeOutCirc: Ease = BezierEase(0.075f, 0.820f, 0.165f, 1.000f)

    /**
     * `overshoots` near the end (returning values > 1 when the input nears 1), but finally
     *        `correcting` i.e. ease(1) == 1.
     */
    @JvmStatic
    val easeOutBack: Ease = BezierEase(0.175f, 0.885f, 0.320f, 1.275f)

    @JvmStatic
    val easeInOut: Ease = BezierEase(0.420f, 0.000f, 0.580f, 1.000f)

    @JvmStatic
    val easeInOutQuad: Ease = BezierEase(0.455f, 0.030f, 0.515f, 0.955f)

    @JvmStatic
    val easeInOutCubic: Ease = BezierEase(0.645f, 0.045f, 0.355f, 1.000f)

    @JvmStatic
    val easeInOutExpo: Ease = BezierEase(1.000f, 0.000f, 0.000f, 1.000f)

    @JvmStatic
    val easeInOutCirc: Ease = BezierEase(0.785f, 0.135f, 0.150f, 0.860f)

    /**
     * [Eases.easeInOutBack] has a run-up and an overshoot! See [easeInBack] and [easeOutBack]
     */
    @JvmStatic
    val easeInOutBack: Ease = BezierEase(0.680f, -0.550f, 0.265f, 1.550f)

    @JvmStatic
    val bounce: Ease = CompoundEase()
        .addEase(easeInQuad, 1.0f, 1.0f)
        .addEase(easeOutQuad, 0.2f, 0.8f)
        .addEase(easeInQuad, 0.2f, 1.0f)

    @JvmStatic
    val bounce2: Ease = CompoundEase()
        .addEase(easeInQuad, 1.0f, 1.0f)
        .addEase(easeOutQuad, 0.2f, 0.8f)
        .addEase(easeInQuad, 0.2f, 1.0f)
        .addEase(easeOutQuad, 0.1f, 0.95f)
        .addEase(easeInQuad, 0.1f, 1.0f)

    @JvmStatic val bounce3: Ease = CompoundEase()
            .addEase(easeInQuad, 1.0f, 1.0f)
            .addEase(easeOutQuad, 0.2f, 0.8f)
            .addEase(easeInQuad, 0.2f, 1.0f)
            .addEase(easeOutQuad, 0.1f, 0.95f)
            .addEase(easeInQuad, 0.1f, 1.0f)
            .addEase(easeOutQuad, 0.05f, 0.99f)
            .addEase(easeInQuad, 0.05f, 1.0f)

}

