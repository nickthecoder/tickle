package uk.co.nickthecoder.tickle.graphics

import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFWErrorCallback
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.info
import uk.co.nickthecoder.tickle.sound.SoundManager
import java.util.concurrent.CountDownLatch
import java.util.concurrent.LinkedBlockingDeque

/**
 */
class OpenGL {
    companion object {

        private var keepRunning = false

        private var openGLThread = Thread {
            loop()
        }.apply {
            name = "OpenGL"
            //isDaemon = true
        }

        /**
         * Used for testing only - Makes the current thread the OpenGL thread,
         * [begin] and [end] do not need to be called, and there is no looping thread.
         * Any actions added by [runLater] will never be applied.
         */
        internal fun makeOpenGLThread() {
            openGLThread = Thread.currentThread()
        }

        private val pendingActions = LinkedBlockingDeque<() -> Unit>()

        private fun loop() {
            info("Started OpenGL Thread")
            while (keepRunning) {
                val item = try {
                    pendingActions.take()
                } catch (e: InterruptedException) {
                    null
                }
                try {
                    item?.let { it() }
                } catch (e: Exception) {
                    // Ideally, we shouldn't let exceptions get this far. Catch them earlier!
                    e.printStackTrace()
                }
            }
            info("Ended OpenGL Thread")
        }

        fun runLater(action: () -> Unit) {
            pendingActions.put(action)
        }

        fun isOpenGLThread() = Thread.currentThread() === openGLThread

        fun ensureOpenGLThread() {
            if (!isOpenGLThread()) {
                throw RuntimeException("Not on OpenGL Thread")
            }
        }

        fun begin() {
            keepRunning = true
            if (!GLFW.glfwInit()) {
                throw IllegalStateException("Unable to initialize GLFW")
            }
            GLFWErrorCallback.createPrint(System.err).set()

            openGLThread.start()

        }

        fun end() {
            keepRunning = false
            onOpenGLThread { }

            info("Ending OpenGL")

            ignoreErrors { Renderer.cleanUp() }
            info("Cleaned up Renderer")

            ignoreErrors { SoundManager.cleanUp() }
            info("Cleaned up SoundManager")

            // Terminate GLFW and free the error callback
            ignoreErrors { GLFW.glfwTerminate() }
            info("Terminated GL")
            ignoreErrors { GLFW.glfwSetErrorCallback(null).free() }
            info("Removed GL Error Callback")
        }

    }
}

fun <T> onOpenGLThread(action: () -> T): T {
    if (OpenGL.isOpenGLThread()) {
        return action()
    }
    val countdown = CountDownLatch(1)
    var result: T? = null
    var exception: Exception? = null
    OpenGL.runLater {
        try {
            result = action()
            countdown.countDown()
        } catch (e: Exception) {
            exception = e
        }
    }
    countdown.await()
    exception?.let { throw it }
    return result!!
}

/**
 * When cleaning up OpenGL, we don't want an exception to prevent a clean shutdown.
 * So we often want to ignore any errors.
 */
fun ignoreErrors(action: () -> Any?) {
    try {
        action()
    } catch (e: Exception) {
    }
}
