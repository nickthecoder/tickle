/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.physics

import org.jbox2d.callbacks.ContactListener
import org.jbox2d.common.Vec2
import org.jbox2d.dynamics.Body
import org.jbox2d.dynamics.World
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Clock
import uk.co.nickthecoder.tickle.GameInfo
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.ImmutableVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Part of the Physics Engine, which allows objects to collide realistically with each other.
 *
 * Tickle uses a library called JBox2d, which has a few quirks, that Tickle tries to hide from you!
 *
 * [TickleWorld] is a wrapper around JBox2D's [World] class.
 * (JBox2D is written is C-style code, because it was ported from C). This [TickleWorld] wrapper,
 * is more in keeping with object oriented languages.
 *
 * There are wrappers for all other JBox2D objects too (e.g. [TickleBody] is a wrapper of [Body]).
 * One of the important aspects of these wrappers is to convert between units of measure automatically.
 * According to JBox2D's documentation, a world should be no more than 10x10.
 * Therefore in [GameInfo.physicsInfo] we have a scaling factor, which is used to convert between
 * tickle's coordinates, and JBox2D's.
 * You should never need to use JBox2D's coordinate system ;-)
 *
 * The values for [gravity], [scale] etc should be taken from [GameInfo.physicsInfo].
 *
 * NOTE. Many properties of the physics engine uses [ImmutableVector2], rather than [Vector2].
 * This is because when we change the values, we have wrappers around JBox2D's classes,
 * and if we used [Vector2], then it would be possible to change the x or y values,
 * without the wrapper noticing the change (and therefore, at best, nothing would happen).
 */
class TickleWorld(
    gravity: IVector2 = Vector2(0.0f, 0.0f),
    val scale: Float,
    val timeStep: Float,
    val velocityIterations: Int = 8,
    val positionIterations: Int = 3
) {

    val jBox2dWorld = JBox2DWorld(tickleToPhysics(gravity), true)

    private var accumulator: Double = 0.0

    var gravity: IVector2
        get() = physicsToImmutableTickle(jBox2dWorld.gravity)
        set(v) {
            if (jBox2dWorld.gravity.x == 0f && jBox2dWorld.gravity.y == 0f) {
                // Force all objects to become awake. Without this they won't start to move
                // if they were previously in zero G.
                for (body in bodies()) {
                    body.isAwake = true
                }
            }
            jBox2dWorld.gravity = tickleToPhysics(v)
        }

    private var previousTickSeconds = 0.0

    init {
        previousTickSeconds = Clock.seconds
    }

    fun tickleBodies(): Iterator<TickleBody> = TickleBodyIterator()
    fun bodies(): Iterator<Body> = BodyIterator()

    /**
     * Convert a length in Tickle's world coordinates to JBox2d's coordinates.
     *
     * Note, game code shouldn't need to perform this conversion, as Tickle converts between coordinate
     * systems automatically.
     *
     * 1 unit in JBox2s is worth [scale] units in Tickle's coordinate system.
     */
    fun tickleToPhysics(physicsLength: Float) = physicsLength / scale

    /**
     * Convert a JBox2d length into Tickle's coordinate system.
     *
     * Note, game code shouldn't need to perform this conversion, as Tickle converts between coordinate
     * systems automatically.
     *
     * 1 unit in JBox2s is worth [scale] units in Tickle's coordinate system.
     */
    fun physicsToTickle(jBox2dLength: Float) = jBox2dLength * scale

    /**
     * Convert a Tickle world vector to JBox2d's Vec2.
     *
     * Note, game code shouldn't need to perform this conversion, as Tickle converts between coordinate
     * systems automatically.
     *
     * 1 unit in JBox2s is worth [scale] units in Tickle's coordinate system.
     */
    fun tickleToPhysics(vector: IVector2) = Vec2(tickleToPhysics(vector.x), tickleToPhysics(vector.y))

    /**
     * Convert a JBox2d vector into Tickle's coordinate system.
     *
     * Note, game code shouldn't need to perform this conversion, as Tickle converts between coordinate
     * systems automatically.
     *
     * 1 unit in JBox2s is worth [scale] units in Tickle's coordinate system.
     */
    fun physicsToTickle(vec2: Vec2) = Vector2(physicsToTickle(vec2.x), physicsToTickle(vec2.y))
    fun physicsToImmutableTickle(vec2: Vec2) = ImmutableVector2(physicsToTickle(vec2.x), physicsToTickle(vec2.y))

    /**
     * Convert a Tickle world vector to JBox2d's Vec2.
     *
     * Note, game code shouldn't need to perform this conversion, as Tickle converts between coordinate
     * systems automatically.
     *
     * Unlike the other [tickleToPhysics], this doesn't create a *new* [Vec2], but instead
     * updates an existing one.
     *
     * 1 unit in JBox2s is worth [scale] units in Tickle's coordinate system.
     */
    fun tickleToPhysics(to: Vec2, from: IVector2) {
        to.x = tickleToPhysics(from.x)
        to.y = tickleToPhysics(from.y)
    }

    /**
     * Convert a JBox2d's vector into Tickle coordinate system.
     *
     * Note, game code shouldn't need to perform this conversion, as Tickle converts between coordinate
     * systems automatically.
     *
     * Unlike the other [physicsToTickle], this doesn't create a *new* [Vector2], but instead
     * updates an existing one.
     *
     * 1 unit in JBox2s is worth [scale] units in Tickle's coordinate system.
     */
    fun physicsToTickle(to: Vector2, from: Vec2) {
        to.x = physicsToTickle(from.x)
        to.y = physicsToTickle(from.y)
    }

    fun createBody(bodyDef: TickleBodyDef, actor: Actor): TickleBody {
        bodyDef.position = actor.position
        bodyDef.angle = Angle.radians(actor.direction.radians - actor.appearance.directionRadians)

        val body = bodyDef.createBody(this, actor)// jBox2dWorld.createBody(bodyDef.delegated)
        bodyDef.fixtureDefs.forEach { fixtureDefOrig ->
            val fixtureDef = fixtureDefOrig.copy()
            val shape = fixtureDef.shapeDef.createShape(this)
            fixtureDef.shape = shape
            body.jBox2DBody.createFixture(fixtureDef)

        }
        actor.body = body
        return body
    }

    internal fun destroyBody(body: TickleBody) {
        jBox2dWorld.destroyBody(body.jBox2DBody)
    }

    internal fun resetAccumulator() {
        accumulator = 0.0
        previousTickSeconds = Clock.seconds
    }

    internal fun tick() {
        if (previousTickSeconds == 0.0) {
            previousTickSeconds = Clock.seconds
        }
        accumulator += (Clock.seconds - previousTickSeconds)

        if (accumulator > timeStep) {

            // First make sure that the body is up to date. If the Actor's position or direction have been changed by game code,
            // then the Body will need to be updated before we can call "step".
            var body = jBox2dWorld.bodyList
            while (body != null) {
                val tickleBody = body.userData as TickleBody
                tickleBody.actor.ensureBodyIsUpToDate()
                body = body.next
            }

            jBox2dWorld.step(timeStep, velocityIterations, positionIterations)
            accumulator -= timeStep

            // We've performed ONE step for this frame already.
            // But if the frame rate is low, then we may need to do another one.
            // Note. timeStep * 1.5 is used rather than 1.0 so that if the actual frame rate is
            // very close to the required frame rate we don't end up doing 2 frames in one iteration,
            // and then zero in the next.
            while (accumulator > timeStep * 1.5) {
                jBox2dWorld.step(timeStep, velocityIterations, positionIterations)
                accumulator -= timeStep
            }

            // Update the actor's positions and directions
            body = jBox2dWorld.bodyList
            while (body != null) {
                val tickleBody = body.userData as TickleBody
                val actor = tickleBody.actor
                actor.updateFromBody(this)
                body = body.next
            }
        }
        previousTickSeconds = Clock.seconds
    }

    fun addContactListener(contactListener: ContactListener) {
        jBox2dWorld.addContactListener(contactListener)
    }

    fun removeContactListener(contactListener: ContactListener) {
        jBox2dWorld.removeContactListener(contactListener)
    }

    /**
     * Allows us to iterate over the C style linked list of jbox2d's bodies.
     */
    internal inner class BodyIterator : Iterator<Body> {
        var current: Body? = jBox2dWorld.bodyList
        override fun hasNext() = current != null
        override fun next(): Body {
            val result = current
            current = current?.next
            return result!!
        }
    }

    /**
     * Allows us to iterate over all [TickleBody]s within the [TickleWorld].
     */
    internal inner class TickleBodyIterator : Iterator<TickleBody> {
        var current: Body? = jBox2dWorld.bodyList
        override fun hasNext() = current != null
        override fun next(): TickleBody {
            val result = current
            current = current?.next
            return result!!.userData as TickleBody
        }
    }

}

class JBox2DWorld(gravity: Vec2, doSleep: Boolean) : World(gravity, doSleep) {

    fun addContactListener(contactListener: ContactListener) {
        when (val existingListener = m_contactManager.m_contactListener) {
            null -> {
                setContactListener(contactListener)
            }
            is CompoundContactListener -> {
                existingListener.listeners.add(contactListener)
            }
            else -> {
                val compound = CompoundContactListener()
                compound.listeners.add(existingListener)
                compound.listeners.add(contactListener)
                setContactListener(compound)
            }
        }
    }

    fun removeContactListener(contactListener: ContactListener) {
        val existingListener = m_contactManager.m_contactListener
        if (existingListener === contactListener) {
            setContactListener(null)
        } else if (existingListener is CompoundContactListener) {
            existingListener.listeners.remove(contactListener)
            if (existingListener.listeners.isEmpty()) {
                setContactListener(null)
            }
        }
    }

}
