/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.misc

import org.lwjgl.opengl.GL11.GL_RGBA
import uk.co.nickthecoder.tickle.graphics.Texture
import java.nio.ByteBuffer

/**
 * For manipulating images as a byte array.
 * The order of the channels in the byte array is RGBA.
 * When reading/writing pixels as ints, then the MSByte is the alpha channel i.e.
 * the int is 0xAABBGGRR
 */
class PixelArray(val width: Int, val height: Int, val array: ByteArray) {

    constructor(width: Int, height: Int) : this(width, height, ByteArray(width * height * 4))

    constructor(texture: Texture) : this(texture.width, texture.height, texture.read())

    fun redAt(x: Int, y: Int) = array[(y * width + x) * 4].toInt() and 0xff
    fun greenAt(x: Int, y: Int) = array[(y * width + x) * 4 + 1].toInt() and 0xff
    fun blueAt(x: Int, y: Int) = array[(y * width + x) * 4 + 2].toInt() and 0xff
    fun alphaAt(x: Int, y: Int) = array[(y * width + x) * 4 + 3].toInt() and 0xff

    /**
     */
    fun pixelAt(x: Int, y: Int): Int {
        val offset = (y * width + x) * 4
        return array[offset].toInt() and 0xff or
                (array[offset + 1].toInt() and 0xff).shl(8) or
                (array[offset + 2].toInt() and 0xff).shl(16) or
                (array[offset + 3].toInt() and 0xff).shl(24)
    }

    /**
     * Gets a single pixel's RGB channels as an int
     */
    fun colorAt(x: Int, y: Int): Int {
        val offset = (y * width + x) * 4
        return (array[offset].toInt() and 0xff) or
                (array[offset + 1].toInt() and 0xff).shl(8) or
                (array[offset + 2].toInt() and 0xff).shl(16)
    }

    /**
     * Compatible with PixerWriter.setArgb.
     */
    fun argbColorAt(x: Int, y: Int): Int {
        val offset = (y * width + x) * 4
        return (array[offset + 2].toInt() and 0xff) or // Blue
                (array[offset + 1].toInt() and 0xff).shl(8) or // Green
                (array[offset].toInt() and 0xff).shl(16) or // Red
                (array[offset + 3].toInt() and 0xff).shl(24) // alpha
    }

    fun setPixel(x: Int, y: Int, pixel: Int) {
        val offset = (y * width + x) * 4
        array[offset] = red(pixel).toByte()
        array[offset + 1] = green(pixel).toByte()
        array[offset + 2] = blue(pixel).toByte()
        array[offset + 3] = alpha(pixel).toByte()
    }

    /**
     * Sets the RGB channels, leaving the alpha untouched.
     */
    fun setColor(x: Int, y: Int, pixel: Int) {
        val offset = (y * width + x) * 4
        array[offset] = (pixel and 0xff).toByte()
        array[offset + 1] = (pixel.shr(8) and 0xff).toByte()
        array[offset + 2] = (pixel.shr(16) and 0xff).toByte()
    }

    fun setAlpha(x: Int, y: Int, alpha: Int) {
        array[(y * width + x) * 4 + 3] = (alpha and 0xff).toByte()
    }

    fun toBuffer(): ByteBuffer {
        val buffer = ByteBuffer.allocateDirect(array.size)
        buffer.put(array)
        buffer.position(0)
        return buffer
    }

    fun toTexture() = Texture(width, height, GL_RGBA, toBuffer())


    fun dumpAlpha() {
        for (y in 0 until height) {
            for (x in 0 until width) {
                if (alphaAt(x, y) and 128 == 0) {
                    print("#")
                } else {
                    print(" ")
                }
            }
            println()
        }
    }

    companion object {
        fun alpha(pixel: Int) = pixel.shr(24) and 0xff
        fun blue(pixel: Int) = pixel.shr(16) and 0xff
        fun green(pixel: Int) = pixel.shr(8) and 0xff
        fun red(pixel: Int) = pixel and 0xff

        fun pixel(red: Int, green: Int, blue: Int, alpha: Int): Int {
            return red + green.shl(8) + blue.shl(16) + alpha.shl(24)
        }

        fun color(red: Int, green: Int, blue: Int): Int {
            return red + green.shl(8) + blue.shl(16)
        }
    }
}
