/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.util.IntegerMargins
import uk.co.nickthecoder.tickle.util.Polar2d
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * This is here for backwards compatibility only.
 * Previously, Nine Patches were defined in a [Costume]'s event. But now, they are defined as part of [Pose].
 * See [Pose.resizeType].
 *
 * Note [left], [bottom], [right] and [top] are MARGINS inside the [Pose]'s rectangle.
 */
class NinePatch(var pose: Pose, left: Int, bottom: Int, right: Int, top: Int) : IntegerMargins(left, bottom, right, top)

/**
 * Draws an [Actor] using a nine-patch, which allows the actor scaled to any size, and the nine patch
 * will only scale the inner parts. The corners will be draw 1:1.
 *
 * NOTE. [Pose] has [Pose.ninePatchRect], but we use a local version of this [ninePatchRect].
 * This is because we also want to support backwards compatibility, when [Pose] didn't have it.
 * When using the `new` system, the local copy of [ninePatchRect] will be the same as the on on [Pose].
 *
 * Note. when using nine patches, the Actor's scale is ignored. Use [Actor.resize] to resize.
 * [Actor.flipX] and [Actor.flipY] are also currently ignored.
 */
class NinePatchAppearance(actor: Actor, override val pose: Pose, val margins: IntegerMargins) :
    ResizeAppearance(actor) {

    override val directionRadians: Double
        get() = pose.direction.radians

    /**
     * A 3x3 array of Poses
     */
    private val pieces = Array<Array<Pose>>(3) {
        Array<Pose>(3) {
            val piecePose = pose.copy()
            piecePose.offsetX = 0f
            piecePose.offsetY = 0f
            piecePose.resizeType = ResizeType.SCALE
            piecePose
        }
    }

    private val tempVector = Vector2()

    init {
        val w = pose.pixelRect.width
        val h = pose.pixelRect.height
        val widths = listOf(margins.left, w - margins.left - margins.right, margins.right)
        val heights = listOf(margins.bottom, h - margins.bottom - margins.top, margins.top)

        // Relative to the parent pose.
        val lefts = listOf(0, margins.left, w - margins.right)
        val tops = listOf(h - margins.bottom, margins.top, 0)

        for (x in 0..2) {
            for (y in 0..2) {
                val piecePose = pieces[x][y]

                piecePose.pixelRect.left = pose.pixelRect.left + lefts[x]
                piecePose.pixelRect.right = piecePose.pixelRect.left + widths[x]

                piecePose.pixelRect.top = pose.pixelRect.top + tops[y]
                piecePose.pixelRect.bottom = piecePose.pixelRect.top + heights[y]

                piecePose.updateRect()
            }
        }
        size.x = pose.pixelRect.width.toFloat()
        size.y = pose.pixelRect.height.toFloat()
        oldSize.set(size)

        sizeAlignment.x = pose.offsetX / pose.pixelRect.width
        sizeAlignment.y = pose.offsetY / pose.pixelRect.height
        oldAlignment.set(sizeAlignment)
    }

    override fun draw(renderer: Renderer) {

        val widths: Array<Float> =
            arrayOf(margins.left.toFloat(), (width() - margins.left - margins.right).toFloat(), margins.right.toFloat())
        val heights: Array<Float> = arrayOf(
            margins.bottom.toFloat(),
            (height() - margins.top - margins.bottom).toFloat(),
            margins.top.toFloat()
        )

        val lefts: Array<Float> = arrayOf(0.0f, margins.left.toFloat(), (width() - margins.right).toFloat())
        val bottoms: Array<Float> = arrayOf(0.0f, margins.bottom.toFloat(), (height() - margins.top).toFloat())

        //println("Drawing nine patch $width , $height widths=$widths heights = $heights")

        val modelMatrix = if (actor.isSimpleImage()) {
            null
        } else {
            actor.calculateModelMatrix()
        }

        for (y in 0..2) {
            for (x in 0..2) {
                val piecePose = pieces[x][y]

                if (piecePose.pixelRect.width > 0 && piecePose.pixelRect.height > 0) {
                    val left = (actor.x - offsetX()).toFloat() + lefts[x]
                    val bottom = (actor.y - offsetY()).toFloat() + bottoms[y]
                    renderer.drawTexture(
                        pose.texture,
                        left, bottom, left + widths[x], bottom + heights[y],
                        piecePose.rect,
                        actor.tint,
                        modelMatrix
                    )
                }

            }
        }
    }

    /**
     * Use a NinePatch as a line, from the Actor's position to an arbitrary point.
     * The Actor's direction is calculated from the angle between these to points. The nine patch's x size is resized to
     * the magnitude of the line, and the height remains the same.
     * Therefore, the NinePatch should be drawn with the line pointing along the X axis.
     */
    fun lineTo(x: Float, y: Float) {
        tempVector.set(x - actor.position.x, y - actor.position.y)
        actor.direction.radians = tempVector.angleRadians()
        size.x = tempVector.length()
    }

    /**
     * Use a NinePatch as a line, from the Actor's position to an arbitrary point.
     * The Actor's direction is calculated from the angle between these to points. The nine patch's x size is resized to
     * the magnitude of the line, and the height remains the same.
     * Therefore, the NinePatch should be drawn with the line pointing along the X axis.
     */
    fun lineTo(point: Vector2) {
        tempVector.set(point)
        tempVector -= actor.position
        actor.direction.radians = tempVector.angleRadians()
        size.x = tempVector.length()
    }

    /**
     * Assuming this was scaled using [lineTo], then this returns the other end of the line.
     * The "lineStart" is [Actor.position].
     */
    fun lineEnd(): Vector2 {
        val polar = Polar2d(actor.direction, size.x)
        return actor.position + polar.vector()
    }

    override fun toString() = "NinePatchAppearance pose=${pose} size=$size margins : $margins"

}
