/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.physics

import org.jbox2d.dynamics.Body
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.ImmutableVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * An object in the [TickleWorld].
 * This is a thin wrapper around JBox2d's [Body] class, which is written in a C style
 * (because it was ported from C code).
 * This wrapper is, IMHO, easier to use, and is more Java-like.
 */
class TickleBody(
        val jBox2DBody: Body,
        val tickleWorld: TickleWorld,
        val actor: Actor) {

    init {
        jBox2DBody.userData = this
    }

    val mass: Float
        get() = jBox2DBody.mass

    private val pLinearVelocity = Vector2()
    var linearVelocity: IVector2
        get() = tickleWorld.physicsToImmutableTickle(jBox2DBody.linearVelocity)
        set(v) {
            tickleWorld.tickleToPhysics(jBox2DBody.linearVelocity, v)
        }

    var angularVelocity: Float
        get() = tickleWorld.physicsToTickle(jBox2DBody.angularVelocity)
        set(v) {
            jBox2DBody.angularVelocity = v
        }

    var isAwake: Boolean
        get() = jBox2DBody.isAwake
        set(v) {
            jBox2DBody.isAwake = v
        }

    internal val joints = mutableListOf<TickleJoint<*, *>>()

    // Make a COPY, so that game code can call destroy on the joints without concurrent modification exceptions.O
    fun joints(): List<TickleJoint<*, *>> = joints.toList()

    fun scaleJoints(scaleBy: Vector2) {
        for (joint in joints().toList()) {
            // Mouse joints are weird. I don't know how to scale them correctly!
            // Maybe I'm creating the mouse joints wrong in the first place???
            if (joint !is TickleMouseJoint) {
                if (joint.actorA === actor) {
                    joint.localAnchorA = ImmutableVector2(joint.localAnchorA * scaleBy)
                }
                if (joint.actorB === actor) {
                    joint.localAnchorB = ImmutableVector2(joint.localAnchorB * scaleBy)
                }
            }
        }
    }

    fun fixtures(): Iterator<TickleFixture> = TickleFixtureIterator()

    private inner class TickleFixtureIterator : Iterator<TickleFixture> {

        private var next = jBox2DBody.fixtureList
        override fun hasNext() = next != null
        override fun next(): TickleFixture {
            val result = TickleFixture(next)
            next = next.next
            return result
        }
    }

}
