/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.misc

import org.jbox2d.dynamics.Body
import org.joml.Vector2i
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.physics.TickleBody
import uk.co.nickthecoder.tickle.util.*
import kotlin.math.roundToInt

fun lerp(from: Rect, to: Rect, t: Float, dest: Rect) {
    dest.left = lerp(from.left, to.left, t)
    dest.right = lerp(from.right, to.right, t)
    dest.top = lerp(from.top, to.top, t)
    dest.bottom = lerp(from.bottom, to.bottom, t)
}

fun lerp(from: RectInt, to: RectInt, t: Float, dest: RectInt) {
    dest.left = lerp(from.left, to.left, t)
    dest.right = lerp(from.right, to.right, t)
    dest.top = lerp(from.top, to.top, t)
    dest.bottom = lerp(from.bottom, to.bottom, t)
}

fun lerp(from: Int, to: Int, t: Float): Int = ((1 - t) * from + t * to).roundToInt()
fun lerp(from: Double, to: Double, t: Double) = (1 - t) * from + t * to
fun lerp(from: Float, to: Float, t: Float) = (1 - t) * from + t * to

fun lerp(from: IVector2, to: IVector2, t: Float) =
    ImmutableVector2((1f - t) * from.x + t * to.x, (1f - t) * from.y + t * to.y)

fun lerp(from: IVector2, to: IVector2, t: Float, dest: Vector2) {
    dest.x = lerp(from.x, to.x, t)
    dest.y = lerp(from.y, to.y, t)
}

fun Vector2.lerp(from: IVector2, to: IVector2, t: Float): Vector2 {
    lerp(from, to, t, this)
    return this
}

fun Vector2.lerp(to: IVector2, t: Float): Vector2 {
    lerp(this, to, t, this)
    return this
}

fun lerp(from: Color, to: Color, t: Float): Color {
    val s = 1 - t
    return Color(
        from.red * s + to.red * t,
        from.green * s + to.green * t,
        from.blue * s + to.blue * t,
        from.alpha * s + to.alpha * t
    )
}


fun vector2ToString(vector: Vector2) = "${vector.x},${vector.y}"
fun vector2FromString(string: String): Vector2 {
    // For backwards compatibility
    val split = if (string.contains("x")) {
        string.split("x")
    } else {
        string.split(",")
    }
    return Vector2(split[0].toFloat(), split[1].toFloat())
}

fun vector2iToString(vector: Vector2i) = "${vector.x}x${vector.y}"
fun vector2iFromString(string: String): Vector2i {
    val split = string.split("x")
    return Vector2i(split[0].toInt(), split[1].toInt())
}

/**
 * Returns an Iterable of all elements sorted according to the specified [comparator].
 */
fun <T> Iterable<T>.sortedBackwardsWith(comparator: Comparator<in T>): Iterable<T> {
    if (this is Collection) {
        if (size <= 1) return this
        @Suppress("UNCHECKED_CAST")
        val array = (toTypedArray<Any?>() as Array<T>)
        array.sortWith(comparator)
        array.reverse()
        return array.asList()
    }
    return toList().apply { sortedBackwardsWith(comparator) }
}


fun Body.tickleBody() = (userData as TickleBody)

fun Body.actor() = tickleBody().actor


// CharSequence

// Weird - Kotlin has sumByDouble, but not sumByFloat.
inline fun CharSequence.sumByFloat(selector: (Char) -> Float): Float {
    var sum = 0f
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

//

fun sqrt(value: Float) = Math.sqrt(value.toDouble()).toFloat()
