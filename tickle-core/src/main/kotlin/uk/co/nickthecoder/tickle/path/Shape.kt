package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.action.Eases
import uk.co.nickthecoder.tickle.util.*

/**
 * A Shape can define the motion of an object (i.e. the path it takes over time), as well as a Shape
 * drawn on the screen (See [VisibleShape]).
 *
 * All points are defined using the mutable Vector2, and when [ContiguousShape]s are combined into a [Path],
 * the [end] of one [ContiguousShape] shares same instance with the [start] of the next.
 * Due to the mutability, all subclasses must be wary of storing "cached" data, such as path lengths, normals etc.
 * See [dirty].
 */
interface Shape<T : Shape<T>> {

    val start: Vector2
    val end: Vector2

    /**
     * The length of the path. e.g. for a circle, it will be PI * diameter.
     */
    fun length(): Float

    /**
     * Set [into], so that it falls on this [ContiguousShape]. The distance along the path
     * is governed by [t], where 0 is the start of the path, and 1 is the end.
     *
     * [t] is normally in the range 0..1.
     * However, it is possible for it to be outside this range.
     * e.g. when using[Eases.easeInBack], [Eases.easeOutBack] and [Eases.easeInOutBack]
     */
    fun along(t: Float, into: Vector2)

    fun along(t: Float): Vector2 {
        val result = Vector2()
        along(t, result)
        return result
    }

    /**
     * Set [into] to the point [distance] units from the start position.
     */
    fun alongDistance(distance: Float, into: Vector2) {
        along(distance / length(), into)
    }

    /**
     * Returns the point [distance] units along the Shape from the start position.
     */
    fun alongDistance(distance: Float) = along(distance / length())


    /**
     * Create a duplicate, which does NOT share [Vector2] instances with the original.
     */
    fun copy(): T

    /**
     * Create a copy, in the reverse order, which shares the same [Vector2] instances as the original.
     * Consider calling [copy] to ensure that the original and the copy do not share [Vector2] instances.
     */
    fun reverse(): T

    /**
     * Create a translated copy.
     */
    fun translate(by: IVector2): T

    /**
     * Create a translated copy.
     */
    fun scale(by: IVector2): T

    /**
     * Create a translated copy.
     */
    fun rotate(by: IAngle): T

    // Convenience functions
    fun translate(x: Float, y: Float) = translate(ImmutableVector2(x, y))
    fun scale(x: Float, y: Float) = scale(ImmutableVector2(x, y))
    fun rotateDegrees(degrees: Double) = rotate(Angle.degrees(degrees))

    fun moveTo(position: Vector2) = translate(position - start)
    fun moveTo(x: Float, y: Float) = moveTo(Vector2(x, y))

    /**
     * Returns a new [ContiguousShape], whose [start] point is [position].
     */
    fun relativeTo(position: Vector2): Shape<T> {
        val delta = position - start
        return if (delta.x == 0f && delta.y == 0f) {
            this
        } else {
            translate(delta)
        }
    }

    fun toPolylines(
        distanceTolerance: Float = ContiguousShape.DEFAULT_DISTANCE_TOLERANCE,
        angleTolerance: Double = ContiguousShape.DEFAULT_ANGLE_TOLERANCE
    ): List<Polyline>

    /**
     * Called when points defining this [ContiguousShape] have changed, and any cached data is no longer valid.
     */
    fun dirty() {}
}
