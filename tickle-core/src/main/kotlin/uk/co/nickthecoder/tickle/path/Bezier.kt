/*
Tickle
Copyright (C) 2023 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
// Copyright notice for buildPoints function, ported from C++ (plus my own changes).
// https://web.archive.org/web/20180307160123/http://antigrain.com/research/adaptive_bezier/index.html
//----------------------------------------------------------------------------
// Anti-Grain Geometry (AGG) - Version 2.5
// A high quality rendering engine for C++
// Copyright (C) 2002-2006 Maxim Shemanarev
// Contact: mcseem@antigrain.com
//          mcseemagg@yahoo.com
//          http://antigrain.com
//
// AGG is free software you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation either version 2
// of the License, or (at your option) any later version.
//
// AGG is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with AGG if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
// MA 02110-1301, USA.
//----------------------------------------------------------------------------
*/
package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.util.IAngle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * A Cubic [Bezier] curve has a [start] and [end] points, as well as two control points [c1] and [c2].
 * The tangent of the curve at [start] is along the line [start]..[c1] and the tangent of the curve
 * at [end] is [c2]..[end].
 * Therefore, we can stitch together a sequence of bezier curves, which are smooth, if the
 * `a.end == b.start` and if `a.c2`, `a.end` and `b.c1` form a straight line.
 *
 * In many cases the distance `a.c2 .. a.end` and `a.end .. b.c2` are made equal.
 */
class Bezier(override val start: Vector2, val c1: Vector2, val c2: Vector2, end: Vector2) : ContiguousShape<Bezier> {

    override var end: Vector2 = end
        private set

    override fun joinEnd(to: Vector2) {
        end = to
    }

    /**
     * Approximate the length, by splitting the curve into 10 straight lines.
     */
    override fun length(): Float {
        var length = 0f
        val prev = Vector2(start)
        val next = Vector2()
        val tmp = Vector2()
        for (i in 1..10) {
            along(i.toFloat() / 10f, next)
            tmp.set(next)
            tmp -= prev
            length += tmp.length()
            prev.set(next)
        }
        return length
    }

    override fun along(t: Float, into: Vector2) {
        into.x = pointAlongBezier(start.x, c1.x, c2.x, end.x, t.toDouble())
        into.y = pointAlongBezier(start.y, c1.y, c2.y, end.y, t.toDouble())
    }

    override fun translate(by: IVector2) = Bezier(start + by, c1 + by, c2 + by, end + by)
    override fun scale(by: IVector2) = Bezier(start * by, c1 * by, c2 * by, end * by)
    override fun rotate(by: IAngle) = Bezier(start.rotate(by), c1.rotate(by), c2.rotate(by), end.rotate(by))

    override fun copy() = openOrClosed(Bezier(Vector2(end), Vector2(c2), Vector2(c1), Vector2(start)))

    override fun reverse() = Bezier(end, c2, c1, start)

    /**
     * Ported from C++
     * Many thanks.
     * https://web.archive.org/web/20180307160123/http://antigrain.com/research/adaptive_bezier/index.html
     */
    override fun buildPoints(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {

        val distanceToleranceSquare = distanceTolerance * distanceTolerance

        fun calcDistanceSquared(x1: Float, y1: Float, x2: Float, y2: Float): Float {
            val dx = x2 - x1
            val dy = y2 - y1
            return dx * dx + dy * dy
        }

        fun recursiveBezier(
            x1: Float, y1: Float,
            x2: Float, y2: Float,
            x3: Float, y3: Float,
            x4: Float, y4: Float,
            level: Int
        ) {
            if (level > curveRecursionLimit) {
                return
            }

            // Calculate all the mid-points of the line segments
            //----------------------
            val x12 = (x1 + x2) / 2
            val y12 = (y1 + y2) / 2
            val x23 = (x2 + x3) / 2
            val y23 = (y2 + y3) / 2
            val x34 = (x3 + x4) / 2
            val y34 = (y3 + y4) / 2
            val x123 = (x12 + x23) / 2
            val y123 = (y12 + y23) / 2
            val x234 = (x23 + x34) / 2
            val y234 = (y23 + y34) / 2
            val x1234 = (x123 + x234) / 2
            val y1234 = (y123 + y234) / 2


            // Try to approximate the full cubic curve by a single straight line
            //------------------
            val dx = x4 - x1
            val dy = y4 - y1

            var d2 = Math.abs(((x2 - x4) * dy - (y2 - y4) * dx))
            var d3 = Math.abs(((x3 - x4) * dy - (y3 - y4) * dx))
            var da1: Float
            var da2: Float
            var k: Float

            //when (case) {
            if (d2 < curveCollinearityEpsilon) {

                if (d3 < curveCollinearityEpsilon) {
                    // println("A $x1234, $y1234")

                    // All collinear OR p1==p4
                    //----------------------
                    k = dx * dx + dy * dy
                    if (k == 0f) {
                        d2 = calcDistanceSquared(x1, y1, x2, y2)
                        d3 = calcDistanceSquared(x4, y4, x3, y3)
                    } else {
                        k = 1 / k
                        da1 = x2 - x1
                        da2 = y2 - y1
                        d2 = k * (da1 * dx + da2 * dy)
                        da1 = x3 - x1
                        da2 = y3 - y1
                        d3 = k * (da1 * dx + da2 * dy)
                        if (d2 > 0 && d2 < 1 && d3 > 0 && d3 < 1) {
                            // Simple collinear case, 1---2---3---4
                            // We can leave just two endpoints
                            return
                        }
                        d2 = if (d2 <= 0) calcDistanceSquared(x2, y2, x1, y1)
                        else if (d2 >= 1) calcDistanceSquared(x2, y2, x4, y4)
                        else calcDistanceSquared(x2, y2, x1 + d2 * dx, y1 + d2 * dy)

                        d3 = if (d3 <= 0) calcDistanceSquared(x3, y3, x1, y1)
                        else if (d3 >= 1) calcDistanceSquared(x3, y3, x4, y4)
                        else calcDistanceSquared(x3, y3, x1 + d3 * dx, y1 + d3 * dy)
                    }
                    if (d2 > d3) {
                        if (d2 < distanceToleranceSquare) {
                            into.add(Vector2(x2, y2))
                            return
                        }
                    } else {
                        if (d3 < distanceToleranceSquare) {
                            into.add(Vector2(x3, y3))
                            return
                        }
                    }
                } else {
                    // println("B $x1234, $y1234")

                    // p1,p2,p4 are collinear, p3 is significant
                    //----------------------
                    if (d3 * d3 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        if (angleTolerance < curveAngleToleranceEpsilon) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = Math.abs(
                            Math.atan2((y4 - y3).toDouble(), (x4 - x3).toDouble()) -
                                    Math.atan2((y3 - y2).toDouble(), (x3 - x2).toDouble())
                        ).toFloat()
                        if (da1 >= pi) da1 = 2 * pi - da1

                        if (da1 < angleTolerance) {
                            into.add(Vector2(x2, y2))
                            into.add(Vector2(x3, y3))
                            return
                        }

                        if (m_cusp_limit != 0.0f) {
                            if (da1 > m_cusp_limit) {
                                into.add(Vector2(x3, y3))
                                return
                            }
                        }
                    }
                }

            } else {
                if (d3 < curveCollinearityEpsilon) {
                    // println("C $x1234, $y1234")

                    // p1,p3,p4 are collinear, p2 is significant
                    //----------------------
                    if (d2 * d2 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        if (angleTolerance < curveAngleToleranceEpsilon) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = Math.abs(
                            Math.atan2((y3 - y2).toDouble(), (x3 - x2).toDouble()) -
                                    Math.atan2((y2 - y1).toDouble(), (x2 - x1).toDouble())
                        ).toFloat()
                        if (da1 >= pi) da1 = 2 * pi - da1

                        if (da1 < angleTolerance) {
                            into.add(Vector2(x2, y2))
                            into.add(Vector2(x3, y3))
                            return
                        }

                        if (m_cusp_limit != 0.0f) {
                            if (da1 > m_cusp_limit) {
                                into.add(Vector2(x2, y2))
                                return
                            }
                        }
                    }

                } else {
                    // println("D $x1234, $y1234")

                    // Regular case
                    //-----------------
                    if ((d2 + d3) * (d2 + d3) <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        // If the curvature doesn't exceed the distance_tolerance value
                        // we tend to finish subdivisions.
                        //----------------------
                        if (angleTolerance < curveAngleToleranceEpsilon) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle & Cusp Condition
                        //----------------------
                        k = Math.atan2((y3 - y2).toDouble(), (x3 - x2).toDouble()).toFloat()
                        da1 = Math.abs(k - Math.atan2((y2 - y1).toDouble(), (x2 - x1).toDouble())).toFloat()
                        da2 = Math.abs(Math.atan2((y4 - y3).toDouble(), (x4 - x3).toDouble()) - k).toFloat()
                        if (da1 >= pi) da1 = 2 * pi - da1
                        if (da2 >= pi) da2 = 2 * pi - da2

                        if (da1 + da2 < angleTolerance) {
                            // Finally we can stop the recursion
                            //----------------------
                            into.add(Vector2(x23, y23))
                            return
                        }

                        if (m_cusp_limit != 0.0f) {
                            if (da1 > m_cusp_limit) {
                                into.add(Vector2(x2, y2))
                                return
                            }

                            if (da2 > m_cusp_limit) {
                                into.add(Vector2(x3, y3))
                                return
                            }
                        }
                    }
                }
            }

            // Continue subdivision
            //----------------------
            recursiveBezier(x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1)
            recursiveBezier(x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1)
        }

        //------------------------------------------------------------------------

        recursiveBezier(start.x, start.y, c1.x, c1.y, c2.x, c2.y, end.x, end.y, 0)
        into.add(end)
    }

    override fun toString() = "Bezier $start -> $end c1=$c1 c2=$c2"

    companion object {
        const val curveDistanceEpsilon = 1e-30f
        const val curveCollinearityEpsilon = 1e-30f
        const val curveAngleToleranceEpsilon = 0.01
        const val curveRecursionLimit = 30
        const val pi = 3.1415927f
        var m_cusp_limit = 0.0f
    }
}

private fun bez03(u: Double) = Math.pow((1 - u), 3.0)
private fun bez13(u: Double) = 3.0 * u * (Math.pow((1.0 - u), 2.0))
private fun bez23(u: Double) = 3.0 * (Math.pow(u, 2.0)) * (1.0 - u)
private fun bez33(u: Double) = Math.pow(u, 3.0)

fun pointAlongBezier(p0: Float, p1: Float, p2: Float, p3: Float, u: Double) =
    (bez03(u) * p0 + bez13(u) * p1 + bez23(u) * p2 + bez33(u) * p3).toFloat()
