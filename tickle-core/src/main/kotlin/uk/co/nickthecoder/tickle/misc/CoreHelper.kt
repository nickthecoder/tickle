package uk.co.nickthecoder.tickle.misc

fun clamp(v: Int, from: Int, to: Int) = Math.min(Math.max(v, from), to)
fun clamp(v: Double, from: Double, to: Double) = Math.min(Math.max(v, from), to)
fun clamp(v: Float, from: Float, to: Float) = Math.min(Math.max(v, from), to)

fun <T : Comparable<T>> min(a: T, b: T) = if (a <= b) a else b
fun <T : Comparable<T>> max(a: T, b: T) = if (a >= b) a else b

fun debugTimer(label: String, action: () -> Unit) {
    val now = System.currentTimeMillis()
    action()
    println("$label ${System.currentTimeMillis() - now} ms")
}
