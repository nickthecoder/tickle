/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.action.Action

/**
 * All effects return null.
 */
abstract class AbstractButtonEffects : ButtonEffects {

    override fun initialise(button: Button) {}
    override fun down(button: Button): Action? = null
    override fun up(button: Button): Action? = null
    override fun enter(button: Button): Action? = null
    override fun exit(button: Button): Action? = null
    override fun clicked(button: Button): Action? = null
    override fun disable(button: Button): Action? = null
    override fun enable(button: Button): Action? = null

}
