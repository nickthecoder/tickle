package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.action.Action

/**
 * Hides the [actor] when the mouse pointer is outside of the window.
 * Consider pairing this with [FollowMouse]. e.g.
 *
 *      HideWhenMouseIsOutside( actor ) and FollowMouse( actor )
 */
class HideWhenMouseIsOutside(val actor: Actor) : Action {
    override fun act(): Boolean {
        if (Game.instance.window.isMouseWithinWindow()) {
            actor.show()
        } else {
            actor.hide()
        }
        return false
    }
}