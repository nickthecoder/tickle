package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.util.*

/**
 * A simple shape, such as a straight line, an elliptical arc or a single section of a bezier curve.
 * [ContiguousShape]s can be combined into a [Path], (which is made up of many [ContiguousShape]s).
 *
 * Contiguous means you can move from [start] to [end] without jumps.
 * A [Shape] might NOT have this property.
 *
 * NOTE, a [Path] also implements this interface, so a [Path] can be treated like any other [ContiguousShape].
 * A [CompoundPath] is not contiguous, and is therefore a [Shape], not a [ContiguousShape].
 *
 */
interface ContiguousShape<T : ContiguousShape<T>> : Shape<T> {

    /**
     * Join the [end] point, so that it shares the same [Vector2] instance as [to].
     * This is used by [Path] to ensure that the segments in copied paths are correctly joined.
     * It is also used by [Path.close].
     */
    fun joinEnd(to: Vector2)

    /**
     * See [joinEnd]
     * @return true if [start] and [end] are the same instance.
     */
    fun isClosed() = start === end

    /**
     * Add all points EXCLUDING the start point.
     */
    fun buildPoints(
        into: MutableList<Vector2>,
        distanceTolerance: Float = DEFAULT_DISTANCE_TOLERANCE,
        angleTolerance: Double = DEFAULT_ANGLE_TOLERANCE
    )

    /**
     * Converts any [ContiguousShape] into a [Polyline] (a sequence of line segments).
     * You can then use [Polyline.createStrokeMesh] to render the curve.
     *
     * @param distanceTolerance Used by [Bezier] and  [EllipticalArc].
     *     If the approximated point is less than this distance away from the actual point, then
     *     the curve is not refined anymore (i.e. we stop adding extra line segments).
     * @param angleTolerance Used by [Bezier].
     *     If three consecutive points form an angle which is less than this, then
     *     the curve is not refined anymore (i.e. we stop adding extra line segments).
     */
    fun toPolyline(
        distanceTolerance: Float = DEFAULT_DISTANCE_TOLERANCE,
        angleTolerance: Double = DEFAULT_ANGLE_TOLERANCE
    ): Polyline {
        val points = mutableListOf<Vector2>()
        points.add(start)
        buildPoints(points, distanceTolerance, angleTolerance)
        return openOrClosed(Polyline(points))
    }

    override fun toPolylines(distanceTolerance: Float, angleTolerance: Double): List<Polyline> {
        return listOf(toPolyline(distanceTolerance, angleTolerance))
    }

    /**
     * When making a copy, call this to ensure the copy is also closed when this is closed.
     * See [isClosed], [joinEnd].
     */
    fun <T2 : ContiguousShape<*>> openOrClosed(copy: T2): T2 {
        if (isClosed()) {
            copy.joinEnd(copy.start)
        }
        return copy
    }

    companion object {
        /**
         * Used as the default value for [buildPoints] and [toPolyline].
         *
         * An arbitrary constant found by experimenting visually.
         *
         * NOTE, this could be reduced if [EllipticalArc.buildPoints] were rewritten.
         * (as the binary chop approach doesn't work very well).
         */
        const val DEFAULT_DISTANCE_TOLERANCE = 1.6f

        /**
         * Use as the default value for [buildPoints] and [toPolyline].
         *
         * An arbitrary constant found by experimenting visually.
         */
        const val DEFAULT_ANGLE_TOLERANCE = 0.33
    }
}
