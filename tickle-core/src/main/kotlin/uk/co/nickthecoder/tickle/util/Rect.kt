/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import java.util.*

/**
 * A rectangle using Float
 */
data class Rect(
    var left: Float,
    var bottom: Float,
    var right: Float,
    var top: Float
) {

    val width
        get() = right - left
    val height
        get() = top - bottom

    constructor() : this(0f, 0f, 0f, 0f)

    constructor(other: Rect) : this(other.left, other.bottom, other.right, other.top)

    fun set(l: Float, b: Float, r: Float, t: Float) {
        left = l
        bottom = b
        right = r
        top = t
    }

    fun set(other: Rect) {
        left = other.left
        bottom = other.bottom
        right = other.right
        top = other.top
    }

    fun plus(dx: Float, dy: Float, dest: Rect = this): Rect {
        dest.left = left + dx
        dest.right = right + dx
        dest.top = top + dy
        dest.bottom = bottom + dy
        return dest
    }

    operator fun plusAssign( by : Vector2 ) {
        left += by.x
        bottom += by.y
        right += by.x
        top += by.y
    }

    fun contains(point: Vector2): Boolean = left <= point.x && right >= point.x && bottom <= point.y && top >= point.y

    fun contains(other: Rect): Boolean = left <= other.left && right >= other.right &&
            bottom <= other.bottom && top >= other.top

    fun overlaps(other: Rect): Boolean = left < other.right && right > other.left &&
            bottom < other.top && top > other.bottom

    override fun equals(other: Any?): Boolean {
        if (other !is Rect) {
            return false
        }
        return other.left == left && other.bottom == bottom && other.right == right && other.top == top
    }

    override fun hashCode() = Objects.hash(left, bottom, top, right)

    override fun toString(): String = "($left , $bottom , $right , $top)"
}
