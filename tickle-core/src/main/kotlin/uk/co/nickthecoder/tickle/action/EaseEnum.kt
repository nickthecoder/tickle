/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action

/**
 * This contains the same [Ease] constants as the object [Eases], but provides them in an enum,
 * so that they can be used within as an @Attribute (and therefore can be picked from
 * Tickle's resources editor / scene editor).
 */
enum class EaseEnum : Ease {

    linear {
        override fun ease(t: Float) = LinearEase.instance.ease(t)
    },


    easeIn {
        override fun ease(t: Float) = Eases.easeIn.ease(t)
    },
    easeInQuad {
        override fun ease(t: Float) = Eases.easeInQuad.ease(t)
    },
    easeInCubic {
        override fun ease(t: Float) = Eases.easeInCubic.ease(t)
    },
    easeInExpo {
        override fun ease(t: Float) = Eases.easeInExpo.ease(t)
    },
    easeInCirc {
        override fun ease(t: Float) = Eases.easeInCirc.ease(t)
    },
    easeInBack {
        override fun ease(t: Float) = Eases.easeInBack.ease(t)
    },


    easeOut {
        override fun ease(t: Float) = Eases.easeOut.ease(t)
    },
    easeOutQuad {
        override fun ease(t: Float) = Eases.easeOutQuad.ease(t)
    },
    easeOutCubic {
        override fun ease(t: Float) = Eases.easeOutCubic.ease(t)
    },
    easeOutExpo {
        override fun ease(t: Float) = Eases.easeOutExpo.ease(t)
    },
    easeOutCirc {
        override fun ease(t: Float) = Eases.easeOutCirc.ease(t)
    },
    easeOutBack {
        override fun ease(t: Float) = Eases.easeOutBack.ease(t)
    },


    easeInOut {
        override fun ease(t: Float) = Eases.easeInOut.ease(t)
    },
    easeInOutQuad {
        override fun ease(t: Float) = Eases.easeInOutQuad.ease(t)
    },
    easeInOutCubic {
        override fun ease(t: Float) = Eases.easeInOutCubic.ease(t)
    },
    easeInOutExpo {
        override fun ease(t: Float) = Eases.easeInOutExpo.ease(t)
    },
    easeInOutCirc {
        override fun ease(t: Float) = Eases.easeInOutCirc.ease(t)
    },
    easeInOutBack {
        override fun ease(t: Float) = Eases.easeInOutBack.ease(t)
    },


    bounce {
        override fun ease(t: Float) = Eases.bounce.ease(t)
    },
    bounce2 {
        override fun ease(t: Float) = Eases.bounce2.ease(t)
    },
    bounce3 {
        override fun ease(t: Float) = Eases.bounce3.ease(t)
    }

}
