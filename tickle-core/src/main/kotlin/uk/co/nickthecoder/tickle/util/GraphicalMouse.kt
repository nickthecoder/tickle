package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.ActionRole
import uk.co.nickthecoder.tickle.action.movement.FollowMouse
import uk.co.nickthecoder.tickle.action.movement.HideWhenMouseIsOutside

class GraphicalMouse : ActionRole() {
    override fun createAction() = HideWhenMouseIsOutside(actor).and(FollowMouse(actor))
}
