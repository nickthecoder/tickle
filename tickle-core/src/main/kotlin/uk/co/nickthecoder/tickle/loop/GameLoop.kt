/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.loop

import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.physics.TickleWorld
import uk.co.nickthecoder.tickle.stage.Stage
import uk.co.nickthecoder.tickle.stage.View

/**
 * Responsible for coordinating the "tick" calls to the [Producer], the [Director], all [View]s, all [Stage]s
 * all [TickleWorld]s (if physics is enabled) and the [Action]s held by [Scene.actions].
 * All Role's tick methods are called by Stage's tick method, not from GameLoop directly.
 *
 * The order of "tick" calls in [AbstractGameLoop] is as follows :
 *
 * * [Producer.preTick]
 * * [Director.preTick]
 * * [Producer.tick]
 * * [Director.tick]
 * * When the game is not paused (See [Game.paused]):
 *      * [Scene.actions].tick
 *      * [Stage]s.tick (which calls each Role's tick)
 *      * [View]s.tick
 *      * If the physics engine is enabled, then [TickleWorld]s.tick
 * * [Director.postTick]
 * * [Producer.postTick]
 *
 * The default implementation is [DefaultGameLoop]. However, you may create your own implementation
 * if your game has special needs, in which case, consider extending [AbstractGameLoop].
 *
 * For example, you could ensure that ticks have a constant time step (so that your game is predictably
 * even running, regardless of load) by skipping frame drawing when the game is running too slowly.
 */
interface GameLoop {

    val tickCount: Long
    val droppedFrames : Int

    fun sceneStarted()
    fun loopBody()
    fun resetStats()
    fun expectedFrameRate() : Double
    fun actualFPS(): Double
}
