package uk.co.nickthecoder.tickle.misc

import java.awt.Toolkit
import java.awt.datatransfer.*


interface TickleClipboard {
    fun set(text: String)
    fun get(): String

    companion object {
        var instance = AWTClipboard()
    }
}

class AWTClipboard : TickleClipboard, ClipboardOwner {
    override fun get(): String {
        val clipboard = Toolkit.getDefaultToolkit().systemClipboard ?: return ""
        val contents = clipboard.getContents(null) ?: return ""

        if (contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
            try {
                return contents.getTransferData(DataFlavor.stringFlavor) as? String ?: ""
            } catch (e: Exception) {
            }
        }
        return ""
    }

    override fun set(text: String) {
        Toolkit.getDefaultToolkit().systemClipboard?.setContents(StringSelection(text), this)
    }

    override fun lostOwnership(clipboard: Clipboard?, contents: Transferable?) {
    }

}
