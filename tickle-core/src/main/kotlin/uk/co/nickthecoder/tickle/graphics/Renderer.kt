/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.graphics

import org.joml.Matrix3x2f
import org.lwjgl.opengl.EXTFramebufferObject.*
import org.lwjgl.opengl.GL11.*
import org.lwjgl.system.MemoryUtil
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.graphics.programs.*
import uk.co.nickthecoder.tickle.path.VisibleShape
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.Rect
import uk.co.nickthecoder.tickle.util.Vector2
import java.nio.FloatBuffer


/**
 * A reworking of my original renderer.
 *
 * We can have different OpenGL programs for different scenarios.
 * For example, an unscaled, un-rotated [Actor] does not need its own model-matrix.
 * As there are likely to be *lots* of these, then performance should (in theory)
 * be much better, especially on crusty old hardware, which uses software, rather than
 * hardware for rendering.
 */
class Renderer private constructor() {

    var enableAntiAlias = true

    /**
     * A matrix which converts Tickle's world coordinates to OpenGL's output coordinates
     * (where 0,0 is the center of the output, and the height and width of the output is 2).
     */
    private val viewMatrix = Matrix3x2f()

    /**
     * [viewMatrix] is pre-multiplied by this when we want to flip the output in the Y direction.
     * See [outputTexture].
     */
    private val flipYMatrix = Matrix3x2f().scale(1f, -1f)

    private val textureProgram = TextureProgram()
    private val transformedTextureProgram = TransformedTextureProgram()
    private val simpleTriangleProgram = SimpleTriangleProgram()

    private val flatStrokeProgram = FlatStrokeProgram()
    private val flatStrokeAntiAliasProgram = FlatStrokeAntiAliasProgram()
    private val strokeProgram = StrokeProgram(mirrored = false)
    private val strokeMirroredProgram = StrokeProgram(mirrored = true)
    private val strokeAntiAliasProgram = StrokeAntiAliasProgram(mirrored = false)
    private val strokeAntiAliasMirroredProgram = StrokeAntiAliasProgram(mirrored = true)

    private val subs = listOf(
        textureProgram,
        transformedTextureProgram,
        simpleTriangleProgram,
        flatStrokeProgram,
        flatStrokeAntiAliasProgram,
        strokeProgram,
        strokeMirroredProgram
    )

    var flipY = false

    private val globalOpacityColor = Colors.WHITE

    /**
     * Normally this is `1f`. Lowering it will cause all texture based drawing methods to render
     * partially transparent.
     * It is not used during game play.
     * It is used in the editor to make locked actors, and actors in locked stages appear semi-transparent.
     */
    var globalOpacity = 1f
        set(v) {
            field = v
            globalOpacityColor.alpha = v
        }

    private var outputFBO = 0

    /**
     * Render output to a [Texture], or set to null to output to the display device (i.e. the [Window]).
     * You must NOT change this while rendering a scene. i.e. not between calls to [beginView] and [endView].
     */
    var outputTexture: Texture? = null
        set(v) {
            if (field !== v) {

                reset()

                if (v == null) {
                    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

                    // Delete now unused FBO
                    if (outputFBO != 0) {
                        glDeleteFramebuffersEXT(outputFBO)
                        outputFBO = 0
                    }
                    flipY = false
                } else {
                    outputFBO = glGenFramebuffersEXT()

                    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, outputFBO)
                    glFramebufferTexture2DEXT(
                        GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
                        v.handle, 0
                    )
                    // The display buffer is the "wrong way up" compared to our Textures.
                    // So rendering upside down gives as a texture as we need it.
                    flipY = true
                }
                field = v
            }
        }

    /**
     * The height of the [outputTexture], or the [Window] when [outputTexture] == null
     */
    val outputHeight: Int
        get() = outputTexture?.height ?: Game.instance.window.height

    /**
     * The width of the [outputTexture], or the [Window] when [outputTexture] == null
     */
    val outputWidth: Int
        get() = outputTexture?.width ?: Game.instance.window.width

    private fun reset() {
        currentSubRenderer?.unuse()
        currentSubRenderer = null
        currentTexture = null
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    }

    fun clear(color: Color) {
        glClearColor(color.red, color.green, color.blue, color.alpha)
        glClear(GL_COLOR_BUFFER_BIT)
    }

    fun clip(left: Int, bottom: Int, width: Int, height: Int) {
        if (flipY) {
            glViewport(left, outputHeight - bottom - height, width, height)
        } else {
            glViewport(left, bottom, width, height)
        }
    }

    fun beginView(viewMatrix: Matrix3x2f) {
        reset()

        currentTexture = null
        // Ensure that the RendererProgram.use() is called again, when the first pose/texture is drawn.
        // Without this, side-effects can occur. e.g. PixelTouching was binding to a new texture,
        // and SimpleRendererProgram didn't know it had to rebind its current texture.
        if (flipY) {
            flipYMatrix.mul(viewMatrix, this.viewMatrix)
        } else {
            this.viewMatrix.set(viewMatrix)
        }
    }

    fun beginView(left: Double, bottom: Double, right: Double, top: Double) {
        beginView(left.toFloat(), bottom.toFloat(), right.toFloat(), top.toFloat())
    }

    private val tmpMatrix = Matrix3x2f()
    fun beginView(left: Float, bottom: Float, right: Float, top: Float) {
        tmpMatrix.set(
            2f / (right - left), 0f,
            0f, 2f / (top - bottom),
            -(right + left) / (right - left), -(top + bottom) / (top - bottom)
        )
        beginView(tmpMatrix)
    }

    fun endView() {
        reset()
    }

    fun drawPose(pose: Pose, x: Float, y: Float) {
        if (globalOpacity == 1f) {
            with(textureProgram) {
                if (currentSubRenderer === textureProgram && currentTexture === pose.texture) {
                    drawPose(pose, x, y)
                } else {
                    currentSubRenderer?.flush()
                    currentSubRenderer = textureProgram
                    currentTexture = pose.texture
                    use(viewMatrix)
                    setup(pose.texture)
                    drawPose(pose, x, y)
                }
            }
        } else {
            with(transformedTextureProgram) {
                use(viewMatrix)
                setup(pose.texture, globalOpacityColor)
                drawPose(pose, x, y)
                unuse()
            }
        }
    }

    private fun tintColor(tint: Color?): Color? {
        return if (globalOpacity == 1f) {
            tint
        } else {
            tint?.mulOpacity(globalOpacity) ?: globalOpacityColor
        }
    }

    fun drawPose(pose: Pose, x: Float, y: Float, tint: Color? = null, scale: IVector2) {
        if (tint == null && globalOpacity == 1f) {
            with(textureProgram) {
                use(viewMatrix)
                setup(pose.texture)
                drawPose(pose, x, y, scale)
                unuse()
            }
        } else {
            with(transformedTextureProgram) {
                use(viewMatrix)
                setup(pose.texture, tintColor(tint))
                drawPose(pose, x, y, scale)
                unuse()
            }
        }
    }

    fun drawPose(pose: Pose, x: Float, y: Float, tint: Color? = null, modelMatrix: Matrix3x2f? = null) {
        if (tint == null && modelMatrix == null && globalOpacity == 1f) {
            with(textureProgram) {
                use(viewMatrix)
                setup(pose.texture)
                drawPose(pose, x, y)
                unuse()
            }
        } else {
            with(transformedTextureProgram) {
                use(viewMatrix)
                setup(pose.texture, tintColor(tint), modelMatrix)
                drawPose(pose, x, y)
                unuse()
            }
        }
    }

    fun drawTexture(texture: Texture, world: Rect, tRect: Rect, tint: Color? = null, modelMatrix: Matrix3x2f? = null) {
        drawTexture(
            texture,
            world.left, world.bottom, world.right, world.top,
            tRect.left, tRect.bottom, tRect.right, tRect.top,
            tint,
            modelMatrix
        )
    }

    fun drawTexture(
        texture: Texture,
        left: Float, bottom: Float, right: Float, top: Float,
        tRect: Rect,
        tint: Color? = null,
        modelMatrix: Matrix3x2f? = null
    ) {
        drawTexture(
            texture,
            left, bottom, right, top,
            tRect.left, tRect.bottom, tRect.right, tRect.top,
            tint,
            modelMatrix
        )
    }

    fun drawTexture(
        texture: Texture,
        left: Float, bottom: Float, right: Float, top: Float,
        tLeft: Float, tBottom: Float, tRight: Float, tTop: Float,
        tint: Color? = null,
        modelMatrix: Matrix3x2f? = null
    ) {
        if (tint == null && modelMatrix == null && globalOpacity == 1f) {
            with(textureProgram) {
                if (currentSubRenderer === textureProgram && currentTexture === texture) {
                    draw(left, bottom, right, top, tLeft, tBottom, tRight, tTop)
                } else {
                    currentSubRenderer?.flush()
                    currentSubRenderer = textureProgram
                    currentTexture = texture
                    use(viewMatrix)
                    setup(texture)
                    draw(left, bottom, right, top, tLeft, tBottom, tRight, tTop)
                }
            }
        } else {
            with(transformedTextureProgram) {
                use(viewMatrix)
                setup(texture, tintColor(tint), modelMatrix)
                draw(
                    left, bottom, right, top,
                    tLeft, tBottom, tRight, tTop,
                )
                unuse()
            }
        }
    }

    fun fillTriangles(vertices: List<IVector2>, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(simpleTriangleProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            draw(vertices)
            unuse()
        }
    }

    /**
     * Draws a solid rectangle.
     */
    fun fillRect(rect: Rect, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(simpleTriangleProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            fillRect(rect)
            unuse()
        }
    }

    /**
     * Draws a rectangular outline. The [thickness] is *outside* the rect.
     * Assuming that [rect] left <= right and bottom <= top.
     * If they are the other way round, then the thickness will be *inside* the rect.
     */
    fun strokeRect(rect: Rect, thickness: Float, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(simpleTriangleProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            strokeRect(rect, thickness)
            unuse()
        }
    }

    fun stroke(shape: VisibleShape, modelMatrix: Matrix3x2f? = null) {

        when (val strokePaint = shape.strokePaint) {
            is FlatColor -> {
                val sub = flatStrokeProgram
                with(sub) {
                    setup(strokePaint.color, modelMatrix)
                    for (mesh in shape.strokeMeshes) {
                        draw(mesh.points)
                    }
                }
            }

            is GradientAlong -> {
                val sub = if (strokePaint.isMirrored) strokeMirroredProgram else strokeProgram
                with(sub) {
                    use(viewMatrix)
                    setup(strokePaint.gradient.texture, strokePaint.repeat, modelMatrix)
                    for (mesh in shape.strokeMeshes) {
                        draw(mesh.points, mesh.uvs, true)
                    }
                    unuse()
                }
            }

            is GradientAcross -> {
                val sub = if (strokePaint.isMirrored) strokeMirroredProgram else strokeProgram
                with(sub) {
                    use(viewMatrix)
                    setup(strokePaint.gradient.texture, strokePaint.repeat, modelMatrix)
                    for (mesh in shape.strokeMeshes) {
                        draw(mesh.points, mesh.uvs, false)
                    }
                    unuse()
                }
            }
        }
    }

    /**
    @param modelViewScale Defines how wide the anti-alias edge will be.
    A value of 1 is the "correct" value if the model and view do not scale
    (i.e. if a shape is 10 wide, then it will appear 10 pixel wide on screen).
     */
    fun strokeAntiAlias(shape: VisibleShape, modelViewScale: Float, modelMatrix: Matrix3x2f? = null) {
        if (!enableAntiAlias) {
            stroke(shape, modelMatrix)
        } else {
            val edge = 1f / shape.thickness / modelViewScale

            when (val strokePaint = shape.strokePaint) {

                is FlatColor -> {
                    with(flatStrokeAntiAliasProgram) {
                        use(viewMatrix)
                        for (mesh in shape.strokeMeshes) {
                            setup(strokePaint.color, edge, modelMatrix)
                            draw(mesh.points, mesh.uvs)
                        }
                        unuse()
                    }
                }

                is GradientAlong -> {
                    val sub =
                        if (strokePaint.isMirrored) strokeAntiAliasMirroredProgram else strokeAntiAliasProgram
                    with(sub) {
                        use(viewMatrix)
                        for (mesh in shape.strokeMeshes) {
                            setup(strokePaint.texture, edge, strokePaint.repeat, modelMatrix)
                            draw(mesh.points, mesh.uvs, true)
                        }
                        unuse()
                    }
                }

                is GradientAcross -> {
                    val sub = if (strokePaint.isMirrored) strokeAntiAliasMirroredProgram else strokeAntiAliasProgram
                    with(sub) {
                        use(viewMatrix)
                        for (mesh in shape.strokeMeshes) {
                            setup(strokePaint.gradient.texture, edge, strokePaint.repeat, modelMatrix)
                            draw(mesh.points, mesh.uvs, false)
                        }
                        unuse()
                    }
                }
            }
        }
    }


    fun line(from: Vector2, to: Vector2, thickness: Float, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(simpleTriangleProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            drawLine(from, to, thickness)
            unuse()
        }
    }

    private fun delete() {
        for (sub in subs) {
            sub.delete()
        }
        floatBuffer.clear()
    }

    companion object {
        // For optimising drawPose
        internal var currentSubRenderer: ShaderProgram? = null
        internal var currentTexture: Texture? = null

        // We use one float buffer for all rendering.
        internal var floatBuffer: FloatBuffer = MemoryUtil.memAllocFloat(40960)
        lateinit var vertexBuffer: VertexBuffer

        private var instance: Renderer? = null

        private val IDENTITY_MATRIX_3x2 = Matrix3x2f()

        fun instance(): Renderer {
            instance?.let { return it }
            vertexBuffer = VertexBuffer()
            vertexBuffer.bind()
            /* Upload null data to allocate storage for the VBO */
            val size = (floatBuffer.capacity() * java.lang.Float.BYTES).toLong()
            vertexBuffer.uploadData(size, Usage.DYNAMIC_DRAW)

            instance = Renderer()
            return instance!!
        }

        fun cleanUp() {
            ignoreErrors {
                instance?.let {
                    it.delete()
                    instance = null
                }
            }
            ignoreErrors {
                if (this::vertexBuffer.isInitialized) {
                    vertexBuffer.delete()
                }
            }
        }
    }

}
