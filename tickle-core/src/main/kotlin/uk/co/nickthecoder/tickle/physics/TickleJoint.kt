/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.physics

import org.jbox2d.common.Vec2
import org.jbox2d.dynamics.joints.Joint
import org.jbox2d.dynamics.joints.JointDef
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.ImmutableVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * JBox2D uses [Joint] and [JointDef], and it seems that you cannot change some aspects of a [Joint],
 * without deleting the joint, and adding a new one.
 *
 * As Tickle is trying to be easy to use (rather than maximally efficient), a TickleJoint CAN be
 * dynamically altered, without extra hassle (it deletes the [Joint], and creates a new one for you).
 * Unless you change joints extremely often, this won't noticeably affect performance.
 */
abstract class TickleJoint<J : Joint, D : JointDef>(
    val actorA: Actor,
    val actorB: Actor,
    localAnchorA: IVector2,
    localAnchorB: IVector2
) {

    var localAnchorA: ImmutableVector2 = ImmutableVector2(localAnchorA)
        set(v) {
            field = v
            replace()
        }

    var localAnchorB: ImmutableVector2 = ImmutableVector2(localAnchorB)
        set(v) {
            field = v
            replace()
        }

    protected var jBox2dJoint: J? = null

    val tickleWorld: TickleWorld = actorA.body?.tickleWorld
        ?: throw IllegalArgumentException("actorA does not have a body. Check the 'Physics' tab of its costume.")


    init {
        actorB.body
            ?: throw IllegalArgumentException("actorB does not have a body. Check the 'Physics' tab of its costume.")
        if (actorA.body?.tickleWorld != actorB.body?.tickleWorld) throw IllegalArgumentException("actorA's world is not the same as actorB's")
    }

    /**
     * This is a more efficient version of [getAnchorA].
     * The result is set in [dest] instead of creating a new [Vector2].
     */
    fun getAnchorA(dest: Vector2) {
        jBox2dJoint?.getAnchorA(tmpVec2)
        tickleWorld.physicsToTickle(dest, tmpVec2)
    }

    /**
     * This is a more efficient version of [getAnchorB]
     * The result is set in [dest] instead of creating a new [Vector2].
     */
    fun getAnchorB(dest: Vector2) {
        jBox2dJoint?.getAnchorB(tmpVec2)
        tickleWorld.physicsToTickle(dest, tmpVec2)
    }

    /**
     * Gets the position of the joint's anchor position.
     */
    fun getAnchorA(): Vector2 {
        val out = Vector2()
        getAnchorA(out)
        return out
    }

    /**
     * Gets the position of the joint's anchor position.
     */
    fun getAnchorB(): Vector2 {
        val out = Vector2()
        getAnchorB(out)
        return out
    }

    protected abstract fun createDef(): D


    protected fun create() {
        @Suppress("UNCHECKED_CAST")
        jBox2dJoint = tickleWorld.jBox2dWorld.createJoint(createDef()) as J
        jBox2dJoint?.userData = this
        actorA.body?.joints?.add(this)
        actorB.body?.joints?.add(this)
    }

    fun destroy() {
        actorA.body?.joints?.remove(this)
        actorB.body?.joints?.remove(this)
        jBox2dJoint?.let {
            if (tickleWorld.jBox2dWorld.isLocked) {
                throw RuntimeException("JBox2d is locked")
            }
            tickleWorld.jBox2dWorld.destroyJoint(it)
            jBox2dJoint = null
        }
    }

    protected open fun replace() {
        destroy()
        create()
    }

    companion object {
        private val tmpVec2 = Vec2()
    }
}
