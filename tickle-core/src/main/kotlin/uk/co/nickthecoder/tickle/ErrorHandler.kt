/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

/**
 * If a game throws an exception while it is running, we can catch it,
 * and do something, possibly useful.
 *
 * For handling of errors from other sources (such as loading resources, the editor etc.),
 * then use [TickleErrorHandler] instead.
 */
interface GameErrorHandler {

    fun error(owner: Any?, e: Exception)

    companion object {
        @JvmStatic
        var instance: GameErrorHandler = SimpleGameErrorHandler()
    }
}

open class SimpleGameErrorHandler : GameErrorHandler {

    override fun error(owner: Any?, e: Exception) {

        e.printStackTrace()
        try {
            if (owner != null) {
                System.err.println("Exception thrown by : ${owner}.")
            }
            if (owner is Role) {
                owner.actor.stage?.remove(owner.actor)
            }
        } catch (e2: Exception) {
            // Just in case. "remove" could fail, and so could println( $owner )
            System.err.println("Error handler failed : $e2")
        }
    }


}

class SilentGameErrorHandler : GameErrorHandler {
    override fun error(owner: Any?, e: Exception) {
    }
}

interface TickleErrorHandler {

    fun error(e: Exception)

    fun error(message: String)

    fun info(message: String)

    fun warn(message: String)

    /**
     * Deprecated code can call this to inform Game developers of their use of deprecated methods.
     * The default behaviour ignores the message.
     * When running Games from the Editor, the message is NOT ignored (MainWindow is the TickleErrorHandler).
     */
    fun deprecated(message: String, replacement: String) {}

    companion object {
        @JvmStatic
        var instance: TickleErrorHandler = SimpleTickleErrorHandler()
    }
}


// NOTE, This is called severe, rather than error, to avoid a name clash with Kotlin's error method.
// I've had issues copy/pasting code with intelliJ which include functions with name classes.
// It tends to add the namespace of Kotlin's function. Grr.
fun severe(message: String) {
    TickleErrorHandler.instance.error(message)
}

fun severe(e: Exception) {
    TickleErrorHandler.instance.error(e)
}

fun warn(message: String) {
    TickleErrorHandler.instance.warn(message)
}

fun deprecated(message: String, replacement: String) {
    TickleErrorHandler.instance.deprecated(message, replacement)
}

fun info(message: String) {
    TickleErrorHandler.instance.info(message)
}

class SilentTickleErrorHandler : TickleErrorHandler {

    override fun info(message: String) {
    }

    override fun warn(message: String) {
    }

    override fun error(message: String) {
    }

    override fun error(e: Exception) {
    }
}

open class SimpleTickleErrorHandler : TickleErrorHandler {

    override fun info(message: String) {
        System.err.println("INFO: $message")
    }

    override fun warn(message: String) {
        System.err.println("WARNING: $message")
    }

    override fun error(message: String) {
        System.err.println("ERROR : $message")
    }

    override fun error(e: Exception) {
        e.printStackTrace()
    }

}

class DelayedErrorHandler(val immediate: TickleErrorHandler) : TickleErrorHandler {

    val exceptions = mutableListOf<Exception>()
    val messages = mutableListOf<String>()


    override fun info(message: String) {
        immediate.info(message)
        messages.add("INFO : $message")
    }

    override fun warn(message: String) {
        immediate.warn(message)
        messages.add("WARNING : $message")
    }

    override fun error(message: String) {
        immediate.warn(message)
        messages.add("ERROR : $message")
    }

    override fun error(e: Exception) {
        immediate.error(e)
        messages.add("ERROR : $e")
    }

}
