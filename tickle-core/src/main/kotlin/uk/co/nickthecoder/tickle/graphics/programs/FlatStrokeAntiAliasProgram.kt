package uk.co.nickthecoder.tickle.graphics.programs

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.util.IVector2

/**
 * Similar to [StrokeAntiAliasProgram], but with a flat [Color], instead of a gradient.
 */
class FlatStrokeAntiAliasProgram : ShaderProgram(
    VERTEX, REGULAR,
    3
) {

    val colorLocation = getUniformLocation("color")
    val edgeLocation = getUniformLocation("edge")

    fun setup(color: Color, edge: Float, modelMatrix: Matrix3x2f? = null) {
        setUniform(edgeLocation, edge * 2)
        setUniform(colorLocation, color)
        enablePositionU()
        setModelMatrix(modelMatrix)
    }

    fun draw(points: List<IVector2>, uvs: List<IVector2>) {
        if (points.size != uvs.size) {
            throw IllegalArgumentException("vertices size ${points.size} != uvs size ${uvs.size}")
        }
        requiredVertices(points.size)
        for ((i, point) in points.withIndex()) {
            val uv = uvs[i]
            Renderer.floatBuffer.put(point.x).put(point.y)
                .put(uv.y) // Using UV.y for antialiasing
        }
    }

    companion object {

        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute float v;
            
            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;
            
            varying float t;
            varying float s;
            
            void main() {
                s = v;
                gl_Position = vec4( viewMatrix * modelMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val REGULAR = """
            #version 120

            uniform vec4 color;
            
            uniform float edge; // 0..1 Thickness of the fuzzy edge as a ratio of the line thickness.
            
            varying float s; // 0 = Left 1 = Right (for antialiasing)
                                 
            void main() {
                
                // Fuzz = 1 when s in the range (edge)..(1-edge)
                // But approaches 0 when s approaches 0 or 1
                float fuzz = clamp( 0, 1, ( (1 - abs(s*2 - 1)) / edge) );
                
                gl_FragColor = vec4( color.xyz, color.w * fuzz );
            }
            """.trimIndent()

    }

}
