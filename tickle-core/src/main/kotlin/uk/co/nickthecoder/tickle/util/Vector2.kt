package uk.co.nickthecoder.tickle.util

import java.lang.Float.parseFloat
import java.util.*

/**
 * The base interface for [ImmutableVector2] and [Vector2]
 * (which is mutable, i.e. the x and y can be changed).
 *
 * Tickle uses [Vector2], when the x & y values do change,
 * [ImmutableVector2] when they must NOT change, and
 * [IVector2] when we don't care.
 *
 * Typically, game scripts always use [Vector2], but you can use the others
 * if you want to be fancy! (I only use [Vector2] in my games' scripts)
 */
interface IVector2 {

    val x: Float
    val y: Float

    fun immutable() : ImmutableVector2
    fun mutable() : Vector2

    fun plus(x: Float, y: Float) = Vector2(this.x + x, this.y + y)
    fun minus(x: Float, y: Float) = Vector2(this.x - x, this.y - y)
    fun times(x: Float, y: Float) = Vector2(this.x * x, this.y * y)
    fun div(x: Float, y: Float) = Vector2(this.x / x, this.y / y)

    operator fun times(scale: Float) = Vector2(x * scale, y * scale)
    operator fun div(scale: Float) = Vector2(x / scale, y / scale)
    operator fun rem(other: Float) = Vector2(x % other, y % other)

    operator fun plus(other: IVector2): Vector2 = Vector2(x + other.x, y + other.y)
    operator fun minus(other: IVector2) = Vector2(x - other.x, y - other.y)
    operator fun times(other: IVector2) = Vector2(x * other.x, y * other.y)
    operator fun div(other: IVector2) = Vector2(x / other.x, y / other.y)
    operator fun rem(other: IVector2) = Vector2(x % other.x, y % other.y)

    operator fun unaryMinus() = Vector2(-x, -y)

    fun reversed() = Vector2(-x, -y)
    fun perpendicular() = Vector2(-y, x)

    fun length(): Float {
        val dx = x.toDouble()
        val dy = y.toDouble()
        return Math.sqrt(dx * dx + dy * dy).toFloat()
    }

    fun lengthSquared() = x * x + y * y

    fun distanceSquared(other: IVector2): Float {
        val tx = x - other.x
        val ty = y - other.y
        return tx * tx + ty * ty
    }

    fun abs() = Vector2(Math.abs(x), Math.abs(y))

    fun dot(other: IVector2) = x * other.x + y * other.y

    fun cross(other: IVector2) = x * other.y - y * other.x

    /**
     * Returns the angle of the lines (0,0)->this and (0,0)->other
     */
    fun angle(other: IVector2) =
        Angle.radians(Math.acos((this.dot(other) / this.length() / other.length()).toDouble()))

    fun angleRadians(other: IVector2) = Math.acos((this.dot(other) / this.length() / other.length()).toDouble())

    /**`
     * The angle in RADIANS between the Y=0 line, and the line (0,0)->this
     */
    fun angle() = Angle.radians(Math.atan2(y.toDouble(), x.toDouble()))
    fun angleRadians() = Math.atan2(y.toDouble(), x.toDouble())

    /**
     * Rotates the vector about the origin.
     */
    fun rotateRadians(radians: Double): Vector2 {
        val sin = Math.sin(radians)
        val cos = Math.cos(radians)
        return Vector2((cos * x + sin * y).toFloat(), (-sin * x + cos * y).toFloat())
    }

    fun rotateDegrees(degrees: Double) = rotateRadians(Math.toRadians(degrees))
    fun rotate(angle: IAngle) = rotateRadians(angle.radians)

    /**
     * Is this point on the left of the infinite line passing through points [a] to [b]?
     *
     *      result > 0 for point left of the line through a to b
     *      result = 0 for point on the line
     *      result < 0 for point right of the line
     */
    fun leftOf(a: Vector2, b: Vector2): Float {
        return ((b.x - a.x) * (y - a.y)
                - (x - a.x) * (b.y - a.y))
    }


}

/**
 * And immutable x,y coordinate (i.e. you cannot change the x and y values).
 * I've never used this in my own games' scripts, I always use [Vector2]. YMMV.
 *
 */
class ImmutableVector2(override val x: Float, override val y: Float) : IVector2 {
    constructor(from: IVector2) : this(from.x, from.y)

    override fun immutable() = this
    override fun mutable() = Vector2(this)

    override fun hashCode() = Objects.hash(x, y)

    override fun equals(other: Any?): Boolean {
        if (other is IVector2) {
            return x == other.x && y == other.y
        }
        return false
    }

    override fun toString() = "($x , $y)"

    companion object {
        @JvmStatic
        val ZERO: IVector2 = ImmutableVector2(0f, 0f)
    }

}

/**
 * A mutable x,y coordinate (i.e. the x and y values can be changed).
 *
 * In most cases, I've chosen to use a mutable object, so that new objects aren't being created and then destroyed,
 * which could affect game speed.
 *
 * In general mutable objects are "bad", because side effects can have weird and tricky to debug problems.
 */
class Vector2(override var x: Float, override var y: Float) : IVector2 {

    constructor() : this(0f, 0f)

    constructor(other: IVector2) : this(other.x, other.y)

    override fun immutable() = ImmutableVector2(this)
    override fun mutable() = Vector2(this)

    fun set(x: Float, y: Float) {
        this.x = x
        this.y = y
    }

    fun set(other: IVector2) {
        x = other.x
        y = other.y
    }

    fun set(angle: IAngle) {
        x = Math.cos(angle.radians).toFloat()
        y = Math.sin(angle.radians).toFloat()
    }

    fun setPlus(a: IVector2, b: IVector2) {
        x = a.x + b.x
        y = a.y + b.y
    }

    fun setMinus(a: IVector2, b: IVector2) {
        x = a.x - b.x
        y = a.y - b.y
    }

    fun setUnaryMinus(a: IVector2) {
        x = -a.x
        y = -a.y
    }

    fun setTimes(a: IVector2, b: IVector2) {
        x = a.x * b.x
        y = a.y * b.y
    }

    fun setTimes(scale: Float) {
        x *= scale
        y *= scale
    }

    fun setDiv(a: IVector2, b: IVector2) {
        x = a.x / b.x
        y = a.y / b.y
    }


    fun setRem(a: IVector2, b: IVector2) {
        x = a.x % b.x
        y = a.y % b.y
    }

    operator fun plusAssign(other: IVector2) {
        x += other.x
        y += other.y
    }

    fun plusAssign(dx: Float, dy: Float) {
        x += dx
        y += dy
    }

    operator fun minusAssign(other: IVector2) {
        x -= other.x
        y -= other.y
    }

    fun minusAssign(dx: Float, dy: Float) {
        x -= dx
        y -= dy
    }

    operator fun timesAssign(other: IVector2) {
        x *= other.x
        y *= other.y
    }

    operator fun timesAssign(scalar: Float) {
        x *= scalar
        y *= scalar
    }

    fun timesAssign(dx: Float, dy: Float) {
        x *= dx
        y *= dy
    }

    operator fun divAssign(other: IVector2) {
        x /= other.x
        y /= other.y
    }

    operator fun divAssign(scalar: Float) {
        x /= scalar
        y /= scalar
    }

    fun divAssign(dx: Float, dy: Float) {
        x /= dx
        y /= dy
    }

    operator fun remAssign(other: IVector2) {
        x %= other.x
        y %= other.y
    }

    fun setReversed() {
        x = -x
        y = -y
    }

    fun setPerpendicular() {
        val tx = x
        x = y
        y = -tx
    }

    /**
     * Create a Vector of length 1 pointing in the same direction as this.
     * If [x] and [y] are both zero, then Vector2(1,0) is returned.
     */
    fun unit(): Vector2 {
        return if (x == 0f && y == 0f) {
            Vector2(1f, 0f)
        } else {
            this / length()
        }
    }

    /**
     * Similar to [unit], but if the [length] is zero, then Vector(0,0) is returned.
     */
    fun unit0(): Vector2 {
        return if (x == 0f && y == 0f) {
            Vector2(0f, 0f)
        } else {
            this / length()
        }
    }

    /**
     * Create a Vector of length [magnitude] pointing in the same direction as this.
     * If [x] and [y] are both zero, then Vector2(magnitude,0) is returned.
     */
    fun magnitude( magnitude : Float ) : Vector2 {
        return if (x == 0f && y == 0f) {
            Vector2(magnitude, 0f)
        } else {
            this * ( magnitude / length() )
        }
    }

    /**
     * Similar to [magnitude], but if the [length] is zero, then Vector(0,0) is returned.
     */
    fun magnitude0( magnitude : Float ) : Vector2 {
        return if (x == 0f && y == 0f) {
            Vector2(0f, 0f)
        } else {
            this * ( magnitude / length() )
        }
    }

    /**
     * Scales this Vector to length 1.
     * If [x] and [y] are both zero, then x is set to 1 and y is set to 0.
     */
    fun setUnit() {
        if (x == 0f && y == 0f) {
            x = 1f
            y = 0f
        } else {
            val l = length()
            x /= l
            y /= l
        }
    }

    fun setMagnitude(magnitude: Float) {
        setUnit()
        setTimes(magnitude)
    }

    fun distance(other: IVector2) = (this - other).length()

    fun towards(other: IVector2, distance: Float) =
        other * Math.min(1.0f, distance / distance(other)) + this * (1.0f - Math.min(1.0f, distance / distance(other)))


    fun setRotate(angle: IAngle) {
        setRotateRadians(angle.radians)
    }

    fun setRotateRadians(radians: Double) {
        val sin = Math.sin(radians)
        val cos = Math.cos(radians)
        val tempX = (cos * x + sin * y).toFloat()
        y = (-sin * x + cos * y ).toFloat()
        x = tempX
    }

    override fun hashCode() = Objects.hash(x, y)

    override fun equals(other: Any?): Boolean {
        if (other is IVector2) {
            return x == other.x && y == other.y
        }
        return false
    }

    override fun toString() = "($x , $y)"

    companion object {

        fun parse(str: String): Vector2 {
            var stripped = str.trim()
            if (stripped.startsWith("(") || stripped.startsWith("[")) {
                stripped = stripped.substring(1)
            }
            if (stripped.endsWith(")") || stripped.endsWith("]")) {
                stripped = stripped.substring(stripped.length - 1)
            }
            val items = stripped.split(",")
            return Vector2(parseFloat(items[0].trim()), parseFloat(items[1].trim()))
        }
    }

}
