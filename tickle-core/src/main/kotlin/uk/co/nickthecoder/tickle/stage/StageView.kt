/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.stage

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.ActorDetails
import uk.co.nickthecoder.tickle.Appearance
import uk.co.nickthecoder.tickle.Role
import uk.co.nickthecoder.tickle.collision.Overlapping
import uk.co.nickthecoder.tickle.collision.PixelOrBoundingBoxTouching
import uk.co.nickthecoder.tickle.collision.PixelOverlapping
import uk.co.nickthecoder.tickle.collision.Touching
import uk.co.nickthecoder.tickle.events.KeyListener
import uk.co.nickthecoder.tickle.util.Vector2
import uk.co.nickthecoder.tickle.misc.sortedBackwardsWith

private val tempVector = Vector2()

interface StageView : WorldView {

    var stage: Stage

    val comparator: Comparator<ActorDetails>

    val roleComparator: Comparator<Role>

    /**
     * Determines how [findActorsAt], [findRolesAt] etc test if the actor is at the given point.
     * The default used by [AbstractStageView] is [PixelOrBoundingBoxTouching].
     */
    var touching: Touching

    /**
     * Determines actors are tested to see if they are overlapping one another.
     * The default used by [AbstractStageView] is [PixelOverlapping].
     */
    var overlapping: Overlapping



    fun visible(actor: Actor): Boolean {
        val world = actor.appearance.worldRect()

        tempVector.x = world.right
        tempVector.y = world.top
        worldToViewport(tempVector, tempVector)
        if (tempVector.x < 0 || tempVector.y < 0) return false

        tempVector.x = world.left
        tempVector.y = world.bottom
        worldToViewport(tempVector, tempVector)
        return tempVector.x < rect.width && tempVector.y < rect.height
    }

    /**
     * Returns an iterator of Actors that need to be drawn, in the order which they must be drawn.
     * Actors outside the view do not *have* to be included in the iterator.
     */
    fun actorsToDraw(): Iterator<Actor>

    /**
     * Returns the Actors touching the given position, the top-most first.
     */
    fun findActorsAt(point: Vector2): Iterable<Actor> {
        return stage.actors.filter { touching.touching(it, point) }.sortedBackwardsWith(comparator)
    }

    /**
     * Returns the top-most Actor at the given position, or null, if there are no Actors
     * touching the position.
     *
     * Uses [Appearance.touching]. See [Appearance.pixelTouching] for details on how to change the threshold.
     */
    fun findActorAt(point: Vector2): Actor? {
        return findActorsAt(point).firstOrNull()
    }

    fun <T : Role> findRolesAt(klass: Class<T>, point: Vector2): Iterable<T> {
        @Suppress("UNCHECKED_CAST")
        return stage.actors.filter { it.touching(point) && klass.isInstance(it.role) }
            .sortedBackwardsWith(comparator).map { it.role as T }
    }

    fun <T : Role> findRoleAt(klass: Class<T>, point: Vector2): T? {
        return findRolesAt(klass, point).firstOrNull()
    }

    fun requestFocus(listener: KeyListener)

    fun hasFocus( listener : KeyListener) : Boolean

}
