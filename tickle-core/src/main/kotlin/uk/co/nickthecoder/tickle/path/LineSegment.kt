package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.misc.lerp
import uk.co.nickthecoder.tickle.util.IAngle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.Vector2

class LineSegment(override val start: Vector2, end: Vector2) : ContiguousShape<LineSegment> {

    override var end: Vector2 = end
        private set

    override fun joinEnd(to: Vector2) {
        end = to
    }

    override fun length() = (end - start).length()

    override fun along(t: Float, into: Vector2) {
        into.lerp(start, end, t)
    }

    override fun reverse() = LineSegment(end, start)

    override fun translate(by: IVector2) = LineSegment(start + by, end + by)

    override fun scale(by: IVector2) = LineSegment(start * by, end * by)

    override fun rotate(by: IAngle) = LineSegment(start.rotate(by), end.rotate(by))

    override fun buildPoints(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {
        into.add(end)
    }

    fun toPolyline() = Polyline(listOf(start, end))
    override fun toPolyline(distanceTolerance: Float, angleTolerance: Double) = Polyline(listOf(start, end))

    override fun copy() = LineSegment(Vector2(start), Vector2(end))

    fun normal() = (end - start).perpendicular().apply { setUnit() }

    fun direction() = (end - start)

    fun normalisedDirection() = direction().apply { setUnit() }

    operator fun plus(a: Vector2) = LineSegment(start + a, end + a)
    operator fun minus(a: Vector2) = LineSegment(start - a, end - a)

    override fun toString() = "LineSegment $start -> $end"

    companion object {

        fun intersection(a: LineSegment, b: LineSegment, infiniteLines: Boolean): IVector2? {
            // calculate un-normalized direction vectors
            val r = a.direction()
            val s = b.direction()

            val originDist = b.start - a.start

            val uNumerator = originDist.cross(r)
            val denominator = r.cross(s)

            if (Math.abs(denominator) < 0.0001f) {
                // The lines are parallel
                return null
            }

            // solve the intersection positions
            val u = uNumerator / denominator
            val t = originDist.cross(s) / denominator

            if (!infiniteLines && (t < 0 || t > 1 || u < 0 || u > 1)) {
                // the intersection lies outside the line segments
                return null
            }

            // calculate the intersection point
            return a.start + r * t
        }
    }

}
