package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.events.Input
import uk.co.nickthecoder.tickle.stage.Stage
import uk.co.nickthecoder.tickle.stage.StageView
import uk.co.nickthecoder.tickle.stage.View
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Convenient static methods, so you don't need Game.instance... etc.
 * This class is automatically statically imported in all feather game scripts,
 * so you can use all methods using just the method name. e.g.
 *
 *      startScene( "menu" )
 */
object Extras {

    @JvmStatic
    fun startScene(scenePath: String) {
        Game.instance.startScene(scenePath)
    }

    @JvmStatic
    fun findView(viewName: String): View? = Game.instance.scene.findView(viewName)

    @JvmStatic
    fun findStageView(viewName: String): StageView? = Game.instance.scene.findStageView(viewName)

    @JvmStatic
    fun findStage(stageName: String): Stage? = Game.instance.scene.findStage(stageName)

    @JvmStatic
    fun findActorById(id: Int): Actor? = Game.instance.scene.findActorById(id)

    @JvmStatic
    fun findActorByName(name: String): Actor? = Game.instance.scene.findActorByName(name)


    // Remove the need for Game.instance.

    @JvmStatic
    fun getWindow() = Game.instance.window

    @JvmStatic
    fun getResources() = Game.instance.resources

    @JvmStatic
    fun getCurrentSceneName() = Game.instance.sceneName

    @JvmStatic
    fun getPreviousSceneName() = Game.instance.previousSceneName

    @JvmStatic
    fun saveWindowSize() {
        Game.instance.saveWindowSize()
    }

    @JvmStatic
    fun loadWindowSize() {
        Game.instance.loadWindowSize()
    }

    @JvmStatic
    fun findInput(name: String): Input? = Game.instance.resources.inputs.find(name)

    @JvmStatic
    fun quit() {
        Game.instance.quit()
    }

    @JvmStatic
    fun mergeScene(name: String) {
        Game.instance.mergeScene(name)
    }

    @JvmStatic
    fun getProducer() = Game.instance.producer

    @JvmStatic
    fun getDirector() = Game.instance.director

    @JvmStatic
    fun getWindowMousePosition(into: Vector2) {
        Game.instance.window.getMousePosition(into)
    }

    /**
     * Gets the position of the mouse relative to the top left of the window, in pixels.
     * The result will be < 0 or > width/height if the mouse is outside of the window.
     */
    @JvmStatic
    fun getWindowMousePosition(): Vector2 = Game.instance.window.getMousePosition()

    /**
     * Gets the position of the mouse relative to the top left of the window, in pixels.
     * The result will be < 0 or > width/height if the mouse is outside of the window.
     */
    @JvmStatic
    fun getWindowMousePositionBounded(into: Vector2) {
        Game.instance.window.getMousePixelsBounded(into)
    }

    /**
     * Gets the position of the mouse relative to the top left of the window, in pixels.
     * If the mouse is outside of the window, the result is bounded to the size of the window.
     */
    @JvmStatic
    fun getWindowMousePositionBounded(): Vector2 = Game.instance.window.getMousePixelsBounded()

    @JvmStatic
    fun isMouseWithinWindow(): Boolean = Game.instance.window.isMouseWithinWindow()

    /**
     * Gets the position of the mouse relative to the top left of the window, in pixels.
     */
    @JvmStatic
    fun setWindowMousePosition(position: Vector2) {
        Game.instance.window.setMousePosition(position)
    }

    @JvmStatic
    fun playSound(name: String) {
        Game.instance.resources.sounds.find(name)?.play()
    }

    @JvmStatic
    fun arrangeViews() {
        Game.instance.scene.arrangeViews()
    }

    @JvmStatic
    fun dumpStack() {
        Thread.dumpStack()
    }

    @JvmStatic
    fun error(e: Exception) {
        TickleErrorHandler.instance.error(e)
    }

    @JvmStatic
    fun error(message: String) {
        TickleErrorHandler.instance.error(message)
    }

    @JvmStatic
    fun warn(message: String) {
        TickleErrorHandler.instance.warn(message)
    }

    @JvmStatic
    fun info(message: String) {
        TickleErrorHandler.instance.info(message)
    }

}
