/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.util.Rect
import uk.co.nickthecoder.tickle.util.RectInt
import uk.co.nickthecoder.tickle.util.Vector2
import uk.co.nickthecoder.tickle.misc.lerp

open class MoveRectBy(
    val rect: Rect,
    seconds: Float,
    val amount: Vector2,
    ease: Ease
)

    : TimedAnimation(seconds, ease) {

    constructor(rect: Rect, seconds: Float, amount: Vector2)
            : this(rect, seconds, amount, LinearEase.instance)

    private val initialPosition = Rect()
    private val finalPosition = Rect()

    override fun storeInitialValue() {
        initialPosition.set(rect)
        finalPosition.set(rect)
        finalPosition += amount
    }

    override fun update(t: Float) {
        lerp(initialPosition, finalPosition, t, rect)
    }

}


open class MoveRectIntBy(
    val rect: RectInt,
    seconds: Float,
    val amount: Vector2,
    ease: Ease
)

    : TimedAnimation(seconds, ease) {

    constructor(rect: RectInt, seconds: Float, amount: Vector2)
            : this(rect, seconds, amount, LinearEase.instance)

    private val initialPosition = RectInt(0,0,0,0)
    private val finalPosition = RectInt(0,0,0,0)

    override fun storeInitialValue() {
        initialPosition.set(rect)
        finalPosition.set(rect)
        finalPosition += amount
    }

    override fun update(t: Float) {
        lerp(initialPosition, finalPosition, t, rect)
    }

}

