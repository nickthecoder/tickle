package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.IAngle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.ImmutableVector2

data class Mesh(val points: List<IVector2>, val uvs: List<IVector2>, val thickness: Float, val length: Float) {

    /**
     * Create a translated copy.
     */
    fun translate(by: IVector2): Mesh = Mesh(points.map { it + by }, uvs, thickness, length)

    /**
     * Create a translated copy.
     */
    fun scale(by: Float) = Mesh(points.map { it * by }, uvs, thickness * by, length * by)

    /**
     * Create a translated copy.
     */
    fun rotate(by: IAngle) = Mesh(points.map { it.rotate(by) }, uvs, thickness, length)

    // Convenience functions
    fun translate(x: Float, y: Float) = translate(ImmutableVector2(x, y))
    fun rotateDegrees(degrees: Double) = rotate(Angle.degrees(degrees))

}
