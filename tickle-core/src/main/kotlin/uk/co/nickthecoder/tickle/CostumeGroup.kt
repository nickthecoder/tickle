/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.resources.ResourceMap
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.util.DeletableResource
import uk.co.nickthecoder.tickle.util.Dependable
import uk.co.nickthecoder.tickle.misc.RenamableResource

class CostumeGroup(resources: Resources) : ResourceMap<Costume>(resources), DeletableResource, RenamableResource {

    /**
     * The order of the groups within the CostumePicker
     */
    var sortOrder = 0

    var initiallyExpanded = true

    var showInSceneEditor: Boolean = true

    /**
     * A CostumeGroup can be deleted, even if it has Costumes (because those costumes become group-less).
     * So a CostumeGroup always returns an empty list, and can therefore always be deleted.
     */
    override fun dependables(resources: Resources) = emptyList<Dependable>()

    override fun delete(resources: Resources) {
        items().toMutableMap().forEach { name, costume ->
            remove(name)
            costume.costumeGroup = null
        }
        resources.costumeGroups.remove(this)
    }

    override fun rename(resources: Resources, newName: String) {
        resources.costumeGroups.rename(this, newName)
    }
}
