package uk.co.nickthecoder.tickle

/**
 * The Clock is updated once per frame, so that all game objects see the same time throughout a single tick.
 *
 */
object Clock {
    /**
     * A measure of time in seconds. Updated once per frame, It is taken from [System].nanoTime() converted to
     * seconds.
     */
    @JvmStatic
    var seconds: Double = 0.0

    /**
     * When the game started in seconds.
     * The game elapsed time is `[seconds] - [gameStartSeconds]`
     */
     @JvmStatic
    var gameStartSeconds : Double = 0.0

    /**
     * When the scene started in seconds.
     * The scene elapsed time is `[seconds] - [sceneStartSeconds]`
     */
     @JvmStatic
    var sceneStartSeconds : Double = 0.0

    /**
     * Incremented by one each frame.
     * The counter is reset at the start of each scene.
     */
    @JvmStatic
    var tickCount: Int = 0

}
