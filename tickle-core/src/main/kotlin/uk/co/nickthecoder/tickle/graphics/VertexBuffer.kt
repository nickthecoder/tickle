/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.graphics

import org.lwjgl.opengl.GL15.*
import java.nio.FloatBuffer
import java.nio.IntBuffer

class VertexBuffer {

    val handle: Int = glGenBuffers()

    fun bind() {
        glBindBuffer(GL_ARRAY_BUFFER, handle)
    }

    fun delete() {
        glDeleteBuffers(handle)
    }

    fun uploadData(data: FloatBuffer, usage: Usage) {
        glBufferData(GL_ARRAY_BUFFER, data, usage.value)
    }

    fun uploadData(size: Long, usage: Usage) {
        glBufferData(GL_ARRAY_BUFFER, size, usage.value)
    }

    fun uploadSubData(offset: Long, data: FloatBuffer) {
        glBufferSubData(GL_ARRAY_BUFFER, offset, data)
    }

    fun uploadData(data: IntBuffer, usage: Usage) {
        glBufferData(GL_ARRAY_BUFFER, data, usage.value)
    }

}