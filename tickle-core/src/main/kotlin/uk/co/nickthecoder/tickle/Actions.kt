package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.action.Action
import java.util.concurrent.CopyOnWriteArrayList

/**
 * A list of [Action], which
 */
class Actions {
    private val actions = CopyOnWriteArrayList<Action>()

    /**
     * Calls [action].begin, and assuming it returns false (indicating that the Action has not finished),
     * [action] is added to a list, and will be acted upon when [tick] is called.
     */
    fun add( action : Action ) {
        if (! action.begin()) {
            actions.add( action )
        }
    }

    /**
     * Act on all [Action]s one by one.
     * When an action is finished it is automatically removed.
     */
    fun tick() {
        val game = Game.instance
        for (action in actions) {
            game.tryCatch(this) {
                if (action.act()) {
                    actions.remove(action)
                }
            }
        }
    }

    override fun toString() = "Actions count=${actions.size} "
}