/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.Eases
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.misc.lerp
import uk.co.nickthecoder.tickle.util.*
import kotlin.math.sqrt

abstract class EllipticalArcBase(
    val position: Vector2,
    seconds: Float,
    val radius: IVector2,
    val tilt: IAngle?, // See tilt method. The public constructors always set this to null.
    val largeArc: Boolean,
    val sweepArc: Boolean,
    ease: Ease
)

    : TimedAnimation(seconds, ease) {

    protected abstract fun calculateFinalPosition()

    protected val finalPosition = Vector2()

    /**
     * The center of a unit circle in a coordinate system adjusted so that [tilt] and [radius] can be
     * ignored when fitting a circle which touches the start [position] and [finalPosition].
     * The coordinate system is rotated by tilt, then scaled by 1/[radius], then rotated back again.
     */
    private val o = Vector2()

    private var startRadians = 0.0

    private var endRadians = 0.0

    override fun storeInitialValue() {
        calculateFinalPosition()

        // Special case for when doing a complete revolution
        // We would get divide by zeros without this!
        if (finalPosition == position) {
            if (tilt == null) {
                o.set(position.x - radius.x, position.y)
            } else {
                o.set(-radius.x, radius.y)
                o.setRotate(tilt)
                o += position
            }
            startRadians = 0.0
            endRadians = Math.PI * 2.0
            return
        }

        val a = position
        val b = finalPosition

        // transform a and b by : rotate, then scale by 1/radius then rotate back, and we can then solve for the simpler case of a circle of radius 1
        val ta = if (tilt == null) a / radius else (a.rotateRadians(-tilt.radians) / radius).rotate(tilt)
        val tb = if (tilt == null) b / radius else (b.rotateRadians(-tilt.radians) / radius).rotate(tilt)

        // A unit vector of the normal of the line AB
        val normal = (tb - ta).perpendicular()
        var d = (normal.x * normal.x + normal.y * normal.y)
        val unitNormal = normal / Math.sqrt(d.toDouble()).toFloat()

        d = sqrt(Math.max(0f, 1f - d / 4))
        if (largeArc == sweepArc) {
            d = -d
        }
        o.x = (tb.x + ta.x) / 2 + d * unitNormal.x
        o.y = (tb.y + ta.y) / 2 + d * unitNormal.y
        val oa = ta - o
        val ob = tb - o

        startRadians = Math.acos(oa.x / oa.length().toDouble())
        if (oa.y < 0.0) startRadians = -startRadians

        endRadians = Math.acos(ob.x / ob.length().toDouble())
        if (ob.y < 0.0) endRadians = -endRadians

        if (sweepArc && startRadians > endRadians) {
            endRadians += 2 * Math.PI
        }
        if (!sweepArc && startRadians < endRadians) {
            endRadians -= 2 * Math.PI
        }

        o *= radius
    }

    override fun update(t: Float) {
        val theta = lerp(startRadians, endRadians, t.toDouble())
        position.set(
            Math.cos(theta).toFloat(),
            Math.sin(theta).toFloat()
        )

        // We are now on the unit circle at the appropriate angle [theta].
        if (tilt != null) position.setRotate(tilt)
        position *= radius
        if (tilt != null) position.setRotateRadians(-tilt.radians)

        // Finally offset to the origin of the circle
        position += o

    }

    /**
     * If the [radius] isn't large enough to span distance from the start [position] to the [finalPosition],
     * then we need to *jump* to the [finalPosition].
     * This way later movement [Action]s will have to "correct" start [position].
     */
    override fun act() = if (super.act()) {
        position.set(finalPosition)
        true
    } else {
        false
    }

}

/**
 * Moves in an elliptical arc from [position] to a fixed finalPosition.
 * Instead of the more obvious way of defining an ellipse/circle (using a center point),
 * we *calculate* the center point. Doing it this way makes it easier to join movements together.
 * This idea was inspired by SVG paths.
 *
 * When fitting an elliptical arc which touches [position] and finalPosition, there are in general
 * four solutions.
 * [largeArc] and [sweepArc] determine which of the solutions to use.
 *
 *      * [largeArc] : We either choose the "long" path ([largeArc] == true), or the short path.
 *      * [sweepArc] : Chooses which of the mirror symmetric solutions (about the line [position] .. finalPosition)
 *
 * See [https://www.w3.org/TR/SVG11/paths.html#PathDataEllipticalArcCommands]
 *
 * NOTE, The calculating of the ellipse occurs during [begin], not during this [Action]'s constructor.
 *
 */
class EllipticalArcTo private constructor(

    position: Vector2,
    seconds: Float,
    private val finalPos: IVector2,
    radius: IVector2,
    tilt: IAngle?,
    largeArc: Boolean,
    sweepArc: Boolean,
    ease: Ease

) : EllipticalArcBase(position, seconds, radius, tilt, largeArc, sweepArc, ease) {

    constructor(
        position: Vector2, seconds: Float,
        finalPos: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean,
        ease: Ease
    ) : this(
        position, seconds,
        finalPos, radius, null, largeArc, sweepArc,
        ease
    )

    constructor(
        position: Vector2, seconds: Float,
        finalPos: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean,
    ) : this(
        position, seconds,
        finalPos, radius, null, largeArc, sweepArc,
        Eases.linear
    )

    fun tilt(angle: IAngle) = EllipticalArcTo(
        position, seconds, finalPos, radius, angle,
        largeArc, sweepArc, ease
    )

    override fun calculateFinalPosition() {
        this.finalPosition.set(finalPos)
    }

    companion object {

        // NOTE, I deliberately chose to use an upper case C, because from the game programmers point of view,
        // I don't think CircularArcTo should *look* different to EllipticalArcTo.
        // Feather doesn't use the "new" keyword, and these methods are auto-imported (statically).
        @JvmStatic
        fun CircularArcTo(
            position: Vector2, seconds: Float,
            finalPosition: IVector2,
            radius: Float,
            largeArc: Boolean, sweep: Boolean,
            ease: Ease
        ) = EllipticalArcTo(position, seconds, finalPosition, Vector2(radius, radius), largeArc, sweep, ease)

        @JvmStatic
        fun CircularArcTo(
            position: Vector2, seconds: Float,
            finalPosition: IVector2,
            radius: Float,
            largeArc: Boolean, sweep: Boolean
        ) = EllipticalArcTo(position, seconds, finalPosition, Vector2(radius, radius), largeArc, sweep, Eases.linear)

    }
}


/**
 * Moves in an elliptical arc from [position] to [position] + [delta].
 * Instead of the more obvious way of defining an ellipse/circle (using a center point),
 * we *calculate* the center point. Doing it this way makes it easier to join movements together.
 * This idea was inspired by SVG paths.
 *
 * When fitting an elliptical arc which touches [position] and [finalPosition], there are in general
 * four solutions.
 * [largeArc] and [sweepArc] determine which of the solutions to use.
 *
 *      * [largeArc] : We either choose the "long" path ([largeArc]== true), or the short path.
 *      * [sweepArc] : Chooses which of the mirror symmetric solutions (about the line [position] .. [finalPosition])
 *
 * See [https://www.w3.org/TR/SVG11/paths.html#PathDataEllipticalArcCommands]
 *
 * NOTE, The calculating of the ellipse occurs during [begin], not during this [Action]'s constructor.
 *
 */
class EllipticalArcBy private constructor(

    position: Vector2,
    seconds: Float,
    private val delta: IVector2,
    radius: IVector2,
    tilt: IAngle?,
    largeArc: Boolean,
    sweepArc: Boolean,
    ease: Ease

) : EllipticalArcBase(position, seconds, radius, tilt, largeArc, sweepArc, ease) {

    constructor(
        position: Vector2, seconds: Float,
        delta: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean,
        ease: Ease
    ) : this(
        position, seconds,
        delta, radius, null, largeArc, sweepArc,
        ease
    )

    constructor(
        position: Vector2, seconds: Float,
        delta: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean
    ) : this(
        position, seconds,
        delta, radius, null, largeArc, sweepArc,
        Eases.linear
    )

    // NOTE, The Extras object has methods CircularArcBy, which takes a Float for the radius.

    /**
     * The ellipse is tilted, by [angle].
     */
    fun tilt(angle: IAngle) = EllipticalArcBy(
        position, seconds, delta, radius, angle,
        largeArc, sweepArc, ease
    )


    override fun calculateFinalPosition() {
        finalPosition.setPlus(position, delta)
    }

    companion object {

        // NOTE, I deliberately chose to use an upper case C, because from the game programmers point of view,
        // I don't think CircularArcBy should *look* different to EllipticalArcBy.
        // Feather doesn't use the "new" keyword, and these methods are auto-imported.
        @JvmStatic
        fun CircularArcBy(
            position: Vector2, seconds: Float,
            delta: IVector2,
            radius: Float,
            largeArc: Boolean, sweep: Boolean,
            ease: Ease
        ) = EllipticalArcBy(position, seconds, delta, Vector2(radius, radius), largeArc, sweep, ease)

        @JvmStatic
        fun CircularArcBy(
            position: Vector2, seconds: Float,
            delta: IVector2,
            radius: Float,
            largeArc: Boolean, sweep: Boolean
        ) = EllipticalArcBy(position, seconds, delta, Vector2(radius, radius), largeArc, sweep, Eases.linear)

    }

}

/**
 * Move in an ellipse, from [position] back to [position] again.
 * [start] is the current angle around the ellipse. This is used to calculate the center of the
 * ellipse. e.g. if [start] is 0°, then the center will be left of [position] (west),
 * if 90°, then the center will be below [position] (south).
 *
 * [tilt] is used for non-circles only. If 0, then the ellipse isn't tilted.
 */
class Elliptical private constructor(

    private val position: Vector2,
    seconds: Float,
    private val radius: IVector2,
    private val start: Angle,
    private val tilt: Angle,
    private val reverse: Boolean,
    ease: Ease

) : TimedAnimation(seconds, ease) {

    private val origin = Vector2()

    constructor(position: Vector2, seconds: Float, radius: IVector2, ease: Ease) :
            this(position, seconds, radius, Angle.radians(0.0), Angle.radians(0.0), false, ease)

    constructor(position: Vector2, seconds: Float, radius: IVector2) :
            this(position, seconds, radius, Angle.radians(0.0), Angle.radians(0.0), false, Eases.linear)


    fun start(angle: Angle) = Elliptical(position, seconds, radius, angle, tilt, reverse, ease)
    fun tilt(tilt: Angle) = Elliptical(position, seconds, radius, start, tilt, reverse, ease)
    fun reverse() = Elliptical(position, seconds, radius, start, tilt, !reverse, ease)

    override fun update(t: Float) {
        val theta = start.radians + lerp(
            0.0, if (reverse) {
                -Angle.TAU
            } else {
                Angle.TAU
            },
            t.toDouble()
        )
        position.set(
            Math.cos(theta).toFloat(),
            Math.sin(theta).toFloat()
        )
        position *= radius
        if (tilt.radians != 0.0) {
            position.setRotate(tilt)
        }
        position += origin
    }

    override fun storeInitialValue() {
        origin.x = -Math.cos(start.radians).toFloat()
        origin.y = -Math.sin(start.radians).toFloat()
        origin *= radius
        if (tilt.radians != 0.0) {
            origin.setRotateRadians(tilt.radians)
        }
        origin += position
    }

    companion object {
        @JvmStatic
        fun Circular(position: Vector2, seconds: Float, radius: Float, ease: Ease) =
            Elliptical(position, seconds, Vector2(radius, radius), ease)

        @JvmStatic
        fun Circular(position: Vector2, seconds: Float, radius: Float) =
            Elliptical(position, seconds, Vector2(radius, radius), Eases.linear)
    }
}
