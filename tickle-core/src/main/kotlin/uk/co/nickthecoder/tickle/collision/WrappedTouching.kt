/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.collision

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.stage.WrappedStageView
import uk.co.nickthecoder.tickle.util.Vector2

class WrappedTouching(val view: WrappedStageView, val single: Touching)
    : Touching {

    override fun touching(actor: Actor, point: Vector2): Boolean {
        val dx = actor.position.x - point.x
        val dy = actor.position.y - point.y
        val width = (view.joinRight - view.joinLeft)
        val height = (view.joinTop - view.joinBottom)

        val adjustX : Float = when {
            (dx > width / 2) -> width
            (dx < -width / 2) -> -width
            else -> 0f
        }
        val adjustY : Float = when {
            (dy > height / 2) -> height
            (dy < -height / 2) -> -height
            else -> 0f
        }
        point.x += adjustX
        point.y += adjustY
        val result = single.touching(actor, point)
        point.x -= adjustX
        point.y -= adjustY

        return result
    }

}
