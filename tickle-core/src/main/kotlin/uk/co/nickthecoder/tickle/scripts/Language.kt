/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.scripts

import java.io.File
import java.lang.reflect.Modifier

/**
 * The base class for scripting languages, allowing game code to be dynamically loaded.
 *
 * Also, you must add the path(s) where script files are located.
 * However, the default launchers: Tickle and EditorMain will do the following automatically :
 *
 *    ScriptManager.setClasspath(File(resourcesFile.parent, "scripts"))
 */
abstract class Language {

    abstract val fileExtension: String

    abstract val name: String

    protected val classesByName = mutableMapOf<String, Class<*>>()

    open fun register() {
        ScriptManager.register(this)
    }

    abstract fun setClasspath(directory: File)

    abstract fun reload()

    abstract fun addScript(file: File)

    abstract fun scriptFile(className: String): File

    open fun nameForClass(klass: Class<*>): String? {
        for ((name, k) in classesByName) {
            if (k === klass) {
                return name
            }
        }
        return null
    }

    fun classForName(className: String): Class<*>? {
        return classesByName[className]
    }

    /**
     * Returns classes known by this script language, that are sub-classes of the type given.
     * For example, it can be used to find all the scripted Roles, or scripted Directors etc.
     */
    fun subTypes(type: Class<*>): List<Class<*>> {
        val result = classesByName.filter {
            !it.value.isInterface && !Modifier.isAbstract(it.value.modifiers) && type.isAssignableFrom(it.value)
        }.toSortedMap().map { it.value }
        return result
    }

    fun createScript(scriptDirectory: File, scriptName: String, type: Class<*>? = null): File {
        val file = File(scriptDirectory, scriptName + ".${fileExtension}")
        if (!scriptDirectory.exists()) {
            scriptDirectory.mkdirs()
        }
        file.writeText(generateScript(scriptName, type))
        ScriptManager.load(file)
        return file
    }

    abstract fun generateScript(name: String, type: Class<*>?): String
}
