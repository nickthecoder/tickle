/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.animation

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.misc.lerp

class Fade(
        val color: Color,
        seconds: Float,
        val finalAlpha: Float,
        ease: Ease
)

    : TimedAnimation(seconds, ease) {

    constructor(color: Color, seconds: Float, finalAlpha: Float)
            : this(color, seconds, finalAlpha, LinearEase.instance)

    constructor(actor: Actor, seconds: Float, finalAlpha: Float, ease: Ease)
            : this(actor.tint, seconds, finalAlpha, ease)

    constructor(actor: Actor, seconds: Float, finalAlpha: Float)
            : this(actor.tint, seconds, finalAlpha, LinearEase.instance)

    private var initialAlpha = 0f

    override fun storeInitialValue() {
        initialAlpha = color.alpha
    }

    override fun update(t: Float) {
        color.alpha = lerp(initialAlpha, finalAlpha, t)
    }
}
