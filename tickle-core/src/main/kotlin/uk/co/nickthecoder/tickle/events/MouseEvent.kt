/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.events

import uk.co.nickthecoder.tickle.graphics.Window
import uk.co.nickthecoder.tickle.stage.*
import uk.co.nickthecoder.tickle.util.Vector2

class MouseEvent(
        val window: Window,
        val button: Int,
        val state: ButtonState,
        val mods: Int
) : Event() {

    /**
     * The position relative to the window
     */
    val windowPosition = Vector2()

    /**
     * The position relative to the [View] that processed the event.
     * This will be invalid (probably 0,0) if the event is not associated with a View.
     * Note, this is NOT world coordinates.
     */
    val viewportPosition = Vector2()

    /**
     * Used by [StageView]s the [viewportPosition] is converted to a world coordinate.
     */
    val worldPosition = Vector2()

    /**
     * When events are passed onto a Role (via a View), this is the position relative to the
     * Actor's origin. Note that scaling and rotation are NOT processed. So for example, if the click is
     * 10 pixels to the right of the Actor's position, then this will be (10,0) regardless of the Actor's scale or
     * its direction.
     */
    val localPosition = Vector2()

    internal var captured: Boolean = false

    /**
     * Capture the mouse, so that only the caller receives future mouse events until [release] is called.
     */
    fun capture() {
        captured = true
        consume()
    }

    /**
     * This is the opposite of [capture]
     */
    fun release() {
        captured = false
        consume()
    }

    override fun toString() =
        "MouseEvent button#$button @ screenPosition View : $viewportPosition World : $worldPosition   Local : $localPosition"
}
