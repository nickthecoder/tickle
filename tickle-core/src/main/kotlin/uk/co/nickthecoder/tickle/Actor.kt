/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import org.jbox2d.common.Vec2
import org.jbox2d.dynamics.Body
import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.TextStyle
import uk.co.nickthecoder.tickle.physics.TickleBody
import uk.co.nickthecoder.tickle.physics.TickleWorld
import uk.co.nickthecoder.tickle.physics.scale
import uk.co.nickthecoder.tickle.sound.SoundManager
import uk.co.nickthecoder.tickle.stage.FlexPosition
import uk.co.nickthecoder.tickle.stage.Stage
import uk.co.nickthecoder.tickle.stage.StageView
import uk.co.nickthecoder.tickle.stage.WorldView
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.Vector2

class Actor(var costume: Costume, role: Role? = null)

    : ActorDetails {

    val id = nextId++

    override var name: String = ""

    /**
     * The role defines the behaviour of this Actor (how it moves etc).
     *
     * Usually, an actor keeps the same Role forever.
     * However, in rare cases, the role of an Actor can be changed.
     * If you do change the actor's Role, ensure that the role has not been used by another Actor,
     * otherwise bad things will happen!
     *
     * You may re-use a role (that was previously assigned to the same actor). Note, the role's
     * [Role.begin] and [Role.activated] methods will be called each time the role is changed.
     * So if you want to run code only once, then you'll need to set a flag, and have an "if"
     * in your [Role.begin] and [Role.activated] methods.
     */
    var role: Role? = role
        set(v) {
            field?.end()
            field = v
            v?.actor = this
            if (stage != null && stage!!.hasBegun()) {
                v?.begin()
                v?.activated()
            }
        }

    /**
     * This is a simple variable holding the [Stage] this actor is on.
     * Note that [stage] is really a get/set without a field.
     * By having this pair, an Actor's stage can be set by the game programmer in two ways :
     * 1. Setting Actor.stage
     * 2. Calling Stage.add( actor )
     */
    internal var _stage: Stage? = null

    var stage: Stage?
        get() = _stage
        set(v) {
            if (v == null) {
                _stage?.remove(this)
            } else {
                v.add(this)
            }
        }

    /**
     * Only used by the SceneEditor, not used while the game is running.
     */
    var isLocked = false

    val position = Vector2(0.0f, 0.0f)

    /**
     * How much to move this Actor by when the window is resized. x and y range from :
     *
     *      0 => Left/Bottom Aligned
     *      0.5 => Centered
     *      1.0 => Right/Top Aligned
     *
     * This is useful for actor which typically don't move, but you want them to stay "aligned".
     * For example, I use them on "menus", where I want the "Quit" button to stay in the bottom right of the
     * view, even when the view changes size/aspect ratio.
     * In this case, I would set `x` to 1.0 (right aligned) and `y` to 0.0 (bottom aligned)
     *
     * NOTE, This only takes effect when [Scene.arrangeViews] is called, and the actor is on a [Stage]
     * with a [StageView] using [WorldView] with [FlexPosition.enableAutoPosition] == true.
     *
     * For Actors created in the Scene Editor, you can set these values in the `Attributes` dockable.
     * No code is needed in this case :-)
     */
    val autoPositionAlignment = Vector2(0.0f, 0.0f)

    /**
     * Used when the Actor has a [Body] to detect when the position has changed by game code, and therefore the Body
     * needs to be updated.
     */
    private val oldPosition = Vector2(Float.MIN_VALUE, Float.MAX_VALUE)

    /**
     * Gets or sets the x value of position
     */
    override var x: Float
        get() = position.x
        set(v) {
            position.x = v
        }

    override var y: Float
        get() = position.y
        set(v) {
            position.y = v
        }

    override var zOrder: Float = 0f

    var direction: Angle = object : Angle() {
        override var radians = 0.0
            set(v) {
                if (field != v) {
                    field = v
                    dirtyMatrix = true
                    // If the Actor has a Body, this ensures that the body will be updated before JBox2D's calculations
                    // are applied.
                    oldPosition.x = Float.MIN_VALUE
                }
            }
    }

    /**
     * Scales the Actor, if this Actor uses the JBox2d physics engine, then the shapes and the joints between them will
     * also be scaled. However, if the actor is scaled by different x and y values, and circular shapes are used,
     * then these will be converted to polygons, because JBox2d doesn't support ellipses.
     * So make sure that you only scale by scalars (equal x and y values) if this would cause problems
     * (polygons are slower than circles, and polygons won't roll as nicely due to their pointy nature).
     * Also note that once a circle has been converted to a polygon, resetting the scale will NOT return to
     * using circles.
     */
    val scale = Vector2(1.0f, 1.0f)

    /**
     * A convenience, setting scale.x and scale.y to the same value.
     * When getting, if scale.x and scale.y differ, then only scale.x is returned.
     */
    var scaleXY: Float
        get() = scale.x
        set(v) {
            scale.x = v
            scale.y = v
        }

    private val oldScale = Vector2(1.0f, 1.0f)

    /**
     * Changes the color. The red, green, blue and alpha values are multiplied by the texture's pixel values
     * before being displayed on the screen.
     *
     * e.g. A white object will appear red when [tint] = Color(1,0,0).
     * However, a black object will still appear black (because 1 * 0 = 0, so the red value is still zero).
     *
     * You can also use this to change the transparency.
     * e.g. a tint of Color(0,0,0,0.5) will make the actor semi-transparent
     * (assuming it is fully opaque normally). If the actor is already slightly transparent, then
     * it will become even more transparent.
     */
    var tint: Color = Color.white()

    /**
     * Deprecated. Use [tint] instead.
     */
    var color: Color
        get() {
            deprecated("Actor.color", "Actor.tint")
            return tint
        }
        set(v) {
            deprecated("Actor.color", "Actor.tint")
            tint = v
        }

    var appearance: Appearance = InvisibleAppearance(this)

    internal val modelMatrix = Matrix3x2f()

    internal var dirtyMatrix: Boolean = true
        get() {
            return field || position != oldPosition || oldScale != scale
        }

    var body: TickleBody? = null

    val textAppearance: TextAppearance?
        get() {
            val app = appearance
            if (app is TextAppearance) {
                return app
            }
            return null
        }

    val resizeAppearance: ResizeAppearance?
        get() = appearance as? ResizeAppearance

    val ninePatchAppearance: NinePatchAppearance?
        get() {
            val app = appearance
            if (app is NinePatchAppearance) {
                return app
            }
            return null
        }

    val tiledAppearance: TiledAppearance?
        get() {
            val app = appearance
            if (app is TiledAppearance) {
                return app
            }
            return null
        }

    val poseAppearance: PoseAppearance?
        get() {
            val app = appearance
            if (app is PoseAppearance) {
                return app
            }
            return null
        }

    init {
        role?.actor = this
    }

    /**
     * Return false iff the actor requires special transformations to render it.
     */
    internal fun isSimpleImage(): Boolean =
        direction.radians == appearance.directionRadians && scale.x == 1f && scale.y == 1f

    internal fun calculateModelMatrix(): Matrix3x2f {
        if (dirtyMatrix) {
            recalculateModelMatrix()

            body?.let { body ->
                if (oldScale != scale) {
                    val ratio = scale / oldScale
                    body.jBox2DBody.scale(ratio.x, ratio.y)

                    // Should we be automatically scaling the joints too? Probably!
                    // body.scaleJoints(ratio)

                    oldScale.set(scale)
                    updateBody()
                }
                if (oldPosition != position) {
                    updateBody()
                    oldPosition.set(position)
                }
            }

            dirtyMatrix = false

        }
        return modelMatrix
    }

    private fun recalculateModelMatrix() {
        modelMatrix.identity().translate(x, y)
        if (direction.radians != appearance.directionRadians) {
            modelMatrix.rotate((direction.radians - appearance.directionRadians).toFloat())
        }
        if (scale.x != 1f || scale.y != 1f) {
            modelMatrix.scale(scale.x, scale.y)
        }
        modelMatrix.translate(-x, -y)
    }

    fun resize(width: Float, height: Float) {
        appearance.resize(width, height)
    }

    fun width() = appearance.width()

    fun height() = appearance.height()

    /**
     * Change to a different Pose.
     * Note, when the new pose is [ResizeType.NINE_PATCH] or [ResizeType.TILE], then we
     * the size is retained from the previous appearance.
     *
     * When changing from one [ResizeType.SCALE] the final size will depend on the size of the [pose],
     * and the existing [scale] factor.
     */
    fun changeAppearance(pose: Pose) {

        appearance = when (pose.resizeType) {
            ResizeType.TILE -> {
                val tiled = TiledAppearance(this, pose)
                tiled.resize(appearance.width(), appearance.height())
                tiled
            }

            ResizeType.NINE_PATCH -> {
                val ninePatch = NinePatchAppearance(this, pose, pose.ninePatchMargins)
                ninePatch.resize(appearance.width(), appearance.height())
                ninePatch
            }

            else -> {
                PoseAppearance(this, pose)
            }
        }
    }

    fun changeAppearance(ninePatch: NinePatch) {
        val npa = NinePatchAppearance(this, ninePatch.pose, ninePatch)
        npa.resize(appearance.width(), appearance.height())
        appearance = npa
    }

    fun changeAppearance(text: String, textStyle: TextStyle) {
        appearance = TextAppearance(this, text, textStyle)
    }

    private var hiddenAppearance: Appearance? = null
    fun hide() {
        if (appearance !is InvisibleAppearance) {
            hiddenAppearance = appearance
            appearance = InvisibleAppearance(this)
        }
    }

    fun show() {
        if (hiddenAppearance != null && appearance is InvisibleAppearance) {
            appearance = hiddenAppearance!!
        }
    }

    /**
     * A single event can do zero or more of the following :
     *
     * * Play a sound
     * * Change Pose
     * * Change text and/or text style
     * * Change NinePatch
     * * Change Costume
     *
     * Note, if an event triggers a new costume, the other changes will look to the new costume
     * for changes of Pose, text etc.
     *
     */
    fun event(name: String) {

        costume.chooseCostume(name)?.let {
            costume = it
        }

        val pose = costume.choosePose(name)
        if (pose == null) {

            val newText = costume.chooseString(name) ?: textAppearance?.text
            val textStyle = costume.chooseTextStyle(name) ?: textAppearance?.textStyle
            if (newText != null && textStyle != null) {
                changeAppearance(newText, textStyle)
            } else {

                val ninePatch = costume.chooseNinePatch(name)
                if (ninePatch != null) {
                    appearance = NinePatchAppearance(
                        this,
                        ninePatch.pose,
                        ninePatch
                    )
                }

            }

        } else {
            changeAppearance(pose)
        }

        costume.chooseSoundEvent(name)?.let { soundEvent ->
            if ((!soundEvent.onlyWhenVisible) || isVisible()) {
                SoundManager.play(
                    soundEvent.sound,
                    soundEvent.gain * soundEvent.sound.defaultGain.toFloat(),
                    soundEvent.newPriority,
                    soundEvent.priorityThreshold,
                    soundEvent.onlyOnce
                )
            }
        }
    }

    fun stopEvent(eventName: String) {
        costume.events[eventName]?.soundEvents?.forEach { soundEvent ->
            SoundManager.stop(soundEvent.sound)
        }
    }

    /**
     * Return true if the axis aligned bounding box is at least partially within the first view's viewport.
     * If the actor is not on a stage, or the stage has no views, then false is returned.
     */
    fun isVisible(): Boolean {
        val view = stage?.firstView() ?: return false

        return view.worldRect().contains(appearance.worldRect())
    }

    /**
     * Creates a new Actor, whose Costume is defined by looking at this Actor's events of type "Costume".
     * The role for the new Actor is defined by the new Actor's Costume.
     *
     * The new Actor is placed on the same Stage, and at the same position as this Actor.
     *
     * The new Actor is placed on the same stage as this Actor.
     */
    fun createChild(eventName: String): Actor {
        val childActor = costume.createChild(eventName)
        childActor.x = x
        childActor.y = y
        updateBody()

        stage?.add(childActor)
        return childActor
    }

    /**
     * Creates a new Actor, using the Costume provided.
     * The role for the new Actor is defined by the [costume].
     *
     * The new Actor is placed on the same Stage, and at the same position as this Actor.
     */
    fun createChild(costume: Costume): Actor {
        val childActor = costume.createActor()
        childActor.x = x
        childActor.y = y
        updateBody()

        stage?.add(childActor)
        return childActor
    }

    /**
     * Calls [Role.end], and removes the actor from the [Stage].
     *
     * If you want to test if an Actor is dead, the best you can do is check if [stage] == null.
     * However, this isn't perfect, because it is possible for an Actor to be removed from a Stage
     * without the [Role.end] being called.
     */
    fun die() {
        role?.end()
        stage?.remove(this)
    }

    /**
     * Is the point within the actor's rectangular region? Note, this uses a rectangle not aligned
     * with the x/y axis, and is therefore still useful when the actor is rotated.
     */
    fun contains(vector: Vector2) = appearance.contains(vector)

    /**
     * For a PoseAppearance, is the vector non-transparent pixel of the pose.
     * For a TextAppearance, is the vector within the bounding rectangle of the text (note uses a rectangle not aligned
     * with the x/y axis, and is therefore still useful when the actor is rotated).
     */
    fun touching(vector: Vector2) = appearance.touching(vector)

    /**
     * Directly changes the position and the angle of the body. Note, this can cause strange behaviour if the body
     * overlaps another body.
     */
    internal fun updateBody() {
        body?.let { body ->
            val world = body.tickleWorld
            world.tickleToPhysics(tempVec, position)
            body.jBox2DBody.setTransform(tempVec, (direction.radians - (appearance.directionRadians)).toFloat())
        }
    }

    internal fun updateFromBody(world: TickleWorld) {
        body?.let { body ->
            world.physicsToTickle(position, body.jBox2DBody.position)
            direction.radians = body.jBox2DBody.angle.toDouble() + appearance.directionRadians
            // Copy the Actor's position, so that we can test if game code has changed the position, and therefore
            // we will know if the Body needs to be updated. See ensureBodyIsUpToDate.
            oldPosition.set(position)
        }
    }

    fun ensureBodyIsUpToDate() {
        body?.let {
            // NinePatchAppearance and TiledAppearance need to scale and/or change the offsets of the fixtures
            // if the size or alignment has changed.
            appearance.updateBody()

            // oldPosition and oldScale are both used for two purposes :
            // 1) To know when the body is out of date
            // 2) To know when the modelMatrix is out of date.
            // So, recalculate the mode matrix, which will also sync the body.
            calculateModelMatrix()
        }
    }

    private val tempVec = Vec2()

    fun moveForwards(amount: Float) {
        val delta = direction.vector()
        delta *= amount
        position += delta
    }

    fun moveSidewards(amount: Float) {
        val delta = direction.vector()
        delta.setPerpendicular()
        delta *= amount
        position += delta
    }

    override fun toString() = "Actor #$id @ $position Role=${role?.javaClass?.simpleName ?: "<none>"}"

    companion object {
        private var nextId: Int = 0
    }
}
