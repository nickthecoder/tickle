/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.misc

import uk.co.nickthecoder.tickle.resources.Resources

/**
 * A resource, which can be copied, and given a new name.
 *
 * See [Copyable], which does NOT require the copy to be given a new name.
 */
interface CopyableResource<T> {
    fun copy(resources: Resources, newName : String) : T
}
