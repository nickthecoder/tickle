/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.resources

import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.events.CompoundInput
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.sound.Sound
import uk.co.nickthecoder.tickle.util.Dependable
import uk.co.nickthecoder.tickle.misc.ResourcesReader
import java.io.File

open class Resources {

    var file: File = File("")

    val resourceDirectory: File
        get() = file.parentFile ?: File(".").absoluteFile


    val gameInfo = GameInfo("Tickle", "ticklegame", 600, 400, fullScreen = false, resizable = false)

    val textures = ResourceMap<Texture>(this)

    val poses = ResourceMap<Pose>(this)

    val costumes = ResourceMap<Costume>(this)

    val costumeGroups = ResourceMap<CostumeGroup>(this)

    val inputs = ResourceMap<CompoundInput>(this)

    val layouts = ResourceMap<Layout>(this)

    val fontResources = ResourceMap<FontResource>(this)

    val sounds = ResourceMap<Sound>(this)


    val sceneDirectory: File
        get() = File(file.parentFile, "scenes").absoluteFile

    val macrosDirectory: File
        get() = File(file.parentFile, "macros").absoluteFile

    val texturesDirectory: File
        get() = File(file.parentFile, "images").absoluteFile

    val fontDirectory: File
        get() = File(file.parentFile, "fonts").absoluteFile

    val soundsDirectory: File
        get() = File(file.parentFile, "sounds").absoluteFile

    /**
     * If the load failed, then this prevents the resources from being saved, which would overwrite the file
     * with garbage.
     */
    var loadFailed = false

    val listeners = mutableListOf<ResourcesListener>()

    init {
        instance = this
    }

    open fun load(resourcesFile: File) {
        ResourcesReader.load(this, resourcesFile)
    }

    fun save() {
        saveAs(file)
    }

    open fun saveAs(resourcesFile: File) {
        throw IllegalStateException("Resources are read-only")
    }

    open fun createAttributes(): Attributes {
        return RuntimeAttributes()
    }

    open fun findName(resource: Any?, default: String = "?") = findNameOrNull(resource) ?: default

    open fun findNameOrNull(resource: Any?): String? {
        if (resource == null) return null
        return when (resource) {
            is Texture -> textures.findName(resource)
            is Pose -> poses.findName(resource)
            is Costume -> if (resource.costumeGroup == null) {
                costumes.findName(resource)
            } else {
                resource.costumeGroup!!.findName(resource)
            }

            is CostumeGroup -> costumeGroups.findName(resource)
            is CompoundInput -> inputs.findName(resource)
            is Layout -> layouts.findName(resource)
            is FontResource -> fontResources.findName(resource)
            else -> null
        }
    }

    fun findCostumeGroup(costumeName: String): CostumeGroup? {
        costumeGroups.items().values.forEach { costumeGroup ->
            if (costumeGroup.find(costumeName) != null) {
                return costumeGroup
            }
        }
        return null
    }

    fun toPath(file: File): String {
        try {
            return file.absoluteFile.toRelativeString(resourceDirectory)
        } catch (e: Exception) {
            return file.absolutePath
        }
    }

    fun fromPath(path: String): File {
        return resourceDirectory.resolve(path).absoluteFile
    }

    fun sceneExists(path: String): Boolean = path.isNotBlank() && scenePathToFile(path).exists()

    fun scenePathToFile(path: String): File {
        return sceneDirectory.resolve(path + ".scene")
    }

    fun sceneFileToPath(file: File): String {
        val path = if (file.isAbsolute) {
            file.relativeToOrSelf(sceneDirectory).path
        } else {
            file.path
        }
        if (path.endsWith(".scene")) {
            return path.substring(0, path.length - 6)
        } else {
            return path
        }
    }

    fun fireAdded(resource: Any, name: String) {
        listeners.toList().forEach {
            it.resourceAdded(resource, name)
        }
    }

    fun fireRemoved(resource: Any, name: String) {
        listeners.toList().forEach {
            it.resourceRemoved(resource, name)
        }
    }

    fun fireRenamed(resource: Any, oldName: String, newName: String) {
        listeners.toList().forEach {
            it.resourceRenamed(resource, oldName, newName)
        }
    }

    fun fireChanged(resource: Any) {
        listeners.toList().forEach {
            it.resourceChanged(resource)
        }
    }

    /**
     * Reloads the textures, sounds and fonts.
     */
    fun reload() {
        textures.items().values.forEach { it.reload() }
        sounds.items().values.forEach { it.reload() }
        fontResources.items().values.forEach { it.reload() }

        listeners.toList().forEach {
            it.resourcesReloaded()
        }
    }

    fun scriptDirectory() = File(file.parentFile.absoluteFile, "scripts")

    fun macroDirectory() = File(file.parentFile.absoluteFile, "macros")

    fun destroy() {
        textures.items().values.forEach { it.destroy() }
        fontResources.items().values.forEach { it.destroy() }
        sounds.items().values.forEach { it.destroy() }

        textures.clear()
        poses.clear()
        costumes.clear()
        costumeGroups.clear()
        inputs.clear()
        layouts.clear()
        fontResources.clear()
        sounds.clear()
    }

    /**
     * Returns all scenes (as SceneStubs) which depend on [costume].
     * This implementation always returns an empty list, but DesignResources overrides this.
     *
     * This pattern allows us to rename a costume in the Editor, without Costume being dependant on
     * SceneStub (which is not in library tickle-code, it is in tickle-editor)
     */
    open fun sceneDependables(costume: Costume): List<Dependable> = emptyList()

    /**
     * Returns all scenes (as SceneStubs) which depend on [layout].
     * This implementation always returns an empty list, but DesignResources overrides this.
     *
     * This pattern allows us to rename a Layout in the Editor, without Layout being dependant on
     * SceneStub (which is not in library tickle-code, it is in tickle-editor)
     */
    open fun sceneDependables(layout: Layout): List<Dependable> = emptyList()

    /**
     * This implementation does nothing, but DesignResources overrides this.
     *
     * We can renamed a layout from the Editor, without Layout being dependant on SceneStub.
     * SceneStub is in tickle-editor, NOT in tickle-core.
     */
    open fun updateScenesAfterLayoutRenamed(oldName: String, newName: String) {}

    companion object {
        /**
         * A convenience, so that game scripts can easily get access to the resources.
         *
         * I've deprecated this, because when we test a game from the Editor, we create a COPY of the resources,
         * so resources is no longer a singleton!
         *
         * FYI, The editor must create a COPY of the resources, because the game may "fiddle" with the resources.
         * For example, a multi-player game may make copies of the Poses/Costumes, with different colours for
         * each player. The editor doesn't want to see those new Poses/Costumes!
         *
         * `Game.instance.resources` from your game code instead of `Resources.instance`.
         *
         * From within the Editor, we should get the resources via Editor.resources.
         */
        @Deprecated(message = "When using the editor, there can be TWO sets of resources. Use Game.instance.resources, or Editor.resources")
        lateinit var instance: Resources
    }
}
