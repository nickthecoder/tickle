/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.ActionHolder
import uk.co.nickthecoder.tickle.events.MouseListener
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.resources.AutoScale
import uk.co.nickthecoder.tickle.resources.Layout
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.stage.*
import uk.co.nickthecoder.tickle.util.Rect


/**
 * A new [Scene] is created each time a scene begins, and the old [Scene] is left to be garbage collected.
 * Therefore, nothing unique to a scene is held on [Game].
 * A Scene holds the [Stage]s, [View]s and the [Director]. (Not that in previous version of Tickle,
 * Director was part of [Game], and is still available as a "getter" via [Game.director]).
 */
class Scene {

    var layoutName: String = "default"

    var director: Director = NoDirector()
        set(v) {
            if (field is MouseListener) {
                Game.instance.mouseListeners.remove(field as MouseListener)
            }
            if (v is MouseListener) {
                Game.instance.mouseListeners.add(v)
            }
            field = v
        }

    var background: Color = Color.black()

    var showMouse: Boolean = true

    /**
     * To help document your game. e.g. to explain what this scene does, and how.
     */
    var comments: String = ""

    /**
     * Not used during normal game play, only used in the SceneEditor.
     *
     * When we load a [Scene] in the editor, we need to separate the parts which are `included`
     * from additional scenes
     * (because we don't want these to be edited, and nor do we want these saved as part of `this` scene).
     * However, we may still want to see these parts of the scene when editing.
     * (When they are made visible, they are drawn semi-transparent).
     *
     * Key is the path of the included scene.
     */
    val includedScenes = mutableMapOf<String, Scene>()

    private val mutableStages = mutableMapOf<String, Stage>()

    val stages: Map<String, Stage> = mutableStages

    /**
     * [Action]s, which are automatically acted upon as part of the game loop.
     * Once an action is finished, it is automatically removed from the list.
     * [Actions.tick] is called during the game loop after all [Role]s, the [Director] and [Producer] "ticked",
     * but before the [View]s [Stage]s are "ticked".
     *
     * Beware that actions created from a [Role] will still be acted upon if the Role's
     * [Actor] dies.
     *
     * Also beware of creating actions which `contest` with each other. e.g. If you have two
     * Actions which are both panning a view, then the view may jump back and forth, as they
     * both try to pull in different directions.
     * The recommended way to combat this is to create an [ActionHolder] on your [Director]
     * specifically for each "type" of action, and replace the action, rather than creating
     * an extra one.
     */
    val actions = Actions()

    /**
     * [View]s keyed on the view's name (as given in the `Layout` in the Editor)
     */
    internal val views = mutableMapOf<String, View>()

    private var orderedViews: List<View>? = null

    internal fun merge(resources: Resources, newScene: Scene): Scene {
        val defaultStageName = resources.layouts.find(layoutName)?.defaultStageName ?: "?"
        val defaultStage = findStage(defaultStageName) ?: stages().firstOrNull()

        for ((newStageName, newStage) in newScene.stages) {
            val intoStage = stages[newStageName] ?: defaultStage ?: continue
            for (newActor in newStage.actors.toList()) {
                intoStage.add(newActor)
                newActor.role?.let { role ->
                    role.begin()
                    role.activated()
                }
            }
        }
        return newScene
    }

    fun stages() = stages.values

    fun views(): Collection<View> = orderedViews ?: views.values

    fun stageViews(): Collection<StageView> = (orderedViews ?: views.values).filterIsInstance(StageView::class.java)

    fun findStage(name: String) = stages[name]

    fun findView(name: String) = views[name]

    fun findStageView(name: String): StageView? {
        val view = findView(name)
        return if (view is StageView) {
            view
        } else {
            null
        }
    }

    fun findActorById(id: Int): Actor? {
        stages().forEach {
            val result = it.findActorById(id)
            if (result != null) return result
        }
        return null
    }

    fun findActorByName(name: String): Actor? {
        stages().forEach {
            val a = it.findActorByName(name)
            if (a != null) return a
        }
        return null
    }

    fun addStage(name: String, stage: Stage) {
        mutableStages[name] = stage
    }

    fun removeStage(name: String) {
        mutableStages.remove(name)
    }

    fun renameStage(oldName: String, newName: String) {
        val stage = mutableStages[oldName] ?: return
        mutableStages.remove(oldName)
        mutableStages[newName] = stage
    }

    fun addView(name: String, view: View) {
        views[name] = view
        reorderViews()
    }

    fun removeView(name: String) {
        views.remove(name)
        reorderViews()
    }

    /**
     * If you change a view's zOrder, you must call this, otherwise it has no effect.
     */
    fun reorderViews() {
        orderedViews = views.values.sortedBy { it.zOrder }
    }

    fun begin() {
        val game = Game.instance
        game.window.showMouse(showMouse)

        game.director.createWorlds(Game.instance.resources)
        stages.values.forEach { stage ->
            val world = stage.world
            if (world != null) {
                stage.actors.forEach { actor ->
                    actor.costume.bodyDef?.let { bodyDef ->
                        world.createBody(bodyDef, actor)
                    }
                }
            }
            stage.begin()
        }
        views.values.forEach { view ->
            view.begin()
        }
    }

    fun activated() {
        stages.values.forEach { stage ->
            stage.activated()
        }
    }

    /**
     * Call this from [Producer.layout], to reposition and scale the views given the data in the `Layout`
     * section of the Editor.
     *
     * Each view's position and size in pixel coordinates is first calculated by [FlexPosition].
     * Then, we compare the view's new size, with the size it would have been if the Window
     * were size : [GameInfo.width] x [GameInfo.height]. And apply a scaling factor depending on
     * [AutoScale].
     *
     * AutoScale does NOT alter the view's [StageView.worldFocal.
     * However, the view's [StageView.viewportFocal] IS changed.
     *
     * If [FlexPosition.enableAutoPosition] == true, then the positions of the actors in that Stage are also changed,
     * based on the [Actor]'s alignment ([Actor.autoPositionAlignment]),
     * as well as how the view's [StageView.worldRect] has changed size.
     * For example, if you right-align an actor, and the view (in world coordinates) gets wider,
     * then the actor will be moved right (assuming autoPositionAlignment.x > 0).
     *
     * BEWARE. if [FlexPosition.enableAutoPosition] == true, and you are using Actions with absolute positions,
     * then the animation will probably stutter, and the final position won't be correct.
     * This is because the final absolute position will be based off of the OLD view's size.
     * In such cases, it may be best to disable `autoPosition`, and either do "something clever!", or use
     * [AutoScale.LETTERBOX], so that resizing the window never hides your lovely animation ;-)
     */
    fun arrangeViews() {
        val gi = Game.instance.resources.gameInfo
        val window = Game.instance.window

        views.values.forEach { view ->
            val position = view.flexPosition
            if (position != null) {

                val defaultRect = position.calculateRectangle(gi.width, gi.height)
                val newRect = position.calculateRectangle(window.width, window.height)

                // These are only used for WorldView/StageView
                var oldWorldRect = Rect()
                var viewFocalRatioX = 0f
                var viewFocalRatioY = 0f

                val oldMargins = position.calculateMargins(view, defaultRect)

                if (view is WorldView) {
                    oldWorldRect = view.worldRect()
                    // Remember were the view focal is as a ratio of the view's size.
                    viewFocalRatioX = (view.viewportFocal.x - oldMargins.left) /
                            (view.width.toFloat() - oldMargins.left - oldMargins.right)
                    viewFocalRatioY = (view.viewportFocal.y - oldMargins.bottom) /
                            (view.height.toFloat() - oldMargins.bottom - oldMargins.top)
                }

                view.rect = newRect

                if (view is WorldView) {

                    position.adjustScale(view, defaultRect)

                    if (view is StageView) {
                        if (position.enableAutoPosition) {
                            val newWorldRect = view.worldRect()
                            val dx = newWorldRect.width - oldWorldRect.width
                            val dy = newWorldRect.height - oldWorldRect.height
                            // Move each actor according to its autoPositionAlignment
                            for (actor in view.stage.actors) {
                                actor.x += dx * actor.autoPositionAlignment.x
                                actor.y += dy * actor.autoPositionAlignment.y
                            }
                        }
                    }

                    val newMargins = position.calculateMargins(view, defaultRect)
                    // Adjust the viewFocal, so that it is still in the same place as a proportion of the width/height.
                    view.viewportFocal.x =
                        newMargins.left + (view.width - newMargins.left - newMargins.right) * viewFocalRatioX
                    view.viewportFocal.y =
                        newMargins.bottom + (view.height - newMargins.bottom - newMargins.top) * viewFocalRatioY
                }
            }

        }
    }

    fun end() {
        stages.values.forEach { stage ->
            stage.end()
        }
    }

    fun draw(renderer: Renderer) {
        renderer.clear(background)

        orderedViews?.forEach { view ->
            view.draw(renderer)
        }
    }


    /**
     * Used by the Editor to determine this scene uses [costume].
     * If it does, then [costume] cannot be deleted.
     */
    fun dependsOn(costume: Costume): Boolean {
        for (stage in stages()) {
            for (actor in stage.actors) {
                if (actor.costume.dependsOn(costume)) return true
            }
        }
        return false
    }

    /**
     * Used by the Editor to determine if this scene uses [layout].
     * If it does, then [layout] cannot be deleted.
     */
    fun dependsOn(resources: Resources, layout: Layout) = resources.findNameOrNull(layout) == layoutName

}
