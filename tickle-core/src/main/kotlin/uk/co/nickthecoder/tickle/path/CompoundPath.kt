package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.util.IAngle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * A set of [Path]s, with discontinuous jumps between them.
 *
 * This allows us to define shapes with holes.
 */
class CompoundPath(startPoint: Vector2 = Vector2()) : Shape<CompoundPath> {

    val paths = mutableListOf<Path>()

    private var currentPath = Path(startPoint).apply {
        paths.add(this)
    }

    override val start: Vector2
        get() = paths.first().start

    override val end: Vector2
        get() = paths.last().end

    override fun length() = paths.sumOf { it.length().toDouble() }.toFloat()


    fun lineTo(to: Vector2) = currentPath.lineTo(to)
    fun lineTo(x: Float, y: Float) = currentPath.lineTo(x, y)

    fun lineBy(delta: IVector2) = currentPath.lineBy(delta)
    fun lineBy(x: Float, y: Float) = currentPath.lineBy(x, y)


    fun ellipticalArcTo(to: Vector2, radius: Vector2, largeArc: Boolean, sweepArc: Boolean) =
        currentPath.ellipticalArcTo(to, radius, largeArc, sweepArc)

    fun ellipticalArcTo(to: Vector2, radius: Vector2, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        currentPath.ellipticalArcTo(to, radius, largeArc, sweepArc, tilt)

    fun ellipticalArcBy(delta: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean) =
        currentPath.ellipticalArcBy(delta, radius, largeArc, sweepArc)

    fun ellipticalArcBy(delta: IVector2, radius: IVector2, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        currentPath.ellipticalArcBy(delta, radius, largeArc, sweepArc, tilt)


    fun circularArcTo(to: Vector2, radius: Float, largeArc: Boolean, sweepArc: Boolean) =
        currentPath.circularArcTo(to, radius, largeArc, sweepArc)

    fun circularArcTo(to: Vector2, radius: Float, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        currentPath.circularArcTo(to, radius, largeArc, sweepArc, tilt)

    fun circularArcBy(delta: IVector2, radius: Float, largeArc: Boolean, sweepArc: Boolean) =
        currentPath.circularArcBy(delta, radius, largeArc, sweepArc)

    fun circularArcBy(delta: IVector2, radius: Float, largeArc: Boolean, sweepArc: Boolean, tilt: IAngle) =
        currentPath.circularArcBy(delta, radius, largeArc, sweepArc, tilt)


    /**
     * Add a bezier curve. The start of the bezier curve is this path's current end point
     * or (0,0) if there are no [ContiguousShape]s.
     *
     * [to] is the final point. [deltaC1] is the control point relative to the start, and
     * [deltaC2] is the 2nd control point, relative to [to].
     */
    fun bezierTo(to: Vector2, deltaC1: Vector2, deltaC2: Vector2) =
        currentPath.bezierTo(to, deltaC1, deltaC2)

    /**
     * Add a bezier curve. The first control point is symmetrically opposite to the previous
     * bezier's second control point. If this [Path] is currently empty, or if
     * the previous [ContiguousShape] is not a [Bezier], then an exception is thrown.
     */
    fun bezierTo(to: Vector2, deltaC2: Vector2) =
        currentPath.bezierTo(to, deltaC2)

    /**
     * Add a bezier curve. The start of the bezier curve is this path's current end point
     * or (0,0) if there are no [ContiguousShape]s.
     *
     * The end point of the curve is `start + delta`.
     *
     * [deltaC1] is the control point relative to the start, and
     *
     * [deltaC2] is the 2nd control point, relative to the end.
     */
    fun bezierBy(delta: IVector2, deltaC1: IVector2, deltaC2: IVector2) = currentPath.bezierBy(delta, deltaC1, deltaC2)

    /**
     * Add a bezier curve. The start of the bezier curve is this path's current end point
     * or (0,0) if there are no [ContiguousShape]s.
     *
     * The end point of the curve is `start + delta`.
     * The first control point is symmetrically opposite to the previous
     * bezier's second control point. If this [Path] is currently empty, or if
     * the previous [ContiguousShape] is not a [Bezier], then an exception is thrown.
     *
     * [deltaC2] is the 2nd control point, relative to the end.
     */
    fun bezierBy(delta: IVector2, deltaC2: IVector2) = currentPath.bezierBy(delta, deltaC2)


    fun close() {
        currentPath.close()
        jumpTo(Vector2(currentPath.end))
    }

    fun jumpTo(pos: Vector2) {
        if (currentPath.shapes.isEmpty()) {
            paths.removeLast()
        }
        currentPath = Path(pos)
        paths.add(currentPath)
    }

    fun jumpTo(x: Float, y: Float) = jumpTo(Vector2(x, y))

    fun jumpBy(delta: IVector2) = jumpTo(currentPath.end + delta)
    fun jumpBy(x: Float, y: Float) = jumpTo(currentPath.end.x + x, currentPath.end.y + y)


    override fun copy(): CompoundPath {
        val copy = CompoundPath()
        for (path in paths) {
            copy.paths.add(path.copy())
        }
        return copy()
    }

    override fun reverse(): CompoundPath {
        val copy = CompoundPath()
        for (path in paths) {
            copy.paths.add(path.reverse())
        }
        return copy()
    }

    override fun translate(by: IVector2): CompoundPath {
        val copy = CompoundPath()
        for (path in paths) {
            copy.paths.add(path.translate(by))
        }
        return copy()
    }

    override fun scale(by: IVector2): CompoundPath {
        val copy = CompoundPath()
        for (path in paths) {
            copy.paths.add(path.scale(by))
        }
        return copy()
    }

    override fun rotate(by: IAngle): CompoundPath {
        val copy = CompoundPath()
        for (path in paths) {
            copy.paths.add(path.rotate(by))
        }
        return copy()
    }

    override fun along(t: Float, into: Vector2) {
        TODO("Not yet implemented")
    }

    override fun toPolylines(distanceTolerance: Float, angleTolerance: Double) = paths.map { it.toPolyline() }

}
