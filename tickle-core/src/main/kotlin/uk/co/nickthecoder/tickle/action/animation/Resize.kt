/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.animation

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.misc.lerp

class Resize(
        val actor: Actor,
        seconds: Float,
        val finalWidth: Float = actor.appearance.width(),
        val finalHeight: Float = actor.appearance.height(),
        ease: Ease
)

    : TimedAnimation(seconds, ease) {

    constructor(actor: Actor, seconds: Float, finalWidth: Float, finalHeight: Float) : this(actor, seconds, finalWidth, finalHeight, LinearEase.instance)

    private var initialWidth = 0f
    private var initialHeight = 0f

    override fun storeInitialValue() {
        initialWidth = actor.appearance.width()
        initialHeight = actor.appearance.height()
    }

    override fun update(t: Float) {
        actor.resize(lerp(initialWidth, finalWidth, t), lerp(initialHeight, finalHeight, t))
    }

}
