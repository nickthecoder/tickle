/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.physics.offset
import uk.co.nickthecoder.tickle.physics.scale
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * The base class for NinePatchAppearance and TiledAppearance which can both be given an arbitrary size without scaling.
 */
abstract class ResizeAppearance(override val actor: Actor) : AbstractAppearance(actor), AppearanceWithPose {

    val size = Vector2(1.0f, 1.0f)

    /**
     * The alignment, where x,y are both in the range 0..1
     * (0,0) means the Actor's position is at the bottom left of the NinePatch.
     * (1,1) means the Actor's position is at the top right of the NinePatch.
     */
    val sizeAlignment = Vector2(0.5f, 0.5f)

    internal val oldSize = Vector2(1.0f, 1.0f)

    protected val oldAlignment = Vector2(0.5f, 0.5f)


    override fun height() = size.y

    override fun width() = size.x


    override fun offsetX() = size.x * sizeAlignment.x

    override fun offsetY() = size.y * sizeAlignment.y

    override fun touching(point: Vector2) = pixelTouching(point)

    override fun resize(width: Float, height: Float) {
        size.x = width
        size.y = height
    }

    override fun updateBody() {
        if (oldSize != size) {
            actor.body?.jBox2DBody?.scale((size.x / oldSize.x).toFloat(), (size.y / oldSize.y).toFloat())

            oldSize.set(size)
        }
        if (oldAlignment != sizeAlignment) {
            actor.body?.let { body ->
                val world = body.tickleWorld
                body.jBox2DBody.offset(
                    world.tickleToPhysics((oldAlignment.x - sizeAlignment.x) * width()),
                    world.tickleToPhysics((oldAlignment.y - sizeAlignment.y) * height())
                )
            }
            oldAlignment.set(sizeAlignment)
        }
    }

}
