/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.stage

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.ActorDetails
import uk.co.nickthecoder.tickle.Role
import uk.co.nickthecoder.tickle.collision.Overlapping
import uk.co.nickthecoder.tickle.collision.PixelOrBoundingBoxTouching
import uk.co.nickthecoder.tickle.collision.PixelOverlapping
import uk.co.nickthecoder.tickle.collision.Touching
import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.misc.sortedBackwardsWith
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.RectInt
import uk.co.nickthecoder.tickle.util.Vector2

// Note, this is marked as abstract, so that the Editor will NOT show it in the list of possible StageViews,
// as it requires a zero argument constructor.

abstract class AbstractStageView(override val comparator: Comparator<ActorDetails>)
    : StageView, MouseListener {

    override val roleComparator: Comparator<Role> = Comparator { o1, o2 ->
        comparator.compare(o1.actor, o2.actor)
    }

    override var flexPosition: FlexPosition? = null

    override var zOrder = 0

    override var rect = RectInt(0, 0, 10, 10)

    override lateinit var stage: Stage

    final override val worldFocal = Vector2(0f, 0f)
    final override val viewportFocal = Vector2(0f, 0f)

    override var direction = Angle()

    override val scale = Vector2(1f, 1f)


    /**
     * Lazily evaluates
     */
    override var overlapping: Overlapping //= PixelOverlapping.instance
        get() {
            if (overlappingPrivate == null) {
                overlappingPrivate = defaultOverlapping()
            }
            return overlappingPrivate!!
        }
        set(v) {
            overlappingPrivate = v
        }
    private var overlappingPrivate: Overlapping? = null
    protected open fun defaultOverlapping(): Overlapping = PixelOverlapping.instance

    override var touching: Touching
        get() {
            if (touchingPrivate == null) {
                touchingPrivate = defaultTouching()
            }
            return touchingPrivate!!
        }
        set(v) {
            touchingPrivate = v
        }
    private var touchingPrivate: Touching? = null
    protected open fun defaultTouching(): Touching = PixelOrBoundingBoxTouching.instance

    override var handleMouseButtons: Boolean = true

    private var mouseCapturedBy: MouseButtonListener? = null

    override fun begin() {
    }

    override val worldWidth: Float
        get() = width / scale.x

    override val worldHeight: Float
        get() = height / scale.y

    override fun worldToViewport(world: Vector2, into: Vector2) {
        into.x = (world.x - worldFocal.x) * scale.x
        into.y = (world.y - worldFocal.y) * scale.y

        if (direction.radians != 0.0) {
            into.setRotateRadians(direction.radians)
        }

        into.x += viewportFocal.x
        into.y += viewportFocal.y
    }

    override fun viewportToWorld(viewport: Vector2, into: Vector2) {
        into.x = viewport.x - viewportFocal.x
        into.y = viewport.y - viewportFocal.y

        if (direction.radians != 0.0) {
            into.setRotateRadians(-direction.radians)
        }

        into.x /= scale.x
        into.y /= scale.y

        into.x += worldFocal.x
        into.y += worldFocal.y
    }

    override fun windowToWorld(window: Vector2, world: Vector2) {
        windowToViewport(window, world)
        viewportToWorld(world, world)
    }

    override fun worldToWindow(world: Vector2, window: Vector2) {
        worldToViewport(world, window)
        viewportToWindow(window, window)
    }

    private val viewMatrix = Matrix3x2f()

    private fun calculateViewMatrix(): Matrix3x2f {

        // Calculate the world coordinates of the view's bounds if there were no rotation of the view.
        val left = (-viewportFocal.x / scale.x + worldFocal.x)
        val bottom = (-viewportFocal.y / scale.y + worldFocal.y)
        val right = ((rect.width - viewportFocal.x) / scale.x + worldFocal.x)
        val top = ((rect.height - viewportFocal.y) / scale.y + worldFocal.y)

        // Copied (with adjustments) from Matrix4f.setOrtho2D
        viewMatrix.set(
            2f / (right - left), 0f,
            0f, 2f / (top - bottom),
            -(right + left) / (right - left), -(top + bottom) / (top - bottom)
        )

        if (direction.radians != 0.0) {
            viewMatrix.translate(worldFocal.x, worldFocal.y)
            if (direction.radians != 0.0) {
                viewMatrix.rotate(direction.radians.toFloat())
            }
            viewMatrix.translate(-worldFocal.x, -worldFocal.y)
        }

        return viewMatrix
    }

    override fun draw(renderer: Renderer) {
        renderer.clip(rect.left, rect.bottom, rect.width, rect.height)
        renderer.beginView(calculateViewMatrix())
        drawActors(renderer)
        renderer.endView()
    }

    open fun drawActors(renderer: Renderer) {
        actorsToDraw().forEach { actor ->
            actor.appearance.draw(renderer)
        }
    }


    /**
     * Returns an iterator of Actors in this view.
     * The actors will be iterated in the OPPOSITE order compared to [actorsToDraw], which
     * makes it useful for finding actors at a given point of the view.
     * If [all] is true, then all actors must be returned.
     * If [all] is false, then it is implementation dependant - it may only return visible actors, or all actors.
     */
    open fun actorsBackwards(all: Boolean): Iterator<Actor> =
        stage.actors.sortedBackwardsWith(comparator).iterator()

    override fun actorsToDraw(): Iterator<Actor> =
        stage.actors.sortedWith(comparator).iterator()

    private fun setLocalPosition(listener: MouseButtonListener, event: MouseEvent) {
        if (listener is Role) {
            event.localPosition.set(
                event.worldPosition.x - listener.actor.position.x,
                event.worldPosition.y - listener.actor.position.y
            )
        }
    }

    private fun sendMouseButtonEvent(listener: MouseButtonListener, event: MouseEvent) {
        setLocalPosition(listener, event)
        listener.onMouseButton((event))
        if (event.isConsumed()) {
            mouseCapturedBy = if (event.captured && mouseCapturedBy != listener) {
                listener
            } else {
                null
            }
        }
    }

    override fun onMouseButton(event: MouseEvent) {

        if (handleMouseButtons) {
            viewportToWorld(event.viewportPosition, event.worldPosition)

            mouseCapturedBy?.let {
                event.captured = true
                sendMouseButtonEvent(it, event)
                if (event.isConsumed()) return
            }

            // Is the mouse button within the viewport?
            val viewPos = event.viewportPosition
            if (viewPos.x >= 0 && viewPos.y >= 0 && viewPos.x < rect.width && viewPos.y < rect.height) {

                findActorsAt(event.worldPosition).forEach { actor ->
                    val role = actor.role
                    if (role is MouseButtonListener) {
                        sendMouseButtonEvent(role, event)
                        if (event.isConsumed()) return
                    }
                }
            }
        }

    }

    override fun onMouseMove(event: MouseEvent) {
        viewportToWorld(event.viewportPosition, event.worldPosition)

        if (mouseCapturedBy is MouseListener) {
            setLocalPosition(mouseCapturedBy!!, event)
            (mouseCapturedBy as MouseListener).onMouseMove(event)
        }
    }

    private var focus: KeyListener? = null

    override fun requestFocus(listener: KeyListener) {
        (focus as? FocusListener)?.lostFocus()
        focus = listener
        (focus as?FocusListener)?.gainedFocus()
    }

    override fun hasFocus(listener: KeyListener): Boolean {
        return listener === focus
    }

    override fun tick() {}

    override fun toString() =
        "${javaClass.simpleName} : $rect ${viewportFocal.x}, ${viewportFocal.y} -> ${worldFocal.x}, ${worldFocal.y}"
}

private val UNSCALED = Vector2(1f, 1f)
