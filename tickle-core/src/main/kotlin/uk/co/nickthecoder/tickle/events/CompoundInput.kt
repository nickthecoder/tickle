/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.events

import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.util.DeletableResource
import uk.co.nickthecoder.tickle.util.Dependable
import uk.co.nickthecoder.tickle.misc.RenamableResource


class CompoundInput : Input, DeletableResource, RenamableResource {

    val inputs = mutableSetOf<Input>()

    var comments = ""

    fun add(input: Input) {
        inputs.add(input)
    }

    fun remove(input: Input) {
        inputs.remove(input)
    }

    override fun isPressed(): Boolean {
        return inputs.firstOrNull { it.isPressed() } != null
    }

    override fun matches(event: KeyEvent): Boolean {
        return inputs.firstOrNull { it.matches(event) } != null
    }
    override fun matches(event: MouseEvent): Boolean {
        return inputs.firstOrNull { it.matches(event) } != null
    }


    override fun dependables(resources: Resources) = emptyList<Dependable>()

    override fun delete(resources: Resources) {
        resources.inputs.remove(this)
    }

    override fun rename(resources: Resources, newName: String) {
        resources.inputs.rename(this, newName)
    }

}
