package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.graphics.Window

/**
 * If the [inner] [Action] may go wonky if the window is resized during its operation,
 * then we use wrap the action in [PreventWindowResize].
 * The window will not be resizable when this action starts, and will be resizable again
 * when it finishes.
 *
 * Note, there is a counter mechanism, so that multiple overlapping actions can use this technique,
 * without conflict.
 *
 * Warning: If this action is stopped before completion, then the window will be forever non-resizable.
 *
 * See [Window.preventResize]
 */
class PreventWindowResize(val inner: Action) : Action {
    override fun begin(): Boolean {
        if (inner.begin()) return true
        Game.instance.window.preventResize()
        return false
    }

    override fun act(): Boolean {
        if (inner.act()) {
            Game.instance.window.allowResize()
            return true
        }
        return false
    }
}