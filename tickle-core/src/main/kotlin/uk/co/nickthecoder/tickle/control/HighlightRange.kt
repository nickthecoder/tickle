package uk.co.nickthecoder.tickle.control

/**
 * A range of text which appears different, e.g. a different foreground/background color.
 *
 * Ranges can be [transient], which means they are NOT part of the undo/redo history.
 * An automatic syntax highlighter would use transient ranges.
 *
 * Ranges which are NOT transient will be stored in the undo/redo history, and should be used when the
 * highlight is integral to the document. For example, if you want to make a word bold, then it would not
 * be transient.
 *
 * When text is added immediately after a range, [expand] determines if the range should be extended.
 *
 * [owner] can be any object of your choosing (or null), and exists solely so that it is easy to remove (or adjust)
 * all of the ranges owned by a single object without having to keep your own copy of the ranges.
 * For example, a syntax highlighter would set the owner, and then whenever the document is re-highlighted, the
 * existing ranges can be deleted via [TextDocument.removeHighlightRanges].
 */
class HighlightRange(
        internal var from: TextDocument.Position,
        internal var to: TextDocument.Position,
        val highlight: Highlight,
        val transient: Boolean = true,
        val expand: Boolean = false,
        val owner: Any? = null
) {
    fun overlaps(other: HighlightRange): Boolean {
        return to > other.from && from < other.to
    }
}
