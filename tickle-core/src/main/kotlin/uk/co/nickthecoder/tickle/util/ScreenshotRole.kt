package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.AbstractRole
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.TickleErrorHandler
import uk.co.nickthecoder.tickle.info
import java.io.File

/**
 * Takes a screenshot as soon as the scene begins, saves it to a file.
 * The actor is then killed.
 *
 * I wrote this, to make it easy to create screenshots and thumbnails of the game.
 * The thumbnails will be used by the Lau
 */
class ScreenshotRole : AbstractRole() {

    @Attribute
    var filename: String = "screenshot.png"

    /**
     * Should we overwrite the file if it already exists?
     */
    @Attribute
    var overwrite = false

    /**
     * The width of the thumbnail, or 0 for a full-size screenshot
     */
    @Attribute
    var thumbnailWidth = 0

    // Take the screenshot on the Nth tick(), so that the window has been drawn.
    @Attribute
    var delayInTicks = 10

    private var tickCount = 0

    override fun begin() {
        actor.hide()
        if (overwrite || !file().exists()) {
            val gi = Game.instance.resources.gameInfo
            // We *are* going to take a screenshot, so change to the "default" window size,
            // This ensures that the aspect ratio is normal, and
            // the image is not HUGE (e.g. on a high DPI display).
            Game.instance.window.maximise(false)
            Game.instance.window.resize(gi.width, gi.height)
        }
    }

    override fun tick() {
        tickCount++
        if (tickCount >= delayInTicks) {
            val file = file()
            if (overwrite || !file.exists()) {
                info("Saving $file")
                val window = Game.instance.window
                if (thumbnailWidth == 0) {
                    Screenshot.save(window, file)
                } else {
                    Screenshot.save(window, file, thumbnailWidth.toDouble() / window.width)
                }
            }

            actor.die()
        }
    }

    fun file() : File {
        val file = File(filename)
        if (file.isAbsolute || filename.contains("..")) {
            actor.die()
            throw IllegalArgumentException("Only relative filenames are allowed for security reasons")
        }
        // The file is relative to the game's folder, not the current directory.
        return File(Game.instance.resources.resourceDirectory, filename)
    }
}
