package uk.co.nickthecoder.tickle.action

import uk.co.nickthecoder.tickle.Actor

/**
 * Calls [Actor.event] once per frame, each time with a difference event name.
 *
 * Primarily designed to form animations. This assumes that each of [eventNames] is a Pose event.
 * i.e. the [Actor]'s costume has events with different poses for each of the [eventNames].
 *
 * For example, suppose we have events called "eyesLeft" "eyesCenter" and "eyesRight", then
 * we can glance in both directions using :
 *
 *      glanceAction = EventsAction( actor, "eyesLeft", "eyesCenter", "eyesRight", "eyesCenter" )
 *
 * We could do this only occasionally using :
 *
 *      ( Delay(10) then glanceAction ).forever()
 */
class EventsAction(

    val actor: Actor,
    val nFrames: Int,
    vararg val eventNames: String

) : Action {

    constructor(actor: Actor, vararg eventNames: String) : this(actor, 1, * eventNames)

    private var index = 0
    private var n = 0

    override fun begin(): Boolean {
        index = 0
        n = 0
        return eventNames.isEmpty()
    }

    override fun act(): Boolean {
        if (n == 0) {
            actor.event(eventNames[index])
        }
        n++
        if (n >= nFrames) {
            n = 0
            index++
            return index >= eventNames.size
        } else {
            return false
        }
    }

    /**
     * Issue the events forwards and then backwards.
     *
     * @param repeatable If true, then A,B,C will result in A,B,C,B
     * otherwise it results in A,B,C,B,A
     */
    fun pingPong(repeatable: Boolean): EventsAction {
        val events = eventNames.toMutableList()

        for (i in eventNames.size - 2 downTo if (repeatable) 1 else 0) {
            events.add(eventNames[i])
        }

        return EventsAction(actor, nFrames, * events.toTypedArray())
    }
}
