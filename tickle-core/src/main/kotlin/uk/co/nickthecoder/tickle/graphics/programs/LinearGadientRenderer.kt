package uk.co.nickthecoder.tickle.graphics.programs

import org.joml.Matrix3x2f
import org.lwjgl.opengl.GL11.*
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.Vector2

// TODO Nowhere near working! (And not used either).
@Deprecated("Initial development efforts. Not ready for use!")
class LinearGradientProgram(mirrored: Boolean) : ShaderProgram(
    VERTEX,
    REGULAR, // TODO if (mirrored) MIRRORED else REGULAR,
    2
) {
    val fromLocation = getUniformLocation("from")
    val toLocation = getUniformLocation("to")
    val repeatLocation = getUniformLocation("repeat")

    fun setup(texture: Texture, from: Vector2, to: Vector2, repeat: Float, modelMatrix: Matrix3x2f? = null) {
        enablePosition()
        setUniform(fromLocation, from)
        setUniform(toLocation, to)
        setUniform(repeatLocation, repeat)
        setModelMatrix(modelMatrix)
        texture.bind()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    }

    fun draw(points: List<IVector2>) {
        requiredVertices(points.size)
        for (point in points) {
            Renderer.floatBuffer.put(point.x).put(point.y)
        }
    }

    companion object {

        private val VERTEX = """
            #version 120

            attribute vec2 position;
            
            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;

            uniform vec2 from; // Gradient start point
            uniform vec2 to; // Gradient end point

            varying vec2 fromFrag; // `from` converted to (-1..1) for the fragment shader
            varying vec2 toFrag; // `to` convert to (-1..1) for the fragment shader
            varying vec2 pos;

            void main() {
                mat3 matrix = viewMatrix * modelMatrix;
                fromFrag = matrix * vec3( from, 1.0 ).xy;
                toFrag = matrix * vec3( to, 1.0 ).xy;
                pos = matrix * vec3(position, 1.0).xy;
                gl_Position = vec4( pos, 1.0, 1.0 );
            }
            
            """.trimIndent()

        private val REGULAR = """
            #version 120
            
            uniform float repeat;
           
            varying vec2 fromFrag;
            varying vec2 toFrag;
            varying vec2 pos;

            uniform sampler2D gradient;
                                 
            void main() {
                //float t = 
                //gl_FragColor = texture2D( gradient, vec2(tRepeated, 0.5));
                gl_FragColor = pos.x;
            }
            """.trimIndent()

        private val MIRRORED = """
            
            """.trimIndent()

    }

}
