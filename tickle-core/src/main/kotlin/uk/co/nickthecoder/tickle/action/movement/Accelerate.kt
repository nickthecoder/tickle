/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.Clock
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Calculates the time since the previous [act], and increases [velocity] by [acceleration] * deltaTime.
 *
 * Despite this being called "Accelerate", it can be used for other purposes.
 * For example, if you pass in a position and velocity (as the 2nd argument), then it will
 * be performing a movement at constant velocity.
 */
open class Accelerate(
    val velocity: Vector2,
    val acceleration: Vector2

) : Action {

    private var previousTime = Clock.seconds

    override fun begin(): Boolean {
        previousTime = Clock.seconds
        return super.begin()
    }

    override fun act(): Boolean {
        val diff = (Clock.seconds - previousTime).toFloat()
        velocity.x += acceleration.x * diff
        velocity.y += acceleration.y * diff
        previousTime = Clock.seconds
        return false
    }
}
