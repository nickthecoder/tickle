/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.events.KeyEvent
import uk.co.nickthecoder.tickle.events.MouseButtonListener
import uk.co.nickthecoder.tickle.events.MouseEvent
import uk.co.nickthecoder.tickle.events.ResizeEvent
import uk.co.nickthecoder.tickle.util.AutoFlushPreferences
import java.util.prefs.Preferences

/**
 * Looks at the big picture, and shouldn't be involved in the minor details.
 * Your Producer class (if you have one) should be named after your game. e.g. "SpaceInvaders"
 * Many games don't need a Producer, and therefore use the class [NoProducer].
 *
 * Example responsibilities of a Producer :
 * * Perform actions when the game first starts and when it ends.
 * * Add additional auto-generated resources to the Resources class at the beginning of the game.
 * * To hold information carried forward from one scene to the next, such as score and lives remaining.
 *
 * See also [Director].
 */
interface Producer : MouseButtonListener {

    fun begin()

    /**
     * Called soon after a scene's resource have been loaded, when a new scene starts.
     * Typically you only override this when you need some initialisation to occur
     * BEFORE [sceneBegin].
     */
    fun sceneLoaded()

    /**
     * Called after [sceneLoaded] but before [Director.begin].
     */
    fun sceneBegin()

    /**
     * Call after [sceneBegin], but before [Director.activated].
     * Typically you only override this when you need some initialisation to occur
     * AFTER [sceneBegin].
     */
    fun sceneActivated()

    fun preTick()

    fun tick()

    fun postTick()

    fun sceneEnding()

    fun sceneEnded()

    fun end()

    fun onKey(event: KeyEvent)

    /**
     * Adjust the view's positions, and scales, as well as the actors positions, so that everything is still in
     * the "right" place when the window is resized.
     * Automatically called when the scene starts, and you should also call it from [onResize] if you game's
     * window is resizable (See GameInfo.resizable: In the `Editor`, see the `Game Info` item in the `Resources Tree`).
     */
    fun layout()

    /**
     * Called when the window is resized.
     */
    fun onResize(event: ResizeEvent)

    fun message(message: String)

    /**
     * Gets the preferences node, for persisting data, such as high-scores, levels unlocked etc.
     * The default node uses Tickle's package name and the id of your game (as defined in [GameInfo]).
     */
    fun preferencesRoot(): AutoFlushPreferences {
        return AutoFlushPreferences(Preferences.userNodeForPackage(Game::class.java).node(Game.instance.resources.gameInfo.id))
    }

}

/**
 * This is a convenient base class, as it contains default implementations for all methods defined in [Producer].
 * All are empty, except for [layout] and [onResize].
 */
abstract class AbstractProducer : Producer {

    override fun sceneLoaded() {}

    override fun begin() {}

    override fun sceneBegin() {}

    override fun sceneActivated() {}

    override fun preTick() {}

    override fun tick() {}

    override fun postTick() {}

    override fun sceneEnding() {}

    override fun sceneEnded() {}

    override fun end() {}

    override fun onKey(event: KeyEvent) {}

    override fun onMouseButton(event: MouseEvent) {}

    override fun onResize(event: ResizeEvent) {}

    override fun layout() {}

    override fun message(message: String) {}

}

/**
 * Used by games which need no special Producer.
 */
class NoProducer : AbstractProducer() {
    override fun layout() {
        Game.instance.scene.arrangeViews()
    }
}

