package uk.co.nickthecoder.tickle.action

import uk.co.nickthecoder.tickle.Actor

class ShowActor( val actor : Actor) : Action {
    override fun begin(): Boolean {
        actor.show()
        return true
    }
    override fun act() = true
}

class HideActor( val actor : Actor) : Action {
    override fun begin(): Boolean {
        actor.hide()
        return true
    }
    override fun act() = true
}