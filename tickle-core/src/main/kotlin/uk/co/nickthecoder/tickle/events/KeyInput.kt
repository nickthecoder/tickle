/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.events

import org.lwjgl.glfw.GLFW.GLFW_PRESS
import org.lwjgl.glfw.GLFW.glfwGetKey
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.graphics.Window


class KeyInput(
        val key: Key,
        val state: ButtonState = ButtonState.PRESSED,
        shift: Boolean? = null,
        control: Boolean? = null,
        alt: Boolean? = null

) : ModifierInput(shift, control, alt) {

    /**
     * NOTE, this ignores [state].
     */
    override fun isPressed(): Boolean {
        val windowHandle = Game.instance.window.handle
        if (glfwGetKey(windowHandle, key.code) != GLFW_PRESS) return false
        return modifierMatches()
    }

    override fun matches(event: KeyEvent): Boolean {
        if (key != event.key) return false
        if (state == event.state || (state == ButtonState.REPEATED && event.state != ButtonState.RELEASED)) {
            return modifierMatches(event.mods)
        }
        return false
    }

    override fun matches(event: MouseEvent): Boolean {
        return false
    }

    override fun toString() = "KeyInput key=$key state=$state"

}
