/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import kotlin.math.roundToInt

/**
 * A rectangle using Ints.
 * Note, the right and bottom values are EXCLUSIVE, so width = right - left
 *
 * The Y axis points up, so top > bottom
 *
 * For a rectangle with the y axis pointing down, use [YDownRect] instead.
 */
data class RectInt(
        var left: Int,
        var bottom: Int,
        var right: Int,
        var top: Int) {

    val width
        get() = right - left
    val height
        get() = top - bottom

    constructor(other: RectInt) : this(other.left, other.bottom, other.right, other.top)

    fun set(other: RectInt) {
        left = other.left
        bottom = other.bottom
        right = other.right
        top = other.top
    }

    operator fun plusAssign(by: Vector2) {
        left += by.x.roundToInt()
        right += by.x.roundToInt()
        bottom += by.y.roundToInt()
        top += by.y.roundToInt()
    }

    operator fun minusAssign(by: Vector2) {
        left -= by.x.roundToInt()
        right -= by.x.roundToInt()
        bottom -= by.y.roundToInt()
        top -= by.y.roundToInt()
    }

    fun aspectRatio() = width.toDouble() / height

    override fun toString(): String = "($left,$bottom , $right,$top)"
}
