/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.scripts

import uk.co.nickthecoder.tickle.info
import uk.co.nickthecoder.tickle.severe
import java.io.File

/**
 * Holds the set of script languages used by your game.
 * Note, this only supports languages that can be dynamically compiled to JVM classes.
 * So, for example, it does not support interpreted javascript.
 *
 * If your game only use compiled Kotlin/Java, then there is no need for ScriptManager.
 */
object ScriptManager {

    /**
     * This allows Tickle to rename classes, and the old names still works.
     */
    val renamedClasses = mutableMapOf(
            "uk.co.nickthecoder.tickle.resources.NoStageConstraint" to "uk.co.nickthecoder.tickle.editor.resources.NoStageConstraint",
            "uk.co.nickthecoder.tickle.resources.GridConstraint" to "uk.co.nickthecoder.tickle.editor.resources.GridConstraint"
    )

    private val languages = mutableMapOf<String, Language>()

    private var classpath: File? = null

    fun languages() = languages.values

    fun findLanguage(fileExtension: String) = languages[fileExtension]

    fun registerLanguage(languageClassName: String) {
        try {
            val klass = javaClass.classLoader.loadClass(languageClassName)
            register(klass.getDeclaredConstructor().newInstance() as Language)
        } catch (e: Exception) {
            severe(e)
        }
    }

    fun register(language: Language) {
        if (!languages.containsKey(language.fileExtension)) {
            info("Registering language $language")
            languages[language.fileExtension] = language
            classpath?.let { language.setClasspath(it) }
        }
    }

    /**
     * Note, only one directory is currently supported. All script classes must be only
     * within that directory.
     * Packages are currently NOT supported, so there are no subdirectories.
     */
    fun setClasspath(directory: File) {
        info("ScriptManager path = $directory")
        classpath = directory
        languages.values.forEach { it.setClasspath(directory) }
        reload()
    }

    fun reload() {
        languages.values.forEach { it.reload() }
    }

    fun load(file: File) {
        languages[file.extension]?.addScript(file)
    }

    fun subTypes(type: Class<*>): MutableList<Class<*>> {
        val results = mutableListOf<Class<*>>()

        languages.values.forEach { language ->
            results.addAll(language.subTypes(type))
        }
        return results
    }

    /**
     * If the class has been renamed, then returns the new name for the class.
     * Otherwise return the original [className].
     */
    fun renamedClass(className: String) = renamedClasses[className] ?: className

    fun scriptFile(className: String): File? {
        languages.values.forEach {
            val file = it.scriptFile(className)
            if (file.exists()) return file
        }
        return null
    }

    fun classForName(className: String): Class<*> {
        val name = renamedClass(className)

        try {
            return Class.forName(name)
        } catch (e: ClassNotFoundException) {
            languages.values.forEach { language ->
                language.classForName(name)?.let { return it }
            }
        }
        throw ClassNotFoundException(name)
    }

    fun nameForClass(klass: Class<*>): String {
        for (language in languages.values) {
            language.nameForClass(klass)?.let { return it }
        }
        return klass.name
    }
}
