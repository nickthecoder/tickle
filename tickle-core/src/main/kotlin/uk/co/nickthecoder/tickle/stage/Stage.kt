/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.stage

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Role
import uk.co.nickthecoder.tickle.physics.TickleWorld
import uk.co.nickthecoder.tickle.util.Vector2

interface Stage {

    val views: List<StageView>

    val actors: Set<Actor>

    var world: TickleWorld?

    /**
     * Has [begin] been called on this stage?
     * [Actor] uses this, to know if setting its [Role] should call [Role.begin] etc.
     * If the stage hasn't begun, then it should wait for the stage (and all of its actors)
     * to begin instead.
     */
    fun hasBegun(): Boolean

    fun begin()

    fun activated()

    fun end()

    /**
     * Calls tick on all of the Actors' Roles on this stage. Note that if the Stage has a TickleWorld, then do NOT
     * call world.tick from within this method. Instead, Scene will call stage.world.tick for all stages after
     * ALL of the stage's tick methods have been called. i.e. all the Roles' tick methods are called, then all
     * lf the worlds' tick methods.
     */
    fun tick()

    fun add(actor: Actor)

    fun remove(actor: Actor)

    fun addView(view: StageView)

    fun firstView(): StageView? = views.firstOrNull()

    fun findActorById(id: Int): Actor? {
        return actors.firstOrNull { it.id == id }
    }

    fun findActorByName(name: String): Actor? {
        return actors.firstOrNull { it.name == name }
    }

    /**
     * Note, the default implementation is slow, as it iterates over all actors. If you call this often and/or the stage
     * has hundreds of Actors, consider creating a subclass, optimised using a Neighbourhood.
     */
    fun findActorsAt(point: Vector2): Iterable<Actor> {
        return firstView()?.findActorsAt(point) ?: emptyList()
    }

    /**
     * Note, the default implementation is slow, as it iterates over all actors. If you call this often and/or the stage
     * has hundreds of Actors, consider creating a subclass, optimised using a Neighbourhood.
     */
    fun findActorAt(point: Vector2): Actor? {
        return findActorsAt(point).firstOrNull()
    }

    /**
     * Note, the default implementation is slow, as it iterates over all actors. If you call this often and/or the stage
     * has hundreds of Actors, consider creating a subclass, with a map of the actors by their class.
     * [OptimisedStage] does just that!
     *
     * Another way to optimise things is to create stage(s) for specific roles.
     * For example, create a stage for all things which need to check for "overlapping" because they are deadly,
     * and another things for "trivial" roles, such as "explosions". The trivial stage may have thousands of
     * actors, none of which need to be checked.
     * But this solution adds extra constraints on the z-order (explosions will either be above or below
     * ALL of the non-trivial actors).
     */
    fun <T : Role> findRolesByClass(type: Class<T>): List<T> {
        @Suppress("UNCHECKED_CAST")
        return actors.filter { type.isInstance(it.role) }.map { it.role as T }
    }

    fun <T : Role> findRoleByClass(type: Class<T>): T? {
        @Suppress("UNCHECKED_CAST")
        return actors.firstOrNull { type.isInstance(it.role) }?.role as T?
    }

    /**
     * Finds all roles at the given point of a given type.
     *
     * Note, the default implementation is slow, as it iterates over all actors. If you call this often and/or the stage
     * has hundreds of Actors, consider creating a subclass, optimised using a Neighbourhood.
     */
    fun <T : Role> findRolesAt(type: Class<T>, point: Vector2): Iterable<T> {
        return firstView()?.findRolesAt(type, point) ?: emptyList()
    }

    fun <T : Role> findRoleAt(type: Class<T>, point: Vector2): T? {
        return findRolesAt(type, point).firstOrNull()
    }
}

/**
 * An idiomatic version of findRoles(Class<T>)
 */
inline fun <reified T : Role> Stage.findRoles(): Iterable<T> {
    return findRolesByClass(T::class.java)
}

/**
 * An idiomatic version of findRole(Class<T>)
 */
inline fun <reified T : Role> Stage.findRole(): T? {
    return findRoles<T>().firstOrNull()
}

/**
 * An idiomatic version of findRolesAt(Class<T>, Vector2)
 */
inline fun <reified T : Role> Stage.findRolesAt(point: Vector2): List<T> {
    return findRoles<T>().filter { it.actor.touching(point) }
}

/**
 * An idiomatic version of findRoleAt(Class<T>, Vector2)
 */
inline fun <reified T : Role> Stage.findRoleAt(point: Vector2): T? {
    return findRolesAt<T>(point).firstOrNull()
}
