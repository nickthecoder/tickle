/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class Attribute(
        val about: String = "",
        val order: Int = 1,
        val scale: Float = 1f,
        val rows: Int = 1, // For Strings only
        val hasAlpha: Boolean = true, // For Color only
        val isSceneName: Boolean = false, // For Strings only. Uses a SceneNameParameter instead of StringParameter
        val isActorName: Boolean = false, // For Strings only. Uses ActorNameParameter instead of StringParameter
        val isRelative: Boolean = false, // For Vector2 only. The following add extra drag handles in the SceneEditor.
        val isPosition: Boolean = false, // For Vector2 only
        val isDirection: Boolean = false // For Angle only
)

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class CostumeAttribute(
        val about: String = "",
        val order: Int = 1,
        val rows: Int = 1, // For Strings only
        val hasAlpha: Boolean = true, // For Color attributes only
        val isSceneName : Boolean = false // For Strings only. Uses a SceneNameParameter instead of StringParameter
)
