package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.ActionRole
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.Delay
import uk.co.nickthecoder.tickle.action.Once

class FramesPerSecond : ActionRole() {

    override fun createAction(): Action {
        return Once {
            actor.textAppearance?.text =
                "${Game.instance.gameLoop.actualFPS().toInt()} (${Game.instance.gameLoop.droppedFrames})"
        }.then(Delay(1.0)).forever()
    }

}
