package uk.co.nickthecoder.tickle.stage

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.util.Vector2

interface Handle {

    val name: String

    val actor: Actor

    val hovering: Boolean

    val dragTip: String

    fun draw(viewportPosition: Vector2, highlight: Boolean)

    fun update()

    /**
     * The world X coordinate of the handle
     */
    fun x(): Float

    /**
     * The world Y coordinate of the handle
     */
    fun y(): Float

    fun isNear(x: Float, y: Float): Boolean

    fun hover(x: Float, y: Float): Boolean

    fun moveTo(pos: Vector2, symmetric: Boolean, snap: Boolean)

    fun end(pos: Vector2, symmetric: Boolean, snap: Boolean) {}
}


abstract class AbstractHandle(
    override val name: String,
    override val actor: Actor,
    override val dragTip: String,
) : Handle {

    override var hovering: Boolean = false
        set(v) {
            if (v != field) {
                field = v
            }
        }

    override fun hover(x: Float, y: Float): Boolean {
        hovering = isNear(x, y)
        return hovering
    }

    override fun isNear(x: Float, y: Float): Boolean {
        val dx = x - x()
        val dy = y - y()
        return dx * dx + dy * dy < 36 // 6 pixels
    }
}

/*


inner class DirectionArrow(
    name: String,
    actorResource: DesignActorResource,
    val data: DesignAttributeData,
    distance: Int
)

    : Arrow(name, actorResource, distance) {

    val parameter = if (data.parameter is AngleParameter)
        (data.parameter as AngleParameter).degreesP
    else
        data.parameter as DoubleParameter

    override fun get() = parameter.value ?: 0.0

    override fun set(degrees: Double) {
        sceneEditor.history.makeChange(ChangeDoubleParameter(actorResource, parameter, degrees))
    }

    override fun moveTo(x: Float, y: Float, snap: Boolean) {

        val dx = x - actorResource.x
        val dy = y - actorResource.y

        val atan = Math.atan2(dy.toDouble(), dx.toDouble())
        var angle = if (atan < 0) atan + Math.PI * 2 else atan

        angle -= angle.rem(Math.toRadians(if (snap) 15.0 else 1.0))
        set(Math.toDegrees(angle))
    }


    override fun drawArrowHandle(length: Float) {
        lineWithHandle(length) { drawDiamondHandle() }
    }
}

inner class PolarArrow(name: String, val actorResource: DesignActorResource, val data: DesignAttributeData)

    : AbstractDragHandle(name) {

    val parameter = data.parameter!! as PolarParameter

    fun set(angleRadians: Double, magnitude: Float) {
        sceneEditor.history.makeChange(
            ChangePolarParameter(
                actorResource,
                parameter,
                Math.toDegrees(angleRadians),
                magnitude
            )
        )
    }

    override fun x() = actorResource.x + parameter.value.vector().x * data.scale
    override fun y() = actorResource.y + parameter.value.vector().y * data.scale

    override fun moveTo(x: Float, y: Float, snap: Boolean) {

        val dx = x - actorResource.x
        val dy = y - actorResource.y

        val atan = Math.atan2(dy.toDouble(), dx.toDouble())
        var angle = if (atan < 0) atan + Math.PI * 2 else atan

        angle -= angle.rem(Math.toRadians(if (snap) 15.0 else 1.0))
        var mag = Math.sqrt((dx * dx + dy * dy).toDouble()) / data.scale
        if (snap) {
            mag = Math.floor(mag)
        }

        set(angle, mag.toFloat())
    }


    override fun draw() {
        with(canvas.graphicsContext2D) {
            save()
            translate(actorResource.x, actorResource.y)
            val length = parameter.magnitude!! * data.scale

            rotate(parameter.value.angle.degrees)
            drawOutlined(handleColor(hovering)) { lineWithHandle(length) }
            restore()
        }
    }

}

open inner class AbsoluteHandle(name: String, val actorResource: DesignActorResource, val data: DesignAttributeData)

    : AbstractDragHandle(name) {

    val parameter = data.parameter!! as Vector2Parameter

    fun set(x: Float, y: Float) {
        sceneEditor.history.makeChange(ChangeVector2Parameter(actorResource, parameter, x, y))
    }

    override fun x() = parameter.x!!
    override fun y() = parameter.y!!

    override fun moveTo(x: Float, y: Float, snap: Boolean) {
        set(x, y)
    }

    override fun draw() {

        with(canvas.graphicsContext2D) {
            save()
            translate(parameter.x!!, parameter.y!!)
            drawOutlined(handleColor(hovering)) { drawSquareHandle() }
            restore()
        }
    }

}

inner class RelativeHandle(name: String, actorResource: DesignActorResource, data: DesignAttributeData)

    : AbsoluteHandle(name, actorResource, data) {

    override fun x() = actorResource.x + super.x() * data.scale
    override fun y() = actorResource.y + super.y() * data.scale

    override fun moveTo(x: Float, y: Float, snap: Boolean) {

        var dx = (x - actorResource.x) / data.scale
        var dy = (y - actorResource.y) / data.scale

        if (snap) {
            dx = Math.floor(dx.toDouble()).toFloat()
            dy = Math.floor(dy.toDouble()).toFloat()
        }

        set(dx, dy)
    }


    override fun draw() {

        with(canvas.graphicsContext2D) {
            save()
            translate(actorResource.x + parameter.x!! * data.scale, actorResource.y + parameter.y!! * data.scale)
            drawOutlined(handleColor(hovering)) { drawDiamondHandle() }
            restore()
        }
    }

}
*/
