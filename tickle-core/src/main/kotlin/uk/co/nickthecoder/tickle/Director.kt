/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.events.KeyEvent
import uk.co.nickthecoder.tickle.events.MouseButtonListener
import uk.co.nickthecoder.tickle.events.MouseEvent
import uk.co.nickthecoder.tickle.events.ResizeEvent
import uk.co.nickthecoder.tickle.physics.PhysicsInfo
import uk.co.nickthecoder.tickle.physics.TickleWorld
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.util.MessageButton

/**
 * Looks after a single Scene. A game will typically have at least two Directors, one to handle the menu or splash
 * screen, and another for playing the game. They are typically called "Menu" and "Play".
 *
 * A typical game has many "scenes" (AKA "levels"), and a Director is created when a new scene is loaded,
 * and is thrown away when the scene ends.
 * Therefore they cannot hold information that is carried over from one scene to the next (e.g. score,
 * lives remaining). Such information must be held on Producer instead.
 *
 * Typical uses of a Director :
 *
 * - Decide when the level has been completed (or failed), and move to the next level, or return to the main menu.
 * - Listen for a "quit" key, to end the game, and return to the main menu.
 * - Act as a single point of communication between the Actors' Roles
 *
 * There are many more things that Director can do!
 */
interface Director : MouseButtonListener {

    /**
     * Called shortly after [Game.startScene], before [begin].
     */
    fun sceneLoaded()

    /**
     * Called after [sceneLoaded] and before [activated], when we are starting a new scene
     * ([Game.startScene]).
     */
    fun begin()

    /**
     * Called after [begin] and before [activated] when your game uses the Physics Engine.
     */
    fun createWorlds(resources: Resources)

    /**
     * Called after [begin] when we are starting a new scene ([Game.startScene])
     */
    fun activated()

    /**
     * Called when a scene is being loaded, as well as when the window is resized.
     *
     * Sometimes, a [Director] will want to ensure the views are "centered" on the game's main character,
     * and when the window is resized, this may need to be recalculated.
     */
    fun layout()

    /**
     * Called once per frame, before [tick]. Rarely used.
     */
    fun preTick()

    /**
     * Called once per frame.
     */
    fun tick()

    /**
     * Called once per frame after [tick]. Rarely used.
     */
    fun postTick()

    /**
     * Called when starting a new scene, and also when quitting the game.
     * Use this to "tidy up".
     *
     * At this point, the actors, stages and views of the old scene still exist.
     */
    fun ending()

    /**
     * Called when starting a new scene, and also when quitting the game.
     * Use this to "tidy up".
     *
     * At this point, the actors, stages and views have all been removed.
     */
    fun ended()

    fun onKey(event: KeyEvent)

    fun onResize(event: ResizeEvent)

    /**
     * This feature was "inspired" by Scratch - One game object can broadcast a message, which others can act on.
     *
     * You might like to use [MessageButton] to send a message to a [Director]/[Producer],
     * as a simple way to perform an action from a menu, without needing to create a special Role for the task.
     */
    fun message(message: String)

    companion object {

        fun createDirector(directorString: String): Director {
            try {
                val klass = ScriptManager.classForName(directorString)
                val newDirector = klass.getDeclaredConstructor().newInstance()
                if (newDirector is Director) {
                    return newDirector
                } else {
                    severe("'$directorString' is not a type of Director")
                }
            } catch (e: Exception) {
                severe("Failed to create a Director from : '$directorString'")
                e.printStackTrace()
            }
            return NoDirector()
        }
    }
}

abstract class AbstractDirector : Director {

    protected fun createWorld(phy: PhysicsInfo): TickleWorld {
        return TickleWorld(
                gravity = phy.gravity,
                scale = phy.scale.toFloat(),
                timeStep = 1.0f / Game.instance.gameLoop.expectedFrameRate().toFloat(),
                velocityIterations = phy.velocityIterations,
                positionIterations = phy.positionIterations)
    }

    /**
     * The default behaviour is to create a single shared world attached to every Stage,
     * if [GameInfo.physicsInfo] == true.
     * Otherwise no [TickleWorld]s are created.
     *
     * It is quite common to override this method, and create a single world on just one stage.
     */
    override fun createWorlds(resources:Resources) {
        if (resources.gameInfo.physicsEngine) {
            val world = createWorld(resources.gameInfo.physicsInfo)
            Game.instance.scene.stages.values.forEach { stage ->
                stage.world = world
            }
        }
    }

    /**
     * The default implementation does nothing.
     */
    override fun sceneLoaded() {}

    /**
     * The default implementation does nothing.
     */
    override fun begin() {}

    /**
     * The default implementation does nothing.
     */
    override fun layout() {}

    /**
     * The default implementation does nothing.
     */
    override fun activated() {}

    /**
     * The default implementation does nothing
     */
    override fun ending() {}

    /**
     * The default implementation does nothing
     */
    override fun ended() {}

    /**
     * The default implementation does nothing.
     */
    override fun onKey(event: KeyEvent) {}

    /**
     * The default implementation does nothing
     */
    override fun onResize(event: ResizeEvent) {}

    /**
     * The default implementation does nothing.
     */
    override fun onMouseButton(event: MouseEvent) {}

    /**
     * The default implementation does nothing.
     */
    override fun preTick() {}

    /**
     * The default implementation does nothing.
     */
    override fun tick() {}

    /**
     * The default implementation does nothing.
     */
    override fun postTick() {}

    /**
     * The default implementation does nothing.
     */
    override fun message(message: String) {}
}

class NoDirector : AbstractDirector()
