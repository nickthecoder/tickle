package uk.co.nickthecoder.tickle.graphics

import org.lwjgl.opengl.GL11.GL_RGBA
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.misc.lerp
import java.lang.IllegalArgumentException
import java.nio.ByteBuffer

/**
 * A GradientStop is a [color] and a position [t] of the stop (in the range 0..1).
 */
data class GradientStop(var t: Float, val color: Color) : Comparable<GradientStop> {
    override fun compareTo(other: GradientStop): Int {
        return t.compareTo(other.t)
    }

    override fun toString() = "$t -> $color"
}

/**
 * Defines the colors and positions of the "stops".
 *
 * The end result is a [texture] size ([resolution]x1) pixels.
 */
class Gradient(
    from: Color = Color.white(),
    to: Color = from,
    val ease: Ease = LinearEase.instance,
    val resolution: Int = 256
) {

    private var cachedTexture: Texture? = null
    private val stops = mutableListOf<GradientStop>()
    private var dirty = true
        set(v) {
            if (!v) {
                cachedTexture?.destroy()
                cachedTexture = null
            }
            field = v
        }

    val texture: Texture
        get() {
            cachedTexture?.let { return it }
            dirty = false
            return createTexture(stops.toTypedArray(), ease, resolution).also { cachedTexture = it }
        }

    init {
        stops.add(GradientStop(0f, from.copy()))
        stops.add(GradientStop(1f, to.copy()))
    }

    operator fun get(index: Int): GradientStop {
        val stop = stops[index]
        return GradientStop(stop.t, stop.color.copy())
    }

    fun size() = stops.size

    fun addStop(t: Float, color: Color) {
        stops.add(1, GradientStop(t, color))
        stops.sort()
        dirty = true
    }

    fun setColor(index: Int, color: Color) {
        stops[index].color.set(color)
        dirty = true
    }

    fun setT(index: Int, t: Float) {
        if (index == 0 || index == stops.size - 1) throw IllegalArgumentException("Cannot change the t value of the first or last stop")
        stops[index].t = t
        stops.sort()
        dirty = true
    }

    override fun toString() = "Gradient : $stops"

    companion object {

        /**
         * Create a Texture size ([resolution]x1) to be used as a [Gradient].
         */
        fun createTexture(
            stops: Array<GradientStop>,
            ease: Ease = LinearEase.instance,
            resolution: Int = 256
        ): Texture {
            if (stops.size < 2) throw IllegalArgumentException("At least 2 stops are needed for a Gradient")
            if (stops.first().t != 0f) throw IllegalArgumentException("The first stop must be at t=0")
            if (stops.last().t != 1f) throw IllegalArgumentException("The last stop must be at t=1")

            val widthMinus1 = (resolution - 1).toFloat()
            val buffer = ByteBuffer.allocateDirect((resolution) * 4)

            var stopIndex = 1
            var fromStop = stops[0]
            var toStop = stops[1]
            var fromT = fromStop.t
            var toT = toStop.t
            var stopWidth = toStop.t - fromT

            for (x in 0 until resolution) {
                val t = x / widthMinus1

                if (t > toT) {
                    stopIndex++
                    if (stopIndex < stops.size) {
                        fromStop = toStop
                        toStop = stops[stopIndex]
                        fromT = fromStop.t
                        toT = toStop.t
                        stopWidth = toStop.t - fromT
                    }
                }
                val localT = ease.ease((t - fromT) / stopWidth)

                val color = lerp(fromStop.color, toStop.color, localT)

                buffer.put(Color.channelAsByte(color.red))
                    .put(Color.channelAsByte(color.green))
                    .put(Color.channelAsByte(color.blue))
                    .put(Color.channelAsByte(color.alpha))
            }

            buffer.flip()
            return Texture(resolution, 1, GL_RGBA, buffer)
        }

    }
}
