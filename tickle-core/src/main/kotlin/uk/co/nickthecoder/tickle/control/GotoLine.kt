package uk.co.nickthecoder.tickle.control

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.Costume
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.TextAppearance
import uk.co.nickthecoder.tickle.events.KeyEvent
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.stage.GameStage
import uk.co.nickthecoder.tickle.stage.ZOrderStageView
import uk.co.nickthecoder.tickle.util.Rect
import uk.co.nickthecoder.tickle.util.RectInt

class GotoLine(var textArea: TextArea)

    : ZOrderStageView() {

    val lineNumber = object : SingleLineTextArea() {
        override fun onKey(event: KeyEvent) {

            if (enter.matches(event)) {
                event.consume()
                val txt = text
                val comma = txt.indexOf(",")

                val line = if (comma > 0) {
                    txt.substring(0, comma).toIntOrNull() ?: return
                } else {
                    txt.toIntOrNull()
                }

                val column = if (comma > 0) {
                    txt.substring(comma + 1).toIntOrNull()
                } else {
                    1
                }

                if (line != null && column != null) {
                    textArea.caretPosition = TextDocument.Position(line - 1, column - 1)
                    textArea.ensureCaretVisible()
                    textArea.requestFocus()
                    close()
                }
            }

            if (escape.matches(event)) {
                event.consume()
                textArea.requestFocus()
                close()
            }

            if (! event.isConsumed()) {
                super.onKey(event)
            }
        }
    }

    var backgroundColor = Color.create("#888888ee")

    init {
        val width = Game.instance.window.width

        stage = GameStage()
        zOrder = 99999

        val textStyle = TextStyle(textArea.fontResource!!, TextHAlignment.LEFT, TextVAlignment.BOTTOM, Color.black())

        val labelA = Actor(Costume(Game.instance.resources)).apply {
            appearance = TextAppearance(this, "Line : ", textStyle)
            x = 10f
        }

        val lineNumberA = Actor(Costume(Game.instance.resources)).apply {
            appearance = TextAppearance(this, "", textStyle)
            x = 100f
            y = 3f
        }

        stage.add(labelA)
        stage.add(lineNumberA)

        Game.instance.scene.addStage(stageAndViewName, stage)
        Game.instance.scene.addView(stageAndViewName, this)

        lineNumber.width = 100f
        lineNumberA.role = lineNumber

        val height : Float = lineNumber.height + 6f
        rect = RectInt(0, 0, width, height.toInt())

        labelA.y = (height - labelA.appearance.height()) / 2 // Center vertically.
        lineNumber.requestFocus()
    }

    fun close() {
        stage.end()
        Game.instance.scene.removeStage(stageAndViewName)
        Game.instance.scene.removeView(stageAndViewName)
    }

    override fun drawActors(renderer: Renderer) {
        //TODO renderer.drawRect(Rect(0f, 0f, rect.width.toFloat(), rect.height.toFloat()), backgroundColor)
        super.drawActors(renderer)
    }

    companion object {
        val stageAndViewName = GotoLine::class.java.name

        fun show(textArea: TextArea) {
            val existing = Game.instance.scene.findStageView(stageAndViewName) as? GotoLine
            if (existing != null) {
                existing.textArea = textArea
                existing.lineNumber.requestFocus()
            } else {
                GotoLine(textArea)
            }
        }
    }
}
