/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.collision

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.TextAppearance
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Tests for [Touching] differently depending on whether the actor is text, or not (a pose appearance).
 *
 * For text, a simple bounding rectangle is used [BoundingRectangleTouching], and for other actors,
 * pixel detection is used [PixelTouching].
 *
 * The reason for difference, is that the expectation for text is different to images. For example,
 * if you click in the valley of a Y, you probably want it to register as a click.
 * However, if you have a Y shaped ship, then clicking in the value, should probably NOT register as a click
 * (maybe there is another actor in that valley that you intended to click).
 *
 * Limitations:
 *
 * This only makes sense when the text is horizontal or vertical. If text is diagonal, then large
 * areas will be "clickable", which probably shouldn't be.
 *
 * When using multi-line text, the lengths of each line are NOT taken into consideration.
 * The touching region is a simple, axis aligned rectangle.
 */
class PixelOrBoundingBoxTouching(val pixel: PixelTouching, val box: BoundingRectangleTouching)
    : Touching {

    override fun touching(actor: Actor, point: Vector2): Boolean {
        return if (actor.appearance is TextAppearance) {
            box.touching(actor, point)
        } else {
            pixel.touching(actor, point)
        }
    }

    companion object {
        val instance = PixelOrBoundingBoxTouching(PixelTouching.instance, BoundingRectangleTouching.instance)
    }

}
