/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.events

import org.lwjgl.glfw.GLFW
import uk.co.nickthecoder.tickle.Game


class MouseInput(
        val mouseButton: Int,
        val state: ButtonState,
        shift: Boolean? = null,
        control: Boolean? = null,
        alt: Boolean? = null

) : ModifierInput(shift, control, alt) {

    /**
     * NOTE, this ignores [state].
     */
    override fun isPressed(): Boolean {
        return GLFW.glfwGetMouseButton(Game.instance.window.handle, mouseButton) == GLFW.GLFW_PRESS && modifierMatches()
    }

    override fun matches(event: KeyEvent): Boolean {
        return false
    }

    override fun matches(event: MouseEvent): Boolean {
        if (mouseButton != event.button || state != event.state) return false
        return modifierMatches(event.mods)
    }

    override fun toString() = "MouseInput key=$mouseButton state=$state"
}
