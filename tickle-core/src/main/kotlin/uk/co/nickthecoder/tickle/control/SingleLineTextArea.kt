package uk.co.nickthecoder.tickle.control

import uk.co.nickthecoder.tickle.TextAppearance
import uk.co.nickthecoder.tickle.events.KeyEvent

/**
 * A [TextArea] with only one line of text. You can use this like you would a TextField, as Tickle
 * doesn't (yet) have a dedicated TextField control.
 */
open class SingleLineTextArea : TextArea() {

    init {
        showLineNumbers = false
        height = 0f
        marginBottom = marginTop
        marginRight = marginLeft
    }

    override fun begin() {
        if (height == 0f) {
            (actor.appearance as? TextAppearance)?.let { ta ->
                height = ta.textStyle.fontResource.fontTexture.lineHeight + marginTop + marginBottom
            }
        }
        super.begin()
    }

    override fun onKey(event: KeyEvent) {

        if (enter.matches(event) || indent.matches(event) || unindent.matches(event) || gotoLine.matches(event)) {
            return
        }

        super.onKey(event)
    }

}
