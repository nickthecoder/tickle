/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.Role

/**
 * [TaggedRole]s can be tagged with arbitrary tag values (an enum is a common choice, but you are free
 * to use any objects).
 *
 * You can then find [TaggedRole]s with particular tags.
 *
 * There are also extra functions which look for roles with matching tags are overlapping
 * any other [Role].
 * For example, you can check if a player is overlapping anything with a `deadly` tag.
 *
 * There is also an option to search for [TaggedRole]s based on their class name.
 * To enable this, you must call [Tags.addByClass]
 *
 * [TagManager.instance] is automatically recreated at the start of every scene.
 * However, if you need multiple TagManager per Scene, then you may want to use your own instance.
 */
class TagManager {

    /**
     * All [TaggedRole]s keyed by their tab [Any] type.
     */
    private val rolesByTag = mutableMapOf<Any, MutableSet<TaggedRole>>()

    /**
     * To speed up looking for [Role]s based on their type.
     * Keyed on the [TaggedRole]'s type.
     */
    private val rolesByClass = mutableMapOf<Class<out TaggedRole>, MutableSet<TaggedRole>>()

    /**
     * All [TaggedRole]s managed by this.
     */
    private val allRoles = mutableSetOf<TaggedRole>()


    /**
     * Returns those roles with the give [tag].
     */
    fun findRoles(tag: Any): Set<TaggedRole> = rolesByTag[tag] ?: emptySet()

    /**
     * Finds a [TaggedRole] with the given [tag].
     * If there are none, then null is returned.
     * If there are many, then one will be chosen (do not assume *which* one will be chosen!)
     */
    fun findARole(tag: Any): TaggedRole? = rolesByTag[tag]?.firstOrNull()

    /**
     * Finds [TaggedRole]s which have __any__ of the [tags] specified.
     * If there are none, then an empty set is returned.
     */
    fun findRolesWithAnyTags(vararg tags: Any): Set<TaggedRole> {
        if (tags.isEmpty()) return emptySet()
        if (tags.size == 1) return findRoles(tags.first())
        val result = mutableSetOf<TaggedRole>()
        for (tag in tags) {
            result.addAll(findRoles(tag))
        }
        return result
    }

    /**
     * Similar to [findRolesWithAnyTags], but returns just one.
     * If there are none, then null is returned.
     */
    fun findARoleWithAnyTags(vararg tags: Any): Role? {
        tags.forEach { tag ->
            val role = findARole(tag)
            if (role != null) return role
        }
        return null
    }

    /**
     * Finds [TaggedRole]s which have __all__ of the [tags] specified.
     * If there are none, then an empty set is returned.
     *
     * It is quicker, if the first of [tags] has fewest results.
     * For example, if there are 100 `blue` and 10 `deadly`,
     * then it is quicker if you placed `deadly` first.
     */
    fun findRolesWithAllTags(vararg tags: Any): Set<TaggedRole> {
        if (tags.isEmpty()) return emptySet()
        if (tags.size == 1) return findRoles(tags[0])

        val result = mutableSetOf<TaggedRole>()
        result.addAll(findRoles(tags[0]))

        for (i in 1 until tags.size) {
            val tag = tags[i]
            result.removeIf { rolesByTag[tag]?.contains(it) != true }
        }

        return result
    }

    /**
     * Similar to [findRolesWithAllTags], but returns just one.
     * If there are none, the null is returned.
     */
    fun findARoleWithAllTags(vararg tags: Any): TaggedRole? {
        if (tags.isEmpty()) return null
        if (tags.size == 1) return findARole(tags[0])

        fun matches(role: TaggedRole): Boolean {
            for (i in 1 until tags.size) {
                val tag = tags[i]
                val set = rolesByTag[tag] ?: return false
                if (!set.contains(role)) return false
            }
            return true
        }

        for (possibleMatch in findRoles(tags[0])) {
            if (matches(possibleMatch)) {
                return possibleMatch
            }
        }

        return null
    }

    /**
     * Finds roles of a given class. Note, this uses `isInstance`, therefore it will also return
     * roles of a sub-class of [klass].
     *
     * The first time this is called for a give [klass], it is expensive, as we have to call
     * `isInstance` on all [TaggedRole]s. Subsequent calls will be quick though.
     */
    fun <T : TaggedRole> findRolesByClass(klass: Class<T>): List<T> {
        val entry: Set<TaggedRole>? = rolesByClass[klass]
        if (entry == null) {
            val result: MutableSet<TaggedRole> = allRoles.filterIsInstance(klass).toMutableSet()
            rolesByClass[klass] = result
            return result.filterIsInstance(klass)
        }
        return entry.filterIsInstance(klass)
    }

    /**
     * Finds any role with the given [tag], which is overlapping [me].
     * Returns null if none are found.
     */
    fun findOverlapping(me: Role, tag: Any): TaggedRole? {
        findRoles(tag).forEach { other ->
            if (other.actor.stage?.firstView()?.overlapping?.overlapping(me.actor, other.actor) == true) {
                return other
            }
        }
        return null
    }

    /**
     * Finds a role with __all__ the given [tags], which is overlapping [me].
     * Returns null if none are found.
     */
    fun findOverlappingWithAllTags(me: Role, vararg tags: Any): TaggedRole? {
        findRolesWithAllTags(tags).forEach { other ->
            if (other.actor.stage?.firstView()?.overlapping?.overlapping(me.actor, other.actor) == true) {
                return other
            }
        }
        return null
    }

    /**
     * Finds a role with __any__ of the given [tags], which is overlapping [me].
     * Returns null if none are found.
     */
    fun findOverlappingWithAnyTags(me: Role, vararg tags: Any): TaggedRole? {
        findRolesWithAnyTags(tags).forEach { other ->
            if (other.actor.stage?.firstView()?.overlapping?.overlapping(me.actor, other.actor) == true) {
                return other
            }
        }
        return null
    }

    fun <T : TaggedRole> findOverlappingByClass(me: Role, klass: Class<T>): T? {
        findRolesByClass(klass).forEach { other ->
            if (other.actor.stage?.firstView()?.overlapping?.overlapping(me.actor, other.actor) == true) {
                return other
            }
        }
        return null
    }

    /**
     * Returns the closest role with the given tag, or null if there are no roles with that tag.
     * [toMe] is never returned.
     */
    fun closest(toMe: Role, tag: Any): TaggedRole? {
        return toMe.closest(findRoles(tag)) as? TaggedRole
    }

    internal fun add(role: TaggedRole, tag: Any) {
        val set = rolesByTag[tag]
        if (set == null) {
            val newSet = mutableSetOf(role)
            rolesByTag[tag] = newSet
        } else {
            set.add(role)
        }
    }

    internal fun addByClass(role: TaggedRole) {
        allRoles.add(role)
        rolesByClass.forEach { (key, set) ->
            if (key.isInstance(role)) {
                set.add(role)
            }
        }
    }

    internal fun remove(role: Role, tag: Any) {
        rolesByTag[tag]?.remove(role)
    }

    internal fun removeByClass(role: Role) {
        allRoles.remove(role)
        rolesByClass.values.forEach { it.remove(role) }
    }

    internal fun clear() {
        rolesByTag.clear()
        allRoles.clear()
        rolesByClass.clear()
    }

    fun dump() {
        println("TagManager dump... $this")
        for ((name, list) in rolesByTag) {
            println("Roles by tag $name\n    " + list.joinToString(separator = "\n    "))
        }
        println("End dump")
    }

    companion object {
        @JvmStatic
        var instance = TagManager()
    }
}
