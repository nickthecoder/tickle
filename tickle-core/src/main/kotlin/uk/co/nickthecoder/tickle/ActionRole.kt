/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import uk.co.nickthecoder.tickle.action.AbstractAction
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.ActionHolder
import uk.co.nickthecoder.tickle.action.Kill

/**
 * A [Role] that only has an [Action].
 * It is a convenient way of using an [Action] within a [Role].
 *
 * The hard work is done by the super class [ActionHolder].
 *
 * Note, if you want the Actor to die, then ensure you include a [Kill] action,
 * otherwise the Actor will stay living, but do nothing.
 *
 * There are two ways to create the [Action].
 * Either it is passed directly to ActionRole's constructor, or
 * more commonly by overriding [createAction].
 *
 * Common bug : If the action is never performed, check that super.begin() is
 * called if your code has its own begin method.
 * Likewise, check that super.tick is called if your code has its own [tick] method.
 */
open class ActionRole(private val defaultAction: Action?) : ActionHolder(), Role {

    constructor() : this(null)

    override lateinit var actor: Actor

    /**
     * Creates the [Action] by calling [createAction].
     */
    override fun begin() {
        then(createAction())
    }

    /**
     * Does nothing. However, if you override this method, IMHO, it is good practice to call super.activated()
     */
    override fun activated() {}

    open fun createAction(): Action? = defaultAction

    override fun tick() {
        act()
    }

    override fun end() {}

}

/**
 * You can use this in a similar way as `forever()`, however,
 * the action will be rebuilt, rather than just repeated.
 * So, if random values are used by the action, then the random numbers will be generated again.
 *
 * For another way to recreate actions, which does not depend on ActionRole,
 * see FactoryAction class (in ActionExtensions.feather).
 */
class RecreateAction(val role: ActionRole) : AbstractAction() {
    override fun act(): Boolean {
        role.replaceAction(role.createAction())
        return true
    }
}

