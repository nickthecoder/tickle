package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.events.MouseEvent
import java.lang.reflect.Field

/**
 * An easy way to load/save and store values using [AutoFlushPreferences], as well
 * as a simple in-game GUI for the player to change the values.
 *
 * Create a sub-class of [PersistentOptions] (maybe called just "Options").
 * Add `var`s for each value you want to keep track of,
 * including an annotation called '@Option' e.g.
 *
 *      class Options : PersistentOptions {
 *          @Option( values = """
 *              Easy : 0
 *              Medium : 1
 *              Hard : 2
 *          """ )
 *          var difficulty : int = 0
 *
 *          @Option( values = """
 *              Off : false
 *              You cheater! : true
 *          """ )
 *          var enableCheats = false
 *
 *          // Create a single instance...
 *          static val instance = Options()
 *
 *          // Only needed if you plan to use the reset() method.
 *          init {
 *              rememberDefaultValues()
 *          }
 *      }
 *
 * Note that the defaultValue is a String, regardless of the actual type (int and boolean).
 * You can also use floats, doubles and String types (not shown in this example).
 * But you cannot use other types (such as Vector2); Sorry.
 *
 * Load and save the options from you game's [Producer] :
 *
 *      override fun begin() {
 *          super.begin()
 *          Options.instance.load()
 *      }
 *
 * Create a scene where the player can change the options, with the following [Director] :
 *
 *      class OptionsDirector : AbstractDirect() {
 *
 *          override fun begin() {
 *              super.begin()
 *              Options.instance.initialiseControls()
 *          }
 *
 *          override fun ending() {
 *              // Updates the `var`s with @Option annotations, from the data held by the OptionButtons.
 *              Options.instance.readControls()
 *
 *              Options.instance.save()
 *              super.ending()
 *          }
 *
 *      }
 *
 * Create a Costume from a Font, which a Role of type [OptionButton].
 * Add one [OptionButton] Actor to you scene (using the Scene Editor) for each
 * of the fields with the @Option attribute. Make sure that the Actor's name is
 * the same as the field name. i.e. `difficulty` and `enableCheats` in this example.
 *
 * Optionally, you can add a [MessageButton] to the scene, which sends a `resetOptions` message
 * to your OptionsDirector :
 *
 *          override fun message( message : String ) {
 *              if (message == "resetOptions" ) {
 *                  Options.instance.reset()
 *              }
 *          }
 *
 * That's it. You are good to go. Now you can use the options from anywhere in your game. e.g.
 *
 *      if (Options.instance.enableCheats) { ... }
 *
 * Note, you can have different groups of options. So instead the `Options` class, you could have
 * separate `GraphicsOptions` and `GamePlayOptions`, and have a scene for each group.
 *
 * There is a special case for an options field named `isFullScreen`.
 * A restart is required for this to take effect. e.g.
 *
 *      @Option( defaultValue = "0", values = """
 *              Windowed (requires a restart): false
 *              Full-Screen (requires a restart): true
 *          """ )
 *          var isFullScreen = false
 */
open class PersistentOptions {

    private val defaultValues = mutableMapOf<String, Any>()

    fun rememberDefaultValues() {
        for (field in this.javaClass.declaredFields) {
            annotation(field) ?: continue
            defaultValues[field.name] = field.get(this)
        }
    }

    private fun annotation(field: Field): Option? {
        return field.annotations.filterIsInstance(Option::class.java).firstOrNull()
    }

    private fun findField(name: String): Field? {
        return try {
            this.javaClass.getField(name)
        } catch (e: NoSuchFieldException) {
            warn("Field $name not found. Ignoring.")
            null
        }
    }

    /**
     * Call this from the [Director] of the options scene during [Director.begin].
     */
    fun initialiseControls() {
        for (stage in Game.instance.scene.stages()) {
            for (role in stage.findRolesByClass(OptionButtonRole::class.java)) {
                if (role.actor.name.isNotBlank()) {
                    val field = findField(role.actor.name) ?: continue
                    initialiseControl(role, field)
                }
            }
        }
    }

    /**
     * Call this from the [Director] of the options scene during [Director.ending].
     */
    private fun initialiseControl(role: OptionButtonRole, field: Field) {
        val annotation = annotation(field)
        if (annotation == null) {
            warn("Field ${role.actor.name} has no @Option annotation. Ignoring.")
        } else {
            val values = mutableListOf<PersistentOptionValue>()
            for (line in annotation.values.split("\n")) {
                val colon = line.indexOf(":")
                if (line.isNotBlank()) {
                    if (colon <= 0) {
                        warn("Option ${role.actor.name} - Expected LABEL : VALUE. Found: $line.")
                    } else {
                        stringToValue(field.type, line.substring(colon + 1).trim())?.let { value ->
                            values.add(
                                PersistentOptionValue(
                                    line.substring(0, colon).trim(),
                                    value
                                )
                            )
                        }
                    }
                }
            }
            if (values.isEmpty()) {
                role.actor.die()
            } else {
                role.handler.update(values, field.get(this))
            }
        }
    }


    /**
     * Call this from the option scene's [Director.ending] method.
     */
    fun readControls() {
        for (stage in Game.instance.scene.stages()) {
            for (role in stage.findRolesByClass(OptionButtonRole::class.java)) {
                if (role.actor.name.isNotBlank()) {
                    val field = findField(role.actor.name) ?: continue
                    setField(field, role.handler.value())
                }
            }
        }
    }

    /**
     * Call this from your game's [Director] in [Director.begin]
     */
    fun load() {
        val preferences = preferences
        for (field in this.javaClass.declaredFields) {
            annotation(field) ?: continue
            val value = when (field.type) {
                String::class.java -> preferences.getString(field.name)
                Int::class.javaPrimitiveType, Int::class.java -> preferences.getInt(field.name)
                Float::class.javaPrimitiveType, Float::class.java -> preferences.getFloat(field.name)
                Double::class.javaPrimitiveType, Double::class.java -> preferences.getDouble(field.name)
                Boolean::class.javaPrimitiveType, Boolean::class.java -> preferences.getBoolean(field.name)
                else -> {
                    warn("Options of type ${field.type.simpleName} are not supported")
                    continue
                }
            } ?: continue

            try {
                field.set(this, value)
            } catch (e: Exception) {
                warn("Failed to set field ${field.name} to value $value (${value.javaClass})")
            }
        }
    }

    /**
     * Either call this ehn your game ends, from you game's [Director.ending], or
     * from the option scene's [Director] from [Director.ending].
     */
    fun save() {
        val preferences = preferences
        for (field in this.javaClass.declaredFields) {
            annotation(field) ?: continue
            val value = try {
                field.get(this)
            } catch (e: Exception) {
                severe("Failed to get the value of field ${field.name}")
                continue
            }

            when (value) {
                // These lines *look* the same, but each are different typed put( value : ??? ) methods.
                is String -> preferences[field.name] = value.toString()
                is Int -> preferences[field.name] = value
                is Float -> preferences[field.name] = value
                is Double -> preferences[field.name] = value
                is Boolean -> preferences[field.name] = value
                else -> {
                    warn("PersistentOptions: Type ${field.type.simpleName} is not supported")
                }
            }
        }
    }

    private fun stringToValue(type: Class<*>, str: String): Any? {
        try {
            return when (type) {
                String::class.java -> str
                Int::class.javaPrimitiveType, Int::class.java -> str.toInt()
                Float::class.javaPrimitiveType, Float::class.java -> str.toFloat()
                Double::class.javaPrimitiveType, Double::class.java -> str.toDouble()
                Boolean::class.javaPrimitiveType, Boolean::class.java -> str.toBoolean()
                else -> {
                    warn("Options of type ${type.simpleName} are not supported")
                    null
                }
            }
        } catch (e: Exception) {
            warn("Couldn't convert '$str' to ${type.simpleName}")
            return null
        }
    }

    fun reset() {
        for (field in this.javaClass.declaredFields) {
            annotation(field) ?: continue
            defaultValues[field.name]?.let { setField(field, it) }
        }
        initialiseControls()
    }

    private fun setField(field: Field, stringValue: String) {
        stringToValue(field.type, stringValue)?.let {
            setField(field, it)
        }
    }

    private fun setField(field: Field, value: Any) {
        field.set(this, value)
    }

    companion object {

        @JvmStatic
        val preferences: AutoFlushPreferences
            get() = Game.instance.preferences.node("options")
    }

}

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class Option(
    /**
     * A multi-line string. One line for each possible value, in the format :
     *
     *      LABEL : VALUE
     */
    val values: String = ""
)

/**
Each option has a set of possible values. This holds the data for one possible value.
[label] is for display purposes only, and is only used on the options scene.
The value will be converted to the appropriate type by [PersistentOptions].
 */
class PersistentOptionValue(val label: String, val value: Any) {
    override fun toString() = "$label = $value"
}

interface OptionButtonRole : Role {
    val handler: OptionButtonHandler
}

/**
 * When clicked, the text will change to the next possible in the list of
 * [OptionButtonHandler.values] [PersistentOptionValue]. Cycling back round to the first option.
 *
 * [PersistentOptions] initialises [OptionButtonHandler.values] and [OptionButtonHandler.index]
 * from [PersistentOptions.initialiseControls].
 *
 * NOTE. You can create your own implementation instead of using [OptionButton].
 * It must implement the interface [OptionButtonRole].
 */
open class OptionButton : Button() {

    lateinit var handler: OptionButtonHandler

    override fun begin() {
        super.begin()
        handler = OptionButtonHandler(this)
    }

    override fun onClicked(event: MouseEvent) = handler.onClicked()
}

/**
 * Used by [OptionButton] to hold the list of [values], and update the Actor's text when
 * the button is pressed.
 *
 * This is not part of [OptionButton], so that it's possible to use this code from your own
 * [Button] sub-classes. For example, if your game has AnimatedOptionButtons, which slide into
 * view when the scene starts, then you can create a sub-class of AnimatedOptionButtons,
 * with :
 *
 *      val handler = OptionButtonHandler( this )
 *      override fun getHandler() = handler
 *      override fun onClicked(event:MouseEvent) {
 *          handler.onClicked()
 *      }
 *
 */
class OptionButtonHandler(val button: Button) {

    /**
     * The key is the label. Note that the value is a String (not an int, boolean, float etc).
     * PersistentOptions handles the conversion to the correct types.
     */
    internal var values = listOf<PersistentOptionValue>()

    /**
     * The index into [values] of the currently displayed value.
     */
    internal var index = 0

    fun onClicked() {
        if (values.isEmpty()) return
        if (++index >= values.size) {
            index = 0
        }
        updateText()
    }

    fun update(values: List<PersistentOptionValue>, value: Any) {
        this.values = values
        index = 0
        for (i in values.indices) {
            if (values[i].value == value) {
                index = i
                break
            }
        }
        updateText()
    }

    fun updateText() {
        button.actor.textAppearance?.text = values[index].label
    }

    fun value(): Any = values[index].value

}

// Option Button is currently the only type of control,
// but at a later date I may add an OptionSlider for int/float.
private fun updateControl(role: OptionButton, value: String) {

    role.handler.updateText()
    warn("Option ${role.actor.name} - Label for '$value' not found, defaulting to 1st.")
}
