/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.loop

import org.lwjgl.glfw.GLFW
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.physics.TickleWorld

abstract class AbstractGameLoop(val game: Game) : GameLoop {

    private var startNanos = System.nanoTime()

    override var tickCount = 0L

    override var droppedFrames : Int = 0

    override fun resetStats() {
        startNanos = System.nanoTime()
        tickCount = 0
        droppedFrames = 0
    }

    override fun actualFPS(): Double {
        return (tickCount.toDouble() * 1_000_000_000 / (System.nanoTime() - startNanos))
    }

    override fun sceneStarted() {
        resetStats()
        game.scene.stages.values.forEach { stage ->
            stage.world?.resetAccumulator()
        }
    }

    protected fun tick() {
        GLFW.glfwPollEvents()

        tickCount++

        with(game) {
            // NOTE, [GameLoop] documents the order of these tick calls.
            // Update that documentation if/when this code is changed!
            tryCatch(producer) { producer.preTick() }
            tryCatch(director) { director.preTick() }

            tryCatch(producer) { producer.tick() }
            tryCatch(director) { director.tick() }

            if (!game.paused) {
                tryCatch(scene.actions) { scene.actions.tick() }

                // Using toList, just in case a new view/stage was added. Avoids ConcurrentModificationException
                game.scene.views.values.toList().forEach { tryCatch(it) { it.tick() } }
                game.scene.stages.values.toList().forEach { tryCatch(it) { it.tick() } }

                // Call tick on all TickleWorlds
                // Each stage can have its own TickleWorld. However, in the most common case, there is only
                // one tickle world shared by all stages. Given the way that TickleWorld keeps a constant
                // time step, we COULD just call tick on every stage's TickleWorld without affecting the game.
                // However, debugging information may be misleading, as the first stage will cause a step, and
                // the subsequent stages will skip (because the accumulator is near zero).
                // So, this code ticks all Stages' worlds, but doesn't bother doing it twice for the same world.
                var world: TickleWorld? = null
                game.scene.stages.values.forEach { stage ->
                    val stageWorld = stage.world
                    if (stageWorld != null) {
                        if (stageWorld != world) {
                            world = stageWorld
                            stageWorld.tick()
                        }
                    }
                }
            }

            tryCatch(director) { director.postTick() }
            tryCatch(producer) { producer.postTick() }

        }
    }

    protected fun mouseMoveTick() {
        game.mouseMoveTick()
    }

    protected fun processRunLater() {
        game.processRunLater()
    }
}
