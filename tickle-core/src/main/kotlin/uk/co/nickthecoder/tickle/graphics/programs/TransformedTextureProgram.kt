package uk.co.nickthecoder.tickle.graphics.programs

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.Pose
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Texture
import uk.co.nickthecoder.tickle.util.IVector2

class TransformedTextureProgram : ShaderProgram(VERTEX, FRAGMENT, 4) {

    val colorLocation = getUniformLocation("color")

    fun setup(texture: Texture, tint: Color? = null, modelMatrix: Matrix3x2f? = null) {
        texture.bind()
        enablePositionUV()
        setUniform(colorLocation, tint ?: white)
        setModelMatrix(modelMatrix)
    }

    fun drawPose(pose: Pose, x: Float, y: Float, scale: IVector2) {

        val left = x - pose.offsetX * scale.x
        val bottom = y - pose.offsetY * scale.y
        val right = left + pose.pixelRect.width * scale.x
        val top = bottom + pose.pixelRect.height * scale.y

        val tLeft = pose.rect.left
        val tRight = pose.rect.right
        val tBottom = pose.rect.bottom
        val tTop = pose.rect.top

        requiredTriangles(2)
        Renderer.floatBuffer
            .put(left).put(bottom).put(tLeft).put(tBottom)
            .put(right).put(bottom).put(tRight).put(tBottom)
            .put(right).put(top).put(tRight).put(tTop)

            .put(right).put(top).put(tRight).put(tTop)
            .put(left).put(top).put(tLeft).put(tTop)
            .put(left).put(bottom).put(tLeft).put(tBottom)
    }

    fun drawPose(pose: Pose, x: Float, y: Float) {

        val left = (x - pose.offsetX)
        val bottom = (y - pose.offsetY)
        val right = left + pose.pixelRect.width
        val top = bottom + pose.pixelRect.height

        val tLeft = pose.rect.left
        val tRight = pose.rect.right
        val tBottom = pose.rect.bottom
        val tTop = pose.rect.top

        requiredTriangles(2)
        Renderer.floatBuffer
            .put(left).put(bottom).put(tLeft).put(tBottom)
            .put(right).put(bottom).put(tRight).put(tBottom)
            .put(right).put(top).put(tRight).put(tTop)

            .put(right).put(top).put(tRight).put(tTop)
            .put(left).put(top).put(tLeft).put(tTop)
            .put(left).put(bottom).put(tLeft).put(tBottom)
    }

    fun draw(
        left: Float, bottom: Float, right: Float, top: Float,
        tLeft: Float, tBottom: Float, tRight: Float, tTop: Float
    ) {
        requiredTriangles(2)
        Renderer.floatBuffer
            .put(left).put(bottom).put(tLeft).put(tBottom)
            .put(right).put(bottom).put(tRight).put(tBottom)
            .put(right).put(top).put(tRight).put(tTop)

            .put(right).put(top).put(tRight).put(tTop)
            .put(left).put(top).put(tLeft).put(tTop)
            .put(left).put(bottom).put(tLeft).put(tBottom)
    }

    companion object {
        private val VERTEX = """
            #version 120

            attribute vec2 position;
            attribute vec2 uv;

            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;
            
            varying vec2 textureCoord;

            void main() {
                textureCoord = uv;

                gl_Position = vec4( viewMatrix * modelMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val FRAGMENT = """
            #version 120
            
            varying vec2 textureCoord;

            uniform sampler2D texImage;
            uniform vec4 color;

            void main() {
                gl_FragColor = color * texture2D(texImage, textureCoord);
            }

            """.trimIndent()
    }
}