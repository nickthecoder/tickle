/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.misc.lerp
import java.util.*

class RandomFactory(seed: Long?) {

    constructor() : this(null)

    private val random = if (seed == null || seed == 0L) Random() else Random(seed)

    /**
     * Returns a random number greater than or equals to 0 and less than 1.
     */
    fun nextDouble() = random.nextDouble()

    fun nextDouble(ease: Ease = LinearEase.instance) = ease.ease(random.nextFloat()).toDouble()

    /**
     * Returns a random number greater than or equals to 0 and less than 1.
     */
    fun nextFloat() = random.nextDouble().toFloat()

    fun nextFloat(ease: Ease) = ease.ease(random.nextFloat())


    /**
     * Returns a random integer from 0 to max - 1.
     */
    fun randomInt(max: Int) = random.nextInt(max)

    /**
     * If max is +ve, returns a random number greater than or equal to 0 and less than max.
     * If max is -ve, returns a random number greater than max and less than or equal to 0.
     */
    fun randomDouble(max: Double) = random.nextDouble() * max

    /**
     * If max is +ve, returns a random number greater than or equal to 0 and less than max.
     * If max is -ve, returns a random number greater than max and less than or equal to 0.
     */
    fun randomFloat(max: Float) = random.nextFloat() * max

    /**
     * Returns true 1 in [n] times.
     */
    fun oneIn(n: Int) = random.nextInt(n) == 0

    /**
     * Returns a number in the range -[limit] to [limit].
     */
    fun plusMinus(limit: Double) = nextDouble() * limit * 2 - limit

    fun plusMinus(limit: Double, ease: Ease) = nextDouble(ease) * limit * 2 - limit

    fun plusMinus(limit: Float): Float = (nextDouble() * limit * 2 - limit).toFloat()

    fun plusMinus(limit: Float, ease: Ease): Float = (nextDouble(ease) * limit * 2 - limit).toFloat()

    /**
     * Returns a number in the range [from] (inclusive) to [to] (exclusive).
     */
    fun between(from: Int, to: Int) = from + randomInt(to - from)

    fun between(from: Double, to: Double) = lerp(from, to, nextDouble())

    fun between(from: Double, to: Double, ease: Ease) = lerp(from, to, nextDouble(ease))

    fun between(from: Float, to: Float) = lerp(from, to, nextFloat())

    fun between(from: Float, to: Float, ease: Ease) = lerp(from, to, nextFloat(ease))

    fun between(from: Angle, to: Angle) = Angle.radians(lerp(from.radians, to.radians, nextDouble()))

    fun between(from: Angle, to: Angle, ease: Ease) = Angle.radians(lerp(from.radians, to.radians, nextDouble(ease)))

    fun randomAngle() = Angle.degrees(randomDouble(360.0))

    /**
     * Returns a color linearly interpolated between the two given colors.
     */
    fun between(from: Color, to: Color) = between(from, to, LinearEase.instance)

    fun between(from: Color, to: Color, ease: Ease): Color {
        val result = Color()
        from.lerp(to, nextDouble(ease).toFloat(), result)
        return result
    }

    /**
     * Returns a Polar2d coordinate between the [from] and [to].
     * A single random number is chosen from 0..1, and this is used to linearly interpolate both the
     * magnitude and the direction. Therefore the possible results will lie on a curve (not a straight line).
     */
    fun between(from: Polar2d, to: Polar2d) = Polar2d(from).lerp(to, nextFloat())

    fun between(from: Polar2d, to: Polar2d, ease: Ease) = Polar2d(from).lerp(to, nextFloat(ease))

    /**
     * Returns a vector randomly distributed along the line between [from] and [to].
     */
    fun between(from: Vector2, to: Vector2): Vector2 = Vector2(from).lerp(to, nextFloat() )

    fun between(from: Vector2, to: Vector2, ease: Ease): Vector2 = Vector2(from).lerp(to, nextFloat(ease) )

    /**
     * Returns a randomly selected item from the list.
     * Return null if the list is empty.
     *
     * Note, [listItem] is preferred, because it returns an object of the correct type.
     */
    fun item(list: List<*>): Any? {
        return if (list.isEmpty()) null else list[randomInt(list.size)]
    }

    /**
     * Returns a randomly selected item from the list, or null, if the list is empty.
     */
    fun <T> listItem(list: List<T>): T? {
        return if (list.isEmpty()) null else list[randomInt(list.size)]
    }

    companion object {
        /**
         * A shared instance.
         */
        var instance = RandomFactory()
    }

}


/**
 * Static methods using a shared instance of [RandomFactory].
 *
 * It is more terse to use `Rand`, rather than [RandomFactory] directly.
 */
object Rand {

    @JvmStatic
    fun randomInt(max: Int) = RandomFactory.instance.randomInt(max)

    @JvmStatic
    fun randomDouble(max: Double) = RandomFactory.instance.randomDouble(max)

    @JvmStatic
    fun randomFloat(max: Float) = RandomFactory.instance.randomFloat(max)

    @JvmStatic
    fun oneIn(n: Int) = RandomFactory.instance.oneIn(n)

    @JvmStatic
    fun plusMinus(limit: Double) = RandomFactory.instance.plusMinus(limit)

    @JvmStatic
    fun plusMinus(limit: Double, ease: Ease) = RandomFactory.instance.plusMinus(limit, ease)

    @JvmStatic
    fun plusMinus(limit: Float) = RandomFactory.instance.plusMinus(limit)

    @JvmStatic
    fun plusMinus(limit: Float, ease: Ease) = RandomFactory.instance.plusMinus(limit, ease)

    @JvmStatic
    fun between(from: Double, to: Double) = RandomFactory.instance.between(from, to)

    @JvmStatic
    fun between(from: Double, to: Double, ease: Ease) = RandomFactory.instance.between(from, to, ease)

    @JvmStatic
    fun between(from: Float, to: Float) = RandomFactory.instance.between(from, to)

    @JvmStatic
    fun between(from: Float, to: Float, ease: Ease) = RandomFactory.instance.between(from, to, ease)

    @JvmStatic
    fun between(from: Color, to: Color) = RandomFactory.instance.between(from, to)

    @JvmStatic
    fun between(from: Color, to: Color, ease: Ease) = RandomFactory.instance.between(from, to, ease)

    @JvmStatic
    fun between(from: Polar2d, to: Polar2d) = RandomFactory.instance.between(from, to)

    @JvmStatic
    fun between(from: Polar2d, to: Polar2d, ease: Ease) = RandomFactory.instance.between(from, to, ease)

    @JvmStatic
    fun between(from: Vector2, to: Vector2) = RandomFactory.instance.between(from, to)

    @JvmStatic
    fun between(from: Vector2, to: Vector2, ease: Ease) = RandomFactory.instance.between(from, to, ease)

    @JvmStatic
    fun between(from: Angle, to: Angle) = RandomFactory.instance.between(from, to)

    @JvmStatic
    fun between(from: Angle, to: Angle, ease: Ease) = RandomFactory.instance.between(from, to, ease)

    @JvmStatic
    fun randomAngle() = RandomFactory.instance.randomAngle()

    @JvmStatic
    fun <T> listItem(list: List<T>) = RandomFactory.instance.listItem(list)

    @JvmStatic
    @Deprecated(message = "Not typed", replaceWith = ReplaceWith("listItem"))
    fun item(list: List<*>) = RandomFactory.instance.item(list)

}
