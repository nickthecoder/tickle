/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonObject
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.TextHAlignment
import uk.co.nickthecoder.tickle.graphics.TextVAlignment
import uk.co.nickthecoder.tickle.graphics.onOpenGLThread
import uk.co.nickthecoder.tickle.misc.JsonUtil
import uk.co.nickthecoder.tickle.resources.*
import uk.co.nickthecoder.tickle.scripts.ScriptManager
import uk.co.nickthecoder.tickle.stage.Stage
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

object SceneReader {

    fun load(file: File, resources: Resources, mergeIncludes: Boolean = true): Scene {
        val jRoot = Json.parse(InputStreamReader(FileInputStream(file))).asObject()

        val layout = resources.layouts.find(jRoot.getString("layout", "default"))
            ?: resources.layouts.items().values.first()

        val scene = layout.createScene(resources).apply {
            val directorString =
                ScriptManager.renamedClass(jRoot.getString("director", NoDirector::class.java.name))
            // Run on OpenGL Thread, just in case the director runs OpenGL code in its constructor.
            director = onOpenGLThread { Director.createDirector(directorString) }

            val directorAttributes = resources.createAttributes()
            JsonUtil.loadAttributes(jRoot, directorAttributes, "directorAttributes")
            directorAttributes.applyToObject(director)

            layoutName = jRoot.getString("layout", "default")
            background = Color.create(jRoot.getString("background", "#FFFFFF"))
            showMouse = jRoot.getBoolean("showMouse", true)
            comments = jRoot.getString("comments", "")
        }

        val defaultLayoutStageName = layout.layoutStages.filter { it.value.isDefault }.keys.firstOrNull()
        val defaultStage = defaultLayoutStageName?.let { scene.findStage(it) } ?: scene.stages().first()

        jRoot.get("include")?.let {
            val jIncludes = it.asArray()
            jIncludes.forEach { jInclude ->
                val includePath = jInclude.asString()
                val includeFile = resources.scenePathToFile(includePath)
                if (mergeIncludes) {
                    mergeScene(includeFile, resources, defaultStage, scene)
                } else {
                    scene.includedScenes[includePath] = load(includeFile, resources, true)
                }
            }
        }

        jRoot.get("stages")?.let {
            val jStages = it.asArray()
            jStages.forEach { jStage ->
                loadStage(resources, scene, defaultStage, jStage.asObject())
            }
        }
        return scene
    }

    fun mergeScene(file: File, resources: Resources, defaultStage: Stage, intoScene: Scene) {

        val jRoot = Json.parse(InputStreamReader(FileInputStream(file))).asObject()

        jRoot.get("include")?.let {
            val jIncludes = it.asArray()
            jIncludes.forEach { jInclude ->
                val includePath = jInclude.asString()
                val includeFile = resources.scenePathToFile(includePath)
                mergeScene(includeFile, resources, defaultStage, intoScene)
            }
        }

        jRoot.get("stages")?.let {
            val jStages = it.asArray()
            jStages.forEach { jStage ->
                loadStage(resources, intoScene, defaultStage, jStage.asObject())
            }
        }
    }

    private fun loadStage(resources: Resources, scene: Scene, defaultStage: Stage, jStage: JsonObject) {

        val stageName = jStage.getString("name", "default")
        val stage = scene.findStage(stageName) ?: defaultStage

        jStage.get("actors")?.let {
            val jActors = it.asArray()
            jActors.forEach {
                loadActor(resources, stage, it.asObject())
            }
        }
    }

    private fun loadActor(resources: Resources, stage: Stage, jActor: JsonObject) {

        // NOTE. load the attributes FIRST, then change the costume, as that gives Attributes the chance to remove
        // attributes unsupported by the Role class.
        val costumeName = jActor.get("costume").asString()
        val costume = resources.costumes.find(costumeName)
        if (costume == null) {
            severe("Costume : $costumeName not found")
            return
        }

        // Run on the OpenGL thread, in case the Role runs GL code in its constructor.
        val actor = onOpenGLThread { costume.createActor() }

        val roleAttributes = resources.createAttributes()
        JsonUtil.loadAttributes(jActor, roleAttributes)
        actor.role?.let { roleAttributes.applyToObject(it) }

        with(actor) {
            name = jActor.getString("name", "")
            x = jActor.getFloat("x", 0f)
            y = jActor.getFloat("y", 0f)
            zOrder = jActor.getFloat("zOrder", costume.zOrder)

            // Legacy (when scale was a single Double, rather than a Vector2)
            jActor.get("scale")?.let {
                scale.x = it.asFloat()
                scale.y = it.asFloat()
            }

            autoPositionAlignment.x = jActor.getFloat("autoPositionAlignmentX", 0f)
            autoPositionAlignment.y = jActor.getFloat("autoPositionAlignmentY", 0f)
            direction.degrees = jActor.getDouble("direction", 0.0)
            scale.x = jActor.getFloat("scaleX", 1f)
            scale.y = jActor.getFloat("scaleY", 1f)

            tint = jActor.getColor("color", uk.co.nickthecoder.tickle.graphics.Color.white())

            isLocked = jActor.getBoolean("isLocked", false)

            val appearance = actor.appearance

            if (appearance is TextAppearance) {
                appearance.text = jActor.getString("text", "")
                val textStyle = appearance.textStyle.copy()
                textStyle.color = jActor.getColor("textColor", textStyle.color)
                textStyle.outlineColor = jActor.getOptionalColor("textOutlineColor", textStyle.outlineColor)
                textStyle.halignment = jActor.getHAlignment("textHAlignment", textStyle.halignment)
                textStyle.valignment = jActor.getVAlignment("textVAlignment", textStyle.valignment)
                jActor.getFont(resources, "font", textStyle.fontResource)?.let { textStyle.fontResource = it }

                appearance.textStyle = textStyle
            }


            if (appearance is ResizeAppearance) {
                val rect = costume.chooseNinePatch(costume.initialEventName)?.pose?.pixelRect
                    ?: costume.choosePose(costume.initialEventName)?.pixelRect
                appearance.size.x = jActor.getFloat("sizeX", rect?.width?.toFloat() ?: 1f)
                appearance.size.y = jActor.getFloat("sizeY", rect?.height?.toFloat() ?: 1f)
                appearance.sizeAlignment.x = jActor.getFloat("alignmentX", 0.5f)
                appearance.sizeAlignment.y = jActor.getFloat("alignmentY", 0.5f)
            }

        }
        // println("Added $actor to $stage")
        stage.add(actor)
    }

    private fun JsonObject.getFont(resources: Resources, name: String, defaultValue: FontResource?): FontResource? {
        val str = getString(name, null)
        return if (str == null) {
            defaultValue
        } else {
            resources.fontResources.find(str)
        }
    }
}


inline fun <reified T : Enum<T>> JsonObject.getEnum(name: String, defaultValue: T): T {
    val str = getString(name, null)
    return if (str == null) {
        defaultValue
    } else {
        enumValueOf(str)
    }
}

inline fun <reified T : Enum<T>> JsonObject.getOptionalEnum(name: String, defaultValue: T?): T? {
    val str = getString(name, null)
    return if (str == null) {
        defaultValue
    } else {
        enumValueOf<T>(str)
    }
}

private fun JsonObject.getVAlignment(name: String, defaultValue: TextVAlignment?) =
    getOptionalEnum(name, defaultValue) ?: TextVAlignment.BASELINE

private fun JsonObject.getHAlignment(name: String, defaultValue: TextHAlignment?) =
    getOptionalEnum(name, defaultValue) ?: TextHAlignment.CENTER


private fun JsonObject.getOptionalColor(name: String, defaultValue: Color?): Color? {
    val str = getString(name, null)
    return if (str == null) {
        defaultValue
    } else {
        Color.create(str)
    }
}

private fun JsonObject.getColor(name: String, defaultValue: Color): Color {
    val str = getString(name, null)
    return if (str == null) {
        defaultValue
    } else {
        Color.create(str)
    }
}
