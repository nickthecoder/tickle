package uk.co.nickthecoder.tickle.action.animation

import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.TimedAnimation

/**
 * NOTE. The first frame is performed at t=0, and the last frame at t=1
 * and therefore the moment that this animation ends, the last frame has
 * just been displayed.
 * This is useful when ping ponging, as we can just reverse the order, so we get :
 *
 *     0 1 2 3 44 3 2 1 0
 *
 * Where frame 4 is done twice (but with no gap in between)
 */
abstract class FrameAction(
    val frames: Int,
    seconds: Float,
    ease: Ease
) : TimedAnimation(seconds, ease) {

    var skipFirstFrame: Boolean = false

    private var currentFrame = 0

    override fun storeInitialValue() {
        currentFrame = 0
        if (!skipFirstFrame) {
            frame(0)
        }
    }

    override fun update(t: Float) {
        val frame = Math.floor((t * (frames - 1)).toDouble()).toInt()
        if (frame != currentFrame) {
            frame(frame)
            currentFrame = frame
        }
    }

    abstract fun frame(frame: Int)

    /**
     * Returns an action which performs the frames in reverse.
     * The same ease is used (often you want a different ease for the reverse direction)
     */
    fun reverse(): FrameAction {
        return Reversed(ease)
    }

    fun reverse(ease: Ease): Action {
        return this.then(Reversed(ease))
    }

    /**
     * Returns this action, then the [reverse] action.
     * The same ease is used (often you want a different ease for the reverse direction)
     *
     * Note, the reverse sets [skipFirstFrame], so that the last frame of the original isn't repeated.
     * If you want to repeat the ping-pong, consider setting [skipFirstFrame] on this too.
     */
    fun pingPong(): Action {
        return this.then(reverse())
    }

    fun pingPong(ease: Ease): Action {
        return this.then(reverse(ease))
    }

    /**
     * If FrameAction does :       0, 1, 2, 3, 4
     * Then PingPongAction does  : 4, 3, 2, 1, 0
     */
    inner class Reversed(ease: Ease) : FrameAction(frames, seconds, ease) {

        init {
            skipFirstFrame = true
        }

        override fun storeInitialValue() {
            if (!skipFirstFrame) {
                this@FrameAction.storeInitialValue()
            }
            currentFrame = 0
        }

        override fun frame(frame: Int) {
            this@FrameAction.frame(frames - frame - 1)
        }

    }
}
