/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.CostumeEvent
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.resources.Resources

/**
 * Creates additional poses for a Costume, which can be used as an "explosion".
 * This is called "Simple", because you do not need to define to shapes of the fragments,
 * they are randomly generated.
 */
class SimpleFragmentMaker(initialWidth: Int, initialHeight: Int) {

    private val managedTexture = ManagedTexture(initialWidth, initialHeight)

    private val costumeNameList = mutableListOf<String>()

    fun makeFragments(costumeName: String) {
        makeFragments(costumeName, 6)
    }

    fun makeFragments(costumeName: String, fragmentCount: Int) {
        makeFragments(Game.instance.resources, costumeName, fragmentCount)
    }

    fun makeFragments(resources: Resources, costumeName: String, fragmentCount: Int) {

        // If the fragments have already been created, then return immediately.
        if (resources.poses.find("${costumeName}-fragment1") != null) return

        val costume = resources.costumes.find(costumeName) ?: return
        val pose = costume.choosePose("default") ?: return
        val mask = FragmentMaker.randomMaskPose(pose, fragmentCount, 10)
        costumeNameList.add(costumeName)
        val fragments = FragmentMaker(pose, mask, managedTexture).generate()

        var event = costume.events["fragments"]
        if (event == null) {
            event = CostumeEvent(costume)
            costume.events["fragments"] = event
        }
        var count = 1
        for (fragment in fragments) {
            val fragmentPose = fragment.pose ?: continue
            resources.poses.add("${costumeName}-fragment${count}", fragmentPose)
            event.poses.add(fragmentPose)
            count++
        }
    }

    fun removeFragments(costumeName: String) {
        removeFragments(Game.instance.resources, costumeName)
    }

    fun removeFragments(resources: Resources, costumeName: String) {
        costumeNameList.remove(costumeName)
        var i = 1
        while (resources.poses.find("${costumeName}-fragment${i}") != null) {
            resources.poses.remove("${costumeName}-fragment${i}")
            i++
        }
        resources.costumes.find(costumeName)?.events?.remove("fragments")
    }

}
