/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.animation

import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.misc.lerp

class TurnTo(
    val heading: Angle,
    seconds: Float,
    val finalHeading: Angle,
    ease: Ease
)

    : TimedAnimation(seconds, ease) {

    constructor(heading: Angle, seconds: Float, angle: Angle) : this(heading, seconds, angle, LinearEase.instance)

    private var initialRadians: Double = 0.0

    private var finalRadians: Double = 0.0

    override fun storeInitialValue() {
        initialRadians = heading.radians
        finalRadians = finalHeading.radians
    }

    override fun update(t: Float) {
        heading.radians = lerp(initialRadians, finalRadians, t.toDouble())
    }

}
