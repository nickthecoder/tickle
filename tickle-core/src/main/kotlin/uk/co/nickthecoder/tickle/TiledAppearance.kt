/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle

import org.joml.Matrix3x2f
import uk.co.nickthecoder.tickle.graphics.Color
import uk.co.nickthecoder.tickle.graphics.Renderer

/**
 * Tiles a single Pose to build an image of the required width and height.
 * This is useful for creating rectangular objects that can have an arbitrary size, such as floors and walls
 * as well as patterned backgrounds.
 *
 * Note that rendering is done by repeatedly drawing the Pose, so if the Pose is small compared to the Actor's size,
 * then it will be somewhat slow. So if you want to tile a small pattern, consider making the Pose have multiple copies
 * of the pattern.
 */
class TiledAppearance(actor: Actor, override val pose: Pose) : ResizeAppearance(actor) {

    override val directionRadians: Double
        get() = pose.direction.radians

    private val poseWidth = pose.pixelRect.width

    private val poseHeight = pose.pixelRect.height

    private val partPose = pose.copy()

    init {
        size.x = pose.pixelRect.width.toFloat()
        size.y = pose.pixelRect.height.toFloat()
        oldSize.set(size)

        sizeAlignment.x = pose.offsetX / pose.pixelRect.width
        sizeAlignment.y = pose.offsetY / pose.pixelRect.height
        oldAlignment.set(sizeAlignment)
    }

    override fun draw(renderer: Renderer) {

        val left = (actor.x - sizeAlignment.x * width())
        val bottom = (actor.y - sizeAlignment.y * height())

        val simple = actor.isSimpleImage()
        val modelMatrix: Matrix3x2f? = if (!simple) {
            actor.calculateModelMatrix()
        } else {
            null
        }

        val colored = actor.tint != WHITE

        // Draw all of the complete pieces (i.e. where the pose does not need clipping).
        var y = 0.0f
        while (y < size.y - poseHeight) {
            var x = 0.0f
            while (x < size.x - poseWidth) {
                if (colored) {
                    renderer.drawTexture(
                        pose.texture,
                        left + x, bottom + y, left + x + poseWidth, bottom + y + poseHeight,
                        pose.rect,
                        actor.tint,
                        modelMatrix = modelMatrix
                    )
                } else {
                    renderer.drawTexture(
                        pose.texture,
                        left + x, bottom + y, left + x + poseWidth, bottom + y + poseHeight,
                        pose.rect,
                        modelMatrix = modelMatrix
                    )
                }

                x += poseWidth
            }

            y += poseHeight
        }

        val rightEdge: Float =
            size.x - if (size.x % poseWidth == 0f) poseWidth.toFloat() else (size.x % poseWidth)
        val topEdge: Float = y
        val partWidth: Float = (size.x - rightEdge)
        val partHeight: Float = (size.y - topEdge)

        // Draw the partial pieces on the right edge
        y = 0.0f
        partPose.pixelRect.top = pose.pixelRect.top
        partPose.pixelRect.right = (pose.pixelRect.left + partWidth).toInt()
        partPose.updateRect()
        while (y < size.y - poseHeight) {
            //println("Drawing right edge from ${partPose.rect}")
            if (colored) {
                renderer.drawTexture(
                    pose.texture,
                    left + rightEdge, bottom + y, left + size.x, bottom + y + poseHeight,
                    partPose.rect,
                    tint = actor.tint,
                    modelMatrix = modelMatrix
                )
            } else {
                renderer.drawTexture(
                    pose.texture,
                    left + rightEdge, bottom + y, left + size.x, bottom + y + poseHeight,
                    partPose.rect,
                    modelMatrix = modelMatrix
                )
            }
            y += poseHeight
        }

        // Draw the partial pieces on the top edge
        var x = 0.0f
        partPose.pixelRect.top = (pose.pixelRect.bottom - partHeight).toInt()
        partPose.pixelRect.right = pose.pixelRect.right
        partPose.updateRect()
        while (x < size.x - poseWidth) {
            if (colored) {
                renderer.drawTexture(
                    pose.texture,
                    left + x, bottom + topEdge, left + x + poseWidth, bottom + size.y,
                    partPose.rect,
                    tint = actor.tint,
                    modelMatrix = modelMatrix
                )
            } else {
                renderer.drawTexture(
                    pose.texture,
                    left + x, bottom + topEdge, left + x + poseWidth, bottom + size.y,
                    partPose.rect,
                    modelMatrix = modelMatrix
                )
            }
            x += poseWidth
        }

        // Draw the partial piece in the top right corner
        if (rightEdge < size.x && topEdge < size.y) {
            partPose.pixelRect.right = (pose.pixelRect.left + partWidth).toInt()
            partPose.pixelRect.top = (pose.pixelRect.bottom - partHeight).toInt()
            partPose.updateRect()
            if (colored) {
                renderer.drawTexture(
                    pose.texture,
                    left + rightEdge, bottom + topEdge, left + size.x, bottom + size.y.toFloat(),
                    partPose.rect,
                    tint = actor.tint,
                    modelMatrix = modelMatrix
                )
            } else {
                renderer.drawTexture(
                    pose.texture,
                    left + rightEdge, bottom + topEdge, left + size.x, bottom + size.y.toFloat(),
                    partPose.rect,
                    modelMatrix = modelMatrix
                )
            }
        }

    }

    override fun toString() = "TiledAppearance pose=$pose size=$size"
}

private val WHITE = Color.white()
