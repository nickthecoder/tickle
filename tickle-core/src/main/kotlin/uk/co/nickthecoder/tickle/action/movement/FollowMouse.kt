/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.stage.WorldView
import uk.co.nickthecoder.tickle.util.GraphicalMouse
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * [position] will be set to the position of the mouse, in world coordinates of the specified
 * [view].
 *
 * This is useful if you wish to turn off the mouse pointer in the Scene details, and
 * replace it with an Actor (i.e. a graphical mouse pointer), rather than the "boring" normal one!
 *
 * The action never finishes.
 *
 * Consider pairing this with [HideWhenMouseIsOutside]. e.g.
 *
 *      HideWhenMouseIsOutside( actor ) and FollowMouse( actor )
 *
 * See [GraphicalMouse], whose action does just that!
 */
class FollowMouse(
    val position: Vector2,
    val view: WorldView,
    val constrainToWindow: Boolean

) : Action {

    constructor(actor: Actor, constrainToWindow: Boolean)
            : this(actor.position, actor.stage!!.firstView()!!, constrainToWindow)

    constructor(actor: Actor)
            : this(actor.position, actor.stage!!.firstView()!!, constrainToWindow = false)

    override fun act(): Boolean {
        if (constrainToWindow) {
            view.getMousePositionWorldBounded(position)
        } else {
            view.getMousePositionWorld(position)
        }
        return false
    }
}
