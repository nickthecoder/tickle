/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.Clock
import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.action.animation.Turn
import uk.co.nickthecoder.tickle.action.animation.TurnBy
import uk.co.nickthecoder.tickle.action.animation.TurnTo
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Changes [position] by [unitsPerSecond] in the direction of [heading].
 *
 * This is quite different from [ForwardsBy], which lasts a set number of seconds, whereas [Forwards] lasts forever.
 * To stop after a set number of seconds use :
 *
 *      myForwardsAction whilst Delay( seconds )
 *
 * Note you may change [heading] during the course of this Action, in which case,
 * the path will no longer be in a straight line.
 * For example, if the [heading] is changed constantly, (using [Turn], [TurnTo] or [TurnBy]), then the path
 * will form a spiral.
 *
 * Beware, if you do change [heading], the final destination isn't as predictable as you may expect.
 * The path is made up of small straight lines for each tick, and small differences in timing of each tick,
 * can lead to large deviations over long time periods.
 */
class Forwards(

    val position: Vector2,
    val unitsPerSecond: Float,
    val heading: Angle

) : Action {

    private var previousTime = Clock.seconds

    override fun begin(): Boolean {
        previousTime = Clock.seconds
        return super.begin()
    }

    override fun act() : Boolean {
        val diff = (Clock.seconds - previousTime).toFloat()

        position += (heading.vector().apply {
            this *= unitsPerSecond * diff
        })
        return false
    }
}
