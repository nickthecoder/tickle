/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action

/**
 * [Ease]s are a simple way to make movements (and other Actions) more visually appealing.
 * They can change the `speed` of an Action.
 *
 * For example [Eases.easeIn] will start off slow, and gradually get faster, but end
 * abruptly (without slowing down).
 * Note that the total time of an Action is the same regardless of the [Ease] used.
 * So the `average speed` stays the same.
 *
 * A [LinearEase] (which is the default) will keep a constant `speed` throughout.
 *
 * Note, eases are not confined to movements. For example, the Fade actions also use [Ease], and
 * therefore you can control the `speed` with which the alpha channel changes.
 *
 */
interface Ease {

    /**
     * Given a number in the range 0..1, transform it in some way, but remaining in the range 0..1.
     *
     * Note, this isn't quite true. You may exceed this range in rare cases.
     * [Eases.easeInBack], [Eases.easeOutBack], [Eases.easeInOutBack] exceed the range 0..1
     */
    fun ease(t: Float): Float

    fun backwards() = BackwardsEase(this)
    fun forwardsAndBackwards() = ForwardsAndBackwardsEase(this)
}

/**
 * A no-op ease. [ease] returns the same `t` value that it was given.
 */
class LinearEase : Ease {
    override fun ease(t: Float) = t

    companion object {
        val instance = LinearEase()
    }
}

/**
 * You can make make any eased Action go backwards using this. e.g.
 *
 *      MoveBy( actor.position, 2, Vector2( 100, 0 ), Eases.easeIn.backwards() )
 *
 * The actor will jump to the right, and then move leftwards.
 *
 * Due to the jump, this may only be useful for looping actions, such as Elliptical,
 * or closed Paths. (where there will be no jump, because the start and end positions are the same).
 *
 */
class BackwardsEase(val ease: Ease) : Ease {
    override fun ease(t: Float) = 1 - t
}

/**
 * For values of `t` 0 .. 0.5, we map them to 0..1
 *
 * For values of `t` 0.5 .. 1, we map then to 1..0
 *
 * The endpoint of the animation will be the same as the start.
 */
class ForwardsAndBackwardsEase(val ease: Ease) : Ease {
    override fun ease(t: Float) = if (t < 0.5f) ease.ease(t * 2) else ease.ease(1 - t) * 2
}
