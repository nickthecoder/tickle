/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.misc.PixelArray

/**
 * Create a set of [Pose]s, where each pose is just *part* of the original.
 * These fragments can then be used to create an explosion effect, when an Actor dies, it is
 * replaced by a set of Actors, one for each fragment, which head out on their own trajectories.
 *
 * [Producer.begin] is a good place to call this from.
 *
 * The source pose is fragmented using a "mask", where each pixel is colour coded to indicate
 * which pixels belong to each fragment.
 * e.g. If you want three fragments, then use a pose with just red, green and blue pixels
 * (the actual colours don't matter).
 * The alpha channel is ignored, and it is often nice to use the same alpha channel as the source pose.
 * When creating this colour mask, be sure that each pixel is EXACTLY the right colour. This means you
 * cannot use a "fuzzy" brush.
 *
 * While it is usual for each fragment to be made up of a single contiguous block of pixels,
 * this isn't required. So if you use a mask with two separate blobs of blue pixels, these will
 * form ONE fragment.
 */
class FragmentMaker(
        val sourcePose: Pose,
        val maskPose: Pose,
        val texture: ManagedTexture) {

    constructor(costume: Costume, sourceEventName: String = "default", maskEventName: String = "fragmentMask", texture: ManagedTexture) :
            this(costume.choosePose(sourceEventName)!!, costume.choosePose(maskEventName)!!, texture)

    fun generate(): List<FragmentInfo> {

        if (maskPose.pixelRect.width > sourcePose.pixelRect.width) throw IllegalArgumentException("The mask is wider than the source")
        if (maskPose.pixelRect.height > sourcePose.pixelRect.height) throw IllegalArgumentException("The mask is taller than the source")

        val width = sourcePose.pixelRect.width
        val height = sourcePose.pixelRect.height

        val source = PixelArray(sourcePose.texture)
        val mask = PixelArray(maskPose.texture)

        fun sourcePixelAt(x: Int, y: Int): Int {
            return source.pixelAt(x + sourcePose.pixelRect.left, y + sourcePose.pixelRect.top)
        }

        fun maskColorAt(x: Int, y: Int): Int {
            return mask.colorAt(x + maskPose.pixelRect.left, y + maskPose.pixelRect.top)
        }

        // Create FragmentInfo for each unique color in the mask image
        val infoMap = mutableMapOf<Int, FragmentInfo>()
        for (y in 0 until height) {
            for (x in 0 until width) {
                val color = maskColorAt(x, y)
                if (color != 0) {
                    val info = infoMap[color]
                    if (info == null) {
                        infoMap[color] = FragmentInfo(color, x, y)
                    } else {
                        info.update(x, y)
                    }
                }
            }
        }

        // Create poses
        for (info in infoMap.values) {

            val dest = PixelArray(info.width, info.height)

            for (y in 0 until dest.height) {
                val sourceY = y + info.minY
                for (x in 0 until dest.width) {
                    val sourceX = x + info.minX
                    if (maskColorAt(sourceX, sourceY) == info.color) {
                        dest.setPixel(x, y, sourcePixelAt(sourceX, sourceY))
                    } else {
                        dest.setAlpha(x, y, 0)
                    }
                }
            }
            val pose = texture.add(dest)
            pose.direction.radians = sourcePose.direction.radians

            // Make the Pose's offset The center of gravity
            pose.offsetX = Math.round(info.centerX).toFloat() - info.minX
            pose.offsetY = pose.pixelRect.height - (Math.round(info.centerY).toFloat() - info.minY)

            // When we create the fragments, we need to know how much to offset them by, so that they appear
            // in the same place as the original.
            // We'll "abuse" the snapPoints to store this info! It has NOTHING to do with snapPoints!!!
            pose.snapPoints.add(
                Vector2(
                    pose.offsetX - (sourcePose.offsetX - info.minX),
                    pose.offsetY - (sourcePose.offsetY - (sourcePose.pixelRect.height - info.maxY))
                )
            )
            info.pose = pose
        }

        return infoMap.values.toList()
    }

    class FragmentInfo(val color: Int, var minX: Int, var minY: Int) {
        var pose: Pose? = null

        /**
         * Relative to the source pose (from the TOP)
         */
        var maxX: Int = minX
        var maxY = minY

        val width: Int
            get() = maxX - minX + 1

        val height: Int
            get() = maxY - minY + 1

        private var sumX: Double = 0.0
        private var sumY: Double = 0.0
        private var pixelCount = 0

        /**
         * Relative to the source pose (from the TOP)
         */
        val centerX
            get() = sumX / pixelCount
        val centerY
            get() = sumY / pixelCount

        fun update(x: Int, y: Int) {
            sumX += x
            sumY += y
            pixelCount++
            if (x < minX) minX = x
            if (x > maxX) maxX = x
            if (y < minY) minY = y
            if (y > maxY) maxY = y
        }
    }

    companion object {

        @JvmStatic
        fun addToCostume(poses: Collection<Pose>, costume: Costume, fragmentEventName: String = "fragments") {
            var event = costume.events[fragmentEventName]
            if (event == null) {
                event = CostumeEvent(costume)
                costume.events[fragmentEventName] = event
            }

            event.poses.addAll(poses)
        }

        @JvmStatic
        fun randomMask(pose: Pose, fragmentCount: Int, alphaThreshold: Int = 1): PixelArray {

            val width = pose.pixelRect.width
            val height = pose.pixelRect.height
            val source = PixelArray(pose.texture)
            val dest = PixelArray(width, height)

            val neighbours = List(fragmentCount) { mutableListOf<Pair<Int, Int>>() }

            // Create seeds points.
            for (n in 0 until fragmentCount) {
                var x = Rand.randomInt(width)
                var y = Rand.randomInt(height)
                // Don't let the same seed point be used for two fragments.
                // The seed points cannot be on transparent pixels
                while (dest.colorAt(x, y) != 0 || source.alphaAt(
                        x + pose.pixelRect.left,
                        y + pose.pixelRect.top
                    ) < alphaThreshold
                ) {
                    x = Rand.randomInt(width)
                    y = Rand.randomInt(height)
                }
                // Change the pixel color to mark that it is being used.
                dest.setColor(x, y, 1)
                neighbours[n].add(Pair(x, y))
            }
            // Reset the seed points color
            for (n in 0 until fragmentCount) {
                val (x, y) = neighbours[n][0]
                dest.setColor(x, y, 0)
            }

            fun listAdd(list: MutableList<Pair<Int, Int>>, x: Int, y: Int) {
                if (x >= 0 && y >= 0 && x < width && y < height
                    && dest.colorAt(x, y) == 0 //0x006600
                    && source.alphaAt(x + pose.pixelRect.left, y + pose.pixelRect.top) >= alphaThreshold
                ) {
                    list.add(Pair(x, y))
                }
            }

            // Flood fill from the seed points till the whole image is filled.
            var done = 0
            var pixelsSet = 0
            while (done < fragmentCount) {
                done = 0
                for (n in 0 until fragmentCount) {
                    val list = neighbours[n]
                    if (list.isEmpty()) {
                        done++
                    } else {
                        val i = Rand.randomInt(list.size)
                        val (x, y) = list[i]
                        list.removeAt(i)
                        if (dest.colorAt(x, y) == 0) {
                            pixelsSet++
                            dest.setColor(x, y, 1 + n * 255 / fragmentCount)
                            dest.setAlpha(x, y, source.alphaAt(x + pose.pixelRect.left, y + pose.pixelRect.top))

                            // Add neighbours if they aren't already being used.
                            listAdd(list, x - 1, y)
                            listAdd(list, x + 1, y)
                            listAdd(list, x, y - 1)
                            listAdd(list, x, y + 1)
                        }
                    }
                }
            }

            return dest
        }

        /**
         * Generates a randomly generated Pose suitable for the colorMask of [FragmentMaker].
         * Note, the new pose is added to a new Texture, so dispose of this texture when you've finished with it.
         *
         * This is quite inefficient, so it is slow for large Poses. Sorry.
         * Don't use this during game play!
         */
        @JvmStatic
        fun randomMaskPose(pose: Pose, fragmentCount: Int, alphaThreshold: Int = 1): Pose {
            val texture = randomMask(pose, fragmentCount, alphaThreshold).toTexture()
            return Pose(texture, YDownRect(0, 0, texture.width, texture.height))
        }


        /**
         * Create a set of Actors at the same position as the [parent], and on the same Stage.
         * The [Pose] is taken from the parent's Costume event with name [eventName].
         * The roles for the new Actors are null.
         * The costumes are null too.
         */
        @JvmStatic
        fun createFragmentActors(parent: Actor, eventName: String = "fragments"): List<Actor> {
            val event = parent.costume.events[eventName] ?: return emptyList()

            val fragments = mutableListOf<Actor>()
            for (pose in event.poses) {
                val fragment = Actor(parent.costume)
                fragment.appearance = PoseAppearance(fragment, pose)
                fragments.add(fragment)
                fragment.position.set(parent.position)
                fragment.zOrder = parent.zOrder
                fragment.direction.set(parent.direction)
                fragment.scale.set(parent.scale)
                parent.stage?.add(fragment)
                // FragmentMaker put the offset into a snap point. Use it if it is there.
                pose.snapPoints.firstOrNull()?.let { offset ->
                    val radians = parent.direction.radians - (parent.poseAppearance?.directionRadians ?: 0.0)
                    val transformedOffset = offset * parent.scale
                    transformedOffset.setRotateRadians(-radians)

                    fragment.x += transformedOffset.x
                    fragment.y += transformedOffset.y

                }
            }

            return fragments
        }

        @JvmStatic
        fun createFragmentRoles(parent: Actor, roleFactory: FragmentRoleFactory): List<Role> {
            return createFragmentActors(parent, "fragments").map { fragment ->
                val offset = fragment.position - parent.position
                val role = roleFactory.create(fragment, offset)
                fragment.role = role
                role
            }
        }
    }

}

interface FragmentRoleFactory {
    fun create(actor: Actor, offset: Vector2): Role
}
