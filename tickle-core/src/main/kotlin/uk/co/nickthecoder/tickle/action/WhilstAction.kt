/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action

/**
 * Acts on both the [body] and [condition] in parallel.
 * When [condition] ends before [body], this action also ends (and [body] is not given a chance to complete).
 *
 * However, if the [body] ends before the [condition], then the behaviour depends on [endEarly] :
 *
 * *        [endEarly] == true, then this action ends when the [body] ends.
 * *        [endEarly] == false, then [condition] is allowed to continue, and this action only
 *          ends when both parts have ended.
 *
 * Example. Suppose we have a "circularPath" action, which moves an actor in circles (or other shaped paths).
 * If it takes 4 seconds to complete one lap, then :
 *
 *      circularPathAction.forever() whilst Delay(10)
 *
 * will completed 2 and a half laps, and then stop. Nice ;-)
 */
class WhilstAction(
        val condition: Action,
        val body: Action,
        val endEarly: Boolean

) : Action {

    constructor( condition: Action, body : Action ) : this( condition, body, true )

    private var conditionFinished = false
    private var bodyFinished = false

    override fun begin(): Boolean {
        bodyFinished = false
        conditionFinished = condition.begin()
        return if (conditionFinished) {
            true
        } else {
            bodyFinished = body.begin()
            if (endEarly) {
                bodyFinished
            } else {
                false
            }
        }
    }

    override fun act(): Boolean {
        conditionFinished = condition.act()
        return if (conditionFinished) {
            true
        } else {
            if (!bodyFinished) {
                bodyFinished = body.act()
            }
            if ( endEarly ) {
                bodyFinished
            } else {
                false
            }
        }
    }
}
