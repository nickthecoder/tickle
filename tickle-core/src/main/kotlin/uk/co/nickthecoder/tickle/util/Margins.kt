package uk.co.nickthecoder.tickle.util

class Margins(
    @JvmField
    var left: Float,
    @JvmField
    var bottom: Float,
    @JvmField
    var right: Float,
    @JvmField
    var top: Float
) {

    override fun toString() = "Margins : Left: $left   bottom: $bottom   right: $right   top: $top"

}

