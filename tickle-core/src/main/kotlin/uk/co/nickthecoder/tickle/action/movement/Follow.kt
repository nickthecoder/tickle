/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.action.Action
import uk.co.nickthecoder.tickle.util.Angle
import uk.co.nickthecoder.tickle.util.IVector2
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Follows another position.
 *
 * The most common use for this is when an item in a game is made up of two (or more) parts,
 * and the subordinate parts follow the main part.
 *
 * For example, a tank's turret may follow the tank's body.
 * If the turret is offset from the tank (e.g. nearer the front), then make use of [offset],
 * and pass the tank body's direction to [rotation].
 *
 * Consider also using [FollowAngle] on the turret's direction to ensure that when the body rotates, the turret
 * does too.
 *
 * Note, if [offset] is null, then [rotation] does nothing, and is ignored.
 *
 * Note, it may be tempting to use this action on the [follower]'s Role (the turret).
 * However, this is bad style, because it assumes that the follower's tick() is called after the following's tick().
 * IMHO, In the example above it is safer to [act] on this action at the end of the tank body's tick method.
 * If you don't follow this advice, the actors may end up 1 frame out of sync with each other.
 */
class Follow(

    val follower: Vector2,
    val following: IVector2,
    val offset: IVector2?,
    val rotation: Angle?

) : Action {

    constructor(follower: Vector2, following: Vector2, offset: Vector2)
            : this(follower, following, offset, null)

    constructor(follower: Vector2, actor: Actor, offset: Vector2)
            : this(follower, actor.position, offset, actor.direction)

    private val tmp = Vector2()


    override fun act(): Boolean {
        if (offset == null) {
            follower.set(following)
        } else {
            if (rotation != null) {
                tmp.set(offset)
                tmp.setRotate(rotation)
                follower.setPlus(following, tmp)
            } else {
                follower.setPlus(following, offset)
            }
        }
        return false
    }
}
