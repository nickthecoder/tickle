package uk.co.nickthecoder.tickle.action.movement

import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.Eases
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.path.Shape
import uk.co.nickthecoder.tickle.path.Path
import uk.co.nickthecoder.tickle.path.ContiguousShape
import uk.co.nickthecoder.tickle.util.Vector2

/**
 * Updates [position] over time, to follow a [ContiguousShape].
 *
 * NOTE, to follow a [Path], it is more efficient to use [FollowPath], which has
 * various optimisations.
 */
class FollowShape(val position: Vector2, seconds: Float, val shape: Shape<*>, ease: Ease) :
    TimedAnimation(seconds, ease) {

    constructor(position: Vector2, seconds: Float, shape: Shape<*>) : this(position, seconds, shape, Eases.linear)

    override fun update(t: Float) {
        shape.along(t, position)
    }

    override fun storeInitialValue() {
    }
}
