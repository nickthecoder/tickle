/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action.animation

import uk.co.nickthecoder.tickle.Actor
import uk.co.nickthecoder.tickle.action.Ease
import uk.co.nickthecoder.tickle.action.LinearEase
import uk.co.nickthecoder.tickle.action.TimedAnimation
import uk.co.nickthecoder.tickle.util.Vector2
import uk.co.nickthecoder.tickle.misc.lerp

/**
 * The final value of [scale] will be [scale] * [by] at the end of the action.
 */
class ScaleBy(

    val scale: Vector2,
    seconds: Float,
    val by: Vector2,
    ease: Ease = LinearEase.instance

) : TimedAnimation(seconds, ease) {

    constructor(scale: Vector2, seconds: Float, by: Vector2) :
            this(scale, seconds, by, LinearEase.instance)

    constructor(scale: Vector2, seconds: Float, by: Float, ease: Ease)
            : this(scale, seconds, Vector2(by, by), ease)

    constructor(scale: Vector2, seconds: Float, by: Float) :
            this(scale, seconds, by, LinearEase.instance)


    // We include constructors which take an Actor for convenience.
    // And pass actor.scale to the constructors above.

    constructor(actor: Actor, seconds: Float, by: Vector2, ease: Ease) :
            this(actor.scale, seconds, by, ease)

    constructor(actor: Actor, seconds: Float, by: Vector2) :
            this(actor, seconds, by, LinearEase.instance)

    constructor(actor: Actor, seconds: Float, by: Float, ease: Ease)
            : this(actor, seconds, Vector2(by, by), ease)

    constructor(actor: Actor, seconds: Float, by: Float) :
            this(actor, seconds, by, LinearEase.instance)


    private var initialScale = Vector2()

    override fun storeInitialValue() {
        initialScale.set(scale)
    }

    override fun update(t: Float) {
        val scaleBy = Vector2(1f,1f)
        lerp(scaleBy, by, t, scaleBy)
        scale.set( initialScale * scaleBy )
    }

}
