package uk.co.nickthecoder.tickle.path

import uk.co.nickthecoder.tickle.misc.lerp
import uk.co.nickthecoder.tickle.util.*
import kotlin.math.sqrt

/**
 * Note, when moving along the ellipse, I am using time `t` to choose an angle between the start and end angles.
 * However, for elongated ellipses, this means the speed will change
 * (slow near the left/right and fast near the top/bottom when the ellipse is stretched in the X axis).
 */
class EllipticalArc(
    override val start: Vector2,
    end: IVector2,
    radius: IVector2,
    var largeArc: Boolean,
    var sweepArc: Boolean,
    tilt: IAngle? = null
) :
    ContiguousShape<EllipticalArc> {

    constructor(from: Vector2, to: Vector2, radius: Vector2, largeArc: Boolean, sweepArc: Boolean) :
            this(from, to, radius, largeArc, sweepArc, null)

    override var end = end.mutable()
        private set

    var radius = radius.mutable()
    var tilt = tilt?.mutable()
    private var startRadians = 0.0
    private var endRadians = 0.0
    private var length: Float = 0f

    private val center = Vector2()

    init {
        recalculate()
    }

    private fun recalculate() {
        val tilt = tilt
        // Special case for when doing a complete revolution
        // We would get divide by zeros without this!
        if (end == start) {
            if (sweepArc) {
                center.set(-radius.x, 0f)
                startRadians = 0.0
                endRadians = Math.PI * 2.0
            } else {
                center.set(radius.x, 0f)
                startRadians = Math.PI
                endRadians = -Math.PI
            }
            if (tilt != null) {
                center.setRotate(tilt)
            }
            center += start
        } else {

            val a = start
            val b = end

            // transform a and b by : rotate, then scale by 1/radius then rotate back, and we can then solve for the simpler case of a circle of radius 1
            val ta = if (tilt == null) a / radius else (a.rotateRadians(-tilt.radians) / radius).rotate(tilt)
            val tb = if (tilt == null) b / radius else (b.rotateRadians(-tilt.radians) / radius).rotate(tilt)

            // A unit vector of the normal of the line AB
            val normal = (tb - ta).perpendicular()
            var d = (normal.x * normal.x + normal.y * normal.y)
            val unitNormal = normal / Math.sqrt(d.toDouble()).toFloat()

            d = sqrt(Math.max(0f, 1f - d / 4))
            if (largeArc == sweepArc) {
                d = -d
            }
            center.x = (tb.x + ta.x) / 2 + d * unitNormal.x
            center.y = (tb.y + ta.y) / 2 + d * unitNormal.y
            val oa = ta - center
            val ob = tb - center

            startRadians = Math.acos(oa.x / oa.length().toDouble())
            if (oa.y < 0.0) startRadians = -startRadians

            endRadians = Math.acos(ob.x / ob.length().toDouble())
            if (ob.y < 0.0) endRadians = -endRadians

            if (sweepArc && startRadians > endRadians) {
                endRadians += 2 * Math.PI
            }
            if (!sweepArc && startRadians < endRadians) {
                endRadians -= 2 * Math.PI
            }

            center *= radius
        }

        // Calculate an approximation of the full perimeter
        // https://www.mathsisfun.com/geometry/ellipse-perimeter.html
        val perimeter = Math.PI * (3.0 * (radius.x + radius.y) -
                Math.sqrt(
                    (3.0 * radius.x + radius.y) *
                            (3.0 * radius.y + radius.x)
                ))

        // Calculate the angle we are travelling through compared to a full ellipse.
        var da = endRadians - startRadians
        if (da < 0) da = -da
        if (da > Angle.TAU) da -= Angle.TAU
        if ((largeArc && da < Math.PI) || (!largeArc && da > Math.PI)) {
            da = Angle.TAU - da
        }
        length = (perimeter * da / Angle.TAU).toFloat()
    }


    override fun joinEnd(to: Vector2) {
        end = to
    }

    override fun length() = length

    private fun point(theta: Double): Vector2 {
        val result = Vector2(
            Math.cos(theta).toFloat() * radius.x,
            Math.sin(theta).toFloat() * radius.y
        )
        tilt?.let { result.setRotate(it) }
        result += center
        return result
    }

    private fun point(theta: Double, result: Vector2) {
        result.set(
            Math.cos(theta).toFloat() * radius.x,
            Math.sin(theta).toFloat() * radius.y
        )
        tilt?.let { result.setRotate(it) }
        result += center
    }

    override fun along(t: Float, into: Vector2) {
        val theta = lerp(startRadians, endRadians, t.toDouble())
        point(theta, into)
    }

    override fun translate(by: IVector2) =
        openOrClosed(EllipticalArc(start + by, end + by, radius, largeArc, sweepArc, tilt))

    override fun scale(by: IVector2) =
        openOrClosed(EllipticalArc(start * by, end * by, radius * by, largeArc, sweepArc, tilt))

    override fun rotate(by: IAngle): EllipticalArc {
        val tilt = tilt
        val newTilt = if (tilt == null) {
            Angle(by)
        } else {
            tilt + by
        }
        return openOrClosed(EllipticalArc(start.rotate(by), end.rotate(by), radius, largeArc, sweepArc, newTilt))
    }

    override fun reverse() = openOrClosed(EllipticalArc(end, start, radius, largeArc, sweepArc))

    override fun copy() = openOrClosed(EllipticalArc(Vector2(start), Vector2(end), radius, largeArc, sweepArc, tilt))


    /**
     * This general form (defined in [ContiguousShape]) calls [buildPoints] without [angleTolerance].
     * @param distanceTolerance
     * @param angleTolerance Ignored.
     */
    override fun buildPoints(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {
        buildPoints(into, distanceTolerance)
    }

    /**
     * Uses recursive binary chop from the [start] to [end].
     * Each time we recurse, we have two angles, and their correct points on the ellipse.
     * We then take the midpoint or the chord, and calculate the distance to the circumference.
     * If this distance > [distanceTolerance], we recurse, otherwise we ignore the midpoint,
     * and use the chord.
     *
     * Improvements
     * ============
     * This is NOT a good implementation!
     *
     * The ellipse always _inscribes_ the PolyLine (all points of the ellipse either touch, or are outside
     * the Polyline).
     * It would be better if the error were equally inside and outside.
     *
     * Because we use binary chop, we only ever have a power of two (for circles).
     * e.g. we get a jump from 32 to 64 to 128 points.
     * Ideally, the number of points would grow gradually.
     *
     * A special case for circles could be quicker (as the error is always constant).
     */
    fun buildPoints(into: MutableList<Vector2>, distanceTolerance: Float) {

        fun recursiveEllipse(fromAngle: Double, toAngle: Double, fromPoint: Vector2, toPoint: Vector2) {
            val approximate = (fromPoint + toPoint) / 2f
            val midAngle = (fromAngle + toAngle) / 2
            val actual = point(midAngle)
            val error = approximate - actual

            if (error.length() > distanceTolerance) {
                recursiveEllipse(fromAngle, midAngle, fromPoint, actual)
                into.add(actual)
                recursiveEllipse(midAngle, toAngle, actual, toPoint)
            }
        }

        if (radius.x == radius.y) {
            // Special case for circular arcs
            // Consider a sector of a circle with angle `a`. We could approximate the circle using this sector's
            // chord. Can we pick such a sector, so that the error is exactly [distanceTolerance]?
            // Bisect the sector (angle `theta` = a/2).
            // We now have a pair of right-angled triangles: Angle=theta, Hypotenuse=radius, Adjacent = radius-error.
            // So Cos(theta) = (radius-error)/radius
            // We can solve for theta using [distanceTolerance] as our error.
            val theta = Math.acos((radius.x - distanceTolerance) / radius.x.toDouble())

            // So how many lines do we need so that the angle is theta (or just a little less)
            val lines = Math.ceil(Math.abs((endRadians - startRadians) / theta)).toInt()
            val delta = (endRadians - startRadians) / lines
            var angle = startRadians + delta
            for (i in 1 until lines) {
                into.add(point(angle))
                angle += delta
            }
            into.add(end)
        } else {
            // General case of elliptical arcs (not circular)
            recursiveEllipse(startRadians, endRadians, start, end)
            into.add(end)
        }
    }


    override fun dirty() {
        recalculate()
    }

    override fun toString() =
        "EllipticalArc $start -> $end r=$radius largeArc=$largeArc sweepArc=$sweepArc tilt =$tilt"

    companion object {
        @JvmStatic
        fun circle(center: Vector2, radius: Float): EllipticalArc {
            return EllipticalArc(
                center + Vector2(radius, 0f),
                center + Vector2(radius, 0f),
                Vector2(radius, radius),
                largeArc = false,
                sweepArc = false
            )
        }
    }

}
