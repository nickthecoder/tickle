/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.sandbox

import org.lwjgl.glfw.GLFW
import uk.co.nickthecoder.tickle.*
import uk.co.nickthecoder.tickle.events.*
import uk.co.nickthecoder.tickle.graphics.*
import uk.co.nickthecoder.tickle.resources.Resources
import uk.co.nickthecoder.tickle.util.YDownRect


abstract class Sandbox(val title: String = "Sandbox", val width: Int = 800, val height: Int = 600)

    : WindowListener {

    lateinit var window: Window

    lateinit var renderer: Renderer

    val resources = Resources()
    lateinit var game: Game

    fun createEllipseTexture(
        width: Int,
        height: Int,
        radius: Double,
        foreground: Byte = 255.toByte(),
        background: Byte = 0.toByte()
    ): Texture {
        val pixels = ByteArray(width * height * 4) { byteIndex ->
            val pixelIndex = byteIndex / 4
            val rx = (pixelIndex % width - width / 2).toDouble() / width * 2 // -1..1
            val ry = (pixelIndex / width - height / 2).toDouble() / height * 2 // -1..1
            // A quick and dirty way to create an ellipse! White, with a semi-transparent grey background.
            val radiusSquared = rx * rx + ry * ry
            if (radiusSquared > radius * radius) background else foreground
        }
        return Texture.create(width, height, pixels)
    }

    fun createEllipsePose(
        poseName: String,
        width: Int,
        height: Int,
        radius: Double,
        foreground: Byte = 255.toByte(),
        background: Byte = 0.toByte()
    ): Pose {
        val texture = createEllipseTexture(width, height, radius, foreground, background)
        val pose = Pose(texture, YDownRect(0, 0, width, height)).apply {
            offsetX = width / 2f
            offsetY = height / 2f
        }
        resources.poses.add(poseName, pose)
        return pose
    }

    // Creates a Costume, with a single Pose (an ellipse), and an actor using that costume.
    fun createEllipseActor(costumeName: String, pose: Pose): Actor {
        val costume = Costume(resources)

        costume.events["default"] = CostumeEvent(costume).apply { poses.add(pose) }
        resources.costumes.add(costumeName, costume)

        val actor = Actor(costume)
        actor.appearance = PoseAppearance(actor, pose)

        return actor
    }

    fun start() {
        OpenGL.begin()
        onOpenGLThread {
            try {
                window = Window(title, width, height)
                renderer = Renderer.instance()

                window.show()
                window.listeners.add(this)

                game = Game(window, resources)

                prepare()

                while (!window.shouldClose()) {
                    GLFW.glfwPollEvents()
                    tick()
                    window.swap()
                    game.seconds = System.nanoTime() / 1_000_000_000.0
                }
                cleanUp()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    abstract fun prepare()

    abstract fun tick()

    open fun cleanUp() {
        Renderer.cleanUp()
        window.delete()
        OpenGL.end()
    }

    override fun onKey(event: KeyEvent) {
        println("Key pressed $event")
        if (event.key == Key.ESCAPE) {
            window.close()
        }
    }

    override fun onMouseButton(event: MouseEvent) {
    }

    override fun onResize(event: ResizeEvent) {
    }
}
