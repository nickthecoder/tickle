package uk.co.nickthecoder.tickle.util

import uk.co.nickthecoder.tickle.*

/**
 * Integer margins. Currently only used for nine-patches.
 *
 * The `preferred` way to create a nine patch, is to set [Pose]'s [Pose.resizeType] to [ResizeType.NINE_PATCH].
 * However, in the past (and kept for backwards compatibility), a [Costume] could have a
 * [CostumeEvent], with a [NinePatch].
 */
open class IntegerMargins(
    @JvmField
    var left: Int,
    @JvmField
    var bottom: Int,
    @JvmField
    var right: Int,
    @JvmField
    var top: Int
) {
    override fun toString() = "IntegerMargins : Left: $left   bottom: $bottom   right: $right   top: $top"
}
