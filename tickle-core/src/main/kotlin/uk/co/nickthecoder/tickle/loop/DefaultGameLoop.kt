/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.loop

import uk.co.nickthecoder.tickle.Game
import uk.co.nickthecoder.tickle.graphics.Renderer
import uk.co.nickthecoder.tickle.graphics.Window

/**
 * This is the default implementation of [GameLoop].
 *
 * All the "tick" methods will be called, then the scene is drawn, and then the displayed image
 * is swapped.
 *
 * By default "swap" waits for the monitor's horizontal sync, and therefore the game's speed
 * will be governed by the monitor's refresh rate.
 * See [Window.enableVSync].
 *
 * However, if a one game loop iteration takes more time than the monitor's refresh rate,
 * then the game will run twice as slow (or three times...).
 *
 * If you have a high-speed monitor, then we do attempt to slow things done to something reasonable!
 * e.g. 140hz monitors are scaled down to 70 fps.
 * The aim, is to keep a frame rate of roughly 60fps.
 *
 * Using [DefaultGameLoop] means that you have no control over the period for a single "tick", and therefore,
 * if you move an Actor by a fixed number of pixels per tick, then that Actor will move at DIFFERENT speeds
 * depending on the computer on which it is running.
 * To get around this, either use a different implementation of [GameLoop], or take [Game.tickDuration] into
 * account when moving objects.
 *
 * If your games uses physics (JBox2d), then TickleWorld ensures that the time step is consistent.
 * i.e. objects controlled by JBox2d WiLL move using a constant time step, regardless of load
 * (at the expense of potential stuttering).
 *
 */
class DefaultGameLoop(game: Game) : AbstractGameLoop(game) {

    private var rate: Double

    private var extraDelays: Int = 0

    /**
     * Note, if only 1 frame is reported to be dropped every now and then, it is still possible to get smooth game play,
     * because subsequent frames may be quick enough to take up the slack.
     * Alas, the algorithm isn't clever enough to know if a frame is *really* dropped :-(
     * However, reported dropped frames is still an indicator that something is wrong!
     */
    override var droppedFrames = 0

    private val expectedFrameNanos: Long

    init {
        rate = 60.0
        for (scale in 1..10) {
            val foo = Game.instance.window.refreshRate().toDouble() / scale
            if (foo <= 80) {
                rate = foo
                extraDelays = scale - 1
                break
            }
        }
        expectedFrameNanos = (1_000_000_000 / rate).toLong()
    }

    override fun expectedFrameRate() = rate

    override fun loopBody() {
        val bodyStart = System.nanoTime()

        tick()
        mouseMoveTick()
        processRunLater()
        game.scene.draw(Renderer.instance())
        val bodyDuration = System.nanoTime() - bodyStart
        if (bodyDuration > expectedFrameNanos) {
            //println("Dropped frame : ${(expectedFrameNanos - bodyDuration) / 1000} μs")
            droppedFrames++
        }
        game.window.swap()
        if (extraDelays != 0) {
            Thread.sleep((1_000_000 * extraDelays / rate).toLong())
        }
    }

}
