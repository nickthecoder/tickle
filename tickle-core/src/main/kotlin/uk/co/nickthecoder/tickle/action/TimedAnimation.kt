/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.tickle.action

import uk.co.nickthecoder.tickle.Clock

/**
 * A base class for many Actions which are time based, and used [Ease]s.
 *
 * Implementing classes should keep to the following conventions for constructor parameters order :
 *
 *  * The object being changed (such as a Vector2, or Angle etc)
 *  * The time in [seconds]
 *  * Attributes which describe the action (such as the amount of movement, the direction etc).
 *  * And finally the [Ease] (which can always be omitted, and defaults to linear).
 */
abstract class TimedAnimation(
    val seconds: Float,
    val ease: Ease

) : Action {

    protected var startTime = -1.0

    override fun begin(): Boolean {
        startTime = Clock.seconds
        storeInitialValue()

        if (seconds <= 0f) {
            update(1f)
            ended()
            return true
        }

        return false
    }

    override fun act(): Boolean {
        val now = Clock.seconds
        val s = Math.min(1.0, (now - startTime) / seconds).toFloat()

        val t = ease.ease(s)
        update(t)
        if (s == 1f) {
            ended()
            return true
        }
        return false
    }

    /**
     * A chance for subclasses to store initial values.
     * For example MoveTo needs to store the initial position.
     */
    protected abstract fun storeInitialValue()

    /**
     * Where all the magic happens.
     *
     * @param t The proportion through the action usually in the range 0..1.
     * However, some ease functions overshoot (and therefore the range extends beyond 1),
     * and some initially "reverse", so [t] can also be negative.
     */
    protected abstract fun update(t: Float)

    /**
    A chance for subclasses to clean up, at the end of the action.
    Rarely used.
     */
    protected open fun ended() {}

}
