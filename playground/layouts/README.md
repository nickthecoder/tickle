# layouts

This helped me while addind extra features related to auto-scaling views.

## menu scene

Uses a LETTERBOX view.

I set the background blue, and added an enirely black nine-patch covering the "deafult" size of the menu.
When we scale the window, we should be able to see some of the blue background, as the black nine-patch
is only within the letterboxed region.

To ensure that clipping is working as expected, there are red squares near the corners, which should be
clipped. These clipped squares show the extent of the view.

In a "real" game, the black nine-patch could be replaced with a background graphic, and the blue background
might be replaced by black.


